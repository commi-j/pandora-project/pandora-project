package tk.labyrinth.pandora.misc4j.java.io;

import org.apache.commons.io.FilenameUtils;
import tk.labyrinth.misc4j2.java.io.IoUtils;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Commitman
 * @version 1.0.3
 */
public class FileUtils {

	public static final int NOT_FOUND = -1;

	/**
	 * Returns child File of file.
	 *
	 * @param file non-null
	 * @param name fit
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static File child(@Nullable File file, String name) {
		File result;
		if (file != null) {
			result = new File(file, name);
		} else {
			result = null;
		}
		return result;
	}

	/**
	 * Returns the extension of file.
	 * <pre>
	 * "foo"     -&gt; foo
	 * "foo.bar" -&gt; "foo"
	 * ".bar"    -&gt; ""
	 * </pre>
	 *
	 * @param file non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static String getBaseName(File file) {
		String result;
		{
			Objects.requireNonNull(file, "file");
			//
			String name = file.getName();
			//
			final int index = FilenameUtils.indexOfExtension(name);
			//
			if (index != NOT_FOUND) {
				result = name.substring(0, index);
			} else {
				result = name;
			}
		}
		return result;
	}

	/**
	 * Returns the extension of file.
	 * <pre>
	 * "foo"      -&gt; null
	 * "foo."     -&gt; ""
	 * "foo.java" -&gt; "java"
	 * </pre>
	 *
	 * @param file non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static String getExtension(File file) {
		String result;
		{
			Objects.requireNonNull(file, "file");
			//
			String name = file.getName();
			//
			final int index = FilenameUtils.indexOfExtension(name);
			//
			if (index != NOT_FOUND) {
				result = name.substring(index + 1);
			} else {
				result = null;
			}
		}
		return result;
	}

	/**
	 * @param file non-null
	 *
	 * @return non-null
	 *
	 * @see org.apache.commons.io.FileUtils#readLines(File, Charset)
	 * @since 1.0.1
	 */
	public static List<String> readLines(File file) {
		return Objects.requireNonNull(IoUtils.tryReturnWithResourceSupplier(
				() -> new BufferedReader(new FileReader(file, StandardCharsets.UTF_8)),
				reader -> reader.lines().collect(Collectors.toList())));
	}
}
