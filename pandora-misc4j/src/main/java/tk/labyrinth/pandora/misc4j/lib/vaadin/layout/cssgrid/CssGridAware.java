package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;

import javax.annotation.Nullable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Mixin interface with CSS Grid features:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/">https://css-tricks.com/snippets/css/complete-guide-grid/</a><br>
 * <br>
 * Note: This one may add "grid-area" style property on children but does not remove it on child removal.<br>
 *
 * @author Commitman
 * @version 1.0.3
 * @see CssGridLayout
 */
public interface CssGridAware extends HasComponents, HasStyle {

	default void add(Component component, String gridArea) {
		component.getElement().getStyle().set(CssGridProperty.GRID_AREA.key(), gridArea);
		//
		add(component);
	}

	default void add(Component component, String gridColumn, String gridRow) {
		CssGridItem.setGridColumn(component, gridColumn);
		CssGridItem.setGridRow(component, gridRow);
		//
		add(component);
	}

	/**
	 * @param alignContent nullable, removes if null
	 *
	 * @see AlignContent
	 */
	default void setAlignContent(@Nullable AlignContent alignContent) {
		setAlignContent(this, alignContent);
	}

	/**
	 * @param alignItems nullable, removes if null
	 *
	 * @see AlignItems
	 */
	default void setAlignItems(@Nullable AlignItems alignItems) {
		setAlignItems(this, alignItems);
	}

	default void setDisplayGrid() {
		setDisplayGrid(this);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt="TODO"><br>
	 *
	 * @param gap nullable, removes if null
	 */
	default void setGap(@Nullable String gap) {
		setGap(this, gap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt="TODO"><br>
	 *
	 * @param columnGap nullable, removes if null
	 * @param rowGap    nullable, removes if null
	 */
	default void setGap(@Nullable String columnGap, @Nullable String rowGap) {
		setGap(this, columnGap, rowGap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-template-areas.svg" alt="TODO"><br>
	 *
	 * @param gridTemplateAreas nullable, removes if null
	 */
	default void setGridTemplateAreas(@Nullable CssGridTemplateAreas gridTemplateAreas) {
		setGridTemplateAreas(this, gridTemplateAreas);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-template-areas.svg" alt="TODO"><br>
	 *
	 * @param gridTemplateAreas nullable, removes if null
	 */
	default void setGridTemplateAreas(@Nullable String... gridTemplateAreas) {
		setGridTemplateAreas(this, gridTemplateAreas);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param gridTemplateColumns nullable, removes if null
	 */
	default void setGridTemplateColumns(@Nullable String gridTemplateColumns) {
		setGridTemplateColumns(this, gridTemplateColumns);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param gridTemplateRows nullable, removes if null
	 */
	default void setGridTemplateRows(@Nullable String gridTemplateRows) {
		setGridTemplateRows(this, gridTemplateRows);
	}

	/**
	 * @param justifyContent nullable, removes if null
	 *
	 * @see JustifyContent
	 */
	default void setJustifyContent(@Nullable JustifyContent justifyContent) {
		setJustifyContent(this, justifyContent);
	}

	/**
	 * @param justifyItems nullable, removes if null
	 *
	 * @see JustifyItems
	 */
	default void setJustifyItems(@Nullable JustifyItems justifyItems) {
		setJustifyItems(this, justifyItems);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt="TODO"><br>
	 *
	 * @param rowGap nullable, removes if null
	 */
	default void setRowGap(@Nullable String rowGap) {
		setRowGap(this, rowGap);
	}

	/**
	 * @param hasStyle     non-null
	 * @param alignContent nullable, removes if null
	 *
	 * @see AlignContent
	 */
	static void setAlignContent(HasStyle hasStyle, @Nullable AlignContent alignContent) {
		hasStyle.getStyle().set(CssGridProperty.ALIGN_CONTENT.key(), alignContent != null ? alignContent.value() : null);
	}

	/**
	 * @param hasStyle   non-null
	 * @param alignItems nullable, removes if null
	 *
	 * @see AlignItems
	 */
	static void setAlignItems(HasStyle hasStyle, @Nullable AlignItems alignItems) {
		hasStyle.getStyle().set(CssGridProperty.ALIGN_ITEMS.key(), alignItems != null ? alignItems.value() : null);
	}

	static void setDisplayGrid(HasStyle hasStyle) {
		hasStyle.getStyle().set("display", "grid");
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt="TODO"><br>
	 *
	 * @param hasStyle non-null
	 * @param gap      nullable, removes if null
	 */
	static void setGap(HasStyle hasStyle, @Nullable String gap) {
		hasStyle.getStyle().set(CssGridProperty.GAP.key(), gap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt="TODO"><br>
	 *
	 * @param hasStyle  non-null
	 * @param columnGap nullable, removes if null
	 * @param rowGap    nullable, removes if null
	 */
	static void setGap(HasStyle hasStyle, @Nullable String columnGap, @Nullable String rowGap) {
		hasStyle.getStyle().set(CssGridProperty.COLUMN_GAP.key(), columnGap);
		hasStyle.getStyle().set(CssGridProperty.ROW_GAP.key(), rowGap);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-template-areas.svg" alt="TODO"><br>
	 *
	 * @param hasStyle          non-null
	 * @param gridTemplateAreas nullable, removes if null
	 */
	static void setGridTemplateAreas(HasStyle hasStyle, @Nullable CssGridTemplateAreas gridTemplateAreas) {
		setGridTemplateAreas(hasStyle, gridTemplateAreas != null ? gridTemplateAreas.render() : null);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-areas</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-template-areas.svg" alt="TODO"><br>
	 *
	 * @param hasStyle          non-null
	 * @param gridTemplateAreas nullable, removes if null
	 */
	static void setGridTemplateAreas(HasStyle hasStyle, @Nullable String... gridTemplateAreas) {
		hasStyle.getStyle().set(CssGridProperty.GRID_TEMPLATE_AREAS.key(), gridTemplateAreas != null
				? Stream.of(gridTemplateAreas).map(areas -> "\"" + areas + "\"").collect(Collectors.joining(" "))
				: null);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param hasStyle            non-null
	 * @param gridTemplateColumns nullable, removes if null
	 */
	static void setGridTemplateColumns(HasStyle hasStyle, @Nullable String gridTemplateColumns) {
		hasStyle.getStyle().set(CssGridProperty.GRID_TEMPLATE_COLUMNS.key(), gridTemplateColumns);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-template-columns-rows</a><br>
	 *
	 * @param hasStyle         non-null
	 * @param gridTemplateRows nullable, removes if null
	 */
	static void setGridTemplateRows(HasStyle hasStyle, @Nullable String gridTemplateRows) {
		hasStyle.getStyle().set(CssGridProperty.GRID_TEMPLATE_ROWS.key(), gridTemplateRows);
	}

	/**
	 * @param hasStyle       non-null
	 * @param justifyContent nullable, removes if null
	 *
	 * @see JustifyContent
	 */
	static void setJustifyContent(HasStyle hasStyle, @Nullable JustifyContent justifyContent) {
		hasStyle.getStyle().set(
				CssGridProperty.JUSTIFY_CONTENT.key(),
				justifyContent != null ? justifyContent.value() : null);
	}

	/**
	 * @param hasStyle     non-null
	 * @param justifyItems nullable, removes if null
	 *
	 * @see JustifyItems
	 */
	static void setJustifyItems(HasStyle hasStyle, @Nullable JustifyItems justifyItems) {
		hasStyle.getStyle().set(CssGridProperty.JUSTIFY_ITEMS.key(), justifyItems != null ? justifyItems.value() : null);
	}

	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-grid-column-row-gap</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/dddgrid-gap.svg" alt="TODO"><br>
	 *
	 * @param hasStyle non-null
	 * @param rowGap   nullable, removes if null
	 */
	static void setRowGap(HasStyle hasStyle, @Nullable String rowGap) {
		hasStyle.getStyle().set(CssGridProperty.ROW_GAP.key(), rowGap);
	}
}
