package tk.labyrinth.pandora.misc4j.lib.vaadin.tool.setup;

import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.spring.SpringVaadinServletService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@ConditionalOnClass(SpringVaadinServletService.class)
@LazyComponent
public class RouteConfigurationTool implements VaadinServiceInitListener {

	@Nullable
	private List<Consumer<RouteConfiguration>> configurersToFeed = new ArrayList<>();

	private RouteConfiguration routeConfiguration = null;

	public void configure(Consumer<RouteConfiguration> configurer) {
		if (routeConfiguration != null) {
			configurer.accept(routeConfiguration);
		} else {
			if (configurersToFeed == null) {
				throw new UnreachableStateException();
			}
			configurersToFeed.add(configurer);
		}
	}

	@Override
	public void serviceInit(ServiceInitEvent event) {
		routeConfiguration = RouteConfiguration.forApplicationScope();
		{
			List<Consumer<RouteConfiguration>> configurers = configurersToFeed;
			if (configurers == null) {
				throw new UnreachableStateException();
			}
			configurers.forEach(this::configure);
			configurersToFeed = null;
		}
	}
}
