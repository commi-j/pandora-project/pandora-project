package tk.labyrinth.pandora.misc4j.java.lang.reflect;

import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class WildcardTypeUtils {

	private static WildcardType cast(Type type) {
		return (WildcardType) type;
	}

	public static Stream<Type> getUpperBoundStream(WildcardType wildcardType) {
		return Arrays.stream(wildcardType.getUpperBounds());
	}

	public static boolean hasLowerBounds(Type wildcardType) {
		return hasLowerBounds(cast(wildcardType));
	}

	public static boolean hasLowerBounds(WildcardType wildcardType) {
		return wildcardType.getLowerBounds().length > 0;
	}
}
