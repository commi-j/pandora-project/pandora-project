package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/pr_text_color.php">https://www.w3schools.com/cssref/pr_text_color.php</a><br>
 * <a href="https://www.w3schools.com/colors/colors_names.asp">https://www.w3schools.com/colors/colors_names.asp</a><br>
 *
 * @author Commitman
 * @version 1.0.3
 */
@Value(staticConstructor = "of")
public class Color implements CssProperty {

	/**
	 * @since 1.0.1
	 */
	public static final Color GRAY = of("gray");

	/**
	 * @since 1.0.1
	 */
	public static final Color GREY = of("grey");

	/**
	 * @since 1.0.0
	 */
	public static final Color INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final Color INITIAL = of("initial");

	/**
	 * @since 1.0.1
	 */
	public static final Color LIGHTGRAY = of("lightgray");

	/**
	 * @since 1.0.1
	 */
	public static final Color LIGHTGREY = of("lightgrey");

	/**
	 * @since 1.0.0
	 */
	public static final Color NOT_SPECIFIED = of(null);

	/**
	 * @since 1.0.0
	 */
	public static final Color RED = of("red");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "color";
	}
}
