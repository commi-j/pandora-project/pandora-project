package tk.labyrinth.pandora.misc4j.java.lang.reflect;

import lombok.NonNull;
import tk.labyrinth.pandora.misc4j.java.lang.JavaConstants;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;

import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author Commitman
 * @version 1.0.9
 */
public class ClassUtils {

	/**
	 * @param classBinaryName non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static Class<?> find(@NonNull String classBinaryName) {
		Class<?> result;
		{
			Class<?> keywordClass = JavaConstants.KEYWORDS.get(classBinaryName);
			if (keywordClass != null) {
				result = keywordClass;
			} else {
				try {
					result = Class.forName(classBinaryName);
				} catch (ClassNotFoundException ex) {
					result = null;
				}
			}
		}
		return result;
	}

	/**
	 * @param javaClass non-null
	 * @param name      fit
	 *
	 * @return nullable
	 *
	 * @since 1.0.7
	 */
	public static Field findDeclaredField(Class<?> javaClass, String name) {
		Field result;
		{
			try {
				result = javaClass.getDeclaredField(name);
			} catch (NoSuchFieldException ex) {
				result = null;
			}
		}
		return result;
	}

	/**
	 * @param classBinaryName non-null
	 * @param <T>             Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <T> Class<T> findInferred(String classBinaryName) {
		return (Class<T>) find(classBinaryName);
	}

	/**
	 * Binary names: https://docs.oracle.com/javase/specs/jls/se8/html/jls-13.html#jls-13.1
	 *
	 * @param classBinaryName non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Class<?> get(@NonNull String classBinaryName) {
		Class<?> result = find(classBinaryName);
		{
			if (result == null) {
				throw new IllegalArgumentException("Not found: classBinaryName = " + classBinaryName);
			}
		}
		return result;
	}

	public static Field getDeclaredField(Class<?> javaClass, String name) {
		Field result = findDeclaredField(javaClass, name);
		{
			if (result == null) {
				throw new IllegalArgumentException("Not found: javaClass = %s, name = %s".formatted(javaClass, name));
			}
		}
		return result;
	}

	/**
	 * @param javaClass non-null
	 * @param name      fit
	 *
	 * @return non-null
	 *
	 * @since 1.0.7
	 */
	public static Class<?> getDeclaredFieldClass(Class<?> javaClass, String name) {
		return getDeclaredField(javaClass, name).getType();
	}

	/**
	 * @param javaClass non-null
	 * @param name      fit
	 *
	 * @return non-null
	 *
	 * @since 1.0.7
	 */
	public static Type getDeclaredFieldType(Class<?> javaClass, String name) {
		return getDeclaredField(javaClass, name).getGenericType();
	}

	/**
	 * @param javaClass non-null
	 * @param fieldName proper Java field name
	 * @param target    null for static field, non-null otherwise
	 * @param <T>       Class/target type
	 *
	 * @return context-dependent
	 *
	 * @since 1.0.8
	 */
	@Nullable // TODO: Nullability is known by developer in most cases.
	public static <T> Object getDeclaredFieldValue(Class<T> javaClass, String fieldName, @Nullable T target) {
		Object result;
		{
			Field field = getDeclaredField(javaClass, fieldName);
			//
			try {
				field.setAccessible(true);
				//
				result = field.get(target);
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			}
		}
		return result;
	}

	/**
	 * @param javaClass non-null
	 * @param fieldName proper Java field name
	 * @param target    null for static field, non-null otherwise
	 * @param <T>       Class/target type
	 * @param <R>       Inferred field/result type
	 *
	 * @return context-dependent
	 *
	 * @since 1.0.8
	 */
	@Nullable // TODO: Nullability is known by developer in most cases.
	@SuppressWarnings("unchecked")
	public static <T, R> R getDeclaredFieldValueInferred(Class<T> javaClass, String fieldName, @Nullable T target) {
		return (R) getDeclaredFieldValue(javaClass, fieldName, target);
	}

	/**
	 * @param classBinaryName non-null
	 * @param <T>             Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> Class<T> getInferred(String classBinaryName) {
		Class<T> result = findInferred(classBinaryName);
		{
			if (result == null) {
				throw new IllegalArgumentException("Not found: classBinaryName = " + classBinaryName);
			}
		}
		return result;
	}

	/**
	 * @param cl non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static String getLongName(Class<?> cl) {
		return cl.getEnclosingClass() != null
				? getLongName(cl.getEnclosingClass()) + "." + cl.getSimpleName()
				: cl.getSimpleName();
	}

	/**
	 * @param cl non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static String getSignature(Class<?> cl) {
		String result;
		if (isArray(cl)) {
			result = getSignature(cl.getComponentType()) + "[]";
		} else {
			if (!isKeyword(cl)) {
				String packageName = cl.getPackageName();
				//
				result = !packageName.isEmpty()
						? packageName + ":" + getLongName(cl)
						: getLongName(cl);
			} else {
				// Keyword types have no packages or enclosing types.
				result = cl.getSimpleName();
			}
		}
		return result;
	}

	/**
	 * class -> false;<br>
	 * interface -> true;<br>
	 * abstract class -> true;<br>
	 * enum -> false;<br>
	 *
	 * @param javaClass non-null
	 *
	 * @return true if abstract, false otherwise
	 *
	 * @since 1.0.8
	 */
	public static boolean isAbstract(Class<?> javaClass) {
		return Modifier.isAbstract(javaClass.getModifiers());
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if array, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isArray(@Nullable Class<?> cl) {
		return cl != null && cl.isArray();
	}

	/**
	 * Generic class is the one that has type parameters, e.g. {@link List}.<br>
	 * Counterpart of generic classes are plain classes.<br>
	 *
	 * @param javaClass nullable
	 *
	 * @return true if generic, false otherwise
	 *
	 * @see #isPlain(Class)
	 * @since 1.0.3
	 */
	public static boolean isGeneric(@Nullable Class<?> javaClass) {
		return javaClass != null && javaClass.getTypeParameters().length > 0;
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if keyword, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isKeyword(@Nullable Class<?> cl) {
		return isPrimitive(cl) || isVoid(cl);
	}

	/**
	 * Generic class is the one that has no type parameters, e.g. {@link String}.<br>
	 * Counterpart of plain classes are generic classes.<br>
	 *
	 * @param javaClass nullable
	 *
	 * @return true if plain, false otherwise
	 *
	 * @see #isGeneric(Class)
	 * @since 1.0.2
	 */
	public static boolean isPlain(@Nullable Class<?> javaClass) {
		return javaClass != null && javaClass.getTypeParameters().length == 0;
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if primitive, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isPrimitive(@Nullable Class<?> cl) {
		return cl != null && cl.isPrimitive();
	}

	/**
	 * @param cl nullable
	 *
	 * @return true if void, false otherwise
	 *
	 * @since 1.0.1
	 */
	public static boolean isVoid(@Nullable Class<?> cl) {
		return cl == void.class;
	}

	/**
	 * Integer, Number -> Number;<br>
	 * Number, Integer -> Number;<br>
	 * Integer, String -> null;<br>
	 *
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.4
	 */
	@Nullable
	public static Class<?> pickSuperclass(Class<?> first, Class<?> second) {
		Class<?> result;
		{
			if (first.isAssignableFrom(second)) {
				result = first;
			} else if (second.isAssignableFrom(first)) {
				result = second;
			} else {
				result = null;
			}
		}
		return result;
	}

	/**
	 * @param javaClass non-null
	 *
	 * @return javaClass
	 *
	 * @since 1.0.2
	 */
	public static Type requirePlain(Class<?> javaClass) {
		if (!isPlain(javaClass)) {
			throw new IllegalArgumentException("Require plain: %s".formatted(javaClass));
		}
		return javaClass;
	}

	/**
	 * Unwraps proxies.
	 *
	 * @param object non-null
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static <T> Class<T> resolveClass(T object) {
		Class<?> result;
		{
			if (object instanceof Proxy) {
				result = object.getClass().getInterfaces()[0];
			} else {
				result = object.getClass();
			}
		}
		return ObjectUtils.infer(result);
	}
}
