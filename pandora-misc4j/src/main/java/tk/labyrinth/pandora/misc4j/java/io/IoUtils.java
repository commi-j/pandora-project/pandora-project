package tk.labyrinth.pandora.misc4j.java.io;

import tk.labyrinth.pandora.misc4j.java.lang.CheckedRunnable;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedConsumer;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedFunction;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedSupplier;

import javax.annotation.Nullable;
import java.io.Closeable;
import java.io.IOException;

/**
 * @author Commitman
 * @version 1.0.5
 */
public class IoUtils {

	/**
	 * @param resource non-null
	 * @param function non-null
	 * @param <R>      Resource
	 * @param <T>      Type
	 *
	 * @return function result
	 *
	 * @since 1.0.4
	 */
	@Nullable
	public static <R extends Closeable, T> T tryReturnWithResource(R resource, CheckedFunction<R, T, ? extends
			IOException> function) {
		return ResourceUtils.tryReturnWithResource(resource, function);
	}

	/**
	 * @param supplier non-null, providing non-null resource once
	 * @param function non-null
	 * @param <R>      Resource
	 * @param <T>      Type
	 *
	 * @return function result
	 *
	 * @since 1.0.4
	 */
	@Nullable
	public static <R extends Closeable, T> T tryReturnWithResourceSupplier(
			CheckedSupplier<R, ? extends IOException> supplier,
			CheckedFunction<R, T, ? extends IOException> function) {
		return ResourceUtils.tryReturnWithResourceSupplier(supplier, function);
	}

	/**
	 * @param resource non-null
	 * @param consumer non-null
	 * @param <R>      Resource
	 *
	 * @since 1.0.2
	 */
	public static <R extends Closeable> void tryWithResource(R resource, CheckedConsumer<R, ? extends
			IOException> consumer) {
		ResourceUtils.tryWithResource(resource, consumer);
	}

	/**
	 * @param supplier non-null, providing non-null resource once
	 * @param consumer non-null
	 * @param <R>      Resource
	 *
	 * @since 1.0.3
	 */
	public static <R extends Closeable> void tryWithResourceSupplier(
			CheckedSupplier<R, ? extends IOException> supplier,
			CheckedConsumer<R, ? extends IOException> consumer) {
		ResourceUtils.tryWithResourceSupplier(supplier, consumer);
	}

	/**
	 * @param runnable non-null
	 *
	 * @throws RuntimeException potentially wrapping {@link IOException}
	 * @since 1.0.1
	 */
	public static void unchecked(CheckedRunnable<? extends IOException> runnable) {
		try {
			runnable.run();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @param supplier non-null
	 * @param <T>      Type
	 *
	 * @return nullable, depends on <b>supplier</b>
	 *
	 * @throws RuntimeException potentially wrapping {@link IOException}
	 * @since 1.0.0
	 */
	@Nullable
	public static <T> T unchecked(CheckedSupplier<T, ? extends IOException> supplier) {
		try {
			return supplier.get();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
