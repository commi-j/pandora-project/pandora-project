package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/pr_dim_width.php">https://www.w3schools.com/cssref/pr_dim_width.php</a>
 *
 * @version 1.0.4
 */
@Value(staticConstructor = "of")
public class Width implements CssProperty {

	/**
	 * @since 1.0.1
	 */
	public static final Width AUTO = of("auto");

	/**
	 * @since 1.0.3
	 */
	public static final Width HUNDRED_PERCENT = of("100%");

	/**
	 * @since 1.0.3
	 */
	public static final Width HUNDRED_VIEWPORT_WIDTH = of("100vw");

	/**
	 * @since 1.0.1
	 */
	public static final Width INHERIT = of("inherit");

	/**
	 * @since 1.0.1
	 */
	public static final Width INITIAL = of("initial");

	/**
	 * @since 1.0.1
	 */
	public static final Width NOT_SPECIFIED = of(null);

	@Nullable
	String value;

	@Override
	public String getName() {
		return "width";
	}

	/**
	 * @param viewportHeight non-negative, percent
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Width ofViewportHeight(int viewportHeight) {
		return of("%svh".formatted(viewportHeight));
	}

	/**
	 * @param viewportWidth non-negative, percent
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Width ofViewportWidth(int viewportWidth) {
		return of("%svw".formatted(viewportWidth));
	}
}
