package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/pr_dim_height.php">https://www.w3schools.com/cssref/pr_dim_height.php</a>
 *
 * @version 1.0.1
 */
@Value(staticConstructor = "of")
public class Height implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final Height AUTO = of("auto");

	/**
	 * @since 1.0.0
	 */
	public static final Height HUNDRED_PERCENT = of("100%");

	/**
	 * @since 1.0.0
	 */
	public static final Height HUNDRED_VIEWPORT_HEIGHT = of("100vh");

	/**
	 * @since 1.0.0
	 */
	public static final Height INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final Height INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final Height NOT_SPECIFIED = of(null);

	@Nullable
	String value;

	@Override
	public String getName() {
		return "height";
	}

	/**
	 * @param viewportHeight non-negative, percent
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Height ofViewportHeight(int viewportHeight) {
		return of("%svh".formatted(viewportHeight));
	}

	/**
	 * @param viewportWidth non-negative, percent
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Height ofViewportWidth(int viewportWidth) {
		return of("%svw".formatted(viewportWidth));
	}
}
