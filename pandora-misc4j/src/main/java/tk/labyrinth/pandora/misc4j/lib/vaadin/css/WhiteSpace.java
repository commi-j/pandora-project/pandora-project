package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import com.vaadin.flow.component.HasText;
import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/pr_text_white-space.php">https://www.w3schools.com/cssref/pr_text_white-space.php</a>
 *
 * @author Commitman
 * @version 1.0.2
 * @see HasText.WhiteSpace
 */
@Value(staticConstructor = "of")
public class WhiteSpace implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace NORMAL = of("normal");

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace NOT_SPECIFIED = of(null);

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace NOWRAP = of("nowrap");

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace PRE = of("pre");

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace PRE_LINE = of("pre-line");

	/**
	 * @since 1.0.0
	 */
	public static final WhiteSpace PRE_WRAP = of("pre-wrap");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "white-space";
	}
}
