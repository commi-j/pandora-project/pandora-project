package tk.labyrinth.pandora.misc4j.java.util;

import java.nio.charset.Charset;
import java.util.Base64;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class Base64Utils {

	/**
	 * @param bytes non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static byte[] decodeBytes(byte[] bytes) {
		return Base64.getDecoder().decode(bytes);
	}

	/**
	 * @param bytes   non-null
	 * @param charset non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String decodeBytesToString(byte[] bytes, Charset charset) {
		return new String(decodeBytes(bytes), charset);
	}

	/**
	 * @param string  non-null
	 * @param charset non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String decodeString(String string, Charset charset) {
		return new String(decodeBytes(string.getBytes(charset)), charset);
	}

	/**
	 * @param string  non-null
	 * @param charset non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static byte[] decodeStringToBytes(String string, Charset charset) {
		return decodeBytes(string.getBytes(charset));
	}

	/**
	 * @param bytes non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static byte[] encodeBytes(byte[] bytes) {
		return Base64.getEncoder().encode(bytes);
	}

	/**
	 * @param bytes   non-null
	 * @param charset non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String encodeBytesToString(byte[] bytes, Charset charset) {
		return new String(encodeBytes(bytes), charset);
	}

	/**
	 * @param string  non-null
	 * @param charset non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String encodeString(String string, Charset charset) {
		return new String(encodeBytes(string.getBytes(charset)), charset);
	}

	/**
	 * @param string  non-null
	 * @param charset non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static byte[] encodeStringToBytes(String string, Charset charset) {
		return encodeBytes(string.getBytes(charset));
	}
}
