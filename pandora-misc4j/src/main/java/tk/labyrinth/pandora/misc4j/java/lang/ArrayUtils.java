package tk.labyrinth.pandora.misc4j.java.lang;

import java.util.List;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ArrayUtils {

	/**
	 * @param array non-null
	 * @param <T>   Type
	 *
	 * @return single element
	 *
	 * @since 1.0.0
	 */
	public static <T> T requireSingle(T[] array) {
		if (array.length != 1) {
			throw new IllegalArgumentException("Require single: %s".formatted(List.of(array)));
		}
		return array[0];
	}
}
