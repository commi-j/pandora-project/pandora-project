package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
 *
 * @see CssFlexboxAware
 * @see CssFlexboxLayout
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum CssFlexboxProperty {
	ALIGN_ITEMS("align-items"),
	ALIGN_SELF("align-self"),
	FLEX_DIRECTION("flex-direction"),
	FLEX_GROW("flex-grow"),
	FLEX_SHRINK("flex-shrink"),
	FLEX_WRAP("flex-wrap"),
	JUSTIFY_CONTENT("justify-content");

	private final String key;
}
