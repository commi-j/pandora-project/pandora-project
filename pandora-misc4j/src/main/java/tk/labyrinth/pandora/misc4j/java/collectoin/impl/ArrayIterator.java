package tk.labyrinth.pandora.misc4j.java.collectoin.impl;

import java.util.Iterator;
import java.util.Objects;

/**
 * @param <E> Element
 *
 * @author Commitman
 * @version 1.0.0
 */
public class ArrayIterator<E> implements Iterator<E> {

	private final E[] array;

	private int pointer;

	public ArrayIterator(E[] array) {
		this.array = Objects.requireNonNull(array, "array");
	}

	@Override
	public boolean hasNext() {
		return pointer < array.length;
	}

	@Override
	public E next() {
		return array[pointer++];
	}
}
