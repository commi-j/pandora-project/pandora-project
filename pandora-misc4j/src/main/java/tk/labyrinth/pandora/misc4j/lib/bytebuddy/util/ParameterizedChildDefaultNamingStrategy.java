package tk.labyrinth.pandora.misc4j.lib.bytebuddy.util;

import net.bytebuddy.NamingStrategy;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.utility.RandomString;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

import java.util.stream.Collectors;

/**
 * See net.bytebuddy.NamingStrategy$SuffixingRandom.
 */
public class ParameterizedChildDefaultNamingStrategy implements NamingStrategy {

	/**
	 * See ByteBuddy#BYTE_BUDDY_DEFAULT_PREFIX.
	 */
	private static final String BYTE_BUDDY_DEFAULT_PREFIX = "ByteBuddy";

	private final RandomString randomString = new RandomString();

	@Override
	public String rebase(TypeDescription typeDescription) {
		throw new NotImplementedException();
	}

	@Override
	public String redefine(TypeDescription typeDescription) {
		throw new NotImplementedException();
	}

	@Override
	public String subclass(TypeDescription.Generic superClass) {
		String typeName;
		{
			String originalTypeName = superClass.asErasure().getName();
			if (!originalTypeName.startsWith("java.")) {
				typeName = originalTypeName;
			} else {
				typeName = "net.bytebuddy.renamed." + originalTypeName;
			}
		}
		String parameterNames = superClass.getTypeArguments().stream()
				.map(TypeDescription.Generic::asErasure)
				.map(TypeDescription::getSimpleName)
				.collect(Collectors.joining("And"));
		return typeName + "Of" + parameterNames + "ByByteBuddy$" + randomString.nextString();
	}
}
