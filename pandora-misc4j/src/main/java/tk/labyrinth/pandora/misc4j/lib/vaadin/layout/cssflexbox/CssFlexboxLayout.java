package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.html.Div;

/**
 * Flexible layout based on CSS Flexbox.<br>
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
 *
 * @see CssFlexboxAware
 */
@Tag("css-flexbox-layout")
public class CssFlexboxLayout extends Component implements CssFlexboxAware, HasComponents, HasSize {

	{
		setDisplayFlex();
	}

	public CssFlexboxLayout() {
		super();
	}

	public CssFlexboxLayout(FlexDirection flexDirection) {
		this();
		setFlexDirection(flexDirection);
	}

	/**
	 * Adds a Div with {@code flex-grow: 1;} to separate Components into Start and End sections.
	 *
	 * @return added Component
	 */
	public Div addStretch() {
		Div div = new Div();
		CssFlexItem.setFlexGrow(div, 1);
		add(div);
		return div;
	}
}
