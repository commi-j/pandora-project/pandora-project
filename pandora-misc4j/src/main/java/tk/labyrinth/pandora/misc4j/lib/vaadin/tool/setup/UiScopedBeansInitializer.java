package tk.labyrinth.pandora.misc4j.lib.vaadin.tool.setup;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.AfterNavigationListener;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.BeforeLeaveListener;
import com.vaadin.flow.server.UIInitEvent;
import com.vaadin.flow.server.UIInitListener;
import com.vaadin.flow.spring.scopes.VaadinUIScope;
import io.vavr.Tuple3;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.Eager;

import java.util.Objects;
import java.util.Optional;

/**
 * @author Commitman
 * @version 1.0.1
 */
@LazyComponent
@Slf4j
public class UiScopedBeansInitializer implements BeanFactoryAware, UIInitListener {

	private static final List<Class<?>> supportedUiListenerClasses = List.of(
			AfterNavigationListener.class,
			BeforeEnterListener.class,
			BeforeLeaveListener.class);

	private List<ObjectProvider<?>> listenerProviders;

	private void initialize(ConfigurableListableBeanFactory beanFactory) {
		val uiScopedBeanDefinitions = List.of(beanFactory.getBeanDefinitionNames())
				.map(beanFactory::getBeanDefinition)
				.filter(UiScopedBeansInitializer::isEligible)
				.map(beanDefinition -> {
					Class<?> beanClass = ClassUtils.get(beanDefinition.getBeanClassName());
					//
					return new Tuple3<>(beanDefinition, beanClass, beanFactory.getBeanProvider(beanClass));
				});
		//
		listenerProviders = uiScopedBeanDefinitions
				.filter(tuple -> isEagerInitCandidate(Pair.of(tuple._1(), tuple._2())))
				.filter(tuple -> {
					boolean result;
					{
						if (Objects.equals(tuple._1().getScope(), VaadinUIScope.VAADIN_UI_SCOPE_NAME) &&
								tuple._1().isLazyInit()) {
							// We do not initialize lazy beans, so such combination looks like misconfiguration.
							logger.warn(
									"Detected UI Scoped Bean which is eligible listener but also Lazy: className = {}",
									tuple._2().getName());
							//
							result = false;
						} else {
							result = true;
						}
					}
					return result;
				})
				.map(Tuple3::_3);
	}

	@Override
	public void setBeanFactory(@NonNull BeanFactory beanFactory) throws BeansException {
		initialize((ConfigurableListableBeanFactory) beanFactory);
	}

	@Override
	public void uiInit(UIInitEvent event) {
		UI ui = event.getUI();
		//
		logger.debug("Initializing UI with id = {}", ui.getUIId());
		//
		ui.accessSynchronously(() -> listenerProviders
				.map(ObjectProvider::getObject)
				.forEach(listener -> {
					if (listener instanceof AfterNavigationListener afterNavigationListener) {
						logger.debug("Registering AfterNavigationListener with class.name = {} for UI with id = {}",
								listener.getClass().getName(), ui.getUIId());
						//
						ui.addAfterNavigationListener(afterNavigationListener);
					}
					if (listener instanceof BeforeEnterListener beforeEnterListener) {
						logger.debug("Registering BeforeEnterListener with class.name = {} for UI with id = {}",
								listener.getClass().getName(), ui.getUIId());
						//
						ui.addBeforeEnterListener(beforeEnterListener);
					}
					if (listener instanceof BeforeLeaveListener beforeLeaveListener) {
						logger.debug("Registering BeforeLeaveListener with class.name = {} for UI with id = {}",
								listener.getClass().getName(), ui.getUIId());
						//
						ui.addBeforeLeaveListener(beforeLeaveListener);
					}
				}));
	}

	private static boolean isEligible(BeanDefinition beanDefinition) {
		return Objects.equals(beanDefinition.getScope(), VaadinUIScope.VAADIN_UI_SCOPE_NAME) ||
				(beanDefinition.getBeanClassName() != null &&
						//
						// FIXME: Think of smarter way to check annotations.
						//  also may dive into this bug:
						//    Not found: classBinaryName = tk.labyrinth.satool.application.SatoolApplication$$EnhancerBySpringCGLIB$$d335690
						//
						Optional.ofNullable(ClassUtils.find(beanDefinition.getBeanClassName()))
								.map(cl -> cl.isAnnotationPresent(RegisterOnUiInit.class))
								.orElse(false));
	}

	public static boolean isEagerInitCandidate(Pair<BeanDefinition, Class<?>> beanDefinitionAndClass) {
		return supportedUiListenerClasses.exists(supportedUiListenerClass ->
				supportedUiListenerClass.isAssignableFrom(beanDefinitionAndClass.getRight())) ||
				MergedAnnotations.from(beanDefinitionAndClass.getRight()).isPresent(Eager.class);
	}
}
