package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Aligns grid items along the block (column) axis (as opposed to justify-items which aligns along the inline (row)
 * axis). This value applies to all grid items inside the container.<br>
 * <br>
 * Values:<br>
 * - stretch – fills the whole height of the cell (this is the default)<br>
 * - start – aligns items to be flush with the start edge of their cell<br>
 * - end – aligns items to be flush with the end edge of their cell<br>
 * - center – aligns items in the center of their cell<br>
 * - baseline – align items along text baseline. There are modifiers to baseline — first baseline and last baseline
 * which will use the baseline from the first or last line in the case of multi-line text.<br>
 * <br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-items</a><br>
 *
 * @author Commitman
 * @version 1.0.1
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum AlignItems {
	BASELINE("baseline"),
	CENTER("center"),
	END("end"),
	START("start"),
	STRETCH("stretch");

	private final String value;
}
