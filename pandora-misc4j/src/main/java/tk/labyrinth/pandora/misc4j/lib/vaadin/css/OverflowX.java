package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/csS/css_overflow.asp">https://www.w3schools.com/csS/css_overflow.asp</a>
 *
 * @author Commitman
 * @version 1.0.0
 * @see Overflow
 * @see OverflowY
 */
@Value(staticConstructor = "of")
public class OverflowX implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final OverflowX AUTO = of("auto");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowX HIDDEN = of("hidden");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowX INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowX INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowX NOT_SPECIFIED = of(null);

	/**
	 * @since 1.0.0
	 */
	public static final OverflowX SCROLL = of("scroll");

	/**
	 * @since 1.0.0
	 */
	public static final OverflowX VISIBLE = of("visible");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "overflow-x";
	}
}
