package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/index.php">https://www.w3schools.com/cssref/index.php</a>
 *
 * @author Commitman
 * @version 1.0.2
 */
public interface CssProperty {

	String getName();

	@Nullable
	String getValue();

	static CssProperty custom(String name, @Nullable String value) {
		return CustomCssProperty.of(name, value);
	}
}
