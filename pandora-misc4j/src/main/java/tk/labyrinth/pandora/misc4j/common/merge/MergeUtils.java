package tk.labyrinth.pandora.misc4j.common.merge;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.function.BinaryOperator;
import java.util.function.Function;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class MergeUtils {

	/**
	 * Merges property of two objects resulting in a new value. This method does not actually produce a new value
	 * but instead return one already set, checking there are no collisions.<br>
	 * Formally:<br>
	 * - If both properties are non-null and equal, first property is returned;<br>
	 * - If both properties are non-null and not equal, exception is thrown;<br>
	 * - If only one property is non-null, it is returned;<br>
	 * - If both properties are null, null is returned;<br>
	 *
	 * @param <T>              Type of merged elements
	 * @param <P>              Property to merge
	 * @param first            non-null
	 * @param second           non-null
	 * @param propertyFunction non-null, returns nullable
	 * @param propertyName     non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static <T, P> P mergeEqual(T first, T second, Function<T, P> propertyFunction, String propertyName) {
		P result;
		{
			P firstProperty = propertyFunction.apply(first);
			P secondProperty = propertyFunction.apply(second);
			if (firstProperty != null) {
				if (secondProperty != null) {
					if (!Objects.equals(firstProperty, secondProperty)) {
						throw new IllegalArgumentException("Require equal " + propertyName + ": " +
								"first = " + first + ", " +
								"second = " + second);
					}
					result = firstProperty;
				} else {
					result = firstProperty;
				}
			} else {
				result = secondProperty;
			}
		}
		return result;
	}

	/**
	 * @param <T>              Type of merged elements
	 * @param <P>              Property to merge
	 * @param first            non-null
	 * @param second           non-null
	 * @param propertyFunction non-null, returns nullable
	 * @param propertyOperator non-null, receives non-null, returns nullable
	 *
	 * @return nullable
	 *
	 * @since 1.0.1
	 */
	@Nullable
	public static <T, P> P mergeWithOperator(
			T first,
			T second,
			Function<T, P> propertyFunction,
			BinaryOperator<P> propertyOperator) {
		P result;
		{
			P firstProperty = propertyFunction.apply(first);
			P secondProperty = propertyFunction.apply(second);
			if (firstProperty != null) {
				if (secondProperty != null) {
					result = propertyOperator.apply(firstProperty, secondProperty);
				} else {
					result = firstProperty;
				}
			} else {
				result = secondProperty;
			}
		}
		return result;
	}
}
