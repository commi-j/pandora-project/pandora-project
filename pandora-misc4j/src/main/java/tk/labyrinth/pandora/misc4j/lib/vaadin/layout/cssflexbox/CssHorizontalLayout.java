package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.Component;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.LayoutHelper;

/**
 * Initialized with {@link #setFlexDirection(FlexDirection) setFlexDirection}({@link FlexDirection#ROW}).<br>
 * Note: this property may be altered later, even to "column" values.<br>
 */
public class CssHorizontalLayout extends CssFlexboxLayout implements LayoutHelper {

	public CssHorizontalLayout() {
		super(FlexDirection.ROW);
	}

	public CssHorizontalLayout(Component... components) {
		this();
		add(components);
	}
}
