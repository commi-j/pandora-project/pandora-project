package tk.labyrinth.pandora.misc4j.lib.vaadin.util;

import com.vaadin.flow.component.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

/**
 * @author Commitman
 * @version 1.0.4
 */
public class ReactiveVaadin {

	private static final Logger logger = LoggerFactory.getLogger(ReactiveVaadin.class);

	public static <T> Flux<T> uiFlux(UI ui, Flux<T> flux) {
		Sinks.Many<T> sink = Sinks.unsafe().many().multicast().onBackpressureBuffer(1);
		//
		Disposable disposable = flux.subscribe(
				next -> {
					UI currentUi = UI.getCurrent();
					//
					if (currentUi != null) {
						if (currentUi == ui) {
							// Accessing from same UI Thread -> emitting directly
							//
							sink.tryEmitNext(next);
						} else {
							// Accessing from different UI Thread -> erroneous behaviour
							//
							throw new IllegalArgumentException(
									"Handling next signal from different UI: actual = %s, expected = %s, next = %s"
											.formatted(currentUi.getUIId(), ui.getUIId(), next));
						}
					} else {
						// Accessing from non-UI Thread -> emitting through ui.access
						//
						ui.access(() -> sink.tryEmitNext(next));
					}
				},
				error -> {
					UI currentUi = UI.getCurrent();
					//
					if (currentUi != null) {
						if (currentUi == ui) {
							// Accessing from same UI Thread -> emitting directly
							//
							sink.tryEmitError(error);
						} else {
							// Accessing from different UI Thread -> erroneous behaviour
							//
							logger.error("Failed to deliver the following error signal", error);
							//
							throw new IllegalArgumentException(
									"Handling error signal from different UI: actual = %s, expected = %s, error = %s"
											.formatted(currentUi.getUIId(), ui.getUIId(), error));
						}
					} else {
						// Accessing from non-UI Thread -> emitting through ui.access
						//
						ui.access(() -> sink.tryEmitError(error));
					}
				},
				() -> {
					UI currentUi = UI.getCurrent();
					//
					if (currentUi != null) {
						if (currentUi == ui) {
							// Accessing from same UI Thread -> emitting directly
							//
							sink.tryEmitComplete();
						} else {
							// Accessing from different UI Thread -> erroneous behaviour
							//
							throw new IllegalArgumentException(
									"Handling complete signal from different UI: actual = %s, expected = %s"
											.formatted(currentUi.getUIId(), ui.getUIId()));
						}
					} else {
						// Accessing from non-UI Thread -> emitting through ui.access
						//
						ui.access(sink::tryEmitComplete);
					}
				});
		//
		ui.addDetachListener(event -> disposable.dispose());
		//
		return sink.asFlux();
	}

	public static <T> Mono<T> uiMono(UI ui, Mono<T> mono) {
		Sinks.One<T> sink = Sinks.unsafe().one();
		//
		Disposable disposable = mono.subscribe(
				value -> {
					UI currentUi = UI.getCurrent();
					//
					if (currentUi != null) {
						if (currentUi == ui) {
							// Accessing from same UI Thread -> emitting directly
							//
							sink.tryEmitValue(value);
						} else {
							// Accessing from different UI Thread -> erroneous behaviour
							//
							throw new IllegalArgumentException(
									"Handling value signal from different UI: actual = %s, expected = %s, value = %s"
											.formatted(currentUi.getUIId(), ui.getUIId(), value));
						}
					} else {
						// Accessing from non-UI Thread -> emitting through ui.access
						//
						ui.access(() -> sink.tryEmitValue(value));
					}
				},
				error -> {
					UI currentUi = UI.getCurrent();
					//
					if (currentUi != null) {
						if (currentUi == ui) {
							// Accessing from same UI Thread -> emitting directly
							//
							sink.tryEmitError(error);
						} else {
							// Accessing from different UI Thread -> erroneous behaviour
							//
							logger.error("Failed to deliver the following error signal", error);
							//
							throw new IllegalArgumentException(
									"Handling error signal from different UI: actual = %s, expected = %s, error = %s"
											.formatted(currentUi.getUIId(), ui.getUIId(), error));
						}
					} else {
						// Accessing from non-UI Thread -> emitting through ui.access
						//
						ui.access(() -> sink.tryEmitError(error));
					}
				},
				() -> {
					UI currentUi = UI.getCurrent();
					//
					if (currentUi != null) {
						if (currentUi == ui) {
							// Accessing from same UI Thread -> emitting directly
							//
							sink.tryEmitEmpty();
						} else {
							// Accessing from different UI Thread -> erroneous behaviour
							//
							throw new IllegalArgumentException(
									"Handling empty signal from different UI: actual = %s, expected = %s"
											.formatted(currentUi.getUIId(), ui.getUIId()));
						}
					} else {
						// Accessing from non-UI Thread -> emitting through ui.access
						//
						ui.access(sink::tryEmitEmpty);
					}
				});
		//
		ui.addDetachListener(event -> disposable.dispose());
		//
		return sink.asMono();
	}
}
