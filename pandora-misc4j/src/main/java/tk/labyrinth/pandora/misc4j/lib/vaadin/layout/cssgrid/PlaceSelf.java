package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Documentation:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self</a><br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self</a><br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self</a><br>
 * Start:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-start.svg" alt="TODO"><br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-start.svg" alt="TODO"><br>
 * End:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-end.svg" alt="TODO"><br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-end.svg" alt="TODO"><br>
 * Center:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-center.svg" alt="TODO"><br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-center.svg" alt="TODO"><br>
 * Stretch:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-stretch.svg" alt="TODO"><br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-stretch.svg" alt="TODO"><br>
 *
 * @author Commitman
 * @version 1.0.2
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum PlaceSelf {
	/**
	 * Documentation:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-center.svg" alt="TODO"><br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-center.svg" alt="TODO"><br>
	 */
	CENTER("center"),
	/**
	 * Documentation:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-end.svg" alt="TODO"><br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-end.svg" alt="TODO"><br>
	 */
	END("end"),
	/**
	 * Documentation:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-start.svg" alt="TODO"><br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-start.svg" alt="TODO"><br>
	 */
	START("start"),
	/**
	 * Documentation:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-align-self</a><br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-place-self</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-self-stretch.svg" alt="TODO"><br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/align-self-stretch.svg" alt="TODO"><br>
	 */
	STRETCH("stretch");

	private final String value;
}
