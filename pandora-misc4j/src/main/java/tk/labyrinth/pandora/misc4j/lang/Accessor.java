package tk.labyrinth.pandora.misc4j.lang;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.1
 */
public interface Accessor<T> {

	/**
	 * @return current value, any
	 *
	 * @since 1.0.0
	 */
	T get();

	/**
	 * @return true if value !=null, false otherwise
	 *
	 * @since 1.0.1
	 */
	boolean has();

	/**
	 * @param value any
	 *
	 * @return previous value, any
	 *
	 * @since 1.0.0
	 */
	T set(T value);
}
