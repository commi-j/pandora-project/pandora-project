package tk.labyrinth.pandora.misc4j.lib.vaadin.meta;

import org.springframework.context.annotation.Lazy;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScope;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that bean with scope other than singleton or @{@link PrototypeScope} to be initialized
 * when corresponding scope is created.<br>
 * This is initially designed for Vaadin UI and VaadinSession scopes.<br>
 *
 * @see Lazy
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
public @interface Eager {
	// empty
}
