package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.Component;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.LayoutHelper;

/**
 * Initialized with {@link #setFlexDirection(FlexDirection) setFlexDirection}({@link FlexDirection#COLUMN}).<br>
 * Note: this property may be altered later, even to "row" values.<br>
 */
public class CssVerticalLayout extends CssFlexboxLayout implements LayoutHelper {

	public CssVerticalLayout() {
		super(FlexDirection.COLUMN);
	}

	public CssVerticalLayout(Component... components) {
		this();
		add(components);
	}
}
