package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/Css/css_text_decoration.asp">https://www.w3schools.com/Css/css_text_decoration.asp</a>
 *
 * @author Commitman
 * @version 1.0.2
 * @see TextDecoration
 */
@Value(staticConstructor = "of")
public class TextDecorationLine implements CssProperty {

	public static final TextDecorationLine LINE_THROUGH = of("line-through");

	public static final TextDecorationLine OVERLINE = of("overline");

	public static final TextDecorationLine UNDERLINE = of("underline");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "text-decoration-line";
	}
}
