package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
 * Start:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-start.svg" alt="TODO"><br>
 * End:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-end.svg" alt="TODO"><br>
 * Center:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-center.svg" alt="TODO"><br>
 * Stretch:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-stretch.svg" alt="TODO"><br>
 * Space-around:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-space-around.svg" alt="TODO"><br>
 * Space-between:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-space-between.svg" alt="TODO"><br>
 * Space-evenly:<br>
 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-space-evenly.svg" alt="TODO"><br>
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum JustifyContent {
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-center.svg" alt="TODO"><br>
	 */
	CENTER("center"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-end.svg" alt="TODO"><br>
	 */
	END("end"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-space-around.svg" alt="TODO"><br>
	 */
	SPACE_AROUND("space-around"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-space-between.svg" alt="TODO"><br>
	 */
	SPACE_BETWEEN("space-between"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-space-evenly.svg" alt="TODO"><br>
	 */
	SPACE_EVENLY("space-evenly"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-start.svg" alt="TODO"><br>
	 */
	START("start"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-content</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-content-stretch.svg" alt="TODO"><br>
	 */
	STRETCH("stretch");

	private final String value;
}
