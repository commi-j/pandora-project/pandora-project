package tk.labyrinth.pandora.misc4j.lib.vaadin.html.table;

import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Tag("td")
public class HtmlTableData extends HtmlContainer implements ClickNotifier<HtmlTableData> {

	public HtmlTableData() {
		// no-op
	}

	public HtmlTableData(String text) {
		add(text);
	}
}
