package tk.labyrinth.pandora.misc4j.lib.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.jackson.datatype.VavrModule;

public class VavrObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			// https://github.com/vavr-io/vavr-jackson
			objectMapper.registerModule(new VavrModule());
		}
		return objectMapper;
	}
}
