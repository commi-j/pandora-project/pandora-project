package tk.labyrinth.pandora.misc4j.common.configurer;

import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;

import java.util.List;

/**
 * @param <O> Object
 *
 * @author Commitman
 * @version 1.0.0
 */
public interface ObjectConfigurer<O> {

	O configure(O object);

	static <T> T configure(T object, List<? extends ObjectConfigurer<T>> configurers) {
		return configurers.stream().reduce(
				object,
				(objectToConfigure, configurer) -> configurer.configure(objectToConfigure),
				FunctionUtils::throwUnreachableStateException);
	}
}
