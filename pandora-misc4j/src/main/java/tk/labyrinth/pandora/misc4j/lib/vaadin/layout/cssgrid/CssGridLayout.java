package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.Tag;

/**
 * Flexible layout based on CSS Grid.<br>
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/">https://css-tricks.com/snippets/css/complete-guide-grid/</a><br>
 *
 * @see CssGridAware
 */
@Tag("css-grid-layout")
public class CssGridLayout extends Component implements CssGridAware, HasSize {

	{
		setDisplayGrid();
	}
}
