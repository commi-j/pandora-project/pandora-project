package tk.labyrinth.pandora.misc4j.java.collectoin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class CollectorUtils {

	/**
	 * Returns an element from a {@link Stream}, failing in following cases:<br>
	 * - Stream size is greater than 1;<br>
	 * - Stream is empty;<br>
	 * - Stream's element is null.<br>
	 *
	 * @param <E> Element
	 *
	 * @return non-null
	 *
	 * @see #findOnly(boolean)
	 * @see #findOnly(boolean, boolean)
	 */
	public static <E> Collector<E, ?, E> findOnly() {
		return findOnly(false, false);
	}

	/**
	 * Returns an element from a {@link Stream} or null if it is empty, failing in following cases:<br>
	 * - Stream size is greater than 1;<br>
	 * - Stream is empty and {@code allowEmptyStream} is false;<br>
	 * - Stream's element is null.<br>
	 *
	 * @param allowEmptyStream whether to fail or not if stream is empty
	 * @param <E>              Element
	 *
	 * @return nullable when {@code allowEmptyStream}, non-null otherwise
	 *
	 * @see #findOnly()
	 * @see #findOnly(boolean, boolean)
	 */
	public static <E> Collector<E, ?, E> findOnly(boolean allowEmptyStream) {
		return findOnly(allowEmptyStream, false);
	}

	/**
	 * Returns an element from a {@link Stream} or null if it is empty, failing in following cases:<br>
	 * - Stream size is greater than 1;<br>
	 * - Stream is empty and {@code allowEmptyStream} is false;<br>
	 * - Stream's element is null and {@code allowNullElement} is false.<br>
	 *
	 * @param allowEmptyStream whether to fail or not if stream is empty
	 * @param allowNullElement whether to fail or not if element is null
	 * @param <E>              Element
	 *
	 * @return nullable when {@code allowEmptyStream} or {@code allowNullElement}, non-null otherwise
	 *
	 * @see #findOnly()
	 * @see #findOnly(boolean)
	 */
	public static <E> Collector<E, ?, E> findOnly(boolean allowEmptyStream, boolean allowNullElement) {
		return Collectors.collectingAndThen(Collectors.toList(), list -> {
			if (list.size() > 1) {
				throw new IllegalArgumentException("Require only element: " + list);
			}
			E result;
			if (!list.isEmpty()) {
				result = list.get(0);
				if (result == null && !allowNullElement) {
					throw new IllegalArgumentException("Require non-null element: " + list);
				}
			} else {
				if (!allowEmptyStream) {
					throw new IllegalArgumentException("Require non-empty stream");
				} else {
					result = null;
				}
			}
			return result;
		});
	}

	/**
	 * Unlike {@link Collectors#toList()} this Collector guarantees to use {@link ArrayList}.<br>
	 *
	 * @param <E> Element
	 *
	 * @return non-null
	 */
	public static <E> Collector<E, ?, List<E>> toArrayList() {
		return Collectors.toCollection(ArrayList::new);
	}
}
