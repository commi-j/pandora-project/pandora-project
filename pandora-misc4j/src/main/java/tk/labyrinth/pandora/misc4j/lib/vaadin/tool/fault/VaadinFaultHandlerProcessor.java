package tk.labyrinth.pandora.misc4j.lib.vaadin.tool.fault;

import com.vaadin.flow.server.ErrorEvent;
import com.vaadin.flow.server.ErrorHandler;
import com.vaadin.flow.server.SessionInitEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.misc4j.lib.vaadin.tool.setup.AutoregisteringSessionInitListener;

import java.util.List;

@Component
@ConditionalOnBean(VaadinFaultHandler.class)
@RequiredArgsConstructor
@Slf4j
public class VaadinFaultHandlerProcessor implements AutoregisteringSessionInitListener, ErrorHandler {

	private final ObjectProvider<List<VaadinFaultHandler>> faultHandlersProvider;

	@Override
	public void error(ErrorEvent event) {
		Throwable fault = event.getThrowable();
		faultHandlersProvider.getObject().forEach(faultHandler -> {
			try {
				faultHandler.handle(fault);
			} catch (RuntimeException ex) {
				logger.error(null, ex);
			}
		});
	}

	@Override
	public void sessionInit(SessionInitEvent event) {
		event.getSession().setErrorHandler(this);
	}
}
