package tk.labyrinth.pandora.misc4j.lib.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import tk.labyrinth.pandora.misc4j.common.configurer.ObjectConfigurer;

public interface ObjectMapperConfigurer extends ObjectConfigurer<ObjectMapper> {

	@Override
	ObjectMapper configure(ObjectMapper objectMapper);
}
