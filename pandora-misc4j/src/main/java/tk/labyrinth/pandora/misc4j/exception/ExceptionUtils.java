package tk.labyrinth.pandora.misc4j.exception;

import tk.labyrinth.misc4j2.collectoin.ListUtils;
import tk.labyrinth.pandora.misc4j.exception.wrapping.ExceptionWrapper;
import tk.labyrinth.pandora.misc4j.exception.wrapping.WrappingError;
import tk.labyrinth.pandora.misc4j.exception.wrapping.WrappingRuntimeException;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;

/**
 * @author Commitman
 * @version 1.0.4
 */
public class ExceptionUtils {

	/**
	 * @param value nullable
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static String render(@Nullable Object value) {
		return value != null
				? "type = " + value.getClass().getName() + " & value = " + value
				: "null";
	}

	/**
	 * @param list nullable
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static String renderList(@Nullable List<?> list) {
		return list != null
				? ListUtils.mapNonnull(list, ExceptionUtils::render).toString()
				: "null";
	}

	/**
	 * Wraps <b>throwable</b> into an unchecked exception and throws it.<br>
	 * - If <b>throwable</b> is an instance of {@link Exception}, it is wrapped with {@link WrappingRuntimeException};<br>
	 * - Otherwise ({@link Error} or unorthodox implementation of {@link Throwable}) it is wrapped with {@link WrappingError};<br>
	 *
	 * @param throwable non-null
	 *
	 * @since 1.0.2
	 */
	public static void throwUnchecked(Throwable throwable) {
		Objects.requireNonNull(throwable, "throwable");
		//
		if (throwable instanceof Exception) {
			throw new WrappingRuntimeException((Exception) throwable);
		} else {
			throw new WrappingError(throwable);
		}
	}

	/**
	 * Unwraps provided {@link Throwable} if it is instance of {@link ExceptionWrapper}, returns itself otherwise.
	 *
	 * @param throwable non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.4
	 */
	public static Throwable unwrap(Throwable throwable) {
		Throwable result;
		{
			if (throwable instanceof ExceptionWrapper exceptionWrapper) {
				result = exceptionWrapper.unwrap();
			} else {
				result = throwable;
			}
		}
		return result;
	}
}
