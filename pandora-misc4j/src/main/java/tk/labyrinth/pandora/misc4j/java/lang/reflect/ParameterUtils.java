package tk.labyrinth.pandora.misc4j.java.lang.reflect;

import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.5
 */
public class ParameterUtils {

	private static Stream<TypeVariable<? extends Class<?>>> doGetAliases(
			TypeVariable<? extends Class<?>> typeVariable) {
		Stream<TypeVariable<? extends Class<?>>> result;
		{
			Class<?> originalClass = typeVariable.getGenericDeclaration();
			//
			result = Stream.concat(
					Stream.of(typeVariable),
					Stream
							.concat(
									Stream.of(originalClass.getGenericSuperclass()),
									Stream.of(originalClass.getGenericInterfaces()))
							.filter(TypeUtils::isParameterizedType)
							.flatMap(superType -> IntStream
									.range(0, TypeUtils.getClass(superType).getTypeParameters().length)
									.filter(index -> TypeUtils.asParameterizedType(superType)
											.getActualTypeArguments()[index] == typeVariable)
									.mapToObj(index -> TypeUtils.getClass(superType).getTypeParameters()[index]))
							.flatMap(ParameterUtils::doGetAliases));
		}
		return result;
	}

	// TODO: Order properly.
	private static Map<TypeVariable<?>, Type> getTypeParameters(Type actualType, Class<?> declaringType) {
		if (!declaringType.isAssignableFrom(TypeUtils.getClass(actualType))) {
			throw new IllegalArgumentException(
					"Require actualType assignableTo declaringType: actualType = %s, declaringType = %s"
							.formatted(actualType, declaringType));
		}
		//
		Map<TypeVariable<?>, Type> result;
		{
			Map<TypeVariable<?>, Type> typeArguments = org.apache.commons.lang3.reflect.TypeUtils.getTypeArguments(
					actualType,
					declaringType);
			if (typeArguments != null) {
				if (!typeArguments.isEmpty()) {
					result = typeArguments;
				} else {
					if (actualType != declaringType) {
						// FIXME: Just wondering what could it be.
						//
						throw new NotImplementedException();
					}
					result = Stream.of(declaringType.getTypeParameters())
							.collect(Collectors.toMap(Function.identity(), Function.identity()));
				}
			} else {
				throw new IllegalArgumentException("Require parameterized: actualType = %s, declaringType = %s"
						.formatted(actualType, declaringType));
			}
		}
		return result;
	}

	/**
	 * @param actualType   non-null
	 * @param typeVariable non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static Type getActualParameter(Type actualType, TypeVariable<? extends Class<?>> typeVariable) {
		Map<TypeVariable<?>, Type> typeParameters = getTypeParameters(actualType, typeVariable.getGenericDeclaration());
		return typeParameters.get(typeVariable);
	}

	public static Type getActualParameter(Type actualType, Class<?> declaringType, int index) {
		return getActualParameters(actualType, declaringType).skip(index).findFirst().orElseThrow();
	}

	public static Stream<Type> getActualParameters(Type actualType, Class<?> declaringType) {
		Map<TypeVariable<?>, Type> typeArguments = getTypeParameters(actualType, declaringType);
		return Stream.of(declaringType.getTypeParameters()).map(typeArguments::get);
	}

	public static List<TypeVariable<? extends Class<?>>> getAliases(TypeVariable<? extends Class<?>> typeVariable) {
		// TODO: Move distinct to be parameter of doGetAliases.
		return doGetAliases(typeVariable).distinct().collect(Collectors.toList());
	}

	public static Type getFirstActualParameter(Type actualType, Class<?> declaringType) {
		return getActualParameter(actualType, declaringType, 0);
	}

	/**
	 * @param actualType    non-null
	 * @param declaringType non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static Class<?> getFirstActualParameterAsClass(Type actualType, Class<?> declaringType) {
		return TypeUtils.getClass(getFirstActualParameter(actualType, declaringType));
	}

	public static Type getSecondActualParameter(Type actualType, Class<?> declaringType) {
		return getActualParameter(actualType, declaringType, 1);
	}

	/**
	 * @param actualType    non-null
	 * @param declaringType non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static Class<?> getSecondActualParameterAsClass(Type actualType, Class<?> declaringType) {
		return TypeUtils.getClass(getSecondActualParameter(actualType, declaringType));
	}

	/**
	 * @param initialTypeVariable non-null
	 * @param actualClass         non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.4
	 */
	public static TypeVariable<? extends Class<?>> resolveTypeParameter(
			TypeVariable<? extends Class<?>> initialTypeVariable,
			Class<?> actualClass) {
		TypeVariable<? extends Class<?>> result;
		{
			Class<?> initialClass = initialTypeVariable.getGenericDeclaration();
			if (initialClass.isAssignableFrom(actualClass)) {
				result = initialTypeVariable;
			} else if (actualClass.isAssignableFrom(initialClass)) {
				result = getAliases(initialTypeVariable).stream()
						.filter(alias -> alias.getGenericDeclaration().isAssignableFrom(actualClass))
						.findFirst()
						.orElseThrow();
			} else {
				throw new IllegalArgumentException("Not related");
			}
		}
		return result;
	}
}
