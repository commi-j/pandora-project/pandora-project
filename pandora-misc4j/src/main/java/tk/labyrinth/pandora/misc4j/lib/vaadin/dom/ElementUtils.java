package tk.labyrinth.pandora.misc4j.lib.vaadin.dom;

import com.vaadin.flow.component.HasElement;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ElementUtils {

	public static HasElement setTitle(HasElement hasElement, @Nullable String title) {
		if (title != null) {
			hasElement.getElement().setAttribute("title", title);
		} else {
			hasElement.getElement().removeAttribute("title");
		}
		return hasElement;
	}
}
