package tk.labyrinth.pandora.misc4j.java.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class UrlUtils {

	/**
	 * Creates {@link URL} without declaring checked {@link MalformedURLException}.
	 *
	 * @param urlString valid URL
	 *
	 * @return non-null
	 *
	 * @throws RuntimeException wrapping {@link MalformedURLException}
	 * @since 1.0.1
	 */
	public static URL from(String urlString) {
		try {
			return new URL(urlString);
		} catch (MalformedURLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static String normalizePath(String path) {
		String result;
		{
			if (!path.startsWith("/")) {
				result = "/%s".formatted(path);
			} else {
				result = path;
			}
		}
		return result;
	}

	/**
	 * Unchecked exception variant of {@link URL#openStream()}.
	 *
	 * @param url non-null
	 *
	 * @return non-null
	 *
	 * @throws RuntimeException wrapping {@link IOException}
	 * @since 1.0.0
	 */
	public static InputStream openStream(URL url) {
		try {
			return url.openStream();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
