package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/Css/css_margin.asp">https://www.w3schools.com/Css/css_margin.asp</a>
 *
 * @author Commitman
 * @version 1.0.2
 */
@Value(staticConstructor = "of")
public class Margin implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final Margin AUTO = of("auto");

	/**
	 * @since 1.0.0
	 */
	public static final Margin INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final Margin INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final Margin NOT_SPECIFIED = of(null);

	@Nullable
	String value;

	@Override
	public String getName() {
		return "margin";
	}
}
