package tk.labyrinth.pandora.misc4j.exception.wrapping;

/**
 * Indicates a wrapper for exception that adds no extra value to it.
 * This interface has a contract that implementing {@link Throwable}
 * must never return null via its {@link Throwable#getCause() getCause()} method.
 *
 * @author Commitman
 * @version 1.0.1
 * @see Throwable#getCause()
 */
public interface ExceptionWrapper {

	/**
	 * Returns {@link Throwable} wrapped by this object. For classes that extend {@link Throwable} this method
	 * is already implemented as {@link Throwable#getCause()};
	 *
	 * @return non-null
	 */
	Throwable getCause();

	/**
	 * Returns first {@link #getCause() cause} that is not an instance of {@link ExceptionWrapper}.
	 *
	 * @return non-null
	 */
	default Throwable unwrap() {
		Throwable result;
		{
			Throwable cause = getCause();
			if (cause instanceof ExceptionWrapper) {
				result = ((ExceptionWrapper) cause).unwrap();
			} else {
				result = cause;
			}
		}
		return result;
	}
}
