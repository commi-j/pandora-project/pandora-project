package tk.labyrinth.pandora.misc4j.java.util.function;

import java.util.function.Consumer;

/**
 * {@link Consumer} variant that may throw exception of parameterized type.
 *
 * @param <T> Type (Input)
 * @param <E> Exception (Throwable)
 *
 * @author Commitman
 * @version 1.0.0
 * @see Consumer
 */
public interface CheckedConsumer<T, E extends Throwable> {

	/**
	 * Performs this operation on the given argument.
	 *
	 * @param t the input argument
	 *
	 * @throws E custom fault
	 */
	void accept(T t) throws E;
}
