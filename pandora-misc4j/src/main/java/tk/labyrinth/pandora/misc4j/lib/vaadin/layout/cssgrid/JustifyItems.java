package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Aligns grid items along the inline (row) axis (as opposed to align-items which aligns along the block (column) axis).
 * This value applies to all grid items inside the container.<br>
 * <br>
 * Values:<br>
 * - start – aligns items to be flush with the start edge of their cell<br>
 * - end – aligns items to be flush with the end edge of their cell<br>
 * - center – aligns items in the center of their cell<br>
 * - stretch – fills the whole width of the cell (this is the default)<br>
 * <br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
 *
 * @author Commitman
 * @version 1.0.1
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum JustifyItems {
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-center.svg" alt="TODO"><br>
	 */
	CENTER("center"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-end.svg" alt="TODO"><br>
	 */
	END("end"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-start.svg" alt="TODO"><br>
	 */
	START("start"),
	/**
	 * Guide:<br>
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items">https://css-tricks.com/snippets/css/complete-guide-grid/#prop-justify-items</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/11/justify-items-stretch.svg" alt="TODO"><br>
	 */
	STRETCH("stretch");

	private final String value;
}
