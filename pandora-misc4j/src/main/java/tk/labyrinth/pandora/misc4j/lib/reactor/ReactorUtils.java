package tk.labyrinth.pandora.misc4j.lib.reactor;

import io.vavr.collection.List;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class ReactorUtils {

	/**
	 * @param fluxes non-null
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	@SuppressWarnings("unchecked")
	public static <T> Flux<List<T>> listLatest(List<Flux<T>> fluxes) {
		return Flux.combineLatest(fluxes, (latests) -> (List<T>) List.of(latests));
	}

	/**
	 * @param flux non-null
	 * @param <T>  Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static <T> Sinks.Many<T> wrap(Flux<T> flux) {
		Sinks.Many<T> many = Sinks.many().multicast().directAllOrNothing();
		//
		Disposable disposable = flux.subscribe(many::tryEmitNext, many::tryEmitError, many::tryEmitComplete);
		many.asFlux().subscribe(null, null, disposable::dispose);
		//
		return many;
	}
}
