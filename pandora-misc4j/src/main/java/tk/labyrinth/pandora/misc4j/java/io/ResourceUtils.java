package tk.labyrinth.pandora.misc4j.java.io;

import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedConsumer;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedFunction;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedSupplier;

import javax.annotation.Nullable;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.4
 */
public class ResourceUtils {

	/**
	 * @param resourceName non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Stream<URL> getResourceUrls(String resourceName) {
		return getResourceUrls(resourceName, Thread.currentThread().getContextClassLoader());
	}

	/**
	 * @param resourceName non-null
	 * @param classLoader  non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static Stream<URL> getResourceUrls(String resourceName, ClassLoader classLoader) {
		try {
			return StreamUtils.from(classLoader.getResources(resourceName));
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @param resource non-null
	 * @param function non-null
	 * @param <R>      Resource
	 * @param <T>      Type
	 *
	 * @return function result
	 *
	 * @since 1.0.3
	 */
	@Nullable
	public static <R extends AutoCloseable, T> T tryReturnWithResource(
			R resource,
			CheckedFunction<R, T, ? extends Exception> function) {
		try (resource) {
			return function.apply(resource);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @param supplier non-null, providing non-null resource once
	 * @param function non-null
	 * @param <R>      Resource
	 * @param <T>      Type
	 *
	 * @return function result
	 *
	 * @since 1.0.3
	 */
	@Nullable
	public static <R extends AutoCloseable, T> T tryReturnWithResourceSupplier(
			CheckedSupplier<R, ? extends Exception> supplier,
			CheckedFunction<R, T, ? extends Exception> function) {
		try (R resource = Objects.requireNonNull(supplier.get(), "supplier.get()")) {
			return function.apply(resource);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @param resource non-null
	 * @param consumer non-null
	 * @param <R>      Resource
	 *
	 * @since 1.0.1
	 */
	public static <R extends AutoCloseable> void tryWithResource(
			R resource,
			CheckedConsumer<R, ? extends Exception> consumer) {
		try (resource) {
			consumer.accept(resource);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @param supplier non-null, providing non-null resource once
	 * @param consumer non-null
	 * @param <R>      Resource
	 *
	 * @since 1.0.2
	 */
	public static <R extends AutoCloseable> void tryWithResourceSupplier(
			CheckedSupplier<R, ? extends Exception> supplier,
			CheckedConsumer<R, ? extends Exception> consumer) {
		try {
			R resource = Objects.requireNonNull(supplier.get(), "supplier.get()");
			try (resource) {
				consumer.accept(resource);
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
