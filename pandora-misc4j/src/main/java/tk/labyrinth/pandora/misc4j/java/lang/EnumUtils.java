package tk.labyrinth.pandora.misc4j.java.lang;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class EnumUtils {

	/**
	 * @param value   nullable
	 * @param values  non-null
	 * @param <E>Enum
	 *
	 * @return whether <b>value</b> is contained in <b>values</b>
	 */
	@SafeVarargs
	public static <E extends Enum<E>> boolean in(@Nullable E value, E... values) {
		boolean result = false;
		for (E innerValue : values) {
			if (innerValue == value) {
				result = true;
				break;
			}
		}
		return result;
	}
}
