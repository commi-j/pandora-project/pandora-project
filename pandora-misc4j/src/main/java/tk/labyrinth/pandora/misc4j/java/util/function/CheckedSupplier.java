package tk.labyrinth.pandora.misc4j.java.util.function;

import javax.annotation.Nullable;
import java.util.function.Supplier;

/**
 * {@link Supplier} variant that may throw exception of parameterized type.<br>
 *
 * @param <T> Type
 * @param <E> Exception (Throwable)
 *
 * @author Commitman
 * @version 1.0.0
 * @see Supplier
 */
public interface CheckedSupplier<T, E extends Throwable> {

	/**
	 * Gets a result.
	 *
	 * @return a result
	 *
	 * @throws E custom fault
	 */
	@Nullable
	T get() throws E;
}
