package tk.labyrinth.pandora.misc4j.exception.wrapping;

import java.util.Objects;

/**
 * {@link Error} that wraps a {@link Throwable}.<br>
 *
 * @author Commitman
 * @version 1.0.0
 * @see ExceptionWrapper#unwrap()
 * @see Throwable#getCause()
 * @see WrappingRuntimeException
 */
public class WrappingError extends Error implements ExceptionWrapper {

	public WrappingError(Throwable cause) {
		super(Objects.requireNonNull(cause, "cause"));
	}
}
