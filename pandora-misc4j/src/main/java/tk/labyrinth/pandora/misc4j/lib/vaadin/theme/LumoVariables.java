package tk.labyrinth.pandora.misc4j.lib.vaadin.theme;

/**
 * @author Commitman
 * @version 1.0.2
 */
public class LumoVariables {

	public static final String BASE_COLOUR = "var(--lumo-base-color)";

	public static final String BODY_TEXT_COLOUR = "var(--lumo-body-text-color)";

	public static final String BORDER_RADIUS = "var(--lumo-border-radius)";

	public static final String CONTRAST_10PCT = "var(--lumo-contrast-10pct)";

	public static final String DISABLED_TEXT_COLOUR = "var(--lumo-disabled-text-color)";

	public static final String ERROR_COLOUR = "var(--lumo-error-color)";

	public static final String ERROR_COLOUR_50PCT = "var(--lumo-error-color-50pct)";

	public static final String ERROR_TEXT_COLOUR = "var(--lumo-error-text-color)";

	public static final String PRIMARY_COLOUR = "var(--lumo-primary-color)";

	public static final String SECONDARY_TEXT_COLOUR = "var(--lumo-secondary-text-color)";

	/**
	 * Added in Vaadin 24.1.
	 */
	public static final String WARNING_COLOUR = "var(--lumo-warning-color)";
}
