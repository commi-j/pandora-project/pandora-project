package tk.labyrinth.pandora.misc4j.java.lang;

import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Commitman
 * @version 1.0.7
 */
public class StringUtils {

	/**
	 * @param first  non-null
	 * @param second non-null
	 *
	 * @return true if first contains second case insensitively, false otherwise
	 *
	 * @since 1.0.2
	 */
	public static boolean containsIgnoreCase(String first, String second) {
		return first.toLowerCase().contains(second.toLowerCase());
	}

	/**
	 * <pre>
	 * "a"  -> "a";<br>
	 * " "  -> " ";<br>
	 * ""   -> null;<br>
	 * null -> null;<br>
	 * </pre>
	 *
	 * @param value nullable
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static String emptyToNull(@Nullable String value) {
		return value != null && !value.isEmpty() ? value : null;
	}

	/**
	 * @param first  nullable
	 * @param second nullable
	 *
	 * @return true if first equals to second case insensitively, false otherwise
	 *
	 * @since 1.0.3
	 */
	public static boolean equalsIgnoreCase(String first, String second) {
		return first != null
				? (second != null && first.equalsIgnoreCase(second))
				: second == null;
	}

	/**
	 * @param value     non-null
	 * @param character any
	 *
	 * @return non-null
	 *
	 * @since 1.0.4
	 */
	public static List<Integer> findAllIndicesOf(String value, char character) {
		List<Integer> result = new ArrayList<>();
		{
			int index = -1;
			while ((index = value.indexOf(character, index + 1)) != -1) {
				result.add(index);
			}
		}
		return result;
	}

	/**
	 * @param value          non-empty
	 * @param characterIndex greater than or equal to 0, less than value.length()
	 *
	 * @return true if escaped, false otherwise
	 *
	 * @since 1.0.5
	 */
	public static boolean isCharacterEscaped(String value, int characterIndex) {
		boolean result;
		{
			if (characterIndex > 0) {
				char previousCharacter = value.charAt(characterIndex - 1);
				//
				if (previousCharacter == '\\') {
					return !isCharacterEscaped(value, characterIndex - 1);
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
		return result;
	}

	/**
	 * Checks that provided <b>string</b> is <b>fit</b>, i.e. satisfies the following criteria:<br>
	 * 1. Is not null;<br>
	 * 2. Is not empty;<br>
	 * 3. Has no circumventing whitespaces (i.e. trimmed);<br>
	 *
	 * @param string any
	 *
	 * @return true if fit, false otherwise
	 *
	 * @see #requireFit(String)
	 * @since 1.0.7
	 */
	public static boolean isFit(@Nullable String string) {
		return string != null && !string.isEmpty() && string.length() == string.trim().length();
	}

	/**
	 * <pre>
	 * "a"  -> "a";<br>
	 * " "  -> " ";<br>
	 * ""   -> "";<br>
	 * null -> "";<br>
	 * </pre>
	 *
	 * @param value nullable
	 *
	 * @return nullable
	 *
	 * @since 1.0.1
	 */
	public static String nullToEmpty(@Nullable String value) {
		return value != null ? value : "";
	}

	/**
	 * Ensures that provided <b>string</b> is <b>fit</b>, i.e. satisfies the following criteria:<br>
	 * 1. Is not null;<br>
	 * 2. Is not empty;<br>
	 * 3. Has no circumventing whitespaces (i.e. trimmed);<br>
	 *
	 * @param string fit
	 *
	 * @return fit
	 *
	 * @see #isFit(String)
	 * @since 1.0.7
	 */
	@NonNull
	public static String requireFit(@NonNull String string) {
		if (!isFit(string)) {
			throw new IllegalArgumentException("Require fit: %s".formatted(string));
		}
		//
		return string;
	}

	/**
	 * @param value   non-null
	 * @param pattern non-null
	 *
	 * @return non-null with non-null left and nullable right
	 *
	 * @since 1.0.5
	 */
	// TODO: Mark 2nd parameter as nullable.
	public static Pair<String, String> splitByFirstOccurrence(String value, String pattern) {
		String[] split = value.split(pattern, 2);
		//
		return Pair.of(split[0], split.length == 2 ? split[1] : null);
	}

	/**
	 * @param value   non-null
	 * @param pattern non-null
	 *
	 * @return non-null with non-null left and nullable right
	 *
	 * @since 1.0.6
	 */
	// TODO: Mark 2nd parameter as nullable.
	// TODO: This one does not actually support patterns.
	public static Pair<String, String> splitByLastOccurrence(String value, String pattern) {
		int index = value.lastIndexOf(pattern);
		//
		return index != -1
				? Pair.of(value.substring(0, index), value.substring(index + pattern.length()))
				: Pair.of(value, null);
	}
}
