package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/csS/css_overflow.asp">https://www.w3schools.com/csS/css_overflow.asp</a>
 *
 * @author Commitman
 * @version 1.0.3
 * @see OverflowX
 * @see OverflowY
 */
@Value(staticConstructor = "of")
public class Overflow implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final Overflow AUTO = of("auto");

	/**
	 * @since 1.0.0
	 */
	public static final Overflow HIDDEN = of("hidden");

	/**
	 * @since 1.0.0
	 */
	public static final Overflow INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final Overflow INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final Overflow NOT_SPECIFIED = of(null);

	/**
	 * @since 1.0.0
	 */
	public static final Overflow SCROLL = of("scroll");

	/**
	 * @since 1.0.0
	 */
	public static final Overflow VISIBLE = of("visible");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "overflow";
	}
}
