package tk.labyrinth.pandora.misc4j.lib.vaadin.tool.setup;

import com.vaadin.flow.server.VaadinServletContext;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import jakarta.servlet.ServletContext;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyConfiguration;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedBean;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Deprecated // FIXME: Or may be not? Not sure.
@LazyConfiguration
public class SpringVaadinBeans {

	@PrototypeScopedBean
	public static ApplicationRouteRegistry applicationRouteRegistry(VaadinServletContext vaadinServletContext) {
		return ApplicationRouteRegistry.getInstance(vaadinServletContext);
	}

	@PrototypeScopedBean
	public static VaadinServletContext vaadinServletContext(ServletContext servletContext) {
		return new VaadinServletContext(servletContext);
	}
}
