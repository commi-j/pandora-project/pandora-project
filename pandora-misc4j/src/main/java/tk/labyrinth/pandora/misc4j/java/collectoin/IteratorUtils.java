package tk.labyrinth.pandora.misc4j.java.collectoin;

import tk.labyrinth.pandora.misc4j.java.collectoin.impl.ArrayIterator;

import javax.annotation.Nullable;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class IteratorUtils {

	/**
	 * @param iterator   non-null
	 * @param consumer   non-null
	 * @param <E>Element
	 */
	public static <E> void forEachWithIndex(Iterator<? extends E> iterator, BiConsumer<Integer, ? super E> consumer) {
		Objects.requireNonNull(iterator, "iterator");
		Objects.requireNonNull(consumer, "consumer");
		//
		int index = 0;
		while (iterator.hasNext()) {
			consumer.accept(index++, iterator.next());
		}
	}

	/**
	 * @param values non-null
	 * @param <E>    Element
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	@SafeVarargs
	public static <E> Iterator<E> from(E... values) {
		return new ArrayIterator<>(values);
	}

	/**
	 * @param enumeration non-null
	 * @param <E>         Element
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <E> Iterator<E> from(Enumeration<E> enumeration) {
		Objects.requireNonNull(enumeration, "enumeration");
		//
		return new Iterator<>() {
			@Override
			public boolean hasNext() {
				return enumeration.hasMoreElements();
			}

			@Override
			public E next() {
				return enumeration.nextElement();
			}
		};
	}

	/**
	 * @param enumeration nullable
	 * @param <E>         Element
	 *
	 * @return nullable
	 *
	 * @since 1.0.0
	 */
	@Nullable
	public static <E> Iterator<E> fromNullable(@Nullable Enumeration<E> enumeration) {
		return enumeration != null ? from(enumeration) : null;
	}
}
