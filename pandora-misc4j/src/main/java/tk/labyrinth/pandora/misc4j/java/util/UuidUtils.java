package tk.labyrinth.pandora.misc4j.java.util;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Commitman
 * @version 1.0.2
 */
public class UuidUtils {

	public static final String PATTERN_STRING =
			"[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}";

	public static final Pattern PATTERN = Pattern.compile(PATTERN_STRING);

	public static final UUID ZERO = UUID.fromString("00000000-0000-0000-0000-000000000000");

	/**
	 * Replaces leading chunks of zeros with (0):<br>
	 * 0f000000-0000-0000-0000-000000000000 -> 0f000000-0000-0000-0000-000000000000;<br>
	 * 00000000-0000-0f00-0000-000000000000 -> (0)-0f00-0000-000000000000;<br>
	 * 00000000-0000-0000-0000-0000f0000000 -> (0)-0000f0000000;<br>
	 * 00000000-0000-0000-0000-000000000000 -> (0);<br>
	 *
	 * @param uuid non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static String uuidToShortString(UUID uuid) {
		return uuid.toString().replaceFirst("^[0-]+(?<lastHyphen>(-|$))", "(0)${lastHyphen}");
	}
}
