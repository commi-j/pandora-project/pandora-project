package tk.labyrinth.pandora.misc4j.lib.vaadin.layout;

import com.vaadin.flow.component.HasComponents;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import java.util.function.Consumer;

public interface LayoutHelper extends HasComponents {

	/**
	 * DSL-like method to add a {@link CssHorizontalLayout} to this Component.
	 * This method creates the layout, passes it to the provided <b>layoutConfigurer</b> and then adds to <b>this</b>.
	 * <pre>
	 * rootLayout.addHorizontalLayout(headerLayout -> {
	 *   // magic
	 * });
	 * rootLayout.addVerticalLayout(contentLayout -> {
	 *   // magic
	 * });
	 * </pre>
	 *
	 * @param layoutConfigurer non-null
	 */
	default void addHorizontalLayout(Consumer<CssHorizontalLayout> layoutConfigurer) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		//
		layoutConfigurer.accept(layout);
		//
		add(layout);
	}

	/**
	 * DSL-like method to add a {@link CssVerticalLayout} to this Component.
	 * This method creates the layout, passes it to the provided <b>layoutConfigurer</b> and then adds to <b>this</b>.
	 * <pre>
	 * rootLayout.addHorizontalLayout(headerLayout -> {
	 *   // magic
	 * });
	 * rootLayout.addVerticalLayout(contentLayout -> {
	 *   // magic
	 * });
	 * </pre>
	 *
	 * @param layoutConfigurer non-null
	 */
	default void addVerticalLayout(Consumer<CssVerticalLayout> layoutConfigurer) {
		CssVerticalLayout layout = new CssVerticalLayout();
		//
		layoutConfigurer.accept(layout);
		//
		add(layout);
	}
}
