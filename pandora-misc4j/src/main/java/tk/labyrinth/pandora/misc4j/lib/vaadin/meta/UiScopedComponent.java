package tk.labyrinth.pandora.misc4j.lib.vaadin.meta;

import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.stereotype.Component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Shorthand for @{@link Component} and @{@link UIScope}.
 *
 * @author Commitman
 * @version 1.0.0
 */
@Component
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@UIScope
public @interface UiScopedComponent {
	// empty
}
