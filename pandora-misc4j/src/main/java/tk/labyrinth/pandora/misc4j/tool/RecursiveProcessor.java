package tk.labyrinth.pandora.misc4j.tool;

import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class RecursiveProcessor {

	/**
	 * @param input    non-null
	 * @param function non-null, receives unique input item, returns pair of new items to process and output items
	 * @param <I>      Input
	 * @param <O>      Output
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <I, O> List<O> process(List<I> input, Function<I, Pair<List<I>, List<O>>> function) {
		List<O> result;
		{
			Set<I> encounteredItems = new HashSet<>(input.asJava());
			LinkedList<I> itemsToProcess = new LinkedList<>(input.asJava());
			ArrayList<O> outputItems = new ArrayList<>();
			//
			while (!itemsToProcess.isEmpty()) {
				I item = itemsToProcess.remove(0);
				//
				Pair<List<I>, List<O>> functionResult = function.apply(item);
				//
				functionResult.getLeft().forEach(newItem -> {
					if (encounteredItems.add(newItem)) {
						itemsToProcess.add(newItem);
					}
				});
				outputItems.addAll(functionResult.getRight().asJava());
			}
			//
			result = List.ofAll(outputItems);
		}
		return result;
	}
}
