package tk.labyrinth.pandora.misc4j.text;

import java.util.regex.Pattern;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class TextUtils {

	/**
	 * @param mask non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static String maskToPattern(String mask) {
		return mask.replace("?", ".?").replace("*", ".*?");
	}

	/**
	 * @param text non-null
	 * @param mask non-null
	 *
	 * @return true if matches, false otherwise
	 *
	 * @since 1.0.0
	 */
	public static boolean matchesMask(String text, String mask) {
		return Pattern.compile(maskToPattern(mask)).matcher(text).matches();
	}
}
