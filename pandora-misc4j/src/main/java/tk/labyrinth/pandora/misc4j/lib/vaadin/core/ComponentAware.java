package tk.labyrinth.pandora.misc4j.lib.vaadin.core;

import com.vaadin.flow.component.Component;

/**
 * @author Commitman
 * @version 1.0.0
 */
public interface ComponentAware {

	default Component asComponent() {
		return (Component) this;
	}
}
