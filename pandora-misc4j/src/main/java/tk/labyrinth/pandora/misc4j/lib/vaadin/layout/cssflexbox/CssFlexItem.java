package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox;

import com.vaadin.flow.component.HasElement;

import javax.annotation.Nullable;

/**
 * @version 1.0.1
 */
public interface CssFlexItem {

	/**
	 * @param component non-null
	 * @param alignSelf nullable, removes if null
	 *
	 * @see AlignSelf
	 */
	static void setAlignSelf(HasElement component, @Nullable AlignSelf alignSelf) {
		component.getElement().getStyle().set(
				CssFlexboxProperty.ALIGN_SELF.key(),
				alignSelf != null ? alignSelf.value() : null);
	}

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/10/flex-grow.svg" alt="TODO"><br>
	 *
	 * @param component non-null
	 * @param flexGrow  nullable, removes if null
	 */
	static void setFlexGrow(HasElement component, double flexGrow) {
		setFlexGrow(component, Double.valueOf(flexGrow));
	}

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 * Image:<br>
	 * <img src="https://css-tricks.com/wp-content/uploads/2018/10/flex-grow.svg" alt="TODO"><br>
	 *
	 * @param component non-null
	 * @param flexGrow  nullable, removes if null
	 */
	static void setFlexGrow(HasElement component, @Nullable Double flexGrow) {
		component.getElement().getStyle().set(
				CssFlexboxProperty.FLEX_GROW.key(),
				flexGrow != null ? flexGrow.toString() : null);
	}

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 *
	 * @param component  non-null
	 * @param flexShrink nullable, removes if null
	 */
	static void setFlexShrink(HasElement component, double flexShrink) {
		setFlexShrink(component, Double.valueOf(flexShrink));
	}

	/**
	 * Guide (scrolling required):<br>
	 * <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/">https://css-tricks.com/snippets/css/a-guide-to-flexbox/</a><br>
	 *
	 * @param component  non-null
	 * @param flexShrink nullable, removes if null
	 */
	static void setFlexShrink(HasElement component, @Nullable Double flexShrink) {
		component.getElement().getStyle().set(
				CssFlexboxProperty.FLEX_SHRINK.key(),
				flexShrink != null ? flexShrink.toString() : null);
	}
}
