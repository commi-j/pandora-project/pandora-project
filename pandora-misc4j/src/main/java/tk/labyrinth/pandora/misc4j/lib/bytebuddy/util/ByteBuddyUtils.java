package tk.labyrinth.pandora.misc4j.lib.bytebuddy.util;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.ClassFileVersion;
import net.bytebuddy.NamingStrategy;
import net.bytebuddy.description.type.TypeDescription;

import java.lang.reflect.Type;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ByteBuddyUtils {

	private static final NamingStrategy defaultNamingStrategy = new ParameterizedChildDefaultNamingStrategy();

	@SuppressWarnings("unchecked")
	public static <C> Class<? extends C> generateParameterizedChild(Class<C> baseType, Type... parameterTypes) {
		return (Class<? extends C>) new ByteBuddy(ClassFileVersion.JAVA_V8)
				.with(defaultNamingStrategy)
				.subclass(TypeDescription.Generic.Builder.parameterizedType(baseType, parameterTypes).build())
				.make()
				.load(Thread.currentThread().getContextClassLoader())
				.getLoaded();
	}
}
