package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Guide:<br>
 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/">https://css-tricks.com/snippets/css/complete-guide-grid/</a><br>
 *
 * @author Commitman
 * @version 1.0.3
 * @see CssGridAware
 * @see CssGridItem
 * @see CssGridLayout
 */
@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public enum CssGridProperty {
	ALIGN_CONTENT("align-content"),
	ALIGN_ITEMS("align-items"),
	ALIGN_SELF("align-self"),
	COLUMN_GAP("column-gap"),
	GAP("gap"),
	GRID_AREA("grid-area"),
	GRID_COLUMN("grid-column"),
	GRID_COLUMN_END("grid-column-end"),
	GRID_COLUMN_START("grid-column-start"),
	GRID_ROW("grid-row"),
	GRID_ROW_END("grid-row-end"),
	GRID_ROW_START("grid-row-start"),
	GRID_TEMPLATE_AREAS("grid-template-areas"),
	GRID_TEMPLATE_COLUMNS("grid-template-columns"),
	GRID_TEMPLATE_ROWS("grid-template-rows"),
	JUSTIFY_CONTENT("justify-content"),
	JUSTIFY_ITEMS("justify-items"),
	JUSTIFY_SELF("justify-self"),
	ROW_GAP("row-gap");

	private final String key;
}
