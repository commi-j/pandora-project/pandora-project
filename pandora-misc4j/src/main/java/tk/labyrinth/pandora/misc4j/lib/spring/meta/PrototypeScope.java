/*
 * Copyright 2000-2017 Vaadin Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package tk.labyrinth.pandora.misc4j.lib.spring.meta;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Shorthand for @{@link Scope}({@link BeanDefinition#SCOPE_PROTOTYPE}).
 *
 * @author Commitman
 * @version 1.0.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface PrototypeScope {
	// empty
}
