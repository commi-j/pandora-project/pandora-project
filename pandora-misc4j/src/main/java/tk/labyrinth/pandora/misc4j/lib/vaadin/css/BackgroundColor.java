package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/pr_background-color.php">https://www.w3schools.com/cssref/pr_background-color.php</a>
 * <a href="https://www.w3schools.com/colors/colors_names.asp">https://www.w3schools.com/colors/colors_names.asp</a><br>
 *
 * @author Commitman
 * @version 1.0.3
 */
@Value(staticConstructor = "of")
public class BackgroundColor implements CssProperty {

	/**
	 * @since 1.0.1
	 */
	public static final BackgroundColor BLACK = of("black");

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor GRAY = of("gray");

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor GREY = of("grey");

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor LIGHTGRAY = of("lightgray");

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor LIGHTGREY = of("lightgrey");

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor NOT_SPECIFIED = of(null);

	/**
	 * @since 1.0.0
	 */
	public static final BackgroundColor RED = of("red");

	/**
	 * @since 1.0.1
	 */
	public static final BackgroundColor WHITE = of("white");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "background-color";
	}
}
