package tk.labyrinth.pandora.misc4j.lib.spring.meta;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class BeanDefinitionRegistryUtils {

	public static <T> Stream<Class<? extends T>> getInheritingBeanTypes(
			BeanDefinitionRegistry registry,
			Class<T> baseType) {
		return Stream.of(registry.getBeanDefinitionNames())
				.map(registry::getBeanDefinition)
				// FIXME: Find generic solution
				.filter(beanDefinition -> !Objects.equals(beanDefinition.getAttribute("bbGenerated"), true))
				.map(BeanDefinition::getBeanClassName)
				.filter(Objects::nonNull)
				.map(typeName -> {
					try {
						// TODO: Replace with utility method of getting Class.
						return Class.forName(typeName);
					} catch (ClassNotFoundException ex) {
						throw new RuntimeException(ex);
					}
				})
				.filter(baseType::isAssignableFrom)
				.map(type -> type.asSubclass(baseType));
	}
}
