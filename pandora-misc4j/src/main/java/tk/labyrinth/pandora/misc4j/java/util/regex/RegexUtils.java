package tk.labyrinth.pandora.misc4j.java.util.regex;

import javax.annotation.CheckForNull;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class RegexUtils {

	/**
	 * @param value   non-null
	 * @param pattern non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static List<MatchResult> findAll(String value, Pattern pattern) {
		return pattern.matcher(value).results().collect(Collectors.toList());
	}

	/**
	 * @param value     non-null
	 * @param pattern   non-null
	 * @param groupName non-null
	 *
	 * @return nullable
	 *
	 * @since 1.0.1
	 */
	@CheckForNull
	public static String findGroupValue(String value, Pattern pattern, String groupName) {
		String result;
		{
			Matcher matcher = pattern.matcher(value);
			if (matcher.matches()) {
				result = matcher.group(groupName);
			} else {
				result = null;
			}
		}
		return result;
	}

	/**
	 * @param value   non-null
	 * @param pattern non-null
	 *
	 * @return true or false
	 *
	 * @since 1.0.1
	 */
	public static boolean matches(String value, Pattern pattern) {
		return pattern.matcher(value).matches();
	}
}
