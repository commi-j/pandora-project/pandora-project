package tk.labyrinth.pandora.misc4j.lib.vaadin.tool.fault;

/**
 * @see VaadinFaultHandlerProcessor
 */
public interface VaadinFaultHandler {

	void handle(Throwable fault);
}
