package tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid;

import com.vaadin.flow.component.HasElement;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.4
 */
public interface CssGridItem {

	/**
	 * @param component non-null
	 * @param alignSelf nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setAlignSelf(HasElement component, @Nullable PlaceSelf alignSelf) {
		component.getElement().getStyle().set(
				CssGridProperty.ALIGN_SELF.key(),
				alignSelf != null ? alignSelf.value() : null);
	}

	/**
	 * <a href="https://css-tricks.com/snippets/css/complete-guide-grid/#aa-grid-area">https://css-tricks.com/snippets/css/complete-guide-grid/#aa-grid-area</a>
	 *
	 * @param hasElement non-null
	 * @param gridArea   nullable, removes if null
	 *
	 * @since 1.0.4
	 */
	static void setGridArea(HasElement hasElement, @Nullable String gridArea) {
		hasElement.getElement().getStyle().set(
				CssGridProperty.GRID_AREA.key(),
				gridArea);
	}

	/**
	 * @param component  non-null
	 * @param gridColumn nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setGridColumn(HasElement component, @Nullable String gridColumn) {
		component.getElement().getStyle().set(CssGridProperty.GRID_COLUMN.key(), gridColumn);
	}

	/**
	 * @param component     non-null
	 * @param gridColumnEnd nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setGridColumnEnd(HasElement component, @Nullable String gridColumnEnd) {
		component.getElement().getStyle().set(CssGridProperty.GRID_COLUMN_END.key(), gridColumnEnd);
	}

	/**
	 * @param component      non-null
	 * @param gridColumnSpan nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setGridColumnSpan(HasElement component, @Nullable Integer gridColumnSpan) {
		setGridColumn(component, gridColumnSpan != null ? "span " + gridColumnSpan : null);
	}

	/**
	 * @param component       non-null
	 * @param gridColumnStart nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setGridColumnStart(HasElement component, @Nullable String gridColumnStart) {
		component.getElement().getStyle().set(CssGridProperty.GRID_COLUMN_START.key(), gridColumnStart);
	}

	/**
	 * @param component non-null
	 * @param gridRow   nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setGridRow(HasElement component, @Nullable String gridRow) {
		component.getElement().getStyle().set(CssGridProperty.GRID_ROW.key(), gridRow);
	}

	/**
	 * @param component   non-null
	 * @param gridRowSpan nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setGridRowSpan(HasElement component, @Nullable Integer gridRowSpan) {
		setGridRow(component, gridRowSpan != null ? "span " + gridRowSpan : null);
	}

	/**
	 * @param component      non-null
	 * @param gridRowSpan    nullable, removes if null
	 * @param gridColumnSpan nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setGridSpan(HasElement component, @Nullable Integer gridRowSpan, @Nullable Integer gridColumnSpan) {
		setGridRowSpan(component, gridRowSpan);
		setGridColumnSpan(component, gridColumnSpan);
	}

	/**
	 * @param component   non-null
	 * @param justifySelf nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setJustifySelf(HasElement component, @Nullable PlaceSelf justifySelf) {
		component.getElement().getStyle().set(
				CssGridProperty.JUSTIFY_SELF.key(),
				justifySelf != null ? justifySelf.value() : null);
	}

	/**
	 * @param component non-null
	 * @param placeSelf nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setPlaceSelf(HasElement component, @Nullable PlaceSelf placeSelf) {
		setPlaceSelf(component, placeSelf, placeSelf);
	}

	/**
	 * @param component   non-null
	 * @param justifySelf nullable, removes if null
	 * @param alignSelf   nullable, removes if null
	 *
	 * @since 1.0.3
	 */
	static void setPlaceSelf(HasElement component, @Nullable PlaceSelf justifySelf, @Nullable PlaceSelf alignSelf) {
		setJustifySelf(component, justifySelf);
		setAlignSelf(component, alignSelf);
	}
}
