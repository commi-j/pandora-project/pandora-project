package tk.labyrinth.pandora.misc4j.lib.vaadin.html.table;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Tag("th")
public class HtmlTableHeader extends HtmlContainer {

	public HtmlTableHeader() {
		// no-op
	}

	public HtmlTableHeader(String text) {
		add(text);
	}
}
