package tk.labyrinth.pandora.misc4j.lib.vaadin.util;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import java.util.function.Function;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class GridUtils {

	/**
	 * @param list                  non-null
	 * @param <T>                   Type
	 * @param renderElementFunction non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static <T> Component renderComponentList(List<T> list, Function<T, Component> renderElementFunction) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.setAlignItems(AlignItems.BASELINE);
		}
		{
			list.forEach(element -> layout.add(renderElementFunction.apply(element)));
		}
		return layout;
	}

	/**
	 * @param list                  non-null
	 * @param <T>                   Type
	 * @param renderElementFunction non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static <T> Component renderValueList(List<T> list, Function<T, Object> renderElementFunction) {
		return renderComponentList(list, element -> new Span(renderElementFunction.apply(element).toString()));
	}

	/**
	 * @param list non-null
	 * @param <T>  Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static <T> Component renderValueList(List<T> list) {
		return renderValueList(list, element -> element);
	}
}
