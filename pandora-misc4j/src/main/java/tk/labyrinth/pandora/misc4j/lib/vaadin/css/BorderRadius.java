package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/css3_pr_border-radius.php">https://www.w3schools.com/cssref/css3_pr_border-radius.php</a>
 *
 * @author Commitman
 * @version 1.0.2
 */
@Value(staticConstructor = "of")
public class BorderRadius implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final BorderRadius INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final BorderRadius INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final BorderRadius NOT_SPECIFIED = of(null);

	@Nullable
	String value;

	@Override
	public String getName() {
		return "border-radius";
	}
}
