package tk.labyrinth.pandora.misc4j.java.collectoin;

import javax.annotation.Nullable;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class SetUtils {

	/**
	 * @param set     non-null
	 * @param element nullable
	 * @param <E>     Element
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <E> Set<E> append(Set<E> set, @Nullable E element) {
		return StreamUtils.concat(set.stream(), element).collect(Collectors.toUnmodifiableSet());
	}
}
