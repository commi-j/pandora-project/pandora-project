package tk.labyrinth.pandora.misc4j.java.lang;

import java.util.concurrent.Callable;

/**
 * {@link Runnable} variant that may throw exception of parameterized type.<br>
 * <b>Note:</b> Unlike {@link Callable} this class allows unchecked exceptions as well.<br>
 *
 * @param <E> Exception (Throwable)
 *
 * @author Commitman
 * @version 1.0.0
 * @see Runnable
 */
public interface CheckedRunnable<E extends Throwable> {

	void run() throws E;
}
