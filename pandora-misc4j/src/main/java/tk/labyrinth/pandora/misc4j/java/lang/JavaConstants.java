package tk.labyrinth.pandora.misc4j.java.lang;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class JavaConstants {

	public static final Map<String, Class<?>> KEYWORDS = Stream
			.of(
					boolean.class,
					byte.class,
					char.class,
					double.class,
					float.class,
					int.class,
					long.class,
					short.class,
					void.class)
			.collect(Collectors.toMap(
					Class::getName,
					Function.identity()));
}
