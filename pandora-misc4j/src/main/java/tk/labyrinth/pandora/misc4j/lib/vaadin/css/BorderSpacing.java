package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/csSref/pr_border-spacing.php">https://www.w3schools.com/csSref/pr_border-spacing.php</a>
 *
 * @author Commitman
 * @version 1.0.1
 */
@Value(staticConstructor = "of")
public class BorderSpacing implements CssProperty {

	@Nullable
	String value;

	@Override
	public String getName() {
		return "border-spacing";
	}
}
