package tk.labyrinth.pandora.misc4j.data;

/**
 * Singleton сlass to be used where nulls are forbidden to indicate success.<br>
 * Examples are:<br>
 * - {@link reactor.core.publisher.Mono}.<br>
 *
 * @author Commitman
 * @version 1.0.0
 */
public enum Success {
	VALUE
}
