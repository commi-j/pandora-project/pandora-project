package tk.labyrinth.pandora.misc4j.java.lang;

import io.vavr.collection.Stream;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

/**
 * @author Commitman
 * @version 1.1.1
 */
public class ObjectUtils {

	/**
	 * Accepts object and applies provided {@link Consumer} to it. Returns the object received as first argument.
	 * This method is useful for functional style chained invocations.
	 *
	 * @param object   non-null
	 * @param consumer non-null
	 * @param <T>      Type
	 *
	 * @return object
	 *
	 * @since 1.0.8
	 */
	public static <T> T consumeAndReturn(T object, Consumer<T> consumer) {
		consumer.accept(object);
		//
		return object;
	}

	/**
	 * @param defaultValue any
	 * @param suppliers    non-null
	 * @param <T>          Type
	 *
	 * @return any
	 *
	 * @since 1.0.9
	 */
	@Nullable // TODO: PolyNull
	@SafeVarargs
	public static <T> T findFirstNonDefaultValue(T defaultValue, Supplier<T>... suppliers) {
		return Stream.of(suppliers)
				.map(Supplier::get)
				.filter(value -> !Objects.equals(value, defaultValue))
				.headOption()
				.getOrElse(defaultValue);
	}

	/**
	 * @param value  nullable
	 * @param values non-null
	 * @param <T>    Type
	 *
	 * @return whether <b>value</b> is contained in <b>values</b>
	 */
	@SafeVarargs
	public static <T> boolean in(@Nullable T value, T... values) {
		boolean result = false;
		for (T innerValue : values) {
			if (Objects.equals(innerValue, value)) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * @param value non-null
	 * @param <T>   Type
	 *
	 * @return non-null
	 *
	 * @since 1.1.1
	 */
	public static <T> T infer(Object value) {
		return inferNullable(Objects.requireNonNull(value, "value"));
	}

	/**
	 * @param value nullable
	 * @param <T>   Type
	 *
	 * @return nullable
	 *
	 * @since 1.1.1
	 */
	@Nullable
	@SuppressWarnings("unchecked")
	public static <T> T inferNullable(@Nullable Object value) {
		return (T) value;
	}

	/**
	 * Applies function to provided object and returns application result.
	 * This method is convenient for chained invocation.
	 *
	 * @param object   any
	 * @param function non-null
	 * @param <T>      Type
	 *
	 * @return any
	 *
	 * @since 1.0.6
	 */
	@Nullable
	public static <T, R> R map(@Nullable T object, Function<T, R> function) {
		return object != null ? function.apply(object) : null;
	}

	/**
	 * @param first                nullable
	 * @param second               nullable
	 * @param mergeOperator        non-null with non-null result
	 * @param defaultValueSupplier non-null with non-null value
	 * @param <T>                  Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> T mergeNullable(@Nullable T first, @Nullable T second, BinaryOperator<T> mergeOperator, Supplier<T> defaultValueSupplier) {
		Objects.requireNonNull(mergeOperator, "mergeOperator");
		Objects.requireNonNull(defaultValueSupplier, "defaultValueSupplier");
		//
		T result;
		if (first != null) {
			if (second != null) {
				result = Objects.requireNonNull(mergeOperator.apply(first, second), "mergeResult");
			} else {
				result = first;
			}
		} else {
			if (second != null) {
				result = second;
			} else {
				result = Objects.requireNonNull(defaultValueSupplier.get(), "defaultValue");
			}
		}
		return result;
	}

	/**
	 * @param value        nullable
	 * @param defaultValue non-null
	 * @param <T>          Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static <T> T orElse(@CheckForNull T value, T defaultValue) {
		return value != null ? value : defaultValue;
	}

	/**
	 * @param defaultValue any
	 * @param suppliers    non-null
	 * @param <T>          Type
	 *
	 * @return any
	 *
	 * @since 1.0.7
	 * @deprecated See {@link #findFirstNonDefaultValue}
	 */
	@Deprecated
	@SafeVarargs
	public static <T> T pickFirstNonDefaultValue(T defaultValue, Supplier<T>... suppliers) {
		return Stream.of(suppliers)
				.map(Supplier::get)
				.filter(value -> !Objects.equals(value, defaultValue))
				.head();
	}

	/**
	 * Recursively applies provided <b>operator</b> to <b>initialValue</b> and subsequent results
	 * until <b>predicate</b> returns false, preconditionally (while equivalent).
	 *
	 * @param initialValue non-null
	 * @param predicate    non-null
	 * @param operator     non-null
	 * @param <T>          Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static <T> T reduce(T initialValue, Predicate<T> predicate, UnaryOperator<T> operator) {
		return Objects.requireNonNull(reduceNullable(initialValue, predicate, operator));
	}

	/**
	 * Recursively applies provided <b>operator</b> to <b>initialValue</b> and subsequent results
	 * until <b>predicate</b> returns false, preconditionally (while equivalent).
	 *
	 * @param initialValue nullable
	 * @param predicate    non-null
	 * @param operator     non-null
	 * @param <T>          Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.3
	 */
	@Nullable
	public static <T> T reduceNullable(@Nullable T initialValue, Predicate<T> predicate, UnaryOperator<T> operator) {
		T value = initialValue;
		while (predicate.test(value)) {
			value = operator.apply(value);
		}
		return value;
	}

	/**
	 * Checks <b>value</b> for being null.
	 *
	 * @param value null
	 * @param name  nullable
	 * @param <T>   Type
	 *
	 * @return null
	 *
	 * @throws IllegalArgumentException if value is not null
	 * @since 1.0.2
	 */
	@Nullable
	public static <T> T requireNull(@Nullable T value, @Nullable String name) {
		if (value != null) {
			throw new IllegalArgumentException("Require null: %s%s".formatted(name != null ? name + ": " : "", value));
		}
		return null;
	}
}
