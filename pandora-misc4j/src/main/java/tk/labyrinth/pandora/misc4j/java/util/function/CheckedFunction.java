package tk.labyrinth.pandora.misc4j.java.util.function;

import javax.annotation.Nullable;
import java.util.function.Function;

/**
 * {@link Function} variant that may throw exception of parameterized type.
 *
 * @param <T> Type (Input)
 * @param <R> Result
 * @param <E> Exception (Throwable)
 *
 * @author Commitman
 * @version 1.0.0
 * @see Function
 */
public interface CheckedFunction<T, R, E extends Throwable> {

	/**
	 * Applies this function to the given argument.
	 *
	 * @param t the function argument
	 *
	 * @return the function result
	 *
	 * @throws E custom fault
	 */
	@Nullable
	R apply(T t) throws E;
}
