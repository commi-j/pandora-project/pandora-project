package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/csSref/pr_class_cursor.php">https://www.w3schools.com/csSref/pr_class_cursor.php</a>
 *
 * @author Commitman
 * @version 1.0.3
 */
@Value(staticConstructor = "of")
public class Cursor implements CssProperty {

	public static final Cursor ALIAS = of("alias");

	public static final Cursor ALL_SCROLL = of("all-scroll");

	public static final Cursor AUTO = of("auto");

	public static final Cursor CELL = of("cell");

	public static final Cursor COL_RESIZE = of("col-resize");

	public static final Cursor CONTEXT_MENU = of("context-menu");

	public static final Cursor COPY = of("copy");

	public static final Cursor CROSSHAIR = of("crosshair");

	public static final Cursor DEFAULT = of("default");

	public static final Cursor E_RESIZE = of("e-resize");

	public static final Cursor EW_RESIZE = of("ew-resize");

	public static final Cursor INHERIT = of("inherit");

	public static final Cursor INITIAL = of("initial");

	public static final Cursor GRAB = of("grab");

	public static final Cursor GRABBING = of("grabbing");

	public static final Cursor HELP = of("help");

	public static final Cursor MOVE = of("move");

	public static final Cursor N_RESIZE = of("n-resize");

	public static final Cursor NE_RESIZE = of("ne-resize");

	public static final Cursor NESW_RESIZE = of("nesw-resize");

	public static final Cursor NO_DROP = of("no-drop");

	public static final Cursor NONE = of("none");

	public static final Cursor NOT_ALLOWED = of("not-allowed");

	public static final Cursor NS_RESIZE = of("ns-resize");

	public static final Cursor NW_RESIZE = of("nw-resize");

	public static final Cursor NWSE_RESIZE = of("nwse-resize");

	public static final Cursor POINTER = of("pointer");

	public static final Cursor PROGRESS = of("progress");

	public static final Cursor ROW_RESIZE = of("row-resize");

	public static final Cursor S_RESIZE = of("s-resize");

	public static final Cursor SE_RESIZE = of("se-resize");

	public static final Cursor SW_RESIZE = of("sw-resize");

	public static final Cursor TEXT = of("text");

	public static final Cursor URL = of("URL");

	public static final Cursor VERTICAL_TEXT = of("vertical-text");

	public static final Cursor W_RESIZE = of("w-resize");

	public static final Cursor WAIT = of("wait");

	public static final Cursor ZOOM_IN = of("zoom-in");

	public static final Cursor ZOOM_OUT = of("zoom-out");

	@Nullable
	String value;

	@Override
	public String getName() {
		return "cursor";
	}
}
