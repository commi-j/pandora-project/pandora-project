package tk.labyrinth.pandora.misc4j.java.util.function;

import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;

import java.util.function.Function;

/**
 * @author Commitman
 * @version 1.0.2
 */
public class FunctionUtils {

	public static final Object DUMMY = new Object();

	/**
	 * For use in Flux distinctUntilChanged transformation as Reactor does not support nulls.
	 *
	 * @param function non-null
	 * @param <T>Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static <T> Function<T, ?> getOrDummy(Function<T, ?> function) {
		return function.andThen(result -> result != null ? result : DUMMY);
	}

	/**
	 * @param value any
	 * @param <T>   Input Type
	 * @param <R>   Result
	 *
	 * @return unreachable
	 *
	 * @throws UnreachableStateException always
	 * @since 1.0.2
	 */
	public static <T, R> R throwUnreachableStateException(T value) {
		throw new UnreachableStateException(String.valueOf(value));
	}

	/**
	 * @param first  any
	 * @param second any
	 * @param <T>    Input Type
	 * @param <R>    Result
	 *
	 * @return unreachable
	 *
	 * @throws UnreachableStateException always
	 * @since 1.0.0
	 */
	public static <T, R> R throwUnreachableStateException(T first, R second) {
		throw new UnreachableStateException();
	}
}
