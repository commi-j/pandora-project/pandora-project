package tk.labyrinth.pandora.misc4j.util;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class ExecUtils {

	public static Outcome exec(
			String command,
			@Nullable List<String> environmentVariables,
			@Nullable Path path,
			Duration timeout) {
		try {
			Process process = Runtime.getRuntime().exec(
					command,
					environmentVariables != null ? environmentVariables.toArray(String[]::new) : null,
					path != null ? path.toFile() : null);
			//
			boolean terminated = process.waitFor(timeout.toMillis(), TimeUnit.MILLISECONDS);
			if (!terminated) {
				process.destroyForcibly();
				process.waitFor();
			}
			int exitCode = process.exitValue();
			//
			List<String> output = IOUtils.readLines(process.inputReader());
			List<String> error = IOUtils.readLines(process.errorReader());
			//
			return Outcome.builder()
					.error(String.join("\n", error))
					.exitCode(exitCode)
					.output(String.join("\n", output))
					.build();
		} catch (IOException | InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Outcome {

		String error;

		int exitCode;

		String output;

		public String render() {
			return !output.isEmpty()
					? !error.isEmpty()
					? "%s\n\n%s\n".formatted(output, error)
					: "%s\n".formatted(output)
					: "%s\n".formatted(error);
		}
	}
}
