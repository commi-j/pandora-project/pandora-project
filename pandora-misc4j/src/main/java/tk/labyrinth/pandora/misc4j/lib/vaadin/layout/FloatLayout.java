package tk.labyrinth.pandora.misc4j.lib.vaadin.layout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import lombok.Getter;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;

import javax.annotation.Nullable;

@Getter
@Tag("float-layout")
public class FloatLayout extends CssGridLayout {

	private Double xAlignment;

	private Double yAlignment;

	public FloatLayout() {
		this(null, 0.5, 0.5);
	}

	public FloatLayout(Component content) {
		this(content, 0.5, 0.5);
	}

	public FloatLayout(@Nullable Component content, @Nullable Double xAlignment, @Nullable Double yAlignment) {
		setContent(content);
		setAlignment(xAlignment, yAlignment);
		//
		setGridTemplateAreas(
				". . .",
				". content .",
				". . .");
	}

	private void updateGrid() {
		setGridTemplateColumns(xAlignment != null
				? xAlignment + "fr auto " + (1 - xAlignment) + "fr"
				: "0px 1fr 0px");
		setGridTemplateRows(yAlignment != null
				? yAlignment + "fr auto " + (1 - yAlignment) + "fr"
				: "0px 1fr 0px");
	}

	@Nullable
	public Component getContent() {
		return getChildren().findFirst().orElse(null);
	}

	public void setAlignment(double xAlignment, double yAlignment) {
		setAlignment(Double.valueOf(xAlignment), Double.valueOf(yAlignment));
	}

	public void setAlignment(@Nullable Double xAlignment, @Nullable Double yAlignment) {
		this.xAlignment = xAlignment;
		this.yAlignment = yAlignment;
		updateGrid();
	}

	public void setContent(@Nullable Component content) {
		removeAll();
		if (content != null) {
			add(content, "content");
		}
	}

	public void setXAlignment(double xAlignment) {
		setAlignment(Double.valueOf(xAlignment), yAlignment);
	}

	public void setXAlignment(@Nullable Double xAlignment) {
		setAlignment(xAlignment, yAlignment);
	}

	public void setYAlignment(double yAlignment) {
		setAlignment(xAlignment, Double.valueOf(yAlignment));
	}

	public void setYAlignment(@Nullable Double yAlignment) {
		setAlignment(xAlignment, yAlignment);
	}
}
