package tk.labyrinth.pandora.misc4j.integration.auth;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class AuthorizationUtils {

	/**
	 * @param token non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.1
	 */
	public static String composeBearer(String token) {
		return "Bearer %s".formatted(token);
	}

	public static String encodeBasic(String username, String password) {
		return "Basic %s".formatted(encodeCredentials(username, password));
	}

	public static String encodeCredentials(String username, String password) {
		return new String(
				Base64.getEncoder().encode((username + ":" + password).getBytes(StandardCharsets.UTF_8)),
				StandardCharsets.UTF_8);
	}
}
