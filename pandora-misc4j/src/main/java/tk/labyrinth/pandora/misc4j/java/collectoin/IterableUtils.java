package tk.labyrinth.pandora.misc4j.java.collectoin;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

/**
 * @author Commitman
 * @version 1.0.1
 */
public class IterableUtils {

	public static <E> void forEachWithIndex(
			@Nonnull Iterable<? extends E> iterable,
			@Nonnull BiConsumer<Integer, ? super E> consumer) {
		IteratorUtils.forEachWithIndex(iterable.iterator(), consumer);
	}
}
