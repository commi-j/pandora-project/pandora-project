package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.2
 */
@Value(staticConstructor = "of")
public class CustomCssProperty implements CssProperty {

	String name;

	@Nullable
	String value;
}
