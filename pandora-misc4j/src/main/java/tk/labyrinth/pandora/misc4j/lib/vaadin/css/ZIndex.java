package tk.labyrinth.pandora.misc4j.lib.vaadin.css;

import lombok.Value;

import javax.annotation.Nullable;

/**
 * <a href="https://www.w3schools.com/cssref/pr_pos_z-index.php">https://www.w3schools.com/cssref/pr_pos_z-index.php</a>
 *
 * @author Commitman
 * @version 1.0.3
 */
@Value(staticConstructor = "of")
public class ZIndex implements CssProperty {

	/**
	 * @since 1.0.0
	 */
	public static final ZIndex AUTO = of("auto");

	/**
	 * @since 1.0.0
	 */
	public static final ZIndex INHERIT = of("inherit");

	/**
	 * @since 1.0.0
	 */
	public static final ZIndex INITIAL = of("initial");

	/**
	 * @since 1.0.0
	 */
	public static final ZIndex NOT_SPECIFIED = of((String) null);

	/**
	 * @since 1.0.2
	 */
	public static final ZIndex ZERO = of(0);

	@Nullable
	String value;

	@Override
	public String getName() {
		return "z-index";
	}

	public static ZIndex of(@Nullable Integer value) {
		return of(value != null ? value.toString() : null);
	}
}
