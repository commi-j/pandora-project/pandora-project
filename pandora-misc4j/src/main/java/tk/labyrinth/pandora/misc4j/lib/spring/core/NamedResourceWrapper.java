package tk.labyrinth.pandora.misc4j.lib.spring.core;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

/**
 * @author Commitman
 * @version 1.0.0
 */
@RequiredArgsConstructor
public class NamedResourceWrapper implements Resource {

	private final String filename;

	private final Resource resource;

	@Override
	public long contentLength() throws IOException {
		return resource.contentLength();
	}

	@Override
	public Resource createRelative(String relativePath) throws IOException {
		return resource.createRelative(relativePath);
	}

	@Override
	public boolean exists() {
		return resource.exists();
	}

	@Override
	public String getDescription() {
		return resource.getDescription();
	}

	@Override
	public File getFile() throws IOException {
		return resource.getFile();
	}

	@Override
	public String getFilename() {
		return filename;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return resource.getInputStream();
	}

	@Override
	public URI getURI() throws IOException {
		return resource.getURI();
	}

	@Override
	public URL getURL() throws IOException {
		return resource.getURL();
	}

	@Override
	public long lastModified() throws IOException {
		return resource.lastModified();
	}
}
