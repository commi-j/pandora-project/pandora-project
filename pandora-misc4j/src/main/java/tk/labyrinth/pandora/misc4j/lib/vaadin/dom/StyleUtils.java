package tk.labyrinth.pandora.misc4j.lib.vaadin.dom;

import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.dom.Style;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.CssProperty;

import javax.annotation.Nullable;

/**
 * @author Commitman
 * @version 1.0.6
 */
public class StyleUtils {

	/**
	 * @param hasElement      non-null
	 * @param backgroundColor nullable
	 *
	 * @return hasElement
	 *
	 * @since 1.0.4
	 */
	public static HasElement setBackgroundColor(HasElement hasElement, @Nullable String backgroundColor) {
		setBackgroundColor(hasElement.getElement().getStyle(), backgroundColor);
		return hasElement;
	}

	/**
	 * @param style           non-null
	 * @param backgroundColor nullable
	 *
	 * @return style
	 *
	 * @since 1.0.4
	 */
	public static Style setBackgroundColor(Style style, @Nullable String backgroundColor) {
		if (backgroundColor != null) {
			style.set("background-color", backgroundColor);
		} else {
			style.remove("background-color");
		}
		return style;
	}

	/**
	 * @param hasElement non-null
	 * @param border     nullable
	 *
	 * @return hasElement
	 *
	 * @since 1.0.4
	 */
	public static HasElement setBorder(HasElement hasElement, @Nullable String border) {
		setBorder(hasElement.getElement().getStyle(), border);
		return hasElement;
	}

	/**
	 * @param style  non-null
	 * @param border nullable
	 *
	 * @return style
	 *
	 * @since 1.0.4
	 */
	public static Style setBorder(Style style, @Nullable String border) {
		if (border != null) {
			style.set("border", border);
		} else {
			style.remove("border");
		}
		return style;
	}

	public static HasElement setColor(HasElement hasElement, @Nullable String color) {
		setColor(hasElement.getElement().getStyle(), color);
		return hasElement;
	}

	public static Style setColor(Style style, @Nullable String color) {
		if (color != null) {
			style.set("color", color);
		} else {
			style.remove("color");
		}
		return style;
	}

	/**
	 * @param element     non-null
	 * @param cssProperty non-null
	 *
	 * @return hasElement
	 *
	 * @since 1.0.6
	 */
	public static Element setCssProperty(Element element, CssProperty cssProperty) {
		setCssProperty(element.getStyle(), cssProperty);
		return element;
	}

	/**
	 * @param hasElement  non-null
	 * @param cssProperty non-null
	 *
	 * @return hasElement
	 *
	 * @since 1.0.5
	 */
	public static HasElement setCssProperty(HasElement hasElement, CssProperty cssProperty) {
		setCssProperty(hasElement.getElement().getStyle(), cssProperty);
		return hasElement;
	}

	/**
	 * @param style       non-null
	 * @param cssProperty non-null
	 *
	 * @return style
	 *
	 * @since 1.0.5
	 */
	public static Style setCssProperty(Style style, CssProperty cssProperty) {
		if (cssProperty.getValue() != null) {
			style.set(cssProperty.getName(), cssProperty.getValue());
		} else {
			style.remove(cssProperty.getName());
		}
		return style;
	}

	public static HasElement setGap(HasElement hasElement, @Nullable String gap) {
		setGap(hasElement.getElement().getStyle(), gap);
		return hasElement;
	}

	public static Style setGap(Style style, @Nullable String gap) {
		if (gap != null) {
			style.set("gap", gap);
		} else {
			style.remove("gap");
		}
		return style;
	}

	public static HasElement setHeight(HasElement hasElement, @Nullable String height) {
		setHeight(hasElement.getElement().getStyle(), height);
		return hasElement;
	}

	public static Style setHeight(Style style, @Nullable String height) {
		if (height != null) {
			style.set("height", height);
		} else {
			style.remove("height");
		}
		return style;
	}

	public static HasElement setMargin(HasElement hasElement, @Nullable String margin) {
		setMargin(hasElement.getElement().getStyle(), margin);
		return hasElement;
	}

	public static Style setMargin(Style style, @Nullable String margin) {
		if (margin != null) {
			style.set("margin", margin);
		} else {
			style.remove("margin");
		}
		return style;
	}

	/**
	 * @param hasElement non-null
	 *
	 * @return hasElement
	 *
	 * @since 1.0.3
	 */
	public static HasElement setOverflowAuto(HasElement hasElement) {
		setOverflowAuto(hasElement.getElement().getStyle());
		return hasElement;
	}

	/**
	 * @param style non-null
	 *
	 * @return style
	 *
	 * @since 1.0.3
	 */
	public static Style setOverflowAuto(Style style) {
		style.set("overflow", "auto");
		return style;
	}

	public static HasElement setOverflowHidden(HasElement hasElement) {
		setOverflowHidden(hasElement.getElement().getStyle());
		return hasElement;
	}

	public static Style setOverflowHidden(Style style) {
		style.set("overflow", "hidden");
		return style;
	}

	public static HasElement setPadding(HasElement hasElement, @Nullable String padding) {
		setPadding(hasElement.getElement().getStyle(), padding);
		return hasElement;
	}

	public static Style setPadding(Style style, @Nullable String padding) {
		if (padding != null) {
			style.set("padding", padding);
		} else {
			style.remove("padding");
		}
		return style;
	}

	/**
	 * @param hasElement non-null
	 * @param textAlign  nullable
	 *
	 * @return hasElement
	 *
	 * @since 1.0.4
	 */
	public static HasElement setTextAlign(HasElement hasElement, @Nullable String textAlign) {
		setTextAlign(hasElement.getElement().getStyle(), textAlign);
		return hasElement;
	}

	/**
	 * @param style     non-null
	 * @param textAlign nullable
	 *
	 * @return style
	 *
	 * @since 1.0.4
	 */
	public static Style setTextAlign(Style style, @Nullable String textAlign) {
		if (textAlign != null) {
			style.set("text-align", textAlign);
		} else {
			style.remove("text-align");
		}
		return style;
	}

	public static HasElement setWidth(HasElement hasElement, @Nullable String width) {
		setWidth(hasElement.getElement().getStyle(), width);
		return hasElement;
	}

	public static Style setWidth(Style style, @Nullable String width) {
		if (width != null) {
			style.set("width", width);
		} else {
			style.remove("width");
		}
		return style;
	}
}
