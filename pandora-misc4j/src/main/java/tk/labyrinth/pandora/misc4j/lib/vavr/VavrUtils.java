package tk.labyrinth.pandora.misc4j.lib.vavr;

import io.vavr.collection.List;

import java.util.function.Function;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class VavrUtils {

	private static <T> List<T> flattenRecursively(T element, Function<T, List<T>> flatMapFunction) {
		return flatMapFunction.apply(element)
				.flatMap(child -> flattenRecursively(child, flatMapFunction))
				.prepend(element);
	}

	/**
	 * @param list            non-null
	 * @param flatMapFunction non-null
	 * @param <T>             Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.0
	 */
	public static <T> List<T> flattenRecursively(List<T> list, Function<T, List<T>> flatMapFunction) {
		return list.flatMap(element -> flattenRecursively(element, flatMapFunction));
	}
}
