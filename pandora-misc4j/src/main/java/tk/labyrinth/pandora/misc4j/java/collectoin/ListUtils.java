package tk.labyrinth.pandora.misc4j.java.collectoin;

import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.misc4j.lang.Accessor;
import tk.labyrinth.pandora.misc4j.lang.SimpleAccessor;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.1.0
 */
public class ListUtils {

	/**
	 * @param list    non-null
	 * @param element nullable
	 * @param <T>     Type
	 *
	 * @return non-null
	 *
	 * @since 1.1.0
	 */
	public static <T> List<T> append(List<T> list, @Nullable T element) {
		return Stream
				.concat(
						list.stream(),
						Stream.of(element))
				.collect(Collectors.toList());
	}

	/**
	 * @param first  nullable
	 * @param second nullable
	 * @param <T>    Type
	 *
	 * @return non-empty
	 *
	 * @since 1.0.4
	 */
	public static <T> List<T> collect(@Nullable T first, Stream<T> second) {
		return StreamUtils.concat(first, second).collect(Collectors.toList());
	}

	/**
	 * @param stream nullable
	 * @param <E>    Element
	 *
	 * @return nullable
	 *
	 * @since 1.0.2
	 */
	@Nullable
	public static <E> List<E> collectNullable(@Nullable Stream<E> stream) {
		return stream != null ? stream.collect(Collectors.toList()) : null;
	}

	/**
	 * @param first  non-null
	 * @param second nullable
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.7
	 * @deprecated #append
	 */
	@Deprecated
	public static <T> List<T> concat(List<T> first, @Nullable T second) {
		Objects.requireNonNull(first, "first");
		//
		return flatten(first, Collections.singletonList(second));
	}

	/**
	 * @param first  nullable
	 * @param second non-null
	 * @param <T>    Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.7
	 * @deprecated #prepend
	 */
	@Deprecated
	public static <T> List<T> concat(@Nullable T first, List<T> second) {
		Objects.requireNonNull(second, "second");
		//
		return flatten(Collections.singletonList(first), second);
	}

	/**
	 * <a href="https://kotlinlang.org/docs/reference/collection-transformations.html#flattening">https://kotlinlang.org/docs/reference/collection-transformations.html#flattening</a>
	 *
	 * @param lists non-null
	 * @param <T>   Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.6
	 */
	// FIXME: Probably rename to concat.
	@SafeVarargs
	public static <T> List<T> flatten(List<T>... lists) {
		return Stream.of(lists).flatMap(List::stream).collect(Collectors.toList());
	}

	/**
	 * <a href="https://kotlinlang.org/docs/reference/collection-transformations.html#flattening">https://kotlinlang.org/docs/reference/collection-transformations.html#flattening</a>
	 *
	 * @param listOfLists non-null
	 * @param <T>         Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.9
	 */
	public static <T> List<T> flatten(List<List<T>> listOfLists) {
		return listOfLists.stream()
				.flatMap(List::stream)
				.collect(Collectors.toList());
	}

	/**
	 * @param iterator non-null
	 * @param <T>      Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static <T> List<T> from(Iterator<T> iterator) {
		Objects.requireNonNull(iterator, "iterator");
		//
		List<T> result = new ArrayList<>();
		iterator.forEachRemaining(result::add);
		return result;
	}

	/**
	 * @param enumeration non-null
	 * @param <T>         Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.5
	 */
	public static <T> List<T> from(Enumeration<T> enumeration) {
		Objects.requireNonNull(enumeration, "enumeration");
		//
		return from(IteratorUtils.from(enumeration));
	}

	/**
	 * @param stream nullable
	 * @param <T>    Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.8
	 */
	@Nullable
	public static <T> List<T> fromNullable(@Nullable Stream<T> stream) {
		return stream != null ? stream.collect(Collectors.toList()) : null;
	}

	/**
	 * @param vararg nullable
	 * @param <T>    Type
	 *
	 * @return nullable
	 *
	 * @since 1.0.8
	 */
	@Nullable
	@SafeVarargs
	public static <T> List<T> fromNullable(@Nullable T... vararg) {
		return vararg != null ? List.of(vararg) : null;
	}

	/**
	 * @param list        non-null
	 * @param mapFunction non-null, never receives nulls
	 * @param <T>         Type
	 * @param <R>         Result
	 *
	 * @return non-null
	 *
	 * @since 1.0.4
	 */
	public static <T, R> List<R> mapNonnull(List<T> list, Function<T, R> mapFunction) {
		return mapNullable(list, element -> element != null ? mapFunction.apply(element) : null);
	}

	/**
	 * Returns List consisting of results of applying mapFunction to elements of provided list.
	 *
	 * @param list        non-null
	 * @param mapFunction non-null, may receive nulls
	 * @param <T>         Type
	 * @param <R>         Result
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static <T, R> List<R> mapNullable(List<T> list, Function<T, R> mapFunction) {
		return list.stream().map(mapFunction).collect(Collectors.toList());
	}

	/**
	 * Returns List consisting of results of applying {@link Object#toString()} to elements of provided list.
	 *
	 * @param list non-null
	 *
	 * @return non-null
	 *
	 * @since 1.0.3
	 */
	public static List<String> mapToString(List<?> list) {
		return mapNullable(list, Objects::toString);
	}

	/**
	 * @param list    non-null
	 * @param element nullable
	 * @param <T>     Type
	 *
	 * @return non-null
	 *
	 * @since 1.1.0
	 */
	public static <T> List<T> prepend(List<T> list, @Nullable T element) {
		return Stream
				.concat(
						Stream.of(element),
						list.stream())
				.collect(Collectors.toList());
	}

	/**
	 * @param list    non-null
	 * @param element any
	 * @param <E>     Element
	 *
	 * @return pair of result list and flag, indicating whether element was removed
	 *
	 * @since 1.1.0
	 */
	public static <E> Pair<List<E>, Boolean> removeFirstEqual(List<E> list, @Nullable E element) {
		val newList = removeFirstMatching(list, innerElement -> Objects.equals(innerElement, element)).getLeft();
		return Pair.of(newList, newList.size() != list.size());
	}

	/**
	 * @param list           non-null
	 * @param matchPredicate non-null
	 * @param <E>            Element
	 *
	 * @return pair of result list and removed element, where null may mean either element was not removed or was null
	 *
	 * @since 1.1.0
	 */
	public static <E> Pair<List<E>, E> removeFirstMatching(List<E> list, Predicate<E> matchPredicate) {
		Accessor<E> accessor = new SimpleAccessor<>(null);
		return Pair.of(
				list.stream()
						.filter(element -> {
							boolean result;
							if (matchPredicate.test(element)) {
								boolean has = accessor.has();
								if (!has) {
									accessor.set(element);
									result = false;
								} else {
									result = true;
								}
							} else {
								result = true;
							}
							return result;
						})
						.collect(Collectors.toList()),
				accessor.get());
	}

	/**
	 * @param element nullable
	 * @param count   number of times to repeat
	 * @param <T>     Type
	 *
	 * @return non-null
	 *
	 * @since 1.0.6
	 */
	public static <T> List<T> repeat(@Nullable T element, int count) {
		return IntStream.range(0, count).mapToObj(index -> element).collect(Collectors.toList());
	}
}
