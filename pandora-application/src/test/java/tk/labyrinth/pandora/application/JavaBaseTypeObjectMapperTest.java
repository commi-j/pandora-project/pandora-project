package tk.labyrinth.pandora.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JavaBaseTypeObjectMapperTest {

	/**
	 * Value with which they are not failed within Gitlab CI.
	 */
	public static final int LOW_CONVERSION_TIME = ObjectModelObjectMapperTest.LOW_CONVERSION_TIME;

	private List<GenericObject> genericObjects;

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	private List<JavaBaseType> javaBaseTypes;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeAll
	void beforeAll() {
		javaBaseTypes = javaBaseTypeRegistry.getJavaBaseTypeEntries();
		//
		Assertions.assertEquals(134, javaBaseTypes.size());
		//
		genericObjects = objectMapper.convertValue(
				javaBaseTypes,
				objectMapper.getTypeFactory().constructParametricType(List.class, GenericObject.class));
	}

	@Test
	void testConvertValueDurationForGenericObjectListToJavaBaseTypeList() {
		long startedAt = System.currentTimeMillis();
		objectMapper.convertValue(
				genericObjects,
				objectMapper.getTypeFactory().constructParametricType(List.class, JavaBaseType.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}

	@Test
	void testConvertValueDurationForGenericObjectToJavaBaseType() {
		long startedAt = System.currentTimeMillis();
		genericObjects.forEach(genericObject -> objectMapper.convertValue(genericObject, JavaBaseType.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}

	@Test
	void testConvertValueDurationForJavaBaseTypeListToGenericObjectList() {
		long startedAt = System.currentTimeMillis();
		objectMapper.convertValue(
				javaBaseTypes,
				objectMapper.getTypeFactory().constructParametricType(List.class, GenericObject.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}

	@Test
	void testConvertValueDurationForJavaBaseTypeToGenericObject() {
		long startedAt = System.currentTimeMillis();
		javaBaseTypes.forEach(javaBaseType -> objectMapper.convertValue(javaBaseType, GenericObject.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}
}
