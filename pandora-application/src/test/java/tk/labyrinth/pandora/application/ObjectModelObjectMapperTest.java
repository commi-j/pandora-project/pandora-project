package tk.labyrinth.pandora.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistryOld;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ObjectModelObjectMapperTest {

	/**
	 * Value with which they are not failed within Gitlab CI.
	 */
	public static final int LOW_CONVERSION_TIME = 30;

	private List<GenericObject> genericObjects;

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ObjectModelRegistryOld objectModelRegistry;

	private List<ObjectModel> objectModels;

	@BeforeAll
	void beforeAll() {
		objectModels = javaBaseTypeRegistry.getJavaBaseTypeEntries()
				.filter(entry -> entry.hasHint(JavaBaseTypeHint.MODEL))
				.map(entry -> objectModelRegistry.findOrCreateModelForJavaClass(entry.resolveClass()));
		//
		Assertions.assertEquals(33, objectModels.size());
		//
		genericObjects = objectMapper.convertValue(
				objectModels,
				objectMapper.getTypeFactory().constructParametricType(List.class, GenericObject.class));
	}

	@Test
	void testConvertValueDurationForGenericObjectListToObjectModelList() {
		long startedAt = System.currentTimeMillis();
		objectMapper.convertValue(
				genericObjects,
				objectMapper.getTypeFactory().constructParametricType(List.class, ObjectModel.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}

	@Test
	void testConvertValueDurationForGenericObjectToObjectModel() {
		long startedAt = System.currentTimeMillis();
		genericObjects.forEach(genericObject -> objectMapper.convertValue(genericObject, ObjectModel.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}

	@Test
	void testConvertValueDurationForObjectModelListToGenericObjectList() {
		long startedAt = System.currentTimeMillis();
		objectMapper.convertValue(
				objectModels,
				objectMapper.getTypeFactory().constructParametricType(List.class, GenericObject.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}

	@Test
	void testConvertValueDurationForObjectModelToGenericObject() {
		long startedAt = System.currentTimeMillis();
		objectModels.forEach(objectModel -> objectMapper.convertValue(objectModel, GenericObject.class));
		long duration = System.currentTimeMillis() - startedAt;
		//
		ContribAssertions.assertWithinRange(0, LOW_CONVERSION_TIME, duration);
	}
}
