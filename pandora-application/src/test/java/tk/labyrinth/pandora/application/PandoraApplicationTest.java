package tk.labyrinth.pandora.application;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;

import java.time.Duration;

class PandoraApplicationTest {

	// TODO: Query some UI bean to ensure it is initialized without errors.
	@Test
	void testRunApplication() {
		long beforeRun = System.currentTimeMillis();
		//
		SpringApplication.run(PandoraApplication.class);
		//
		long afterRun = System.currentTimeMillis();
		//
		Duration runDuration = Duration.ofMillis(afterRun - beforeRun);
		//
		Assertions.assertThat(runDuration).isLessThan(Duration.ofSeconds(20));
	}
}
