package tk.labyrinth.pandora.ui.domain.route;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import tk.labyrinth.pandora.application.PandoraApplication;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.ui.domain.page.SlugPandoraPageReference;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;
import tk.labyrinth.pandora.ui.domain.pageroute.PathPageRouteReference;

@ContextConfiguration(classes = PandoraApplication.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VaadinRoutesSyncingPandoraRouteChangeListenerTest {

	@SmartAutowired
	private TypedObjectManipulator<PageRoute> pageRouteManipulator;

	@Autowired
	private VaadinRoutesSyncingPandoraRouteChangeListener vaadinRoutesSyncingPandoraRouteChangeListener;

	/**
	 * Ensure that reregistering a route works well. There was an issue that Vaadin does not properly handle
	 * removal of route with parameter when we specify only basic method.
	 */
	@Test
	void testOnObjectChange() {
		String path = "foo/bar";
		//
		pageRouteManipulator.createWithAdjustment(
				PageRoute.builder()
						.pageReference(SlugPandoraPageReference.of("pandora-generic-object-table"))
						.path(path)
						.build(),
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		//
		pageRouteManipulator.delete(PathPageRouteReference.of(path));
		//
		pageRouteManipulator.createWithAdjustment(
				PageRoute.builder()
						.pageReference(SlugPandoraPageReference.of("pandora-generic-object-table"))
						.path(path)
						.build(),
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
	}
}
