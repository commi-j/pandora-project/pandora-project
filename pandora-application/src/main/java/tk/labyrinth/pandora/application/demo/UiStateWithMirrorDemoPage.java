package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.router.Route;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.functionalcomponents.vaadin.TextFieldRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateMirror;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "ui-state-with-mirror", layout = DemoRootLayout.class)
@Slf4j
public class UiStateWithMirrorDemoPage extends Composite<Component> {

	@UiStateMirror
	private final Observable<State> stateObservable = Observable.withInitialValue(State.builder()
			.first("f")
			.second("s")
			.build());

	private Component render(State properties, Consumer<State> sink) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			layout.add(TextFieldRenderer.render(builder -> builder
					.label("First")
					.onValueChange(nextValue -> sink.accept(properties.withFirst(nextValue)))
					.value(properties.getFirst())
					.build()));
			layout.add(TextFieldRenderer.render(builder -> builder
					.label("Second")
					.onValueChange(nextValue -> sink.accept(properties.withSecond(nextValue)))
					.value(properties.getSecond())
					.build()));
		}
		return layout;
	}

	@Override
	protected Component initContent() {
		return FunctionalComponent.createWithExternalObservable(stateObservable, this::render);
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@UiStateAttribute
		String first;

		@UiStateAttribute(queryAttributeName = "sec")
		String second;
	}
}
