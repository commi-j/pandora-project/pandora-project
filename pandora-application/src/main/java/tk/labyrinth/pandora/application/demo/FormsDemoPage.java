package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.form.FormsTool;

@RequiredArgsConstructor
@Route(value = "forms", layout = DemoRootLayout.class)
@Slf4j
public class FormsDemoPage extends CssVerticalLayout {

	private final FormsTool formsTool;

	@SmartAutowired
	private TypedObjectSearcher<DatatypeBase> datatypeBaseSearcher;

	@PostConstruct
	private void postConstruct() {
		List<String> initialValue = List.of("foo", "bar");
		//
		add(ButtonRenderer.render(builder -> builder
				.onClick(event -> formsTool.showDialog(datatypeBaseSearcher.getSingle(
						JavaBaseTypeUtils.createDatatypeBaseReference(ClassSignature.class))))
				.text("Show In Dialog")
				.build()));
	}
}
