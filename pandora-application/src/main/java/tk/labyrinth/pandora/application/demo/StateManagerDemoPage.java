package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponent2;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.FunctionalObservable;
import tk.labyrinth.pandora.ui.domain.state2.UiStateManager;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Optional;

@RequiredArgsConstructor
@Route(value = "state-manager", layout = DemoRootLayout.class)
@Slf4j
public class StateManagerDemoPage extends CssVerticalLayout {

	private final UiStateManager uiStateManager;

	@PostConstruct
	private void postConstruct() {
		{
			addClassName(PandoraStyles.LAYOUT);
			//
			setAlignItems(AlignItems.FLEX_START);
		}
		{
			{
				TextField inputTextField = TextFieldRenderer.render(builder -> builder
						.label("Input")
						.onValueChange(nextValue -> uiStateManager.setAttribute(
								"demo",
								Optional.ofNullable(StringUtils.emptyToNull(nextValue))
										.map(SimpleValueWrapper::of)
										.orElse(null)))
						.build());
				//
				uiStateManager.getStateFlux()
						.log("input")
						.subscribe(next ->
								inputTextField.setValue(StringUtils.nullToEmpty(next.findAttributeValueAsString("demo"))));
				//
				add(inputTextField);
			}
			{
				TextField outputTextField = TextFieldRenderer.render(builder -> builder
						.label("Output")
						.readOnly(true)
						.build());
				//
				uiStateManager.getStateFlux()
						.log("output")
						.subscribe(next ->
								outputTextField.setValue(StringUtils.nullToEmpty(next.findAttributeValueAsString("demo"))));
				//
				add(outputTextField);
			}
			{
				add(FunctionalComponent2.of(
						FunctionalObservable.of(
								uiStateManager.getStateFlux()
										.log("value")
										.map(next -> Optional.ofNullable(next.findAttributeValueAsString("demo"))),
								FunctionUtils::throwUnreachableStateException),
						(next, sink) -> {
							Span span = FunctionalComponents.createComponent(Span::new);
							//
							span.setText("Val: %s".formatted(next));
							//
							return span;
						}));
			}
		}
	}
}
