package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

@Route(value = "functional-components", layout = DemoRootLayout.class)
@Slf4j
public class FunctionalComponentsDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			add(FunctionalComponent.create(
					Pair.of("left", "right"),
					(properties, sink) -> {
						CssHorizontalLayout layout = new CssHorizontalLayout();
						{
							layout.addClassNames(PandoraStyles.LAYOUT);
						}
						{
							layout.add(StringBoxRenderer.render(builder -> builder
									.label("Left")
									.onValueChange(nextValue -> sink.accept(Pair.of(nextValue, properties.getRight())))
									.value(properties.getLeft())
									.build()));
							layout.add(StringBoxRenderer.render(builder -> builder
									.label("Right")
									.onValueChange(nextValue -> sink.accept(Pair.of(properties.getLeft(), nextValue)))
									.value(properties.getRight())
									.build()));
						}
						return layout;
					}));
		}
	}
}
