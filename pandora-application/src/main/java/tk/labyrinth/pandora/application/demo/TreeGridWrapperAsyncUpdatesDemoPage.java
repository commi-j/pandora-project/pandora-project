package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.functionalcomponents.advanced.TreeGridWrapper;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ProgressBarRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vavr.VavrUtils;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.function.UnaryOperator;

@Route(value = "tree-grid-wrapper-async-updates", layout = DemoRootLayout.class)
@Slf4j
public class TreeGridWrapperAsyncUpdatesDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		Observable<List<Item>> itemsObservable = Observable.withInitialValue(List.range(0, 2)
				.map(index -> Item.builder()
						.children(List.range(0, 2).map(childIndex -> Item.builder()
								.children(List.empty())
								.name(UUID.randomUUID().toString())
								.running(false)
								.status(null)
								.build()))
						.name(UUID.randomUUID().toString())
						.running(false)
						.status(null)
						.build()));
		//
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout toolbar = new CssHorizontalLayout();
				{
					toolbar.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					toolbar.addStretch();
					//
					toolbar.add(ButtonRenderer.render(builder -> builder
							.onClick(event -> itemsObservable.set(itemsObservable.get()
									.map(item -> item.updateTree(itemToUpdate -> itemToUpdate.toBuilder()
											.running(false)
											.status(null)
											.build()))))
							.text("Reset")
							.build()));
					toolbar.add(ButtonRenderer.render(builder -> builder
							.onClick(event -> {
								List<Item> updatingItems = itemsObservable.get()
										.map(item -> item.updateTree(itemToUpdate -> itemToUpdate.toBuilder()
												.running(true)
												.status(null)
												.build()));
								//
								itemsObservable.set(updatingItems);
								//
								processRun(
										VavrUtils.flattenRecursively(updatingItems, Item::getChildren),
										itemsObservable);
							})
							.text("Run")
							.build()));
				}
				add(toolbar);
			}
			{
				TreeGridWrapper<Item> grid = new TreeGridWrapper<>(
						Item::getChildren,
						item -> item.withChildren(List.empty()),
						Item::getName);
				{
					grid.getContent().addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
				}
				{
					grid
							.addHierarchyColumn(Item::getName)
							.setHeader("Name");
					grid
							.addComponentColumn(item -> {
								Component result;
								{
									if (item.getStatus() != null) {
										result = new Span(item.getStatus());
									} else {
										if (item.getRunning()) {
											result = ProgressBarRenderer.render(builder -> builder
													.indeterminate(true)
													.build());
										} else {
											result = new Div();
										}
									}
								}
								return result;
							})
							.setHeader("Status");
				}
				{
					itemsObservable.subscribe(grid::setItems);
					//
					grid.expandAllItems();
				}
				add(grid);
			}
		}
	}

	private static void processRun(List<Item> items, Observable<List<Item>> itemsObservable) {
		ForkJoinPool.commonPool().execute(() -> items.forEach(item -> {
			ThreadUtils.sleep(500);
			//
			itemsObservable.update(current -> current.map(currentItem -> currentItem.update(
					item.getName(),
					itemToUpdate -> itemToUpdate.toBuilder()
							.running(false)
							.status(Math.random() < 0.5 ? "success" : "failure")
							.build())));
		}));
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Item {

		@NonNull
		List<Item> children;

		@NonNull
		String name;

		@NonNull
		Boolean running;

		@Nullable
		String status;

		public Item update(String name, UnaryOperator<Item> updateOperator) {
			return Objects.equals(name, this.name)
					? updateOperator.apply(this)
					: withChildren(children.map(child -> child.update(name, updateOperator)));
		}

		public Item updateRecursively(UnaryOperator<Item> updateOperator) {
			return updateOperator.apply(withChildren(children.map(child -> child.updateRecursively(updateOperator))));
		}

		public Item updateTree(UnaryOperator<Item> updateOperator) {
			return updateOperator.apply(withChildren(children.map(child -> child.updateTree(updateOperator))));
		}
	}
}
