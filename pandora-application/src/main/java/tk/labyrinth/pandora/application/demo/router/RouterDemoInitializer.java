package tk.labyrinth.pandora.application.demo.router;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.ui.domain.page.PandoraPage;
import tk.labyrinth.pandora.ui.domain.page.PandoraPageVaadinUtils;
import tk.labyrinth.pandora.ui.domain.page.SlugPandoraPageReference;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;

@Bean
public class RouterDemoInitializer extends GenericObjectsContributingInitializer {

	@Override
	protected List<GenericObject> contributeObjects() {
		return List
				.of(
						PandoraPage.builder()
								.handle(PandoraPageVaadinUtils.createVaadinComponentClassPageHandler(RouterDemoPage.class))
								.parametersModel(List.of(
										// TODO
								))
								.slug("router-demo")
								.build(),
						PageRoute.builder()
								.pageReference(SlugPandoraPageReference.of("router-demo"))
								.path("/demo/router-demo")
								.build())
				.map(this::convertAndAddHardcoded);
	}
}
