package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.control.Option;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.simple.SuggestBox;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;

@Route(value = "suggest-box", layout = DemoRootLayout.class)
@Slf4j
public class SuggestBoxDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setAlignItems(AlignItems.FLEX_START);
		}
		{
			Observable<Option<Integer>> state = Observable.withInitialValue(Option.of(10));
			CssHorizontalLayout layout = new CssHorizontalLayout();
			{
				layout.addClassNames(PandoraStyles.LAYOUT);
				//
				layout.setAlignItems(AlignItems.BASELINE);
			}
			{
				SuggestBox<Integer> integerBoxWithInitialValue = new SuggestBox<>();
				state.getFlux().subscribe(nextState -> integerBoxWithInitialValue.render(
						SuggestBox.Properties.<Integer>builder()
								.label("Integer box with initial value")
								.onValueChange(nextValue -> state.set(Option.of(nextValue)))
								.suggestFunction(text -> List.of(0, 10, 11, 12, 100).filter(item -> item.toString().contains(text)))
								.value(nextState.getOrNull())
								.build()));
				layout.add(integerBoxWithInitialValue.asVaadinComponent());
			}
			{
				layout.add(new Button("Set value to 13", event -> state.set(Option.of(13))));
			}
			add(layout);
		}
		{
			Observable<Option<Integer>> state = Observable.withInitialValue(Option.of(10));
			SuggestBox<Integer> integerBoxWithRenderer = new SuggestBox<>();
			state.getFlux().subscribe(nextState -> integerBoxWithRenderer.render(SuggestBox.Properties.<Integer>builder()
					.label("Integer box with renderer")
					.onValueChange(nextValue -> state.set(Option.of(nextValue)))
					.suggestFunction(text -> List.of(0, 10, 11, 12, 100).filter(item -> item.toString().contains(text)))
					.toStringRenderFunction(item -> "value_" + Objects.requireNonNull(item))
					.value(nextState.getOrNull())
					.build()));
			add(integerBoxWithRenderer.asVaadinComponent());
		}
		{
			Observable<Option<Integer>> state = Observable.withInitialValue(Option.of(10));
			SuggestBox<Integer> integerBoxWithRenderer = new SuggestBox<>();
			state.getFlux().subscribe(nextState -> integerBoxWithRenderer.render(SuggestBox.Properties.<Integer>builder()
					.label("Integer box with custom value support (will set -1)")
					.onTextChange(nextText -> {
						logger.info("onTextChange: {}", nextText);
						state.set(Option.of(-1));
					})
					.onValueChange(nextValue -> {
						logger.info("onValueChange: {}", nextValue);
						state.set(Option.of(nextValue));
					})
					.suggestFunction(text -> List.of(0, 10, 11, 12, 100).filter(item -> item.toString().contains(text)))
					.value(nextState.getOrNull())
					.build()));
			add(integerBoxWithRenderer.asVaadinComponent());
		}
	}
}
