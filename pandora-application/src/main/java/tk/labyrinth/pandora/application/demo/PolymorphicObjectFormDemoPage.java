package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.Route;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicRoot;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.json.JsonDisplay;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "polymorphic-object-form", layout = DemoRootLayout.class)
@Slf4j
public class PolymorphicObjectFormDemoPage extends FunctionalPage<PolymorphicObjectFormDemoPage.Parameters> {

	private final JsonDisplay jsonDisplay;

	private final ValueBoxRegistry valueBoxRegistry;

	@SmartAutowired
	private ObjectForm<Animal> animalForm;

	@Override
	protected Parameters getInitialProperties() {
		Bird bird = Bird.builder()
				.featherCount(10)
				.build();
		//
		return Parameters.builder()
				.currentValue(bird)
				.initialValue(bird)
				.build();
	}

	@Override
	protected Component render(Parameters parameters, Consumer<Parameters> sink) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			{
				SplitLayout splitLayout = new SplitLayout();
				{
					splitLayout.setSplitterPosition(60);
				}
				{
					{
						animalForm.render(ObjectForm.Properties.<Animal>builder()
								.currentValue(parameters.currentValue())
								.initialValue(parameters.initialValue())
								.onValueChange(nextValue -> sink.accept(parameters.withCurrentValue(nextValue)))
								.build());
						splitLayout.addToPrimary(animalForm.asVaadinComponent());
					}
					{
						jsonDisplay.render(JsonDisplay.Properties.builder()
								.value(parameters.currentValue())
								.build());
						splitLayout.addToSecondary(jsonDisplay.asVaadinComponent());
					}
				}
				layout.add(splitLayout);
			}
			{
				MutableValueBox<?> valueBox = valueBoxRegistry.getValueBox(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(
						PolymorphicDemoObject.class));
				//
				renderValueBox(valueBox);
				//
				layout.add(valueBox.asVaadinComponent());
			}
		}
		return layout;
	}

	private static <T> void renderValueBox(MutableValueBox<T> valueBox) {
		valueBox.render(MutableValueBox.Properties.<T>builder()
				.currentValue(null)
				.initialValue(null)
				.label("Object")
				.onValueChange(nextValue -> {
					// no-op
				})
				.build());
	}

	@Model
	@PolymorphicRoot(qualifierAttributeName = "kind")
	public interface Animal {
		// empty
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Model
	@PolymorphicLeaf(rootClass = Animal.class, qualifierAttributeValue = "bird")
	@Value
	@With
	public static class Bird implements Animal {

		Integer featherCount;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Model
	@PolymorphicLeaf(rootClass = Animal.class, qualifierAttributeName = "cat", qualifierAttributeValue = "true")
	@Value
	@With
	public static class Cat implements Animal {

		String species;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Model
	@PolymorphicLeaf(rootClass = Animal.class, qualifierAttributeValue = "fish")
	@Value
	@With
	public static class Fish implements Animal {

		Integer length;

		Integer weight;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Model
	@Value
	@With
	public static class PolymorphicDemoObject {

		@Nullable
		Animal currentValue;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Animal currentValue;

		@NonNull
		Animal initialValue;
	}
}
