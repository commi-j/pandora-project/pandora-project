package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponent2;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.FlexWrap;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.net.URL;
import java.util.Optional;

@RequiredArgsConstructor
@Route(value = "box-renderer-registry-demo", layout = DemoRootLayout.class)
public class BoxRendererRegistryDemoPage extends CssHorizontalLayout {

	private final BoxRendererRegistry boxRendererRegistry;

	@PostConstruct
	private void postConstruct() {
		{
			addClassName(PandoraStyles.LAYOUT);
			//
			setAlignItems(AlignItems.FLEX_START);
			setFlexWrap(FlexWrap.WRAP);
		}
		{
			{
				Observable<Pair<String, Integer>> stateObservable = Observable.withInitialValue(Pair.of("string", 12));
				//
				add(FunctionalComponents.createRootContainer(
						stateObservable,
						CssVerticalLayout::new,
						(state, sink, layout) -> {
							{
								layout.addClassName(PandoraStyles.LAYOUT);
							}
							{
								layout.add(boxRendererRegistry
										.getRenderer(String.class)
										.render(builder -> builder
												.currentValue(state.getLeft())
												.initialValue("string")
												.label("String")
												.onValueChange(nextValue -> sink.accept(Pair.of(nextValue, state.getRight())))
												.build())
										.asVaadinComponent());
								//
								layout.add(boxRendererRegistry
										.getRenderer(Integer.class)
										.render(builder -> builder
												.currentValue(state.getRight())
												.initialValue(12)
												.label("Integer")
												.onValueChange(nextValue -> sink.accept(Pair.of(state.getLeft(), nextValue)))
												.build())
										.asVaadinComponent());
							}
						}));
			}
			{
				add(FunctionalComponent2.of(
						Observable.withInitialValue(Optional.<Boolean>empty()),
						(next, sink) -> boxRendererRegistry
								.getRenderer(Boolean.class)
								.render(builder -> builder
										.currentValue(next.orElse(null))
										.initialValue(null)
										.label("Boolean")
										.onValueChange(nextValue -> sink.accept(Optional.ofNullable(nextValue)))
										.build())
								.asVaadinComponent()));
				//
				add(FunctionalComponent2.of(
						Observable.withInitialValue(Optional.<CodeObjectModelReference>empty()),
						(next, sink) -> boxRendererRegistry
								.getRenderer(CodeObjectModelReference.class)
								.render(builder -> builder
										.currentValue(next.orElse(null))
										.initialValue(null)
										.label("Reference")
										.onValueChange(nextValue -> sink.accept(Optional.ofNullable(nextValue)))
										.build())
								.asVaadinComponent()));
				//
				add(FunctionalComponent2.of(
						Observable.withInitialValue(Optional.<URL>empty()),
						(next, sink) -> boxRendererRegistry
								.getRenderer(URL.class)
								.render(builder -> builder
										.currentValue(next.orElse(null))
										.initialValue(null)
										.label("URL")
										.onValueChange(nextValue -> sink.accept(Optional.ofNullable(nextValue)))
										.build())
								.asVaadinComponent()));
			}
		}
	}
}
