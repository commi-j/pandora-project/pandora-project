package tk.labyrinth.pandora.application.domain.init;

import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.application.demo.AceEditorDemoPage;
import tk.labyrinth.pandora.application.demo.ActivityFeedDemoPage;
import tk.labyrinth.pandora.application.demo.AdvancedTableViewDemoPage;
import tk.labyrinth.pandora.application.demo.BoxRendererRegistryDemoPage;
import tk.labyrinth.pandora.application.demo.ConfirmationViewsDemoPage;
import tk.labyrinth.pandora.application.demo.CustomBoxDemoPage;
import tk.labyrinth.pandora.application.demo.DemoPage;
import tk.labyrinth.pandora.application.demo.FaultValueViewDemoPage;
import tk.labyrinth.pandora.application.demo.FormsDemoPage;
import tk.labyrinth.pandora.application.demo.FunctionalComponentsDemoPage;
import tk.labyrinth.pandora.application.demo.GridDemoPage;
import tk.labyrinth.pandora.application.demo.PolymorphicObjectFormDemoPage;
import tk.labyrinth.pandora.application.demo.SelectorBoxDemoPage;
import tk.labyrinth.pandora.application.demo.StateManagerDemoPage;
import tk.labyrinth.pandora.application.demo.SuggestBoxDemoPage;
import tk.labyrinth.pandora.application.demo.SuggestsForModelAttributeDemoPage;
import tk.labyrinth.pandora.application.demo.TreeGridUpdatesDemoPage;
import tk.labyrinth.pandora.application.demo.TreeGridWrapperAsyncUpdatesDemoPage;
import tk.labyrinth.pandora.application.demo.TreeGridWrapperUpdatesDemoPage;
import tk.labyrinth.pandora.application.demo.UiStateDemoPage;
import tk.labyrinth.pandora.application.demo.UiStateWithMirrorDemoPage;
import tk.labyrinth.pandora.application.demo.ValueBoxesDemoPage;
import tk.labyrinth.pandora.application.demo.ValueWrappersDemoPage;
import tk.labyrinth.pandora.application.demo.VavrListViewDemoPage;
import tk.labyrinth.pandora.application.demo.webcomponents.SpinnerElementDemoPage;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.ui.domain.applayout.AppLayoutConfigurationRegistry;
import tk.labyrinth.pandora.ui.domain.applayout.PandoraAppLayoutConfiguration;
import tk.labyrinth.pandora.ui.domain.route.PandoraRouteVaadinTool;
import tk.labyrinth.pandora.ui.domain.sidebar.PandoraSidebarItem;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarCategoryConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SlugSidebarCategoryConfigurationReference;

@Bean
@RequiredArgsConstructor
public class PandoraDemoAppLayoutConfigurationContributor implements VaadinServiceInitListener {

	private final AppLayoutConfigurationRegistry appLayoutConfigurationRegistry;

	private final PandoraRouteVaadinTool pandoraRouteVaadinTool;

	@SmartAutowired
	private TypedObjectManipulator<SidebarCategoryConfiguration> sidebarCategoryConfigurationManipulator;

	private PandoraSidebarItem leaf(String text, Class<?> target) {
		return PandoraSidebarItem.builder()
				.target(pandoraRouteVaadinTool.getPath(target))
				.text(text)
				.build();
	}

	// FIXME: We use Vaadin-bound solution because it is initialized later that PandoraInitializer.
	//  Need to find a way to make Pandora init at the end, or probably get rid of strict dependency.
	@Override
	public void serviceInit(ServiceInitEvent event) {
		{
			// FIXME: It seems that we can not manage the order in which listeners are invoked, so we make sure this one is initialized.
			pandoraRouteVaadinTool.serviceInit(event);
		}
		//
		sidebarCategoryConfigurationManipulator.createWithAdjustment(
				SidebarCategoryConfiguration.builder()
						.items(List.of(
								PandoraSidebarItem.builder()
										.text("Home")
										.target(pandoraRouteVaadinTool.getPath(DemoPage.class))
										.build(),
								PandoraSidebarItem.builder()
										.text("Pandora Components")
										.children(List.of(
												leaf("Activity Feed", ActivityFeedDemoPage.class),
												leaf("Advanced Table View", AdvancedTableViewDemoPage.class),
												leaf("Box Renderer Registry", BoxRendererRegistryDemoPage.class),
												leaf("Confirmation Views", ConfirmationViewsDemoPage.class),
												leaf("Custom Box", CustomBoxDemoPage.class),
												leaf("Fault Value View", FaultValueViewDemoPage.class),
												leaf("Forms", FormsDemoPage.class),
												leaf("Functional Components", FunctionalComponentsDemoPage.class),
												leaf("Polymorphic Object Form", PolymorphicObjectFormDemoPage.class),
												leaf("Selector Box", SelectorBoxDemoPage.class),
												leaf("Suggest Box", SuggestBoxDemoPage.class),
												leaf("Suggests For Model Attribute", SuggestsForModelAttributeDemoPage.class),
												leaf("Tree Grid Updates", TreeGridUpdatesDemoPage.class),
												leaf("Tree Grid Wrapper Async Updates", TreeGridWrapperAsyncUpdatesDemoPage.class),
												leaf("Tree Grid Wrapper Updates", TreeGridWrapperUpdatesDemoPage.class),
												leaf("UI State", UiStateDemoPage.class),
												leaf("UI State With Mirror", UiStateWithMirrorDemoPage.class),
												leaf("Value Boxes", ValueBoxesDemoPage.class),
												leaf("Value Wrappers", ValueWrappersDemoPage.class),
												leaf("Vavr List View", VavrListViewDemoPage.class)))
										.build(),
								PandoraSidebarItem.builder()
										.text("Pandora Features")
										.children(List.of(
												leaf("State Manager", StateManagerDemoPage.class)))
										.build(),
								PandoraSidebarItem.builder()
										.text("Vaadin Components")
										.children(List.of(
												leaf("Ace Editor", AceEditorDemoPage.class),
												leaf("Grid", GridDemoPage.class),
												leaf("Spinner Element", SpinnerElementDemoPage.class)))
										.build()))
						.slug("pandora-demo")
						.text("Pandora Demo")
						.build(),
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		//
		appLayoutConfigurationRegistry.setAppLayoutConfiguration(PandoraAppLayoutConfiguration.builder()
				.priority(1)
				.sidebar(SidebarConfiguration.builder()
						.categoryReferences(List.of(
								SlugSidebarCategoryConfigurationReference.of("pandora-demo"),
								SlugSidebarCategoryConfigurationReference.of("pandora-default")))
						.mode(SidebarConfiguration.SidebarMode.SLIDE)
						.build())
				.useHeader(false)
				.build());
	}
}
