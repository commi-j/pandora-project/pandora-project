package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableColumnDescriptor;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableColumnVisibility;
import tk.labyrinth.pandora.ui.component.table.AdvancedTablePageModel;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSearchResult;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSelectionModel;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableView;
import tk.labyrinth.pandora.ui.component.table.renderedobject.GenericObjectInlineRenderer;
import tk.labyrinth.pandora.ui.component.table.renderedobject.RenderedObject;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateMirror;

import java.util.Objects;

@RequiredArgsConstructor
@Route(value = "advanced-table-view", layout = DemoRootLayout.class)
@Slf4j
public class AdvancedTableViewDemoPage extends Composite<Component> {

	private final AdvancedTableView advancedTableView;

	private final GenericObjectInlineRenderer genericObjectInlineRenderer;

	@UiStateMirror
	private final Observable<State> stateObservable = Observable.withInitialValue(State.builder()
			.columnVisibilities(List.of(
					AdvancedTableColumnVisibility.from("+one"),
					AdvancedTableColumnVisibility.from("+two"),
					AdvancedTableColumnVisibility.from("-three")))
			.pageNumber(1)
			.predicate(null)
			.build());

	@Override
	protected Component initContent() {
		ObjectModel objectModel = ObjectModel.builder()
				.attributes(List.of(
						ObjectModelAttribute.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(Integer.class))
								.name("one")
								.build(),
						ObjectModelAttribute.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(Integer.class))
								.name("two")
								.build(),
						ObjectModelAttribute.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(Integer.class))
								.name("three")
								.build()))
				.code("dummy")
				.name("Dummy")
				.build();
		List<GenericObject> genericObjects = List.of(1, 2, 3).map(index -> GenericObject.of(
				GenericObjectAttribute.ofSimple("one", index.toString()),
				GenericObjectAttribute.ofSimple("two", String.valueOf(index * 2)),
				GenericObjectAttribute.ofSimple("three", String.valueOf(index * 3))));
		{
			stateObservable.subscribe((state, sink) -> advancedTableView.setParameters(
					AdvancedTableView.Parameters.builder()
							.columnDescriptors(state.columnVisibilities().map(columnVisibility ->
									AdvancedTableColumnDescriptor.builder()
											.attributeName(columnVisibility.getAttributeName())
											.displayName(columnVisibility.getAttributeName())
											.visible(columnVisibility.getVisible())
											.build()))
							//
							// FIXME: Should append hidden columns, otherwise they are lost.
							.onColumnOrderChange(nextColumnOrder -> sink.accept(state.withColumnVisibilities(
									nextColumnOrder.map(attributeName -> state.columnVisibilities()
											.find(columnVisibility -> Objects.equals(
													columnVisibility.getAttributeName(),
													attributeName))
											.get()))))
							.onColumnVibisibilyChange(nextColumnVisibility -> sink.accept(state.withColumnVisibilities(
									state.columnVisibilities().replace(
											state.columnVisibilities()
													.find(columnVisibility -> Objects.equals(
															columnVisibility.getAttributeName(),
															nextColumnVisibility.getAttributeName()))
													.get(),
											nextColumnVisibility))))
							.pageModel(AdvancedTablePageModel.builder()
									.onSelectedPageIndexChange(nextSelectedPageIndex ->
											sink.accept(state.withPageNumber(nextSelectedPageIndex + 1)))
									.selectedPageIndex(state.pageNumber() - 1)
									.build())
							.searchFunction(searchParameters -> {
								List<RenderedObject> renderedObjects = genericObjects.map(genericObject ->
										genericObjectInlineRenderer.render(objectModel, genericObject));
								//
								return AdvancedTableSearchResult.builder()
										.objectsTry(Try.success(renderedObjects))
										.pageCount(1)
										.build();
							})
							.selectionModel(AdvancedTableSelectionModel.builder()
									.onPredicateChange(nextPredicate -> sink.accept(state.withPredicate(nextPredicate)))
									.predicate(state.predicate())
									.build())
							.build()));
		}
		return advancedTableView;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
		@UiStateAttribute(queryAttributeName = "columns")
		List<AdvancedTableColumnVisibility> columnVisibilities;

		@NonNull
		@UiStateAttribute(queryAttributeName = "page")
		Integer pageNumber;

		@Nullable
		@UiStateAttribute
		Predicate predicate;
	}
}
