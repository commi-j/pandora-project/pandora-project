package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.Route;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.core.change.ChangeStatus;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRun;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.stores.rootobject.UidReference;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "value-boxes", layout = DemoRootLayout.class)
@Slf4j
public class ValueBoxesDemoPage extends FunctionalPage<ValueBoxesDemoPage.Parameters> {
	// TODO: This page is to show how to use typed Java boxes (acquire, update).

	private final ValueBoxRegistry valueBoxRegistry;

	@Override
	protected Parameters getInitialProperties() {
		return Parameters.builder()
				.build();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Component render(Parameters parameters, Consumer<Parameters> sink) {
		CssGridLayout layout = new CssGridLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setGridTemplateColumns("1fr 1fr 1fr 1fr");
		}
		{
			{
				MutableValueBox<ChangeStatus> enumValueBox = valueBoxRegistry.getValueBox(ChangeStatus.class);
				//
				enumValueBox.render(builder -> builder
						.currentValue(parameters.enumValue())
						.initialValue(parameters.enumValue())
						.label("Enum Box (ChangeStatus)")
						.onValueChange(nextValue -> sink.accept(parameters.withEnumValue(nextValue)))
						.build());
				//
				layout.add(enumValueBox.asVaadinComponent());
			}
			{
				MutableValueBox<Pair<String, Integer>> simplePairValueBox = valueBoxRegistry.getValueBoxInferred(
						TypeUtils.parameterize(Pair.class, String.class, Integer.class));
				//
				simplePairValueBox.render(builder -> builder
						.currentValue(parameters.simplePairValue())
						.initialValue(parameters.simplePairValue())
						.label("Simple Pair Box (Pair<String,Integer>)")
						.onValueChange(nextValue -> sink.accept(parameters.withSimplePairValue(nextValue)))
						.build());
				//
				layout.add(simplePairValueBox.asVaadinComponent());
			}
			{
				MutableValueBox<UidReference<JobRun>> referenceValueBox = (MutableValueBox<UidReference<JobRun>>)
						valueBoxRegistry.getValueBox(TypeUtils.parameterize(UidReference.class, JobRun.class));
				//
				referenceValueBox.render(builder -> builder
						.currentValue(parameters.referenceValue())
						.initialValue(parameters.referenceValue())
						.label("Reference Box (UidReference<JobRun>)")
						.onValueChange(nextValue -> sink.accept(parameters.withReferenceValue(nextValue)))
						.build());
				//
				layout.add(referenceValueBox.asVaadinComponent());
			}
		}
		{
			// FIXME: Cheap solution to track changes.
			Notification notification = new Notification();
			notification.setDuration(1000);
			notification.add(parameters.toString());
			notification.setPosition(Notification.Position.BOTTOM_CENTER);
			notification.open();
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		ChangeStatus enumValue;

		UidReference<JobRun> referenceValue;

		Pair<String, Integer> simplePairValue;
	}
}
