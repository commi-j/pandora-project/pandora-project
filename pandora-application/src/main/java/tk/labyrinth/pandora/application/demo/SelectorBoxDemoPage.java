package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.selector.SelectorBoxRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

@RequiredArgsConstructor
@Route(value = "selector-box", layout = DemoRootLayout.class)
@Slf4j
public class SelectorBoxDemoPage extends CssVerticalLayout {

	private final SelectorBoxRenderer selectorBoxRenderer;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			add(FunctionalComponent.create(
					Predicates.and(
							Predicates.equalTo("a", "b"),
							Predicates.equalTo("c", "d"),
							Predicates.or(
									Predicates.and(
											Predicates.greaterThanOrEqualTo("e", 0),
											Predicates.lessThan("e", 1)),
									Predicates.in("year", 2020, 2021))),
					(properties, sink) -> selectorBoxRenderer.render(SelectorBoxRenderer.Parameters.builder()
							.onValueChange(sink)
							.value(properties)
							.build())));
		}
	}
}
