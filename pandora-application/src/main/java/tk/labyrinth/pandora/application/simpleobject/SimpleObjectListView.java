package tk.labyrinth.pandora.application.simpleobject;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.NumberField;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.component.value.listview.ListItem;
import tk.labyrinth.pandora.ui.component.value.listview.ValueListViewBase;

@PrototypeScopedComponent
public class SimpleObjectListView extends ValueListViewBase<SimpleObject> {

	@Override
	protected void configureGrid(Grid<ListItem<SimpleObject>> grid) {
		grid
				.addComponentColumn(component(pair -> {
					NumberField result = new NumberField();
					result.setWidthFull();
					return result;
				}))
				.setFlexGrow(2);
		grid
				.addColumn(value(pair -> pair.getRight().getUuid()))
				.setFlexGrow(1);
	}

	@Override
	protected SimpleObject createNewElementInstance() {
		return SimpleObject.builder().build();
	}
}
