package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;
import de.f0rce.ace.AceEditor;
import de.f0rce.ace.enums.AceMode;
import de.f0rce.ace.enums.AceTheme;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.functionalcomponents.vaadin.SelectRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.domain.state.model.TypedUiStateBinder;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Optional;
import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "ace-editor", layout = DemoRootLayout.class)
@Slf4j
public class AceEditorDemoPage extends FunctionalPage<AceEditorDemoPage.State> {

	private final ConverterRegistry converterRegistry;

	private final TypedUiStateBinder<State> typedUiStateBinder;

	@PostConstruct
	private void postConstruct() {
		{
//			Sinks.Many<State> propertiesSink = ReactorUtils.wrap(getContent().getPropertiesFlux());
//			//
//			typedUiStateBinder.bind(
//					State.class,
//					() -> getContent().getProperties(),
//					nextUiState -> getContent().setProperties(converterRegistry.convert(nextUiState, State.class)),
//					propertiesSink.asFlux());
//			addDetachListener(event -> propertiesSink.tryEmitComplete());
		}
	}

	@Override
	protected State getInitialProperties() {
		return State.builder()
				.mode(null)
				.theme(null)
				.value(null)
				.build();
	}

	@Override
	protected Component render(State properties, Consumer<State> sink) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout header = new CssHorizontalLayout();
				//
				header.add(SelectRenderer.<AceMode>render(builder -> builder
						.items(List.of(AceMode.values()).prepend(null).asJava())
						.label("Mode")
						.onValueChange(nextValue -> sink.accept(properties.withMode(nextValue)))
						.value(properties.getMode())
						.build()));
				header.add(SelectRenderer.<AceTheme>render(builder -> builder
						.items(List.of(AceTheme.values()).prepend(null).asJava())
						.label("Theme")
						.onValueChange(nextValue -> sink.accept(properties.withTheme(nextValue)))
						.value(properties.getTheme())
						.build()));
				//
				layout.add(header);
			}
			{
				AceEditor aceEditor = new AceEditor();
				{
					Optional.ofNullable(properties.getMode()).ifPresent(aceEditor::setMode);
					Optional.ofNullable(properties.getTheme()).ifPresent(aceEditor::setTheme);
					//
					aceEditor.setValue(properties.getValue());
				}
				{
					aceEditor.addAceChangedListener(event -> sink.accept(properties.withValue(event.getValue())));
				}
				layout.add(aceEditor);
			}
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@Nullable
		@UiStateAttribute
		AceMode mode;

		@Nullable
		@UiStateAttribute
		AceTheme theme;

		@Nullable
		String value;
	}
}
