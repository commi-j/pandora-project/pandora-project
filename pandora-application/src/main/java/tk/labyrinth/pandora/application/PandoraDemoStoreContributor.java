package tk.labyrinth.pandora.application;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

@Deprecated // FIXME: Use GenericObjectsContributingInitializer
@LazyComponent
@RequiredArgsConstructor
// FIXME: Replace ApplicationRunner with Pandora initializer or object contributor.
public class PandoraDemoStoreContributor implements ApplicationRunner {

	@SmartAutowired
	private TypedObjectManipulator<StoreConfiguration> storeConfigurationManipulator;

	@SmartAutowired
	private TypedObjectManipulator<StoreRoute> storeRouteManipulator;

	@Override
	public void run(ApplicationArguments args) {
		String slug = "pandora-demo";
		//
		{
			StoreConfiguration storeConfiguration = StoreConfiguration.builder()
					.designation(StoreDesignation.COMMON)
					.kind(StoreKind.IN_MEMORY)
					.slug(slug)
					.build();
			//
			storeConfigurationManipulator.createWithAdjustment(
					storeConfiguration,
					PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		}
		{
			StoreRoute storeRoute = StoreRoute.builder()
					.distance(2)
					.slug("%s-route".formatted(slug))
					.storeConfigurationPredicate(Predicates.equalTo(StoreConfiguration.SLUG_ATTRIBUTE_NAME, slug))
					.build();
			//
			storeRouteManipulator.createWithAdjustment(storeRoute, PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		}
	}
}
