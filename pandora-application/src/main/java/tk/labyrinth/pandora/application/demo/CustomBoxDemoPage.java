package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.reference.CustomBox;
import tk.labyrinth.pandora.ui.component.reference.VeryCustomBox;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

@Route(value = "custombox", layout = DemoRootLayout.class)
@Slf4j
public class CustomBoxDemoPage extends CssVerticalLayout {

	{
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setAlignItems(AlignItems.FLEX_START);
		}
		{
			TextField textField = new TextField();
			StyleUtils.setBorder(textField, "solid black 1px");
			textField.setLabel("Text Field Label");
			{
				ContextMenu contextMenu = new ContextMenu();
				contextMenu.addItem("A");
				contextMenu.addItem("B");
				contextMenu.setTarget(textField);
				textField.setSuffixComponent(new Button(
						VaadinIcon.ARROW_DOWN.create(),
						event -> contextMenu.setVisible(true)));
			}
			add(textField);
		}
		{
			ComboBox<String> comboBox = new ComboBox<>();
			StyleUtils.setBorder(comboBox, "solid black 1px");
			comboBox.setItems("A", "B");
			comboBox.setLabel("Combo Box Label");
			{
				Span l = new Span("P");
				l.getElement().setAttribute("slot", "prefix");
				comboBox.getElement().appendChild(l.getElement());
			}
			{
				Span l = new Span("S");
				l.getElement().setAttribute("slot", "suffix");
				comboBox.getElement().appendChild(l.getElement());
			}
			add(comboBox);
		}
		{
			CustomBox<Object> customBox = new CustomBox<>();
			StyleUtils.setBorder(customBox, "solid black 1px");
			{
				VaadinIcon.OPTIONS.create();
				VaadinIcon.COGS.create();
				VaadinIcon.SLIDERS.create();
				VaadinIcon.WRENCH.create();
			}
			customBox.setLabel("Custom Box Label");
			add(customBox);
		}
		{
			VeryCustomBox veryCustomBox = new VeryCustomBox();
			StyleUtils.setBorder(veryCustomBox, "solid black 1px");
			add(veryCustomBox);
		}
	}
}
