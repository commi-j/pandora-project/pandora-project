package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataCommunicator;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.functionalcomponents.advanced.TreeGridWrapper;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ProgressBarRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;
import java.util.UUID;
import java.util.function.UnaryOperator;

@Route(value = "tree-grid-wrapper-updates", layout = DemoRootLayout.class)
@Slf4j
public class TreeGridWrapperUpdatesDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		Observable<List<Item>> itemsObservable = Observable.withInitialValue(List.range(0, 2)
				.map(index -> Item.builder()
						.children(List.range(0, 2)
								.map(childIndex -> Item.builder()
										.children(List.empty())
										.value(Math.random())
										.uid(UUID.randomUUID())
										.build()))
						.value(Math.random())
						.uid(UUID.randomUUID())
						.build()));
		//
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				TreeGridWrapper<Item> grid = new TreeGridWrapper<>(
						Item::getChildren,
						item -> item.withChildren(List.empty()),
						Item::getUid);
				{
					grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
					//
					grid.setSelectionMode(Grid.SelectionMode.NONE);
				}
				{
					grid
							.addHierarchyColumn(Item::getUid)
							.setHeader("Uid");
					grid
							.addColumn(Item::getValue)
							.setHeader("Value");
					grid
							.addComponentColumn(item -> ObjectUtils.consumeAndReturn(
									new CssVerticalLayout(),
									layout -> item.getChildren().forEach(child ->
											layout.add(new Span(child.getValue().toString())))))
							.setHeader("Children Values");
					grid
							.addComponentColumn(item -> ProgressBarRenderer.render(builder -> builder
									.indeterminate(true)
									.build()))
							.setFlexGrow(0)
							.setHeader("Animation");
					grid
							.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
									.onClick(event -> {
										itemsObservable.update(current -> current.map(currentElement ->
												currentElement.update(
														item.getUid(),
														child -> child.withValue(Math.random()))));
										//
										Notification.show(
												item.getValue().toString(),
												1000,
												Notification.Position.BOTTOM_END);
									})
									.text("Randomize")
									.build()));
				}
				{
					itemsObservable.subscribe(grid::setItems);
					//
					grid.expandAllItems();
				}
				add(grid);
			}
		}
	}

	public static <T> T getRootItem(HierarchicalDataCommunicator<T> dataCommunicator, T item) {
		T parent = dataCommunicator.getParentItem(item);
		//
		return parent != null ? getRootItem(dataCommunicator, parent) : item;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		String refreshMode;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Item {

		List<Item> children;

		UUID uid;

		Double value;

		public Item update(UUID uid, UnaryOperator<Item> updateOperator) {
			return Objects.equals(uid, this.uid)
					? updateOperator.apply(this)
					: withChildren(children.map(child -> child.update(uid, updateOperator)));
		}

		public Item updateRecursively(UnaryOperator<Item> updateOperator) {
			return updateOperator.apply(withChildren(children.map(child -> child.updateRecursively(updateOperator))));
		}

		public Item updateTree(UnaryOperator<Item> updateOperator) {
			return updateOperator.apply(withChildren(children.map(child -> child.updateTree(updateOperator))));
		}
	}
}
