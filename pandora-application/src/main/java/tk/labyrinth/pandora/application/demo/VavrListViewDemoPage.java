package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.mutable.SimpleMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.StringBox;
import tk.labyrinth.pandora.ui.component.value.list.VavrListViewRenderer;
import tk.labyrinth.pandora.ui.component.value.listview.ListItem;

@Route(value = "vavr-list-view", layout = DemoRootLayout.class)
@Slf4j
public class VavrListViewDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		List<ListItem<String>> initialValue = List.of(
				ListItem.ofNotModified("foo"),
				ListItem.ofNotModified("bar"),
				ListItem.ofNotExisting());
		//
		add(FunctionalComponent.createWithExternalObservable(
				Observable.withInitialValue(initialValue),
				(state, sink) -> VavrListViewRenderer.<String>render(builder -> builder
						.context(null)
						.elementBoxSupplier(() -> new SimpleMutableValueBox<>(new StringBox()))
						.onAddElementClick(() -> sink.accept(state.insert(
								state.size() - 1,
								ListItem.ofAdded(null))))
						.onValueChange(sink)
						.value(state)
						.build())));
	}
}
