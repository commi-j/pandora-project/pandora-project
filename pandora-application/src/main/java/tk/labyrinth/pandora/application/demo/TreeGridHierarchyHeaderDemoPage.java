package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.treegrid.TreeGrid;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.TreeGridHierarchyHeader;

public class TreeGridHierarchyHeaderDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		TreeGrid<?> grid = new TreeGrid<>();
		TreeGridHierarchyHeader hierarchyHeader = new TreeGridHierarchyHeader();
		//
		grid
				.addHierarchyColumn(i -> i)
				.setHeader(hierarchyHeader);
	}
}
