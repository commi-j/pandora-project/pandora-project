package tk.labyrinth.pandora.application.test;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.List;
import java.util.concurrent.ForkJoinPool;

@RequiredArgsConstructor
@Route("test-focus-while-manipulating-components")
@Slf4j
public class TestFocusWhileManipulatingComponentsPage extends CssVerticalLayout {

	private final CssVerticalLayout layout = new CssVerticalLayout();

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				add(new Label("All actions are performed after 2 second delay."));
			}
			{
				CssHorizontalLayout buttonsLayout = new CssHorizontalLayout();
				{
					buttonsLayout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					{
						buttonsLayout.add(new Button("Do Nothing"));
					}
					{
						Button resetButton = new Button("Reset");
						{
							resetButton.addClickListener(event -> {
								UI ui = UI.getCurrent();
								ForkJoinPool.commonPool().execute(() -> {
									ThreadUtils.sleep(2000);
									//
									ui.access(() -> {
										layout.removeAll();
										layout.add(new TextField());
									});
								});
							});
						}
						buttonsLayout.add(resetButton);
					}
					{
						Button removeAndAddButton = new Button("Remove and add");
						{
							removeAndAddButton.addClickListener(event -> {
								UI ui = UI.getCurrent();
								ForkJoinPool.commonPool().execute(() -> {
									ThreadUtils.sleep(2000);
									//
									ui.access(() -> {
										List<TextField> elements = layout.getChildren()
												.map(TextField.class::cast)
												.toList();
										//
										layout.removeAll();
										//
										layout.add(elements.toArray(TextField[]::new));
									});
								});
							});
						}
						buttonsLayout.add(removeAndAddButton);
					}
					{
						Button insertBefore = new Button("Insert Before");
						{
							insertBefore.addClickListener(event -> {
								UI ui = UI.getCurrent();
								ForkJoinPool.commonPool().execute(() -> {
									ThreadUtils.sleep(2000);
									//
									ui.access(() -> layout.getElement().insertChild(0, new TextField().getElement()));
								});
							});
						}
						buttonsLayout.add(insertBefore);
					}
				}
				add(buttonsLayout);
			}
			{
				{
					layout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					layout.add(new TextField());
				}
				add(layout);
			}
		}
	}
}
