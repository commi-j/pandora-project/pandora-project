package tk.labyrinth.pandora.application.simpleobject;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.UUID;

@Builder(toBuilder = true)
@Value
@With
public class SimpleObject {

	Integer integer;

	String string;

	UUID uuid;
}
