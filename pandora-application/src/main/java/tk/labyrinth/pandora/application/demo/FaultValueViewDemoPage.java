package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.value.view.FaultValueViewRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

@Route(value = "faultvalueview", layout = DemoRootLayout.class)
@Slf4j
public class FaultValueViewDemoPage extends CssVerticalLayout {

	{
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			Component component = FaultValueViewRenderer.render(FaultValueViewRenderer.Properties.builder()
					.fault(new RuntimeException())
					.rawValue("Very Long Raw Text")
					.build());
			StyleUtils.setWidth(component, "8em");
			add(component);
		}
	}
}
