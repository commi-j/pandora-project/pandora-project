package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.ui.domain.state.UiStateAttributeKind;
import tk.labyrinth.pandora.ui.domain.state.model.GenericUiStateBinder;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "ui-state", layout = DemoRootLayout.class)
@Slf4j
public class UiStateDemoPage extends FunctionalPage<UiStateDemoPage.State> {

	private final ConverterRegistry converterRegistry;

	private final GenericUiStateBinder genericUiStateBinder;
//	private final TypedUiStateBinder<State> typedUiStateBinder;

	@PostConstruct
	private void postConstruct() {
		{
//			Sinks.Many<State> propertiesSink = ReactorUtils.wrap(getContent().getPropertiesFlux());
//			//
//			addAttachListener(event -> typedUiStateBinder.bind(
//					State.class,
//					() -> getContent().getProperties(),
//					nextUiState -> getContent().setProperties(converterRegistry.convert(nextUiState, State.class)),
//					propertiesSink.asFlux()));
//			addDetachListener(event -> propertiesSink.tryEmitComplete());
		}
//		{
//			List<UiStateAttributeHandler> attributeHandlers = List.of(
//					attributeHandlerProvider.getObject("first", null, "first"),
//					attributeHandlerProvider.getObject("second", null, "sec"));
//			//
//			Sinks.Many<State> propertiesSink = ReactorUtils.wrap(getContent().getPropertiesFlux());
//			//
//			genericUiStateBinder.bind(
//					attributeHandlers,
//					nextUiState -> getContent().setProperties(converterRegistry.convert(nextUiState, State.class)),
//					propertiesSink.asFlux().map(nextLocalState ->
//							converterRegistry.convert(nextLocalState, GenericObject.class)));
//			addDetachListener(event -> propertiesSink.tryEmitComplete());
//		}
//		{
//			List<StateAttributeHandler> attributeHandlers = List.of(
//					attributeHandlerProvider.getObject("first", null, "first"),
//					attributeHandlerProvider.getObject("second", null, "sec"));
//			//
//			attributeHandlers.forEach(attributeHandler ->
//					uiStateHandler.registerAttributeHandler(null, attributeHandler));
//			addDetachListener(event -> attributeHandlers.forEach(uiStateHandler::unregisterAttributeHandler));
//		}
//		{
//			getContent().getPropertiesFlux()
//					.map(nextState -> converterRegistry.convert(nextState, GenericObject.class))
//					.distinctUntilChanged()
//					.subscribe(nextObject -> uiStateHandler.setAttributes(List.of(
//							Pair.of("first", nextObject.findAttributeValue("first")),
//							Pair.of("second", nextObject.findAttributeValue("second")))));
//			//
//			uiStateHandler.getStateFlux().subscribe(nextUiState -> getContent().setProperties(State.builder()
//					.first(nextUiState.findAttributeValueAsString("first"))
//					.second(nextUiState.findAttributeValueAsString("second"))
//					.build()));
//		}
	}

	@Override
	protected State getInitialProperties() {
		return State.builder()
				.first(null)
				.second(null)
				.build();
	}

	@Override
	protected Component render(State properties, Consumer<State> sink) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			layout.add(TextFieldRenderer.render(builder -> builder
					.label("First")
					.onValueChange(nextValue -> sink.accept(properties.withFirst(nextValue)))
					.value(properties.getFirst())
					.build()));
			layout.add(TextFieldRenderer.render(builder -> builder
					.label("Second")
					.onValueChange(nextValue -> sink.accept(properties.withSecond(nextValue)))
					.value(properties.getSecond())
					.build()));
		}
		return layout;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@UiStateAttribute(kind = UiStateAttributeKind.QUERY)
		String first;

		@UiStateAttribute(kind = UiStateAttributeKind.QUERY, queryAttributeName = "sec")
		String second;
	}
}
