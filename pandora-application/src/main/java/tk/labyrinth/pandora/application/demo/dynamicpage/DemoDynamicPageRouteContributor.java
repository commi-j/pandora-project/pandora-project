package tk.labyrinth.pandora.application.demo.dynamicpage;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.TypedObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.ui.domain.page.SlugPandoraPageReference;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;
import tk.labyrinth.pandora.views.domain.page.PageUtils;

@Bean
public class DemoDynamicPageRouteContributor extends TypedObjectsContributingInitializer<PageRoute> {

	@Override
	protected List<PageRoute> doContributeObjects() {
		return List.of(
				PageRoute.builder()
						.pageReference(SlugPandoraPageReference.of(PageUtils.computeSinglePageFunctionSlug(
								DemoDynamicPageRenderer.class)))
						.path("demo/foobar")
						.build(),
				PageRoute.builder()
						.pageReference(SlugPandoraPageReference.of(PageUtils.computeSinglePageFunctionSlug(
								DemoDynamicPageRenderer.class)))
						.path("demo/barfoo")
						.build());
	}

	@Override
	protected GenericObject postProcess(GenericObject genericObject) {
		return genericObject.withAddedAttribute(PandoraHardcodedConstants.ATTRIBUTE);
	}
}
