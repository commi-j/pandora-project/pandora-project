package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ProgressBarRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.valuewrapper.ListValueWrapperBoxRenderer;
import tk.labyrinth.pandora.ui.component.valuewrapper.ObjectValueWrapperBoxRenderer;
import tk.labyrinth.pandora.ui.component.valuewrapper.SimpleValueWrapperBoxRenderer;
import tk.labyrinth.pandora.ui.component.valuewrapper.ValueWrapperBoxRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Consumer;

@Route(value = "value-wrappers", layout = DemoRootLayout.class)
@Slf4j
public class ValueWrappersDemoPage extends FunctionalPage<ValueWrappersDemoPage.Properties> {

	@Override
	protected Properties getInitialProperties() {
		return Properties.builder()
				.value(SimpleValueWrapper.of("value"))
				.build();
	}

	@Override
	protected Component render(Properties properties, Consumer<Properties> sink) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setWidth("20em");
		}
		{
			layout.add(ValueWrapperBoxRenderer.render(ValueWrapperBoxRenderer.Parameters.builder()
					.label("Value Wrapper")
					.onValueChange(nextValue -> sink.accept(properties.withValue(nextValue)))
					.value(properties.getValue())
					.build()));
			//
			layout.add(ProgressBarRenderer.render(builder -> builder
					.value(0d)
					.build()));
			//
			layout.add(SimpleValueWrapperBoxRenderer.render(SimpleValueWrapperBoxRenderer.Parameters.builder()
					.label("Simple Value Wrapper")
					.onValueChange(nextValue -> sink.accept(properties.withValue(nextValue)))
					.value(properties.getValue() instanceof SimpleValueWrapper simpleValueWrapper
							? simpleValueWrapper
							: null)
					.build()));
			layout.add(ObjectValueWrapperBoxRenderer.render(ObjectValueWrapperBoxRenderer.Properties.builder()
					.label("Object Value Wrapper")
					.onValueChange(nextValue -> sink.accept(properties.withValue(nextValue)))
					.value(properties.getValue() instanceof ObjectValueWrapper objectValueWrapper
							? objectValueWrapper
							: null)
					.build()));
			layout.add(ListValueWrapperBoxRenderer.render(ListValueWrapperBoxRenderer.Properties.builder()
					.label("List Value Wrapper")
					.onValueChange(nextValue -> sink.accept(properties.withValue(nextValue)))
					.value(properties.getValue() instanceof ListValueWrapper listValueWrapper
							? listValueWrapper
							: null)
					.build()));
		}
		return layout;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		ValueWrapper value;
	}
}
