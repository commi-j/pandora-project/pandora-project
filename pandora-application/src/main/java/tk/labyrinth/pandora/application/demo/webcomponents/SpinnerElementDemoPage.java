package tk.labyrinth.pandora.application.demo.webcomponents;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.application.demo.DemoRootLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Height;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Width;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.pandora.webcomponents.SpinnerElement;

@RequiredArgsConstructor
@Route(value = "spinner-element", layout = DemoRootLayout.class)
@Slf4j
public class SpinnerElementDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setHeightFull();
		}
		{
			Grid<SpinnerElement.Tipo> grid = new Grid<>();
			{
				grid.setHeightFull();
				grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			}
			{
				grid
						.addColumn(SpinnerElement.Tipo::name);
				grid
						.addComponentColumn(item -> {
							Div div = new Div();
							{
								StyleUtils.setCssProperty(div, Height.of("4em"));
								StyleUtils.setCssProperty(div, Width.of("4em"));
							}
							{
								SpinnerElement spinnerElement = new SpinnerElement();
								//
								spinnerElement.getStyle().set("--background-color-spinner", "blue");
								//
								StyleUtils.setCssProperty(spinnerElement, Height.HUNDRED_PERCENT);
								StyleUtils.setCssProperty(spinnerElement, Width.HUNDRED_PERCENT);
								//
								spinnerElement.setTipo(item);
								spinnerElement.setVisible(true);
								//
								div.add(spinnerElement);
							}
							return div;
						});
			}
			{
				grid.setItems(SpinnerElement.Tipo.values());
			}
			add(grid);
		}
	}
}
