package tk.labyrinth.pandora.application.demo.tools;

import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.Eager;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.UiScopedComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.domain.state.rework.UiStateAttribute2;

import java.util.Optional;

@Eager
@UiScopedComponent
public class DemoUiStateBundle {

	@UiStateAttribute2
	private Observable<String> nonNullStateAttribute = Observable.withInitialValue("foo");

	@UiStateAttribute2
	private Observable<Optional<String>> nullableStateAttribute = Observable.withInitialValue(Optional.empty());
}
