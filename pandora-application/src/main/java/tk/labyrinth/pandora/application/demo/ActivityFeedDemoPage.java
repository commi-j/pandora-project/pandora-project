package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import tk.labyrinth.pandora.activities.activity.PandoraActivity;
import tk.labyrinth.pandora.activities.activity.PandoraActivityColourIndicator;
import tk.labyrinth.pandora.activities.activity.PandoraActivityFeedView;
import tk.labyrinth.pandora.activities.activity.PandoraActivityRegistry;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.jobs.model.jobrunner.DummyJobRunner;
import tk.labyrinth.pandora.jobs.tool.JobRunnerRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.domain.confirmation.ConfirmationViewTool;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

@RequiredArgsConstructor
@Route(value = "activity-feed", layout = DemoRootLayout.class)
@Slf4j
public class ActivityFeedDemoPage extends CssHorizontalLayout {

	private final ConfirmationViewTool confirmationViewTool;

	private final JobRunnerRegistry jobRunnerRegistry;

	private final PandoraActivityFeedView pandoraActivityFeedView;

	@PostConstruct
	private void postConstruct() {
		{
			CssVerticalLayout layout = new CssVerticalLayout();
			{
				layout.addClassNames(PandoraStyles.LAYOUT);
				//
				CssFlexItem.setFlexGrow(layout, 1);
				//
				layout.setAlignItems(AlignItems.BASELINE);
			}
			{
				layout.add(ButtonRenderer.render(builder -> builder
						.onClick(event -> confirmationViewTool
								.showTypedViewDialog(DummyJobRunner.INITIAL_PARAMETERS.withDuration(Duration.ofSeconds(12)))
								.subscribeAlwaysAccepted(result ->
										jobRunnerRegistry.runJob(DummyJobRunner.class, result)))
						.text("Add Job")
						.build()));
			}
			add(layout);
		}
		{
			CssFlexItem.setFlexGrow(pandoraActivityFeedView, 1);
			add(pandoraActivityFeedView);
		}
	}

	@LazyComponent
	@RequiredArgsConstructor
	public static class ActivityInitializer implements ApplicationRunner {

		private final PandoraActivityRegistry pandoraActivityRegistry;

		@Override
		public void run(ApplicationArguments args) throws Exception {
			Instant now = Instant.now();
			//
			pandoraActivityRegistry.updateActivity(PandoraActivity.builder()
					.colourIndicator(PandoraActivityColourIndicator.GREEN)
					.finishedAt(now.minusSeconds(20))
					.startedAt(now.minusSeconds(30))
					.statusText("Completed Dummy")
					.uid(UUID.randomUUID())
					.build());
			pandoraActivityRegistry.updateActivity(PandoraActivity.builder()
					.colourIndicator(PandoraActivityColourIndicator.BLUE)
					.startedAt(now.minusSeconds(10))
					.statusText("Dummy In Progress")
					.uid(UUID.randomUUID())
					.build());
		}
	}
}
