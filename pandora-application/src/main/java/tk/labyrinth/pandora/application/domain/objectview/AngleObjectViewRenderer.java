package tk.labyrinth.pandora.application.domain.objectview;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.ui.domain.objectview.RegisterObjectViewRenderer;
import tk.labyrinth.pandora.ui.domain.objectview.TypedObjectViewRenderer;

@Bean
@RegisterObjectViewRenderer("Custom")
@RequiredArgsConstructor
public class AngleObjectViewRenderer implements TypedObjectViewRenderer<Angle> {

	@Override
	public Component render(Parameters<Angle> parameters) {
		// TODO: We wanted to test @RegisterObjectViewRenderer and it works we don't actually need to implement this class.
		//  But this could be a view to edit various angle properties and calculate others based on it.
		return new Span("ANGLE_TODO");
	}
}
