package tk.labyrinth.pandora.application.simpleobject;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelFactory;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.JsonDisplayer;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectMultiView;
import tk.labyrinth.pandora.ui.component.value.box.simple.SuggestBox;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.UUID;

@RequiredArgsConstructor
@Route("simpledemo")
public class SimpleDemoPage extends CssVerticalLayout {

	private final BeanContext beanContext;

	private final JsonDisplayer jsonDisplayer;

	private final ObjectModelFactory objectModelFactory;

	private final SimpleObjectListView simpleListView;

	@PostConstruct
	private void postConstruct() {
		ObjectModel objectModel = objectModelFactory.createModelFromJavaClass(SimpleObject.class);
		//
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setHeightFull();
		}
		{
			CssHorizontalLayout formButtonsLayout = new CssHorizontalLayout();
			{
				Button createButton = new Button(
						"Create Form",
						event -> {
							GenericObject value = GenericObject.empty();
							//
							GenericObjectMultiView genericObjectMultiview = beanContext.getBeanFactory()
									.withBean(objectModel)
									.getBean(GenericObjectMultiView.class);
							genericObjectMultiview.render(GenericObjectMultiView.Properties.builder()
									.currentValue(value)
									.initialValue(value)
//									.objectModel(objectModel)
									.build());
							//
							ConfirmationViews
									.showViewDialog(genericObjectMultiview)
									.subscribeAlwaysNotAccepted(next -> {
//										jsonDisplayer.display(form.getValue());
									});
						});
				formButtonsLayout.add(createButton);
			}
			{
				Button editButton = new Button(
						"Edit Form",
						event -> {
							GenericObject value = GenericObject.of(
									GenericObjectAttribute.ofSimple("integer", "12"));
							//
							GenericObjectMultiView genericObjectMultiview = beanContext.getBeanFactory()
									.withBean(objectModel)
									.getBean(GenericObjectMultiView.class);
							genericObjectMultiview.render(GenericObjectMultiView.Properties.builder()
									.currentValue(value)
									.initialValue(value)
//									.objectModel(objectModel)
//									.onValueChange()
									.build());
							//
							ConfirmationViews
									.showViewDialog(genericObjectMultiview)
									.subscribeAlwaysNotAccepted(next -> {
//										jsonDisplayer.display(form.getValue());
									});
						});
				formButtonsLayout.add(editButton);
			}
			add(formButtonsLayout);
		}
		{
			simpleListView.setValue(List.of(
					SimpleObject.builder()
							.string("s0")
							.uuid(UUID.randomUUID())
							.build(),
					SimpleObject.builder()
							.string("s1")
							.uuid(UUID.randomUUID())
							.build()));
			//
			CssFlexItem.setFlexGrow(simpleListView.asVaadinComponent(), 1);
			add(simpleListView.asVaadinComponent());
		}
		{
			SuggestBox<String> seasonsBox = new SuggestBox<>();
			seasonsBox.render(SuggestBox.Properties.<String>builder()
					.suggestFunction(text -> List.of("WINTER", "SPRING", "SUMMER", "AUTUMN")
							.filter(item -> item == null || StringUtils.containsIgnoreCase(item, text)))
					.build());
			add(seasonsBox.asVaadinComponent());
		}
	}
}
