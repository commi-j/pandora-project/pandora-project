package tk.labyrinth.pandora.application.demo.dynamicpage;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.views.domain.page.meta.PageFunction;
import tk.labyrinth.pandora.views.domain.page.meta.PageRendererBean;

@PageRendererBean
public class DemoDynamicPageRenderer {

	@PageFunction
	public View render(Parameters parameters) {
		return null;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		Integer integer;

		String string;
	}
}
