package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.UUID;

@Route(value = "grid", layout = DemoRootLayout.class)
@Slf4j
public class GridDemoPage extends CssVerticalLayout {

	{
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			Grid<String> grid = new Grid<>();
			{
				grid
						.addColumn(item -> item)
						.setWidth("90%");
				grid
						.addColumn(item -> "FIXED")
						.setFrozenToEnd(true)
						.setWidth("20%");
			}
			{
				grid.setItems(List.range(0, 100)
						.map(index -> UUID.randomUUID().toString())
						.asJava());
			}
			add(grid);
		}
	}
}
