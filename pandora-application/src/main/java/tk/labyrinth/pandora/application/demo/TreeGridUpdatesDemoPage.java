package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataCommunicator;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import lombok.With;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ProgressBarRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.RadioButtonGroupRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.UUID;

@Route(value = "tree-grid-updates", layout = DemoRootLayout.class)
@Slf4j
public class TreeGridUpdatesDemoPage extends CssVerticalLayout {

	private final Observable<State> stateObservable = Observable.withInitialValue(State.builder()
			.refreshMode("Refresh None")
			.build());

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout toolbar = new CssHorizontalLayout();
				{
					toolbar.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					stateObservable.subscribe((state, sink) -> {
						toolbar.removeAll();
						//
						toolbar.add(RadioButtonGroupRenderer.<String>render(builder -> builder
								.items(List
										.of(
												"Refresh None",
												"Refresh Self",
												"Refresh Root",
												"Refresh Root and Children",
												"Refresh All")
										.asJava())
								.onValueChange(event -> sink.accept(state.withRefreshMode(event.getValue())))
								.value(state.getRefreshMode())
								.build()));
					});
				}
				add(toolbar);
			}
			{
				TreeGrid<Item> grid = new TreeGrid<>();
				{
					grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
					//
					grid.setSelectionMode(Grid.SelectionMode.NONE);
				}
				{
					grid
							.addHierarchyColumn(Item::getUid)
							.setHeader("Uid");
					grid
							.addColumn(Item::getValue)
							.setHeader("Value");
					grid
							.addComponentColumn(item -> ObjectUtils.consumeAndReturn(
									new CssVerticalLayout(),
									layout -> item.getChildren().forEach(child ->
											layout.add(new Span(child.getValue().toString())))))
							.setHeader("Children Values");
					grid
							.addComponentColumn(item -> ProgressBarRenderer.render(builder -> builder
									.indeterminate(true)
									.build()))
							.setFlexGrow(0)
							.setHeader("Animation");
					grid
							.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
									.onClick(event -> {
										item.setValue(Math.random());
										//
										switch (stateObservable.get().getRefreshMode()) {
											case "Refresh None" -> {
												// no-op
											}
											case "Refresh Self" -> grid.getDataProvider().refreshItem(item);
											case "Refresh Root" -> grid.getDataProvider().refreshItem(
													getRootItem(grid.getDataCommunicator(), item));
											case "Refresh Root and Children" -> grid.getDataProvider().refreshItem(
													getRootItem(grid.getDataCommunicator(), item),
													true);
											case "Refresh All" -> grid.getDataProvider().refreshAll();
											default -> throw new IllegalStateException();
										}
										//
										Notification.show(
												item.getValue().toString(),
												1000,
												Notification.Position.BOTTOM_END);
									})
									.text("Randomize")
									.build()));
				}
				{
					List<Item> items = List.range(0, 2)
							.map(index -> Item.builder()
									.children(List.range(0, 2)
											.map(childIndex -> Item.builder()
													.children(List.empty())
													.value(Math.random())
													.uid(UUID.randomUUID())
													.build()))
									.value(Math.random())
									.uid(UUID.randomUUID())
									.build());
					//
					grid.setItems(items.asJava(), item -> item.getChildren().asJava());
					//
					grid.expand(items.asJava());
				}
				add(grid);
			}
		}
	}

	public static <T> T getRootItem(HierarchicalDataCommunicator<T> dataCommunicator, T item) {
		T parent = dataCommunicator.getParentItem(item);
		//
		return parent != null ? getRootItem(dataCommunicator, parent) : item;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		String refreshMode;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@FieldDefaults(level = AccessLevel.PRIVATE)
	@Getter
	@Setter
	@With
	public static class Item {

		List<Item> children;

		UUID uid;

		Double value;
	}
}
