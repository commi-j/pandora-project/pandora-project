package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;

@RequiredArgsConstructor
@Route(value = "", layout = DemoRootLayout.class)
@Slf4j
public class DemoPage extends CssHorizontalLayout {
	// empty
}
