package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;
import tk.labyrinth.pandora.uiapplication.navbar.ProvidesNavbarComponents;
import tk.labyrinth.pandora.uiapplication.sidebar.ProvidesSidebarComponents;

import java.util.function.Supplier;

@ParentLayout(ConfigurableAppLayout.class)
@RoutePrefix("demo")
public class DemoRootLayout extends CssGridLayout implements
		ProvidesNavbarComponents,
		ProvidesSidebarComponents,
		RouterLayout {

	private Supplier<List<PandoraAttachableComponentWrapper>> navbarComponentsSupplier;

	@PostConstruct
	private void postConstruct() {
		setGridTemplateColumns(".");
		setGridTemplateRows(".");
	}

	@Override
	public List<PandoraAttachableComponentWrapper> provideNavbarComponents() {
		return List.empty();
	}

	@Override
	public List<PandoraAttachableComponentWrapper> provideSidebarComponents() {
		return List.empty();
	}
}
