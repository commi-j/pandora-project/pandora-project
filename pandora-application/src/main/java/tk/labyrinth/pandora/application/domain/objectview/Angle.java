package tk.labyrinth.pandora.application.domain.objectview;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

import java.math.BigDecimal;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel
@Value
@With
public class Angle {

	BigDecimal cosValue;

	BigDecimal degreeValue;

	BigDecimal radianValue;

	BigDecimal sinValue;
}
