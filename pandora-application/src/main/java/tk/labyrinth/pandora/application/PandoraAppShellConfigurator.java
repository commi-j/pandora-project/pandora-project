package tk.labyrinth.pandora.application;

import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.uiapplication.appshell.PandoraAppShellConfiguratorBase;

@LazyComponent
public class PandoraAppShellConfigurator implements PandoraAppShellConfiguratorBase {
	// empty
}
