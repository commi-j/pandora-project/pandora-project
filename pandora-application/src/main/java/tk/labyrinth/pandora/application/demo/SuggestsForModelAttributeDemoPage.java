package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.component.valuebox.suggest.HasSuggests;
import tk.labyrinth.pandora.ui.component.valuebox.suggest.SuggestContext;
import tk.labyrinth.pandora.ui.component.valuebox.suggest.SuggestProvider;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

@RequiredArgsConstructor
@Route(value = "suggests-for-model-attribute", layout = DemoRootLayout.class)
public class SuggestsForModelAttributeDemoPage extends CssVerticalLayout {

	private final ValueBoxRegistry valueBoxRegistry;

	@PostConstruct
	private void postConstruct() {
		Group value = Group.builder()
				.memberNames(List.of("Alice", "Bob"))
				.build();
		Observable<Group> valueObservable = Observable.withInitialValue(value);
		//
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			MutableValueBox<Group> valueBox = valueBoxRegistry.getValueBox(Group.class);
			//
			valueObservable.subscribe((next, sink) -> valueBox.render(builder -> builder
					.currentValue(next)
					.initialValue(value)
					.label("Group")
					.onValueChange(sink)
					.build()));
			//
			add(valueBox.asVaadinComponent());
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Model
	@Value
	@With
	public static class Group {

		@HasSuggests(provider = LeaderFromMembersSuggestProvider.class)
		String leaderName;

		List<String> memberNames;
	}

	@LazyComponent
	public static class LeaderFromMembersSuggestProvider implements SuggestProvider<String> {

		@Override
		public List<String> provideSuggests(SuggestContext context) {
			return context.getAncestorValue(1, Group.class).getMemberNames();
		}
	}
}
