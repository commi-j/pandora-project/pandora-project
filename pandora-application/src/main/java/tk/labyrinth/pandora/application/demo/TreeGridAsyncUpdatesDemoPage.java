package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.data.provider.hierarchy.TreeDataProvider;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ProgressBarRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;
import java.util.function.UnaryOperator;

@Slf4j
public class TreeGridAsyncUpdatesDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		Observable<List<Item>> itemsObservable = Observable.withInitialValue(List.range(0, 5)
				.map(index -> Item.builder()
						.children(List.range(0, 2).map(childIndex -> Item.builder()
								.children(List.empty())
								.name(UUID.randomUUID().toString())
								.running(false)
								.status(null)
								.build()))
						.name(UUID.randomUUID().toString())
						.running(false)
						.status(null)
						.build()));
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout toolbar = new CssHorizontalLayout();
				{
					toolbar.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					toolbar.addStretch();
					//
					toolbar.add(ButtonRenderer.render(builder -> builder
							.onClick(event -> itemsObservable.set(itemsObservable.get()
									.map(item -> item.updateTree(itemToUpdate -> itemToUpdate.toBuilder()
											.running(false)
											.status(null)
											.build()))))
							.text("Reset")
							.build()));
					toolbar.add(ButtonRenderer.render(builder -> builder
							.onClick(event -> {
								List<Item> updatingItems = itemsObservable.get()
										.map(item -> item.updateTree(itemToUpdate -> itemToUpdate.toBuilder()
												.running(true)
												.status(null)
												.build()));
								//
								itemsObservable.set(updatingItems);
								//
//								processRun(
//										VavrUtils.flattenRecursively(updatingItems, Item::getChildren),
//										itemsObservable);
							})
							.text("Run")
							.build()));
				}
				add(toolbar);
			}
			{
				TreeGrid<Item> grid = new TreeGrid<>();
				{
					grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
					//
					grid.setUniqueKeyDataGenerator("key", Item::getName);
				}
				{
					grid
							.addHierarchyColumn(Item::getName)
							.setHeader("Name");
					grid
							.addComponentColumn(item -> {
								Component result;
								{
									if (item.getStatus() != null) {
										result = new Span(item.getStatus());
									} else {
										if (item.getRunning()) {
											result = ProgressBarRenderer.render(builder -> builder
													.indeterminate(true)
													.build());
										} else {
											result = new Div();
										}
									}
								}
								return result;
							})
							.setHeader("Status");
				}
				{
					TreeData<Item> treeData = new TreeData<>();
					grid.setDataProvider(new TreeDataProvider<>(treeData));
					//
					itemsObservable.subscribe(next ->
							updateRecursivelyFromRoot(grid, treeData, next, Item::getChildren));
					//
					grid.expandRecursively(itemsObservable.get().asJava(), 0);
				}
				add(grid);
			}
		}
	}

	private static void processRun(List<Item> items, Observable<List<Item>> itemsObservable) {
		ForkJoinPool.commonPool().execute(() -> items.forEach(item -> {
			ThreadUtils.sleep(500);
			//
			itemsObservable.update(current -> current.map(currentItem -> currentItem.update(
					item.getName(),
					itemToUpdate -> itemToUpdate.toBuilder()
							.running(false)
							.status(Math.random() < 0.5 ? "success" : "failure")
							.build())));
		}));
	}

	private static <T> void updateRecursively(
			TreeGrid<T> treeGrid,
			TreeData<T> treeData,
			@Nullable T parent,
			@Nullable T currentItem,
			T nextItem,
			Function<T, List<T>> childrenFunction) {
		{
			if (!Objects.equals(currentItem, nextItem)) {
				treeData.addItem(parent, nextItem);
				//
				if (currentItem != null) {
					treeData.moveAfterSibling(nextItem, currentItem);
					//
					List.ofAll(treeData.getChildren(currentItem)).forEach(child -> treeData.setParent(child, nextItem));
					//
					treeData.removeItem(currentItem);
					//
					treeGrid.getDataProvider().refreshItem(currentItem);
				}
				//
				treeGrid.getDataProvider().refreshItem(nextItem);
			}
		}
		{
			Map<String, T> keyToChildMap = List.ofAll(treeData.getChildren(nextItem)).toMap(
					child -> treeGrid.getDataCommunicator().getKeyMapper().key(child),
					Function.identity());
			//
			childrenFunction.apply(nextItem).forEach(child -> {
				String key = treeGrid.getDataCommunicator().getKeyMapper().key(child);
				//
				updateRecursively(
						treeGrid,
						treeData,
						nextItem,
						keyToChildMap.get(key).getOrNull(),
						child,
						childrenFunction);
			});
		}
	}

	private static <T> void updateRecursivelyFromRoot(
			TreeGrid<T> treeGrid,
			TreeData<T> treeData,
			List<T> items,
			Function<T, List<T>> childrenFunction) {
		Map<String, T> keyToChildMap = List.ofAll(treeData.getRootItems()).toMap(
				child -> treeGrid.getDataCommunicator().getKeyMapper().key(child),
				Function.identity());
		//
		items.forEach(item -> {
			String key = treeGrid.getDataCommunicator().getKeyMapper().key(item);
			//
			updateRecursively(treeGrid, treeData, null, keyToChildMap.get(key).getOrNull(), item, childrenFunction);
		});
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Item {

		@NonNull
		List<Item> children;

		@NonNull
		String name;

		@NonNull
		Boolean running;

		@Nullable
		String status;

		public Item update(String name, UnaryOperator<Item> updateOperator) {
			return Objects.equals(name, this.name)
					? updateOperator.apply(this)
					: withChildren(children.map(child -> child.update(name, updateOperator)));
		}

		public Item updateRecursively(UnaryOperator<Item> updateOperator) {
			return updateOperator.apply(withChildren(children.map(child -> child.updateRecursively(updateOperator))));
		}

		public Item updateTree(UnaryOperator<Item> updateOperator) {
			return updateOperator.apply(withChildren(children.map(child -> child.updateTree(updateOperator))));
		}
	}
}
