package tk.labyrinth.pandora.application.demo.router;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;

public class RouterDemoPage extends Div {

	{
		add(new Span("ROUTER_DEMO_PAGE"));
	}
}
