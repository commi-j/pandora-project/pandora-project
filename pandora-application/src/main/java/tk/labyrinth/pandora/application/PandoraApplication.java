package tk.labyrinth.pandora.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.functions.PandoraFunctionsModule;
import tk.labyrinth.pandora.iam.PandoraIamModule;
import tk.labyrinth.pandora.routines.PandoraRoutinesModule;
import tk.labyrinth.pandora.ui.PandoraUiModule;
import tk.labyrinth.pandora.views.PandoraViewsModule;

@Import({
		PandoraFunctionsModule.class,
		PandoraIamModule.class,
		PandoraRoutinesModule.class,
		PandoraUiModule.class,
		PandoraViewsModule.class,
})
@SpringBootApplication
//@SpringBootConfiguration ?
public class PandoraApplication {

	public static void main(String... args) {
		SpringApplication.run(PandoraApplication.class, args);
	}
}
