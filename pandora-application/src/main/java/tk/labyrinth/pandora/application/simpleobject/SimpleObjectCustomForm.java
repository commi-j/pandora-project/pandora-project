package tk.labyrinth.pandora.application.simpleobject;

import com.vaadin.flow.component.Component;
import io.vavr.control.Option;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.value.box.simple.IntegerBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.StringBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.UuidBox;

import java.util.UUID;

public class SimpleObjectCustomForm implements ObjectForm<SimpleObject> {

	private final IntegerBox integerBox = new IntegerBox();

	private final CssGridLayout layout = new CssGridLayout();

	private final StringBox stringBox = new StringBox();

	private final UuidBox uuidBox = new UuidBox();

	{
		{
			layout.add(integerBox.asVaadinComponent());
		}
		{
			layout.add(stringBox.asVaadinComponent());
		}
		{
			layout.add(uuidBox.asVaadinComponent());
		}
	}

	@Override
	public Component asVaadinComponent() {
		return layout;
	}

	@Override
	public void render(Properties<SimpleObject> properties) {
		Option<SimpleObject> currentValueOption = Option.of(properties.getCurrentValue());
		//
		integerBox.render(SimpleValueBox.Properties.<Integer>builder()
				.label("integer")
				.nonEditableReasons(properties.getNonEditableReasons())
				.onValueChange(nextValue -> properties.getCurrentValue().withInteger(nextValue))
				.value(currentValueOption.map(SimpleObject::getInteger).getOrNull())
				.build());
		stringBox.render(SimpleValueBox.Properties.<String>builder()
				.label("string")
				.nonEditableReasons(properties.getNonEditableReasons())
				.onValueChange(nextValue -> properties.getCurrentValue().withString(nextValue))
				.value(currentValueOption.map(SimpleObject::getString).getOrNull())
				.build());
		uuidBox.render(SimpleValueBox.Properties.<UUID>builder()
				.label("uuid")
				.nonEditableReasons(properties.getNonEditableReasons())
				.value(currentValueOption.map(SimpleObject::getUuid).getOrNull())
				.build());
	}
}
