package tk.labyrinth.pandora.application.test;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.TaskScheduler;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import java.time.Duration;

@RequiredArgsConstructor
@Route("test-focus-on-grid-refresh")
@Slf4j
public class TestFocusOnGridRefreshPage extends CssVerticalLayout {

	private final Grid<String> grid = new Grid<>();

	private final TaskScheduler taskScheduler;

	@PostConstruct
	private void postConstruct() {
		{
			grid.addComponentColumn(item -> {
				logger.info("RENDER: {}", item);
				//
				TextField textField = new TextField();
				textField.setValue(item);
				return textField;
			});
			//
			add(grid);
		}
		{
			UI ui = UI.getCurrent();
			taskScheduler.scheduleAtFixedRate(
					() -> ui.access(() -> {
						logger.info("REFRESH");
						//
						refresh();
					}),
					Duration.ofSeconds(5));
		}
		{
			refresh();
		}
	}

	private void refresh() {
		grid.setItems("foo", "bar", "one", "two", "three");
	}
}
