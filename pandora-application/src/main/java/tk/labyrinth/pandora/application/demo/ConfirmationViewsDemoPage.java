package tk.labyrinth.pandora.application.demo;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.object.custom.CustomizeFormLayout;
import tk.labyrinth.pandora.ui.domain.confirmation.ConfirmationViewTool;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Route(value = "confirmationviews", layout = DemoRootLayout.class)
@Slf4j
public class ConfirmationViewsDemoPage extends CssVerticalLayout {

	private final ConfirmationViewTool confirmationViewTool;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setAlignItems(AlignItems.BASELINE);
		}
		{
			add(ButtonRenderer.render(builder -> builder
					.onClick(event -> confirmationViewTool
							.showTypedViewDialog(DemoObject.builder()
									.integer(12)
									.string("foo")
									.build())
							.subscribeAlwaysAccepted(result -> {
								// no-op
							}))
					.text("Typed View")
					.build()));
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@CustomizeFormLayout(gridColumnNumber = 1)
	@Value
	@With
	private static class DemoObject {

		Integer integer;

		String string;
	}
}
