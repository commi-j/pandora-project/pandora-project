package tk.labyrinth.pandora.testapplication1;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.labyrinth.pandora.storage.mongodb.EnablePandoraStorageMongoDb;

@EnablePandoraStorageMongoDb
@SpringBootApplication
public class SpringBootTestApplicationWithEnableMongoDb {
	// empty
}
