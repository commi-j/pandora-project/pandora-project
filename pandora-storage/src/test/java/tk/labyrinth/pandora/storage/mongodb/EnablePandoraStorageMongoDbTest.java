package tk.labyrinth.pandora.storage.mongodb;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import tk.labyrinth.pandora.testapplication0.SpringBootTestApplication;
import tk.labyrinth.pandora.testapplication1.SpringBootTestApplicationWithEnableMongoDb;

@Deprecated
@Disabled
class EnablePandoraStorageMongoDbTest {

	@Test
	void testDefault() {
		SpringApplication application = new SpringApplication(SpringBootTestApplication.class);
		application.setWebApplicationType(WebApplicationType.NONE);
		//
		ConfigurableApplicationContext context = application.run();
		//
		MongoTemplate mongoTemplate = context.getBeanProvider(MongoTemplate.class).getIfAvailable();
		Assertions.assertNotNull(mongoTemplate);
	}

	@Test
	void testEnabledWithProperty() {
		System.setProperty(PandoraMongoDbConfiguration.MONGO_DB_URI_PROPERTY, "someValue");
		//
		SpringApplication application = new SpringApplication(SpringBootTestApplicationWithEnableMongoDb.class);
		application.setWebApplicationType(WebApplicationType.NONE);
		//
		ConfigurableApplicationContext context = application.run();
		//
		System.clearProperty(PandoraMongoDbConfiguration.MONGO_DB_URI_PROPERTY);
		//
		MongoTemplate mongoTemplate = context.getBeanProvider(MongoTemplate.class).getIfAvailable();
		Assertions.assertNotNull(mongoTemplate);
	}

	@Disabled("It is not true now")
	@Test
	void testEnabledWithoutProperty() {
		SpringApplication application = new SpringApplication(SpringBootTestApplicationWithEnableMongoDb.class);
		application.setWebApplicationType(WebApplicationType.NONE);
		//
		ConfigurableApplicationContext context = application.run();
		//
		MongoTemplate mongoTemplate = context.getBeanProvider(MongoTemplate.class).getIfAvailable();
		Assertions.assertNull(mongoTemplate);
	}
}