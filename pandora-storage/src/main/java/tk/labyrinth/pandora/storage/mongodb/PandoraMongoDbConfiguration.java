package tk.labyrinth.pandora.storage.mongodb;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import tk.labyrinth.pandora.storage.convert.ClassToStringConverter;
import tk.labyrinth.pandora.storage.convert.ObjectIdToUuidConverter;
import tk.labyrinth.pandora.storage.convert.StringToClassConverter;

@ComponentScan
@Configuration
@Import({
		MongoDbJavaListToVavrListConverter.class,
})
@RequiredArgsConstructor
@Slf4j
public class PandoraMongoDbConfiguration {

	// TODO: This property must override spring one for mongo address.
	public static final String MONGO_DB_URI_PROPERTY = "pandora.storage.mongoDbUri";

	@Autowired(required = false)
	private final java.util.List<ConverterProvider> converterProviders;

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}

	/**
	 * Based on spring-boot-autoconfigure <a href="https://github.com/spring-projects/spring-boot/blob/master/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/mongo/MongoDataConfiguration.java">MongoDataConfiguration</a>.<br>
	 */
	@Bean
	public MongoCustomConversions mongoCustomConversions() {
		List<?> converters = List
				.<Object>of(
						new ClassToStringConverter(),
						new ObjectIdToUuidConverter(),
						new StringToClassConverter()
//						, new FromVavrListToJavaListConverter(),
//						new FromVavrMapToJavaMapConverter()
				)
				.appendAll(Stream.ofAll(converterProviders).flatMap(ConverterProvider::provideConverters));
		logger.info("Adding custom Converters: {}", converters);
		return new MongoCustomConversions(converters.asJava());
	}
}
