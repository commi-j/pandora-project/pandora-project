package tk.labyrinth.pandora.storage.mongodb;

import io.vavr.collection.List;
import org.springframework.core.convert.converter.GenericConverter;

public interface SelfProvidingGenericConverter extends GenericConverter, ConverterProvider {

	@Override
	default List<GenericConverter> provideConverters() {
		return List.of(this);
	}
}
