package tk.labyrinth.pandora.storage.mongodb;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClientSettings;
import lombok.RequiredArgsConstructor;
import org.mongojack.JacksonCodecRegistry;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.mongo.MongoClientSettingsBuilderCustomizer;
import org.springframework.stereotype.Component;

@ConditionalOnBean(ObjectMapper.class)
@Component
@RequiredArgsConstructor
public class MongoDbJacksonCodecCustomizer implements MongoClientSettingsBuilderCustomizer {

	private final ObjectMapper objectMapper;

	@Override
	public void customize(MongoClientSettings.Builder clientSettingsBuilder) {
		MongoClientSettings mongoClientSettings = clientSettingsBuilder.build();
		//
		clientSettingsBuilder.codecRegistry(new JacksonCodecRegistry(
				objectMapper,
				mongoClientSettings.getCodecRegistry(),
				mongoClientSettings.getUuidRepresentation()));
	}
}
