package tk.labyrinth.pandora.storage.mongodb;

import io.vavr.collection.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.GenericConverter;

public interface ConverterProvider {

	/**
	 * @return non-null
	 *
	 * @see Converter
	 * @see GenericConverter
	 */
	List<?> provideConverters();
}
