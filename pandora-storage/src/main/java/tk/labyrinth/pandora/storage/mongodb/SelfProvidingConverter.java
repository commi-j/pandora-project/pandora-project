package tk.labyrinth.pandora.storage.mongodb;

import io.vavr.collection.List;
import org.springframework.core.convert.converter.Converter;

public interface SelfProvidingConverter<S, T> extends Converter<S, T>, ConverterProvider {

	@Override
	default List<Converter<?, ?>> provideConverters() {
		return List.of(this);
	}
}
