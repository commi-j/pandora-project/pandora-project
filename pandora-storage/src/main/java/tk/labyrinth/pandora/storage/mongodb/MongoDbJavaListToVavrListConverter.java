package tk.labyrinth.pandora.storage.mongodb;

import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import tk.labyrinth.pandora.dependencies.vavr.FromJavaListToVavrListConverter;

import java.util.Set;

@ReadingConverter
@RequiredArgsConstructor
@Slf4j
@WritingConverter
public class MongoDbJavaListToVavrListConverter implements SelfProvidingGenericConverter {

	private final FromJavaListToVavrListConverter genericConverter;

	@PostConstruct
	private void postConstruct() {
		logger.debug("Initialized");
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		return genericConverter.convert(source, sourceType, targetType);
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Set.of(new ConvertiblePair(java.util.List.class, List.class));
	}
}
