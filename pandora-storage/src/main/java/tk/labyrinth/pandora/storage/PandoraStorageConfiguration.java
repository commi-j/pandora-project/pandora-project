package tk.labyrinth.pandora.storage;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.stores.PandoraStoresModule;

@Deprecated
@ComponentScan
@Configuration
@Import({
		PandoraStoresModule.class,
})
public class PandoraStorageConfiguration {
	// empty
}
