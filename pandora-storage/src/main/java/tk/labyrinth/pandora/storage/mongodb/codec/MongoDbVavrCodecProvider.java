package tk.labyrinth.pandora.storage.mongodb.codec;

import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

import java.util.UUID;

public class MongoDbVavrCodecProvider implements CodecProvider {

	@Override
	@SuppressWarnings("unchecked")
	public <T> Codec<T> get(Class<T> clazz, CodecRegistry registry) {
		if (clazz == UUID.class) {
			return (Codec<T>) new MongoDbVavrListCodec();
		}
		return null;
	}
}
