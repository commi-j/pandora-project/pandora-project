package tk.labyrinth.pandora.storage.mongodb.codec;

import io.vavr.collection.List;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

public class MongoDbVavrListCodec implements Codec<List<?>> {

	@Override
	public List<?> decode(BsonReader bsonReader, DecoderContext decoderContext) {
		bsonReader.readStartArray();
		bsonReader.peekBinarySubType();
		return List.empty();
	}

	@Override
	public void encode(BsonWriter bsonWriter, List<?> objects, EncoderContext encoderContext) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public Class<List<?>> getEncoderClass() {
//		return (Class<List<?>>) List.class;
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
