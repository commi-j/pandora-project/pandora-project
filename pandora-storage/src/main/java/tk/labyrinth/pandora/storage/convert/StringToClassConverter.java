package tk.labyrinth.pandora.storage.convert;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalConverter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class StringToClassConverter implements ConditionalConverter, Converter<String, Class<?>> {

	@Override
	public Class<?> convert(String source) {
		try {
			return Class.forName(source);
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return targetType.getType() == Class.class;
	}
}
