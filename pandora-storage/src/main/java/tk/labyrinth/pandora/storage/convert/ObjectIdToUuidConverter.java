package tk.labyrinth.pandora.storage.convert;

import org.bson.types.ObjectId;
import org.springframework.core.convert.converter.Converter;

import java.util.UUID;

/**
 * When we missed declaring own id so MongoDb created its own and now we need to read them.
 */
public class ObjectIdToUuidConverter implements Converter<ObjectId, UUID> {

	@Override
	public UUID convert(ObjectId source) {
		String value = source.toString();
		//
		return UUID.fromString("%s-%s-%s-%s-%s".formatted(
				value.substring(0, 8),
				value.substring(8, 12),
				value.substring(12, 16),
				value.substring(16, 20),
				value.substring(20)));
	}
}
