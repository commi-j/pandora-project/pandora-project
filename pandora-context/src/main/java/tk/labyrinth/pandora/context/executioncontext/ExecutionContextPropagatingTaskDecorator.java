package tk.labyrinth.pandora.context.executioncontext;

import lombok.RequiredArgsConstructor;
import org.springframework.core.task.TaskDecorator;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class ExecutionContextPropagatingTaskDecorator implements TaskDecorator {

	private final ExecutionContextHandler executionContextHandler;

	@Override
	public Runnable decorate(Runnable runnable) {
		ExecutionContext executionContext = executionContextHandler.getExecutionContext();
		//
		return () -> {
			executionContextHandler.propagateExecutionContext(executionContext);
			//
			try {
				runnable.run();
			} finally {
				executionContextHandler.cleanExecutionContext();
			}
		};
	}
}
