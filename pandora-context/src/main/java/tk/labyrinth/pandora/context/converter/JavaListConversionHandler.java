package tk.labyrinth.pandora.context.converter;

import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.CheckForNull;
import java.util.List;
import java.util.Objects;

@LazyComponent
public class JavaListConversionHandler implements ListLikeConversionHandler {

	@CheckForNull
	@Override
	public Object findFirstNonNullElement(Object value) {
		return ((List<?>) value).stream()
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);
	}

	@Override
	public boolean supports(Class<?> type) {
		return List.class.isAssignableFrom(type);
	}
}
