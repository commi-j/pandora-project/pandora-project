package tk.labyrinth.pandora.context.converter.java;

import org.springframework.core.convert.converter.Converter;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.time.LocalDate;

@LazyComponent
public class FromLocalDateToStringConverter implements Converter<LocalDate, String> {

	@Override
	public String convert(LocalDate source) {
		return source.toString();
	}
}
