package tk.labyrinth.pandora.context.executioncontext;

import io.vavr.collection.List;

public interface ExecutionContextAttributesContributor {

	List<ExecutionContextAttribute<?>> contributeAttributes();
}
