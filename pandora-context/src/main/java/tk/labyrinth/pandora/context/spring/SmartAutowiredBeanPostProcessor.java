package tk.labyrinth.pandora.context.spring;

import io.vavr.Lazy;
import io.vavr.collection.Map;
import io.vavr.collection.Stream;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.context.BeanFactoryThreadLocalContext;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class SmartAutowiredBeanPostProcessor implements BeanPostProcessor {

	private final SpringBeanContext beanContext;

	private BeanFactory getBeanFactory() {
		BeanFactory threadLocalBeanFactory = BeanFactoryThreadLocalContext.get();
		//
		return threadLocalBeanFactory != null ? threadLocalBeanFactory : beanContext.getBeanFactory();
	}

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		//
		// We don't need to access beanFactory unless there's at least one entry to process.
		// There was an issue with circular bean creation because of activating this one for Spring Boot core beans.
		//
		Lazy<BeanFactory> beanFactoryLazy = Lazy.of(this::getBeanFactory);
		org.springframework.util.ReflectionUtils.doWithFields(bean.getClass(), field -> {
			MergedAnnotation<SmartAutowired> annotation = MergedAnnotations.from(field).get(SmartAutowired.class);
			if (annotation.isPresent()) {
				processField(beanFactoryLazy.get(), bean, field, annotation);
			}
		});
		//
		return bean;
	}

	private static Type doResolve(ResolvableType resolvableType) {
		Type result;
		{
			if (resolvableType.hasGenerics()) {
				result = TypeUtils.parameterize(
						resolvableType.resolve(),
						Stream.of(resolvableType.getGenerics())
								.map(SmartAutowiredBeanPostProcessor::doResolve)
								.toJavaArray(Type[]::new));
			} else {
				Type type = resolvableType.getType();
				//
				if (type instanceof TypeVariable) {
					if (Objects.equals(resolvableType.toString(), "?")) {
						// FIXME: If we have cases like Wat<?> where Wat is parameterized as <T extends String>
						//  result would be ? extends String, not ?.
						result = TypeUtils.wildcardType().build();
					} else {
						result = resolvableType.resolve();
					}
				} else {
					result = type;
				}
			}
		}
		return result;
	}

	private static void processField(
			BeanFactory beanFactory,
			Object bean,
			Field field,
			MergedAnnotation<SmartAutowired> annotation) {
		Type fieldType = resolveType(bean.getClass(), beanFactory.getParameterTypes(), field);
		//
		Object beanToInject = beanFactory.findBean(fieldType);
		//
		if (beanToInject != null) {
			org.springframework.data.util.ReflectionUtils.setField(field, bean, beanToInject);
		} else {
			if (annotation.getBoolean("required")) {
				throw new IllegalArgumentException("No bean found for: type = %s, bean.class = %s, field = %s".formatted(
						fieldType, bean.getClass().getName(), field.getName()));
			}
		}
	}

	// TODO: Move to BeanFactory.
	public static Type resolveType(Class<?> contextType, Type type) {
//		ResolvableType resolvableType = ResolvableType.forType(type, ResolvableType.forClass(contextType));
//		return resolvableType.resolve();
		throw new NotImplementedException();
	}

	// TODO: Move to BeanFactory.
	public static Type resolveType(Class<?> contextType, Map<TypeVariable<?>, Type> parameterTypes, Field field) {
		Type result;
		{
			Map<TypeVariable<?>, Type> matchedParameterTypes = parameterTypes
					.filter(pair -> pair._1().getGenericDeclaration() == contextType);
			ResolvableType processedOwnerType = !matchedParameterTypes.isEmpty()
					? ResolvableType.forType(TypeUtils.parameterize(contextType, parameterTypes.toJavaMap()))
					: ResolvableType.forType(contextType);
			//
			result = doResolve(ResolvableType.forField(field, processedOwnerType));
		}
		return result;
	}
}
