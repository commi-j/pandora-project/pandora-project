package tk.labyrinth.pandora.context;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.bean.factory.SimpleBeanFactory;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Implemented by DI framework.
 */
public interface BeanFactory {

	@Nullable <T> T findBean(Class<T> type);

	@Nullable <T> T findBean(Type type);

	default <T> T getBean(Class<T> type) {
		T result = findBean(type);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: type = " + type);
		}
		return result;
	}

	default <T> T getBean(Type type) {
		T result = findBean(type);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: type = " + type);
		}
		return result;
	}

	List<BeanContributor> getBeanContributors();

	Map<TypeVariable<?>, Type> getParameterTypes();

	@Nullable
	BeanFactory getParentBeanFactory();

	List<BeanContributor> getSuppressedBeanContributors();

	HasSupportDistance.SelectionResult<BeanContributor, Type> selectBeanContributors(Type type);

	BeanFactory stepIn();

	BeanFactory withBean(Object bean);

	BeanFactory withBean(Object bean, boolean shallow);

	BeanFactory withBeanContributor(BeanContributor beanContributor);

	BeanFactory withBeanContributor(Type type, BiFunction<BeanFactory, Type, Object> beanContributorFunction);

	BeanFactory withBeanContributor(Type type, Function<BeanFactory, Object> beanContributorFunction);

	BeanFactory withBeanContributor(Type type, Supplier<Object> beanContributorFunction);

	BeanFactory withBeanContributors(BeanContributor... beanContributors);

	BeanFactory withBeanContributors(List<BeanContributor> beanContributors);

	BeanFactory withBeanIfNotNull(@Nullable Object nullableBean);

	BeanFactory withBeans(List<?> beans);

	@SuppressWarnings("unchecked")
	<T> BeanFactory withBeans(T... beans);

	BeanFactory withParameterType(TypeVariable<?> parameter, Type type);

	BeanFactory withSuppressedBeanContributor(BeanContributor beanContributor);

	BeanFactory withSuppressedBeanContributorsOfClass(Class<?> javaClass);

	BeanFactory withoutSuppressedBeanContributors();

	static BeanFactory empty() {
		return new SimpleBeanFactory(List.empty());
	}
}
