package tk.labyrinth.pandora.context.converter;

import javax.annotation.CheckForNull;

public interface ListLikeConversionHandler {

	@CheckForNull
	Object findFirstNonNullElement(Object value);

	boolean supports(Class<?> type);
}
