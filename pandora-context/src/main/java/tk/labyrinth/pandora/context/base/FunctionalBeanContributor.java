package tk.labyrinth.pandora.context.base;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.function.BiFunction;

@RequiredArgsConstructor
public class FunctionalBeanContributor implements BeanContributor {

	private final BiFunction<BeanFactory, Type, Object> beanContributorFunction;

	private final Type type;

	@Nullable
	@Override
	public Object contributeBean(BeanFactory beanFactory, Type type) {
		return beanContributorFunction.apply(beanFactory, type);
	}

	@Override
	public Integer getSupportDistance(BeanFactory beanFactory, Type type) {
		// TODO: Should check parameters (and probably primitives/arrays?).
		return TypeUtils.getClass(this.type).isAssignableFrom(TypeUtils.getClass(type))
				? 0
				: null;
	}

	@Override
	public String toString() {
		return "%s(%s)".formatted(getClass().getSimpleName(), type);
	}
}
