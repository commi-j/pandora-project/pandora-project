package tk.labyrinth.pandora.context.base;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

@RequiredArgsConstructor
public class WrappingBeanFactory extends BeanFactoryBase {

	private final List<BeanContributor> beanContributors;

	private final Map<TypeVariable<?>, Type> parameterTypes;

	private final BeanFactory parentBeanFactory;

	private final List<BeanContributor> suppressedBeanContributors;

	@Override
	public List<BeanContributor> getBeanContributors() {
		return beanContributors;
	}

	@Override
	public Map<TypeVariable<?>, Type> getParameterTypes() {
		return parameterTypes;
	}

	@Override
	public BeanFactory getParentBeanFactory() {
		return parentBeanFactory;
	}

	@Override
	public List<BeanContributor> getSuppressedBeanContributors() {
		return suppressedBeanContributors;
	}
}
