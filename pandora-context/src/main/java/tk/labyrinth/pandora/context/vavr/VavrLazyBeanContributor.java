package tk.labyrinth.pandora.context.vavr;

import io.vavr.Lazy;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

@ConditionalOnClass(Lazy.class)
@LazyComponent
public class VavrLazyBeanContributor extends UnaryParameterizedBeanContributor<Lazy<?>> {

	@Nullable
	@Override
	protected Integer doGetSupportDistance(Class<? extends Lazy<?>> base, Type parameter) {
		return Integer.MAX_VALUE;
	}

	@Nullable
	@Override
	protected Object doContributeBean(BeanFactory beanFactory, Class<? extends Lazy<?>> base, Type parameter) {
		// FIXME: We should check if underlying bean exists to make it fail fast.
		return Lazy.of(() -> beanFactory.getBean(parameter));
	}
}
