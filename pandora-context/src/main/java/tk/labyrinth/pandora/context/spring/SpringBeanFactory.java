package tk.labyrinth.pandora.context.spring;

import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.ResolvableType;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.context.base.BeanFactoryBase;
import tk.labyrinth.pandora.context.base.InstanceBeanContributor;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;

import javax.annotation.CheckForNull;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

@RequiredArgsConstructor
public class SpringBeanFactory extends BeanFactoryBase {

	public static final int DEFAULT_DISTANCE = BeanContributor.PARENT_SUPPORT_DISTANCE;

	private final org.springframework.beans.factory.BeanFactory springBeanFactory;

	@Override
	protected HasSupportDistance.SelectionResult<BeanContributor, Type> selectLocalBeanContributors(Type type) {
		HasSupportDistance.SelectionResult<BeanContributor, Type> result;
		{
			if (type == org.springframework.beans.factory.BeanFactory.class) {
				result = HasSupportDistance.SelectionResult.of(
						List.of(Pair.of(
								DEFAULT_DISTANCE,
								List.of(InstanceBeanContributor.of(DEFAULT_DISTANCE, springBeanFactory, false)))),
						type);
			} else {
				ObjectProvider<Object> objectProvider = springBeanFactory.getBeanProvider(ResolvableType.forType(type));
				//
				// FIXME: We have an issue that we don't know if we have a bean and what is their number until we actually get it.
				//  This means that scoped beans are always created here, which is really bad and may be erroneous.
				List<BeanContributor> beanContributors = objectProvider.orderedStream()
						.collect(List.collector())
						.map(object -> InstanceBeanContributor.of(DEFAULT_DISTANCE, object, false));
				//
				result = HasSupportDistance.SelectionResult.of(
						!beanContributors.isEmpty()
								? List.of(Pair.of(DEFAULT_DISTANCE, beanContributors))
								: List.empty(),
						type);
			}
		}
		return result;
	}

	@Override
	public List<BeanContributor> getBeanContributors() {
		return List.empty();
	}

	@Override
	public Map<TypeVariable<?>, Type> getParameterTypes() {
		return LinkedHashMap.empty();
	}

	@CheckForNull
	@Override
	public BeanFactory getParentBeanFactory() {
		return null;
	}

	@Override
	public List<BeanContributor> getSuppressedBeanContributors() {
		return List.empty();
	}
}
