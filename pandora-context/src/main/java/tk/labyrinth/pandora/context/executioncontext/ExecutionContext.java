package tk.labyrinth.pandora.context.executioncontext;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;
import java.util.function.Function;

@Value(staticConstructor = "of")
public class ExecutionContext {

	@NonNull
	List<ExecutionContextAttribute<?>> attributes;

	public ExecutionContext enrichWith(List<ExecutionContextAttribute<?>> attributes) {
		return of(attributes.toLinkedMap(ExecutionContextAttribute::getName, Function.identity())
				.merge(this.attributes.toLinkedMap(ExecutionContextAttribute::getName, Function.identity()))
				.values()
				.toList());
		//
		// TODO: We want to fail on collisions but they seems inevitable since UI.access from background thread
		//  have both propagated attributes and scoped ones. Probably solvable by tracking attr source to the contributor.
		//  Alternatively - by registering contributors (keys) and not checking it here.
//		List<ExecutionContextAttribute<?>> enrichedAttributes = this.attributes.appendAll(attributes);
//		//
//		if (enrichedAttributes.size() != enrichedAttributes.distinctBy(ExecutionContextAttribute::getName).size()) {
//			throw new IllegalArgumentException("Require no collisions");
//		}
//		//
//		return of(enrichedAttributes);
	}

	@Nullable
	public Object findAttributeValue(String attributeName) {
		return attributes.find(attribute -> Objects.equals(attributeName, attribute.getName()))
				.map(ExecutionContextAttribute::getValue)
				.getOrNull();
	}

	@SuppressWarnings("unchecked")
	public <T> T findAttributeValueInferred(String attributeName) {
		return (T) findAttributeValue(attributeName);
	}

	public List<ExecutionContextAttribute<?>> findAttributes() {
		return attributes;
	}
}
