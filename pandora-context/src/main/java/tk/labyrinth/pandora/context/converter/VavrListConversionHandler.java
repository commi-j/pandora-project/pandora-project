package tk.labyrinth.pandora.context.converter;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.CheckForNull;
import java.util.Objects;

@LazyComponent
public class VavrListConversionHandler implements ListLikeConversionHandler {

	@CheckForNull
	@Override
	public Object findFirstNonNullElement(Object value) {
		return ((List<?>) value).find(Objects::nonNull).getOrNull();
	}

	@Override
	public boolean supports(Class<?> type) {
		return List.class.isAssignableFrom(type);
	}
}
