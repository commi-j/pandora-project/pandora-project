package tk.labyrinth.pandora.context;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.dependencies.PandoraDependenciesModule;

@ComponentScan
@Import({
		PandoraDependenciesModule.class,
})
@SpringBootConfiguration
public class PandoraContextModule {
	// empty
}
