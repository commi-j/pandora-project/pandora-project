package tk.labyrinth.pandora.context.converter;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.Lazy;
import io.vavr.collection.Stream;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.exception.ExceptionUtils;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class ConverterRegistry {

	private final java.util.List<ListLikeConversionHandler> listLikeConversionHandlers;

	private final ObjectProvider<ObjectMapper> objectMapperProvider;

	@SmartAutowired
	private Lazy<ConversionService> conversionServiceLazy;

	private Lazy<ObjectMapper> objectMapperLazy;

	@WithSpan
	private Object convertListLike(Object actualValue, Type expectedType, ListLikeConversionHandler conversionHandler) {
		Object result;
		{
			Object firstNonNullElement = conversionHandler.findFirstNonNullElement(actualValue);
			//
			if (firstNonNullElement != null) {
				// actualValue contains non-null elements -> conversion possible.
				//
				// expectedType may be not parameterized in cases of raw types or Object.class.
				Type expectedElementType = TypeUtils.isParameterizedType(expectedType)
						// TODO: Check if expected type is list-like,
						//  as we can try to convert List to Map and it won't work out well.
						? ((ParameterizedType) expectedType).getActualTypeArguments()[0]
						: Object.class;
				;
				Class<?> expectedElementClass = TypeUtils.findClass(expectedElementType);
				//
				if (expectedElementClass != null && expectedElementClass.isInstance(firstNonNullElement)) {
					// firstNonNullElement is instance of expectedElementClass -> consider all matches -> conversion not required.
					//
					result = actualValue;
				} else {
					// firstNonNullElement is not instance of expectedElementClass -> conversion required.
					//
					ObjectMapper objectMapper = objectMapperLazy.get();
					result = objectMapper.convertValue(
							actualValue,
							objectMapper.constructType(expectedType));
				}
			} else {
				// actualValue contains no non-null elements -> conversion not required.
				//
				result = actualValue;
			}
		}
		return result;
	}

	@WithSpan
	private Object convertWithConversionService(
			ConversionService conversionService,
			Object actualValue,
			TypeDescriptor actualTypeDescriptor,
			TypeDescriptor expectedTypeDescriptor) {
		return conversionService.convert(actualValue, actualTypeDescriptor, expectedTypeDescriptor);
	}

	@WithSpan
	private Object convertWithObjectMapper(Object actualValue, Type expectedType) {
		ObjectMapper objectMapper = objectMapperLazy.get();
		//
		return objectMapper.convertValue(
				actualValue,
				objectMapper.constructType(expectedType));
	}

	@Nullable
	private ListLikeConversionHandler findListLikeConversionHandler(Class<?> type) {
		return Stream.ofAll(listLikeConversionHandlers)
				.find(listLikeConversionHandler -> listLikeConversionHandler.supports(type))
				.getOrNull();
	}

	@PostConstruct
	private void postConstruct() {
		// FIXME: failing on converting nested objects because of modelReference. Should find smarter solution.
		objectMapperLazy = Lazy.of(() -> objectMapperProvider.getObject().copy()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false));
	}

	public <T> T convert(@Nullable Object actualValue, Class<T> expectedClass) {
		T result;
		{
			if (actualValue != null) {
				Object convertedValue = convert(actualValue, (Type) expectedClass);
				//
				if (convertedValue == null) {
					throw new IllegalArgumentException("Require non-null: actualValue = %s, expectedClass = %s"
							.formatted(ExceptionUtils.render(actualValue), expectedClass));
				}
				//
				result = ObjectUtils.infer(convertedValue);
			} else {
				result = null;
			}
		}
		return result;
	}

	public Object convert(@Nullable Object actualValue, Class<?> expectedGenericClass, Class<?>... parameterClasses) {
		return convert(
				actualValue,
				org.apache.commons.lang3.reflect.TypeUtils.parameterize(expectedGenericClass, parameterClasses));
	}

	@WithSpan
	public Object convert(@Nullable Object actualValue, Type expectedType) {
		Object result;
		{
			if (actualValue != null) {
				// actualValue is not null -> conversion possible.
				//
				Class<?> expectedClass = TypeUtils.getClass(expectedType);
				Class<?> actualClass = TypeUtils.getClass(actualValue.getClass());
				//
				if (expectedClass.isAssignableFrom(actualClass)) {
					// actualClass is assignable to expectedClass -> conversion possible.
					//
					ListLikeConversionHandler listLikeConversionHandler =
							findListLikeConversionHandler(actualClass);
					if (listLikeConversionHandler != null) {
						// actualValue is list (according to registered conversionHandlers) -> conversion possible.
						//
						result = convertListLike(actualValue, expectedType, listLikeConversionHandler);
					} else {
						// actualValue is not list (according to registered conversionHandlers) -> conversion not required.
						//
						result = actualValue;
					}
				} else {
					// actualClass is not assignable to expectedClass -> conversion required.
					//
					TypeDescriptor actualTypeDescriptor = TypeDescriptor.forObject(actualValue);
					TypeDescriptor expectedTypeDescriptor = new TypeDescriptor(
							ResolvableType.forType(expectedType),
							null,
							null);
					//
					ConversionService conversionService = conversionServiceLazy.get();
					//
					if (conversionService.canConvert(actualTypeDescriptor, expectedTypeDescriptor)) {
						result = convertWithConversionService(
								conversionService,
								actualValue,
								actualTypeDescriptor,
								expectedTypeDescriptor);
					} else {
						result = convertWithObjectMapper(actualValue, expectedType);
					}
				}
			} else {
				// actualValue is null -> conversion not required.
				//
				result = null;
			}
		}
		return result;
	}

	public <T> T convertInferred(@Nullable Object actualValue, Type expectedType) {
		return ObjectUtils.inferNullable(convert(actualValue, expectedType));
	}

	public ObjectMapper getObjectMapper() {
		return objectMapperLazy.get();
	}
}
