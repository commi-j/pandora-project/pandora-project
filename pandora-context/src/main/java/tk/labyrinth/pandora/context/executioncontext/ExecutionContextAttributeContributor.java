package tk.labyrinth.pandora.context.executioncontext;

public interface ExecutionContextAttributeContributor<T> {

	ExecutionContextAttribute<T> contributeAttribute();
}
