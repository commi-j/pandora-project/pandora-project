package tk.labyrinth.pandora.context.executioncontext;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ExecutionContextAttribute<T> {

	@NonNull
	String name;

	@NonNull
	Boolean propagatable;

	T value;
}
