package tk.labyrinth.pandora.context;

import io.vavr.collection.List;

import javax.annotation.Nullable;

public class BeanFactoryThreadLocalContext {

	// TODO: Layers?
	private static final ThreadLocal<List<BeanFactory>> threadLocalBeanFactoriesStack =
			ThreadLocal.withInitial(List::empty);

	public static void append(BeanFactory beanFactory) {
		threadLocalBeanFactoriesStack.set(threadLocalBeanFactoriesStack.get().append(beanFactory));
	}

	@Nullable
	public static BeanFactory get() {
		return threadLocalBeanFactoriesStack.get().lastOption().getOrNull();
	}

	public static void remove() {
		threadLocalBeanFactoriesStack.set(threadLocalBeanFactoriesStack.get().init());
	}
}
