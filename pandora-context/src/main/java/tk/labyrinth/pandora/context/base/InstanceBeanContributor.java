package tk.labyrinth.pandora.context.base;

import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;

import java.lang.reflect.Type;
import java.util.UUID;

@Value(staticConstructor = "of")
public class InstanceBeanContributor implements BeanContributor {

	int distance;

	@NonNull
	Object instance;

	boolean shallow;

	/**
	 * This one to make equals/hashCode distinct. Otherwise there may be a problem with suppression of similar beans.<br>
	 * The issue was encountered with beans of class Class.<br>
	 */
	UUID uid = UUID.randomUUID();

	@Override
	public Object contributeBean(BeanFactory beanFactory, Type type) {
		return instance;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BeanFactory beanFactory, Type type) {
		return TypeUtils.isInstance(instance, type) ? distance : null;
	}

	@Override
	public String toString() {
		return "%s(%s,%s)".formatted(distance, getClass().getSimpleName(), instance);
	}
}
