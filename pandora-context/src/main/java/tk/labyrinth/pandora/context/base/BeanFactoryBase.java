package tk.labyrinth.pandora.context.base;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.context.BeanFactoryThreadLocalContext;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;

import javax.annotation.CheckForNull;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public abstract class BeanFactoryBase implements BeanFactory {

	protected HasSupportDistance.SelectionResult<BeanContributor, Type> selectLocalBeanContributors(Type type) {
		return HasSupportDistance.selectHandler(
				getBeanContributors().filter(beanProvider -> !getSuppressedBeanContributors().contains(beanProvider)),
				type,
				(beanContributor, localType) -> beanContributor.getSupportDistance(this, localType));
	}

	protected HasSupportDistance.SelectionResult<BeanContributor, Type> selectParentBeanContributors(Type type) {
		HasSupportDistance.SelectionResult<BeanContributor, Type> result;
		{
			BeanFactoryThreadLocalContext.append(this);
			//
			try {
				BeanFactory parentBeanFactory = getParentBeanFactory();
				//
				result = parentBeanFactory != null
						? parentBeanFactory.selectBeanContributors(type)
						: HasSupportDistance.SelectionResult.empty(type);
			} finally {
				BeanFactoryThreadLocalContext.remove();
			}
		}
		return result;
	}

	@CheckForNull
	@Override
	public <T> T findBean(Class<T> type) {
		return findBean((Type) type);
	}

	@CheckForNull
	@Override
	@SuppressWarnings("unchecked")
	public <T> T findBean(Type type) {
		val selectionResult = selectBeanContributors(type);
		//
		BeanContributor beanContributor = selectionResult.findHandler();
		//
		return beanContributor != null ? (T) beanContributor.contributeBean(stepIn(), type) : null;
	}

	@Override
	public HasSupportDistance.SelectionResult<BeanContributor, Type> selectBeanContributors(Type type) {
		val localBeanContributors = selectLocalBeanContributors(type);
		val parentBeanContributors = selectParentBeanContributors(type);
		//
		return localBeanContributors.mergeWith(parentBeanContributors);
	}

	@Override
	public BeanFactory stepIn() {
		return new WrappingBeanFactory(
				getBeanContributors().filter(beanContributor -> !beanContributor.isShallow()),
				getParameterTypes(),
				getParentBeanFactory(),
				List.empty());
	}

	@Override
	public BeanFactory withBean(@NonNull Object bean) {
		return withBean(bean, true);
	}

	@Override
	public BeanFactory withBean(Object bean, boolean shallow) {
		return withBeanContributor(InstanceBeanContributor.of(0, bean, shallow));
	}

	@Override
	public BeanFactory withBeanContributor(Type type, BiFunction<BeanFactory, Type, Object> beanContributorFunction) {
		return withBeanContributors(new FunctionalBeanContributor(beanContributorFunction, type));
	}

	@Override
	public BeanFactory withBeanContributor(Type type, Function<BeanFactory, Object> beanContributorFunction) {
		return withBeanContributor(type, (beanFactory, innerType) -> beanContributorFunction.apply(beanFactory));
	}

	@Override
	public BeanFactory withBeanContributor(Type type, Supplier<Object> beanContributorFunction) {
		return withBeanContributor(type, (beanFactory, innerType) -> beanContributorFunction.get());
	}

	@Override
	public BeanFactory withBeanContributor(BeanContributor beanContributor) {
		return withBeanContributors(List.of(beanContributor));
	}

	@Override
	public BeanFactory withBeanContributors(BeanContributor... beanContributors) {
		return withBeanContributors(List.of(beanContributors));
	}

	@Override
	public BeanFactory withBeanContributors(List<BeanContributor> beanContributors) {
		return new WrappingBeanFactory(
				getBeanContributors().appendAll(beanContributors).distinct(),
				getParameterTypes(),
				getParentBeanFactory(),
				getSuppressedBeanContributors());
	}

	@Override
	public BeanFactory withBeanIfNotNull(@Nullable Object nullableBean) {
		BeanFactory result;
		{
			if (nullableBean != null) {
				result = withBean(nullableBean);
			} else {
				result = this;
			}
		}
		return result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> BeanFactory withBeans(T... beans) {
		return withBeans(List.of(beans));
	}

	@Override
	public BeanFactory withBeans(List<?> beans) {
		return withBeanContributors(beans.map(bean -> InstanceBeanContributor.of(0, bean, true)));
	}

	@Override
	public BeanFactory withParameterType(TypeVariable<?> parameter, Type type) {
		return new WrappingBeanFactory(
				getBeanContributors(),
				getParameterTypes().put(parameter, type),
				getParentBeanFactory(),
				getSuppressedBeanContributors());
	}

	@Override
	public BeanFactory withSuppressedBeanContributor(BeanContributor beanContributor) {
		return new WrappingBeanFactory(
				getBeanContributors(),
				getParameterTypes(),
				getParentBeanFactory(),
				getSuppressedBeanContributors().append(beanContributor));
	}

	@Override
	public BeanFactory withSuppressedBeanContributorsOfClass(Class<?> javaClass) {
		val matchingBeanContributors = selectLocalBeanContributors(javaClass);
		//
		return new WrappingBeanFactory(
				getBeanContributors(),
				getParameterTypes(),
				getParentBeanFactory(),
				getSuppressedBeanContributors().appendAll(matchingBeanContributors.getHandlers()));
	}

	@Override
	public BeanFactory withoutSuppressedBeanContributors() {
		return new WrappingBeanFactory(
				getBeanContributors(),
				getParameterTypes(),
				getParentBeanFactory(),
				List.empty());
	}
}
