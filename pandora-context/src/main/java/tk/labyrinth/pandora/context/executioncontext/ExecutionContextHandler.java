package tk.labyrinth.pandora.context.executioncontext;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.misc4j.lib.spring.context.ObjectProviderUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class ExecutionContextHandler {

	private final ObjectProvider<ExecutionContextAttributeContributor<?>> executionContextAttributeContributorProviders;

	private final ObjectProvider<ExecutionContextAttributesContributor> executionContextAttributesContributorProvider;

	private final ThreadLocal<ExecutionContext> propagatedExecutionContextThreadLocal = new ThreadLocal<>();

	public void cleanExecutionContext() {
		if (propagatedExecutionContextThreadLocal.get() == null) {
			throw new IllegalStateException("False clean");
		}
		//
		propagatedExecutionContextThreadLocal.set(null);
	}

	public ExecutionContext getExecutionContext() {
		ExecutionContext result;
		{
			List<ExecutionContextAttributesContributor> attributesContributors = List.ofAll(ObjectProviderUtils.tryGet(
					executionContextAttributesContributorProvider,
					fault -> {
						// Ignoring ScopeNotActiveExceptions.
					}));
			List<ExecutionContextAttributeContributor<?>> attributeContributors = List.ofAll(ObjectProviderUtils.tryGet(
					executionContextAttributeContributorProviders,
					fault -> {
						// Ignoring ScopeNotActiveExceptions.
					}));
			//
			List<ExecutionContextAttribute<?>> contributedAttributes =
					attributesContributors.flatMap(ExecutionContextAttributesContributor::contributeAttributes)
							.appendAll(attributeContributors.map(ExecutionContextAttributeContributor::contributeAttribute));
			//
			ExecutionContext propagatedExecutionContext = propagatedExecutionContextThreadLocal.get();
			//
			result = propagatedExecutionContext != null
					? propagatedExecutionContext.enrichWith(contributedAttributes)
					: ExecutionContext.of(contributedAttributes);
		}
		return result;
	}

	public void propagateExecutionContext(ExecutionContext externalExecutionContext) {
		if (propagatedExecutionContextThreadLocal.get() != null) {
			throw new IllegalStateException("False propagate");
		}
		//
		propagatedExecutionContextThreadLocal.set(ExecutionContext.of(externalExecutionContext.getAttributes()
				.filter(ExecutionContextAttribute::getPropagatable)));
	}
}
