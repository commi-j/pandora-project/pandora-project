package tk.labyrinth.pandora.context.spring;

import io.vavr.Lazy;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryAware;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.context.base.WrappingBeanFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class SpringBeanContext implements BeanContext, BeanFactoryAware {

	private Lazy<List<BeanContributor>> beanContributorsLazy;

	private SpringBeanFactory springBeanFactory;

	@Override
	public BeanFactory getBeanFactory() {
		return new WrappingBeanFactory(beanContributorsLazy.get(), LinkedHashMap.empty(), springBeanFactory, List.empty());
	}

	@Override
	public void setBeanFactory(org.springframework.beans.factory.BeanFactory beanFactory) throws BeansException {
		springBeanFactory = new SpringBeanFactory(beanFactory);
		beanContributorsLazy = Lazy.of(() -> List.ofAll(beanFactory.getBeanProvider(BeanContributor.class)));
	}
}
