package tk.labyrinth.pandora.context.vavr;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.ResolvableType;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

@ConditionalOnClass(List.class)
@LazyComponent
@RequiredArgsConstructor
public class VavrListBeanContributor extends UnaryParameterizedBeanContributor<List<?>> {

	private final AutowireCapableBeanFactory springBeanFactory;

	@Nullable
	@Override
	protected Integer doGetSupportDistance(Class<? extends List<?>> base, Type parameter) {
		return Integer.MAX_VALUE;
	}

	@Nullable
	@Override
	protected Object doContributeBean(BeanFactory beanFactory, Class<? extends List<?>> base, Type parameter) {
		List<Object> result;
		{
			Object object = beanFactory.findBean(TypeUtils.parameterize(java.util.List.class, parameter));
			if (object != null) {
				// Explicit bean of java list type.
				//
				result = List.ofAll((java.util.List<?>) object);
			} else {
				// Collection of all beans matching parameter.
				//
				result = List.ofAll(springBeanFactory.getBeanProvider(ResolvableType.forType(parameter))
						.orderedStream());
			}
		}
		return result;
	}
}
