package tk.labyrinth.pandora.context;

/**
 * Implemented by DI framework.
 */
public interface BeanContext {

	/**
	 * Returns {@link BeanFactory} for current scope context.<br>
	 * <br>
	 * This class is not designed to be cached for later use, make sure to request new one each time.<br>
	 *
	 * @return non-null
	 */
	BeanFactory getBeanFactory();
}
