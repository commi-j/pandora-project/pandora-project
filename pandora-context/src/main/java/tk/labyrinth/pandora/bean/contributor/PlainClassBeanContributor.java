package tk.labyrinth.pandora.bean.contributor;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

import java.lang.reflect.Type;

public interface PlainClassBeanContributor<T> extends BeanContributor, TypeAware<T> {

	@Nullable
	@Override
	default T contributeBean(BeanFactory beanFactory, Type type) {
		return doContributeBean(beanFactory, type);
	}

	// FIXME: How could it be nullable?
	@Nullable
	T doContributeBean(BeanFactory beanFactory, Type type);

	@Nullable
	default Integer doGetSupportDistance(BeanFactory beanFactory, Type type) {
		return doGetSupportDistance(type);
	}

	@Deprecated
	@Nullable
	default Integer doGetSupportDistance(Type type) {
		throw new UnsupportedOperationException("Deprecated, see override");
	}

	@Nullable
	@Override
	default Integer getSupportDistance(BeanFactory beanFactory, Type type) {
		return TypeUtils.isAssignableTo(getParameterType(), type)
				? doGetSupportDistance(beanFactory, type)
				: null;
	}
}
