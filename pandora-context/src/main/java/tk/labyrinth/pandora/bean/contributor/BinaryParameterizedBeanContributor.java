package tk.labyrinth.pandora.bean.contributor;

import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

/**
 * @param <T> Binary parameterized type
 */
public abstract class BinaryParameterizedBeanContributor<T> implements BeanContributor, TypeAware<T> {

	private final Class<?> baseClass;

	{
		Type baseType = getParameterType();
		baseClass = TypeUtils.getClass(baseType);
		//
		if (!TypeUtils.isParameterizedType(baseType) ||
				baseClass.getTypeParameters().length != 2) {
			throw new IllegalStateException("Require binary-parameterized: " + this);
		}
	}

	@Nullable
	protected abstract Object doContributeBean(
			BeanFactory beanFactory,
			Class<? extends T> base,
			Type firstParameter,
			Type secondParameter);

	@Nullable
	protected abstract Integer doGetSupportDistance(
			Class<? extends T> base,
			Type firstParameter,
			Type secondParameter);

	@Nullable
	@Override
	public Object contributeBean(BeanFactory beanFactory, Type type) {
		return doContributeBean(beanFactory,
				TypeUtils.getClassInferred(type),
				ParameterUtils.getFirstActualParameter(type, baseClass),
				ParameterUtils.getSecondActualParameter(type, baseClass));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BeanFactory beanFactory, Type type) {
		return TypeUtils.isAssignableTo(type, getParameterType())
				? doGetSupportDistance(
				TypeUtils.getClassInferred(type),
				ParameterUtils.getFirstActualParameter(type, baseClass),
				ParameterUtils.getSecondActualParameter(type, baseClass))
				: null;
	}
}
