package tk.labyrinth.pandora.bean.contributor;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanFactory;

import java.lang.reflect.Type;

/**
 * @see PlainClassBeanContributor
 * @see UnaryParameterizedBeanContributor
 * @see BinaryParameterizedBeanContributor
 */
public interface BeanContributor {

	// FIXME: Rework (remove?) this.
	int PARENT_SUPPORT_DISTANCE = 10;

	/**
	 * @return nullable
	 */
	// FIXME: How could it be nullable?
	@Nullable
	Object contributeBean(BeanFactory beanFactory, Type type);

	/**
	 * The higher returned value the less prioritized it will be. If value is below PARENT_SUPPORT_DISTANCE,
	 * this contributor will be considered before parent {@link BeanFactory}, otherwise it will go after.
	 *
	 * @param beanFactory non-null
	 * @param type        non-null
	 *
	 * @return nullable
	 */
	@Nullable
	Integer getSupportDistance(BeanFactory beanFactory, Type type);

	/**
	 * @param type non-null
	 *
	 * @return non-negative or null
	 */
	@Deprecated // FIXME: in favour of overloaded method.
	@Nullable
	default Integer getSupportDistance(Type type) {
		throw new UnsupportedOperationException("Deprecated, see overload");
	}

	default boolean isShallow() {
		return false;
	}

	/**
	 * @param type non-null
	 *
	 * @return true or false
	 */
	default boolean supportsType(BeanFactory beanFactory, Type type) {
		return getSupportDistance(beanFactory, type) != null;
	}
}
