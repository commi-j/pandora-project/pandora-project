package tk.labyrinth.pandora.bean.factory;

import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.base.WrappingBeanFactory;

public class SimpleBeanFactory extends WrappingBeanFactory {

	public SimpleBeanFactory(List<BeanContributor> beanContributors) {
		super(beanContributors, LinkedHashMap.empty(), null, List.empty());
	}
}
