package tk.labyrinth.pandora.bean.contributor;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @param <T> Unary parameterized type
 */
public abstract class UnaryParameterizedBeanContributor<T> implements BeanContributor, TypeAware<T> {

	private final Class<?> baseClass;

	{
		Type baseType = getParameterType();
		baseClass = TypeUtils.getClass(baseType);
		//
		if (!TypeUtils.isParameterizedType(baseType) ||
				baseClass.getTypeParameters().length != 1) {
			throw new IllegalStateException("Require unary-parameterized: " + this);
		}
	}

	@Nullable
	protected abstract Object doContributeBean(BeanFactory beanFactory, Class<? extends T> base, Type parameter);

	@Nullable
	protected Integer doGetSupportDistance(BeanFactory beanFactory, Class<? extends T> base, Type parameter) {
		return doGetSupportDistance(base, parameter);
	}

	@Deprecated // in favour of overloaded method.
	@Nullable
	protected Integer doGetSupportDistance(Class<? extends T> base, Type parameter) {
		throw new UnsupportedOperationException("Deprecated, see overload");
	}

	@Nullable
	@Override
	public Object contributeBean(BeanFactory beanFactory, Type type) {
		return doContributeBean(
				beanFactory,
				TypeUtils.getClassInferred(type),
				ParameterUtils.getActualParameter(
						type,
						ParameterUtils.resolveTypeParameter(
								baseClass.getTypeParameters()[0],
								TypeUtils.getClass(type))));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BeanFactory beanFactory, Type type) {
		return matchesParameter(type, getParameterType())
				? doGetSupportDistance(
				beanFactory,
				TypeUtils.getClassInferred(type),
				ParameterUtils.getFirstActualParameter(
						type,
						ParameterUtils
								.resolveTypeParameter(
										baseClass.getTypeParameters()[0],
										TypeUtils.getClass(type))
								.getGenericDeclaration()))
				: null;
	}

	public static boolean matchesParameter(Type testType, Type parameterType) {
		boolean result;
		{
			Class<?> testClass = TypeUtils.getClass(testType);
			Class<?> parameterClass = TypeUtils.getClass(parameterType);
			//
			if (testClass.isAssignableFrom(parameterClass)) {
				if (parameterType instanceof Class) {
					// For class we check if test is parent of parameter (i.e. parameter is qualified to represent test).
					//
					result = testClass.isAssignableFrom(parameterClass);
				} else if (parameterType instanceof ParameterizedType parameterizedParameterType) {
					// For parameterized we check if test is parent of parameter and
					// parameters of test are children of parameter's parameters.
					//
					if (testType instanceof ParameterizedType parameterizedTestType) {
						Class<?> superclass = ClassUtils.pickSuperclass(
								TypeUtils.getClass(parameterizedParameterType),
								TypeUtils.getClass(parameterizedTestType));
						//
						List<Type> parameterParameters = ParameterUtils
								.getActualParameters(parameterizedParameterType, superclass)
								.collect(Collectors.toList());
						List<Type> testParameters = ParameterUtils
								.getActualParameters(parameterizedTestType, superclass)
								.collect(Collectors.toList());
						//
						result = IntStream.range(0, superclass.getTypeParameters().length).allMatch(index ->
								TypeUtils.isAssignableTo(testParameters.get(index), parameterParameters.get(index)));
					} else {
						// parameterType is parameterized and testType is not -> testType is RawType, does not match.
						//
						result = false;
					}
				} else {
					// Only Classes and ParameterizedTypes are supported so far.
					// TODO: Find out if we need to support smth else, probably change ex to UnsupOp or IllArg.
					//
					throw new NotImplementedException();
				}
			} else {
				// Classes not assignable - definitely does not match.
				//
				result = false;
			}
		}
		return result;
	}
}
