package tk.labyrinth.pandora.context.vavr;

import io.vavr.Lazy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.context.PandoraContextModule;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;

@ExtendWithSpring
@Import({
		PandoraContextModule.class,
})
class VavrLazyBeanContributorTest {

	@SmartAutowired
	private Lazy<VavrLazyBeanContributor> lazyBeanContributorLazy;

	@SmartAutowired
	private Lazy<String> stringLazy;

	@Test
	void testInjectedMissingBean() {
		ContribAssertions.assertThrows(
				() -> stringLazy.get(),
				fault -> Assertions.assertEquals(
						fault.toString(),
						"java.lang.IllegalArgumentException: Not found: type = class java.lang.String"));
	}

	@Test
	void testInjectedPresentBean() {
		Assertions.assertNotNull(lazyBeanContributorLazy.get());
	}
}