package tk.labyrinth.pandora.context.vavr;

import io.vavr.collection.List;
import lombok.Value;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.context.PandoraContextModule;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;

import java.util.ArrayList;

@ExtendWithSpring
@Import({
		PandoraContextModule.class,
		VavrListBeanContributorTest.FirstItem.class,
		VavrListBeanContributorTest.SecondItem.class,
})
class VavrListBeanContributorTest {

	@SmartAutowired
	private List<Item> items;

	@Test
	void testInjectionOfCollectedBeans() {
		Assertions.assertEquals(List.of(new FirstItem(), new SecondItem()), items);
	}

	@Disabled
	@Test
	void testInjectionOfJavaListBean() {
		// TODO: Find out how to declare different contexts for methods.
		// TODO: Use JavaItemList.
	}

	interface Item {
		// empty
	}

	@Value
	static class FirstItem implements Item {
		// empty
	}

	@Value
	static class JavaItemList extends ArrayList<Item> {
		// empty
	}

	@Value
	static class SecondItem implements Item {
		// empty
	}
}