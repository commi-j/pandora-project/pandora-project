package tk.labyrinth.pandora.context.base;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.bean.contributor.BeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

class WrappingBeanFactoryTest {

	@Test
	void testBeanContributorSuppression() {
		{
			// Without suppression.
			//
			BeanFactory beanFactory = BeanFactory.empty()
					.withBeanContributor(new ValueBeanContributor<>(1, 0))
					.withBeanContributor(new TestingBeanContributor(false));
			Assertions.assertEquals(1, beanFactory.getBean(Integer.class));
		}
		{
			// With suppression.
			//
			BeanFactory beanFactory = BeanFactory.empty()
					.withBeanContributor(new ValueBeanContributor<>(1, 0))
					.withBeanContributor(new TestingBeanContributor(true));
			Assertions.assertEquals(0, beanFactory.getBean(Integer.class));
		}
	}

	@RequiredArgsConstructor
	private static class TestingBeanContributor implements BeanContributor {

		private final boolean suppress;

		private boolean visited = false;

		@Nullable
		@Override
		public Object contributeBean(BeanFactory beanFactory, Type type) {
			Object result;
			{
				if (!visited) {
					visited = true;
					BeanFactory beanFactoryToUse;
					if (suppress) {
						beanFactoryToUse = beanFactory.withSuppressedBeanContributor(this);
					} else {
						beanFactoryToUse = beanFactory;
					}
					result = beanFactoryToUse.getBean(Integer.class);
					visited = false;
				} else {
					result = 1;
				}
			}
			return result;
		}

		@Nullable
		@Override
		public Integer getSupportDistance(BeanFactory beanFactory, Type type) {
			return 0;
		}
	}

	@RequiredArgsConstructor
	private static class ValueBeanContributor<T> implements BeanContributor {

		private final int distance;

		private final T value;

		@Nullable
		@Override
		public Object contributeBean(BeanFactory beanFactory, Type type) {
			return value;
		}

		@Nullable
		@Override
		public Integer getSupportDistance(BeanFactory beanFactory, Type type) {
			return distance;
		}
	}
}