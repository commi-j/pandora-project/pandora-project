package tk.labyrinth.pandora.context.converter.java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.ConversionService;

import java.time.LocalDate;

@SpringBootTest
class FromLocalDateToStringConverterTest {

	@Autowired
	private ConversionService conversionService;

	@Test
	void testConvertFromLocalDateToValueWrapper() {
		Assertions.assertEquals(
				"2022-12-18",
				conversionService.convert(LocalDate.parse("2022-12-18"), String.class));
	}
}