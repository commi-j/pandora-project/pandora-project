package tk.labyrinth.pandora.context.spring;

import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;

class SmartAutowiredBeanPostProcessorTest {

	private final List<Set<String>> listOfSetOfString = null;

	@Test
	void testResolveType() throws NoSuchFieldException {
		Assertions.assertEquals(
				ChildBean.class,
				SmartAutowiredBeanPostProcessor.resolveType(
						ObjectBean.class,
						LinkedHashMap.empty(),
						SuperBean.class.getDeclaredField("tInSuper")));
		Assertions.assertEquals(
				TypeUtils.parameterize(List.class, TypeUtils.parameterize(Set.class, String.class)),
				SmartAutowiredBeanPostProcessor.resolveType(
						SuperBean.class,
						LinkedHashMap.empty(),
						getClass().getDeclaredField("listOfSetOfString")));
		Assertions.assertEquals(
				TypeUtils.parameterize(Option.class, String.class),
				SmartAutowiredBeanPostProcessor.resolveType(
						SuperBean.class,
						LinkedHashMap.of(SuperBean.class.getTypeParameters()[0], String.class),
						SuperBean.class.getDeclaredField("optionTInSuper")));
	}

	@ExtendWithSpring
	@Test
	void testSmartAutowire(@Autowired ObjectBean objectBean) {
		Assertions.assertNotNull(objectBean.childBeanInObject);
		Assertions.assertNotNull(objectBean.childBeanInSuper);
		Assertions.assertNotNull(objectBean.tInSuper);
	}

	@Component
	static class ChildBean {
		// empty
	}

	@Component
	static class ObjectBean extends SuperBean<ChildBean> {

		@SmartAutowired
		private ChildBean childBeanInObject;
	}

	static abstract class SuperBean<T> {

		@SmartAutowired
		ChildBean childBeanInSuper;

		@SmartAutowired(required = false)
		Option<T> optionTInSuper;

		@SmartAutowired
		T tInSuper;
	}

	@Configuration
	@Import({
			ChildBean.class,
			ObjectBean.class,
			SmartAutowiredBeanPostProcessor.class,
			SpringBeanContext.class,
	})
	static class TestConfiguration {
		//
	}
}