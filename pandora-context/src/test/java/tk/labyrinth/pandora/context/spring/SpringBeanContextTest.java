package tk.labyrinth.pandora.context.spring;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.context.PandoraContextModule;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;

@ExtendWithSpring
@Import({
		PandoraContextModule.class,
		SpringBeanContextTest.ParameterizedClass.class,
})
class SpringBeanContextTest {

	@Autowired
	private SpringBeanContext beanContext;

	@Test
	void testGetBeanOfParameterizedClass() {
		// TODO: Spring does not support bean acquisition with parameters. We want to overhaul this behaviour.
		//
		Assertions.assertNotNull(beanContext.getBeanFactory().getBean(ParameterizedClass.class));
		ContribAssertions.assertThrows(
				() -> beanContext.getBeanFactory().getBean(
						TypeUtils.parameterize(ParameterizedClass.class, Integer.class)),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: Not found: type = tk.labyrinth.pandora.context.spring.SpringBeanContextTest.ParameterizedClass<java.lang.Integer>",
						fault.toString()));
	}

	static class ParameterizedClass<T> {
		// empty
	}
}