package tk.labyrinth.pandora.bean.contributor;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.context.BeanFactory;

import java.lang.reflect.Type;

class PlainClassBeanContributorTest {

	@Test
	void testSupports() {
		NumberBeanContributor beanContributor = new NumberBeanContributor();
		//
		Assertions.assertTrue(beanContributor.supportsType(null, Number.class));
		Assertions.assertTrue(beanContributor.supportsType(null, Object.class));
		//
		Assertions.assertFalse(beanContributor.supportsType(null, Integer.class));
	}

	static class NumberBeanContributor implements PlainClassBeanContributor<Number> {

		@Nullable
		@Override
		public Number doContributeBean(BeanFactory beanFactory, Type type) {
			throw new UnsupportedOperationException();
		}

		@Nullable
		@Override
		public Integer doGetSupportDistance(BeanFactory beanFactory, Type type) {
			return 0;
		}
	}
}