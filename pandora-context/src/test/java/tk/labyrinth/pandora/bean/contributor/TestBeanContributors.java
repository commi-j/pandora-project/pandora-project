package tk.labyrinth.pandora.bean.contributor;

import tk.labyrinth.pandora.context.BeanFactory;

import javax.annotation.CheckForNull;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class TestBeanContributors {

	public static class ParameterizedWithPlain extends
			UnaryParameterizedBeanContributor<ArrayList<Number>> {

		@CheckForNull
		@Override
		protected Integer doGetSupportDistance(Class<? extends ArrayList<Number>> base, Type parameter) {
			return 0;
		}

		@CheckForNull
		@Override
		protected Object doContributeBean(
				BeanFactory beanFactory,
				Class<? extends ArrayList<Number>> base,
				Type parameter) {
			throw new UnsupportedOperationException();
		}
	}

	public static class ParameterizedWithWildcard extends
			UnaryParameterizedBeanContributor<ArrayList<?>> {

		@CheckForNull
		@Override
		protected Integer doGetSupportDistance(Class<? extends ArrayList<?>> base, Type parameter) {
			return 0;
		}

		@CheckForNull
		@Override
		protected Object doContributeBean(BeanFactory beanFactory, Class<? extends ArrayList<?>> base, Type parameter) {
			throw new UnsupportedOperationException();
		}
	}

	public static class ParameterizedWithWildcardExtendingParameterizedWithWildcard extends
			UnaryParameterizedBeanContributor<ArrayList<? extends ArrayList<?>>> {

		@CheckForNull
		@Override
		protected Integer doGetSupportDistance(
				Class<? extends ArrayList<? extends ArrayList<?>>> base,
				Type parameter) {
			return 0;
		}

		@CheckForNull
		@Override
		protected Object doContributeBean(
				BeanFactory beanFactory,
				Class<? extends ArrayList<? extends ArrayList<?>>> base,
				Type parameter) {
			throw new UnsupportedOperationException();
		}
	}

	public static class ParameterizedWithWildcardWithExtends extends
			UnaryParameterizedBeanContributor<ArrayList<? extends Number>> {

		@CheckForNull
		@Override
		protected Integer doGetSupportDistance(Class<? extends ArrayList<? extends Number>> base, Type parameter) {
			return 0;
		}

		@CheckForNull
		@Override
		protected Object doContributeBean(
				BeanFactory beanFactory,
				Class<? extends ArrayList<? extends Number>> base,
				Type parameter) {
			throw new UnsupportedOperationException();
		}
	}

	public static class ParameterizedWithWildcardWithSuper extends
			UnaryParameterizedBeanContributor<ArrayList<? super Object>> {

		@CheckForNull
		@Override
		protected Integer doGetSupportDistance(Class<? extends ArrayList<? super Object>> base, Type parameter) {
			return 0;
		}

		@CheckForNull
		@Override
		protected Object doContributeBean(
				BeanFactory beanFactory,
				Class<? extends ArrayList<? super Object>> base,
				Type parameter) {
			throw new UnsupportedOperationException();
		}
	}
}