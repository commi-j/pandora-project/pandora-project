package tk.labyrinth.pandora.bean.contributor;

import io.vavr.control.Option;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.ManagedList;
import tk.labyrinth.pandora.context.BeanFactory;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

class UnaryParameterizedBeanContributorTest {

	@SuppressWarnings("rawtypes")
	private final Option rawOption = null;

	@Disabled("Does not work well for now, need to refactor.")
	@Test
	void testGetSupportDistance() {
		TestParameterizedClassBeanContributor beanContributor = new TestParameterizedClassBeanContributor();
		//
		Assertions.assertNull(beanContributor.getSupportDistance(null, Number.class));
		Assertions.assertNull(beanContributor.getSupportDistance(null, Option.class));
		Assertions.assertEquals(
				0,
				beanContributor.getSupportDistance(null, TypeUtils.parameterize(Option.class, Number.class)));
		Assertions.assertEquals(
				0,
				beanContributor.getSupportDistance(null, NumberOption.class));
		Assertions.assertEquals(
				0,
				beanContributor.getSupportDistance(null, TypeUtils.parameterize(Option.class, Integer.class)));
		Assertions.assertNull(beanContributor.getSupportDistance(
				null,
				TypeUtils.parameterize(Option.class, String.class)));
		Assertions.assertNull(beanContributor.getSupportDistance(null, StringOption.class));
		Assertions.assertNull(beanContributor.getSupportDistance(null, ObjectOption.class));
	}

	@Test
	void testGetSupportDistanceWithRawType() throws NoSuchFieldException {
		TestParameterizedClassBeanContributor beanContributor = new TestParameterizedClassBeanContributor();
		//
		Assertions.assertNull(beanContributor.getSupportDistance(
				null,
				getClass().getDeclaredField("rawOption").getGenericType()));
	}

	/**
	 * Ensuring it works well when List<Integer> is tested agains ArrayList<?>
	 */
	@Test
	void testGetSupportDistanceWithSuperclass() {
		Assertions.assertEquals(
				0,
				new TestBeanContributors.ParameterizedWithWildcard().getSupportDistance(
						null,
						TypeUtils.parameterize(List.class, Integer.class)));
	}

	@Test
	void testMatchesParametersForParameterizedWithPlain() {
		// ArrayList<Number>
		//
		Type parameterType = new TestBeanContributors.ParameterizedWithPlain().getParameterType();
		//
		// FIXME: Should it fail because it is <Number> and not <? extends Number>?
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Integer.class),
				parameterType));
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Number.class),
				parameterType));
		Assertions.assertFalse(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Object.class),
				parameterType));
		Assertions.assertFalse(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(ManagedList.class, Number.class),
				parameterType));
	}

	@Test
	void testMatchesParametersForParameterizedWithWildcard() {
		// ArrayList<?>
		//
		Type parameterType = new TestBeanContributors.ParameterizedWithWildcard().getParameterType();
		//
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Integer.class),
				parameterType));
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Number.class),
				parameterType));
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Object.class),
				parameterType));
		Assertions.assertFalse(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(ManagedList.class, Number.class),
				parameterType));
	}

	@Test
	void testMatchesParametersForParameterizedWithWildcardExtendingParameterizedWithWildcard() {
		// ArrayList<? extends ArrayList<?>>
		//
		Type parameterType = new TestBeanContributors.ParameterizedWithWildcardExtendingParameterizedWithWildcard()
				.getParameterType();
		//
		Assertions.assertFalse(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(
						List.class,
						TypeUtils.parameterize(List.class, TypeUtils.wildcardType().build())),
				parameterType));
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(
						List.class,
						TypeUtils.parameterize(ArrayList.class, TypeUtils.wildcardType().build())),
				parameterType));
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(
						List.class,
						TypeUtils.parameterize(ArrayList.class, Integer.class)),
				parameterType));
		Assertions.assertFalse(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(
						ManagedList.class,
						TypeUtils.parameterize(ArrayList.class, Integer.class)),
				parameterType));
	}

	@Test
	void testMatchesParametersForParameterizedWithWildcardWithExtends() {
		// ArrayList<? extends Number>
		//
		Type parameterType = new TestBeanContributors.ParameterizedWithWildcardWithExtends().getParameterType();
		//
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Integer.class),
				parameterType));
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Number.class),
				parameterType));
		Assertions.assertFalse(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(List.class, Object.class),
				parameterType));
		Assertions.assertFalse(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(ManagedList.class, Number.class),
				parameterType));
	}

	private static abstract class NumberOption implements Option<Number> {
		// empty
	}

	private static abstract class ObjectOption implements Option<Object> {
		// empty
	}

	private static abstract class StringOption implements Option<String> {
		// empty
	}

	private static class TestParameterizedClassBeanContributor extends
			UnaryParameterizedBeanContributor<Option<? extends Number>> {

		@Nullable
		@Override
		protected Object doContributeBean(
				BeanFactory beanFactory,
				Class<? extends Option<? extends Number>> base,
				Type parameter) {
			throw new UnsupportedOperationException();
		}

		@Nullable
		@Override
		protected Integer doGetSupportDistance(Class<? extends Option<? extends Number>> base, Type parameter) {
			return 0;
		}
	}
}