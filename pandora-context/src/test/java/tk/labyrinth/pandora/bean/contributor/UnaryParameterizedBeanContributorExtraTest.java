package tk.labyrinth.pandora.bean.contributor;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.context.BeanFactory;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

class UnaryParameterizedBeanContributorExtraTest {

	@Test
	void testContributeBeanInvokesDoProvideBeanWithIntegerParameter() {
		BeanContributor beanContributor = new BeanContributor();
		//
		Object bean = beanContributor.contributeBean(
				null,
				TypeUtils.parameterize(ParentClass.class, String.class, Integer.class));
		//
		Assertions.assertEquals(Integer.class, bean);
	}

	@Test
	void testMatchesParametersForParameterReductionDuringInheritance() {
		// Parameter type is ChildClass<?>, where ChildClass<B> extends ParentClass<String,B>
		// Test type is ParentClass<String,Integer>
		//
		Assertions.assertTrue(UnaryParameterizedBeanContributor.matchesParameter(
				TypeUtils.parameterize(ParentClass.class, String.class, Integer.class),
				TypeUtils.parameterize(ChildClass.class, TypeUtils.wildcardType().build())));
	}

	private static class BeanContributor extends UnaryParameterizedBeanContributor<ChildClass<?>> {

		@Nullable
		@Override
		protected Object doContributeBean(
				BeanFactory beanFactory,
				Class<? extends ChildClass<?>> base,
				Type parameter) {
			return parameter;
		}

		@Nullable
		@Override
		protected Integer doGetSupportDistance(Class<? extends ChildClass<?>> base, Type parameter) {
			return 1;
		}
	}

	private static class ChildClass<B> extends ParentClass<String, B> {
		// empty
	}

	private static class ParentClass<A, B> {
		// empty
	}
}
