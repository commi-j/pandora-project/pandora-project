package tk.labyrinth.pandora.functions.domain.routine;

import com.cronutils.model.Cron;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.time.Instant;
import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@ModelTagPandora
@RootModel(code = Routine.MODEL_CODE, primaryAttribute = HasUid.UID_ATTRIBUTE_NAME)
@Value
@With
public class Routine implements HasUid {

	public static final String EFFECTIVE_SINCE_ATTRIBUTE_NAME = "effectiveSince";

	public static final String ENABLED_ATTRIBUTE_NAME = "enabled";

	public static final String MODEL_CODE = "routine";

	Cron cron;

	Instant effectiveSince;

	Boolean enabled;

	String functionName;

	String name;

	Object parameters;

	UUID uid;
}
