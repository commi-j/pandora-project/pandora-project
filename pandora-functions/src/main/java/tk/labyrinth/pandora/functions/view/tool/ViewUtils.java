package tk.labyrinth.pandora.functions.view.tool;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.treegrid.TreeGrid;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.util.Objects;

public class ViewUtils {

	public static <T> Grid<T> createGrid() {
		Grid<T> grid = new Grid<>();
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
		}
		return grid;
	}

	public static <T> TreeGrid<T> createTreeGrid() {
		TreeGrid<T> grid = new TreeGrid<>();
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
		}
		return grid;
	}

	public static <T extends HasUid> void processFormConfirm(
			boolean isAdmin,
			TypedObjectManipulator<T> objectManipulator,
			T initialValue,
			T currentValue) {
		if (isAdmin && !Objects.equals(currentValue, initialValue)) {
			// TODO
//			objectManipulator.createOrUpdate(
//					currentValue.withUid(initialValue.getUid() != null ? initialValue.getUid() : UUID.randomUUID()));
			//
			// TODO: We just need to refresh the content here.
			UI.getCurrent().getPage().reload();
		}
	}
}
