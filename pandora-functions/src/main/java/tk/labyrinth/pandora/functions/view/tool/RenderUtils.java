package tk.labyrinth.pandora.functions.view.tool;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.hyperlink.Hyperlink;
import tk.labyrinth.pandora.functionalvaadin.component.SpanRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.html.AnchorRenderer;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoBadgeVariables;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

public class RenderUtils {

	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	public static String format(Date date) {
		return format(date.toInstant());
	}

	public static String format(Instant instant) {
		return formatter.format(instant.atOffset(ZoneOffset.UTC));
	}

	public static Component renderBadge(String text, @Nullable String colourTheme) {
		return SpanRenderer.render(builder -> builder
				.text(text)
				.themes(List
						.of(
								LumoBadgeVariables.BADGE,
								LumoBadgeVariables.PILL,
								LumoBadgeVariables.SMALL,
								colourTheme)
						.filter(Objects::nonNull))
				.width("5.5em")
				.build());
	}

	public static Component renderHyperlink(Hyperlink hyperlink) {
		return AnchorRenderer.render(builder -> builder
				.href(hyperlink.getDestination())
				.target(AnchorTarget.BLANK)
				.text(hyperlink.getText())
				.build());
	}

	public static Component renderJsonNode(JsonNode jsonNode) {
		return jsonNode instanceof ObjectNode objectNode
				? GridUtils.renderValueList(List.ofAll(StreamUtils
				.from(objectNode.fields())
				.map(field -> "%s : %s".formatted(field.getKey(), field.getValue()))))
				: new Span(jsonNode.toString());
	}

	public static Component renderUrl(String url) {
		return renderUrl(url, url);
	}

	public static Component renderUrl(String url, String text) {
		return AnchorRenderer.render(builder -> builder
				.configurer(anchor -> StyleUtils.setCssProperty(anchor, WhiteSpace.NORMAL))
				.href(url)
				.target(AnchorTarget.BLANK)
				.text(text)
				.build());
	}
}
