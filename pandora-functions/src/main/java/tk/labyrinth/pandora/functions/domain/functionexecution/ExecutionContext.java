package tk.labyrinth.pandora.functions.domain.functionexecution;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;

public interface ExecutionContext {

	/**
	 * Return index
	 *
	 * @return non-negative
	 */
	@Nullable
	Integer getMultiRunIndex();

	default boolean isMultiRun() {
		return getMultiRunIndex() != null;
	}

	<P, R> List<Outcome<R>> multiRunFunction(TypedFunctionReference<P, R> functionReference, List<P> parameterses);

	<P, R> Outcome<R> runFunction(TypedFunctionReference<P, R> functionReference, P parameters);
}
