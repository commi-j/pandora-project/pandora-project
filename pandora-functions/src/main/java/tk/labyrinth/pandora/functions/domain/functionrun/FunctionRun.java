package tk.labyrinth.pandora.functions.domain.functionrun;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.bson.codecs.pojo.annotations.BsonId;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.time.Instant;
import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@ModelTagPandora
@RootModel(code = FunctionRun.MODEL_CODE, primaryAttribute = HasUid.UID_ATTRIBUTE_NAME)
@Value
@With
public class FunctionRun implements FunctionRunBase, HasUid {

	public static final String FINISHED_AT_ATTRIBUTE_NAME = "finishedAt";

	public static final String MODEL_CODE = "functionrun";

	public static final String STARTED_AT_ATTRIBUTE_NAME = "startedAt";

	List<ChildFunctionRun> children;

	// TODO: Trigger (user/routine/api).
	String createdBy;

	@Nullable
	Instant finishedAt;

	// TODO: Reference
	String functionName;

	@Nullable
	Outcome<?> outcome;

	Object parameters;

	// TODO: Add scheduledAt
	Instant startedAt;

	@BsonId
	UUID uid;

	@Nullable
	@Override
	public Integer getMultiRunIndex() {
		return null;
	}
}
