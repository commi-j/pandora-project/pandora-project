package tk.labyrinth.pandora.functions.domain.functionrun.invocation;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class FunctionRunParameters {

	Boolean async; // not, may, must

	/**
	 * This run is considered failed due to timeout if no outcome is recorded by the moment specified here.
	 */
	Instant expireAt;

	String failureReaction; // error, warn, info

	Boolean independentRun; // may, warn.
	//
	// TODO: Progress tracking settings (whether to track/persist, frequency, importance).
}
