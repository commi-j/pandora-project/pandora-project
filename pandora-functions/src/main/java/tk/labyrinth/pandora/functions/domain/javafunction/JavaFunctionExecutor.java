package tk.labyrinth.pandora.functions.domain.javafunction;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionExecutor;
import tk.labyrinth.pandora.functions.domain.function.FunctionRegistry;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.init.PandoraInitializer;

import java.util.Objects;
import java.util.UUID;

@Bean
@RequiredArgsConstructor
public class JavaFunctionExecutor implements FunctionExecutor, PandoraInitializer {

	private final ConverterRegistry converterRegistry;

	private final FunctionRegistry functionRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private Map<Function, JavaFunction<?, ?>> functionToJavaFunctionMap;

	@SmartAutowired
	private List<JavaFunction<?, ?>> javaFunctions;

	@SuppressWarnings("unchecked")
	private <P, R> Outcome<R> doExecuteFunction(ExecutionContext context, Function function, Object parameters) {
		JavaFunction<P, R> javaFunction = (JavaFunction<P, R>) functionToJavaFunctionMap.get(function).get();
		//
		P convertedParameters = converterRegistry.convertInferred(
				parameters,
				javaFunction.getReference().getParametersType());
		//
		R result = javaFunction.execute(context, convertedParameters);
		//
		return Outcome.ofResult(result);
	}

	@Override
	public Outcome<Object> executeFunction(ExecutionContext context, Function function, Object parameters) {
		return doExecuteFunction(context, function, parameters);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <P, R> TypedFunctionReference<P, R> getFunctionReference(String functionName) {
		return (TypedFunctionReference<P, R>) functionToJavaFunctionMap
				.find(tuple -> Objects.equals(tuple._1().getName(), functionName))
				.get()
				._2()
				.getReference();
	}

	public Function registerJavaFunction(JavaFunction<?, ?> javaFunction) {
		javaBaseTypeRegistry.registerJavaClass((Class<?>) javaFunction.getReference().getParametersType());
		//
		Function function = Function.builder()
				.name(javaFunction.getClass().getSimpleName())
				.parametersModel(JavaBaseTypeUtils.createDatatypeFromJavaType(javaFunction.getReference().getParametersType()))
				.resultModel(null)
				.uid(UUID.randomUUID())
				.build();
		//
		functionRegistry.registerFunction(function, this);
		//
		return function;
	}

	@Override
	public void run() {
		functionToJavaFunctionMap = List.ofAll(javaFunctions)
				.map(javaFunction -> {
					Function function = registerJavaFunction(javaFunction);
					//
					return Pair.of(function, javaFunction);
				})
				.toMap(Pair::getLeft, Pair::getRight);
	}
}
