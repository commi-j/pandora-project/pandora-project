package tk.labyrinth.pandora.functions.view.domain.routine;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.expresso.query.lang.search.Sort;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.functions.domain.function.FunctionRegistry;
import tk.labyrinth.pandora.functions.domain.functionrun.FunctionRunUtils;
import tk.labyrinth.pandora.functions.domain.routine.Routine;
import tk.labyrinth.pandora.functions.domain.routine.RoutineRegistry;
import tk.labyrinth.pandora.functions.domain.routine.RoutineUtils;
import tk.labyrinth.pandora.functions.view.domain.function.FunctionParametersViewProvider;
import tk.labyrinth.pandora.functions.view.tool.RenderUtils;
import tk.labyrinth.pandora.functions.view.tool.ViewUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.time.Instant;

@RequiredArgsConstructor
@Route(value = "pandora/routines", layout = ConfigurableAppLayout.class)
public class RoutinesPage extends Div implements BeforeEnterObserver {
//	private final AccessManager accessManager;

	private final BoxRendererRegistry boxRendererRegistry;

	private final FunctionRegistry functionRegistry;

	private final RoutineRegistry routineRegistry;

	@SmartAutowired
	private List<FunctionParametersViewProvider> functionParametersViewProviders;

	@SmartAutowired
	private TypedObjectSearcher<Routine> routineSearcher;
	//
//	@All
//	private java.util.List<BoxRendererContributor<?>> boxRendererContributors;

	@PostConstruct
	private void postConstruct() {
		System.out.println();
	}

	private Component render() {
		boolean isAdmin = true;
//				accessManager.isCurrentUserAdmin();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			List<Routine> routines = routineSearcher.search(PlainQuery.builder()
					.sort(Sort.by(Routine.EFFECTIVE_SINCE_ATTRIBUTE_NAME, Sort.Direction.DESCENDING))
					.build());
			{
				layout.addHorizontalLayout(toolbar -> {
					{
						toolbar.addClassName(PandoraStyles.LAYOUT);
					}
					{
						toolbar.addStretch();
						//
						toolbar.add(ButtonRenderer.render(builder -> builder
								.enabled(isAdmin)
								.onClick(event -> {
									// TODO
//									showEditDialog(workspace, Routine.builder().build())
								})
								.text("Add")
								.themeVariants(ButtonVariant.LUMO_PRIMARY)
								.build()));
					}
				});
			}
			{
				Grid<Routine> grid = ViewUtils.createGrid();
				{
					grid
							.addColumn(Routine::getName)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addColumn(Routine::getFunctionName)
							.setHeader("Function")
							.setResizable(true);
					grid
							.addColumn(Routine::getParameters)
							.setHeader("Parameters")
							.setResizable(true);
					grid
							.addColumn(item -> item.getCron() != null ? item.getCron().asString() : null)
							.setHeader("Cron")
							.setResizable(true);
					grid
							.addColumn(item -> {
								Instant nextExecution = RoutineUtils.computeNextExecution(item);
								//
								return nextExecution != null ? FunctionRunUtils.formatToSeconds(nextExecution) : null;
							})
							.setHeader("Next Run")
							.setResizable(true);
					grid
							.addColumn(Routine::getEnabled)
							.setHeader("Enabled")
							.setResizable(true);
					grid
							.addColumn(item -> RenderUtils.format(item.getEffectiveSince()))
							.setHeader("Effective Since")
							.setResizable(true);
					grid
							.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
									.icon(VaadinIcon.EDIT.create())
									.onClick(event -> {
										// TODO
//										showEditDialog(workspace, item);
									})
									.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
									.build()))
							.setFlexGrow(0)
							.setResizable(false);
				}
				{
					grid.setItems(routines.asJava());
				}
				layout.add(grid);
			}
		}
		return layout;
	}
//	private void showEditDialog(@NonNull Workspace workspace, @NonNull Routine object) {
//		boolean isAdmin = accessManager.isCurrentUserAdmin();
//		//
//		ConfirmationViews
//				.showViewFunctionDialog(
//						object,
//						(next, sink) -> {
//							List<Function> functions = functionRegistry.getFunctions();
//							Function function = next.getFunctionName() != null
//									? functions
//									.filter(functionElement -> Objects.equals(functionElement.getName(), next.getFunctionName()))
//									.getOrNull()
//									: null;
//							FunctionParametersViewProvider parametersViewProvider = function != null
//									? List.ofAll(functionParametersViewProviders)
//									.filter(provider -> provider.supports(function))
//									.headOption()
//									.getOrNull()
//									: null;
//							//
//							ContainerWrapper<CssVerticalLayout> layout = FunctionalComponents.createContainer(CssVerticalLayout::new);
//							{
//								layout.addClassName(PandoraStyles.LAYOUT);
//							}
//							{
//								{
//									// FIXME
//									layout.getContainer().getChildren()
//											.filter(H3.class::isInstance)
//											.forEach(Component::removeFromParent);
//									layout.getElement().insertChild(
//											0,
//											new H3("%s Routine".formatted(next.getUid() != null ? "Edit" : "Create")).getElement());
//								}
//								{
//									ContainerWrapper<CssVerticalLayout> formLayout = FunctionalComponents
//											.createContainer(CssVerticalLayout::new);
//									{
//										formLayout.addClassName(PandoraStyles.LAYOUT);
//									}
//									{
//										formLayout.add(StringBoxRenderer.render(builder -> builder
//												.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.VIEWABLE)
//												.currentValue(next.getName())
//												.initialValue(object.getName())
//												.label("Name")
//												.onValueChange(nextValue -> sink.accept(next.withName(nextValue)))
//												.build()));
//										//
//										formLayout.add(SelectRenderer.<String>render(builder -> builder
//												.items(functions
//														.map(Function::getName)
//														.asJava())
//												.label("Function")
//												.onValueChange(nextValue -> sink.accept(next.withFunctionName(nextValue)))
//												.readOnly(!isAdmin)
//												.value(next.getFunctionName())
//												.build()));
//										//
//										formLayout.add(TextFieldRenderer.render(builder -> builder
//												.label("Parameters")
//												.readOnly(true)
//												.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
//														.enabled(isAdmin && parametersViewProvider != null)
//														.icon(VaadinIcon.COG.create())
//														.onClick(event -> {
//															Map<String, Object> context = HashMap.of("workspace", workspace);
//															//
//															parametersViewProvider
//																	.showFunctionParametersView(context, function, next.getParameters())
//																	.subscribeAlwaysAccepted(result -> sink.accept(next.withParameters(result)));
//														})
//														.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
//														.build()))
//												.value(next.getParameters() != null ? next.getParameters().toString() : "")
//												.build()));
//										//
//										formLayout.add(CronBoxRenderer
//												.render(builder -> builder
//														.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.VIEWABLE)
//														.currentValue(next.getCron())
//														.initialValue(object.getCron())
//														.label("Cron")
//														.onValueChange(nextValue -> sink.accept(next.withCron(nextValue)))
//														.build())
//												.asVaadinComponent());
//										//
//										formLayout.add(CheckboxRenderer.render(builder -> builder
//												.enabled(isAdmin)
//												.label("Enabled")
//												.onValueChange(nextValue -> sink.accept(next.withEnabled(nextValue)))
//												.value(next.getEnabled() != null ? next.getEnabled() : false)
//												.build()));
//									}
//									layout.add(formLayout.getContainer());
//								}
//								return layout.asView();
//							}
//						})
//				.subscribeAlwaysAccepted(result -> {
//					if (isAdmin) {
//						Routine routine = result.toBuilder()
//								.effectiveSince(Instant.now())
//								.uid(result.getUid() != null ? result.getUid() : UUID.randomUUID())
//								.build();
//						//
//						routineRepository.persistOrUpdate(routine);
//						//
//						// FIXME: This should be a manipulation listener.
//						routineRegistry.updateRoutine(routine);
//						//
//						// TODO: We just need to refresh the content here.
//						UI.getCurrent().getPage().reload();
//					}
//				});
//}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(this::render)
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}
}
