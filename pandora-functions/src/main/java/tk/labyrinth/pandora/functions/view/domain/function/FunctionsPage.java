package tk.labyrinth.pandora.functions.view.domain.function;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionRegistry;
import tk.labyrinth.pandora.functions.domain.functionrun.FunctionManager;
import tk.labyrinth.pandora.functions.view.tool.ViewUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Route(value = "pandora/functions", layout = ConfigurableAppLayout.class)
public class FunctionsPage extends Div implements BeforeEnterObserver {
//	private final AccessManager accessManager;

	private final FunctionManager functionManager;

	private final FunctionRegistry functionRegistry;

	@SmartAutowired
	private List<FunctionParametersViewProvider> functionParametersViewProviders;
	//
//	private final UserStateHandler userStateHandler;

	private Component render() {
		boolean isAdmin = true;
//				accessManager.isCurrentUserAdmin();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			List<Function> functions = functionRegistry.getFunctions();
			{
				Grid<Function> grid = ViewUtils.createGrid();
				{
					grid
							.addColumn(Function::getName)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addColumn(Function::getParametersModel)
							.setHeader("Parameters Model")
							.setResizable(true);
					grid
							.addColumn(Function::getResultModel)
							.setHeader("Result Model")
							.setResizable(true);
					grid
							.addComponentColumn(item -> {
								FunctionParametersViewProvider viewProvider = List.ofAll(functionParametersViewProviders)
										.filter(provider -> provider.supports(item))
										.headOption()
										.getOrNull();
								//
								return ButtonRenderer.render(builder -> builder
										.enabled(isAdmin && viewProvider != null)
										.onClick(event -> viewProvider.showFunctionParametersView(HashMap.empty(), item, null)
												.subscribeAlwaysAccepted(result -> functionManager.runFunctionAsync(
														"user:%s".formatted(
																"TODO"
//																userStateHandler.getUserState().getAuthenticationInformation().getSubject()
														),
														item.getName(),
														result)))
										.text("Run")
										.themeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
										.build());
							})
							.setFlexGrow(0);
				}
				{
					grid.setItems(functions.asJava());
				}
				layout.add(grid);
			}
		}
		return layout;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(this::render)
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}
}
