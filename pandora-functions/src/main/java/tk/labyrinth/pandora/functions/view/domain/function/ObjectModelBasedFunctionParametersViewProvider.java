package tk.labyrinth.pandora.functions.view.domain.function;

import io.vavr.collection.Map;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.Ordered;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;
import tk.labyrinth.pandora.ui.domain.form.FormsTool;

@Bean
@Priority(Ordered.LOWEST_PRECEDENCE)
@RequiredArgsConstructor
public class ObjectModelBasedFunctionParametersViewProvider implements FunctionParametersViewProvider {

	private final ConverterRegistry converterRegistry;

	private final FormsTool formsTool;

	private final ObjectModelRegistry objectModelRegistry;

	@Override
	public ConfirmationHandle<Object> showFunctionParametersView(
			Map<String, Object> context,
			Function function,
			@Nullable Object parameters) {
		ObjectModel objectModel = objectModelRegistry.getObjectModel(function.getParametersModel());
		//
		return formsTool
				.showDialog(
						objectModel,
						parameters != null ? converterRegistry.convert(parameters, GenericObject.class) : GenericObject.empty())
				.map(value -> value);
	}

	@Override
	public boolean supports(Function function) {
		return function.getParametersModel() != null;
	}
}
