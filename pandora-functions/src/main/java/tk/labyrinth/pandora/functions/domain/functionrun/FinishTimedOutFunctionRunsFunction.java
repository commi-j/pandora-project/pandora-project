package tk.labyrinth.pandora.functions.domain.functionrun;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.fault.StructuredFault;
import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.time.Duration;
import java.time.Instant;

@Bean
@RequiredArgsConstructor
public class FinishTimedOutFunctionRunsFunction implements JavaFunction<Void, Integer> {

	public static final TypedFunctionReference<Void, Integer> REFERENCE =
			new ClassBasedFunctionReference<>(FinishTimedOutFunctionRunsFunction.class);

	@SmartAutowired
	private TypedObjectManipulator<FunctionRun> functionRunManipulator;

	@SmartAutowired
	private TypedObjectSearcher<FunctionRun> functionRunSearcher;

	@Override
	public Integer execute(ExecutionContext context, Void parameters) {
		List<FunctionRun> timedOutFunctionRuns = functionRunSearcher.search(Predicates.and(
				Predicates.notEqualTo(FunctionRun.FINISHED_AT_ATTRIBUTE_NAME, null),
				Predicates.lessThan(FunctionRun.STARTED_AT_ATTRIBUTE_NAME, Instant.now().minus(Duration.ofHours(1)))));
		//
		timedOutFunctionRuns.forEach(timedOutFunctionRun -> functionRunManipulator.update(timedOutFunctionRun.toBuilder()
				.finishedAt(Instant.now())
				.outcome(Outcome.ofFault(StructuredFault.of("Timed out")))
				.build()));
		//
		return timedOutFunctionRuns.size();
	}

	@Override
	public TypedFunctionReference<Void, Integer> getReference() {
		return REFERENCE;
	}
}
