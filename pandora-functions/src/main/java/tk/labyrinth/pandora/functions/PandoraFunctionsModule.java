package tk.labyrinth.pandora.functions;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class PandoraFunctionsModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
