package tk.labyrinth.pandora.functions.domain.functionrun;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functions.domain.function.FunctionRegistry;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContextImpl;
import tk.labyrinth.pandora.functions.domain.functionexecution.Path;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicReference;

@Bean
@RequiredArgsConstructor
@Slf4j
public class FunctionManager {

	private final FunctionRegistry functionRegistry;

	private final ObjectMapper objectMapper;

	@SmartAutowired
	private TypedObjectManipulator<FunctionRun> functionRunManipulator;

	// TODO: functionName should be FunctionReference (or may be add override).
	public Outcome<Object> runFunction(String createdBy, String functionName, Object parameters) {
		AtomicReference<FunctionRun> functionRunReference = new AtomicReference<>(FunctionRun.builder()
				.createdBy(createdBy)
				.uid(UUID.randomUUID())
				.build());
		//
		logger.info("Starting FunctionRun: functionName = {}, uid = {}", functionName, functionRunReference.get().getUid());
		//
		ExecutionContext executionContext = new ExecutionContextImpl(
				functionRegistry,
				null,
				objectMapper,
				(path, childFunctionRun) -> {
					FunctionRun functionRun = computeNextFunctionRun(functionRunReference.get(), path, childFunctionRun);
					//
					functionRunManipulator.createOrUpdate(functionRun);
					//
					functionRunReference.set(functionRun);
				},
				Path.empty());
		//
		Outcome<Object> outcome;
		try {
			outcome = executionContext.runFunction(
					functionRegistry.getFunctionReference(functionName),
					parameters);
		} catch (RuntimeException ex) {
			logger.error("", ex);
			//
			throw new RuntimeException(ex);
		}
		//
		logger.info("Finished FunctionRun: functionName = {}, uid = {}", functionName, functionRunReference.get().getUid());
		//
		return outcome;
	}

	// TODO: functionName should be FunctionReference (or may be add override).
	public void runFunctionAsync(String createdBy, String functionName, Object parameters) {
		ForkJoinPool.commonPool().execute(() -> runFunction(createdBy, functionName, parameters));
	}

	public static FunctionRun computeNextFunctionRun(
			FunctionRun currentFunctionRun,
			Path<Integer> path,
			ChildFunctionRun childFunctionRun) {
		FunctionRun result;
		{
			if (Objects.equals(path, Path.first(0))) {
				result = currentFunctionRun.toBuilder()
						.finishedAt(childFunctionRun.getFinishedAt())
						.functionName(childFunctionRun.getFunctionName())
						.outcome(childFunctionRun.getOutcome())
						.parameters(childFunctionRun.getParameters())
						.startedAt(childFunctionRun.getStartedAt())
						.build();
			} else {
				result = currentFunctionRun.withChildren(updateChild(
						currentFunctionRun.getChildren(),
						path.tail(),
						childFunctionRun));
			}
		}
		return result;
	}

	public static List<ChildFunctionRun> updateChild(
			@Nullable List<ChildFunctionRun> currentChildren,
			Path<Integer> path,
			ChildFunctionRun child) {
		List<ChildFunctionRun> result;
		{
			Integer index = path.getFirst();
			//
			if (path.size() > 1) {
				ChildFunctionRun localChild = currentChildren.get(index);
				//
				result = currentChildren.update(
						index,
						localChild.withChildren(updateChild(localChild.getChildren(), path.tail(), child)));
			} else {
				if (currentChildren != null) {
					result = currentChildren.size() > index
							? currentChildren.update(index, child)
							: currentChildren.append(child);
				} else {
					if (index == 0) {
						result = List.of(child);
					} else {
						throw new IllegalArgumentException();
					}
				}
			}
		}
		return result;
	}
}
