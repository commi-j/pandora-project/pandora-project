package tk.labyrinth.pandora.functions.domain.function;

import com.vaadin.flow.component.html.H3;
import io.vavr.collection.Map;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functions.view.domain.function.FunctionParametersViewProvider;

@Bean
public class VoidFunctionParametersViewProvider implements FunctionParametersViewProvider {

	@Override
	public ConfirmationHandle<Object> showFunctionParametersView(
			Map<String, Object> context,
			Function function,
			@Nullable Object parameters) {
		if (parameters != null) {
			throw new IllegalArgumentException("Require null parameters: %s".formatted(parameters));
		}
		//
		return ConfirmationViews
				.showComponentDialog(new H3("No Parameters"))
				.map(success -> null);
	}

	@Override
	public boolean supports(Function function) {
		return function.getParametersModel() == null;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Data
	@FieldDefaults(level = AccessLevel.PRIVATE)
	public static class EditableParameters {

		Boolean newOnly;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		Boolean newOnly;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Result {
		// empty
	}
}
