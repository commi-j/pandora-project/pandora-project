package tk.labyrinth.pandora.functions.domain.function;

import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;

public interface FunctionExecutor {

	Outcome<Object> executeFunction(ExecutionContext context, Function function, Object parameters);

	<P, R> TypedFunctionReference<P, R> getFunctionReference(String functionName);
}
