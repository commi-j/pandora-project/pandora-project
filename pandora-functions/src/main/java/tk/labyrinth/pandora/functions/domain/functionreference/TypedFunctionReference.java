package tk.labyrinth.pandora.functions.domain.functionreference;

import java.lang.reflect.Type;

public interface TypedFunctionReference<P, R> {

	String getName();

	Type getParametersType();

	Type getResultType();
}
