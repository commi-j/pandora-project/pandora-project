package tk.labyrinth.pandora.functions.domain.function;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class Function {

	String name;

	// TODO: Could it be null?
	Datatype parametersModel;

	// TODO: Could it be null?
	Datatype resultModel;

	@Deprecated // Why do we need it? We should have these as proper model.
	UUID uid;
}
