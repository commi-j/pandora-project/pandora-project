package tk.labyrinth.pandora.functions.domain.functionexecution;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionExecutor;
import tk.labyrinth.pandora.functions.domain.function.FunctionRegistry;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionrun.ChildFunctionRun;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.function.BiConsumer;

@RequiredArgsConstructor
public class ExecutionContextImpl implements ExecutionContext {

	private final FunctionRegistry functionRegistry;

	@Nullable
	private final Integer multiRunIndex;

	private final ObjectMapper objectMapper;

	private final BiConsumer<Path<Integer>, ChildFunctionRun> onFunctionRunUpdate;

	private final Path<Integer> parentPath;

	@Nullable
	@Override
	public Integer getMultiRunIndex() {
		return multiRunIndex;
	}

	public <R> Outcome<R> mapOutcome(Outcome<?> outcome, Type resultType) {
		return outcome.isSuccess()
				? Outcome.ofResult(objectMapper.convertValue(
				outcome.getResult(),
				objectMapper.constructType(resultType)))
				: Outcome.ofFault(outcome.getFault());
	}

	@Override
	public <P, R> List<Outcome<R>> multiRunFunction(
			TypedFunctionReference<P, R> functionReference,
			List<P> parameterses) {
		Pair<Function, FunctionExecutor> functionAndExecutor = functionRegistry
				.getFunctionAndExecutor(functionReference.getName());
		Function function = functionAndExecutor.getLeft();
		FunctionExecutor functionInvoker = functionAndExecutor.getRight();
		//
		List<Outcome<Object>> outcomes = parameterses
				.zipWithIndex()
				.map(tuple -> {
					Integer multiRunIndex = tuple._2();
					Object parameters = tuple._1();
					//
					Path<Integer> currentPath = parentPath.append(multiRunIndex != null ? multiRunIndex : 0);
					//
					ChildFunctionRun functionRun = ChildFunctionRun.builder()
							.functionName(functionReference.getName())
							.multiRunIndex(multiRunIndex)
							.parameters(parameters)
							.startedAt(Instant.now())
							.build();
					//
					onFunctionRunUpdate.accept(currentPath, functionRun);
					//
					ExecutionContext executionContext = new ExecutionContextImpl(
							functionRegistry,
							multiRunIndex,
							objectMapper,
							onFunctionRunUpdate,
							currentPath);
					//
					Try<Outcome<Object>> resultTry = Try.ofSupplier(() -> functionInvoker.executeFunction(
							executionContext,
							function,
							parameters));
					//
					Outcome<Object> outcome = resultTry.isSuccess() ? resultTry.get() : Outcome.ofThrowable(resultTry.getCause());
					//
					onFunctionRunUpdate.accept(
							currentPath,
							functionRun.toBuilder()
									.finishedAt(Instant.now())
									.outcome(outcome)
									.build());
					//
					return outcome;
				});
		//
		// TODO: Check for failures and probably fail itself.
		//
		List<Outcome<R>> mappedOutcomes = outcomes.map(outcome ->
				mapOutcome(outcome, functionReference.getResultType()));
		//
		return mappedOutcomes;
	}

	@Override
	public <P, R> Outcome<R> runFunction(TypedFunctionReference<P, R> functionReference, P parameters) {
		Pair<Function, FunctionExecutor> functionAndExecutor = functionRegistry
				.getFunctionAndExecutor(functionReference.getName());
		Function function = functionAndExecutor.getLeft();
		FunctionExecutor functionInvoker = functionAndExecutor.getRight();
		//
		Path<Integer> currentPath = parentPath.append(0);
		//
		ChildFunctionRun functionRun = ChildFunctionRun.builder()
				.functionName(functionReference.getName())
				.parameters(parameters)
				.startedAt(Instant.now())
				.build();
		//
		onFunctionRunUpdate.accept(currentPath, functionRun);
		//
		ExecutionContext executionContext = new ExecutionContextImpl(
				functionRegistry,
				null,
				objectMapper,
				onFunctionRunUpdate,
				currentPath);
		//
		Try<Outcome<Object>> resultTry = Try.ofSupplier(() -> functionInvoker.executeFunction(
				executionContext,
				function,
				parameters));
		//
		Outcome<Object> outcome = resultTry.isSuccess() ? resultTry.get() : Outcome.ofThrowable(resultTry.getCause());
		//
		onFunctionRunUpdate.accept(
				currentPath,
				functionRun.toBuilder()
						.finishedAt(Instant.now())
						.outcome(outcome)
						.build());
		//
		Outcome<R> mappedOutcome = mapOutcome(outcome, functionReference.getResultType());
		//
		return mappedOutcome;
	}
}
