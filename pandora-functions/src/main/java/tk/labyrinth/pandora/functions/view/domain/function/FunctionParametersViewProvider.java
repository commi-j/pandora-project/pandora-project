package tk.labyrinth.pandora.functions.view.domain.function;

import io.vavr.collection.Map;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functions.domain.function.Function;

public interface FunctionParametersViewProvider {

	ConfirmationHandle<Object> showFunctionParametersView(
			Map<String, Object> context,
			Function function,
			@Nullable Object parameters);

	boolean supports(Function function);
}
