package tk.labyrinth.pandora.functions.domain.routine;

import com.cronutils.model.Cron;
import com.cronutils.model.time.ExecutionTime;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;
import java.time.ZoneOffset;

public class RoutineUtils {

	@Nullable
	public static Instant computeNextExecution(Routine routine) {
		Cron cron = routine.getCron();
		//
		return cron != null
				? ExecutionTime.forCron(cron).nextExecution(Instant.now().atZone(ZoneOffset.UTC)).orElseThrow().toInstant()
				: null;
	}
}
