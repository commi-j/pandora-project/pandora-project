package tk.labyrinth.pandora.functions.domain.function;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Bean
@RequiredArgsConstructor
public class FunctionRegistry {

	private final ConcurrentMap<String, Pair<Function, FunctionExecutor>> functionAndExecutor = new ConcurrentHashMap<>();

	public Pair<Function, FunctionExecutor> getFunctionAndExecutor(String functionName) {
		return functionAndExecutor.get(functionName);
	}

	public FunctionExecutor getFunctionExecutor(String functionName) {
		return functionAndExecutor.get(functionName).getRight();
	}

	public <P, R> TypedFunctionReference<P, R> getFunctionReference(String functionName) {
		return getFunctionExecutor(functionName).getFunctionReference(functionName);
	}

	public List<Function> getFunctions() {
		return List.ofAll(functionAndExecutor.values()).map(Pair::getLeft);
	}

	public void registerFunction(Function function, FunctionExecutor functionExecutor) {
		functionAndExecutor.put(function.getName(), Pair.of(function, functionExecutor));
	}
}
