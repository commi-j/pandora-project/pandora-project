package tk.labyrinth.pandora.functions.domain.routine;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeListener;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

@Bean
@RequiredArgsConstructor
public class RegisteringRoutineChangeListener extends TypedObjectChangeListener<Routine, UidReference<Routine>> {

	private final RoutineRegistry routineRegistry;

	@Override
	protected void onObjectChange(TypedObjectChangeEvent<Routine, UidReference<Routine>> event) {
		if (event.isCreateOrUpdate()) {
			routineRegistry.updateRoutine(event.getNextObjectOrFail());
		} else {
			routineRegistry.deleteRoutine(event.getPrimaryReference().getUid());
		}
	}
}
