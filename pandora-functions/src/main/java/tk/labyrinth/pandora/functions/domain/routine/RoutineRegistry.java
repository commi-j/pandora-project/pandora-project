package tk.labyrinth.pandora.functions.domain.routine;

import com.cronutils.model.Cron;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.SimpleTriggerContext;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functions.domain.functionrun.FunctionManager;
import tk.labyrinth.pandora.stores.init.PandoraInitializer;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ScheduledFuture;

@Bean
@Configuration // TODO: Not sure if making this bean configuration is ok. Conf is needed to enable scheduling.
@EnableScheduling
@RequiredArgsConstructor
@Slf4j
public class RoutineRegistry implements PandoraInitializer {

	private final FunctionManager functionManager;

	private final ConcurrentMap<UUID, ScheduledFuture<?>> futures = new ConcurrentHashMap<>();

	private final TaskScheduler taskScheduler;

	@SmartAutowired
	private TypedObjectSearcher<Routine> routineSearcher;

	private void alignScheduled(Routine routine) {
		ScheduledFuture<?> previousFuture = deleteRoutine(routine.getUid());
		//
		boolean enabled = BooleanUtils.isTrue(routine.getEnabled());
		//
		if (enabled) {
			Cron cron = routine.getCron();
			String functionName = routine.getFunctionName();
			//
			if (cron != null && functionName != null) {
				CronTrigger trigger = new CronTrigger(cron.asString());
				//
				ScheduledFuture<?> nextFuture = taskScheduler.schedule(
						() -> {
							logger.info("Running Routine Function '{}' ({})", routine.getName(), routine.getUid());
							//
							try {
								functionManager.runFunction(
										"routine:%s".formatted(routine.getUid()),
										functionName,
										routine.getParameters());
							} catch (RuntimeException ex) {
								logger.error("", ex);
							}
						},
						trigger);
				//
				futures.put(routine.getUid(), nextFuture);
				//
				Instant nextExecution = trigger.nextExecution(new SimpleTriggerContext(taskScheduler.getClock()));
				//
				if (previousFuture != null) {
					logger.info("Rescheduled Routine '{}' ({}) with next execution at {}",
							routine.getName(), routine.getUid(), nextExecution);
				} else {
					logger.info("Scheduled Routine '{}' ({}) with next execution at {}",
							routine.getName(), routine.getUid(), nextExecution);
				}
			} else {
				logger.warn("Routine '{}' ({}) is not scheduled due to missing attributes: cron = {}, functionName = {}",
						routine.getName(), routine.getUid(), cron, functionName);
			}
		} else {
			if (previousFuture != null) {
				logger.info("Unscheduled Routine '{}' ({})", routine.getName(), routine.getUid());
			}
		}
	}

	public ScheduledFuture<?> deleteRoutine(UUID routineUid) {
		ScheduledFuture<?> future = futures.get(routineUid);
		//
		if (future != null) {
			future.cancel(false);
		}
		//
		return future;
	}

	@Override
	public void run() {
		List<Routine> enabledRoutines = routineSearcher.search(Predicates.equalTo(Routine.ENABLED_ATTRIBUTE_NAME, true));
		//
		enabledRoutines.forEach(this::alignScheduled);
	}

	public void updateRoutine(Routine routine) {
		alignScheduled(routine);
	}
}
