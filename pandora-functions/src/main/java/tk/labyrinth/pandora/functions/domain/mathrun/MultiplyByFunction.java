package tk.labyrinth.pandora.functions.domain.mathrun;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.outcome.Outcome;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;
import tk.labyrinth.pandora.functions.view.domain.function.FunctionParametersViewProvider;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class MultiplyByFunction implements
		FunctionParametersViewProvider,
		JavaFunction<MultiplyByFunction.Parameters, MultiplyByFunction.Result> {

	public static final TypedFunctionReference<Parameters, Result> REFERENCE =
			new ClassBasedFunctionReference<>(MultiplyByFunction.class);

	private final ObjectMapper objectMapper;

	@Override
	public Result execute(ExecutionContext context, Parameters parameters) {
		List<Integer> values = context
				.multiRunFunction(
						MultiplyFunction.REFERENCE,
						List.rangeClosed(1, 10).map(right -> MultiplyFunction.Parameters.builder()
								.left(parameters.value())
								.right(right)
								.build()))
				.map(Outcome::getResult);
		//
		return Result.builder()
				.values(values)
				.build();
	}

	@Override
	public TypedFunctionReference<Parameters, Result> getReference() {
		return REFERENCE;
	}

	@Override
	public ConfirmationHandle<Object> showFunctionParametersView(
			Map<String, Object> context,
			Function function,
			@Nullable Object parameters) {
		Parameters castedParameters = objectMapper.convertValue(parameters, Parameters.class);
		//
		Binder<EditableParameters> binder = new Binder<>();
		binder.setBean(EditableParameters.builder()
				.value(castedParameters != null ? castedParameters.value() : null)
				.build());
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			{
				layout.add(new H3("Parameters"));
			}
			{
				IntegerField valueField = new IntegerField("Value");
				//
				valueField.setRequired(true);
				//
				binder.bind(valueField, EditableParameters::value, EditableParameters::value);
				//
				layout.add(valueField);
			}
		}
		//
		return ConfirmationViews.showComponentDialog(
				layout,
				() -> {
					EditableParameters value = binder.getBean();
					//
					return Parameters.builder()
							.value(value.value())
							.build();
				});
	}

	@Override
	public boolean supports(Function function) {
		return Objects.equals(function.getName(), getClass().getSimpleName());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Data
	@FieldDefaults(level = AccessLevel.PRIVATE)
	public static class EditableParameters {

		Integer value;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Integer value;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Result {

		List<Integer> values;
	}
}
