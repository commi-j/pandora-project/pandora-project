package tk.labyrinth.pandora.functions.domain.mathrun;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;

@Bean
public class MultiplyFunction implements JavaFunction<MultiplyFunction.Parameters, Integer> {

	public static final TypedFunctionReference<Parameters, Integer> REFERENCE =
			new ClassBasedFunctionReference<>(MultiplyFunction.class);

	@Override
	public Integer execute(ExecutionContext context, Parameters parameters) {
		return parameters.getLeft() * parameters.getRight();
	}

	@Override
	public TypedFunctionReference<Parameters, Integer> getReference() {
		return REFERENCE;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Integer left;

		@NonNull
		Integer right;
	}
}
