package tk.labyrinth.pandora.functions.domain.javafunction;

import org.checkerframework.checker.nullness.qual.PolyNull;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;

/**
 * @param <P> Parameters
 * @param <R> Result
 */
public interface JavaFunction<@PolyNull P, @PolyNull R> {

	R execute(ExecutionContext context, P parameters);

	TypedFunctionReference<P, R> getReference();
}
