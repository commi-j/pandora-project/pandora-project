package tk.labyrinth.pandora.applicationtests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled("This is not for automated run (for now)")
class UiTests {

	@Test
	void testEnumBoxProperlyHandlesNullValue() {
		// FIXME: Port should be aligned with app.
		Selenide.open("localhost:8083/demo/value-boxes");
		//
		// TODO
//		{
//			// Go to Models.
//			//
//			Selenide.$(By.linkText("BeholderModels")).click();
//		}
//		{
//			// Create Model.
//			//
//			Selenide.$(By.name("create-object-button")).click();
//			//
//			Selenide.$(By.name("attribute.code")).$(By.tagName("input")).setValue("autotestobject");
//			Selenide.$(By.name("attribute.name")).$(By.tagName("input")).setValue("Autotest Object");
//			//
//			Selenide.$(By.name("confirm-button")).click();
////			Selenide.$(By.name(ConfirmationViews.CONFIRM_BUTTON_NAME)).click();
//		}
//		{
//			// Filter Model.
//			//
//			Selenide.$(By.name("filter.name")).click();
//			//
//			Selenide.$(By.name("value-box")).$(By.tagName("input")).setValue("Autotest Object");
//			//
//			Selenide.$(By.name("confirm-button")).click();
//		}
//		{
//			// Delete Models (could be more than 1 if previous tests failed, deleting them all).
//			//
//			SelenideElement deleteButton;
//			int count = 0;
//			while ((deleteButton = Selenide.$(By.name("delete-object-button"))).isDisplayed()) {
//				deleteButton.click();
//				Selenide.$(By.name("confirm-button")).click();
//				//
//				count++;
//			}
//			Assertions.assertTrue(count > 0);
//		}
		//
//		Selenide.sleep(Long.MAX_VALUE);
	}

	@BeforeAll
	static void beforeAll() {
		Configuration.baseUrl = "";
		Configuration.browser = "firefox";
	}
}
