package tk.labyrinth.pandora.diversity.domain.bean;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Bean
@Documented
@Eager
@org.springframework.context.annotation.Lazy(value = false)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface EagerBean {
	// empty
}
