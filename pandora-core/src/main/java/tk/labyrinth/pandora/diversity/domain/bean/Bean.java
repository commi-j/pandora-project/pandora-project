package tk.labyrinth.pandora.diversity.domain.bean;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation can be put on a class or a method to add it to the DI context.
 * It supports <b>Spring</b> and <b>Quarkus</b> frameworks and ensures they act the same way:<br>
 * - For class, the instance is created by the DI framework.<br>
 * - For method, it is invoked and result is used as an instance.<br>
 * - Bean is lazy, i.e. it is created only when requested by the context.<br>
 * - This annotation works as meta-annotation, i.e. it can be put on another annotation and will propagate
 * all the features above to that annotation.<br>
 */
@jakarta.inject.Singleton
@org.springframework.context.annotation.Bean
@org.springframework.stereotype.Component
@org.springframework.context.annotation.Lazy
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Bean {
	// empty
}
