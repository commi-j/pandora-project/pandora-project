package tk.labyrinth.pandora.core.tool.converter.common;

import org.springframework.core.ResolvableType;

import java.lang.reflect.Type;
import java.util.Arrays;

public class ResolvableTypeUtils {

	public static ResolvableType createParameterized(Class<?> baseClass, Type... parameterTypes) {
		return ResolvableType.forClassWithGenerics(
				baseClass,
				Arrays.stream(parameterTypes)
						.map(ResolvableType::forType)
						.toArray(ResolvableType[]::new));
	}
}
