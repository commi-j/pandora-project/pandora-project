package tk.labyrinth.pandora.core.tool.converter.common;

import org.springframework.core.ResolvableType;
import org.springframework.core.convert.TypeDescriptor;

import java.lang.reflect.Type;

public class TypeDescriptorUtils {

	public static TypeDescriptor createParameterized(Class<?> baseClass, Type... parameterTypes) {
		return new TypeDescriptor(
				ResolvableTypeUtils.createParameterized(baseClass, parameterTypes),
				null,
				null);
	}

	public static TypeDescriptor from(Type javaType) {
		return new TypeDescriptor(ResolvableType.forType(javaType), null, null);
	}

	public static TypeDescriptor getParameter(TypeDescriptor typeDescriptor, int index) {
		return new TypeDescriptor(typeDescriptor.getResolvableType().getGeneric(index), null, null);
	}
}
