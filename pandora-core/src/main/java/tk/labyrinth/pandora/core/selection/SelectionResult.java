package tk.labyrinth.pandora.core.selection;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class SelectionResult<P, R> {

	@NonNull
	EvaluatedResults<R> evaluatedResults;

	@NonNull
	List<SelectionResult<?, ?>> intermediaryResults;

	P parameters;

	@Nullable
	public R findResult() {
		R result;
		{
			List<R> handlers = getResults();
			//
			if (handlers.size() > 1) {
				throw new SelectionException("Multiple found", this);
			}
			//
			result = !handlers.isEmpty() ? handlers.get(0) : null;
		}
		return result;
	}

	@Nullable
	public Integer getBestDistance() {
		return evaluatedResults.getBestDistance();
	}

	public R getResult() {
		R result = findResult();
		//
		if (result == null) {
			throw new SelectionException("Not found", this);
		}
		//
		return result;
	}

	public List<R> getResults() {
		List<Pair<Integer, List<R>>> filteredResults = evaluatedResults.getValue().filter(pair -> pair.getLeft() != null);
		//
		return !filteredResults.isEmpty() ? filteredResults.get(0).getRight() : List.empty();
	}

	public boolean hasResult() {
		return findResult() != null;
	}

	public <R2> SelectionResult<P, R2> map(Function<R, EvaluatedResults<R2>> mapFunction) {
		R result = findResult();
		//
		return new SelectionResult<>(
				result != null ? mapFunction.apply(result) : EvaluatedResults.empty(),
				intermediaryResults.append(this),
				parameters);
	}

	public SelectionResult<P, R> mergeWith(SelectionResult<P, R> other) {
		if (parameters != other.parameters) {
			throw new IllegalArgumentException("To merge SelectionsResults must have same parameters");
		}
		return of(
				parameters,
				evaluatedResults.getValue().appendAll(other.evaluatedResults.getValue())
						.groupBy(Pair::getLeft)
						.filterKeys(Objects::nonNull)
						.mapValues(pairs -> pairs.flatMap(Pair::getRight))
						.map(tuple -> Pair.of(tuple._1(), tuple._2()))
						.toList()
						.sorted(Comparator.comparing(Pair::getLeft)));
	}

	public static <P, R> SelectionResult<P, R> empty(P parameters) {
		return of(parameters, List.empty());
	}

	public static <P, R> SelectionResult<P, R> of(P parameters, List<Pair<Integer, List<R>>> evaluatedResults) {
		return new SelectionResult<>(EvaluatedResults.of(evaluatedResults), List.empty(), parameters);
	}

	public static <P, R> SelectionResult<P, R> ofResult(P parameters, R result) {
		return ofZeroDistance(parameters, List.of(result));
	}

	public static <P, R> SelectionResult<P, R> ofZeroDistance(P parameters, List<R> zeroDistanceResults) {
		return of(parameters, List.of(Pair.of(0, zeroDistanceResults)));
	}
}
