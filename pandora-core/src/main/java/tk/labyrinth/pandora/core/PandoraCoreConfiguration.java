package tk.labyrinth.pandora.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.core.admin.endpoint.HealthcheckEndpoint;
import tk.labyrinth.pandora.telemetry.PandoraTelemetryModule;

@Configuration
@Import({
		HealthcheckEndpoint.class,
		PandoraTelemetryModule.class,
})
public class PandoraCoreConfiguration {
	// empty
}
