package tk.labyrinth.pandora.core;

public class PandoraConstants {

	@Deprecated // Use @ModelTagPandora
	public static final String PANDORA_MODEL_TAG_NAME = "pandora";

	public static final String SETUP_MODEL_TAG_NAME = "setup";

	// TODO: Move to PandoraTemporalConstants.
	public static final String TEMPORAL_MODEL_TAG_NAME = "temporal";
}
