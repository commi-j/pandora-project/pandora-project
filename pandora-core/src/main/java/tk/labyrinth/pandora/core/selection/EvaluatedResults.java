package tk.labyrinth.pandora.core.selection;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

@Value(staticConstructor = "of")
public class EvaluatedResults<R> {

	@NonNull
	List<Pair<@Nullable Integer, List<R>>> value;

	@Nullable
	public Integer getBestDistance() {
		return !value.isEmpty() ? value.get(0).getLeft() : null;
	}

	public static <R> EvaluatedResults<R> empty() {
		return of(List.empty());
	}

	public static <R> EvaluatedResults<R> ofResult(R result) {
		return ofZeroDistance(List.of(result));
	}

	public static <R> EvaluatedResults<R> ofZeroDistance(List<R> zeroDistanceResults) {
		return of(List.of(Pair.of(0, zeroDistanceResults)));
	}
}
