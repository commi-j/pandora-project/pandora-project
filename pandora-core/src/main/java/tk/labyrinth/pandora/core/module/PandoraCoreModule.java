package tk.labyrinth.pandora.core.module;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.core.PandoraCoreConfiguration;

@Import(PandoraCoreConfiguration.class)
@Slf4j
public class PandoraCoreModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
