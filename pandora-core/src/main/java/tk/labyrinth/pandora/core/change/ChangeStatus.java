package tk.labyrinth.pandora.core.change;

import javax.annotation.CheckForNull;
import java.util.Objects;

public enum ChangeStatus {
	ADDED,
	REMOVED,
	MODIFIED,
	NOT_MODIFIED;

	public static ChangeStatus compute(@CheckForNull Object previous, @CheckForNull Object next) {
		ChangeStatus result;
		{
			if (previous != null) {
				if (next != null) {
					// previous != null && next != null
					//
					result = Objects.equals(previous, next) ? NOT_MODIFIED : MODIFIED;
				} else {
					// previous != null && next == null
					//
					result = REMOVED;
				}
			} else {
				// previous == null
				//
				result = next != null ? ADDED : NOT_MODIFIED;
			}
		}
		return result;
	}
}
