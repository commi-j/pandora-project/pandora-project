package tk.labyrinth.pandora.core.selection;

import lombok.Value;

@Value
public class SelectionException extends RuntimeException {

	String message;

	SelectionResult<?, ?> selectionResult;

	@Override
	public String getMessage() {
		return "%s: parameters = %s, evaluatedResults = %s".formatted(
				message, selectionResult.getParameters(), selectionResult.getEvaluatedResults());
	}
}
