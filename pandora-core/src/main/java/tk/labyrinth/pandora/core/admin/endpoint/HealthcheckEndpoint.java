package tk.labyrinth.pandora.core.admin.endpoint;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@Slf4j
public class HealthcheckEndpoint {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}

	@GetMapping("/admin/healthcheck")
	public String healthcheck() {
		return "Healthy at " + Instant.now();
	}
}
