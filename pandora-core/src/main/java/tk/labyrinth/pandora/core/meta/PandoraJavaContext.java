package tk.labyrinth.pandora.core.meta;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker interface to track all annotations and interfaces that are designed
 * to enrich Pandora's context with information from Java classes.<br>
 * These are not exposed to the Pandora's overview.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PandoraJavaContext {
	// empty
}
