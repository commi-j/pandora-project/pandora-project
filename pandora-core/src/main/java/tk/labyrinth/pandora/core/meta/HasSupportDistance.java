package tk.labyrinth.pandora.core.meta;

import io.vavr.collection.List;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.BiFunction;

/**
 * Interface to indicate that this component may participate in challenge for target handling.
 * This is a variant of ordering mechanics to detect handler when multiple are available with two main differences:<br>
 * - It is challenged against individual target (rather than class or other metadata);<br>
 * - It may reject handling target by returning null;<br>
 *
 * @param <T> Type
 */
public interface HasSupportDistance<T> {

	/**
	 * Returns support distance, i.e. "willingness" to handle this specific target.<br>
	 * Returning null means this handler does not support target.<br>
	 * Among handlers returned not null value lowest will be chosen to handle.<br>
	 *
	 * @param target any
	 *
	 * @return non-negative or null
	 */
	@Nullable
	Integer getSupportDistance(T target);

	static <H extends HasSupportDistance<T>, T> SelectionResult<H, T> selectHandler(List<H> handlers, T target) {
		return selectHandler(handlers, target, (handler, innerTarget) -> handler.getSupportDistance(target));
	}

	static <H, T> SelectionResult<H, T> selectHandler(
			List<H> handlers,
			T target,
			BiFunction<H, T, @Nullable Integer> supportDistanceFunction) {
		SelectionResult<H, T> selectionResult = SelectionResult.of(
				handlers
						.groupBy(handler -> supportDistanceFunction.apply(handler, target))
						.map(tuple -> Pair.of(tuple._1(), tuple._2()))
						.toList()
						.sortBy(Comparator.nullsLast(Comparator.naturalOrder()), Pair::getLeft),
				target);
		//
		return selectionResult;
	}

	@Value(staticConstructor = "of")
	class SelectionResult<H, T> {

		List<Pair<Integer, List<H>>> evaluatedHandlers;

		T target;

		@Nullable
		public H findHandler() {
			H result;
			{
				List<H> handlers = getHandlers();
				//
				if (handlers.size() > 1) {
					throw new IllegalArgumentException("Multiple found: this = %s".formatted(this));
				}
				//
				result = !handlers.isEmpty() ? handlers.get(0) : null;
			}
			return result;
		}

		public H getHandler() {
			H result = findHandler();
			//
			if (result == null) {
				throw new IllegalArgumentException("Not found: this = %s".formatted(this));
			}
			//
			return result;
		}

		public List<H> getHandlers() {
			List<Pair<Integer, List<H>>> filteredHandlers = evaluatedHandlers.filter(pair -> pair.getLeft() != null);
			//
			return !filteredHandlers.isEmpty() ? filteredHandlers.get(0).getRight() : List.empty();
		}

		public SelectionResult<H, T> mergeWith(SelectionResult<H, T> other) {
			if (target != other.target) {
				throw new IllegalArgumentException("For merge SelectionsResults must have same target");
			}
			return of(
					evaluatedHandlers.appendAll(other.evaluatedHandlers)
							.groupBy(Pair::getLeft)
							.filterKeys(Objects::nonNull)
							.mapValues(pairs -> pairs.flatMap(Pair::getRight))
							.map(tuple -> Pair.of(tuple._1(), tuple._2()))
							.toList()
							.sorted(Comparator.comparing(Pair::getLeft)),
					target);
		}

		public static <H, T> SelectionResult<H, T> empty(T target) {
			return of(List.empty(), target);
		}
	}
}
