package tk.labyrinth.pandora.dependencies.expresso;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.dependencies.PandoraDependenciesModule;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;

/**
 * @see PredicateObjectMapperConfigurer
 */
@ExtendWithSpring
@Import({
		PandoraDependenciesModule.class,
})
class PredicateObjectMapperConfigurerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testDeserialize() throws JsonProcessingException {
		Assertions.assertEquals(
				Predicates.and(
						Predicates.equalTo("a", "b"),
						Predicates.equalTo("c", "d"),
						Predicates.or(
								Predicates.and(
										Predicates.greaterThanOrEqualTo("e", "0"),
										Predicates.lessThan("e", "1")),
								Predicates.inUnsafe(
										"year",
										objectMapper.getNodeFactory().arrayNode().add(2020).add(2021)))),
				objectMapper.readValue(
						"{\"operator\":\"AND\",\"predicates\":[{\"operator\":\"EQUAL_TO\",\"property\":\"a\",\"value\":\"b\"},{\"operator\":\"EQUAL_TO\",\"property\":\"c\",\"value\":\"d\"},{\"operator\":\"OR\",\"predicates\":[{\"operator\":\"AND\",\"predicates\":[{\"operator\":\"GREATER_THAN_OR_EQUAL_TO\",\"property\":\"e\",\"value\":0},{\"operator\":\"LESS_THAN\",\"property\":\"e\",\"value\":1}]},{\"operator\":\"IN\",\"property\":\"year\",\"value\":[2020,2021]}]}]}",
						Predicate.class));
	}

	@Test
	void testSerialize() throws JsonProcessingException {
		Assertions.assertEquals(
				"{\"operator\":\"AND\",\"predicates\":[{\"operator\":\"EQUAL_TO\",\"property\":\"a\",\"value\":\"b\"},{\"operator\":\"EQUAL_TO\",\"property\":\"c\",\"value\":\"d\"},{\"operator\":\"OR\",\"predicates\":[{\"operator\":\"AND\",\"predicates\":[{\"operator\":\"GREATER_THAN_OR_EQUAL_TO\",\"property\":\"e\",\"value\":0},{\"operator\":\"LESS_THAN\",\"property\":\"e\",\"value\":1}]},{\"operator\":\"IN\",\"property\":\"year\",\"value\":[2020,2021]}]}]}",
				objectMapper.writeValueAsString(
						Predicates.and(
								Predicates.equalTo("a", "b"),
								Predicates.equalTo("c", "d"),
								Predicates.or(
										Predicates.and(
												Predicates.greaterThanOrEqualTo("e", 0),
												Predicates.lessThan("e", 1)),
										Predicates.in("year", 2020, 2021)))));
	}
}