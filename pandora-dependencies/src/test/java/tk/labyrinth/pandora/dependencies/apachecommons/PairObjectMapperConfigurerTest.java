package tk.labyrinth.pandora.dependencies.apachecommons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.core.change.ChangeStatus;

@SpringBootTest
class PairObjectMapperConfigurerTest {

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testToString() throws JsonProcessingException {
		Assertions
				.assertThat(objectMapper.writeValueAsString(Pair.of(ChangeStatus.MODIFIED, null)))
				.isEqualTo("""
						{"left":"MODIFIED"}""");
	}
}