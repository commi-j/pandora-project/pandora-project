package tk.labyrinth.pandora.dependencies.expresso;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.domain.java.JavaPredicateResolver;
import tk.labyrinth.expresso.query.domain.java.JavaPropertyAccessor;

import java.util.List;

public class EnhancedJavaPredicateResolver extends JavaPredicateResolver {

	@Autowired
	private List<JavaPredicateEvaluator<?>> predicateEvaluators;

	public EnhancedJavaPredicateResolver(JavaPropertyAccessor propertyAccessor) {
		super(propertyAccessor);
	}

	@PostConstruct
	private void postConstruct() {
		predicateEvaluators.forEach(this::registerPredicateEvaluator);
	}

	private <P extends Predicate> void registerPredicateEvaluator(JavaPredicateEvaluator<P> predicateEvaluator) {
		register(
				predicateEvaluator.getParameterClass(),
				predicate -> target -> predicateEvaluator.evaluate(
						JavaPredicateEvaluator.Context.builder().build(),
						predicate,
						target));
	}
}
