package tk.labyrinth.pandora.dependencies.spring;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.http.HttpMethod;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;

import java.io.IOException;

@Bean
public class HttpMethodObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer());
		return module;
	}

	public static class Deserializer extends StdNodeBasedDeserializer<HttpMethod> {

		public Deserializer() {
			super(HttpMethod.class);
		}

		@Override
		public HttpMethod convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			return HttpMethod.valueOf(root.textValue());
		}
	}

	public static class Serializer extends StdSerializer<HttpMethod> {

		public Serializer() {
			super(HttpMethod.class);
		}

		@Override
		public void serialize(HttpMethod value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.toString());
		}
	}
}
