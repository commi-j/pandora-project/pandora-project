package tk.labyrinth.pandora.dependencies.expresso;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

/**
 * @param <P> Predicate
 */
public interface JavaPredicateEvaluator<P extends Predicate> extends TypeAware<P> {

	boolean evaluate(Context context, P predicate, Object target);

	@Builder(toBuilder = true)
	@Value
	class Context {
		// TODO?
	}
}
