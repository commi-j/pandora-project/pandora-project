package tk.labyrinth.pandora.dependencies.expresso;

import lombok.Value;
import tk.labyrinth.expresso.lang.predicate.BinaryPredicate;

@Value(staticConstructor = "of")
public class MatchesMaskPredicate implements BinaryPredicate<String, String> {

	String property;

	String value;

	@Override
	public String first() {
		return property;
	}

	@Override
	public String second() {
		return value;
	}

	@Override
	public MatchesMaskPredicate withFirst(String first) {
		return new MatchesMaskPredicate(first, value);
	}

	@Override
	public MatchesMaskPredicate withSecond(String second) {
		return new MatchesMaskPredicate(property, second);
	}
}
