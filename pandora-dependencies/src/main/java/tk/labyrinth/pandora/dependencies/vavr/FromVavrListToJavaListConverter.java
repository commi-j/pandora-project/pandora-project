package tk.labyrinth.pandora.dependencies.vavr;

import io.vavr.collection.List;
import org.springframework.core.convert.converter.Converter;

// TODO
//@WritingConverter
public class FromVavrListToJavaListConverter implements Converter<List<?>, java.util.List<?>> {

	@Override
	public java.util.List<?> convert(List<?> source) {
		return source.asJava();
	}
}
