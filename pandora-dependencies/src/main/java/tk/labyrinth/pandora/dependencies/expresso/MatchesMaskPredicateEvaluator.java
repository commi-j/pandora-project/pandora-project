package tk.labyrinth.pandora.dependencies.expresso;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.domain.java.JavaPropertyAccessor;
import tk.labyrinth.pandora.misc4j.text.TextUtils;

@RequiredArgsConstructor
public class MatchesMaskPredicateEvaluator implements JavaPredicateEvaluator<MatchesMaskPredicate> {

	private final JavaPropertyAccessor propertyAccessor;

	@Override
	public boolean evaluate(Context context, MatchesMaskPredicate predicate, Object target) {
		String value = propertyAccessor.get(target, predicate.getProperty());
		return value != null && TextUtils.matchesMask(value, predicate.getValue());
	}
}
