package tk.labyrinth.pandora.dependencies.expresso;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vavr.collection.List;
import tk.labyrinth.expresso.lang.operator.JunctionOperator;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.operator.StringOperator;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.collectoin.ListUtils;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.io.IOException;

@LazyComponent
public class PredicateObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module, objectMapper);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module, ObjectMapper objectMapper) {
		Deserializer deserializer = new Deserializer(objectMapper);
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		return module;
	}

	public static class Deserializer extends StdNodeBasedDeserializer<Predicate> {

		private final ObjectMapper objectMapper;

		public Deserializer(ObjectMapper objectMapper) {
			super(Predicate.class);
			this.objectMapper = objectMapper;
		}

		@Override
		public Predicate convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			Predicate result;
			{
				ObjectNode objectNode = (ObjectNode) root;
				JsonNode propertyNode = objectNode.get("property");
				//
				if (propertyNode != null) {
					// property operator value (caseSensitive)
					//
					JsonNode caseSensitiveNode = objectNode.get("caseSensitive");
					String operatorString = objectNode.get("operator").textValue();
					JsonNode valueNode = objectNode.get("value");
					//
					if (caseSensitiveNode != null) {
						result = new StringPropertyPredicate(
								caseSensitiveNode.asBoolean(),
								StringOperator.valueOf(operatorString),
								propertyNode.textValue(),
								valueNode.asText());
					} else {
						result = new ObjectPropertyPredicate(
								ObjectOperator.valueOf(operatorString),
								propertyNode.textValue(),
								valueNode.isValueNode() ? valueNode.asText() : valueNode);
					}
				} else {
					JsonNode predicatesNode = objectNode.get("predicates");
					//
					if (predicatesNode != null) {
						// operator predicates
						//
						result = new JunctionPredicate(
								JunctionOperator.valueOf(objectNode.get("operator").textValue()),
								List.ofAll(ListUtils.from(predicatesNode.elements()))
										.map(element -> objectMapper.convertValue(element, Predicate.class))
										.asJava());
					} else {
						throw new NotImplementedException();
					}
				}
			}
			return result;
		}
	}
}
