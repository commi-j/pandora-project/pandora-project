package tk.labyrinth.pandora.dependencies.expresso;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tk.labyrinth.expresso.query.domain.java.JavaPredicateResolver;
import tk.labyrinth.expresso.query.domain.java.JavaPropertyAccessor;
import tk.labyrinth.expresso.query.domain.java.JavaQueryExecutor;
import tk.labyrinth.expresso.query.domain.java.SpringPropertyAccessor;

@Deprecated // FIXME: Maybe?
@ComponentScan
@Configuration
@Import({
		EnhancedJavaPredicateResolver.class,
		MatchesMaskPredicateEvaluator.class,
})
public class PandoraExpressoDependencyConfiguration {

	@Bean
	@ConditionalOnMissingBean(JavaPropertyAccessor.class)
	public static JavaPropertyAccessor javaPropertyAccessor() {
		return new SpringPropertyAccessor();
	}

	@Bean
	@ConditionalOnMissingBean(JavaQueryExecutor.class)
	public static JavaQueryExecutor javaQueryExecutor(
			JavaPropertyAccessor javaPropertyAccessor,
			JavaPredicateResolver javaPredicateResolver) {
		return new JavaQueryExecutor(javaPropertyAccessor, javaPredicateResolver);
	}
}
