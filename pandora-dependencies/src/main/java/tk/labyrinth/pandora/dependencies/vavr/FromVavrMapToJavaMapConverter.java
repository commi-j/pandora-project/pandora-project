package tk.labyrinth.pandora.dependencies.vavr;

import io.vavr.collection.Map;
import org.springframework.core.convert.converter.Converter;

// TODO
//@WritingConverter
public class FromVavrMapToJavaMapConverter implements Converter<Map<?, ?>, java.util.Map<?, ?>> {

	@Override
	public java.util.Map<?, ?> convert(Map<?, ?> source) {
		return source.toJavaMap();
	}
}
