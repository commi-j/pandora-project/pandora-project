package tk.labyrinth.pandora.dependencies.vavr;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import tk.labyrinth.pandora.core.tool.converter.common.TypeDescriptorUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Set;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class FromJavaSetToVavrListConverter implements ConditionalGenericConverter {

	private final ObjectProvider<ConversionService> conversionServiceProvider;

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		TypeDescriptor targetElementType = TypeDescriptorUtils.getParameter(targetType, 0);
		//
		ConversionService conversionService = conversionServiceProvider.getObject();
		//
		return Stream.ofAll((java.util.Set<?>) source)
				.map(element -> conversionService.convert(
						element,
						TypeDescriptor.forObject(element),
						targetElementType))
				.sorted()
				.toList();
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return null;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return java.util.Set.class.isAssignableFrom(sourceType.getType()) &&
				targetType.getType() == List.class &&
				conversionServiceProvider.getObject().canConvert(
						sourceType.getElementTypeDescriptor(),
						TypeDescriptorUtils.getParameter(targetType, 0));
	}
}
