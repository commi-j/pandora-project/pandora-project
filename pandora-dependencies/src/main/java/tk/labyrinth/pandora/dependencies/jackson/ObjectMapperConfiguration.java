package tk.labyrinth.pandora.dependencies.jackson;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperFactory;
import tk.labyrinth.pandora.misc4j.lib.jackson.VavrObjectMapperConfigurer;

import java.util.List;

@Configuration
public class ObjectMapperConfiguration {

	@Bean
	public static ObjectMapper objectMapper(
			@Autowired(required = false) List<ObjectMapperConfigurer> objectMapperConfigurers) {
		return ObjectMapperFactory.defaultConfigured(objectMapperConfigurers)
				// FIXME: Think if we want this as default feature.
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	@Bean
	@ConditionalOnClass(name = "io.vavr.jackson.datatype.VavrModule")
	public static VavrObjectMapperConfigurer vavrObjectMapperConfigurer() {
		return new VavrObjectMapperConfigurer();
	}
}
