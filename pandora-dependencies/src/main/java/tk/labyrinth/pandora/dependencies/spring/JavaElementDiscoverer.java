package tk.labyrinth.pandora.dependencies.spring;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.core.type.filter.TypeFilter;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class JavaElementDiscoverer {

	private final ApplicationContext applicationContext;

	public List<TypeSignature> discoverTypeSignatures(String basePackage, List<TypeFilter> includeFilters) {
		PandoraCandidateComponentProvider pandoraCandidateComponentProvider = new PandoraCandidateComponentProvider(
				applicationContext,
				includeFilters);
		//
		return pandoraCandidateComponentProvider.scanCandidateComponents(basePackage).stream()
				.map(BeanDefinition::getBeanClassName)
				.filter(Objects::nonNull)
				.map(TypeSignature::guessFromBinaryName)
				.collect(List.collector());
	}
}
