package tk.labyrinth.pandora.dependencies.apachecommons;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.io.IOException;

// TODO: Implement as json object with left & right. See vavr map as reference example.
@LazyComponent
public class PairObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module, objectMapper);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module, ObjectMapper objectMapper) {
		JsonDeserializer deserializer = new Deserializer(objectMapper);
		//
		module.addDeserializer(deserializer.handledType(), deserializer);
		module.addSerializer(new Serializer());
		//
		return module;
	}

	@SuppressWarnings("rawtypes")
	public static class Deserializer extends StdNodeBasedDeserializer<Pair> implements ContextualDeserializer {

		private final ObjectMapper objectMapper;

		private final JavaType targetType;

		public Deserializer(Deserializer deserializer, @NonNull JavaType targetType) {
			super(deserializer);
			this.objectMapper = deserializer.objectMapper;
			this.targetType = targetType;
		}

		public Deserializer(ObjectMapper objectMapper) {
			super(Pair.class);
			this.objectMapper = objectMapper;
			this.targetType = objectMapper.constructType(Void.class);
		}

		@Override
		public Pair convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			return Pair.of(
					objectMapper.convertValue(root.get("left"), targetType.containedType(0)),
					objectMapper.convertValue(root.get("right"), targetType.containedType(1)));
		}

		@Override
		public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) {
			return new Deserializer(this, ctxt.getContextualType());
		}
	}

	@SuppressWarnings("rawtypes")
	public static class Serializer extends StdSerializer<Pair> {

		public Serializer() {
			super(Pair.class);
		}

		@Override
		public void serialize(Pair value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			//
			if (value.getLeft() != null) {
				gen.writeObjectField("left", value.getLeft());
			}
			//
			if (value.getRight() != null) {
				gen.writeObjectField("right", value.getRight());
			}
			//
			gen.writeEndObject();
		}
	}
}
