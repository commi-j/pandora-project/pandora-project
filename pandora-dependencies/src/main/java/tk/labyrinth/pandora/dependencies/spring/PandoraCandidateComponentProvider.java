package tk.labyrinth.pandora.dependencies.spring;

import io.vavr.collection.List;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.core.type.filter.TypeFilter;

// TODO: Rework into mechanism that runs once and report results to any interested party. Or not?
public class PandoraCandidateComponentProvider extends CustomClassPathScanningCandidateComponentProvider {

	public PandoraCandidateComponentProvider(ApplicationContext applicationContext, List<TypeFilter> includeFilters) {
		super(false);
		//
		setEnvironment(applicationContext.getEnvironment());
		setResourceLoader(applicationContext);
		//
		includeFilters.forEach(this::addIncludeFilter);
		//
		setResourceProcessingFaultHandler((resource, fault) -> {
			if ((fault instanceof IndexOutOfBoundsException) &&
					resource.toString().contains("/org/atmosphere/inject/")) {
				logger.debug("Failed to read metadata of Atmosphere class: %s".formatted(resource));
			} else {
				throw new RuntimeException(fault);
			}
		});
	}

	@Override
	protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
		// Overriden to permit interfaces.
		return true;
	}
}
