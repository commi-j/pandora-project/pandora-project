package tk.labyrinth.pandora.dependencies;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.core.PandoraCoreConfiguration;

@Import({
		PandoraCoreConfiguration.class,
})
@Slf4j
@SpringBootApplication
public class PandoraDependenciesModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
