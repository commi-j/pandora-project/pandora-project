package tk.labyrinth.pandora.reactive.observable;

import lombok.RequiredArgsConstructor;
import reactor.core.Disposable;

import java.util.concurrent.atomic.AtomicBoolean;

@RequiredArgsConstructor(staticName = "of")
public class SimpleDisposable implements Disposable {

	private final Runnable disposeRunnable;

	private final AtomicBoolean disposed = new AtomicBoolean(false);

	@Override
	public void dispose() {
		if (disposed.compareAndSet(false, true)) {
			disposeRunnable.run();
		}
	}

	@Override
	public boolean isDisposed() {
		return disposed.get();
	}
}
