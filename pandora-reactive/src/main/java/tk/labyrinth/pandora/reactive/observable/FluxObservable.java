package tk.labyrinth.pandora.reactive.observable;

import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.misc4j.lib.reactor.FluxUtils;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public class FluxObservable<T> implements Observable<T> {

	private final Flux<T> flux;

	private FluxObservable(Flux<T> flux) {
		this.flux = flux;
	}

	@Override
	public T get() {
		T value = FluxUtils.lookupValue(flux);
		//
		if (value == null) {
			throw new IllegalArgumentException("Not initialized");
		}
		//
		return value;
	}

	@Override
	public Flux<T> getFlux() {
		return flux;
	}

	@Override
	public boolean initialized() {
		return FluxUtils.lookupValue(flux) != null;
	}

	@Override
	public void set(T value) {
		throw new UnsupportedOperationException("set is not supported by this Observable");
	}

	@Override
	public void subscribe(Consumer<T> consumer) {
		flux.subscribe(consumer);
	}

	@Override
	public void subscribe(BiConsumer<T, Consumer<T>> consumer) {
		throw new UnsupportedOperationException("subscribe with sink is not supported by this Observable");
	}

	@Override
	public T update(UnaryOperator<T> updateOperator) {
		throw new UnsupportedOperationException("update is not supported by this Observable");
	}

	public static <T> FluxObservable<T> of(Flux<T> flux) {
		return new FluxObservable<>(flux);
	}
}
