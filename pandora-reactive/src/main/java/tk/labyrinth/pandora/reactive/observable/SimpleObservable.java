package tk.labyrinth.pandora.reactive.observable;

import com.vaadin.flow.component.UI;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.ReactiveVaadin;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

// TODO: If observable is created within a certain UI scope we may want it to be bound to this UI
//  and accept only changes from it. This is required for eager detection of inconsistent behaviour with scopes,
//  when we access lower scope and binds to its UI.
public class SimpleObservable<T> implements Observable<T> {

	private final Flux<T> flux;

	private final AtomicReference<T> reference = new AtomicReference<>(null);

	private final Sinks.Many<T> sink;

	{
		sink = Sinks.many().replay().latest();
		//
		flux = sink.asFlux().distinctUntilChanged();
	}

	// FIXME: package private to be accesible from Observable.
	SimpleObservable() {
		// no-op
	}

	private T doUpdate(UnaryOperator<T> updateOperator) {
		T result = reference.updateAndGet(updateOperator);
		//
		sink.tryEmitNext(result);
		//
		return result;
	}

	@Override
	public T get() {
		T result = reference.get();
		//
		if (result == null) {
			throw new IllegalArgumentException("Not initialized");
		}
		//
		return result;
	}

	@Override
	public Flux<T> getFlux() {
		// TODO: Not sure if we want to bind this class to Vaadin. Find better solution.
		UI currentUi = UI.getCurrent();
		//
		return currentUi != null
				? ReactiveVaadin.uiFlux(currentUi, flux)
				: flux;
	}

	@Override
	public boolean initialized() {
		return reference.get() != null;
	}

	@Override
	public void set(T value) {
		doUpdate(currentValue -> value);
	}

	@Override
	public void subscribe(Consumer<T> consumer) {
		getFlux().subscribe(consumer);
	}

	/**
	 * @param consumer (next, sink) -> {}
	 */
	@Override
	public void subscribe(BiConsumer<T, Consumer<T>> consumer) {
		getFlux().subscribe(nextValue -> consumer.accept(nextValue, this::set));
	}

	@Override
	public T update(UnaryOperator<T> updateOperator) {
		return doUpdate(updateOperator);
	}

	/**
	 * You may use this one if you want to initialize observable with value that may refer to the observable itself.<br>
	 * (E.g. when you want to update value by function stored within observable.)<br>
	 *
	 * @param initializer non-null
	 * @param <T>         Type
	 *
	 * @return non-null
	 */
	public static <T> SimpleObservable<T> fromInitializer(Function<SimpleObservable<T>, T> initializer) {
		SimpleObservable<T> result = new SimpleObservable<>();
		//
		result.set(initializer.apply(result));
		//
		return result;
	}

	public static <T> SimpleObservable<T> notInitialized() {
		return new SimpleObservable<>();
	}

	public static <T> SimpleObservable<T> withInitialValue(T initialValue) {
		SimpleObservable<T> result = new SimpleObservable<>();
		//
		result.set(initialValue);
		//
		return result;
	}
}
