package tk.labyrinth.pandora.reactive.observable;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.misc4j.lib.reactor.FluxUtils;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

@RequiredArgsConstructor(staticName = "of")
public class FunctionalObservable<T> implements Observable<T> {

	private final Flux<T> flux;

	/**
	 * The default implementation for this function should do the following:<br>
	 * - Get current state from the underlying system;<br>
	 * - Apply provided operator to it to compute next state;<br>
	 * - Set next state to the underlying system;<br>
	 * - Return next state;<br>
	 */
	private final Function<UnaryOperator<T>, T> updateFunction;

	@Override
	public T get() {
		T value = FluxUtils.lookupValue(flux);
		//
		if (value == null) {
			throw new IllegalArgumentException("Not initialized");
		}
		//
		return value;
	}

	@Override
	public Flux<T> getFlux() {
		return flux;
	}

	@Override
	public boolean initialized() {
		return FluxUtils.lookupValue(flux) != null;
	}

	@Override
	public void set(T value) {
		updateFunction.apply(current -> value);
	}

	@Override
	public void subscribe(Consumer<T> consumer) {
		getFlux().subscribe(consumer);
	}

	@Override
	public void subscribe(BiConsumer<T, Consumer<T>> consumer) {
		getFlux().subscribe(nextValue -> consumer.accept(nextValue, this::set));
	}

	@Override
	public T update(UnaryOperator<T> updateOperator) {
		return updateFunction.apply(updateOperator);
	}
}
