package tk.labyrinth.pandora.reactive.observable;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import reactor.core.publisher.Flux;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class MappingObservable<T> implements Observable<T> {

	private final Function<Object, T> fromTarget;

	private final Observable<Object> target;

	@Nullable
	private final Function<T, Object> toTarget;

	@Override
	public T get() {
		return fromTarget.apply(target.get());
	}

	@Override
	public Flux<T> getFlux() {
		return target.getFlux().map(fromTarget);
	}

	@Override
	public boolean initialized() {
		return target.initialized();
	}

	@Override
	public void set(T value) {
		if (toTarget == null) {
			throw new UnsupportedOperationException("set is not supported by this Observable");
		}
		//
		target.set(toTarget.apply(value));
	}

	@Override
	public void subscribe(Consumer<T> consumer) {
		target.subscribe(value -> consumer.accept(fromTarget.apply(value)));
	}

	@Override
	public void subscribe(BiConsumer<T, Consumer<T>> consumer) {
		if (toTarget == null) {
			throw new UnsupportedOperationException("subscribe with sink is not supported by this Observable");
		}
		//
		target.subscribe((next, sink) -> consumer.accept(
				fromTarget.apply(next),
				value -> sink.accept(toTarget.apply(value))));
	}

	@Override
	public T update(UnaryOperator<T> updateOperator) {
		if (toTarget == null) {
			throw new UnsupportedOperationException("update is not supported by this Observable");
		}
		//
		return fromTarget.apply(target.update(current ->
				toTarget.apply(updateOperator.apply(fromTarget.apply(current)))));
	}
}
