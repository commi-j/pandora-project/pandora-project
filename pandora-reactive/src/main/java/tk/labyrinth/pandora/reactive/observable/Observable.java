package tk.labyrinth.pandora.reactive.observable;

import lombok.NonNull;
import reactor.core.publisher.Flux;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * @param <T> Type
 *
 * @author Commitman
 * @version 1.0.0
 */
public interface Observable<@NonNull T> {

	/**
	 * @return non-null
	 *
	 * @throws IllegalArgumentException if not initialized
	 */
	T get();

	Flux<T> getFlux();

	boolean initialized();

	default <R> Observable<R> map(Function<T, R> directFunction) {
		return map(directFunction, null);
	}

	@SuppressWarnings("unchecked")
	default <R> Observable<R> map(Function<T, R> directFunction, Function<R, T> reverseFunction) {
		return new MappingObservable<>(
				(Function<Object, R>) directFunction,
				(Observable<Object>) this,
				(Function<R, Object>) reverseFunction);
	}

	void set(T value);

	void subscribe(Consumer<T> consumer);

	/**
	 * @param consumer (next, sink) -> {}
	 */
	void subscribe(BiConsumer<T, Consumer<T>> consumer);

	T update(UnaryOperator<T> updateOperator);

	/**
	 * You may use this one if you want to initialize observable with value that may refer to the observable itself.<br>
	 * (E.g. when you want to update value by function stored within observable.)<br>
	 *
	 * @param initializer non-null
	 * @param <T>         Type
	 *
	 * @return non-null
	 */
	static <T> Observable<T> fromInitializer(Function<Observable<T>, T> initializer) {
		Observable<T> result = new SimpleObservable<>();
		result.set(initializer.apply(result));
		return result;
	}

	static <T> Observable<T> notInitialized() {
		return new SimpleObservable<>();
	}

	static <T> Observable<T> withInitialValue(@NonNull T initialValue) {
		Observable<T> result = new SimpleObservable<>();
		result.set(initialValue);
		return result;
	}
}
