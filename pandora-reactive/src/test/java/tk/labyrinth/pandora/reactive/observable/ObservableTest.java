package tk.labyrinth.pandora.reactive.observable;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class ObservableTest {

	@Test
	void testSubscribe() throws ExecutionException, InterruptedException, TimeoutException {
		Observable<String> observable = Observable.withInitialValue("foo");
		//
		CompletableFuture<String> future = new CompletableFuture<>();
		observable.subscribe(future::complete);
		String value = future.get(100, TimeUnit.MILLISECONDS);
		//
		Assertions.assertEquals("foo", value);
	}

	@Test
	void testWithNullAsInitialValue() {
		ContribAssertions.assertThrows(
				() -> Observable.withInitialValue(null),
				fault -> Assertions.assertEquals(
						"java.lang.NullPointerException: initialValue is marked non-null but is null",
						fault.toString()));
	}
}