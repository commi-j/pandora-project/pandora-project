package tk.labyrinth.pandora.reactive.observable;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Sinks;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

class FluxObservableTest {

	@Test
	void testGet() {
		Sinks.Many<String> many = Sinks.many().replay().latest();
		FluxObservable<String> observable = FluxObservable.of(many.asFlux());
		//
		ContribAssertions.assertThrows(
				observable::get,
				fault -> Assertions
						.assertThat(fault.toString())
						.isEqualTo("java.lang.IllegalArgumentException: Not initialized"));
		//
		many.tryEmitNext("foo");
		//
		Assertions.assertThat(observable.get()).isEqualTo("foo");
	}

	@Test
	void testInitialized() {
		Sinks.Many<String> many = Sinks.many().replay().latest();
		FluxObservable<String> observable = FluxObservable.of(many.asFlux());
		//
		Assertions.assertThat(observable.initialized()).isFalse();
		//
		many.tryEmitNext("foo");
		//
		Assertions.assertThat(observable.initialized()).isTrue();
	}
}
