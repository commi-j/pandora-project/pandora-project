package tk.labyrinth.pandora.stores.domain.storeconfiguration;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;

import javax.annotation.CheckForNull;

@Builder(toBuilder = true)
@Value
public class MongoDbStoreSettings {

	/**
	 * Caching allows multistore querying.
	 */
	Boolean cacheObjects;

	@CheckForNull
	KeyCredentialsReference credentialsReference;

	String uri;
}
