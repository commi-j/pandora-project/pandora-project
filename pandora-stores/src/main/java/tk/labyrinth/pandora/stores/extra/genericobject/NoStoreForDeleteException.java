package tk.labyrinth.pandora.stores.extra.genericobject;

import lombok.Value;
import tk.labyrinth.pandora.core.selection.SelectionResult;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;

@Value
public class NoStoreForDeleteException extends RuntimeException {

	GenericObject foundObject;

	Reference<GenericObject> reference;

	SelectionResult<GenericObject, GenericObjectStore> selectionResult;
}
