package tk.labyrinth.pandora.stores.extra.predicate;

import io.vavr.collection.List;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.operator.JunctionOperator;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.operator.StringOperator;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.predicate.PropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;

import java.util.ArrayList;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PredicateUtils {

	public static final Pattern PREFIX_NOTATION_PATTERN = Pattern.compile("(?<operator>\\w+)\\s*\\((?<operands>.*)\\)");

	private static List<Predicate> doParsePrefixPredicates(String predicatesString) {
		List<Predicate> result;
		{
			StringTokenizer tokenizer = new StringTokenizer(predicatesString, "(),", true);
			//
			StringBuilder stringBuilder = new StringBuilder();
			int layer = 0;
			//
			ArrayList<Predicate> predicates = new ArrayList<>();
			//
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				//
				if (Objects.equals(token, ",") && layer == 0) {
					predicates.add(parseWithPrefixNotation(stringBuilder.toString().trim()));
					//
					stringBuilder = new StringBuilder();
				} else {
					stringBuilder.append(token);
					//
					if (Objects.equals(token, "(")) {
						layer++;
					} else if (Objects.equals(token, ")")) {
						layer--;
						//
						if (layer < 0) {
							throw new IllegalArgumentException(predicatesString);
						}
					}
				}
			}
			//
			if (layer != 0) {
				throw new IllegalArgumentException(predicatesString);
			}
			//
			predicates.add(parseWithPrefixNotation(stringBuilder.toString().trim()));
			//
			result = List.ofAll(predicates);
		}
		return result;
	}

	public static List<Predicate> dismantleAnd(Predicate predicate) {
		List<Predicate> result;
		{
			if (predicate instanceof JunctionPredicate junctionPredicate) {
				if (junctionPredicate.isAnd()) {
					result = List.ofAll(junctionPredicate.predicates());
				} else {
					result = List.of(predicate);
				}
			} else {
				result = List.of(predicate);
			}
		}
		return result;
	}

	public static Predicate nonNullAttributeToPredicate(@NonNull GenericObjectAttribute attribute) {
		return Predicates.equalTo(attribute.getName(), attribute.getValue().unwrap());
	}

	@Nullable
	public static Predicate nullableAttributeToPredicate(@Nullable GenericObjectAttribute attribute) {
		return attribute != null ? nonNullAttributeToPredicate(attribute) : null;
	}

	public static Predicate parseJunction(JunctionOperator operator, String operandsString) {
		return new JunctionPredicate(
				operator,
				doParsePrefixPredicates(operandsString).asJava());
	}

	public static ObjectPropertyPredicate parseObject(ObjectOperator operator, String operandsString) {
		Pair<String, String> operands = StringUtils.splitByFirstOccurrence(operandsString, " ");
		//
		return new ObjectPropertyPredicate(
				operator,
				operands.getLeft().trim(),
				operator == ObjectOperator.IN
						? List.of(operands.getRight().substring(1, operands.getRight().length() - 1).split(","))
						.map(String::trim)
						.asJava()
						: operands.getRight().trim());
	}

	public static Object parseOperator(String operatorString) {
		return switch (operatorString) {
			case "AND" -> JunctionOperator.AND;
			case "CONTAINS" -> ObjectOperator.CONTAINS; // FIXME: Ambiguous between Object/String.
			case "ENDS_WITH" -> StringOperator.ENDS_WITH;
			case "EQUAL_TO" -> ObjectOperator.EQUAL_TO;
			case "GREATER_THAN" -> ObjectOperator.GREATER_THAN;
			case "GREATER_THAN_OR_EQUAL_TO" -> ObjectOperator.GREATER_THAN_OR_EQUAL_TO;
			case "IN" -> ObjectOperator.IN;
			case "LESS_THAN" -> ObjectOperator.LESS_THAN;
			case "LESS_THAN_OR_EQUAL_TO" -> ObjectOperator.LESS_THAN_OR_EQUAL_TO;
			case "NOT_EQUAL_TO" -> ObjectOperator.NOT_EQUAL_TO;
			case "OR" -> JunctionOperator.OR;
			case "STARTS_WITH" -> StringOperator.STARTS_WITH;
			default -> throw new NotImplementedException(operatorString);
		};
	}

	public static StringPropertyPredicate parseString(StringOperator operator, String operandsString) {
		Pair<String, String> operands = StringUtils.splitByFirstOccurrence(operandsString, " ");
		//
		return new StringPropertyPredicate(
				false, // TODO
				operator,
				operands.getLeft().trim(),
				operands.getRight().trim());
	}

	public static Predicate parseWithPrefixNotation(String predicateString) {
		Predicate result;
		{
			Matcher matcher = PREFIX_NOTATION_PATTERN.matcher(predicateString);
			//
			if (!matcher.matches()) {
				throw new IllegalArgumentException("Require prefix notation: %s".formatted(predicateString));
			}
			//
			String operatorString = matcher.group("operator");
			String operandsString = matcher.group("operands").trim();
			//
			Object operator = parseOperator(operatorString);
			//
			if (operator instanceof JunctionOperator junctionOperator) {
				result = parseJunction(junctionOperator, operandsString);
			} else if (operator instanceof ObjectOperator objectOperator) {
				result = parseObject(objectOperator, operandsString);
			} else if (operator instanceof StringOperator stringOperator) {
				result = parseString(stringOperator, operandsString);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public static String renderWithInfixJunctions(Predicate predicate, boolean wrap) {
		String result;
		{
			if (predicate != null) {
				if (predicate instanceof JunctionPredicate junctionPredicate) {
					if (wrap && junctionPredicate.predicates().size() > 1) {
						result = "( %s )".formatted(renderWithInfixJunctions(predicate, false));
					} else {
						result = List.ofAll(junctionPredicate.predicates())
								.map(predicateElement -> renderWithInfixJunctions(predicateElement, true))
								.mkString(" %s ".formatted(junctionPredicate.operator().toString()));
					}
				} else if (predicate instanceof PropertyPredicate<?> propertyPredicate) {
					result = "%s %s %s".formatted(
							propertyPredicate.property(),
							propertyPredicate.operator(),
							propertyPredicate.value());
				} else {
					throw new NotImplementedException();
				}
			} else {
				result = "";
			}
		}
		return result;
	}

	public static String renderWithPrefixNotation(Predicate predicate) {
		String result;
		{
			if (predicate != null) {
				if (predicate instanceof JunctionPredicate junctionPredicate) {
					result = "%s ( %s )".formatted(
							junctionPredicate.operator(),
							List.ofAll(junctionPredicate.predicates())
									.map(PredicateUtils::renderWithPrefixNotation)
									.mkString(" , "));
				} else if (predicate instanceof PropertyPredicate<?> propertyPredicate) {
					result = "%s ( %s %s )".formatted(
							propertyPredicate.operator(),
							propertyPredicate.property(),
							propertyPredicate.value());
				} else {
					throw new NotImplementedException();
				}
			} else {
				result = "";
			}
		}
		return result;
	}
}
