package tk.labyrinth.pandora.stores.domain.storeroute;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.core.selection.SelectionResult;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectFilterer;
import tk.labyrinth.pandora.stores.extra.predicate.PredicateTool;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@LazyComponent
@RequiredArgsConstructor
public class StoreRouteRegistry {

	private final GenericObjectFilterer genericObjectFilterer;

	private final PredicateTool predicateTool;

	private final ConcurrentMap<String, StoreRoute> storeRoutes = new ConcurrentHashMap<>();

	@Nullable
	public StoreRoute findStoreRoute(GenericObject object) {
		return selectStoreRoute(object).findResult();
	}

	public StoreRoute getStoreRoute(GenericObject object) {
		return selectStoreRoute(object).getResult();
	}

	public List<StoreRoute> listStoreRoutes() {
		return List.ofAll(storeRoutes.values());
	}

	public void registerStoreRoute(StoreRoute storeRoute) {
		storeRoutes.put(
				storeRoute.getSlug(),
				storeRoute.toBuilder()
						.storeConfigurationPredicate(predicateTool.normalizePredicate(
								storeRoute.getStoreConfigurationPredicate()))
						.testPredicate(predicateTool.normalizePredicate(storeRoute.getTestPredicate()))
						.build());
	}

	public SelectionResult<GenericObject, StoreRoute> selectStoreRoute(GenericObject object) {
		val selectionResult = HasSupportDistance.selectHandler(
				List.ofAll(storeRoutes.values()),
				object,
				(storeRoute, innerPredicate) -> !genericObjectFilterer
						.filter(
								List.of(object),
								PlainQuery.builder()
										.predicate(storeRoute.getTestPredicate())
										.build())
						.isEmpty()
						? storeRoute.getDistance()
						: null);
		//
		return SelectionResult.of(object, selectionResult.getEvaluatedHandlers());
	}

	public void unregisterStoreRoute(String name) {
		storeRoutes.remove(name);
	}
}
