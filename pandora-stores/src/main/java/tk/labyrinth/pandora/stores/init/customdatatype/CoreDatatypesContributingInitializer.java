package tk.labyrinth.pandora.stores.init.customdatatype;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.customdatatype.CustomDatatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;

@Bean
@Priority(40)
@RequiredArgsConstructor
public class CoreDatatypesContributingInitializer extends GenericObjectsContributingInitializer {

	@Override
	protected List<GenericObject> contributeObjects() {
		AliasDatatypeBaseReference valueReference = AliasDatatypeBaseReference.of(
				PandoraDatatypeConstants.VALUE_DATATYPE_NAME);
		//
		return List
				.of(
						CustomDatatype.builder()
								.backingTypeReference(SignatureJavaBaseTypeReference.from(Object.class))
								.name(PandoraDatatypeConstants.VALUE_DATATYPE_NAME)
								.parentReference(null)
								.build(),
						CustomDatatype.builder()
								.backingTypeReference(SignatureJavaBaseTypeReference.from(String.class))
								.name(PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME)
								.parentReference(valueReference)
								.build(),
						CustomDatatype.builder()
								.backingTypeReference(SignatureJavaBaseTypeReference.from(List.class))
								.name(PandoraDatatypeConstants.LIST_DATATYPE_NAME)
								.parameterNames(List.of("E"))
								.parentReference(valueReference)
								.build(),
						CustomDatatype.builder()
								.backingTypeReference(SignatureJavaBaseTypeReference.from(GenericObject.class))
								.name(PandoraDatatypeConstants.OBJECT_DATATYPE_NAME)
								.parentReference(valueReference)
								.build(),
						CustomDatatype.builder()
								.backingTypeReference(SignatureJavaBaseTypeReference.from(GenericReference.class))
								.name(PandoraDatatypeConstants.REFERENCE_DATATYPE_NAME)
								.parameterNames(List.of("T"))
								.parentReference(AliasDatatypeBaseReference.of(PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME))
								.build())
				.map(customDatatype -> convert(customDatatype).withAddedAttributes(
						PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(CustomDatatype.NAME_ATTRIBUTE_NAME),
						PandoraHardcodedConstants.ATTRIBUTE));
	}
}
