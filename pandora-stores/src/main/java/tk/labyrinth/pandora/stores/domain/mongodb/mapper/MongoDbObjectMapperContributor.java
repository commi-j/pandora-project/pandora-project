package tk.labyrinth.pandora.stores.domain.mongodb.mapper;

import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;

public interface MongoDbObjectMapperContributor extends HasSupportDistance<CodeObjectModelReference> {

	MongoDbObjectMapper contributeMongoDbObjectMapper(CodeObjectModelReference target);
}
