package tk.labyrinth.pandora.stores.model;

public enum StoreAccessMode {
	READ_ONLY,
	READ_WRITE;
}
