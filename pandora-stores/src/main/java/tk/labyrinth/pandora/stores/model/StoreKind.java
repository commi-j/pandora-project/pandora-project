package tk.labyrinth.pandora.stores.model;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.net.URI;

public enum StoreKind {
	GIT,
	IN_MEMORY,
	MONGO_DB;

	@Nullable
	public static StoreKind resolveStoreKind(URI storeConnectionUri) {
		return switch (storeConnectionUri.getScheme()) {
			case "mongodb", "mongodb+srv" -> StoreKind.MONGO_DB;
			case "SECOND_CASE" -> null;
			default -> null;
		};
	}
}
