package tk.labyrinth.pandora.stores.model;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class StoreFeatures {

	StoreAccessMode accessMode;
}
