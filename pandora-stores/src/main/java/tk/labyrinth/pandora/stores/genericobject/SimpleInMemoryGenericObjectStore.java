package tk.labyrinth.pandora.stores.genericobject;

import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;

@PrototypeScopedComponent
public class SimpleInMemoryGenericObjectStore extends InMemoryGenericObjectStoreBase {

	public SimpleInMemoryGenericObjectStore(StoreConfiguration configuration) {
		super(configuration);
	}
}
