package tk.labyrinth.pandora.stores.listener;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.extra.extendedobject.ExtendedObject;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * @param <T> Type
 */
public abstract class ObjectChangeListener<T> implements GenericObjectChangeListener, TypeAware<T> {

	private final Type objectType = getParameterType();

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	private CodeObjectModelReference objectModelReference;

	@PostConstruct
	private void postConstruct() {
		objectModelReference = javaBaseTypeRegistry.getNormalizedObjectModelReference(
				// FIXME: Hardcode to obtain proper model. Need to replace with configurable resolver here.
				TypeUtils.getClass(objectType) == ExtendedObject.class
						? TypeUtils.getClass(ParameterUtils.getFirstActualParameter(objectType, ExtendedObject.class))
						: TypeUtils.getClass(objectType));
	}

	protected abstract void onObjectChange(ObjectChangeEvent<T> event);

	@Override
	@SuppressWarnings("unchecked")
	public void onGenericObjectChange(ObjectChangeEvent<GenericObject> event) {
		if (matches(event, objectModelReference)) {
			onObjectChange(ObjectChangeEvent.<T>builder()
					.nextObject((T) converterRegistry.convert(event.getNextObject(), objectType))
					.previousObject((T) converterRegistry.convert(event.getPreviousObject(), objectType))
					.primaryReference(((GenericObjectReference) event.getPrimaryReference()).toGenericReferenceInferred())
					.build());
		}
	}

	public static boolean matches(
			ObjectChangeEvent<GenericObject> event,
			CodeObjectModelReference objectModelReference) {
		return (!event.hasPreviousObject() ||
				Objects.equals(PandoraRootUtils.getModelReference(event.getPreviousObject()), objectModelReference)) &&
				(!event.hasNextObject() || Objects.equals(
						PandoraRootUtils.getModelReference(event.getNextObject()),
						objectModelReference));
	}
}
