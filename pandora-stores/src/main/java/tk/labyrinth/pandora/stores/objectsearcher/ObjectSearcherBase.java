package tk.labyrinth.pandora.stores.objectsearcher;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

public abstract class ObjectSearcherBase<Q, T> implements ObjectSearcher<Q, T> {

	@Nullable
	@Override
	public T findSingle(Reference<T> reference) {
		return resolve(reference).findSingle();
	}

	@Override
	public T getSingle(Reference<T> reference) {
		return resolve(reference).getSingle();
	}
}
