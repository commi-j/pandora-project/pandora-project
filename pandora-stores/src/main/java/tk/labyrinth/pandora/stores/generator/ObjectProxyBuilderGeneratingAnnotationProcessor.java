package tk.labyrinth.pandora.stores.generator;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.JaapObjectModelFactory;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.models.tool.builder.BuilderGeneratingAnnotationProcessor;
import tk.labyrinth.pandora.models.tool.builder.GenerateBuilder;
import tk.labyrinth.pandora.stores.extra.objectproxy.ObjectProxyHandler;

import javax.annotation.CheckForNull;
import javax.annotation.processing.Generated;
import javax.lang.model.element.Modifier;

// TODO: Do we need them?
//@AutoService(Processor.class)
@Slf4j
public class ObjectProxyBuilderGeneratingAnnotationProcessor extends CallbackAnnotationProcessor {

	{
		onEachRound(round -> {
			JaapObjectModelFactory modelFactory = new JaapObjectModelFactory();
			//
			RoundContext roundContext = RoundContext.of(round);
			//
			roundContext.getAllTypeElements()
					.filter(TypeElementHandle::isInterface)
					.filter(typeElementHandle -> typeElementHandle.hasMergedAnnotation(
							GenerateBuilder.class,
							MergedAnnotationSpecification.javaCore()))
					.forEach(typeElementHandle -> {
						try {
							String modelCode = getModelCode(typeElementHandle);
							if (modelCode == null || modelCode.isEmpty()) {
								throw new IllegalArgumentException("Require non-empty modelCode: typeElementHandle = %s"
										.formatted(typeElementHandle));
							}
							ObjectModel objectModel = modelFactory.createModelFromTypeElementHandle(typeElementHandle);
							//
							TypeSpec typeSpec = createBuilderSpec(typeElementHandle, objectModel, modelCode);
							//
							JavaFile javaFile = JavaFile
									.builder(typeElementHandle.getPackageQualifiedName(), typeSpec)
									.build();
							//
							IoUtils.unchecked(() -> javaFile.writeTo(round.getProcessingEnvironment().getFiler()));
						} catch (RuntimeException ex) {
							logger.warn("", ex);
						}
					});
		});
	}

	@CheckForNull
	private static String getModelCode(TypeElementHandle typeElementHandle) {
		String result;
		{
			MergedAnnotation modelAnnotation = typeElementHandle.getMergedAnnotation(
					Model.class,
					MergedAnnotationSpecification.metaAnnotation());
			if (modelAnnotation.isPresent()) {
				String code = modelAnnotation.getAttributeValueAsString("code");
				if (!code.isEmpty()) {
					result = code;
				} else {
					result = modelAnnotation.getAttributeValueAsString("value");
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	public static TypeSpec createBuilderSpec(
			TypeElementHandle typeElementHandle,
			ObjectModel objectModel,
			String modelCode) {
		ClassName objectProxyHandlerClassName = ClassName.get(ObjectProxyHandler.class);
		ClassName converterRegistryClassName = ClassName.get(ConverterRegistry.class);
		ClassName genericObjectClassName = ClassName.get(GenericObject.class);
		//
		ClassName tClassName = pandoraSignatureToClassName(typeElementHandle.getQualifiedName());
		ClassName builderClassName = pandoraSignatureToClassName("%sBuilder".formatted(tClassName));
		//
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(builderClassName);
		{
			// ANNOTATION: @Generated("tk.labyrinth.satool.beholder.generators.BeholderObjectProxyBuilderGeneratingAnnotationProcessor")
			typeBuilder.addAnnotation(AnnotationSpec
					.builder(Generated.class)
					.addMember(
							"value",
							"\"%s\"".formatted(ObjectProxyBuilderGeneratingAnnotationProcessor.class.getName()))
					.build());
		}
		{
			// CLASS: public TBuilder {
			typeBuilder.addModifiers(Modifier.PUBLIC);
		}
		{
			objectModel.getAttributes().forEach(attribute -> {
				String attributeName = attribute.getName();
				TypeName attributeTypeName = BuilderGeneratingAnnotationProcessor.typeDescriptionToTypeName(
						BuilderGeneratingAnnotationProcessor.datatypeToTypeDescription(attribute.getDatatype()));
				//
				// FIELD: private A a;
				typeBuilder.addField(FieldSpec
						.builder(
								attributeTypeName,
								attributeName,
								Modifier.PRIVATE)
						.build());
				//
				// GETTER: public A a();
				typeBuilder.addMethod(MethodSpec
						.methodBuilder(attributeName)
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeTypeName)
						.addCode("return %s;".formatted(attributeName))
						.build());
				//
				// SETTER: public TBuilder a(A a);
				typeBuilder.addMethod(MethodSpec
						.methodBuilder(attributeName)
						.addModifiers(Modifier.PUBLIC)
						.returns(builderClassName)
						.addParameter(attributeTypeName, attributeName)
						.addCode("""
								this.%1$s = %1$s;
								return this;""".formatted(attributeName))
						.build());
			});
		}
		{
			// RESULT METHOD: public T build(ObjectProxyHandler objectProxyHandler);
			typeBuilder.addMethod(MethodSpec
					.methodBuilder("build")
					.addModifiers(Modifier.PUBLIC)
					.returns(tClassName)
					.addParameter(objectProxyHandlerClassName, "objectProxyHandler")
					.addCode("""
							return objectProxyHandler.wrap(
									toObject(objectProxyHandler.getConverterRegistry()),
									%s.class);"""
							.formatted(typeElementHandle.getQualifiedName()))
					.build());
		}
		{
			// RESULT METHOD: public GenericObject toObject(ConverterRegistry converterRegistry);
			typeBuilder.addMethod(MethodSpec
					.methodBuilder("toObject")
					.addModifiers(Modifier.PUBLIC)
					.returns(genericObjectClassName)
					.addParameter(converterRegistryClassName, "converterRegistry")
					.addCode("""
							return GenericObject.of(
									io.vavr.collection.LinkedHashMap.<String,Object>empty()
							%s
									.toList()
									.map(tuple -> %s.of(
											tuple._1(),
											converterRegistry.convert(tuple._2(), %s.class))));"""
							.formatted(
									objectModel.getAttributes()
											.map(attribute -> "\t\t\t\t.put(\"%1$s\", %1$s)"
													.formatted(attribute.getName()))
											.append("\t\t\t\t.put(\"modelReference\", %s.of(\"%s\"))"
													.formatted(CodeObjectModelReference.class.getName(), modelCode))
											.mkString("\n"),
									GenericObjectAttribute.class.getName(),
									ValueWrapper.class.getName()))
					.build());
		}
		{
			// FACTORY METHOD: public static TBuilder create();
			typeBuilder.addMethod(MethodSpec
					.methodBuilder("create")
					.addModifiers(Modifier.PUBLIC, Modifier.STATIC)
					.returns(builderClassName)
					.addCode("return new %s();".formatted(builderClassName))
					.build());
		}
		{
			// FACTORY METHOD: public static TBuilder from(T t);
			typeBuilder.addMethod(MethodSpec
					.methodBuilder("from")
					.addModifiers(Modifier.PUBLIC, Modifier.STATIC)
					.returns(builderClassName)
					.addParameter(tClassName, modelCode)
					.addCode("""
							%1$s builder = new %1$s();
							%2$s
							return builder;"""
							.formatted(
									builderClassName,
									objectModel.getAttributes()
											.map(attribute -> "builder.%s(%s.%s());".formatted(
													attribute.getName(),
													modelCode,
													ObjectModelUtils.getGetterName(attribute.getName())))
											.mkString("\n")))
					.build());
		}
		return typeBuilder.build();
	}

	public static ClassName pandoraSignatureToClassName(String pandoraSignature) {
		return ClassName.bestGuess(pandoraSignature);
	}
}
