package tk.labyrinth.pandora.stores.domain.mongodb;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootConstants;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.Objects;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DefaultMongoDbCollectionLocator implements MongoDbCollectionLocator {

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@Override
	public Pair<GenericObject, String> locateCollection(StoreConfiguration storeConfiguration, GenericObject object) {
		Pair<GenericObject, String> result;
		{
			GenericObjectAttribute modelReferenceAttribute = object.getAttribute(
					PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME);
			//
			CodeObjectModelReference objectModelReference = CodeObjectModelReference.from(
					modelReferenceAttribute.getValue().asSimple().getValue());
			//
			ObjectModel objectModel = objectModelSearcher.getSingle(objectModelReference);
			//
			String explicitCollectionName = PandoraMongoDbUtils.findCollectionName(objectModel.getTagsOrEmpty());
			//
			result = Pair.of(
					object.withoutAttribute(PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME),
					explicitCollectionName != null ? explicitCollectionName : objectModel.getCode());
		}
		return result;
	}

	@Override
	public CollectionContext locateCollection(StoreConfiguration storeConfiguration, @Nullable Predicate predicate) {
		CollectionContext result;
		{
			CodeObjectModelReference objectModelReference = extractModelReference(predicate);
			//
			ObjectModel objectModel = objectModelSearcher.getSingle(objectModelReference);
			//
			String explicitCollectionName = PandoraMongoDbUtils.findCollectionName(objectModel.getTagsOrEmpty());
			//
			result = CollectionContext.of(
					explicitCollectionName != null ? explicitCollectionName : objectModelReference.getCode(),
					List.of(
							GenericObjectAttribute.ofSimple(
									PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME,
									objectModelReference.toString())),
					removeModelReferencePredicate(predicate));
		}
		return result;
	}

	@Override
	public CodeObjectModelReference objectModelReferenceFromCollectionName(String collectionName) {
		return CodeObjectModelReference.of(collectionName);
	}

	public static CodeObjectModelReference extractModelReference(Predicate predicate) {
		ObjectPropertyPredicate modelReferencePredicate = findModelReferencePredicate(predicate);
		//
		Objects.requireNonNull(modelReferencePredicate, "modelReferencePredicate");
		//
		return CodeObjectModelReference.from(modelReferencePredicate.value().toString());
	}

	@Nullable
	public static ObjectPropertyPredicate findModelReferencePredicate(Predicate predicate) {
		ObjectPropertyPredicate result;
		{
			if (predicate instanceof JunctionPredicate junctionPredicate) {
				if (junctionPredicate.isAnd()) {
					List<ObjectPropertyPredicate> predicates = junctionPredicate.predicates().stream()
							.map(DefaultMongoDbCollectionLocator::findModelReferencePredicate)
							.filter(Objects::nonNull)
							.collect(List.collector());
					//
					if (predicates.size() > 1) {
						throw new NotImplementedException();
					} else {
						result = !predicates.isEmpty() ? predicates.single() : null;
					}
				} else {
					result = null;
				}
			} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
				if (Objects.equals(
						objectPropertyPredicate.property(),
						PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME)) {
					result = objectPropertyPredicate;
				} else {
					result = null;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static Predicate removeModelReferencePredicate(Predicate predicate) {
		Predicate result;
		{
			if (predicate instanceof JunctionPredicate junctionPredicate) {
				JunctionPredicate nextJunctionPredicate = new JunctionPredicate(
						junctionPredicate.operator(),
						junctionPredicate.predicates().stream()
								.map(DefaultMongoDbCollectionLocator::removeModelReferencePredicate)
								.filter(Objects::nonNull)
								.collect(Collectors.toList()));
				//
				result = !nextJunctionPredicate.predicates().isEmpty() ? nextJunctionPredicate : null;
			} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
				if (Objects.equals(
						objectPropertyPredicate.property(),
						PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME)) {
					result = null;
				} else {
					result = objectPropertyPredicate;
				}
			} else {
				result = predicate;
			}
		}
		return result;
	}
}
