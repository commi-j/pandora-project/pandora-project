package tk.labyrinth.pandora.stores.domain.mongodb;

import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;

public class MongoDbDatatypeConstants {

	public static final String DATE_ALIAS = "MONGO_DB_DATE";

	public static final Datatype DATE_DATATYPE = Datatype.ofNonParameterized(AliasDatatypeBaseReference.of(DATE_ALIAS));

	public static final String OBJECT_ID_ALIAS = "MONGO_DB_OBJECT_ID";

	public static final Datatype OBJECT_ID_DATATYPE = Datatype.ofNonParameterized(
			AliasDatatypeBaseReference.of(OBJECT_ID_ALIAS));
}
