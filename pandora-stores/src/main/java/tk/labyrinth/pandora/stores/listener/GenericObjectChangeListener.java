package tk.labyrinth.pandora.stores.listener;

import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

public interface GenericObjectChangeListener {

	void onGenericObjectChange(ObjectChangeEvent<GenericObject> event);
}
