package tk.labyrinth.pandora.stores.domain.rootobject;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeaturesContributor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

@Bean
@RequiredArgsConstructor
public class RootObjectModelFeatureContributor implements ObjectModelFeaturesContributor {

	@Override
	public List<ObjectModelFeature> contributeFeatures(Class<?> javaClass) {
		String signatureAttributeName = findSignatureAttributeName(javaClass);
		//
		return signatureAttributeName != null
				? List.of(RootObjectModelFeature.of(signatureAttributeName))
				: List.empty();
	}

	@Nullable
	public static String findSignatureAttributeName(Class<?> javaClass) {
		String result;
		{
			MergedAnnotation<RootModel> rootModelMergedAnnotation = MergedAnnotations
					.from(javaClass)
					.get(RootModel.class);
			//
			if (rootModelMergedAnnotation.isPresent()) {
				RootModel rootModelAnnotation = rootModelMergedAnnotation.synthesize();
				//
				result = !rootModelAnnotation.primaryAttribute().isEmpty() ? rootModelAnnotation.primaryAttribute() : "uid";
			} else {
				result = null;
			}
		}
		return result;
	}
}
