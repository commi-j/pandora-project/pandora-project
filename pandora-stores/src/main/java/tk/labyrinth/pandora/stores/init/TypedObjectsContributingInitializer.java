package tk.labyrinth.pandora.stores.init;

import io.vavr.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

public abstract class TypedObjectsContributingInitializer<T> extends GenericObjectsContributingInitializer {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Override
	protected List<GenericObject> contributeObjects() {
		return doContributeObjects().map(object -> postProcess(converterRegistry.convert(object, GenericObject.class)));
	}

	protected abstract List<T> doContributeObjects();

	protected abstract GenericObject postProcess(GenericObject genericObject);
}
