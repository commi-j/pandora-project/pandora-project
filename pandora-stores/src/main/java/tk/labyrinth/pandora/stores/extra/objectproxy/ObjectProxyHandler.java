package tk.labyrinth.pandora.stores.extra.objectproxy;

import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.Map;
import io.vavr.collection.Stream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.datatypes.objectproxy.ObjectProxy;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class ObjectProxyHandler {

	/**
	 * Getter is used by generated ObjectProxyBuilders.
	 */
	@Getter
	private final ConverterRegistry converterRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private Object handleDefaultMethod(Object proxy, Method method, Object[] args) throws ReflectiveOperationException {
		Method methodToInvoke = Stream.of(method.getDeclaringClass().getDeclaredMethods())
				.filter(Method::isDefault)
				.filter(innerMethod -> Objects.equals(innerMethod.getName(), method.getName()))
				.single();
		return methodToInvoke.invoke(null, ArrayUtils.addFirst(args, proxy));
	}

	private Object handleMethods(GenericObject object, Class<?> type, Object proxy, Method method, Object[] args)
			throws ReflectiveOperationException {
		Object result;
		{
			if (!method.isDefault()) {
				String attributeName = ObjectModelUtils.findAttributeName(method);
				if (attributeName != null) {
					result = converterRegistry.convert(
							object.findAttributeValue(attributeName),
							method.getGenericReturnType());
				} else {
					throw new NotImplementedException();
				}
			} else {
				result = handleDefaultMethod(proxy, method, args);
			}
		}
		return result;
	}

	private Object handleObjectMethods(GenericObject object, Class<?> type, Method method, Object[] args) {
		Object result;
		{
			if (Objects.equals(method.getName(), "equals") && method.getParameterCount() == 1 &&
					method.getParameters()[0].getType() == Object.class) {
				Object argument;
				{
					if (args[0] instanceof ObjectProxy objectProxy) {
						argument = objectProxy.getObject();
					} else {
						argument = args[0];
					}
				}
				result = object.equals(argument);
			} else if (Objects.equals(method.getName(), "hashCode") && method.getParameterCount() == 0) {
				result = object.hashCode();
			} else if (Objects.equals(method.getName(), "toString") && method.getParameterCount() == 0) {
				result = "%s(%s)".formatted(
						type.getSimpleName(),
						object.getAttributes()
								.map(attribute -> "(%s=%s)".formatted(attribute.getName(), attribute.getValue()))
								.mkString(","));
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	private Object handleObjectProxyMethods(
			GenericObject object,
			Class<?> type,
			Method method,
			Object[] args) {
		Object result;
		{
			if (Objects.equals(method.getName(), "getObject") &&
					method.getParameterCount() == 0) {
				result = object;
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public <T> T create(Class<T> objectProxyClass, Map<String, Object> attributes) {
		CodeObjectModelReference objectModelReference = javaBaseTypeRegistry.getExplicitObjectModelReference(
				objectProxyClass);
		//
		return wrap(
				GenericObject.of(attributes
						.put(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME, objectModelReference)
						.toList()
						.map(tuple -> GenericObjectAttribute.of(
								tuple._1(),
								converterRegistry.convert(tuple._2(), ValueWrapper.class)))),
				objectProxyClass);
	}

	public <T> T createNew(Class<T> objectProxyClass) {
		return create(objectProxyClass, LinkedHashMap.empty());
	}

	@SuppressWarnings("unchecked")
	public <T> T wrap(GenericObject object, Class<T> objectProxyClass) {
		if (!objectProxyClass.isInterface()) {
			throw new IllegalArgumentException("Require interface: objectProxyClass = %s, object = %s"
					.formatted(objectProxyClass, object));
		}
		//
		CodeObjectModelReference objectModelReference = PandoraRootUtils.findModelReference(object);
		CodeObjectModelReference javaClassModelReference = javaBaseTypeRegistry.getExplicitObjectModelReference(
				objectProxyClass);
		//
		if (objectModelReference != null &&
				!Objects.equals(PandoraRootUtils.getModelReference(object), javaClassModelReference)) {
			throw new IllegalArgumentException("Require matching modelReferences: object = %s, objectProxyClass = %s"
					.formatted(object, objectProxyClass));
		}
		//
		GenericObject objectToUse;
		if (objectModelReference != null) {
			objectToUse = object;
		} else {
			objectToUse = RootObjectUtils.withNonNullModelReference(object, javaClassModelReference);
		}
		//
		return (T) Proxy.newProxyInstance(
				Thread.currentThread().getContextClassLoader(),
				new Class[]{objectProxyClass, ObjectProxy.class},
				(proxy, method, args) -> {
					Object result;
					{
						if (method.getDeclaringClass() == Object.class) {
							result = handleObjectMethods(objectToUse, objectProxyClass, method, args);
						} else if (method.getDeclaringClass() == ObjectProxy.class) {
							result = handleObjectProxyMethods(objectToUse, objectProxyClass, method, args);
						} else {
							result = handleMethods(objectToUse, objectProxyClass, proxy, method, args);
						}
					}
					return result;
				});
	}
}
