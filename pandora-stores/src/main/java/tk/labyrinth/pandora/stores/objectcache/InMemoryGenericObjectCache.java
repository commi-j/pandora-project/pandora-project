package tk.labyrinth.pandora.stores.objectcache;

import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectFilterer;
import tk.labyrinth.pandora.stores.genericobject.SimpleInMemoryGenericObjectStore;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class InMemoryGenericObjectCache implements GenericObjectCache {

	private final GenericObjectFilterer genericObjectFilterer;

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	@SmartAutowired
	private GenericObject filterObject;

	private SimpleInMemoryGenericObjectStore simpleInMemoryGenericObjectStore;

	@PostConstruct
	private void postConstruct() {
		simpleInMemoryGenericObjectStore = new SimpleInMemoryGenericObjectStore(null);
		//
		// FIXME: Looks like some crutch.
		simpleInMemoryGenericObjectStore.setGenericObjectChangeListeners(List.empty());
		simpleInMemoryGenericObjectStore.setGenericObjectFilterer(genericObjectFilterer);
		simpleInMemoryGenericObjectStore.setRootObjectPrimaryReferenceHandler(rootObjectPrimaryReferenceHandler);
	}

	@Override
	public void put(GenericObject object) {
		simpleInMemoryGenericObjectStore.createOrUpdate(object);
	}

	@Override
	public List<GenericObject> search(PlainQuery contextlessQuery) {
		return simpleInMemoryGenericObjectStore.search(contextlessQuery);
	}

	@Override
	public boolean supports(PlainQuery contextlessQuery) {
		return !genericObjectFilterer.filter(List.of(filterObject), contextlessQuery).isEmpty();
	}
}
