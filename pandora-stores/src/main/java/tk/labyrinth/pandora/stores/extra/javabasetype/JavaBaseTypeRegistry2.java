package tk.labyrinth.pandora.stores.extra.javabasetype;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.BackingJavaClassDatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.BackingJavaVariableTypeDatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;

import java.lang.reflect.Type;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class JavaBaseTypeRegistry2 {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final ConcurrentMap<Class<?>, Pair<JavaBaseType, @Nullable ObjectModel>> entries =
			new ConcurrentHashMap<>();

	private final ObjectModelRegistry objectModelRegistry;

	private final JavaBaseTypeRegistry oldRegistry;

	@SmartAutowired
	private TypedObjectManipulator<JavaBaseType> javaBaseTypeManipulator;

	@Nullable
	public CodeObjectModelReference findExplicitObjectModelReference(Class<?> javaClass) {
		return getJavaBaseType(javaClass).getObjectModelReference();
	}

	@Nullable
	public JavaBaseType findJavaBaseType(Reference<DatatypeBase> datatypeBaseReference) {
		JavaBaseType result;
		{
			DatatypeBase datatypeBase = datatypeBaseRegistry.findDatatypeBase(datatypeBaseReference);
			//
			if (datatypeBase != null) {
				SignatureJavaBaseTypeReference javaBaseTypeReference = JavaBaseTypeUtils.findJavaBaseTypeReference(
						datatypeBase);
				//
				result = javaBaseTypeReference != null
						? getJavaBaseType(javaBaseTypeReference)
						: null;
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public JavaBaseType findJavaBaseType(CodeObjectModelReference objectModelReference) {
		JavaBaseType result;
		{
			JavaBaseType jbt = oldRegistry.findJavaBaseType(objectModelReference);
			//
			if (jbt != null) {
				result = jbt;
			} else {
				JavaBaseType javaBaseType = List.ofAll(entries.values())
						.filter(pair -> Objects.equals(
								pair.getLeft().getObjectModelReference(),
								objectModelReference))
						.map(Pair::getLeft)
						.getOrNull();
				//
				if (javaBaseType != null) {
					result = javaBaseType;
				} else {
					SignatureJavaBaseTypeReference javaBaseTypeReference = JavaBaseTypeUtils.findJavaBaseTypeReference(
							objectModelReference);
					//
					if (javaBaseTypeReference != null) {
						throw new NotImplementedException();
//						result = createJavaBaseType(javaBaseTypeReference.resolveClass());
					} else {
						result = null;
					}
				}
			}
		}
		return result;
	}

	@Nullable
	public SignatureJavaBaseTypeReference findJavaBaseTypeReference(Reference<DatatypeBase> datatypeBaseReference) {
		SignatureJavaBaseTypeReference result;
		{
			DatatypeBase datatypeBase = datatypeBaseRegistry.findDatatypeBase(datatypeBaseReference);
			//
			if (datatypeBase != null) {
				result = JavaBaseTypeUtils.findJavaBaseTypeReference(datatypeBase);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public SignatureJavaBaseTypeReference findJavaBaseTypeReference(Datatype datatype) {
		return findJavaBaseTypeReference(datatype.getBaseReference());
	}

	@Nullable
	public Class<?> findJavaClass(Reference<DatatypeBase> datatypeBaseReference) {
		JavaBaseType javaBaseType = findJavaBaseType(datatypeBaseReference);
		//
		return javaBaseType != null ? javaBaseType.resolveClass() : null;
	}

	@Nullable
	public CodeObjectModelReference findObjectModelReference(Class<?> javaClass) {
		return objectModelRegistry.findObjectModelReference(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(javaClass));
	}

	public CodeObjectModelReference getExplicitObjectModelReference(Class<?> javaClass) {
		// TODO: Prettify.
		return Objects.requireNonNull(findExplicitObjectModelReference(javaClass), javaClass.getName());
	}

	public JavaBaseType getJavaBaseType(CodeObjectModelReference objectModelReference) {
		// TODO: Prettify.
		return Objects.requireNonNull(findJavaBaseType(objectModelReference), objectModelReference.toString());
	}

	public JavaBaseType getJavaBaseType(Class<?> javaClass) {
		JavaBaseType result;
		{
			Pair<JavaBaseType, @Nullable ObjectModel> foundPair = entries.get(javaClass);
			//
			if (foundPair != null) {
				result = foundPair.getLeft();
			} else {
				result = registerJavaClass(javaClass);
			}
		}
		return result;
	}

	public JavaBaseType getJavaBaseType(SignatureJavaBaseTypeReference javaBaseTypeReference) {
		return getJavaBaseType(javaBaseTypeReference.resolveClass());
	}

	public Class<?> getJavaClass(Reference<DatatypeBase> datatypeBaseReference) {
		// TODO: Prettify.
		return Objects.requireNonNull(findJavaClass(datatypeBaseReference), datatypeBaseReference.toString());
	}

	public Type getJavaType(Datatype datatype) {
		Type result;
		{
			DatatypeBase datatypeBase = datatypeBaseRegistry.getSingle(datatype.getBaseReference());
			//
			BackingJavaClassDatatypeBaseFeature backingJavaClassDatatypeBaseFeature = datatypeBase.findFeature(
					BackingJavaClassDatatypeBaseFeature.class);
			//
			if (backingJavaClassDatatypeBaseFeature != null) {
				//
				Class<?> baseClass = ClassUtils.get(backingJavaClassDatatypeBaseFeature.getClassSignature().getBinaryName());
				//
				if (datatype.getParameters() != null) {
					result = org.apache.commons.lang3.reflect.TypeUtils.parameterize(
							baseClass,
							datatype.getParameters().map(this::getJavaType).toJavaArray(Type[]::new));
				} else {
					result = baseClass;
				}
			} else {
				BackingJavaVariableTypeDatatypeBaseFeature backingJavaVariableTypeDatatypeBaseFeature = datatypeBase
						.findFeature(BackingJavaVariableTypeDatatypeBaseFeature.class);
				//
				if (backingJavaVariableTypeDatatypeBaseFeature != null) {
					Pair<TypeSignature, String> destructuredVariable = backingJavaVariableTypeDatatypeBaseFeature
							.getVariableTypeDescription().destructureVariable();
					//
					Class<?> javaClass = ClassUtils.get(destructuredVariable.getLeft().getClassSignature().getBinaryName());
					//
					result = List.of(javaClass.getTypeParameters())
							.find(typeParameter -> Objects.equals(typeParameter.getName(), destructuredVariable.getRight()))
							.get();
				} else {
					if (datatypeBase.getParentReference() != null) {
						result = getJavaType(Datatype.builder()
								.baseReference(datatypeBase.getParentReference())
								.parameters(datatype.getParameters())
								.build());
					} else {
						throw new NotImplementedException();
					}
				}
			}
		}
		return result;
	}

	public ObjectModel getObjectModel(Class<?> javaClass) {
		return objectModelRegistry.getObjectModel(objectModelRegistry.findObjectModelReference(
				JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(javaClass)));
	}

	public CodeObjectModelReference getObjectModelReference(@NonNull Class<?> javaClass) {
		CodeObjectModelReference result = findObjectModelReference(javaClass);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: %s".formatted(javaClass.getName()));
		}
		//
		return result;
	}

	public void registerJavaBaseType(JavaBaseType javaBaseType) {
		entries.put(javaBaseType.resolveClass(), Pair.of(javaBaseType, null));
	}

	public JavaBaseType registerJavaClass(Class<?> javaClass) {
		JavaBaseType result;
		{
			Pair<JavaBaseType, @Nullable ObjectModel> pair = entries.get(javaClass);
			//
			if (pair == null) {
				logger.info("Registering dynamic JavaClass: {}", javaClass.getName());
				//
				JavaBaseType javaBaseType = oldRegistry.createJavaBaseType(javaClass);
				//
				// FIXME: It should be create only, but due to certain architecture issues we have collision and this is dirty fix.
				javaBaseTypeManipulator.createWithAdjustment(javaBaseType, PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
				//
				result = javaBaseType;
			} else {
				logger.info("Duplicate request to register dynamic JavaClass: {}", javaClass.getName());
				//
				result = pair.getLeft();
			}
		}
		return result;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class JavaClassObject {

		@Nullable
		ObjectModel customObjectModel;

		@NonNull
		JavaBaseType javaBaseType;
	}
}
