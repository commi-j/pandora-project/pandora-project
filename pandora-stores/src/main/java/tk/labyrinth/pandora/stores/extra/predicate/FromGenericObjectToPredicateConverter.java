package tk.labyrinth.pandora.stores.extra.predicate;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class FromGenericObjectToPredicateConverter implements Converter<GenericObject, Predicate> {

	@Override
	public Predicate convert(GenericObject source) {
		Predicate result;
		{
			ValueWrapper propertyValue = source.findAttributeValue("property");
			if (propertyValue != null) {
				ValueWrapper valueValue = source.getAttributeValue("value");
				result = new ObjectPropertyPredicate(
						ObjectOperator.valueOf(source.getAttributeValueAsString("operator")),
						propertyValue.asSimple().getValue(),
						valueValue.unwrap());
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
