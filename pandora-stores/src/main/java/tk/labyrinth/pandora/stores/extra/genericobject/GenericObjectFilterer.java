package tk.labyrinth.pandora.stores.extra.genericobject;

import io.vavr.collection.List;
import io.vavr.collection.Traversable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.domain.java.JavaContext;
import tk.labyrinth.expresso.query.domain.java.JavaQueryExecutor;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.predicate.PredicateTool;

@LazyComponent
@RequiredArgsConstructor
public class GenericObjectFilterer {

	private final PredicateTool predicateTool;

	private final JavaQueryExecutor queryExecutor;

	private ParameterizedQuery<Class<GenericObject>> adjustQuery(PlainQuery query) {
		return ParameterizedQuery.builderWithClassParameter(GenericObject.class)
				.limit(query.getLimit())
				.offset(query.getOffset())
				.predicate(predicateTool.normalizePredicate(query.getPredicate()))
				.sort(query.getSort())
				.build();
	}

	public long count(Traversable<GenericObject> objects, PlainQuery query) {
		ParameterizedQuery<Class<GenericObject>> adjustedQuery = adjustQuery(query);
		//
		return queryExecutor.count(new PandoraJavaContext(objects), adjustedQuery);
	}

	public List<GenericObject> filter(Traversable<GenericObject> objects, PlainQuery query) {
		ParameterizedQuery<Class<GenericObject>> adjustedQuery = adjustQuery(query);
		//
		return queryExecutor
				.search(new PandoraJavaContext(objects), adjustedQuery)
				.stream()
				.collect(List.collector());
	}

	public boolean matches(GenericObject object, Predicate predicate) {
		ParameterizedQuery<Class<GenericObject>> adjustedQuery = adjustQuery(PlainQuery.of(predicate));
		//
		return queryExecutor.count(new PandoraJavaContext(List.of(object)), adjustedQuery) > 0;
	}

	@Value
	public static class PandoraJavaContext implements JavaContext {

		@NonNull
		Traversable<?> traversable;

		@Override
		@SuppressWarnings("unchecked")
		public <T> java.util.stream.Stream<T> getStream(Class<T> type) {
			return (java.util.stream.Stream<T>) traversable.toJavaStream();
		}
	}
}
