package tk.labyrinth.pandora.stores.domain.mongodb.codec;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
public class MongoDbGenericObjectCodec implements Codec<GenericObject> {

	private final DocumentCodec documentCodec;

	@Autowired
	private ConverterRegistry converterRegistry;

	public MongoDbGenericObjectCodec(CodecRegistry codecRegistry) {
		documentCodec = new DocumentCodec(codecRegistry);
	}

	@Override
	public GenericObject decode(BsonReader reader, DecoderContext decoderContext) {
		return converterRegistry.convert(
				decoderContext.decodeWithChildContext(documentCodec, reader),
				GenericObject.class);
	}

	@Override
	public void encode(BsonWriter writer, GenericObject value, EncoderContext encoderContext) {
		encoderContext.encodeWithChildContext(documentCodec, writer, converterRegistry.convert(value, Document.class));
	}

	@Override
	public Class<GenericObject> getEncoderClass() {
		return GenericObject.class;
	}
}
