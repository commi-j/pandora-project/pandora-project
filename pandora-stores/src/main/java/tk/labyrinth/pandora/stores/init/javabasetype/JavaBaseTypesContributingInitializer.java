package tk.labyrinth.pandora.stores.init.javabasetype;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;

@LazyComponent
@Priority(60)
@RequiredArgsConstructor
public class JavaBaseTypesContributingInitializer extends GenericObjectsContributingInitializer {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	@Override
	protected List<GenericObject> contributeObjects() {
		return javaBaseTypeRegistry.getJavaBaseTypeEntries()
				.map(entry -> convert(entry).withAddedAttributes(
						PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(JavaBaseType.SIGNATURE_ATTRIBUTE_NAME),
						PandoraHardcodedConstants.ATTRIBUTE));
	}
}
