package tk.labyrinth.pandora.stores.objectindex;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;

/**
 * @param <T> Target
 * @param <I> Index
 */
public interface Indexer<T, I extends ObjectIndex<T>> {

	/**
	 * @param target non-null
	 *
	 * @return non-null
	 */
	List<I> createIndices(T target);

	/**
	 * Creates reference that will be used to delete outdated indices.<br>
	 * FIXME: This is a temporal solution, we should automate deletion reference detection.<br>
	 *
	 * @return non-null
	 */
	Reference<T> createTargetReference(T target);
}
