package tk.labyrinth.pandora.stores.datatypes.objectmodelparameter;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.objectmodelparameter.ObjectModelParameter;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectindex.Indexer;

@LazyComponent
public class ObjectModelParameterIndexer implements Indexer<ObjectModel, ObjectModelParameter> {

	@Override
	public List<ObjectModelParameter> createIndices(ObjectModel target) {
		return target.getParameterNames() != null
				? target.getParameterNames().zipWithIndex().map(tuple -> ObjectModelParameter.builder()
				.index(tuple._2())
				.modelCode(target.getCode())
				.name(tuple._1())
				.build())
				: List.empty();
	}

	@Override
	public Reference<ObjectModel> createTargetReference(ObjectModel target) {
		return CodeObjectModelReference.from(target);
	}
}
