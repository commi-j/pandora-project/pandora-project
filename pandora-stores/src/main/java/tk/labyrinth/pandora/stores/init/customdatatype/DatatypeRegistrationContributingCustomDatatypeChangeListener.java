package tk.labyrinth.pandora.stores.init.customdatatype;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.customdatatype.CustomDatatype;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.AliasDatatypeRegistrationReference;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.DatatypeRegistration;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.BackingJavaClassDatatypeBaseFeature;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class DatatypeRegistrationContributingCustomDatatypeChangeListener extends ObjectChangeListener<CustomDatatype> {

	@SmartAutowired
	private TypedObjectManipulator<DatatypeRegistration> datatypeRegistrationManipulator;

	@Override
	protected void onObjectChange(ObjectChangeEvent<CustomDatatype> event) {
		if (event.isCreateOrUpdate()) {
			CustomDatatype customDatatype = event.getNextObjectOrFail();
			//
			DatatypeRegistration datatypeRegistration = DatatypeRegistration.builder()
					.alias(customDatatype.getName())
					.features(customDatatype.getBackingTypeReference() != null
							? List.of(BackingJavaClassDatatypeBaseFeature.of(
							customDatatype.getBackingTypeReference().getSignature().getClassSignature()))
							: List.empty())
					.parentReference(customDatatype.getParentReference())
					.name(customDatatype.getName())
					.parameters(customDatatype.getParameterNames() != null
							? customDatatype.getParameterNames().map(Function.identity())
							: null)
					.signature(customDatatype.getName())
					.build();
			//
			if (event.isCreate()) {
				datatypeRegistrationManipulator.createWithAdjustment(
						datatypeRegistration,
						DatatypeRegistrationContributingCustomDatatypeChangeListener::adjust);
			} else {
				datatypeRegistrationManipulator.updateWithAdjustment(
						datatypeRegistration,
						DatatypeRegistrationContributingCustomDatatypeChangeListener::adjust);
			}
		} else {
			datatypeRegistrationManipulator.delete(AliasDatatypeRegistrationReference.of(
					event.getPreviousObjectOrFail().getName()));
		}
	}

	public static GenericObject adjust(GenericObject genericObject) {
		return genericObject.withAddedAttribute(PandoraIndexConstants.ATTRIBUTE);
	}
}
