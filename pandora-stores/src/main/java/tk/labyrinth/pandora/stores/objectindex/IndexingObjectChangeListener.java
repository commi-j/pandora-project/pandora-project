package tk.labyrinth.pandora.stores.objectindex;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.GenericObjectChangeListener;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;

@LazyComponent
@RequiredArgsConstructor
public class IndexingObjectChangeListener implements GenericObjectChangeListener {

	private final ObjectIndexer objectIndexer;

	@Override
	public void onGenericObjectChange(ObjectChangeEvent<GenericObject> event) {
		objectIndexer.updateIndices(event);
	}
}
