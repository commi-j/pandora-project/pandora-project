package tk.labyrinth.pandora.stores.tool.init;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.util.function.UnaryOperator;

public abstract class GenericObjectContributorBase {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private GenericObjectManipulator genericObjectManipulator;

	protected GenericObject convert(@NonNull Object object) {
		return converterRegistry.convert(object, GenericObject.class);
	}

	protected GenericObject convertAndAddHardcoded(@NonNull Object object) {
		return convert(object).withAddedAttribute(PandoraHardcodedConstants.ATTRIBUTE);
	}

	@Deprecated
	protected GenericObject convertAndAddHashCodeBasedUid(
			@NonNull Object object,
			@NonNull Object valueForUidComputation) {
		return convert(object)
				.withAddedAttribute(RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation));
	}

	protected GenericObject convertAndAddHashCodeBasedUidAndPostProcess(
			@NonNull Object object,
			@NonNull Object valueForUidComputation,
			@NonNull UnaryOperator<GenericObject> postProcessor) {
		return postProcessor.apply(convert(object)
				.withAddedAttribute(RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation)));
	}

	protected GenericObject convertAndPostProcess(
			@NonNull Object object,
			@NonNull UnaryOperator<GenericObject> postProcessor) {
		return postProcessor.apply(convert(object));
	}
}
