package tk.labyrinth.pandora.stores.objectsearcher;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;

import java.util.Objects;

@PrototypeScopedComponent
@RequiredArgsConstructor
@Slf4j
public class TypedObjectSearcher<T> extends ObjectSearcherBase<PlainQuery, T> {

	private final ConverterRegistry converterRegistry;

	private final GenericObjectSearcher genericObjectSearcher;

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	@SmartAutowired
	private Class<T> javaClass;

	private CodeObjectModelReference objectModelReference;

	@WithSpan
	private T convertObject(GenericObject object) {
		T result;
		try {
			result = converterRegistry.convert(object, javaClass);
		} catch (RuntimeException ex) {
			// TODO: Return complex object that contains results and error breakdown.
			logger.error("", ex);
			result = null;
		}
		return result;
	}

	@WithSpan
	private List<T> convertObjects(List<GenericObject> objects) {
		return objects.map(this::convertObject)
				// FIXME: Conversion may return null (in case of exception), so ignoring nulls may misfire.
				.filter(Objects::nonNull);
	}

	@PostConstruct
	private void postConstruct() {
		objectModelReference = javaBaseTypeRegistry.getNormalizedObjectModelReference(javaClass);
	}

	@Override
	public long count(PlainQuery query) {
		return search(query).size();
	}

	@Override
	public <R extends Reference<T>> ResolvedReference<T, R> resolve(R reference) {
		ResolvedReference<GenericObject, GenericObjectReference> resolvedReference = genericObjectSearcher.resolve(
				GenericObjectReference.from(reference.toString()));
		//
		return ResolvedReference.of(
				reference,
				resolvedReference.getObjects().map(object -> converterRegistry.convert(object, javaClass)));
	}

	@Override
	@WithSpan
	public List<T> search(PlainQuery query) {
		List<GenericObject> foundObjects = genericObjectSearcher.search(query.withParameter(objectModelReference));
		//
		List<T> convertedObjects = convertObjects(foundObjects);
		//
		return convertedObjects;
	}

	public List<T> search(Predicate predicate) {
		return search(PlainQuery.builder()
				.predicate(predicate)
				.build());
	}

	public List<T> searchAll() {
		return search(PlainQuery.builder().build());
	}
}
