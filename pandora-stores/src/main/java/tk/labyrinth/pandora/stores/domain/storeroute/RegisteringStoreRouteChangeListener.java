package tk.labyrinth.pandora.stores.domain.storeroute;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;

@LazyComponent
@RequiredArgsConstructor
public class RegisteringStoreRouteChangeListener extends ObjectChangeListener<StoreRoute> {

	private final StoreRouteRegistry storeRouteRegistry;

	@Override
	protected void onObjectChange(ObjectChangeEvent<StoreRoute> event) {
		if (event.getPreviousObject() != null) {
			throw new NotImplementedException();
		}
		storeRouteRegistry.registerStoreRoute(event.getNextObjectOrFail());
	}
}
