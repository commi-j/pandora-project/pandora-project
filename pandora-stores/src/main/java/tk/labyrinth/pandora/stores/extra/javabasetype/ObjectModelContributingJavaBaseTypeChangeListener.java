package tk.labyrinth.pandora.stores.extra.javabasetype;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.origin.PandoraOriginUtils;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class ObjectModelContributingJavaBaseTypeChangeListener extends
		TypedObjectChangeListener<JavaBaseType, SignatureJavaBaseTypeReference> {

	private final ObjectModelFactory objectModelFactory;

	@SmartAutowired
	private TypedObjectManipulator<ObjectModel> objectModelManipulator;

	@Override
	protected void onObjectChange(TypedObjectChangeEvent<JavaBaseType, SignatureJavaBaseTypeReference> event) {
		if (event.hasPreviousObject()) {
			objectModelManipulator.delete(PandoraOriginUtils.createPredicate(event.getPrimaryReference()));
		}
		if (event.hasNextObject()) {
			JavaBaseType javaBaseType = event.getNextObjectOrFail();
			//
			if (javaBaseType.computeDatatypeKind() == DatatypeKind.OBJECT &&
					!Objects.equals(javaBaseType.getHasCustomObjectModel(), true)) {
				Class<?> javaClass = javaBaseType.resolveClass();
				//
				ObjectModel objectModel = objectModelFactory.createModelFromJavaClass(javaClass);
				//
				objectModelManipulator.createWithAdjustment(
						objectModel,
						genericObject -> genericObject.withAddedAttributes(
								PandoraIndexConstants.ATTRIBUTE,
								PandoraOriginUtils.createAttribute(event.getPrimaryReference())));
			}
		}
	}
}
