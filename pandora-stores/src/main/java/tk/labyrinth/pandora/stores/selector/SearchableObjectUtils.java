package tk.labyrinth.pandora.stores.selector;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

import javax.annotation.CheckForNull;

// TODO
public class SearchableObjectUtils {

	public static final String SEARCH_DATA_ATTRIBUTE_NAMES_ATTRIBUTE_NAME = "searchDataAttributeNames";

	@CheckForNull
	public static List<String> findSearchDataAttributeNames(GenericObject object) {
		ValueWrapper searchDataAttributeNames = object.findAttributeValue(SEARCH_DATA_ATTRIBUTE_NAMES_ATTRIBUTE_NAME);
		return searchDataAttributeNames != null ? searchDataAttributeNames.asList().unwrapAsListOfSimple() : null;
	}
}
