package tk.labyrinth.pandora.stores.rootobject;

import java.util.UUID;

public interface HasUid {

	String UID_ATTRIBUTE_NAME = "uid";

	UUID getUid();
}
