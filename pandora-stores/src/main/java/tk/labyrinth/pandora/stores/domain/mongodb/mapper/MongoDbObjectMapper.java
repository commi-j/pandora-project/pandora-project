package tk.labyrinth.pandora.stores.domain.mongodb.mapper;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

public interface MongoDbObjectMapper {

	GenericObject mapOnRead(GenericObject object);

	GenericObject mapOnWrite(GenericObject object);

	Predicate mapPredicate(Predicate predicate);
}
