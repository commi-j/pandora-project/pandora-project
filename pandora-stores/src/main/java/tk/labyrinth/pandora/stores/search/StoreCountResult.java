package tk.labyrinth.pandora.stores.search;

import lombok.Value;

@Value(staticConstructor = "of")
public class StoreCountResult {

	long count;

	String storeName;
}
