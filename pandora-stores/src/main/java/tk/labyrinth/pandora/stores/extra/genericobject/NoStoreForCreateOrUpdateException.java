package tk.labyrinth.pandora.stores.extra.genericobject;

import lombok.Value;
import tk.labyrinth.pandora.core.selection.SelectionResult;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;

@Value
public class NoStoreForCreateOrUpdateException extends RuntimeException {

	GenericObject object;

	GenericObject objectAfterInterception;

	SelectionResult<GenericObject, GenericObjectStore> selectionResult;

	@Override
	public String getMessage() {
		return "object = %s, objectAfterInterception = %s, selectionResult = %s"
				.formatted(object, objectAfterInterception, selectionResult);
	}
}
