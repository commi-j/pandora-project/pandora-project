package tk.labyrinth.pandora.stores.domain.rootobject;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeaturesContributor;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

import java.lang.reflect.Member;
import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class SignatureObjectModelAttributeFeatureContributor implements ObjectModelAttributeFeaturesContributor {

	@Override
	public List<ObjectModelAttributeFeature> contributeFeatures(Member javaMember) {
		String signatureAttributeName = RootObjectModelFeatureContributor.findSignatureAttributeName(
				javaMember.getDeclaringClass());
		//
		return signatureAttributeName != null &&
				Objects.equals(signatureAttributeName, ObjectModelUtils.getAttributeName(javaMember))
				? List.of(SignatureObjectModelAttributeFeature.instance())
				: List.empty();
	}
}
