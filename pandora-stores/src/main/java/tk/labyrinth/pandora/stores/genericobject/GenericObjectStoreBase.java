package tk.labyrinth.pandora.stores.genericobject;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReference;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storesummary.StoreSummary;
import tk.labyrinth.pandora.stores.listener.GenericObjectChangeListener;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;

@RequiredArgsConstructor
public abstract class GenericObjectStoreBase implements GenericObjectStore {

	@Getter
	private final StoreConfiguration configuration;

	@Setter // FIXME
	@SmartAutowired
	private List<GenericObjectChangeListener> genericObjectChangeListeners;

	@Autowired
	@Setter // FIXME
	private RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	@WithSpan
	private void listenChange(Runnable runnable) {
		runnable.run();
	}

	@WithSpan
	protected abstract void doCreate(RootObjectPrimaryReference primaryReference, GenericObject object);

	/**
	 * @param primaryReference non-null
	 * @param object           non-null
	 *
	 * @return previousObject, nullable
	 */
	@Nullable
	protected abstract GenericObject doCreateOrUpdate(RootObjectPrimaryReference primaryReference, GenericObject object);

	protected abstract GenericObject doDelete(RootObjectPrimaryReference primaryReference, GenericObject object);

	protected abstract StoreSummary doGetSummary();

	/**
	 * @param primaryReference non-null
	 * @param object           non-null
	 *
	 * @return previousObject, non-null
	 */
	protected abstract GenericObject doUpdate(RootObjectPrimaryReference primaryReference, GenericObject object);

	@Override
	public void create(GenericObject object) {
		RootObjectPrimaryReference primaryReference = rootObjectPrimaryReferenceHandler.getPrimaryReference(object);
		//
		if (primaryReference == null) {
			throw new IllegalArgumentException("Require non-null primaryReference: object = %s".formatted(object));
		}
		//
		doCreate(primaryReference, object);
		//
		listenChange(() -> genericObjectChangeListeners.forEach(genericObjectChangeListener ->
				genericObjectChangeListener.onGenericObjectChange(ObjectChangeEvent.<GenericObject>builder()
						.nextObject(object)
						.previousObject(null)
						.primaryReference(primaryReference.toGenericObjectReference())
						.build())));
	}

	@Override
	@WithSpan
	public void createOrUpdate(GenericObject object) {
		RootObjectPrimaryReference primaryReference = rootObjectPrimaryReferenceHandler.getPrimaryReference(object);
		//
		GenericObject previousObject = doCreateOrUpdate(primaryReference, object);
		//
		listenChange(() -> genericObjectChangeListeners.forEach(genericObjectChangeListener ->
				genericObjectChangeListener.onGenericObjectChange(ObjectChangeEvent.<GenericObject>builder()
						.nextObject(object)
						.previousObject(previousObject)
						.primaryReference(primaryReference.toGenericObjectReference())
						.build())));
	}

	@Override
	public void delete(GenericObject object) {
		RootObjectPrimaryReference primaryReference = rootObjectPrimaryReferenceHandler.getPrimaryReference(object);
		//
		GenericObject deletedObject = doDelete(primaryReference, object);
		//
		listenChange(() -> genericObjectChangeListeners.forEach(genericObjectChangeListener ->
				genericObjectChangeListener.onGenericObjectChange(ObjectChangeEvent.<GenericObject>builder()
						.nextObject(null)
						.previousObject(deletedObject)
						.primaryReference(primaryReference.toGenericObjectReference())
						.build())));
	}

	@Override
	public void delete(PlainQuery query) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public StoreSummary getSummary() {
		return doGetSummary();
	}

	@Override
	public String toString() {
		return "%s(%s)".formatted(getClass().getName(), configuration.getSlug());
	}

	@Override
	@WithSpan
	public void update(GenericObject object) {
		RootObjectPrimaryReference primaryReference = rootObjectPrimaryReferenceHandler.getPrimaryReference(object);
		//
		GenericObject previousObject = doUpdate(primaryReference, object);
		//
		listenChange(() -> genericObjectChangeListeners.forEach(genericObjectChangeListener ->
				genericObjectChangeListener.onGenericObjectChange(ObjectChangeEvent.<GenericObject>builder()
						.nextObject(object)
						.previousObject(previousObject)
						.primaryReference(primaryReference.toGenericObjectReference())
						.build())));
	}
}
