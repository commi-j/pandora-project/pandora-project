package tk.labyrinth.pandora.stores.extra.predicate;

import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.expresso.lang.operator.JunctionOperator;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;

@Value(staticConstructor = "of")
public class MintermCanonicalForm {

	List<Minterm> minterms;

	public MintermCanonicalForm mergeWith(MintermCanonicalForm other, JunctionOperator operator) {
		MintermCanonicalForm result;
		{
			result = switch (operator) {
				case AND -> MintermCanonicalForm.of(minterms.crossProduct(other.minterms)
						.map(tuple -> tuple._1().mergeWith(tuple._2()))
						.toList());
				case OR -> MintermCanonicalForm.of(minterms.appendAll(other.minterms));
			};
		}
		return result;
	}

	public Predicate toPredicate() {
		return Predicates.or(minterms.map(Minterm::toPredicate).toJavaStream());
	}
}
