package tk.labyrinth.pandora.stores.domain.storeroute;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel(code = StoreRoute.MODEL_CODE, primaryAttribute = StoreRoute.SLUG_ATTRIBUTE_NAME)
@ModelTagPandora
@Value
@With
public class StoreRoute {

	public static final String MODEL_CODE = "storeroute";

	public static final String SLUG_ATTRIBUTE_NAME = "slug";

	/**
	 * Distance to determine rule to apply, lowest wins.
	 */
	Integer distance;

	String slug;

	/**
	 * Predicate to use to find store.
	 */
	Predicate storeConfigurationPredicate;

	/**
	 * Predicate to test against.
	 */
	@Nullable
	Predicate testPredicate;
}
