package tk.labyrinth.pandora.stores.init.customdatatype;

import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;

// FIXME: REVIEW_LOCATION
public class PandoraDatatypeConstants {

	public static final String LIST_DATATYPE_NAME = "LIST";

	public static final String OBJECT_DATATYPE_NAME = "OBJECT";

	public static final AliasDatatypeBaseReference OBJECT_DATATYPE_REFERENCE = AliasDatatypeBaseReference.of(
			OBJECT_DATATYPE_NAME);

	public static final String REFERENCE_DATATYPE_NAME = "REFERENCE";

	public static final String SIMPLE_DATATYPE_NAME = "SIMPLE";

	public static final String VALUE_DATATYPE_NAME = "VALUE";
}
