package tk.labyrinth.pandora.stores.init;

import io.vavr.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.tool.init.GenericObjectContributorBase;

@PandoraExtensionPoint
public abstract class GenericObjectsContributingInitializer extends GenericObjectContributorBase implements
		PandoraInitializer {

	@Autowired
	private GenericObjectManipulator genericObjectManipulator;

	protected abstract List<GenericObject> contributeObjects();

	@Override
	public void run() {
		List<GenericObject> objects = contributeObjects();
		//
		try {
			objects.forEach(genericObjectManipulator::create);
		} catch (RuntimeException ex) {
			// This catch is to be able to set a breakpoint and study specific contributor.
			//
			throw new RuntimeException(ex);
		}
	}
}
