package tk.labyrinth.pandora.stores.extra.filterer;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.domain.java.JavaQueryExecutor;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.datatypes.filterer.SpecificFilterer;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class SpecificFiltererBeanContributor extends UnaryParameterizedBeanContributor<SpecificFilterer<?>> {

	private final JavaQueryExecutor queryExecutor;

	@Nullable
	@Override
	protected Integer doGetSupportDistance(Class<? extends SpecificFilterer<?>> base, Type parameter) {
		return TypeUtils.findClass(parameter) != null
				? Integer.MAX_VALUE
				: null;
	}

	@Nullable
	@Override
	protected Object doContributeBean(
			BeanFactory beanFactory,
			Class<? extends SpecificFilterer<?>> base,
			Type parameter) {
		return new SpecificFilterer<>(TypeUtils.getClass(parameter), queryExecutor);
	}
}
