package tk.labyrinth.pandora.stores.objectmodel;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Bean
@RequiredArgsConstructor
public class ObjectModelRegistry {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final ConcurrentMap<CodeObjectModelReference, ObjectModel> objectModels = new ConcurrentHashMap<>();

	@Nullable
	public ObjectModel findObjectModel(Datatype datatype) {
		ObjectModel result;
		{
			CodeObjectModelReference objectModelReference = findObjectModelReference(datatype);
			//
			result = objectModelReference != null ? getObjectModel(objectModelReference) : null;
		}
		return result;
	}

	@Nullable
	public String findObjectModelCode(Datatype datatype) {
		String result;
		{
			DatatypeBase datatypeBase = datatypeBaseRegistry.findDatatypeBase(datatype.getBaseReference());
			//
			result = datatypeBase != null ? ObjectModelUtils.findObjectModelCode(datatypeBase) : null;
		}
		return result;
	}

	@Nullable
	public String findObjectModelDatatypeAlias(Datatype datatype) {
		String result;
		{
			String objectModelCode = findObjectModelCode(datatype);
			//
			result = objectModelCode != null ? ObjectModelUtils.createDatatypeAlias(objectModelCode) : null;
		}
		return result;
	}

	@Nullable
	public AliasDatatypeBaseReference findObjectModelDatatypeBaseReference(Datatype datatype) {
		AliasDatatypeBaseReference result;
		{
			String objectModelDatatypeAlias = findObjectModelDatatypeAlias(datatype);
			//
			result = objectModelDatatypeAlias != null ? AliasDatatypeBaseReference.of(objectModelDatatypeAlias) : null;
		}
		return result;
	}

	@Nullable
	public ObjectModel findObjectModelForDatatype(Datatype datatype) {
		ObjectModel result;
		{
			DatatypeBase datatypeBase = datatypeBaseRegistry.findDatatypeBase(datatype.getBaseReference());
			//
			result = datatypeBase != null ? findObjectModelForDatatypeBase(datatypeBase) : null;
		}
		return result;
	}

	@Nullable
	public ObjectModel findObjectModelForDatatypeBase(DatatypeBase datatypeBase) {
		ObjectModel result;
		{
			CodeObjectModelReference objectModelReference = ObjectModelUtils.findObjectModelReference(
					datatypeBase.getAliases());
			//
			result = objectModelReference != null ? objectModels.get(objectModelReference) : null;
		}
		return result;
	}

	@Nullable
	public CodeObjectModelReference findObjectModelReference(Datatype datatype) {
		CodeObjectModelReference result;
		{
			DatatypeBase datatypeBase = datatypeBaseRegistry.findDatatypeBase(datatype.getBaseReference());
			//
			result = datatypeBase != null ? ObjectModelUtils.findObjectModelReference(datatypeBase) : null;
		}
		return result;
	}

	public ObjectModel getObjectModel(CodeObjectModelReference objectModelReference) {
		return Objects.requireNonNull(
				objectModels.get(objectModelReference),
				"objectModelReference = %s".formatted(objectModelReference));
	}

	public ObjectModel getObjectModel(Datatype datatype) {
		ObjectModel result = findObjectModel(datatype);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: datatype = %s".formatted(datatype));
		}
		//
		return result;
	}

	public Datatype getObjectModelDatatype(Datatype datatype) {
		Datatype result;
		{
			if (ObjectModelUtils.isObjectModelDatatypeBaseReference(datatype.getBaseReference())) {
				result = datatype;
			} else {
				result = datatype.withBaseReference(getObjectModelDatatypeBaseReference(datatype));
			}
		}
		return result;
	}

	public AliasDatatypeBaseReference getObjectModelDatatypeBaseReference(Datatype datatype) {
		AliasDatatypeBaseReference result = findObjectModelDatatypeBaseReference(datatype);
		//
		if (result == null) {
			throw new IllegalArgumentException("Required: %s".formatted(datatype));
		}
		//
		return result;
	}

	public CodeObjectModelReference getObjectModelReference(Datatype datatype) {
		CodeObjectModelReference result = findObjectModelReference(datatype);
		//
		if (result == null) {
			throw new IllegalArgumentException("Required: %s".formatted(datatype));
		}
		//
		return result;
	}

	@Nullable
	public ObjectModelAttribute lookupAttribute(ObjectModel objectModel, String attributePath) {
		ObjectModelAttribute result;
		{
			Pair<String, String> attributeAndRemainingPath = StringUtils.splitByFirstOccurrence(attributePath, "\\.");
			//
			ObjectModelAttribute attribute = objectModel.findAttribute(attributeAndRemainingPath.getLeft());
			//
			if (attributeAndRemainingPath.getRight() != null) {
				if (attribute != null) {
					ObjectModel nextObjectModel = findObjectModelForDatatype(attribute.getDatatype());
					//
					if (nextObjectModel != null) {
						result = lookupAttribute(nextObjectModel, attributeAndRemainingPath.getRight());
					} else {
						result = null;
					}
				} else {
					result = null;
				}
				//
			} else {
				result = attribute;
			}
		}
		return result;
	}

	public void registerObjectModel(ObjectModel objectModel) {
		objectModels.put(CodeObjectModelReference.from(objectModel), objectModel);
	}

	public void unregisterObjectModel(CodeObjectModelReference objectModelReference) {
		objectModels.remove(objectModelReference);
	}
}
