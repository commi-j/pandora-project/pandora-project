package tk.labyrinth.pandora.stores.objectmanipulator;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.util.function.UnaryOperator;

@LazyComponent
@RequiredArgsConstructor
public class WildcardObjectManipulator {

	private final ConverterRegistry converterRegistry;

	private final GenericObjectManipulator genericObjectManipulator;

	public void createOrUpdate(Object objectWithUid) {
		genericObjectManipulator.createOrUpdate(converterRegistry.convert(objectWithUid, GenericObject.class));
	}

	public void createOrUpdate(Object objectWithoutUid, Object valueForUidComputation) {
		genericObjectManipulator.createOrUpdate(converterRegistry.convert(objectWithoutUid, GenericObject.class)
				.withAddedAttribute(RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation)));
	}

	public void createOrUpdate(
			Object objectWithoutUid,
			Object valueForUidComputation,
			UnaryOperator<GenericObject> enrichOperator) {
		GenericObject genericObject = converterRegistry.convert(objectWithoutUid, GenericObject.class);
		//
		GenericObject genericObjectWithUid = genericObject.withAddedAttribute(
				RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation));
		//
		GenericObject enrichedGenericObject = enrichOperator.apply(genericObjectWithUid);
		//
		genericObjectManipulator.createOrUpdate(enrichedGenericObject);
	}

	public void createWithComputableUid(
			Object objectWithoutUid,
			Object valueForUidComputation,
			UnaryOperator<GenericObject> enrichOperator) {
		GenericObject genericObject = converterRegistry.convert(objectWithoutUid, GenericObject.class);
		//
		GenericObject genericObjectWithUid = genericObject.withAddedAttribute(
				RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation));
		//
		GenericObject enrichedGenericObject = enrichOperator.apply(genericObjectWithUid);
		//
		genericObjectManipulator.create(enrichedGenericObject);
	}

	public void createWithComputableUid(Object objectWithoutUid, Object valueForUidComputation) {
		genericObjectManipulator.create(converterRegistry.convert(objectWithoutUid, GenericObject.class)
				.withAddedAttribute(RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation)));
	}

	public void createWithGeneratedUid(Object objectWithoutUid) {
		genericObjectManipulator.create(converterRegistry.convert(objectWithoutUid, GenericObject.class)
				.withAddedAttribute(RootObjectUtils.createNewUidAttribute()));
	}

	public void createWithProvidedUid(Object objectWithUid) {
		genericObjectManipulator.create(converterRegistry.convert(objectWithUid, GenericObject.class));
	}

	public void createWithProvidedUid(Object objectWithUid, UnaryOperator<GenericObject> enrichOperator) {
		GenericObject genericObject = converterRegistry.convert(objectWithUid, GenericObject.class);
		//
		GenericObject enrichedGenericObject = enrichOperator.apply(genericObject);
		//
		genericObjectManipulator.create(enrichedGenericObject);
	}

	public void update(Object objectWithUid) {
		genericObjectManipulator.update(converterRegistry.convert(objectWithUid, GenericObject.class));
	}

	public void update(Object objectWithoutUid, Object valueForUidComputation) {
		genericObjectManipulator.update(converterRegistry.convert(objectWithoutUid, GenericObject.class)
				.withAddedAttribute(RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation)));
	}
}
