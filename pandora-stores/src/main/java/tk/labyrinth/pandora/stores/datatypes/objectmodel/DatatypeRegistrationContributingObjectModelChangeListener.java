package tk.labyrinth.pandora.stores.datatypes.objectmodel;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.AliasDatatypeRegistrationReference;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.DatatypeRegistration;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

@LazyComponent
@RequiredArgsConstructor
public class DatatypeRegistrationContributingObjectModelChangeListener extends
		TypedObjectChangeListener<ObjectModel, CodeObjectModelReference> {

	@SmartAutowired
	private TypedObjectManipulator<DatatypeRegistration> datatypeRegistrationManipulator;

	@Override
	protected void onObjectChange(TypedObjectChangeEvent<ObjectModel, CodeObjectModelReference> event) {
		String code = event.getPrimaryReference().getCode();
		String alias = ObjectModelUtils.createDatatypeAlias(code);
		//
		if (event.isCreateOrUpdate()) {
			ObjectModel objectModel = event.getNextObjectOrFail();
			//
			datatypeRegistrationManipulator.createOrUpdateWithAdjustment(
					DatatypeRegistration.builder()
							.alias(alias)
							.features(List.empty())
							.name(objectModel.getName())
							.parameters(null) // TODO
							.parentReference(PandoraDatatypeConstants.OBJECT_DATATYPE_REFERENCE)
							.signature(code)
							.build(),
					genericObject -> genericObject.withAddedAttribute(PandoraIndexConstants.ATTRIBUTE));
		} else {
			datatypeRegistrationManipulator.delete(AliasDatatypeRegistrationReference.of(alias));
		}
	}
}
