package tk.labyrinth.pandora.stores.extra.credentials;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@RenderAttribute(Credentials.KEY_ATTRIBUTE_NAME)
@Value(staticConstructor = "of")
public class KeyCredentialsReference implements Reference<Credentials> {

	String key;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Credentials.MODEL_CODE,
				Credentials.KEY_ATTRIBUTE_NAME,
				key);
	}

	public static KeyCredentialsReference from(Credentials object) {
		return KeyCredentialsReference.of(object.getKey());
	}

	@JsonCreator
	public static KeyCredentialsReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Credentials.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(reference.getAttributeValue(Credentials.KEY_ATTRIBUTE_NAME));
	}
}
