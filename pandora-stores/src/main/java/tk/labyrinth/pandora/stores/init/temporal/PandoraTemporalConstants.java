package tk.labyrinth.pandora.stores.init.temporal;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.core.PandoraConstants;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;

/**
 * @see PandoraConstants#TEMPORAL_MODEL_TAG_NAME
 */
@Deprecated // Replace with Index.
public class PandoraTemporalConstants {

	public static final String ATTRIBUTE_NAME = "temporal";

	public static final GenericObjectAttribute ATTRIBUTE = GenericObjectAttribute.ofTrue(
			ATTRIBUTE_NAME);

	public static final Predicate PREDICATE = Predicates.equalTo(ATTRIBUTE_NAME, true);
}
