package tk.labyrinth.pandora.stores.extra.referencemodel;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.CustomReferenceModelFactory;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ModelCodeAndAttributeNamesReferenceModelReference;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ReferenceModel;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// TODO: This should not create entries but just store those registered by contributors.
@Bean
@RequiredArgsConstructor
public class ReferenceModelRegistry {

	private final java.util.List<CustomReferenceModelFactory> customReferenceModelFactories;

	// TODO
	private final ConcurrentMap<Class<?>, ReferenceModel> entries = new ConcurrentHashMap<>();

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ReflectionObjectModelFactory reflectionObjectModelFactory;

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	/**
	 * The result predicate filters objects that are eligible to be referred to with this ReferenceModel.<br>
	 * It ensures all attributes are non-nulls but does not check model.<br>
	 *
	 * @param referenceModelReference non-null
	 *
	 * @return non-null
	 */
	// TODO: Support AliasDatatypeBaseReference.
	public Predicate createAttributePredicateForReferenceModel(
			ModelCodeAndAttributeNamesReferenceModelReference referenceModelReference) {
		return Predicates.and(referenceModelReference.getAttributeNames().toJavaStream()
				.map(attributeName -> Predicates.notEqualTo(attributeName, null)));
	}

	public ReferenceModel createReferenceModel(Type referenceJavaType) {
		ReferenceModel result;
		{
			List<ReferenceModel> referenceModels = customReferenceModelFactories.stream()
					.map(factory -> factory.createCustomReferenceModel(referenceJavaType))
					.filter(Objects::nonNull)
					.collect(List.collector());
			//
			if (!referenceModels.isEmpty()) {
				result = referenceModels.single();
			} else {
				if (TypeUtils.getClass(referenceJavaType) == Reference.class) {
					Type rootObjectJavaType = ParameterUtils.getFirstActualParameter(referenceJavaType, Reference.class);
					//
					ObjectModel objectModel = javaBaseTypeRegistry.getObjectModel(
							TypeUtils.getClass(rootObjectJavaType));
					//
					String signatureAttributeName = rootObjectPrimaryReferenceHandler.getSignatureAttributeName(
							CodeObjectModelReference.from(objectModel));
					//
					result = ReferenceModel.builder()
							.attributeNames(List.of(signatureAttributeName))
							.modelCode(objectModel.getCode())
							.name("%sPrimaryReference".formatted(objectModel.getName()))
							.build();
				} else {
					ObjectModel objectModel = reflectionObjectModelFactory.createModelFromJavaClass(
							TypeUtils.getClass(referenceJavaType));
					//
					if (objectModel.getAttributes().isEmpty()) {
						throw new IllegalArgumentException("Require attributes to be non-empty");
					}
					//
					result = ReferenceModel.builder()
							.attributeNames(objectModel.getAttributes().map(ObjectModelAttribute::getName))
							.modelCode(resolveModelCode(referenceJavaType))
							.name(objectModel.getName())
							.build();
				}
			}
		}
		return result;
	}

	public ModelCodeAndAttributeNamesReferenceModelReference createReferenceModelReference(
			Class<? extends Reference<?>> referenceClass) {
		return ModelCodeAndAttributeNamesReferenceModelReference.from(createReferenceModel(referenceClass));
	}

	public ReferenceModel getReferenceModel(Datatype referenceDatatype) {
		Type referenceJavaType = javaBaseTypeRegistry.getJavaType(referenceDatatype);
		//
		return createReferenceModel(referenceJavaType);
	}

	public ReferenceModel getReferenceModel(Type referenceJavaType) {
		return createReferenceModel(referenceJavaType);
	}

	@SuppressWarnings("unchecked")
	public static String resolveModelCode(Type referenceType) {
		String result;
		{
			if (referenceType instanceof Class<?> javaClass) {
				if (Reference.class.isAssignableFrom(javaClass)) {
					result = ReflectionObjectModelFactory.resolveModelCode(TypeAware.getRawParameterClass(
							(Class<? extends TypeAware<?>>) javaClass));
				} else {
					throw new NotImplementedException();
				}
			} else if (referenceType instanceof ParameterizedType parameterizedType) {
				if (Reference.class.isAssignableFrom((Class<?>) parameterizedType.getRawType())) {
					result = ReflectionObjectModelFactory.resolveModelCode(
							(Class<?>) parameterizedType.getActualTypeArguments()[0]);
				} else {
					throw new NotImplementedException();
				}
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}
}
