package tk.labyrinth.pandora.stores.genericobject;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;

import java.util.UUID;

// FIXME: Applicable only to root objects, so we may want separate interface for that.
// NEXT_GEN
@Value(staticConstructor = "of")
public class GenericObjectUidReference implements Reference<GenericObject> {

	public static final String UID_ATTRIBUTE_NAME = "uid";

	String modelCode;

	UUID uid;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				modelCode,
				UID_ATTRIBUTE_NAME,
				uid);
	}

	public static GenericObjectUidReference from(GenericObject object) {
		return of(
				PandoraRootUtils.getModelReference(object).getCode(),
				UUID.fromString(object.getAttributeValueAsString(UID_ATTRIBUTE_NAME)));
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static GenericObjectUidReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		return of(reference.getModelCode(), UUID.fromString(reference.getAttributeValue(UID_ATTRIBUTE_NAME)));
	}
}
