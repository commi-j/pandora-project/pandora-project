package tk.labyrinth.pandora.stores.domain.storeconfiguration;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;

/**
 * <a href="https://en.wikipedia.org/wiki/Data_store">https://en.wikipedia.org/wiki/Data_store</a>
 */
@Builder(toBuilder = true)
@ModelTagPandora
@RootModel(code = StoreConfiguration.MODEL_CODE, primaryAttribute = StoreConfiguration.SLUG_ATTRIBUTE_NAME)
@Value
@With
public class StoreConfiguration {

	public static final String DESIGNATION_ATTRIBUTE_NAME = "designation";

	public static final String GIT_SETTINGS_ATTRIBUTE_NAME = "gitSettings";

	public static final String KIND_ATTRIBUTE_NAME = "kind";

	public static final String MODEL_CODE = "storeconfiguration";

	public static final String MONGO_DB_SETTINGS_ATTRIBUTE_NAME = "mongoDbSettings";

	public static final String PREDICATE_ATTRIBUTE_NAME = "predicate";

	public static final String SLUG_ATTRIBUTE_NAME = "slug";

	StoreDesignation designation;

	// FIXME: Unify settings.
	@Nullable
	GitStoreSettings gitSettings;

	StoreKind kind;

	// FIXME: Unify settings.
	@Nullable
	MongoDbStoreSettings mongoDbSettings;

	@Nullable
	Predicate predicate;

	String slug;
}
