package tk.labyrinth.pandora.stores.domain.storesummary;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class StoreSummary {

	List<StoreObjectModelSummary> objectModelSummaries;
}
