package tk.labyrinth.pandora.stores.search;

import io.vavr.collection.List;
import lombok.Value;

@Value(staticConstructor = "of")
public class MultistoreCountResult {

	List<StoreCountResult> storeResults;

	public long getCount() {
		return storeResults.toJavaStream().mapToLong(StoreCountResult::getCount).sum();
	}
}
