package tk.labyrinth.pandora.stores.domain.mongodb;

import io.vavr.collection.List;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;

public interface MongoDbDatabaseLocator {

	/**
	 * Returns pair of object stripped of contextual database-related information and databaseName.
	 *
	 * @param storeConfiguration non-null
	 * @param object             non-null
	 *
	 * @return non-null, with nullable right
	 */
	Pair<GenericObject, @Nullable String> locateDatabase(StoreConfiguration storeConfiguration, GenericObject object);

	/**
	 * Returns pair of predicate stripped of contextual database-related information and databaseNa
	 *
	 * @param storeConfiguration non-null
	 * @param predicate          non-null
	 *
	 * @return non-null, with nullable right
	 */
	DatabaseContext locateDatabase(StoreConfiguration storeConfiguration, @Nullable Predicate predicate);

	@Value(staticConstructor = "of")
	class DatabaseContext {

		@Nullable
		String databaseName;

		List<GenericObjectAttribute> implicitAttributes;

		@Nullable
		Predicate refinedPredicate;
	}
}
