package tk.labyrinth.pandora.stores.extra.genericobject;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.core.selection.SelectionResult;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRouteRegistry;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreRegistry;
import tk.labyrinth.pandora.stores.objectmanipulator.GenericObjectManipulatorInterceptor;
import tk.labyrinth.pandora.stores.objectmanipulator.ObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.function.UnaryOperator;

@LazyComponent
@RequiredArgsConstructor
public class GenericObjectManipulator implements
		ObjectManipulator<ParameterizedQuery<CodeObjectModelReference>, GenericObject> {

	private final GenericObjectFilterer genericObjectFilterer;

	private final GenericObjectSearcher genericObjectSearcher;

	private final GenericObjectStoreRegistry genericObjectStoreRegistry;

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	private final StoreRouteRegistry storeRouteRegistry;

	@SmartAutowired
	private List<GenericObjectManipulatorInterceptor> interceptors;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@SmartAutowired
	private TypedObjectSearcher<StoreConfiguration> storeConfigurationSearcher;

	@WithSpan
	private GenericObject composeObjectForStoreSelection(GenericObject object) {
		GenericObject result;
		{
			// There may be no model yet available during app startup.
			// FIXME: Investigate if this can cause some invalid behaviour.
			//
			ObjectModel objectModel = objectModelSearcher.findSingle(PandoraRootUtils.getModelReference(object));
			//
			result = objectModel != null
					? object.withAddedAttributes(objectModel.getTagsOrEmpty().map(tag ->
					GenericObjectAttribute.ofSimple(tag.getName(), "true")))
					: object;
		}
		return result;
	}

	//	@WithSpan
//	private Predicate composePredicateForGenericObject(GenericObject genericObject) {
//		//
//		return Predicates.flatAnd(Stream
//				.of(
//						composePredicateForObjectModelReference(RootObjectUtils.getModelReference(genericObject)),
//						PredicateUtils.nullableAttributeToPredicate(genericObject.findAttribute(
//								TemporalStoreConfigurationRootObjectsContributor.TEMPORAL_ATTRIBUTE_NAME)))
//				.filter(Objects::nonNull));
//	}
//	@WithSpan
//	private Predicate composePredicateForObjectModelReference(CodeObjectModelReference objectModelReference) {
//		return Predicates.flatAnd(Stream
//				.of(
//						executionContextAccessor.getExecutionContext().findInferredAttribute(
//								ExecutionContext.SEARCH_PREDICATE),
//						Predicates.equalTo(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME, objectModelReference),
//						resolveTagsPredicate(objectModelReference))
//				.filter(Objects::nonNull));
//	}
	@Deprecated
	@Nullable
	@WithSpan
	private GenericObjectStore findStoreForManipulation(GenericObject object) {
		GenericObjectStore result;
		{
			StoreRoute storeRoute = storeRouteRegistry.findStoreRoute(object);
			//
			if (storeRoute != null) {
				val storeSelectionResult = genericObjectStoreRegistry.selectStore(storeRoute.getStoreConfigurationPredicate());
				//
				result = storeSelectionResult.findResult();
			} else {
				result = null;
			}
		}
		return result;
	}

	@WithSpan
	private GenericObject intercept(GenericObject object, boolean create, boolean update) {
		return interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> {
					GenericObject result;
					{
						if (create) {
							if (update) {
								result = interceptor.onCreateOrUpdate(nextObject);
							} else {
								result = interceptor.onCreate(nextObject);
							}
						} else {
							if (update) {
								result = interceptor.onUpdate(nextObject);
							} else {
								throw new IllegalArgumentException();
							}
						}
					}
					return result;
				},
				FunctionUtils::throwUnreachableStateException);
	}

	// FIXME: This solution is a crutch, we need to be able to access model directly at evaluation and not alter predicate.
	private Predicate resolveTagsPredicate(CodeObjectModelReference objectModelReference) {
		Predicate result;
		{
			ObjectModel objectModel = objectModelSearcher.findSingle(objectModelReference);
			if (objectModel != null) {
				List<Predicate> predicates = objectModel.getTagsOrEmpty()
						.map(tag -> Predicates.equalTo(tag.getName(), true));
				result = !predicates.isEmpty() ? Predicates.and(predicates.toJavaStream()) : null;
			} else {
				result = null;
			}
		}
		return result;
	}

	public boolean canManipulate(GenericObject object) {
		GenericObject objectForStoreSelection = composeObjectForStoreSelection(object);
		//
		return selectStoreForManipulation(objectForStoreSelection).hasResult();
	}

	@Override
	@WithSpan
	public void create(GenericObject object) {
		GenericObject objectAfterInterception = intercept(object, true, false);
		//
		GenericObject objectForStoreSelection = composeObjectForStoreSelection(objectAfterInterception);
		//
		val selectionResult = selectStoreForManipulation(objectForStoreSelection);
		//
		GenericObjectStore storeForManipulation = selectionResult.findResult();
		//
		if (storeForManipulation == null) {
			throw new NoStoreForCreateOrUpdateException(object, objectAfterInterception, selectionResult);
		}
		//
		storeForManipulation.create(objectAfterInterception);
	}

	@Override
	@WithSpan
	public void createOrUpdate(GenericObject object) {
		GenericObject objectAfterInterception = intercept(object, true, true);
		//
		GenericObject objectForStoreSelection = composeObjectForStoreSelection(objectAfterInterception);
		//
		val selectionResult = selectStoreForManipulation(objectForStoreSelection);
		//
		GenericObjectStore storeForManipulation = selectionResult.findResult();
		//
		if (storeForManipulation == null) {
			throw new NoStoreForCreateOrUpdateException(object, objectAfterInterception, selectionResult);
		}
		//
		storeForManipulation.createOrUpdate(objectAfterInterception);
	}

	@Override
	@WithSpan
	public void delete(ParameterizedQuery<CodeObjectModelReference> query) {
		List<GenericObject> foundObjects = genericObjectSearcher.search(query);
		//
		//
		foundObjects.forEach(foundObject -> delete(
				rootObjectPrimaryReferenceHandler.getPrimaryReference(foundObject).toGenericObjectReference()));
	}

	@Override
	@WithSpan
	public void delete(Reference<GenericObject> reference) {
		GenericObject foundObject = genericObjectSearcher.findSingle(reference);
		//
		if (foundObject != null) {
			GenericObject objectForStoreSelection = composeObjectForStoreSelection(foundObject);
			//
			val selectionResult = selectStoreForManipulation(objectForStoreSelection);
			//
			GenericObjectStore storeForManipulation = selectionResult.findResult();
			//
			if (storeForManipulation == null) {
				throw new NoStoreForDeleteException(foundObject, reference, selectionResult);
			}
			//
			storeForManipulation.delete(foundObject);
		}
	}

	public SelectionResult<GenericObject, GenericObjectStore> selectStoreForManipulation(GenericObject object) {
		SelectionResult<GenericObject, GenericObjectStore> result;
		{
			val storeRouteSelectionResult = storeRouteRegistry.selectStoreRoute(object);
			//
			result = storeRouteSelectionResult.map(storeRoute ->
					genericObjectStoreRegistry.selectStore(storeRoute.getStoreConfigurationPredicate()).getEvaluatedResults());
		}
		return result;
	}

	@Override
	@WithSpan
	public void update(GenericObject object) {
		GenericObject objectAfterInterception = intercept(object, false, true);
		//
		GenericObject objectForStoreSelection = composeObjectForStoreSelection(objectAfterInterception);
		//
		val selectionResult = selectStoreForManipulation(objectForStoreSelection);
		//
		GenericObjectStore storeForManipulation = selectionResult.findResult();
		//
		if (storeForManipulation == null) {
			throw new NoStoreForCreateOrUpdateException(object, objectAfterInterception, selectionResult);
		}
		//
		storeForManipulation.update(objectAfterInterception);
	}

	@Override
	@WithSpan
	public GenericObject updateObjectWithOperator(
			Reference<GenericObject> reference,
			UnaryOperator<GenericObject> updateOperator) {
		GenericObject currentGenericObject = genericObjectSearcher.getSingle(reference);
		//
		GenericObject nextGenericObject = updateOperator.apply(currentGenericObject);
		//
		update(nextGenericObject);
		//
		return nextGenericObject;
	}
}
