package tk.labyrinth.pandora.stores.selector;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.SearchDataAttributes;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;

@Bean
@RequiredArgsConstructor
public class SelectorResolver {

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ObjectModelRegistry objectModelRegistry;

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	@Nullable
	public Predicate selectorToPredicate(Datatype qualifier, Selector selector) {
		Predicate result;
		{
			if (!selector.getSearchText().isEmpty()) {
				Class<?> javaClass = javaBaseTypeRegistry.getJavaClass(qualifier.getBaseReference());
				//
				MergedAnnotation<SearchDataAttributes> mergedAnnotation = MergedAnnotations.from(javaClass)
						.get(SearchDataAttributes.class);
				//
				if (mergedAnnotation.isPresent()) {
					List<String> value = List.of(mergedAnnotation.getStringArray("value"));
					result = Predicates.or(value
							.map(valueElement -> Predicates.contains(valueElement, selector.getSearchText(), false))
							.toJavaStream());
				} else {
					CodeObjectModelReference objectModelReference = objectModelRegistry.findObjectModelReference(qualifier);
					//
					if (objectModelReference != null) {
						String signatureAttributeName = rootObjectPrimaryReferenceHandler.findSignatureAttributeName(
								objectModelReference);
						//
						if (signatureAttributeName == null) {
							throw new IllegalArgumentException("Why?");
						}
						//
						result = Predicates.contains(signatureAttributeName, selector.getSearchText(), false);
					} else {
						// TODO: Investigate such cases.
						//
						result = null;
					}
				}
			} else {
				result = null;
			}
		}
		return result;
	}
}
