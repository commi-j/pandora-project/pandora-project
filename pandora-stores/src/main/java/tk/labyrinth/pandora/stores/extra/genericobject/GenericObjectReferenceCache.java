package tk.labyrinth.pandora.stores.extra.genericobject;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.reference.ReferenceTool;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.listener.GenericObjectChangeListener;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@LazyComponent
@RequiredArgsConstructor
public class GenericObjectReferenceCache implements GenericObjectChangeListener {

	private final ConcurrentMap<Predicate, Optional<GenericObject>> predicateToGenericObjectMap =
			new ConcurrentHashMap<>();

	private final ConcurrentMap<GenericObjectReference, Set<Predicate>> primaryReferenceToPredicatesMap =
			new ConcurrentHashMap<>();

	private final ReferenceTool referenceTool;

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	/**
	 * @param predicate non-null
	 *
	 * @return null means not found, empty means null is cached.
	 */
	@Nullable
	public Optional<GenericObject> find(Predicate predicate) {
		return predicateToGenericObjectMap.get(predicate);
	}

	@Override
	public void onGenericObjectChange(ObjectChangeEvent<GenericObject> event) {
		val primaryReference = event.getPrimaryReference();
		//
		{
			Predicate primaryPredicate = GenericObjectSearcherUtils.composeContextlessPredicate(
					null,
					referenceTool.referenceToParameterizedQuery(primaryReference));
			//
			// FIXME: This does not consider context attributes which may be added when search is invoked. Should be reworked.
			predicateToGenericObjectMap.remove(primaryPredicate);
		}
		{
			Set<Predicate> removedPredicates = primaryReferenceToPredicatesMap.remove(primaryReference);
			//
			if (removedPredicates != null) {
				removedPredicates.forEach(predicateToGenericObjectMap::remove);
			}
		}
	}

	public void put(Predicate predicate, @Nullable GenericObject object) {
		predicateToGenericObjectMap.put(predicate, Optional.ofNullable(object));
		//
		if (object != null) {
			primaryReferenceToPredicatesMap.compute(
					rootObjectPrimaryReferenceHandler.getPrimaryReference(object).toGenericObjectReference(),
					(key, value) -> value != null ? value.add(predicate) : HashSet.of(predicate));
		} else {
			// FIXME: We don't have a mechanism to invalidate mappings to null when corresponding object is created.
		}
	}
}
