package tk.labyrinth.pandora.stores.datatypes.datatyperegistration;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.DatatypeRegistration;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class DatatypeBaseMergingDatatypeRegistrationChangeListener extends ObjectChangeListener<DatatypeRegistration> {

	@SmartAutowired
	private TypedObjectManipulator<DatatypeBase> datatypeBaseManipulator;

	@SmartAutowired
	private TypedObjectSearcher<DatatypeRegistration> datatypeRegistrationSearcher;

	@Override
	protected void onObjectChange(ObjectChangeEvent<DatatypeRegistration> event) {
		String signature = event.extractSameAttributeValue(DatatypeRegistration::getSignature);
		//
		List<DatatypeRegistration> datatypeRegistrations = datatypeRegistrationSearcher.search(PlainQuery.builder()
				.predicate(Predicates.equalTo(DatatypeRegistration.SIGNATURE_ATTRIBUTE_NAME, signature))
				.build());
		//
		if (!datatypeRegistrations.isEmpty()) {
			datatypeBaseManipulator.createOrUpdate(DatatypeBase.builder()
					.aliases(datatypeRegistrations.map(DatatypeRegistration::getAlias))
					.features(datatypeRegistrations.flatMap(DatatypeRegistration::getFeatures))
					.name(datatypeRegistrations
							.map(DatatypeRegistration::getName)
							.filter(Objects::nonNull)
							.distinct()
							.orElse(List.of((String) null))
							.single())
					.parameters(datatypeRegistrations
							.map(DatatypeRegistration::getParameters)
							.filter(Objects::nonNull)
							.distinct()
							.orElse(List.of((List<String>) null))
							.single())
					.parentReference(datatypeRegistrations
							.map(DatatypeRegistration::getParentReference)
							.filter(Objects::nonNull)
							.distinct()
							.orElse(List.of((AliasDatatypeBaseReference) null))
							.single())
					.signature(signature)
					.build());
		} else {
			// TODO: Delete by signature reference.
//			datatypeBaseManipulator.delete(null);
		}
	}
}
