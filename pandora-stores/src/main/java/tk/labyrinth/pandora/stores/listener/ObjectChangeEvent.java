package tk.labyrinth.pandora.stores.listener;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;
import java.util.function.Function;

// TODO: Add primaryReference to it.
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ObjectChangeEvent<T> {

	public enum ChangeKind {
		CREATE,
		DELETE,
		MODIFY
	}

	@Nullable
	T nextObject;

	@Nullable
	T previousObject;

	@NonNull
	Reference<T> primaryReference;

	/**
	 * Returns a value from either previous or next objects. If both are present, the value is expected to be the same.
	 *
	 * @param extractFunction non-null
	 * @param <R>             Result
	 *
	 * @return nullable
	 */
	@Deprecated // TODO: This should be replaced by primaryReference, but we need to check it first.
	@Nullable
	public <R> R extractSameAttributeValue(Function<T, @Nullable R> extractFunction) {
		R result;
		{
			List<R> distinctValues = List.of(previousObject, nextObject)
					.filter(Objects::nonNull)
					.map(extractFunction)
					.distinct();
			//
			if (distinctValues.size() > 1) {
				throw new IllegalArgumentException("Require single value: %s".formatted(distinctValues));
			}
			//
			result = distinctValues.single();
		}
		return result;
	}

	public ChangeKind getChangeKind() {
		return previousObject != null
				? nextObject != null ? ChangeKind.MODIFY : ChangeKind.DELETE
				: ChangeKind.CREATE;
	}

	public T getNextObjectOrFail() {
		return Objects.requireNonNull(nextObject, () -> "nextObject, this = %s".formatted(this));
	}

	public T getPreviousObjectOrFail() {
		return Objects.requireNonNull(previousObject, () -> "previousObject, this = %s".formatted(this));
	}

	public boolean hasNextObject() {
		return nextObject != null;
	}

	public boolean hasPreviousObject() {
		return previousObject != null;
	}

	public boolean isCreate() {
		return getChangeKind() == ChangeKind.CREATE;
	}

	public boolean isCreateOrUpdate() {
		return getChangeKind() != ChangeKind.DELETE;
	}

	public boolean isDelete() {
		return getChangeKind() == ChangeKind.DELETE;
	}

	public boolean isDeleteOrUpdate() {
		return getChangeKind() != ChangeKind.CREATE;
	}
}
