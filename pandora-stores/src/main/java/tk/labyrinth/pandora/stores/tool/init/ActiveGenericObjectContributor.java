package tk.labyrinth.pandora.stores.tool.init;

import io.vavr.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;

@PandoraExtensionPoint
public abstract class ActiveGenericObjectContributor extends GenericObjectContributorBase {

	@Autowired
	private GenericObjectManipulator genericObjectManipulator;

	protected void contributeObjects(List<GenericObject> objects) {
		try {
			objects.forEach(genericObjectManipulator::create);
		} catch (RuntimeException ex) {
			// This catch is to be able to set a breakpoint and study specific contributor.
			//
			throw new RuntimeException(ex);
		}
	}
}
