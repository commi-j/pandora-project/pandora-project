package tk.labyrinth.pandora.stores.extra.genericobject;

import io.opentelemetry.instrumentation.annotations.SpanAttribute;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.Lazy;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextHandler;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.domain.reference.ReferenceTool;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootConstants;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreRegistry;
import tk.labyrinth.pandora.stores.objectcache.GenericObjectCache;
import tk.labyrinth.pandora.stores.objectcache.GenericObjectCacheRegistry;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcherBase;
import tk.labyrinth.pandora.stores.search.MultistoreCountResult;
import tk.labyrinth.pandora.stores.search.MultistoreObjectSearcher;
import tk.labyrinth.pandora.stores.search.StoreCountResult;

import java.util.Objects;
import java.util.Optional;

@Bean
@RequiredArgsConstructor
public class GenericObjectSearcher extends
		ObjectSearcherBase<ParameterizedQuery<CodeObjectModelReference>, GenericObject> implements
		MultistoreObjectSearcher<ParameterizedQuery<CodeObjectModelReference>, GenericObject> {

	public static final String SEARCH_PREDICATE_EXECUTION_CONTEXT_ATTRIBUTE_NAME = "SEARCH_PREDICATE";

	private final ExecutionContextHandler executionContextHandler;

	private final GenericObjectCacheRegistry genericObjectCacheRegistry;

	private final GenericObjectFilterer genericObjectFilterer;

	private final GenericObjectStoreRegistry genericObjectStoreRegistry;

	private final ReferenceTool referenceTool;

	@SmartAutowired
	private Lazy<GenericObjectReferenceCache> genericObjectReferenceCacheLazy;

	protected List<GenericObject> doResolve(ParameterizedQuery<CodeObjectModelReference> query) {
		Predicate contextPredicate = executionContextHandler.getExecutionContext()
				.findAttributeValueInferred(SEARCH_PREDICATE_EXECUTION_CONTEXT_ATTRIBUTE_NAME);
		//
		Predicate contextlessPredicate = GenericObjectSearcherUtils.composeContextlessPredicate(contextPredicate, query);
		//
		List<GenericObject> resolutionResult = doResolveContextless(contextlessPredicate);
		//
		return resolutionResult;
	}

	protected List<GenericObject> doResolveContextless(Predicate contextlessPredicate) {
		List<GenericObject> result;
		{
			Optional<GenericObject> cachedObject = genericObjectReferenceCacheLazy.get().find(contextlessPredicate);
			//
			//noinspection OptionalAssignedToNull
			if (cachedObject != null) {
				result = cachedObject.map(List::of).orElseGet(List::empty);
			} else {
				List<GenericObject> foundObjects = doSearchContextless(PlainQuery.builder()
						.limit(2L)
						.predicate(contextlessPredicate)
						.build());
				//
				if (foundObjects.size() <= 1) {
					genericObjectReferenceCacheLazy.get().put(
							contextlessPredicate,
							!foundObjects.isEmpty() ? foundObjects.single() : null);
				}
				result = foundObjects;
			}
		}
		return result;
	}

	/**
	 * This method exists so that you can put breakpoint to search method
	 * and get no interference from reference methods.
	 *
	 * @param query non-null
	 *
	 * @return non-null
	 */
	protected List<GenericObject> doSearch(ParameterizedQuery<CodeObjectModelReference> query) {
		Predicate contextPredicate = executionContextHandler.getExecutionContext()
				.findAttributeValueInferred(SEARCH_PREDICATE_EXECUTION_CONTEXT_ATTRIBUTE_NAME);
		Predicate objectModelCodePredicate = Predicates.equalTo(
				PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME,
				query.getParameter());
		//
		Predicate composedPredicate = Predicates.flatAnd(java.util.stream.Stream
				.of(contextPredicate, objectModelCodePredicate, query.getPredicate())
				.filter(Objects::nonNull));
		PlainQuery composedQuery = PlainQuery.builder()
				.limit(query.getLimit())
				.offset(query.getOffset())
				.predicate(composedPredicate)
				.sort(query.getSort())
				.build();
		//
		List<GenericObject> searchResult = doSearchContextless(composedQuery);
		//
		return searchResult;
	}

	@WithSpan
	protected List<GenericObject> doSearchContextless(@SpanAttribute PlainQuery contextlessQuery) {
		List<GenericObject> result;
		{
			{
				GenericObjectCache cache = genericObjectCacheRegistry.findCache(contextlessQuery);
				//
				if (cache != null) {
					result = cache.search(contextlessQuery);
				} else {
					List<GenericObject> resultBeforeFiltering;
					//
					List<GenericObjectStore> stores = genericObjectStoreRegistry.getMatchingStores(
							contextlessQuery.getPredicate());
					//
					// TODO: Filter with StoreSummary?
					//
					val storesAndFoundObjects = stores.map(store ->
							Pair.of(store, store.search(contextlessQuery)));
					//
					resultBeforeFiltering = storesAndFoundObjects.flatMap(Pair::getRight);
					//
					// FIXME: For multistore cases we can not provide accurate results if offset is > 0.
					result = genericObjectFilterer.filter(
							resultBeforeFiltering,
							// Dropping offset as it was already applied in stores.
							contextlessQuery.withOffset(null));
				}
			}
		}
		return result;
	}

	@Override
	@WithSpan
	public long count(ParameterizedQuery<CodeObjectModelReference> query) {
		MultistoreCountResult countResult = countMultistore(query);
		return countResult.getCount();
	}

	@Override
	@WithSpan
	public MultistoreCountResult countMultistore(ParameterizedQuery<CodeObjectModelReference> query) {
		Predicate contextPredicate = executionContextHandler.getExecutionContext()
				.findAttributeValueInferred(SEARCH_PREDICATE_EXECUTION_CONTEXT_ATTRIBUTE_NAME);
		Predicate objectModelCodePredicate = Predicates.equalTo(
				PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME,
				query.getParameter());
		//
		Predicate composedPredicate = Predicates.flatAnd(java.util.stream.Stream
				.of(contextPredicate, objectModelCodePredicate, query.getPredicate())
				.filter(Objects::nonNull));
		PlainQuery composedQuery = PlainQuery.builder()
				.limit(query.getLimit())
				.offset(query.getOffset())
				.predicate(composedPredicate)
				.sort(query.getSort())
				.build();
		//
		List<GenericObjectStore> objectStores = genericObjectStoreRegistry.getMatchingStores(composedPredicate);
		List<StoreCountResult> storeCountResults = objectStores.map(objectStore -> StoreCountResult.of(
				objectStore.count(composedQuery),
				objectStore.getConfiguration().getSlug()));
		//
		return MultistoreCountResult.of(storeCountResults);
	}

	@Override
	@WithSpan
	public <R extends Reference<GenericObject>> ResolvedReference<GenericObject, R> resolve(@SpanAttribute R reference) {
		ResolvedReference<GenericObject, R> result;
		{
			List<GenericObject> objects = doResolve(referenceTool.referenceToParameterizedQuery(reference));
			//
			result = ResolvedReference.of(reference, objects);
		}
		return result;
	}

	@WithSpan
	public List<GenericObject> search(CodeObjectModelReference objectModelReference, Predicate predicate) {
		return search(ParameterizedQuery.<CodeObjectModelReference>builder()
				.parameter(objectModelReference)
				.predicate(predicate)
				.build());
	}

	@Override
	@WithSpan
	public List<GenericObject> search(ParameterizedQuery<CodeObjectModelReference> query) {
		return doSearch(query);
	}
}
