package tk.labyrinth.pandora.stores.domain.mongodb;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;

import java.util.Objects;

public class PandoraMongoDbUtils {

	public static final String COLLECTION_NAME_TAG_NAME = "pandoraMongoDbCollectionName";

	public static Tag createCollectionNameTag(String collectionName) {
		return Tag.of(COLLECTION_NAME_TAG_NAME, collectionName);
	}

	@Nullable
	public static String findCollectionName(List<Tag> tags) {
		return tags
				.filter(tag -> Objects.equals(tag.getName(), COLLECTION_NAME_TAG_NAME))
				.map(Tag::getValue)
				.getOrNull();
	}
}
