package tk.labyrinth.pandora.stores.datatypes.datatypebase;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.SignatureDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class DatatypeBaseRegistry {

	@SmartAutowired
	private TypedObjectSearcher<DatatypeBase> datatypeBaseSearcher;

	@Nullable
	public DatatypeKind determineKind(@Nullable Datatype datatype) {
		return datatype != null ? determineKind(datatype.getBaseReference()) : null;
	}

	@Nullable
	public DatatypeKind determineKind(@NonNull Reference<DatatypeBase> datatypeBaseReference) {
		DatatypeKind result;
		{
			DatatypeKind kindOfInput;
			//
			if (AliasDatatypeBaseReference.isAliasDatatypeBaseReference(datatypeBaseReference)) {
				kindOfInput = switch (AliasDatatypeBaseReference.getAlias(datatypeBaseReference)) {
					case PandoraDatatypeConstants.LIST_DATATYPE_NAME -> DatatypeKind.LIST;
					case PandoraDatatypeConstants.OBJECT_DATATYPE_NAME -> DatatypeKind.OBJECT;
					case PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME -> DatatypeKind.SIMPLE;
					default -> null;
				};
			} else if (SignatureDatatypeBaseReference.isSignatureDatatypeBaseReference(datatypeBaseReference)) {
				kindOfInput = switch (SignatureDatatypeBaseReference.getSignature(datatypeBaseReference)) {
					case PandoraDatatypeConstants.LIST_DATATYPE_NAME -> DatatypeKind.LIST;
					case PandoraDatatypeConstants.OBJECT_DATATYPE_NAME -> DatatypeKind.OBJECT;
					case PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME -> DatatypeKind.SIMPLE;
					default -> null;
				};
			} else {
				kindOfInput = null;
			}
			//
			if (kindOfInput != null) {
				result = kindOfInput;
			} else {
				DatatypeBase datatypeBase = findDatatypeBase(datatypeBaseReference);
				//
				if (datatypeBase != null) {
					AliasDatatypeBaseReference parentReference = datatypeBase.getParentReference();
					//
					if (parentReference != null) {
						result = determineKind(parentReference);
					} else {
						result = null;
					}
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	@Nullable
	public DatatypeBase findDatatypeBase(@NonNull Reference<DatatypeBase> datatypeBaseReference) {
		return datatypeBaseSearcher.findSingle(datatypeBaseReference);
	}

	public DatatypeBase getSingle(Reference<DatatypeBase> datatypeBaseReference) {
		return datatypeBaseSearcher.getSingle(datatypeBaseReference);
	}

	public boolean isAssignableTo(Reference<DatatypeBase> potentialChildReference, DatatypeBase potentialAncestor) {
		boolean result;
		{
			DatatypeBase childDatatypeBase = findDatatypeBase(potentialChildReference);
			//
			if (childDatatypeBase != null) {
				if (Objects.equals(childDatatypeBase, potentialAncestor)) {
					// Matches.
					//
					result = true;
				} else {
					AliasDatatypeBaseReference parentReference = childDatatypeBase.getParentReference();
					//
					if (parentReference != null) {
						// Check parent.
						//
						result = isAssignableTo(parentReference, potentialAncestor);
					} else {
						// No parent left to check.
						//
						result = false;
					}
				}
			} else {
				// Child not found.
				//
				result = false;
			}
		}
		return result;
	}

	public boolean isAssignableTo(
			Reference<DatatypeBase> potentialChildReference,
			Reference<DatatypeBase> potentialAncestorReference) {
		boolean result;
		{
			DatatypeBase ancestorDatatypeBase = findDatatypeBase(potentialAncestorReference);
			//
			if (ancestorDatatypeBase != null) {
				result = isAssignableTo(potentialChildReference, ancestorDatatypeBase);
			} else {
				// Ancestor not found.
				//
				result = false;
			}
		}
		return result;
	}

	public boolean isReference(@NonNull Reference<DatatypeBase> datatypeBaseReference) {
		boolean result;
		{
			DatatypeBase datatypeBase = findDatatypeBase(datatypeBaseReference);
			//
			if (datatypeBase != null) {
				AliasDatatypeBaseReference parentReference = datatypeBase.getParentReference();
				//
				result = parentReference != null &&
						Objects.equals(parentReference.getAlias(), PandoraDatatypeConstants.REFERENCE_DATATYPE_NAME);
			} else {
				result = false;
			}
		}
		return result;
	}

	public boolean isReference(@Nullable Datatype datatype) {
		return datatype != null && isReference(datatype.getBaseReference());
	}

	public boolean matches(@Nullable Datatype datatype, @NonNull Class<?> javaClass) {
		boolean result;
		{
			if (datatype != null) {
				DatatypeBase firstDatatypeBase = datatypeBaseSearcher.findSingle(datatype.getBaseReference());
				DatatypeBase secondDatatypeBase = datatypeBaseSearcher.findSingle(
						JavaBaseTypeUtils.createDatatypeBaseReference(javaClass));
				//
				result = firstDatatypeBase != null && Objects.equals(firstDatatypeBase, secondDatatypeBase);
			} else {
				result = false;
			}
		}
		return result;
	}

	/**
	 * @param datatype              nullable
	 * @param datatypeBaseReference non-null
	 *
	 * @return whether datatype.baseReference refer to the same entry as datatypeBaseReference
	 */
	public boolean matches(
			@Nullable Datatype datatype,
			@NonNull Reference<DatatypeBase> datatypeBaseReference) {
		boolean result;
		{
			if (datatype != null) {
				DatatypeBase firstDatatypeBase = datatypeBaseSearcher.findSingle(datatype.getBaseReference());
				DatatypeBase secondDatatypeBase = datatypeBaseSearcher.findSingle(datatypeBaseReference);
				//
				result = firstDatatypeBase != null && Objects.equals(firstDatatypeBase, secondDatatypeBase);
			} else {
				result = false;
			}
		}
		return result;
	}
}
