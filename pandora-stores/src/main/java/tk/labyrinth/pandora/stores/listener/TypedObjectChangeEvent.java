package tk.labyrinth.pandora.stores.listener;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

// TODO: Add primaryReference to it.
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class TypedObjectChangeEvent<T, R extends Reference<T>> {

	public enum ChangeKind {
		CREATE,
		DELETE,
		MODIFY
	}

	@Nullable
	T nextObject;

	@Nullable
	T previousObject;

	@NonNull
	R primaryReference;

	public ChangeKind getChangeKind() {
		return previousObject != null
				? nextObject != null ? ChangeKind.MODIFY : ChangeKind.DELETE
				: ChangeKind.CREATE;
	}

	public T getNextObjectOrFail() {
		return Objects.requireNonNull(nextObject, () -> "nextObject, this = %s".formatted(this));
	}

	public T getPreviousObjectOrFail() {
		return Objects.requireNonNull(previousObject, () -> "previousObject, this = %s".formatted(this));
	}

	public boolean hasNextObject() {
		return nextObject != null;
	}

	public boolean hasPreviousObject() {
		return previousObject != null;
	}

	public boolean isCreate() {
		return getChangeKind() == ChangeKind.CREATE;
	}

	public boolean isCreateOrUpdate() {
		return getChangeKind() != ChangeKind.DELETE;
	}

	public boolean isDelete() {
		return getChangeKind() == ChangeKind.DELETE;
	}

	public boolean isDeleteOrUpdate() {
		return getChangeKind() != ChangeKind.CREATE;
	}
}
