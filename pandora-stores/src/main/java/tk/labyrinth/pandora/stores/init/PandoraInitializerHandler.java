package tk.labyrinth.pandora.stores.init;

import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.SmartLifecycle;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@Slf4j
public class PandoraInitializerHandler implements SmartLifecycle {

	@SmartAutowired
	private List<PandoraInitializer> initializers;

	@Override
	public int getPhase() {
		return Integer.MIN_VALUE + 1000;
	}

	@Override
	public boolean isRunning() {
		return false;
	}

	@Override
	public void start() {
		initializers.forEach(initializer -> {
			try {
				initializer.run();
			} catch (RuntimeException ex) {
				// This catch is to be able to set a breakpoint and study initializer list.
				//
				throw new RuntimeException(ex);
			}
		});
		//
		logger.info("Initialized");
	}

	@Override
	public void stop() {
		// no-op
	}
}
