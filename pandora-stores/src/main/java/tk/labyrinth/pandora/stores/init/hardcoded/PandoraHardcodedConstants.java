package tk.labyrinth.pandora.stores.init.hardcoded;

import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;

import java.util.function.UnaryOperator;

public class PandoraHardcodedConstants {

	public static final String ATTRIBUTE_NAME = "hardcoded";

	public static final GenericObjectAttribute ATTRIBUTE = GenericObjectAttribute.ofTrue(
			ATTRIBUTE_NAME);

	public static UnaryOperator<GenericObject> ADJUSTMENT_FUNCTION = genericObject ->
			genericObject.withAddedAttribute(ATTRIBUTE);
}
