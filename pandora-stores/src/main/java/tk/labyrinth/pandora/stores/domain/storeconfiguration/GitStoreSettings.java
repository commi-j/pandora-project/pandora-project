package tk.labyrinth.pandora.stores.domain.storeconfiguration;

import lombok.Value;
import tk.labyrinth.pandora.stores.extra.credentials.KeyCredentialsReference;

@Value
public class GitStoreSettings {

	String branch;

	KeyCredentialsReference credentialsReference;

	String url;
}
