package tk.labyrinth.pandora.stores.domain.rootobject;

public class PandoraRootConstants {

	public static final String EXPLICIT_SIGNATURE_ATTRIBUTE_NAME_ATTRIBUTE_NAME = "pandoraExplicitSignatureAttributeName";

	public static final String MODEL_REFERENCE_ATTRIBUTE_NAME = "modelReference";

	public static final String UID_ATTRIBUTE_NAME = "uid";
}
