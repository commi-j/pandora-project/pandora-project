package tk.labyrinth.pandora.stores.domain.mongodb.mapper;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.PropertyPredicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class AttributeRenamingMongoDbObjectMapper implements MongoDbObjectMapper {

	@NonNull
	List<Pair<String, String>> attributeRenameRules;

	@Override
	public GenericObject mapOnRead(GenericObject object) {
		return object.map(attribute -> attributeRenameRules
				.find(rule -> Objects.equals(rule.getRight(), attribute.getName()))
				.map(rule -> attribute.withName(rule.getLeft()))
				.getOrElse(attribute));
	}

	@Override
	public GenericObject mapOnWrite(GenericObject object) {
		return object.map(attribute -> attributeRenameRules
				.find(rule -> Objects.equals(rule.getLeft(), attribute.getName()))
				.map(rule -> attribute.withName(rule.getRight()))
				.getOrElse(attribute));
	}

	@Override
	public Predicate mapPredicate(Predicate predicate) {
		return rename(predicate, attributeRenameRules.toMap(Pair::getLeft, Pair::getRight));
	}

	public static Predicate rename(Predicate predicate, Map<String, String> nameMappings) {
		Predicate result;
		{
			if (predicate != null) {
				if (predicate instanceof JunctionPredicate junctionPredicate) {
					result = junctionPredicate.withPredicates(List.ofAll(junctionPredicate.getOperands())
							.map(operand -> rename(operand, nameMappings))
							.asJava());
				} else if (predicate instanceof PropertyPredicate<?> propertyPredicate) {
					String mappedName = nameMappings.get(propertyPredicate.property()).getOrNull();
					//
					result = mappedName != null
							? propertyPredicate.withFirst(mappedName)
							: propertyPredicate;
				} else {
					throw new NotImplementedException();
				}
			} else {
				result = null;
			}
		}
		return result;
	}
}
