package tk.labyrinth.pandora.stores.domain.mongodb.codec;

import lombok.RequiredArgsConstructor;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.CheckForNull;

@LazyComponent
@RequiredArgsConstructor
public class MongoDbGenericObjectCodecProvider implements CodecProvider {

	private final ObjectProvider<MongoDbGenericObjectCodec> genericObjectCodecProvider;

	@CheckForNull
	@Override
	@SuppressWarnings("unchecked")
	public <T> Codec<T> get(Class<T> clazz, CodecRegistry registry) {
		return clazz == GenericObject.class ? (Codec<T>) genericObjectCodecProvider.getObject(registry) : null;
	}
}
