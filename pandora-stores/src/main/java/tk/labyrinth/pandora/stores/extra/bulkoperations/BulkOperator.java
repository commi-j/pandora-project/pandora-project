package tk.labyrinth.pandora.stores.extra.bulkoperations;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class BulkOperator {

	private final GenericObjectManipulator genericObjectManipulator;

	private final GenericObjectSearcher genericObjectSearcher;

	public void bulkEdit(BulkEditCommand command) {
		List<GenericObject> foundObjects = genericObjectSearcher.search(
				ParameterizedQuery.<CodeObjectModelReference>builder()
						.parameter(command.getModelReference())
						.predicate(command.getSelector())
						.build());
		//
		logger.debug("Found %s Objects matching selector %s".formatted(foundObjects.size(), command.getSelector()));
		//
		foundObjects.forEach(foundObject -> genericObjectManipulator.update(foundObject.withNullableAttribute(
				command.getAttributeName(),
				command.getAttributeValue())));
	}
}
