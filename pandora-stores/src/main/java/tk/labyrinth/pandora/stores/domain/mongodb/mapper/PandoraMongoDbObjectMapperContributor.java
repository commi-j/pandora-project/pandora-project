package tk.labyrinth.pandora.stores.domain.mongodb.mapper;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;

import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class PandoraMongoDbObjectMapperContributor implements MongoDbObjectMapperContributor {

	public static final MongoDbObjectMapper MAPPER = new AttributeRenamingMongoDbObjectMapper(List.of(
			Pair.of("uid", "_id")));

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	@Nullable
	@Override
	public MongoDbObjectMapper contributeMongoDbObjectMapper(CodeObjectModelReference target) {
		MongoDbObjectMapper result;
		{
			String signatureAttributeName = rootObjectPrimaryReferenceHandler.findSignatureAttributeName(target);
			//
			if (signatureAttributeName != null) {
				result = !Objects.equals(signatureAttributeName, "_id")
						? new AttributeRenamingMongoDbObjectMapper(List.of(Pair.of(signatureAttributeName, "_id")))
						: NoOpMongoDbObjectMapper.INSTANCE;
			} else {
				// FIXME: We plan to get rid of implicit signatureAttributes.
				//
				result = MAPPER;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(CodeObjectModelReference target) {
		return Integer.MAX_VALUE;
	}
}
