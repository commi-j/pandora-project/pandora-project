package tk.labyrinth.pandora.stores.domain.rootobject;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RootObjectPrimaryReference {

	GenericReferenceAttribute attribute;

	String modelCode;

	public CodeObjectModelReference computeModelReference() {
		return CodeObjectModelReference.of(modelCode);
	}

	public GenericObjectReference toGenericObjectReference() {
		return GenericObjectReference.of(GenericReference.of(modelCode, attribute));
	}

	public static RootObjectPrimaryReference of(String modelCode, GenericObjectAttribute attribute) {
		return RootObjectPrimaryReference.builder()
				.attribute(GenericReferenceAttribute.builder()
						.name(attribute.getName())
						.value(attribute.getValue().asSimple().getValue())
						.build())
				.modelCode(modelCode)
				.build();
	}
}
