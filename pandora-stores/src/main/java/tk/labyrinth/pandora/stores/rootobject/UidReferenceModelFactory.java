package tk.labyrinth.pandora.stores.rootobject;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.CustomReferenceModelFactory;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ReferenceModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.referencemodel.ReferenceModelRegistry;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@LazyComponent
public class UidReferenceModelFactory implements CustomReferenceModelFactory {

	@Nullable
	@Override
	public ReferenceModel createCustomReferenceModel(Type javaType) {
		ReferenceModel result;
		{
			if (javaType instanceof ParameterizedType parameterizedType &&
					parameterizedType.getRawType() == UidReference.class) {
				result = ReferenceModel.builder()
						.attributeNames(List.of("uid"))
						.classSignature(ClassSignature.of(UidReference.class))
						.modelCode(ReferenceModelRegistry.resolveModelCode(javaType))
						.name("UidReference<%s>".formatted(
								((Class<?>) parameterizedType.getActualTypeArguments()[0]).getSimpleName()))
						.build();
			} else {
				result = null;
			}
		}
		return result;
	}
}
