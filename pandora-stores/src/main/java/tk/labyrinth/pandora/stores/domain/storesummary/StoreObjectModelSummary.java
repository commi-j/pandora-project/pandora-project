package tk.labyrinth.pandora.stores.domain.storesummary;

import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;

@Builder(toBuilder = true)
@Value
public class StoreObjectModelSummary {

	@Nullable
	Long count;

	CodeObjectModelReference objectModelReference;
	//
	// TODO: Attributes and their counts.
}
