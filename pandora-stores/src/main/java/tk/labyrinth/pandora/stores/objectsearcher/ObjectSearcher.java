package tk.labyrinth.pandora.stores.objectsearcher;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

/**
 * Component designed to perform read operations from CRUD.<br>
 * For create, update and delete operations see ObjectManipulator.
 *
 * @param <Q> Query
 * @param <T> Type
 */
public interface ObjectSearcher<Q, T> extends TypeAware<T> {

	long count(Q query);

	@Nullable
	T findSingle(Reference<T> reference);

	T getSingle(Reference<T> reference);

	<R extends Reference<T>> ResolvedReference<T, R> resolve(R reference);

	List<T> search(Q query);
}
