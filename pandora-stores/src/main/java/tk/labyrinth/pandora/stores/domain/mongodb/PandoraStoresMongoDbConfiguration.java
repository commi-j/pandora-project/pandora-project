package tk.labyrinth.pandora.stores.domain.mongodb;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class PandoraStoresMongoDbConfiguration {

	@Bean
	@ConditionalOnMissingBean(MongoDbCollectionLocator.class)
	@Lazy
	public MongoDbCollectionLocator mongoDbCollectionLocator() {
		return new DefaultMongoDbCollectionLocator();
	}
}
