package tk.labyrinth.pandora.stores.objectmanipulator;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.Nullable;
import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class TypedObjectManipulatorBeanContributor extends
		UnaryParameterizedBeanContributor<TypedObjectManipulator<?>> {

	@Nullable
	@Override
	protected Object doContributeBean(
			BeanFactory beanFactory,
			Class<? extends TypedObjectManipulator<?>> base,
			Type parameter) {
		return beanFactory
				.withBean(TypeUtils.getClass(parameter))
				.withSuppressedBeanContributor(this)
				.getBean(TypedObjectManipulator.class);
	}

	@Nullable
	@Override
	protected Integer doGetSupportDistance(Class<? extends TypedObjectManipulator<?>> base, Type parameter) {
		return Integer.MAX_VALUE;
	}
}
