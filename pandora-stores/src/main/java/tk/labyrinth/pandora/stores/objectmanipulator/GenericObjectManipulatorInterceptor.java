package tk.labyrinth.pandora.stores.objectmanipulator;

import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

public interface GenericObjectManipulatorInterceptor {

	GenericObject onCreate(GenericObject object);

	GenericObject onCreateOrUpdate(GenericObject object);

	GenericObject onUpdate(GenericObject object);
}
