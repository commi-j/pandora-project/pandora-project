package tk.labyrinth.pandora.stores.rootobject;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.UUID;

@Value(staticConstructor = "of")
public class UidReference<T extends HasUid> implements Reference<T> {

	public static final String UID_ATTRIBUTE_NAME = HasUid.UID_ATTRIBUTE_NAME;

	String modelCode;

	UUID uid;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(modelCode, UID_ATTRIBUTE_NAME, uid);
	}

	@JsonCreator
	public static UidReference<?> from(String value) {
		GenericReference reference = GenericReference.from(value);
		return of(reference.getModelCode(), UUID.fromString(reference.getAttributeValue(UID_ATTRIBUTE_NAME)));
	}
}
