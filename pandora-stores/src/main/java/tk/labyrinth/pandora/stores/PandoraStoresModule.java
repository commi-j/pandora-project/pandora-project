package tk.labyrinth.pandora.stores;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.context.PandoraContextModule;
import tk.labyrinth.pandora.datatypes.PandoraDatatypesModule;

@ComponentScan
@Import({
		PandoraContextModule.class,
		PandoraDatatypesModule.class,
})
@Slf4j
//
// FIXME: Without this annotation we fail for not having bean ConversionService. Need to find better way to add it.
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
//@SpringBootConfiguration
public class PandoraStoresModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
