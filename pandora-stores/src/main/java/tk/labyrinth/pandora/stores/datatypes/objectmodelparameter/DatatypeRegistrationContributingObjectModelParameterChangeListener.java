package tk.labyrinth.pandora.stores.datatypes.objectmodelparameter;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.GenericDatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.AliasDatatypeRegistrationReference;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.DatatypeRegistration;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.objectmodelparameter.ObjectModelParameter;
import tk.labyrinth.pandora.datatypes.objectmodelparameter.ObjectModelParameterUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

@LazyComponent
@RequiredArgsConstructor
public class DatatypeRegistrationContributingObjectModelParameterChangeListener extends
		ObjectChangeListener<ObjectModelParameter> {

	@SmartAutowired
	private TypedObjectManipulator<DatatypeRegistration> datatypeRegistrationManipulator;

	@Override
	protected void onObjectChange(ObjectChangeEvent<ObjectModelParameter> event) {
		String modelCode = event.extractSameAttributeValue(ObjectModelParameter::getModelCode);
		String name = event.extractSameAttributeValue(ObjectModelParameter::getName);
		String signature = ObjectModelParameterUtils.createDatatypeSignature(modelCode, name);
		String alias = ObjectModelParameterUtils.createDatatypeAlias(signature);
		//
		if (event.isCreateOrUpdate()) {
			datatypeRegistrationManipulator.createOrUpdateWithAdjustment(
					DatatypeRegistration.builder()
							.alias(alias)
							.features(List.of(GenericDatatypeBaseFeature.of(ObjectModelUtils.createDatatypeBaseReference(modelCode))))
							.signature(signature)
							.build(),
					genericObject -> genericObject.withAddedAttribute(PandoraIndexConstants.ATTRIBUTE));
		} else {
			datatypeRegistrationManipulator.delete(AliasDatatypeRegistrationReference.of(alias));
		}
	}
}
