package tk.labyrinth.pandora.stores.domain.mongodb.mapper;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

public class NoOpMongoDbObjectMapper implements MongoDbObjectMapper {

	public static final NoOpMongoDbObjectMapper INSTANCE = new NoOpMongoDbObjectMapper();

	@Override
	public GenericObject mapOnRead(GenericObject object) {
		return object;
	}

	@Override
	public GenericObject mapOnWrite(GenericObject object) {
		return object;
	}

	@Override
	public Predicate mapPredicate(Predicate predicate) {
		return predicate;
	}
}
