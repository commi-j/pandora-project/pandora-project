package tk.labyrinth.pandora.stores.objectcache;

import io.vavr.collection.List;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

public interface GenericObjectCache {

	void put(GenericObject object);

	List<GenericObject> search(PlainQuery contextlessQuery);

	boolean supports(PlainQuery contextlessQuery);
}
