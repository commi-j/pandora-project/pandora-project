package tk.labyrinth.pandora.stores.objectcache;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.concurrent.CopyOnWriteArrayList;

@LazyComponent
@RequiredArgsConstructor
public class GenericObjectCacheRegistry {

	private final CopyOnWriteArrayList<GenericObjectCache> caches = new CopyOnWriteArrayList<>();

	@Nullable
	public GenericObjectCache findCache(PlainQuery contextlessQuery) {
		GenericObjectCache result;
		{
			List<GenericObjectCache> matchedCaches = List.ofAll(caches)
					.filter(cache -> cache.supports(contextlessQuery));
			//
			if (!matchedCaches.isEmpty()) {
				result = matchedCaches.single();
			} else {
				result = null;
			}
		}
		return result;
	}

	public void registerCache(GenericObjectCache cache) {
		caches.add(cache);
	}
}
