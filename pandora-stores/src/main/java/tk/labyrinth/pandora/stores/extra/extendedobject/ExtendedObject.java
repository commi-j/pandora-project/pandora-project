package tk.labyrinth.pandora.stores.extra.extendedobject;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

/**
 * @param <T> Type
 */
@Value(staticConstructor = "of")
public class ExtendedObject<T> {

	GenericObject genericObject;

	T object;
}
