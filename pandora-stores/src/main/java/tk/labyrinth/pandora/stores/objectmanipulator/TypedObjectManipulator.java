package tk.labyrinth.pandora.stores.objectmanipulator;

import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelFactory;
import tk.labyrinth.pandora.misc4j.java.util.function.FunctionUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.util.Objects;
import java.util.function.UnaryOperator;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class TypedObjectManipulator<T> implements ObjectManipulator<PlainQuery, T> {

	private final ConverterRegistry converterRegistry;

	private final GenericObjectManipulator genericObjectManipulator;

	private final ObjectModelFactory objectModelFactory;

	@SmartAutowired(required = false)
	private List<TypedObjectManipulatorInterceptor<?>> allInterceptors;

	private List<TypedObjectManipulatorInterceptor<T>> interceptors;

	@SmartAutowired
	private Class<T> type;

	private ObjectModel typeObjectModel;

	@PostConstruct
	@SuppressWarnings({"rawtypes", "unchecked"})
	private void postConstruct() {
		{
			interceptors = (List) allInterceptors.filter(interceptor -> interceptor.getParameterClass() == type);
		}
		{
			// We can't use ObjectModelRegistry here, as this is a Context creation phase where
			// we don't have Runners to enrich the context yet.
			// So we're creating new models instead of using registered ones.
			typeObjectModel = objectModelFactory.createModelFromJavaClass(type);
		}
	}

	@Override
	public void create(T object) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onCreate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		genericObjectManipulator.create(converterRegistry.convert(objectAfterInterception, GenericObject.class));
	}

	@Override
	public void createOrUpdate(T object) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onCreateOrUpdate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		genericObjectManipulator.createOrUpdate(
				converterRegistry.convert(objectAfterInterception, GenericObject.class));
	}

	public void createOrUpdateWithAdjustment(T object, UnaryOperator<GenericObject> adjustOperator) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onCreate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		genericObjectManipulator.createOrUpdate(adjustOperator.apply(
				converterRegistry.convert(objectAfterInterception, GenericObject.class)));
	}

	public void createWithAdjustment(T object, UnaryOperator<GenericObject> adjustOperator) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onCreate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		genericObjectManipulator.create(adjustOperator.apply(
				converterRegistry.convert(objectAfterInterception, GenericObject.class)));
	}

	@Deprecated // uid is no longer an implicit root id. We don't want implicits ids anymore.
	public T createWithGeneratedUid(T object) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onCreate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		GenericObject genericObject = converterRegistry.convert(objectAfterInterception, GenericObject.class)
				.withAddedAttribute(RootObjectUtils.createNewUidAttribute());
		//
		genericObjectManipulator.create(genericObject);
		//
		return converterRegistry.convert(genericObject, type);
	}

	@Deprecated
	public void createWithHashCodeBasedUid(T object, Object valueForUidComputation) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onCreate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		genericObjectManipulator.create(converterRegistry.convert(objectAfterInterception, GenericObject.class)
				.withAddedAttribute(RootObjectUtils.createHashCodeBasedUidAttribute(valueForUidComputation)));
	}

	@Override
	public void delete(PlainQuery query) {
		genericObjectManipulator.delete(query.withParameter(CodeObjectModelReference.from(typeObjectModel)));
	}

	public void delete(Predicate predicate) {
		delete(PlainQuery.builder()
				.predicate(predicate)
				.build());
	}

	@Override
	public void delete(Reference<T> reference) {
		genericObjectManipulator.delete(GenericObjectReference.from(reference.toString()));
	}

	@Override
	public void update(T object) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onUpdate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		genericObjectManipulator.update(converterRegistry.convert(objectAfterInterception, GenericObject.class));
	}

	@Override
	public T updateObjectWithOperator(Reference<T> reference, UnaryOperator<T> updateOperator) {
		GenericObject nextGenericObject = genericObjectManipulator.updateObjectWithOperator(
				GenericObjectReference.from(reference.toString()),
				currentGenericObject -> {
					T currentTypedObject = converterRegistry.convert(currentGenericObject, type);
					//
					T nextTypedObject = updateOperator.apply(currentTypedObject);
					//
					T objectAfterInterception = interceptors.toJavaStream().reduce(
							nextTypedObject,
							(nextObject, interceptor) -> interceptor.onUpdate(nextObject),
							FunctionUtils::throwUnreachableStateException);
					//
					return converterRegistry.convert(objectAfterInterception, GenericObject.class)
							.withoutAttribute(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME)
							.withAddedAttributes(currentGenericObject.getAttributes()
									.filter(currentGenericObjectAttribute -> !typeObjectModel.getAttributes().exists(
											objectModelAttribute -> Objects.equals(
													objectModelAttribute.getName(),
													currentGenericObjectAttribute.getName()))));
				});
		//
		return converterRegistry.convert(nextGenericObject, type);
	}

	public void updateWithAdjustment(T object, UnaryOperator<GenericObject> adjustOperator) {
		T objectAfterInterception = interceptors.toJavaStream().reduce(
				object,
				(nextObject, interceptor) -> interceptor.onUpdate(nextObject),
				FunctionUtils::throwUnreachableStateException);
		//
		genericObjectManipulator.update(adjustOperator.apply(
				converterRegistry.convert(objectAfterInterception, GenericObject.class)));
	}
}
