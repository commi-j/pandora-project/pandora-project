package tk.labyrinth.pandora.stores.extra.extendedobject;

import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.common.ConversionPriorities;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Set;

@LazyComponent
@Priority(ConversionPriorities.GENERIC_OBJECT_PRIORITY - 1)
@RequiredArgsConstructor
public class FromGenericObjectToExtendedObjectConverter implements ConditionalGenericConverter {

	private final ObjectProvider<ConverterRegistry> converterRegistryProvider;

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		return ExtendedObject.of(
				(GenericObject) source,
				converterRegistryProvider.getObject().convert(
						source,
						targetType.getResolvableType().getGeneric(0).getType()));
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Set.of(new ConvertiblePair(GenericObject.class, ExtendedObject.class));
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return sourceType.getType() == GenericObject.class && targetType.getType() == ExtendedObject.class;
	}
}
