package tk.labyrinth.pandora.stores.datatypes.polymorphic;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicObjectModelFeature;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicLinkIndex;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectindex.Indexer;

@LazyComponent
@RequiredArgsConstructor
public class PolymorphicLinkIndexer implements Indexer<ObjectModel, PolymorphicLinkIndex> {

	@Override
	public List<PolymorphicLinkIndex> createIndices(ObjectModel target) {
		List<PolymorphicLinkIndex> result = target.getFeaturesOrEmpty()
				.filter(PolymorphicObjectModelFeature.class::isInstance)
				.map(PolymorphicObjectModelFeature.class::cast)
				.map(feature -> PolymorphicLinkIndex.builder()
						.leafReference(ObjectModelUtils.createDatatypeBaseReference(CodeObjectModelReference.from(target)))
						.qualifierAttribute(feature.getQualifierAttribute())
						.rootReference(feature.getRootReference())
						.build());
		//
		return result;
	}

	@Override
	public Reference<ObjectModel> createTargetReference(ObjectModel target) {
		return CodeObjectModelReference.from(target);
	}
}
