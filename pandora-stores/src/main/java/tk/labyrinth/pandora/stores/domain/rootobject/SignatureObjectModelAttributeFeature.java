package tk.labyrinth.pandora.stores.domain.rootobject;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;

@Model // FIXME: We don't need it to be model, should be discoverable because of being polymorphic.
@PolymorphicLeaf(rootClass = ObjectModelAttributeFeature.class, qualifierAttributeValue = "signature")
@Value
public class SignatureObjectModelAttributeFeature implements ObjectModelAttributeFeature {

	public static SignatureObjectModelAttributeFeature instance() {
		return new SignatureObjectModelAttributeFeature();
	}
}
