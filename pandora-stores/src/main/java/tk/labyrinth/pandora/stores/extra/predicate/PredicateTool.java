package tk.labyrinth.pandora.stores.extra.predicate;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.NegationPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.PropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.dependencies.expresso.MatchesMaskPredicate;
import tk.labyrinth.pandora.misc4j.exception.ExceptionUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.collectoin.ListUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Objects;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PredicateTool {

	private final ConverterRegistry converterRegistry;

	private boolean intersects(MintermCanonicalForm first, MintermCanonicalForm second) {
		// TODO: https://en.wikipedia.org/wiki/Quine%E2%80%93McCluskey_algorithm ?
		//
		boolean result;
		{
			List<Tuple2<Minterm, Minterm>> mintermProduct = first.getMinterms().crossProduct(second.getMinterms())
					.toList();
			//
			result = mintermProduct.toJavaStream().anyMatch(mintermTuple ->
					resolveRelation(mintermTuple._1(), mintermTuple._2()).isEffectiveIntersection());
		}
		return result;
	}

	private boolean isIncludedBy(MintermCanonicalForm first, MintermCanonicalForm second) {
		boolean result;
		{
			List<Tuple2<Minterm, Minterm>> mintermProduct = first.getMinterms().crossProduct(second.getMinterms())
					.toList();
			//
			result = mintermProduct.toJavaStream().allMatch(mintermTuple ->
					resolveRelation(mintermTuple._1(), mintermTuple._2()).isEffectiveSubset());
		}
		return result;
	}

	private SetRelation resolveRelation(Minterm first, Minterm second) {
		SetRelation result;
		{
			val attributeNameToPredicatesPairMap = first.toMap().keySet().addAll(second.toMap().keySet()).toMap(
					Function.identity(),
					property -> Pair.of(first.getPredicates(property), second.getPredicates(property)));
			//
			result = SetRelation.resolveMultiple(attributeNameToPredicatesPairMap
					.map(tuple -> resolveRelation(tuple._2().getLeft(), tuple._2().getRight())));
		}
		return result;
	}

	private SetRelation resolveRelation(List<PropertyPredicate<?>> first, List<PropertyPredicate<?>> second) {
		SetRelation result;
		{
			if (!first.isEmpty()) {
				if (!second.isEmpty()) {
					if (first.size() == 1 && second.size() == 1) {
						PropertyPredicate<?> firstPredicate = first.single();
						PropertyPredicate<?> secondPredicate = second.single();
						//
						if (firstPredicate.operator() == secondPredicate.operator()) {
							if (firstPredicate.operator() == ObjectOperator.EQUAL_TO) {
								result = Objects.equals(firstPredicate.value(), secondPredicate.value())
										? SetRelation.SIMILAR
										: SetRelation.DISJOINT;
							} else {
								throw new NotImplementedException();
							}
						} else if (firstPredicate.operator() == ObjectOperator.IN &&
								secondPredicate.operator() == ObjectOperator.EQUAL_TO) {
							result = ((List) firstPredicate.value()).contains(secondPredicate.value())
									? SetRelation.SUPERSET : SetRelation.DISJOINT;
						} else if (firstPredicate.operator() == ObjectOperator.EQUAL_TO &&
								secondPredicate.operator() == ObjectOperator.IN) {
							result = ((List) secondPredicate.value()).contains(firstPredicate.value())
									? SetRelation.SUBSET : SetRelation.DISJOINT;
						} else if (firstPredicate.operator() == ObjectOperator.EQUAL_TO &&
								secondPredicate instanceof StringPropertyPredicate stringPropertyPredicate) {
							result = resolveStringRelation((ObjectPropertyPredicate) firstPredicate, stringPropertyPredicate);
						} else if ((firstPredicate instanceof StringPropertyPredicate stringPropertyPredicate) &&
								secondPredicate.operator() == ObjectOperator.EQUAL_TO) {
							result = resolveStringRelation((ObjectPropertyPredicate) secondPredicate, stringPropertyPredicate)
									.inversed();
						} else {
							throw new NotImplementedException();
						}
					} else {
						throw new IllegalArgumentException("Supports only single so far.");
					}
				} else {
					result = SetRelation.SUBSET;
				}
			} else {
				result = second.isEmpty() ? SetRelation.SIMILAR : SetRelation.SUPERSET;
			}
		}
		return result;
	}

	public boolean includes(Predicate firstPredicate, Predicate secondPredicate) {
		return isIncludedBy(secondPredicate, firstPredicate);
	}

	public boolean intersects(Predicate firstPredicate, Predicate secondPredicate) {
		return intersects(
				toCanonicalForm(normalizePredicate(firstPredicate)),
				toCanonicalForm(normalizePredicate(secondPredicate)));
	}

	public boolean isIncludedBy(Predicate firstPredicate, Predicate secondPredicate) {
		return isIncludedBy(
				toCanonicalForm(normalizePredicate(firstPredicate)),
				toCanonicalForm(normalizePredicate(secondPredicate)));
	}

	@Nullable
	public Predicate normalizePredicate(@Nullable Predicate predicate) {
		Predicate result;
		{
			if (predicate == null) {
				result = null;
			} else if (predicate instanceof JunctionPredicate junctionPredicate) {
				if (junctionPredicate.predicates().size() == 1) {
					result = normalizePredicate(junctionPredicate.predicates().get(0));
				} else {
					result = new JunctionPredicate(
							junctionPredicate.operator(),
							ListUtils.mapNonnull(junctionPredicate.predicates(), this::normalizePredicate));
				}
			} else if (predicate instanceof MatchesMaskPredicate) {
				result = predicate;
			} else if (predicate instanceof NegationPredicate negationPredicate) {
				result = negationPredicate.withFirst(normalizePredicate(predicate));
			} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
				ValueWrapper normalizedObjectValue = converterRegistry.convert(
						objectPropertyPredicate.second(),
						ValueWrapper.class);
				result = objectPropertyPredicate.withSecond(normalizedObjectValue != null
						? normalizedObjectValue.unwrap()
						: null);
			} else if (predicate instanceof StringPropertyPredicate) {
				result = predicate;
			} else {
				throw new NotImplementedException(ExceptionUtils.render(predicate));
			}
		}
		return result;
	}

	public List<GenericObjectAttribute> predicateToAttributes(@Nullable Predicate predicate) {
		List<GenericObjectAttribute> result;
		{
			if (predicate != null) {
				if (predicate instanceof JunctionPredicate junctionPredicate) {
					if (junctionPredicate.isAnd()) {
						result = List.ofAll(junctionPredicate.predicates())
								.map(ObjectPropertyPredicate.class::cast)
								.map(objectPropertyPredicate -> GenericObjectAttribute.of(
										objectPropertyPredicate.property(),
										converterRegistry.convert(
												objectPropertyPredicate.value(),
												ValueWrapper.class)));
					} else {
						throw new NotImplementedException();
					}
				} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
					result = List.of(GenericObjectAttribute.of(
							objectPropertyPredicate.property(),
							converterRegistry.convert(objectPropertyPredicate.value(), ValueWrapper.class)));
				} else {
					throw new NotImplementedException();
				}
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	public MintermCanonicalForm toCanonicalForm(Predicate normalizedPredicate) {
		MintermCanonicalForm result;
		{
			if (normalizedPredicate instanceof JunctionPredicate junctionPredicate) {
				result = List.ofAll(junctionPredicate.predicates())
						.map(this::toCanonicalForm)
						.reduce((a, b) -> a.mergeWith(b, junctionPredicate.operator()));
			} else if (normalizedPredicate instanceof PropertyPredicate<?> propertyPredicate) {
				result = MintermCanonicalForm.of(List.of(Minterm.of(List.of(propertyPredicate))));
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public static String prepareValue(String value, boolean caseSensitive) {
		return caseSensitive ? value : value.toLowerCase();
	}

	public static SetRelation resolveStringRelation(ObjectPropertyPredicate first, StringPropertyPredicate second) {
		String firstValue = prepareValue((String) first.value(), second.caseSensitive());
		String secondValue = prepareValue(second.value(), second.caseSensitive());
		//
		// SUBSET OR DISJOINT.
		boolean matches = switch (second.operator()) {
			case CONTAINS -> firstValue.contains(secondValue);
			case ENDS_WITH -> firstValue.endsWith(secondValue);
			case STARTS_WITH -> firstValue.startsWith(secondValue);
		};
		//
		return matches ? SetRelation.SUBSET : SetRelation.DISJOINT;
	}
}
