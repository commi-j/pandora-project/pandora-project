package tk.labyrinth.pandora.stores.tool.startup;

import tk.labyrinth.pandora.datatypes.domain.tag.Tag;

public class StartupConstants {

	public static final String STARTUP_ATTRIBUTE_NAME = "startup";

	public static final String STARTUP_TAG_NAME = "startup";

	public static final Tag STARTUP_TAG = Tag.of(STARTUP_TAG_NAME);
}
