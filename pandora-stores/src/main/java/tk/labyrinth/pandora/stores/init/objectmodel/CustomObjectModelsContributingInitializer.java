package tk.labyrinth.pandora.stores.init.objectmodel;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;

@Bean
@Priority(100)
@RequiredArgsConstructor
public class CustomObjectModelsContributingInitializer extends GenericObjectsContributingInitializer {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	@Override
	protected List<GenericObject> contributeObjects() {
		return javaBaseTypeRegistry.getObjectModelEntries().map(this::convertAndAddHardcoded);
	}
}
