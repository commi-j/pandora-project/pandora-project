package tk.labyrinth.pandora.stores.datatypes.javabasetype;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.GenericDatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.DatatypeRegistration;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.BackingJavaClassDatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.BackingJavaVariableTypeDatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeParameterUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.datatypes.domain.origin.PandoraOriginUtils;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

@Bean
@RequiredArgsConstructor
public class DatatypeRegistrationContributingJavaBaseTypeChangeListener extends
		TypedObjectChangeListener<JavaBaseType, SignatureJavaBaseTypeReference> {

	@SmartAutowired
	private TypedObjectManipulator<DatatypeRegistration> datatypeRegistrationManipulator;

	@Override
	protected void onObjectChange(TypedObjectChangeEvent<JavaBaseType, SignatureJavaBaseTypeReference> event) {
		SignatureJavaBaseTypeReference primaryReference = event.getPrimaryReference();
		//
		{
			datatypeRegistrationManipulator.delete(PandoraOriginUtils.createPredicate(primaryReference));
		}
		{
			if (event.isCreateOrUpdate()) {
				TypeSignature typeSignature = primaryReference.getSignature();
				String alias = JavaBaseTypeUtils.createDatatypeAlias(typeSignature);
				AliasDatatypeBaseReference datatypeBaseReference = AliasDatatypeBaseReference.of(alias);
				//
				JavaBaseType javaBaseType = event.getNextObjectOrFail();
				String datatypeSignature = javaBaseType.getObjectModelReference() != null
						? javaBaseType.getObjectModelReference().getCode()
						: alias;
				//
				datatypeRegistrationManipulator.createOrUpdateWithAdjustment(
						DatatypeRegistration.builder()
								.alias(alias)
								.features(List.of(BackingJavaClassDatatypeBaseFeature.of(typeSignature.getClassSignature())))
								.name(typeSignature.getSimpleName())
								.parameters(javaBaseType.getParameterNames())
								.parentReference(computeParent(javaBaseType))
								.signature(datatypeSignature)
								.build(),
						genericObject -> genericObject.withAddedAttributes(
								PandoraIndexConstants.ATTRIBUTE,
								PandoraOriginUtils.createAttribute(primaryReference)));
				//
				if (javaBaseType.getParameterNames() != null) {
					javaBaseType.getParameterNames().forEach(parameterName ->
							datatypeRegistrationManipulator.createOrUpdateWithAdjustment(
									DatatypeRegistration.builder()
											.alias(JavaBaseTypeParameterUtils.createDatatypeAlias(typeSignature, parameterName))
											.features(List.of(
													BackingJavaVariableTypeDatatypeBaseFeature.of(TypeDescription.of(
															JavaBaseTypeParameterUtils.composeSignature(typeSignature.toString(), parameterName))),
													GenericDatatypeBaseFeature.of(datatypeBaseReference)))
											.name(parameterName)
											.parameters(null) // Parameters can not declare other parameters.
											.signature(JavaBaseTypeParameterUtils.composeSignature(datatypeSignature, parameterName))
											.build(),
									genericObject -> genericObject.withAddedAttributes(
											PandoraIndexConstants.ATTRIBUTE,
											PandoraOriginUtils.createAttribute(primaryReference))));
				}
			}
		}
	}

	public static AliasDatatypeBaseReference computeParent(JavaBaseType javaBaseType) {
		AliasDatatypeBaseReference result;
		{
			if (javaBaseType.getHints().contains(JavaBaseTypeHint.ENUM)) {
				result = AliasDatatypeBaseReference.of(PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME);
			} else if (javaBaseType.getHints().contains(JavaBaseTypeHint.REFERENCE)) {
				result = AliasDatatypeBaseReference.of(PandoraDatatypeConstants.REFERENCE_DATATYPE_NAME);
			} else {
				result = switch (javaBaseType.computeDatatypeKind()) {
					case LIST -> AliasDatatypeBaseReference.of(PandoraDatatypeConstants.LIST_DATATYPE_NAME);
					case OBJECT -> PandoraDatatypeConstants.OBJECT_DATATYPE_REFERENCE;
					case SIMPLE -> AliasDatatypeBaseReference.of(PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME);
					case VALUE -> AliasDatatypeBaseReference.of(PandoraDatatypeConstants.VALUE_DATATYPE_NAME);
				};
			}
		}
		return result;
	}
}
