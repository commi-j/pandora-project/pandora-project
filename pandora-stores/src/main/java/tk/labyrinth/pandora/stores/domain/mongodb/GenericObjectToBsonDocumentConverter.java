package tk.labyrinth.pandora.stores.domain.mongodb;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootConstants;

@Bean
@RequiredArgsConstructor
public class GenericObjectToBsonDocumentConverter implements Converter<GenericObject, Document> {

	private final ObjectMapper objectMapper;

	@Override
	public Document convert(GenericObject source) {
		// FIXME: modelReference should not be added to non-root objects firsthand, but for now it is not true.
		Document result = objectMapper.convertValue(
				source.withoutAttribute(PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME),
				Document.class);
		//
		return result;
	}
}
