package tk.labyrinth.pandora.stores.objectindex;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

@RequiredArgsConstructor
public class IndexerWrapper<T, I extends ObjectIndex<T>> {

	@Getter
	private final Indexer<T, I> indexer;

	public Class<I> getIndexClass() {
		return TypeUtils.getClassInferred(ParameterUtils.getSecondActualParameter(
				indexer.getClass(),
				Indexer.class));
	}

	public Class<T> getTargetClass() {
		return TypeUtils.getClassInferred(ParameterUtils.getFirstActualParameter(
				indexer.getClass(),
				Indexer.class));
	}
}
