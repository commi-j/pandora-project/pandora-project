package tk.labyrinth.pandora.stores.objectsearcher;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class TypedObjectSearcherBeanContributor extends UnaryParameterizedBeanContributor<TypedObjectSearcher<?>> {

	@Nullable
	@Override
	protected Object doContributeBean(
			BeanFactory beanFactory,
			Class<? extends TypedObjectSearcher<?>> base,
			Type parameter) {
		return beanFactory
				.withSuppressedBeanContributor(this)
				.withBean(TypeUtils.getClass(parameter), true)
				.getBean(TypedObjectSearcher.class);
	}

	@Nullable
	@Override
	protected Integer doGetSupportDistance(
			BeanFactory beanFactory,
			Class<? extends TypedObjectSearcher<?>> base,
			Type parameter) {
		return Integer.MAX_VALUE;
	}
}
