package tk.labyrinth.pandora.stores.domain.activity;

import io.opentelemetry.api.trace.Span;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Value(staticConstructor = "of")
public class PandoraSpanContext {

	@NonNull
	PandoraSpanActivityRegistry pandoraSpanActivityRegistry;

	Span span;

	public Pair<String, String> getSpanIdPair() {
		return Pair.of(span.getSpanContext().getTraceId(), span.getSpanContext().getSpanId());
	}

	public Void withinSpan(String spanName, Consumer<PandoraSpanContext> function) {
		return pandoraSpanActivityRegistry.withinSpan(spanName, function);
	}

	public <T> T withinSpan(String spanName, Function<PandoraSpanContext, T> function) {
		return pandoraSpanActivityRegistry.withinSpan(spanName, function);
	}

	public Void withinSpan(String spanName, Runnable function) {
		return pandoraSpanActivityRegistry.withinSpan(spanName, function);
	}

	public <T> T withinSpan(String spanName, Supplier<T> function) {
		return pandoraSpanActivityRegistry.withinSpan(spanName, function);
	}
}
