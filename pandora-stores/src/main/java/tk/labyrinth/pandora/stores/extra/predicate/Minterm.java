package tk.labyrinth.pandora.stores.extra.predicate;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Value;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.predicate.PropertyPredicate;

import java.util.Objects;

// TODO: Ensure no duplicate attributes in constructor?
@Value(staticConstructor = "of")
public class Minterm {

	List<PropertyPredicate<?>> predicates;

	public List<PropertyPredicate<?>> getPredicates(String name) {
		return predicates.filter(predicate -> Objects.equals(predicate.property(), name));
	}

	public Minterm mergeWith(Minterm other) {
		// TODO: Ensure no duplicate attributes?
		return Minterm.of(predicates.appendAll(other.predicates));
	}

	public Map<String, List<PropertyPredicate<?>>> toMap() {
		return predicates.groupBy(PropertyPredicate::property);
	}

	public Predicate toPredicate() {
		return Predicates.and(predicates.toJavaStream());
	}
}
