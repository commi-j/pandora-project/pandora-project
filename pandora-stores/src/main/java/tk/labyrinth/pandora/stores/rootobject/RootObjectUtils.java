package tk.labyrinth.pandora.stores.rootobject;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectUtils;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootConstants;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;

import java.util.Objects;
import java.util.UUID;

@Deprecated // We're reworking root objects to add flexibility to key handling.
public class RootObjectUtils {

	@Deprecated
	public static final String UID_ATTRIBUTE_NAME = PandoraRootConstants.UID_ATTRIBUTE_NAME;

	@Deprecated
	public static final String MODEL_REFERENCE_ATTRIBUTE_NAME = PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME;

	@Deprecated
	public static UUID computeUidFromValueHashCode(Object valueForUidComputation) {
		return new UUID(0, valueForUidComputation.hashCode());
	}

	@Deprecated
	public static GenericObjectAttribute createHashCodeBasedUidAttribute(Object valueForUidComputation) {
		return createUidAttribute(computeUidFromValueHashCode(valueForUidComputation));
	}

	public static GenericObjectAttribute createModelReferenceAttribute(CodeObjectModelReference objectModelReference) {
		return GenericObjectAttribute.ofSimple(MODEL_REFERENCE_ATTRIBUTE_NAME, objectModelReference.toString());
	}

	public static GenericObjectAttribute createModelReferenceAttribute(ObjectModel objectModel) {
		return createModelReferenceAttribute(CodeObjectModelReference.from(objectModel));
	}

	public static GenericObjectAttribute createNewUidAttribute() {
		return createUidAttribute(UUID.randomUUID());
	}

	public static GenericObjectAttribute createUidAttribute(UUID uid) {
		return GenericObjectAttribute.ofSimple(UID_ATTRIBUTE_NAME, uid.toString());
	}

	public static GenericObjectReference createUidReference(GenericObject object) {
		return GenericObjectReference.of(GenericReference.of(
				PandoraRootUtils.getModelReference(object).getCode(),
				List.of(GenericReferenceAttribute.of(UID_ATTRIBUTE_NAME, getUid(object).toString()))));
	}

	@Nullable
	public static UUID findUid(GenericObject object) {
		String uidString = object.findAttributeValueAsString(UID_ATTRIBUTE_NAME);
		return uidString != null ? UUID.fromString(uidString) : null;
	}

	public static UUID getUid(GenericObject object) {
		return Objects.requireNonNull(findUid(object), "Require non-null object.uid: %s".formatted(object));
	}

	public static boolean hasEqualUid(GenericObject first, GenericObject second) {
		return GenericObjectUtils.hasEqualAttribute(UID_ATTRIBUTE_NAME, first, second);
	}

	public static GenericObject withNonNullModelReference(
			GenericObject object,
			CodeObjectModelReference modelReference) {
		Objects.requireNonNull(modelReference, "modelReference");
		//
		return object.withNonNullSimpleAttribute(MODEL_REFERENCE_ATTRIBUTE_NAME, modelReference.toString());
	}
}
