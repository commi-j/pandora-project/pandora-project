package tk.labyrinth.pandora.stores.objectmanipulator;

import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcher;

import java.util.function.UnaryOperator;

/**
 * Component designed to perform create, update and delete operations from CRUD.<br>
 * For read operations see {@link ObjectSearcher}.
 */
public interface ObjectManipulator<Q, T> {

	void create(T object);

	void createOrUpdate(T object);

	void delete(Q query);

	void delete(Reference<T> reference);

	void update(T object);

	T updateObjectWithOperator(Reference<T> reference, UnaryOperator<T> updateOperator);
}
