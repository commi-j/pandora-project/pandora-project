package tk.labyrinth.pandora.stores.domain.mongodb;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import io.opentelemetry.instrumentation.annotations.SpanAttribute;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.Document;
import org.bson.internal.ProvidersCodecRegistry;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.domain.mongodb.MongoDbContext;
import tk.labyrinth.expresso.query.domain.mongodb.MongoDbQueryExecutor;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.domain.mongodb.codec.MongoDbGenericObjectCodecProvider;
import tk.labyrinth.pandora.stores.domain.mongodb.mapper.MongoDbObjectMapper;
import tk.labyrinth.pandora.stores.domain.mongodb.mapper.MongoDbObjectMapperContributor;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReference;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storesummary.StoreObjectModelSummary;
import tk.labyrinth.pandora.stores.domain.storesummary.StoreSummary;
import tk.labyrinth.pandora.stores.extra.predicate.PredicateTool;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreBase;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.util.Objects;

// TODO: When authentication error happens check if authSource is not admin. By default users are stored in admin db
//  and if you specify db name in connection string it will look for user there and fail.
//  This looks like an unexpected behaviour so giving a hint to check auth db and probably add authSource=admin would
//  be a good deed.
//
@PrototypeScopedComponent
@Slf4j
public class MongoDbGenericObjectStore extends GenericObjectStoreBase {

	/**
	 * <a href="https://www.mongodb.com/docs/drivers/java/sync/v4.3/fundamentals/crud/write-operations/upsert/">https://www.mongodb.com/docs/drivers/java/sync/v4.3/fundamentals/crud/write-operations/upsert/</a>
	 */
	public static final FindOneAndReplaceOptions UPSERT_OPTIONS = new FindOneAndReplaceOptions().upsert(true);

	private final MongoDbQueryExecutor mongoDbQueryExecutor = new MongoDbQueryExecutor();

	@Autowired
	private MongoDbCollectionLocator collectionLocator;

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired(required = false)
	private MongoDbDatabaseLocator databaseLocator;

	@Autowired
	private MongoDbGenericObjectCodecProvider genericObjectCodecProvider;

	@Autowired
	private RootGenericObjectToBsonDocumentConverter rootGenericObjectToBsonDocumentConverter;

	private MongoClient mongoClient;

	@Nullable
	private MongoDatabase mongoDatabase;

	@SmartAutowired
	private List<MongoDbObjectMapperContributor> mongoDbObjectMapperContributors;

	@Autowired
	private PredicateTool predicateTool;

	public MongoDbGenericObjectStore(StoreConfiguration configuration) {
		super(configuration);
	}

	private Document convertRootObjectForWrite(GenericObject inputObject, GenericObject refinedObject) {
		CodeObjectModelReference modelReference = PandoraRootUtils.findModelReference(inputObject);
		//
		MongoDbObjectMapper mongoDbObjectMapper = getMapper(modelReference);
		//
		val objectToSave = mongoDbObjectMapper.mapOnWrite(refinedObject);
		//
		Document result = rootGenericObjectToBsonDocumentConverter.convert(modelReference, objectToSave);
		//
		return result;
	}

	@WithSpan
	private long doCount(
			@SpanAttribute MongoDbContext context,
			@SpanAttribute ParameterizedQuery<Class<Document>> query) {
		return mongoDbQueryExecutor.count(context, query);
	}

	@WithSpan
	private java.util.List<Document> doSearch(
			@SpanAttribute MongoDbContext context,
			@SpanAttribute ParameterizedQuery<Class<Document>> query) {
		return mongoDbQueryExecutor.search(context, query);
	}

	private MongoDatabase getDatabase(MongoClient mongoClient, String databaseName) {
		MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
		return mongoDatabase.withCodecRegistry(new ProvidersCodecRegistry(java.util.List.of(
				mongoDatabase.getCodecRegistry(),
				genericObjectCodecProvider)));
	}

	private MongoDbObjectMapper getMapper(CodeObjectModelReference objectModelReference) {
		return HasSupportDistance.selectHandler(mongoDbObjectMapperContributors, objectModelReference)
				.getHandler()
				.contributeMongoDbObjectMapper(objectModelReference);
	}

	private MongoDbObjectMapper getMapper(Predicate predicate) {
		return getMapper(DefaultMongoDbCollectionLocator.extractModelReference(predicate));
	}

	private Pair<GenericObject, String> locateCollection(GenericObject object) {
		val result = collectionLocator.locateCollection(getConfiguration(), object);
		//
		Objects.requireNonNull(result, "result");
		Objects.requireNonNull(result.getLeft(), "result.left");
		Objects.requireNonNull(result.getRight(), "result.right");
		//
		return result;
	}

	private Pair<GenericObject, @Nullable String> locateDatabase(GenericObject object) {
		val result = databaseLocator.locateDatabase(getConfiguration(), object);
		//
		Objects.requireNonNull(result, "result");
		Objects.requireNonNull(result.getLeft(), "result.left");
		//
		return result;
	}

	private GenericObject mapOnRead(CodeObjectModelReference objectModelReference, @NonNull Document document) {
		GenericObject genericObject = getMapper(objectModelReference)
				.mapOnRead(
						converterRegistry.convert(document, GenericObject.class)
								// TODO: Check if this attribute actually exists here.
								.withoutAttribute(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME));
		//
		// FIXME: This is a crutch. We need to use smarter modelReference to collectionName and vice versa mapping.
		//  See good example in search method.
		return genericObject.withAddedAttributes(RootObjectUtils.createModelReferenceAttribute(objectModelReference));
	}

	@PostConstruct
	private void postConstruct() {
		ConnectionString connectionString = new ConnectionString(getConfiguration().getMongoDbSettings().getUri());
		//
		mongoClient = MongoClients.create(connectionString);
		mongoDatabase = connectionString.getDatabase() != null
				? getDatabase(mongoClient, connectionString.getDatabase())
				: null;
		//
		if (databaseLocator == null) {
			if (mongoDatabase != null) {
				databaseLocator = new NoOpMongoDbDatabaseLocator();
			} else {
				throw new IllegalArgumentException(
						"Either connectionString should contain database or databaseLocator should be provided");
			}
		}
	}

	private Predicate preparePredicate(MongoDbObjectMapper mapper, Predicate predicate) {
		return mapper.mapPredicate(predicateTool.normalizePredicate(predicate));
	}

	@WithSpan
	private MongoDbSearchContext resolveSearchContext(PlainQuery query) {
		MongoDbDatabaseLocator.DatabaseContext databaseContext = databaseLocator.locateDatabase(
				getConfiguration(),
				predicateTool.normalizePredicate(query.getPredicate()));
		//
		MongoDbCollectionLocator.CollectionContext collectionContext = collectionLocator.locateCollection(
				getConfiguration(),
				predicateTool.normalizePredicate(databaseContext.getRefinedPredicate()));
		//
		if (mongoDatabase != null && databaseContext.getDatabaseName() != null &&
				!Objects.equals(mongoDatabase.getName(), databaseContext.getDatabaseName())) {
			throw new IllegalArgumentException("Require same databaseName: located = %s, connectionString = %s"
					.formatted(databaseContext.getDatabaseName(), mongoDatabase.getName()));
		}
		MongoDatabase mongoDatabaseToUse = mongoDatabase != null
				? mongoDatabase
				: mongoClient.getDatabase(databaseContext.getDatabaseName());
		//
		return MongoDbSearchContext.builder()
				.collectionContext(collectionContext)
				.database(mongoDatabaseToUse)
				.databaseContext(databaseContext)
				.build();
	}

	@Override
	protected void doCreate(RootObjectPrimaryReference primaryReference, GenericObject object) {
		Pair<GenericObject, @Nullable String> objectAndDatabaseName = locateDatabase(object);
		//
		if (mongoDatabase != null && objectAndDatabaseName.getRight() != null &&
				!Objects.equals(mongoDatabase.getName(), objectAndDatabaseName.getRight())) {
			throw new IllegalArgumentException("Require same databaseName: located = %s, connectionString = %s"
					.formatted(objectAndDatabaseName.getRight(), mongoDatabase.getName()));
		}
		MongoDatabase mongoDatabaseToUse = mongoDatabase != null
				? mongoDatabase
				: getDatabase(mongoClient, objectAndDatabaseName.getRight());
		//
		Pair<GenericObject, String> objectAndCollectionName = locateCollection(objectAndDatabaseName.getLeft());
		//
		MongoCollection<Document> mongoCollection = mongoDatabaseToUse.getCollection(
				objectAndCollectionName.getRight());
		//
		Document documentToSave = convertRootObjectForWrite(object, objectAndCollectionName.getLeft());
		//
		mongoCollection.insertOne(documentToSave);
	}

	@Nullable
	@Override
	protected GenericObject doCreateOrUpdate(RootObjectPrimaryReference primaryReference, GenericObject object) {
		Pair<GenericObject, @Nullable String> objectAndDatabaseName = locateDatabase(object);
		//
		if (mongoDatabase != null && objectAndDatabaseName.getRight() != null &&
				!Objects.equals(mongoDatabase.getName(), objectAndDatabaseName.getRight())) {
			throw new IllegalArgumentException("Require same databaseName: located = %s, connectionString = %s"
					.formatted(objectAndDatabaseName.getRight(), mongoDatabase.getName()));
		}
		MongoDatabase mongoDatabaseToUse = mongoDatabase != null
				? mongoDatabase
				: mongoClient.getDatabase(objectAndDatabaseName.getRight());
		//
		Pair<GenericObject, String> objectAndCollectionName = locateCollection(objectAndDatabaseName.getLeft());
		//
		MongoCollection<Document> mongoCollection = mongoDatabaseToUse.getCollection(
				objectAndCollectionName.getRight());
		//
		Document documentToSave = convertRootObjectForWrite(object, objectAndCollectionName.getLeft());
		//
		Document previousDocument = mongoCollection.findOneAndReplace(
				Filters.eq("_id", primaryReference.getAttribute().getValue()),
				documentToSave,
				UPSERT_OPTIONS);
		//
		GenericObject previousObject = previousDocument != null
				? mapOnRead(primaryReference.computeModelReference(), previousDocument)
				: null;
		//
		return previousObject;
	}

	@Override
	protected GenericObject doDelete(RootObjectPrimaryReference primaryReference, GenericObject object) {
		Pair<GenericObject, @Nullable String> objectAndDatabaseName = locateDatabase(object);
		//
		if (mongoDatabase != null && objectAndDatabaseName.getRight() != null &&
				!Objects.equals(mongoDatabase.getName(), objectAndDatabaseName.getRight())) {
			throw new IllegalArgumentException("Require same databaseName: located = %s, connectionString = %s"
					.formatted(objectAndDatabaseName.getRight(), mongoDatabase.getName()));
		}
		MongoDatabase mongoDatabaseToUse = mongoDatabase != null
				? mongoDatabase
				: mongoClient.getDatabase(objectAndDatabaseName.getRight());
		//
		Pair<GenericObject, String> objectAndCollectionName = locateCollection(objectAndDatabaseName.getLeft());
		//
		MongoCollection<Document> mongoCollection = mongoDatabaseToUse.getCollection(
				objectAndCollectionName.getRight());
		//
		Document previousDocument = mongoCollection.findOneAndDelete(
				Filters.eq("_id", primaryReference.getAttribute().getValue()));
		//
		Objects.requireNonNull(previousDocument, "previousDocument");
		//
		GenericObject previousObject = mapOnRead(primaryReference.computeModelReference(), previousDocument);
		//
		return previousObject;
	}

	@Override
	protected StoreSummary doGetSummary() {
		StoreSummary result;
		{
			if (mongoDatabase != null) {
				result = StoreSummary.builder()
						.objectModelSummaries(List.ofAll(mongoDatabase.listCollectionNames())
								.map(collectionName -> StoreObjectModelSummary.builder()
										.count(mongoDatabase.getCollection(collectionName).countDocuments())
										.objectModelReference(collectionLocator.objectModelReferenceFromCollectionName(
												collectionName))
										.build()))
						.build();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Override
	protected GenericObject doUpdate(RootObjectPrimaryReference primaryReference, GenericObject object) {
		Pair<GenericObject, @Nullable String> objectAndDatabaseName = locateDatabase(object);
		//
		if (mongoDatabase != null && objectAndDatabaseName.getRight() != null &&
				!Objects.equals(mongoDatabase.getName(), objectAndDatabaseName.getRight())) {
			throw new IllegalArgumentException("Require same databaseName: located = %s, connectionString = %s"
					.formatted(objectAndDatabaseName.getRight(), mongoDatabase.getName()));
		}
		MongoDatabase mongoDatabaseToUse = mongoDatabase != null
				? mongoDatabase
				: mongoClient.getDatabase(objectAndDatabaseName.getRight());
		//
		Pair<GenericObject, String> objectAndCollectionName = locateCollection(objectAndDatabaseName.getLeft());
		//
		MongoCollection<Document> mongoCollection = mongoDatabaseToUse.getCollection(
				objectAndCollectionName.getRight());
		//
		Document documentToSave = convertRootObjectForWrite(object, objectAndCollectionName.getLeft());
		//
		Document previousDocument = mongoCollection.findOneAndReplace(
				Filters.eq("_id", documentToSave.get("_id")),
				documentToSave);
		//
		Objects.requireNonNull(previousDocument, "previousDocument");
		//
		GenericObject previousObject = mapOnRead(primaryReference.computeModelReference(), previousDocument);
		//
		return previousObject;
	}

	@Override
	@WithSpan
	public long count(PlainQuery query) {
		MongoDbObjectMapper mapper = getMapper(query.getPredicate());
		//
		MongoDbSearchContext searchContext = resolveSearchContext(query);
		MongoDbCollectionLocator.CollectionContext collectionContext = searchContext.getCollectionContext();
		//
		long count = doCount(
				MongoDbContext.of(collectionContext.getCollectionName(), searchContext.getDatabase()),
				ParameterizedQuery.builderWithClassParameter(Document.class)
						.limit(query.getLimit())
						.offset(query.getOffset())
						.predicate(preparePredicate(mapper, collectionContext.getRefinedPredicate()))
						.sort(query.getSort())
						.build());
		//
		return count;
	}

	@Override
	@WithSpan
	public List<GenericObject> search(@SpanAttribute PlainQuery query) {
		MongoDbObjectMapper mapper = getMapper(query.getPredicate());
		//
		MongoDbSearchContext searchContext = resolveSearchContext(query);
		MongoDbDatabaseLocator.DatabaseContext databaseContext = searchContext.getDatabaseContext();
		MongoDbCollectionLocator.CollectionContext collectionContext = searchContext.getCollectionContext();
		//
		List<Document> foundObjects = List.ofAll(doSearch(
				MongoDbContext.of(collectionContext.getCollectionName(), searchContext.getDatabase()),
				ParameterizedQuery.builderWithClassParameter(Document.class)
						.limit(query.getLimit())
						.offset(query.getOffset())
						.predicate(preparePredicate(mapper, collectionContext.getRefinedPredicate()))
						.sort(query.getSort())
						.build()));
		//
		List<GenericObject> convertedObjects = foundObjects
				.map(document -> mapper.mapOnRead(
						converterRegistry.convert(document, GenericObject.class)
								.withoutAttribute(RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME)))
				.map(object -> object
						.withAddedAttributes(collectionContext.getImplicitAttributes())
						.withAddedAttributes(databaseContext.getImplicitAttributes()));
		//
		return convertedObjects;
	}

	@Override
	public String toString() {
		return "MongoDbGenericObjectStore(configuration=%s)".formatted(getConfiguration());
	}
}
