package tk.labyrinth.pandora.stores.domain.rootobject;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatyperegistration.DatatypeRegistration;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

@Bean
@RequiredArgsConstructor
public class RootObjectPrimaryReferenceHandler {

	// FIXME: We need these because there are listeners that create other entires with these types.
	//  If we make listeners to be registered later we may get rid of this solution.
	private final Map<CodeObjectModelReference, String> specialSignatureAttributeCases = HashMap.of(
			CodeObjectModelReference.of(DatatypeRegistration.MODEL_CODE), DatatypeRegistration.ALIAS_ATTRIBUTE_NAME,
			CodeObjectModelReference.of(ObjectModel.MODEL_CODE), ObjectModel.CODE_ATTRIBUTE_NAME,
			CodeObjectModelReference.of("pageroute"), "path" // FIXME: This one is from UI module.
	);

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	// TODO: Key may be a combination of multiple attributes.
	//  But then the question - do we want to remove key attributes or let them duplicate the data? Open question.
	private String computeSignatureAttributeName(
			GenericObject object,
			CodeObjectModelReference objectModelReference) {
		String result;
		{
			String signatureAttributeName = object.findAttributeValueAsString(
					PandoraRootConstants.EXPLICIT_SIGNATURE_ATTRIBUTE_NAME_ATTRIBUTE_NAME);
			//
			if (signatureAttributeName != null) {
				// TODO: Track all such objects and verify they are consistent.
				//  Probably verify they are hardcoded as well.
				//
				result = signatureAttributeName;
			} else {
				result = findSignatureAttributeName(objectModelReference);
			}
		}
		return result;
	}

	@Nullable
	public RootObjectPrimaryReference findPrimaryReference(GenericObject object) {
		RootObjectPrimaryReference result;
		{
			CodeObjectModelReference objectModelReference = CodeObjectModelReference.from(object.getAttributeValueAsString(
					PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME));
			//
			String signatureAttributeName = computeSignatureAttributeName(object, objectModelReference);
			//
			GenericObjectAttribute signatureAttribute = object.findAttribute(signatureAttributeName);
			//
			result = signatureAttribute != null
					? RootObjectPrimaryReference.of(objectModelReference.getCode(), signatureAttribute)
					: null;
		}
		return result;
	}

	@Nullable
	public String findSignatureAttributeName(CodeObjectModelReference objectModelReference) {
		String result;
		{
			String specialSignatureAttributeName = specialSignatureAttributeCases.get(objectModelReference).getOrNull();
			//
			if (specialSignatureAttributeName != null) {
				result = specialSignatureAttributeName;
			} else {
				ObjectModel objectModel = objectModelSearcher.findSingle(objectModelReference);
				//
				if (objectModel != null) {
					RootObjectModelFeature rootObjectModelFeature = objectModel.getFeaturesOrEmpty()
							.filter(RootObjectModelFeature.class::isInstance)
							.map(RootObjectModelFeature.class::cast)
							.getOrNull();
					//
					if (rootObjectModelFeature != null) {
						result = rootObjectModelFeature.getKey();
					} else {
						// FIXME: Should fail if model has no root feature?
						//
						// FIXME: There should be some extra declaration for Pandora objects and no default behaviour. Probably.
						result = PandoraRootConstants.UID_ATTRIBUTE_NAME;
					}
				} else {
					// FIXME: Should fail if model has no root feature?
					//
					// FIXME: There should be some extra declaration for Pandora objects and no default behaviour. Probably.
					result = PandoraRootConstants.UID_ATTRIBUTE_NAME;
				}
			}
		}
		return result;
	}

	public RootObjectPrimaryReference getPrimaryReference(GenericObject object) {
		RootObjectPrimaryReference primaryReference = findPrimaryReference(object);
		//
		if (primaryReference == null) {
			throw new IllegalArgumentException("Require non-null signature: %s".formatted(object));
		}
		//
		return primaryReference;
	}

	public String getSignatureAttributeName(CodeObjectModelReference objectModelReference) {
		String result = findSignatureAttributeName(objectModelReference);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found %s".formatted(objectModelReference));
		}
		//
		return result;
	}
}
