package tk.labyrinth.pandora.stores.domain.mongodb;

import com.mongodb.client.MongoDatabase;
import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value(staticConstructor = "of")
@With
public class MongoDbSearchContext {

	MongoDbCollectionLocator.CollectionContext collectionContext;

	MongoDatabase database;

	MongoDbDatabaseLocator.DatabaseContext databaseContext;
}
