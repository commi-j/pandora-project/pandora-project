package tk.labyrinth.pandora.stores.genericobject;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.core.selection.EvaluatedResults;
import tk.labyrinth.pandora.core.selection.SelectionResult;
import tk.labyrinth.pandora.datatypes.filterer.SpecificFilterer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.extra.predicate.PredicateTool;

import java.util.Map;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class GenericObjectStoreRegistry {

	private final ConverterRegistry converterRegistry;

	private final PredicateTool predicateTool;

	private final Observable<List<Pair<@Nullable Predicate, GenericObjectStore>>> storesObservable =
			Observable.withInitialValue(List.empty());

	@SmartAutowired
	private SpecificFilterer<StoreConfiguration> storeConfigurationFilterer;

	private boolean matches(@Nullable Predicate storePredicate, @Nullable Predicate getPredicate) {
		boolean result;
		{
			if (storePredicate != null && getPredicate != null) {
				result = predicateTool.intersects(storePredicate, getPredicate);
			} else {
				// If either predicate is null we have nothing to check, thus matches.
				//
				result = true;
			}
		}
		return result;
	}

	public List<GenericObjectStore> getMatchingStores(Predicate predicate) {
		return storesObservable.get()
				.filter(entry -> matches(entry.getKey(), predicateTool.normalizePredicate(predicate)))
				.map(Map.Entry::getValue);
	}

	@Deprecated
	public GenericObjectStore getStore(Predicate storeConfigurationPredicate) {
		GenericObjectStore result;
		{
			val storeConfigurationToStoreMap = storesObservable.get().toMap(
					pair -> pair.getRight().getConfiguration(),
					Pair::getRight);
			//
			List<StoreConfiguration> storeConfigurations = storeConfigurationFilterer.filter(
					storeConfigurationToStoreMap.keySet(),
					PlainQuery.builder()
							.predicate(storeConfigurationPredicate)
							.build());
			//
			if (storeConfigurations.size() != 1) {
				throw new IllegalArgumentException(("No single Store determined: " +
						"storeConfigurationPredicate = %s, storeConfigurations = %s")
						.formatted(storeConfigurationPredicate, storeConfigurations));
			}
			//
			result = storeConfigurationToStoreMap.get(storeConfigurations.single()).get();
		}
		return result;
	}

	public GenericObjectStore getStore(String storeName) {
		return storesObservable.get()
				.map(Pair::getRight)
				.filter(store -> Objects.equals(store.getConfiguration().getSlug(), storeName))
				.single();
	}

	public List<GenericObjectStore> listStores() {
		return storesObservable.get().map(Pair::getRight);
	}

	public void registerStore(GenericObjectStore store) {
		storesObservable.update(stores -> {
			int index = stores.indexWhere(pair -> pair.getValue() == store);
			if (index != -1) {
				throw new IllegalArgumentException("Already registered: %s".formatted(store));
			}
			return stores.append(Pair.of(
					predicateTool.normalizePredicate(store.getConfiguration().getPredicate()),
					store));
		});
	}

	public SelectionResult<Predicate, GenericObjectStore> selectStore(
			Predicate storeConfigurationPredicate) {
		return selectStoreConfiguration(storeConfigurationPredicate).map(storeConfiguration ->
				EvaluatedResults.ofResult(getStore(storeConfiguration.getSlug())));
	}

	public SelectionResult<Predicate, StoreConfiguration> selectStoreConfiguration(
			Predicate storeConfigurationPredicate) {
		SelectionResult<Predicate, StoreConfiguration> result;
		{
			val storeConfigurationToStoreMap = storesObservable.get().toMap(
					pair -> pair.getRight().getConfiguration(),
					Pair::getRight);
			//
			List<StoreConfiguration> storeConfigurations = storeConfigurationFilterer.filter(
					storeConfigurationToStoreMap.keySet(),
					PlainQuery.builder()
							.predicate(storeConfigurationPredicate)
							.build());
			//
			result = SelectionResult.ofZeroDistance(storeConfigurationPredicate, storeConfigurations);
		}
		return result;
	}
}
