package tk.labyrinth.pandora.stores.objectmanipulator;

import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

// FIXME: Remove TypeAware when BeanContributor supports multiple beans.
public interface TypedObjectManipulatorInterceptor<T> extends TypeAware<T> {

	T onCreate(T object);

	T onCreateOrUpdate(T object);

	T onUpdate(T object);
}
