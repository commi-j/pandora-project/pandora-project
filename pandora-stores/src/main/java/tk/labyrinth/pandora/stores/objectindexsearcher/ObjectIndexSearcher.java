package tk.labyrinth.pandora.stores.objectindexsearcher;

import io.vavr.collection.List;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

public class ObjectIndexSearcher<T, I extends ObjectIndex<T>> implements TypeAware<I> {

	public T findSingleObject(Reference<I> reference) {
		// TODO:
		throw new NotImplementedException();
	}

	public List<I> searchIndices(PlainQuery query) {
		// TODO:
		throw new NotImplementedException();
	}

	public List<T> searchObjects(PlainQuery query) {
		// TODO:
		throw new NotImplementedException();
	}
}
