package tk.labyrinth.pandora.stores.extra.credentials;

import lombok.NonNull;
import tk.labyrinth.pandora.misc4j.integration.auth.AuthorizationUtils;

public class CredentialsUtils {

	public static String toAuthorizationString(@NonNull Credentials credentials) {
		String result;
		{
			String token = credentials.getToken();
			//
			if (token != null) {
				result = AuthorizationUtils.composeBearer(token);
			} else {
				result = AuthorizationUtils.encodeBasic(credentials.getUsername(), credentials.getPassword());
			}
		}
		return result;
	}
}
