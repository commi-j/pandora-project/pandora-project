package tk.labyrinth.pandora.stores.datatypes.polymorphic;

import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.polymorphic.registry.PolymorphicClassRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.init.PandoraInitializer;

@LazyComponent
@Priority(65)
@RequiredArgsConstructor
public class PolymorphicJavaBaseTypeInitializer implements PandoraInitializer {

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final PolymorphicClassRegistry polymorphicClassRegistry;

	@Override
	public void run() {
		polymorphicClassRegistry.getAllLinks().forEach(tuple -> {
			javaBaseTypeRegistry.getJavaBaseType(tuple._1());
			tuple._2().forEach(leaf -> javaBaseTypeRegistry.getJavaBaseType(leaf.getLeafClass()));
		});
	}
}
