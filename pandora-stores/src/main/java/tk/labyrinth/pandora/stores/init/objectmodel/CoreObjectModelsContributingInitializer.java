package tk.labyrinth.pandora.stores.init.objectmodel;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelFactory;
import tk.labyrinth.pandora.datatypes.objectmodelparameter.ObjectModelParameter;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicLinkIndex;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;

@LazyComponent
@Priority(20)
@RequiredArgsConstructor
public class CoreObjectModelsContributingInitializer extends GenericObjectsContributingInitializer {

	private final ObjectModelFactory objectModelFactory;

	@Override
	protected List<GenericObject> contributeObjects() {
		return List
				.of( // NOTE: Order matters here:
						DatatypeBase.class,
						ObjectModelParameter.class,
						PolymorphicLinkIndex.class)
				.map(objectModelFactory::createModelFromJavaClass)
				.map(objectModel -> convertAndPostProcess(
						objectModel,
						genericObject -> genericObject.withAddedAttributes(
								PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(ObjectModel.CODE_ATTRIBUTE_NAME),
								PandoraHardcodedConstants.ATTRIBUTE)));
	}
}
