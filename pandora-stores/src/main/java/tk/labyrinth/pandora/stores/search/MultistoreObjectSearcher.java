package tk.labyrinth.pandora.stores.search;

public interface MultistoreObjectSearcher<Q, T> {

	MultistoreCountResult countMultistore(Q query);
}
