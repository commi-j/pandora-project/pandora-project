package tk.labyrinth.pandora.stores.extra.predicate;

import io.vavr.collection.Traversable;

public enum SetRelation {
	DISJOINT,
	INTERSECTING,
	SIMILAR,
	SUBSET,
	SUPERSET;

	public SetRelation inversed() {
		return switch (this) {
			case DISJOINT -> DISJOINT;
			case INTERSECTING -> INTERSECTING;
			case SIMILAR -> SIMILAR;
			case SUBSET -> SUPERSET;
			case SUPERSET -> SUBSET;
		};
	}

	public boolean isEffectiveIntersection() {
		return this != DISJOINT;
	}

	public boolean isEffectiveSubset() {
		return this == SIMILAR || this == SUBSET;
	}

	public boolean isEffectiveSuperset() {
		return this == SIMILAR || this == SUPERSET;
	}

	static SetRelation resolveMultiple(Traversable<SetRelation> setRelations) {
		SetRelation result;
		{
			if (setRelations.contains(DISJOINT)) {
				result = DISJOINT;
			} else if (setRelations.contains(INTERSECTING)) {
				result = INTERSECTING;
			} else if (setRelations.contains(SUBSET)) {
				result = setRelations.contains(SUPERSET) ? INTERSECTING : SUBSET;
			} else {
				result = setRelations.contains(SUPERSET) ? SUPERSET : SIMILAR;
			}
		}
		return result;
	}
}
