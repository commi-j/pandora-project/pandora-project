package tk.labyrinth.pandora.stores.datatypes.datatype;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.GenericDatatypeBaseFeature;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

@Bean
@RequiredArgsConstructor
public class DatatypeRegistry {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	public Datatype resolveDatatype(@Nullable Datatype scope, Datatype target) {
		Datatype result;
		{
			if (scope != null) {
				DatatypeBase targetDatatypeBase = datatypeBaseRegistry.getSingle(target.getBaseReference());
				//
				if (targetDatatypeBase.hasFeature(GenericDatatypeBaseFeature.class)) {
					List<GenericDatatypeBaseFeature> genericDatatypeBaseFeatures = targetDatatypeBase.getFeaturesOrEmpty()
							.filter(GenericDatatypeBaseFeature.class::isInstance)
							.map(GenericDatatypeBaseFeature.class::cast);
					//
					if (genericDatatypeBaseFeatures.exists(feature -> datatypeBaseRegistry.matches(scope, feature.getScope()))) {
						DatatypeBase scopeDatatypeBase = datatypeBaseRegistry.getSingle(scope.getBaseReference());
						//
						result = scope.getParameters().get(scopeDatatypeBase.getParameterIndex(targetDatatypeBase.getName()));
					} else {
						result = target;
					}
				} else {
					if (target.getParameters() != null) {
						result = target.withParameters(target.getParameters().map(parameter -> resolveDatatype(scope, parameter)));
					} else {
						result = target;
					}
				}
			} else {
				result = target;
			}
		}
		return result;
	}
}
