package tk.labyrinth.pandora.stores.init.temporal;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;

@LazyComponent
@Priority(10)
@RequiredArgsConstructor
public class TemporalStoreInitializer extends GenericObjectsContributingInitializer {

	@Override
	protected List<GenericObject> contributeObjects() {
		StoreConfiguration storeConfiguration = StoreConfiguration.builder()
				.designation(StoreDesignation.TEMPORAL)
				.kind(StoreKind.IN_MEMORY)
				.predicate(PandoraTemporalConstants.PREDICATE)
				.slug("pandora-temporal-store")
				.build();
		StoreRoute storeRoute = StoreRoute.builder()
				.distance(1)
				.slug("pandora-temporal-store-route")
				.storeConfigurationPredicate(Predicates.equalTo(
						StoreConfiguration.DESIGNATION_ATTRIBUTE_NAME,
						StoreDesignation.TEMPORAL))
				.testPredicate(
						PandoraTemporalConstants.PREDICATE
						// TODO: model.tags.contains(temporal)
//										Predicates.contains(
//												ObjectModel.TAGS_ATTRIBUTE_NAME,
//												PandoraConstants.TEMPORAL_MODEL_TAG_NAME)
				)
				.build();
		//
		return List.of(
				convertAndPostProcess(
						storeConfiguration,
						genericObject -> genericObject.withAddedAttributes(
								PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(StoreConfiguration.SLUG_ATTRIBUTE_NAME),
								PandoraHardcodedConstants.ATTRIBUTE)),
				convertAndPostProcess(
						storeRoute,
						genericObject -> genericObject.withAddedAttributes(
								PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(StoreRoute.SLUG_ATTRIBUTE_NAME),
								PandoraHardcodedConstants.ATTRIBUTE)));
	}
}
