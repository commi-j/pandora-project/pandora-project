package tk.labyrinth.pandora.stores.domain.mongodb;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.customdatatype.CustomDatatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;

@Bean
public class MongoDbCustomDatatypesContributingInitializer extends GenericObjectsContributingInitializer {

	@Override
	protected List<GenericObject> contributeObjects() {
		AliasDatatypeBaseReference simpleReference = AliasDatatypeBaseReference.of(
				PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME);
		//
		return List
				.of(
						CustomDatatype.builder()
								.name(MongoDbDatatypeConstants.DATE_ALIAS)
								.parentReference(simpleReference) // TODO: Instant?
								.build(),
						CustomDatatype.builder()
								.name(MongoDbDatatypeConstants.OBJECT_ID_ALIAS) // TODO: UUID?
								.parentReference(simpleReference)
								.build())
				.map(this::convertAndAddHardcoded);
	}
}
