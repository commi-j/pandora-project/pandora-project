package tk.labyrinth.pandora.stores.init;

public interface PandoraInitializer {

	void run();
}
