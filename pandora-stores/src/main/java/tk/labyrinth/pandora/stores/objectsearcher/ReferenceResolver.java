package tk.labyrinth.pandora.stores.objectsearcher;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;

@LazyComponent
@RequiredArgsConstructor
public class ReferenceResolver {

	private final ConverterRegistry converterRegistry;

	private final GenericObjectSearcher genericObjectSearcher;

	@Nullable
	public <T> T findSingle(Reference<T> reference) {
		return resolve(reference).findSingle();
	}

	public <T> T getSingle(Reference<T> reference) {
		return resolve(reference).getSingle();
	}

	public <T, R extends Reference<T>> ResolvedReference<T, R> resolve(R reference) {
		ResolvedReference<GenericObject, GenericObjectReference> resolvedReference = genericObjectSearcher.resolve(
				GenericObjectReference.from(reference.toString()));
		//
		return ResolvedReference.of(
				reference,
				resolvedReference.getObjects().map(object ->
						converterRegistry.convert(object, reference.getParameterClass())));
	}
}
