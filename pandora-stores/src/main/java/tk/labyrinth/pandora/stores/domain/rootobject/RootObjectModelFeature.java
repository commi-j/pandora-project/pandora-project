package tk.labyrinth.pandora.stores.domain.rootobject;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;

@PolymorphicLeaf(rootClass = ObjectModelFeature.class, qualifierAttributeValue = "root")
@RenderPattern("root:$key")
@Value(staticConstructor = "of")
public class RootObjectModelFeature implements ObjectModelFeature {

	// TODO: We call it signature.
	// TODO: We wan to support pattern with multiple attributes later.
	String key;
}
