package tk.labyrinth.pandora.stores.init.setup;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.core.PandoraConstants;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.MongoDbStoreSettings;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;

import java.net.URI;

@Deprecated
@ConditionalOnProperty("pandora.setupStoreConnectionString")
@LazyComponent
@Priority(2000)
@RequiredArgsConstructor
public class SetupStoreInitializer extends GenericObjectsContributingInitializer {

	/**
	 * Supports:<br>
	 * - mongodb://<br>
	 */
	@Value("${pandora.setupStoreConnectionString:#{null}}")
	private String setupStoreConnectionString;

	@Override
	protected List<GenericObject> contributeObjects() {
		List<GenericObject> result;
		{
			if (setupStoreConnectionString != null) {
				StoreConfiguration storeConfiguration = resolveStoreConnectionString(setupStoreConnectionString);
				StoreRoute storeRoute = StoreRoute.builder()
						.distance(1)
						.slug("pandora-setup-store-route")
						.storeConfigurationPredicate(Predicates.equalTo(
								StoreConfiguration.DESIGNATION_ATTRIBUTE_NAME,
								StoreDesignation.SETUP))
						.testPredicate(
								Predicates.equalTo(PandoraConstants.SETUP_MODEL_TAG_NAME, true)
								// TODO: model.tags.contains(setup)
						)
						.build();
				//
				result = List.of(
						convertAndAddHardcoded(storeConfiguration),
						convertAndAddHardcoded(storeRoute));
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	public static MongoDbStoreSettings resolveMongoDbStoreSettings(URI storeConnectionUri) {
		return MongoDbStoreSettings.builder()
				.uri(storeConnectionUri.toString())
				.build();
	}

	public static StoreConfiguration resolveStoreConnectionString(String storeConnectionString) {
		StoreConfiguration result;
		{
			URI storeConnectionUri = URI.create(storeConnectionString);
			//
			StoreKind storeKind = StoreKind.resolveStoreKind(storeConnectionUri);
			//
			result = StoreConfiguration.builder()
					.designation(StoreDesignation.SETUP)
					.gitSettings(null)
					.kind(storeKind)
					.mongoDbSettings(storeKind == StoreKind.MONGO_DB
							? resolveMongoDbStoreSettings(storeConnectionUri)
							: null)
					.predicate(null)
					.slug("pandora-setup-store")
					.build();
		}
		return result;
	}
}
