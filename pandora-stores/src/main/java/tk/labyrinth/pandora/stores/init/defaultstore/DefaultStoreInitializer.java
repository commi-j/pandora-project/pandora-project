package tk.labyrinth.pandora.stores.init.defaultstore;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.MongoDbStoreSettings;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;

import java.net.URI;

@ConditionalOnProperty("pandora.defaultStoreConnectionString")
@LazyComponent
@Priority(3000)
@RequiredArgsConstructor
public class DefaultStoreInitializer extends GenericObjectsContributingInitializer {

	/**
	 * Supports:<br>
	 * - mongodb://<br>
	 */
	@Value("${pandora.defaultStoreConnectionString:#{null}}")
	private String defaultStoreConnectionString;

	@Override
	protected List<GenericObject> contributeObjects() {
		List<GenericObject> result;
		{
			if (defaultStoreConnectionString != null) {
				StoreConfiguration storeConfiguration = resolveStoreConnectionString(defaultStoreConnectionString);
				StoreRoute storeRoute = StoreRoute.builder()
						.distance(Integer.MAX_VALUE - 1000) // TODO: Use better value for this.
						.slug("pandora-default-store-route")
						.storeConfigurationPredicate(Predicates.equalTo(
								StoreConfiguration.SLUG_ATTRIBUTE_NAME,
								"pandora-default-store"))
						.testPredicate(null)
						.build();
				//
				result = List.of(
						convertAndAddHardcoded(storeConfiguration),
						convertAndAddHardcoded(storeRoute));
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	public static MongoDbStoreSettings resolveMongoDbStoreSettings(URI storeConnectionUri) {
		return MongoDbStoreSettings.builder()
				.uri(storeConnectionUri.toString())
				.build();
	}

	public static StoreConfiguration resolveStoreConnectionString(String storeConnectionString) {
		StoreConfiguration result;
		{
			URI storeConnectionUri = URI.create(storeConnectionString);
			//
			StoreKind storeKind = StoreKind.resolveStoreKind(storeConnectionUri);
			//
			result = StoreConfiguration.builder()
					.designation(StoreDesignation.COMMON)
					.gitSettings(null)
					.kind(storeKind)
					.mongoDbSettings(storeKind == StoreKind.MONGO_DB
							? resolveMongoDbStoreSettings(storeConnectionUri)
							: null)
					.predicate(null)
					.slug("pandora-default-store")
					.build();
		}
		return result;
	}
}
