package tk.labyrinth.pandora.stores.extra.genericobject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalConverter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.common.ConversionPriorities;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.objectproxy.ObjectProxy;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

@Component
@Priority(ConversionPriorities.GENERIC_OBJECT_PRIORITY)
@RequiredArgsConstructor
public class ToGenericObjectConverter implements ConditionalConverter, Converter<Object, GenericObject> {

	// FIXME: Not sure we really need it.
	private final Set<String> ignorablePackages = HashSet.of(
			"io.vavr",
			"java.lang",
			"java.util");

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final ObjectMapper objectMapper;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	private GenericObject doConvert(Object source) {
		GenericObject result;
		{
			// We have this switch to simplify debugging of suspicious conversion exceptions.
			boolean detailed = false;
			//
			if (!detailed) {
				result = objectMapper.convertValue(source, GenericObject.class);
			} else {
				JsonNode jsonNode = objectMapper.convertValue(source, JsonNode.class);
				result = objectMapper.convertValue(jsonNode, GenericObject.class);
			}
		}
		return result;
	}

	@Override
	public GenericObject convert(Object source) {
		GenericObject result;
		{
			if (source instanceof ObjectProxy objectProxy) {
				result = objectProxy.getObject();
			} else {
				GenericObject object = doConvert(source);
				//
				if (PandoraRootUtils.findModelReference(object) != null) {
					result = object;
				} else {
					Class<?> sourceClass = source.getClass();
					//
					String sourceTypePackage = sourceClass.getPackageName();
					//
					if (!ignorablePackages.exists(sourceTypePackage::startsWith)) {
						result = RootObjectUtils.withNonNullModelReference(
								object,
								javaBaseTypeRegistry.getNormalizedObjectModelReference(sourceClass));
					} else {
						result = object;
					}
				}
			}
		}
		return result;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return !sourceType.getType().isInterface();
	}
}
