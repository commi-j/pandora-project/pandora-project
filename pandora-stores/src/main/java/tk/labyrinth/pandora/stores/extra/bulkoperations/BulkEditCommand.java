package tk.labyrinth.pandora.stores.extra.bulkoperations;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

import javax.annotation.CheckForNull;

@Builder(toBuilder = true)
@Value
@With
public class BulkEditCommand {

	String attributeName;

	@CheckForNull
	ValueWrapper attributeValue;

	CodeObjectModelReference modelReference;

	Predicate selector;
}
