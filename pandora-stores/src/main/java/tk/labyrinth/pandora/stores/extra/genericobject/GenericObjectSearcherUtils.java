package tk.labyrinth.pandora.stores.extra.genericobject;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootConstants;

import java.util.Objects;

public class GenericObjectSearcherUtils {

	public static Predicate composeContextlessPredicate(
			@Nullable Predicate contextPredicate,
			ParameterizedQuery<CodeObjectModelReference> query) {
		Predicate modelReferencePredicate = Predicates.equalTo(
				PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME,
				query.getParameter());
		//
		return Predicates.flatAnd(java.util.stream.Stream
				.of(contextPredicate, modelReferencePredicate, query.getPredicate())
				.filter(Objects::nonNull));
	}
}
