package tk.labyrinth.pandora.stores.extra.credentials;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.misc4j.java.collectoin.ListUtils;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

// TODO: Create custom model, write tests for this entity.
@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel(code = Credentials.MODEL_CODE, primaryAttribute = Credentials.KEY_ATTRIBUTE_NAME)
@RenderAttribute(Credentials.KEY_ATTRIBUTE_NAME)
@Value
@With
public class Credentials {

	public static final String ATTRIBUTES_ATTRIBUTE_NAME = "attributes";

	public static final String KEY_ATTRIBUTE_NAME = "key";

	public static final String MODEL_CODE = "credentials";

	public static final String PASSWORD_KEY = "password";

	public static final String TOKEN_KEY = "token";

	public static final String USERNAME_KEY = "username";

	// TODO: Replace with proper object (or not?).
	io.vavr.collection.Map<String, String> attributes;

	String key;

	@Nullable
	public String getPassword() {
		return attributes
				.get(PASSWORD_KEY)
				.getOrNull();
	}

	@Nullable
	public String getToken() {
		return attributes
				.get(TOKEN_KEY)
				.getOrNull();
	}

	@Nullable
	public String getUsername() {
		return attributes
				.get(USERNAME_KEY)
				.getOrNull();
	}

	@JsonCreator
	public static Credentials fromObjectNode(ObjectNode objectNode) {
		Credentials result;
		{
			ObjectNode objectNodeToUse = objectNode.deepCopy().remove(java.util.List.of(
					RootObjectUtils.MODEL_REFERENCE_ATTRIBUTE_NAME));
			//
			// FIXME: This is a flawed solution as we do not support flat structure with "attributes" attribute.
			// FIXME: When we have only 'key' attribute we don't know whether this is:
			//  flat structure with no attributes -> empty list;
			//  attributes structure with missing attributes attribute -> null list (which is malformed);
			//
			ObjectNode attributesNode = (ObjectNode) objectNodeToUse.get(ATTRIBUTES_ATTRIBUTE_NAME);
			String key = objectNodeToUse.get(KEY_ATTRIBUTE_NAME).asText();
			//
			List<Pair<String, String>> attributes;
			if (attributesNode != null) {
				// Parsing structure with attributes.
				//
				attributes = StreamUtils.from(attributesNode.fields())
						.map(field -> Pair.of(field.getKey(), field.getValue().asText()))
						.collect(List.collector());
			} else {
				// Parsing flat structure.
				//
				if (StreamUtils.from(objectNodeToUse.fieldNames()).count() < 2) {
					throw new IllegalArgumentException(
							"When attributes node is not present we expect to have >= 2 keys: 'key' and any other, actual = %s"
									.formatted(ListUtils.from(objectNodeToUse.fieldNames())));
				}
				//
				attributes = StreamUtils.from(objectNodeToUse.without(KEY_ATTRIBUTE_NAME).fields())
						.map(field -> Pair.of(field.getKey(), field.getValue().asText()))
						.collect(List.collector());
			}
			result = Credentials.builder()
					.attributes(attributes.toMap(Pair::getLeft, Pair::getRight))
					.key(key)
					.build();
		}
		return result;
	}
}
