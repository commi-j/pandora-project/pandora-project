package tk.labyrinth.pandora.stores.objectindex;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

@Bean
@Slf4j
public class ObjectIndexer {

	private final List<? extends IndexerWrapper<?, ?>> indexerWrappers;

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private GenericObjectManipulator genericObjectManipulator;

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	private Map<CodeObjectModelReference, List<IndexerWrapper<?, ?>>> objectModelReferenceToIndexerWrappersMap;

	@Autowired
	private RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	public ObjectIndexer(java.util.List<Indexer<?, ?>> indexers) {
		indexerWrappers = List.ofAll(indexers).map(IndexerWrapper::new);
	}

	private <T> List<Pair<CodeObjectModelReference, GenericObjectReference>> createReferencesForDeletion(
			GenericObject object,
			List<IndexerWrapper<?, ?>> indexerWrappers) {
		return indexerWrappers
				.map(indexerWrapper -> {
					IndexerWrapper<T, ?> castedWrapper = ObjectUtils.infer(indexerWrapper);
					//
					T typedObject = converterRegistry.convert(object, castedWrapper.getTargetClass());
					//
					return Pair.of(
							javaBaseTypeRegistry.getNormalizedObjectModelReference(castedWrapper.getIndexClass()),
							converterRegistry.convert(
									castedWrapper.getIndexer().createTargetReference(typedObject),
									GenericObjectReference.class));
				});
	}

	private <T> List<GenericObject> indexObject(
			GenericObject object,
			List<IndexerWrapper<?, ?>> indexerWrappers) {
		return indexerWrappers
				.flatMap(indexerWrapper -> Try
						.ofSupplier(() -> {
							IndexerWrapper<T, ?> castedWrapper = ObjectUtils.infer(indexerWrapper);
							//
							T typedObject = converterRegistry.convert(object, castedWrapper.getTargetClass());
							//
							List<?> typedIndices = castedWrapper.getIndexer().createIndices(typedObject);
							//
							return typedIndices.map(typedIndex -> converterRegistry
									.convert(typedIndex, GenericObject.class)
									.withAddedAttribute(RootObjectUtils.createNewUidAttribute()));
						})
						.recover(fault -> {
							logger.warn("Failed to created index: indexerWrapper = {}, object = {}",
									indexerWrapper, object, fault);
							//
							return List.empty();
						})
						.get());
	}

	@PostConstruct
	private void postConstruct() {
		objectModelReferenceToIndexerWrappersMap = ObjectUtils.infer(indexerWrappers.groupBy(indexerWrapper ->
				javaBaseTypeRegistry.getNormalizedObjectModelReference(indexerWrapper.getTargetClass())));
	}

	public void createIndices(GenericObject object) {
		updateIndices(ObjectChangeEvent.<GenericObject>builder()
				.nextObject(object)
				.previousObject(null)
				.primaryReference(rootObjectPrimaryReferenceHandler.getPrimaryReference(object).toGenericObjectReference())
				.build());
	}

	public void updateIndices(ObjectChangeEvent<GenericObject> event) {
		List<IndexerWrapper<?, ?>> indexerWrappers = objectModelReferenceToIndexerWrappersMap
				.get(getModelReference(event))
				.getOrNull();
		//
		if (indexerWrappers != null) {
			if (event.hasPreviousObject()) {
				val referencesForDeletion = createReferencesForDeletion(event.getPreviousObject(), indexerWrappers);
				referencesForDeletion.forEach(pair -> genericObjectManipulator.delete(ParameterizedQuery.builder()
						.parameter(pair.getLeft())
						.predicate(Predicates.equalTo(ObjectIndex.TARGET_REFERENCE_ATTRIBUTE_NAME, pair.getRight()))
						.build()));
			}
			if (event.hasNextObject()) {
				List<GenericObject> indices = indexObject(event.getNextObject(), indexerWrappers);
				//
				indices.forEach(genericObjectManipulator::create);
			}
		}
	}

	public static CodeObjectModelReference getModelReference(ObjectChangeEvent<GenericObject> event) {
		return PandoraRootUtils.getModelReference(ObjectUtils.orElse(event.getNextObject(), event.getPreviousObject()));
	}
}
