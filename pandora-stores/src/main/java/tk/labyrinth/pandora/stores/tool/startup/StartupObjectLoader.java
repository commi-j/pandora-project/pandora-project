package tk.labyrinth.pandora.stores.tool.startup;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.AbstractFileResolvingResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.io.FileUtils;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@Deprecated // FIXME: Use GenericObjectsContributingInitializer.
@ConditionalOnProperty(value = StartupObjectLoader.STARTUP_OBJECT_DIRECTORIES_PROPERTY)
@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class StartupObjectLoader implements ApplicationRunner {

	public static final String STARTUP_OBJECT_DIRECTORIES_PROPERTY = "pandora.startupObjectDirectories";

	private final GenericObjectManipulator genericObjectManipulator;

	private final ObjectMapper objectMapper;

	private final ResourceLoader resourceLoader;

	@Value("${" + STARTUP_OBJECT_DIRECTORIES_PROPERTY + "}")
	private String startupObjectDirectories;

	@SmartAutowired
	private TypedObjectManipulator<StoreConfiguration> storeConfigurationManipulator;

	@SmartAutowired
	private TypedObjectManipulator<StoreRoute> storeRouteManipulator;

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialize: startupObjectFolders = {}", startupObjectDirectories);
	}

	@SuppressWarnings("ConstantConditions")
	private Map<CodeObjectModelReference, List<GenericObject>> readDirectory(File directory) {
		Map<CodeObjectModelReference, List<GenericObject>> result;
		{
			List<File> files = List.of(directory.listFiles());
			//
			Map<CodeObjectModelReference, List<GenericObject>> objectModelsMap = files
					.find(file -> Objects.equals(FileUtils.getBaseName(file), ObjectModel.MODEL_CODE))
					.map(this::readObjectModels)
					.map(objectModels -> HashMap.of(CodeObjectModelReference.of(ObjectModel.MODEL_CODE), objectModels))
					.getOrElse(HashMap.empty());
			//
			result = files
					.filter(file -> !Objects.equals(FileUtils.getBaseName(file), ObjectModel.MODEL_CODE))
					.toMap(
							objectsFile -> CodeObjectModelReference.of(FileUtils.getBaseName(objectsFile)),
							objectsFile -> {
								String objectModelCode = FileUtils.getBaseName(objectsFile);
								//
								return readObjects(
										objectsFile,
										objectModelsMap
												.getOrElse(
														CodeObjectModelReference.of(ObjectModel.MODEL_CODE),
														List.empty())
												.exists(objectModel -> Objects.equals(
														objectModel.getAttributeValueAsString(
																ObjectModel.CODE_ATTRIBUTE_NAME),
														objectModelCode)));
							})
					.merge(objectModelsMap);
		}
		return result;
	}

	private <T> List<T> readFile(File file, Class<T> elementClass) {
		return IoUtils.unchecked(() -> objectMapper.readValue(
				file,
				objectMapper.constructType(TypeUtils.parameterize(List.class, elementClass))));
	}

	private List<GenericObject> readObjectModels(File objectModelsFile) {
		return readFile(objectModelsFile, ObjectModel.class)
				.map(objectModel -> objectModel.withAddedTag(StartupConstants.STARTUP_TAG))
				.map(objectModel -> objectMapper.convertValue(objectModel, GenericObject.class))
				.map(objectModel -> objectModel.withAddedAttributes(
						RootObjectUtils.createModelReferenceAttribute(CodeObjectModelReference.of(
								ObjectModel.MODEL_CODE)),
						GenericObjectAttribute.ofTrue(StartupConstants.STARTUP_ATTRIBUTE_NAME),
						RootObjectUtils.createHashCodeBasedUidAttribute(objectModel.getAttributeValueAsString(
								ObjectModel.CODE_ATTRIBUTE_NAME))));
	}

	private List<GenericObject> readObjects(File objectsFile, boolean modelIsStartup) {
		List<GenericObject> result;
		{
			CodeObjectModelReference objectModelReference = CodeObjectModelReference.of(FileUtils.getBaseName(
					objectsFile));
			//
			result = readFile(objectsFile, GenericObject.class).map(genericObject -> genericObject.withAddedAttributes(
					List
							.of(
									RootObjectUtils.createModelReferenceAttribute(objectModelReference),
									RootObjectUtils.createNewUidAttribute())
							.appendAll(modelIsStartup
									? List.empty()
									: List.of(GenericObjectAttribute.ofTrue(
									StartupConstants.STARTUP_ATTRIBUTE_NAME)))));
		}
		return result;
	}

	private void setupStore() {
		String name = "startup-store";
		//
		{
			StoreConfiguration storeConfiguration = StoreConfiguration.builder()
					.designation(StoreDesignation.SETUP)
					.kind(StoreKind.IN_MEMORY)
					.predicate(Predicates.equalTo(StartupConstants.STARTUP_ATTRIBUTE_NAME, true))
					.slug(name)
					.build();
			//
			storeConfigurationManipulator.createWithAdjustment(
					storeConfiguration,
					PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		}
		{
			StoreRoute storeRoute = StoreRoute.builder()
					.distance(0)
					.slug("%s-route".formatted(name))
					.storeConfigurationPredicate(Predicates.equalTo(StoreConfiguration.SLUG_ATTRIBUTE_NAME, name))
					.testPredicate(Predicates.equalTo(StartupConstants.STARTUP_ATTRIBUTE_NAME, true))
					.build();
			//
			storeRouteManipulator.createWithAdjustment(storeRoute, PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		}
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		setupStore();
		//
		List.of(startupObjectDirectories.split(",")).forEach(startupObjectFolder -> {
			Resource resource = resourceLoader.getResource(startupObjectFolder);
			//
			if (resource instanceof AbstractFileResolvingResource abstractFileResolvingResource) {
				File file;
				try {
					file = abstractFileResolvingResource.getFile();
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				}
				//
				if (file.isDirectory()) {
					Map<CodeObjectModelReference, List<GenericObject>> startupObjects = readDirectory(file);
					//
					startupObjects.forEach(tuple -> tuple._2().forEach(genericObjectManipulator::create));
				} else {
					throw new IllegalArgumentException("Require directory: %s".formatted(file));
				}
			} else {
				throw new NotImplementedException();
			}
		});
	}
}
