package tk.labyrinth.pandora.stores.init.root;

import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRouteRegistry;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreRegistry;
import tk.labyrinth.pandora.stores.genericobject.SimpleInMemoryGenericObjectStore;
import tk.labyrinth.pandora.stores.init.PandoraInitializer;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;

@LazyComponent
@Priority(0)
@RequiredArgsConstructor
public class RootStoreInitializer implements PandoraInitializer {

	private final ConverterRegistry converterRegistry;

	private final GenericObjectManipulator genericObjectManipulator;

	private final ObjectProvider<SimpleInMemoryGenericObjectStore> genericObjectStoreProvider;

	private final GenericObjectStoreRegistry genericObjectStoreRegistry;

	private final GenericObjectAttribute initDummyAttribute = GenericObjectAttribute.ofTrue("init-dummy");

	private final StoreRouteRegistry storeRouteRegistry;

	private GenericObject registerHardcodedStoreRoute() {
		StoreRoute hardcodedStoreRoute = StoreRoute.builder()
				.distance(0)
				.slug("pandora-hardcoded-route")
				.storeConfigurationPredicate(Predicates.equalTo(
						StoreConfiguration.DESIGNATION_ATTRIBUTE_NAME,
						StoreDesignation.ROOT))
				.testPredicate(Predicates.equalTo(PandoraHardcodedConstants.ATTRIBUTE_NAME, true))
				.build();
		//
		storeRouteRegistry.registerStoreRoute(hardcodedStoreRoute);
		//
		return converterRegistry.convert(hardcodedStoreRoute, GenericObject.class)
				.withAddedAttributes(
						PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(StoreRoute.SLUG_ATTRIBUTE_NAME),
						PandoraHardcodedConstants.ATTRIBUTE,
						initDummyAttribute);
	}

	private GenericObject registerRootStore() {
		// TODO: Add 'initialization-dummy' and 'hardcoded' to it.
		StoreConfiguration rootStoreConfiguration = StoreConfiguration.builder()
				.designation(StoreDesignation.ROOT)
				.kind(StoreKind.IN_MEMORY)
				.predicate(null)
				.slug("pandora-root-store")
				.build();
		//
		GenericObjectStore store = genericObjectStoreProvider.getObject(rootStoreConfiguration);
		//
		genericObjectStoreRegistry.registerStore(store);
		//
		return converterRegistry.convert(rootStoreConfiguration, GenericObject.class)
				.withAddedAttributes(
						PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(StoreConfiguration.SLUG_ATTRIBUTE_NAME),
						PandoraHardcodedConstants.ATTRIBUTE,
						initDummyAttribute);
	}

	@Override
	public void run() {
		{
			GenericObject rootStoreConfiguration = registerRootStore();
			GenericObject hardcodedStoreRoute = registerHardcodedStoreRoute();
			//
			genericObjectManipulator.create(rootStoreConfiguration);
			genericObjectManipulator.create(hardcodedStoreRoute);
		}
		//
		// TODO: Process ObjectInitializers
	}
}
