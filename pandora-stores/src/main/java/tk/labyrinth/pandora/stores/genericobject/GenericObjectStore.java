package tk.labyrinth.pandora.stores.genericobject;

import io.vavr.collection.List;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storesummary.StoreSummary;

public interface GenericObjectStore {

	long count(PlainQuery query);

	void create(GenericObject object);

	void createOrUpdate(GenericObject object);

	void delete(GenericObject object);

	void delete(PlainQuery query);

	StoreConfiguration getConfiguration();

	StoreSummary getSummary();

	List<GenericObject> search(PlainQuery query);

	void update(GenericObject object);
}
