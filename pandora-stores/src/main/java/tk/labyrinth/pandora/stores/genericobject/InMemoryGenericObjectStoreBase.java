package tk.labyrinth.pandora.stores.genericobject;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Setter;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReference;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storesummary.StoreObjectModelSummary;
import tk.labyrinth.pandora.stores.domain.storesummary.StoreSummary;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectFilterer;

public abstract class InMemoryGenericObjectStoreBase extends GenericObjectStoreBase {

	private final Observable<Map<RootObjectPrimaryReference, GenericObject>> objectsObservable = Observable
			.withInitialValue(LinkedHashMap.empty());

	@Autowired
	@Setter // FIXME
	private GenericObjectFilterer genericObjectFilterer;

	public InMemoryGenericObjectStoreBase(StoreConfiguration configuration) {
		super(configuration);
	}

	@Override
	protected void doCreate(RootObjectPrimaryReference primaryReference, GenericObject object) {
		objectsObservable.update(currentObjects -> {
			GenericObject foundObject = currentObjects.get(primaryReference).getOrNull();
			//
			if (foundObject != null) {
				throw new IllegalArgumentException(
						"Require no existing object: primaryReference = %s, object = %s, foundObject = %s"
								.formatted(primaryReference, object, foundObject));
			}
			//
			Map<RootObjectPrimaryReference, GenericObject> nextObjects = currentObjects.put(primaryReference, object);
			//
			return nextObjects;
		});
	}

	@Nullable
	@Override
	protected GenericObject doCreateOrUpdate(RootObjectPrimaryReference primaryReference, GenericObject object) {
		GenericObject[] foundObjectWrapper = new GenericObject[1];
		//
		objectsObservable.update(currentObjects -> {
			GenericObject foundObject = currentObjects.get(primaryReference).getOrNull();
			//
			foundObjectWrapper[0] = foundObject;
			//
			Map<RootObjectPrimaryReference, GenericObject> nextObjects = currentObjects.put(primaryReference, object);
			//
			return nextObjects;
		});
		//
		return foundObjectWrapper[0];
	}

	@Override
	protected GenericObject doDelete(RootObjectPrimaryReference primaryReference, GenericObject object) {
		objectsObservable.update(currentObjects -> {
			GenericObject foundObject = currentObjects.get(primaryReference).getOrNull();
			//
			if (foundObject != object) {
				throw new IllegalArgumentException("Not same");
			}
			//
			Map<RootObjectPrimaryReference, GenericObject> nextObjects = currentObjects.remove(primaryReference);
			//
			return nextObjects;
		});
		//
		return object;
	}

	@Override
	protected StoreSummary doGetSummary() {
		Map<RootObjectPrimaryReference, GenericObject> objects = objectsObservable.get();
		//
		return StoreSummary.builder()
				.objectModelSummaries(objects.values()
						.groupBy(PandoraRootUtils::getModelReference)
						.map(tuple -> StoreObjectModelSummary.builder()
								.count((long) tuple._2().size())
								.objectModelReference(tuple._1())
								.build())
						.toList())
				.build();
	}

	@Override
	protected GenericObject doUpdate(RootObjectPrimaryReference primaryReference, GenericObject object) {
		GenericObject[] foundObjectWrapper = new GenericObject[1];
		//
		objectsObservable.update(currentObjects -> {
			GenericObject foundObject = currentObjects.get(primaryReference).get();
			//
			if (foundObject == null) {
				throw new IllegalArgumentException("Not found: primaryReference = %s".formatted(primaryReference));
			}
			//
			foundObjectWrapper[0] = foundObject;
			//
			Map<RootObjectPrimaryReference, GenericObject> nextObjects = currentObjects.put(primaryReference, object);
			//
			return nextObjects;
		});
		//
		return foundObjectWrapper[0];
	}

	@Override
	@WithSpan
	public long count(PlainQuery query) {
		// TODO: Add doSearch method to separate from actual searches.
		return search(query).size();
	}

	@Override
	public void delete(PlainQuery query) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	@WithSpan
	public List<GenericObject> search(PlainQuery query) {
		return genericObjectFilterer.filter(objectsObservable.get().values(), query);
	}
}
