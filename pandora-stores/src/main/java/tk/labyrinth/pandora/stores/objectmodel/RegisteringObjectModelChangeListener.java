package tk.labyrinth.pandora.stores.objectmodel;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;

@LazyComponent
@RequiredArgsConstructor
public class RegisteringObjectModelChangeListener extends ObjectChangeListener<ObjectModel> {

	private final ObjectModelRegistry objectModelRegistry;

	@Override
	protected void onObjectChange(ObjectChangeEvent<ObjectModel> event) {
		if (event.isCreateOrUpdate()) {
			objectModelRegistry.registerObjectModel(event.getNextObjectOrFail());
		} else {
			objectModelRegistry.unregisterObjectModel(CodeObjectModelReference.from(event.getPreviousObjectOrFail()));
		}
	}
}
