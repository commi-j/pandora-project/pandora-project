package tk.labyrinth.pandora.stores.selector;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class Selector {

	/**
	 * May be empty, never null.
	 */
	@NonNull
	String searchText;
}
