package tk.labyrinth.pandora.stores.extra.javabasetype;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;

@LazyComponent
@RequiredArgsConstructor
public class RegisteringJavaBaseTypeChangeListener extends ObjectChangeListener<JavaBaseType> {

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	@Override
	protected void onObjectChange(ObjectChangeEvent<JavaBaseType> event) {
		if (event.isCreate()) {
			javaBaseTypeRegistry.registerJavaBaseType(event.getNextObjectOrFail());
		} else {
			// JavaBaseType is a representation of Java class, and, as such, has no need in being deregistered,
			// so we only react to the creation.
		}
	}
}
