package tk.labyrinth.pandora.stores.extra.valuewrapper;

import io.vavr.Lazy;
import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.common.ConversionPriorities;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Set;

@LazyComponent
@Priority(ConversionPriorities.VALUE_WRAPPER_PRIORITY)
@RequiredArgsConstructor
public class ToValueWrapperConverter implements ConditionalGenericConverter {

	@SmartAutowired
	private Lazy<ConverterRegistry> converterRegistryLazy;

	@SmartAutowired
	private Lazy<JavaBaseTypeRegistry> javaBaseTypeRegistryLazy;

	@Override
	public ValueWrapper convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		ValueWrapper result;
		{
			JavaBaseType javaBaseType = javaBaseTypeRegistryLazy.get().getJavaBaseType(source.getClass());
			ConverterRegistry converterRegistry = converterRegistryLazy.get();
			//
			result = switch (javaBaseType.computeDatatypeKind()) {
				case LIST -> {
					List<?> list;
					if (source instanceof List<?> vavrList) {
						list = vavrList;
					} else {
						list = converterRegistry.convert(source, List.class);
					}
					//
					yield ListValueWrapper.of(list.map(element ->
							converterRegistry.convert(element, ValueWrapper.class)));
				}
				case OBJECT -> ObjectValueWrapper.of(converterRegistry.convert(source, GenericObject.class));
				case SIMPLE -> SimpleValueWrapper.of(converterRegistry.convert(source, String.class));
				case VALUE -> converterRegistry.convert(source, ValueWrapper.class);
			};
		}
		return result;
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return null;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return ValueWrapper.class.isAssignableFrom(targetType.getType());
	}
}
