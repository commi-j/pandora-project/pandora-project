package tk.labyrinth.pandora.stores.domain.storeroute;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.listener.GenericObjectChangeListener;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.objectcache.GenericObjectCache;
import tk.labyrinth.pandora.stores.objectcache.GenericObjectCacheRegistry;
import tk.labyrinth.pandora.stores.objectcache.InMemoryGenericObjectCache;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class StoreRouteCacheManager implements GenericObjectChangeListener {

	private final BeanContext beanContext;

	private final GenericObjectCacheRegistry genericObjectCacheRegistry;

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private GenericObjectCache cache;

	private CodeObjectModelReference storeRouteModelReference;

	@PostConstruct
	private void postConstruct() {
		storeRouteModelReference = javaBaseTypeRegistry.getNormalizedObjectModelReference(StoreRoute.class);
		//
		cache = beanContext.getBeanFactory()
				.withBean(GenericObject.of(
						RootObjectUtils.createModelReferenceAttribute(
								storeRouteModelReference)))
				.getBean(InMemoryGenericObjectCache.class);
		//
		genericObjectCacheRegistry.registerCache(cache);
	}

	@Override
	public void onGenericObjectChange(ObjectChangeEvent<GenericObject> event) {
		if (event.hasNextObject()) {
			GenericObject nextObject = event.getNextObjectOrFail();
			//
			if (Objects.equals(PandoraRootUtils.getModelReference(nextObject), storeRouteModelReference)) {
				cache.put(nextObject);
			}
		}
		// TODO: Support deletion.
	}
}
