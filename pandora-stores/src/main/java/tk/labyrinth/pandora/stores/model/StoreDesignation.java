package tk.labyrinth.pandora.stores.model;

public enum StoreDesignation {
	COMMON,
	ROOT,
	SETUP,
	TEMPORAL,
}
