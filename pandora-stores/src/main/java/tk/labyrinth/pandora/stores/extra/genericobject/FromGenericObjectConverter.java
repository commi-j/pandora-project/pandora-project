package tk.labyrinth.pandora.stores.extra.genericobject;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.Lazy;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import tk.labyrinth.pandora.datatypes.common.ConversionPriorities;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.extra.objectproxy.ObjectProxyHandler;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@LazyComponent
@Priority(ConversionPriorities.GENERIC_OBJECT_PRIORITY)
@RequiredArgsConstructor
public class FromGenericObjectConverter implements ConditionalGenericConverter {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final ObjectProvider<ObjectMapper> objectMapperProvider;

	private final ObjectProxyHandler objectProxyHandler;

	private Lazy<ObjectMapper> objectMapperLazy;

	@PostConstruct
	private void postConstruct() {
		objectMapperLazy = Lazy.of(() -> objectMapperProvider.getObject()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false));
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		Object result;
		{
			ObjectMapper objectMapper = objectMapperLazy.get();
			//
			if (!Objects.equals(sourceType, targetType)) {
				GenericObject object = (GenericObject) source;
				//
				Class<?> javaClass = Optional.ofNullable(PandoraRootUtils.findModelReference(object))
						.map(javaBaseTypeRegistry::findJavaBaseType)
						.map(JavaBaseType::resolveClass)
						.orElse(null);
				//
				if (javaClass != null) {
					Class<?> targetClass = TypeUtils.getClass(targetType.getType());
					//
					// FIXME: If everything works with this section commented, remove it.
//						if (targetClass.getTypeParameters().length > 0) {
//							throw new IllegalArgumentException("Parameterization not supported: %s".formatted(
//									targetClass));
//						}
					if (!targetClass.isAssignableFrom(javaClass)) {
						throw new IllegalArgumentException(
								"Require javaClass to be assignable to targetClass: javaClass = %s, targetClass = %s"
										.formatted(javaClass, targetClass));
					}
					//
					if (javaClass.isInterface()) {
						result = objectProxyHandler.wrap(object, javaClass);
					} else {
						result = objectMapper.convertValue(
								object,
								objectMapper.constructType(targetType.getResolvableType().getType()));
					}
				} else {
					result = objectMapper.convertValue(
							object,
							objectMapper.constructType(targetType.getResolvableType().getType()));
				}
			} else {
				// FIXME: Are we sure we want to handle such cases?
				result = source;
			}
		}
		return result;
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return null;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		{
			// FIXME: This is a crutch because Spring conversion system does not support proper converter ordering.
			//  See ToValueWrapperConverter.
			if (ValueWrapper.class.isAssignableFrom(targetType.getType())) {
				return false;
			}
		}
		return sourceType.getType() == GenericObject.class;
	}
}
