package tk.labyrinth.pandora.stores.objectmodel;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.Objects;

@Deprecated
@LazyComponent
@RequiredArgsConstructor
public class ObjectModelRegistryOld {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final ObjectModelFactory objectModelFactory;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@Nullable
	public ObjectModel findModelForJavaClass(Class<?> javaClass) {
		return objectModelSearcher.findSingle(javaBaseTypeRegistry.getNormalizedObjectModelReference(javaClass));
	}

	// FIXME: This is a temporal solution, need to find way to dynamically add models from classes.
	public ObjectModel findOrCreateModelForJavaClass(Class<?> javaClass) {
		ObjectModel result;
		{
			ObjectModel objectModel = objectModelSearcher.findSingle(
					javaBaseTypeRegistry.getNormalizedObjectModelReference(javaClass));
			if (objectModel != null) {
				result = objectModel;
			} else {
				result = objectModelFactory.createModelFromJavaClass(javaClass);
			}
		}
		return result;
	}

	public ObjectModel getModelForJavaClass(Class<?> javaClass) {
		ObjectModel result = findModelForJavaClass(javaClass);
		Objects.requireNonNull(result, "javaClass = %s".formatted(javaClass));
		return result;
	}
}
