package tk.labyrinth.pandora.stores.datatypes.polymorphic;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicLinkIndex;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

@LazyComponent
@RequiredArgsConstructor
public class PolymorphicLinkRegistry {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@SmartAutowired
	private TypedObjectSearcher<PolymorphicLinkIndex> polymorphicLinkIndexSearcher;

	public List<PolymorphicLinkIndex> getIndicesForRootDatatype(AliasDatatypeBaseReference datatypeBaseReference) {
		List<PolymorphicLinkIndex> result;
		{
			DatatypeBase datatypeBase = datatypeBaseRegistry.findDatatypeBase(datatypeBaseReference);
			//
			if (datatypeBase != null) {
				result = polymorphicLinkIndexSearcher.search(PlainQuery.builder()
						.predicate(Predicates.in(
								PolymorphicLinkIndex.ROOT_REFERENCE_ATTRIBUTE_NAME,
								datatypeBase.getAliases().map(AliasDatatypeBaseReference::of).asJava()))
						.build());
			} else {
				result = List.empty();
			}
		}
		return result;
	}
}
