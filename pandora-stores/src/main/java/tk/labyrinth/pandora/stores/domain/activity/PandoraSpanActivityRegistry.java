package tk.labyrinth.pandora.stores.domain.activity;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.telemetry.StaticTelemetryProvider;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanCache;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

// TODO: This can be moved to OTEL module.
// TODO: See StaticTracer.
@Bean
@RequiredArgsConstructor
public class PandoraSpanActivityRegistry {

	private final SpanCache spanCache;

	private final Tracer tracer = StaticTelemetryProvider.getTracer();

	public Void withinSpan(String spanName, Consumer<PandoraSpanContext> function) {
		return withinSpan(
				spanName,
				spanContext -> {
					function.accept(spanContext);
					//
					return null;
				});
	}

	public <T> T withinSpan(String spanName, Function<PandoraSpanContext, T> function) {
		Span span = tracer.spanBuilder(spanName).startSpan();
		//
		try {
			return function.apply(PandoraSpanContext.of(this, span));
		} finally {
			span.end();
		}
	}

	public Void withinSpan(String spanName, Runnable function) {
		return withinSpan(
				spanName,
				spanContext -> {
					function.run();
					//
					return null;
				});
	}

	public <T> T withinSpan(String spanName, Supplier<T> function) {
		return withinSpan(
				spanName,
				spanContext -> {
					return function.get();
				});
	}
}
