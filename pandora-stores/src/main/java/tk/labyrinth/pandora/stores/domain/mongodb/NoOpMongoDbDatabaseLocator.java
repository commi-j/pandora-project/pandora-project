package tk.labyrinth.pandora.stores.domain.mongodb;

import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;

public class NoOpMongoDbDatabaseLocator implements MongoDbDatabaseLocator {

	@Override
	public Pair<GenericObject, @Nullable String> locateDatabase(
			StoreConfiguration storeConfiguration,
			GenericObject object) {
		return Pair.of(object, null);
	}

	@Override
	public DatabaseContext locateDatabase(StoreConfiguration storeConfiguration, @Nullable Predicate predicate) {
		return DatabaseContext.of(null, List.empty(), predicate);
	}
}
