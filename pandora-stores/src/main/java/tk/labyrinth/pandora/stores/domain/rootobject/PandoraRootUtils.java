package tk.labyrinth.pandora.stores.domain.rootobject;

import lombok.NonNull;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;

import java.util.Objects;

public class PandoraRootUtils {

	// TODO: This is applied to hardcoded startup entires before ObjectModels are created.
	//  Probably we want to use special store for hardcoded entries that will not require signatures?
	public static GenericObjectAttribute createExplicitSignatureAttributeNameAttribute(
			String explicitSignatureAttributeName) {
		return GenericObjectAttribute.ofSimple(
				PandoraRootConstants.EXPLICIT_SIGNATURE_ATTRIBUTE_NAME_ATTRIBUTE_NAME,
				explicitSignatureAttributeName);
	}

	public static GenericObjectAttribute createModelReferenceAttribute(ObjectModel objectModel) {
		return createModelReferenceAttribute(objectModel.getCode());
	}

	public static GenericObjectAttribute createModelReferenceAttribute(String modelCode) {
		return GenericObjectAttribute.ofSimple(
				PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME,
				CodeObjectModelReference.of(modelCode).toString());
	}

	@Nullable
	public static CodeObjectModelReference findModelReference(@NonNull GenericObject object) {
		val modelReferenceString = object.findAttributeValueAsString(PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME);
		//
		return modelReferenceString != null ? CodeObjectModelReference.from(modelReferenceString) : null;
	}

	@Nullable
	public static String findSignatureAttributeName(ObjectModel objectModel) {
		ObjectModelAttribute signatureAttribute = objectModel.getAttributes()
				.filter(attribute -> attribute.hasFeature(SignatureObjectModelAttributeFeature.class))
				.getOrNull();
		//
		return signatureAttribute != null ? signatureAttribute.getName() : null;
	}

	public static CodeObjectModelReference getModelReference(GenericObject object) {
		return Objects.requireNonNull(
				findModelReference(object),
				"Require non-null object.modelReference: %s".formatted(object));
	}
}
