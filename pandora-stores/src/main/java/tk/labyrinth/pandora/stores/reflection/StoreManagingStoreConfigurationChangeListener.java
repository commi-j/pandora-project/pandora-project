package tk.labyrinth.pandora.stores.reflection;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.domain.mongodb.MongoDbGenericObjectStore;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreRegistry;
import tk.labyrinth.pandora.stores.genericobject.SimpleInMemoryGenericObjectStore;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.model.StoreDesignation;

@Bean
@RequiredArgsConstructor
public class StoreManagingStoreConfigurationChangeListener extends ObjectChangeListener<StoreConfiguration> {

	private final GenericObjectStoreRegistry genericObjectStoreRegistry;

	private final ObjectProvider<MongoDbGenericObjectStore> mongoDbGenericObjectStoreProvider;

	private final ObjectProvider<SimpleInMemoryGenericObjectStore> simpleInMemoryGenericObjectStoreProvider;

	@Override
	protected void onObjectChange(ObjectChangeEvent<StoreConfiguration> event) {
		if (event.hasPreviousObject()) {
			// TODO: Discard previous
		}
		if (event.hasNextObject()) {
			StoreConfiguration storeConfiguration = event.getNextObject();
			//
			if (storeConfiguration.getDesignation() != StoreDesignation.ROOT) {
				genericObjectStoreRegistry.registerStore(initializeStore(storeConfiguration));
			}
		}
	}

	public GenericObjectStore initializeStore(StoreConfiguration storeConfiguration) {
		GenericObjectStore result = switch (storeConfiguration.getKind()) {
			case IN_MEMORY -> simpleInMemoryGenericObjectStoreProvider.getObject(storeConfiguration);
			case MONGO_DB -> mongoDbGenericObjectStoreProvider.getObject(storeConfiguration);
			default -> throw new NotImplementedException();
		};
		return result;
	}
}
