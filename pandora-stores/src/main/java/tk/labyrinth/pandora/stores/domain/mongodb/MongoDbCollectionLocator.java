package tk.labyrinth.pandora.stores.domain.mongodb;

import io.vavr.collection.List;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;

public interface MongoDbCollectionLocator {

	/**
	 * Returns pair of object stripped of contextual database-related information and databaseName.
	 *
	 * @param storeConfiguration non-null
	 * @param object             non-null
	 *
	 * @return non-null
	 */
	Pair<GenericObject, String> locateCollection(StoreConfiguration storeConfiguration, GenericObject object);

	/**
	 * Returns pair of object stripped of contextual database-related information and databaseName.
	 *
	 * @param storeConfiguration non-null
	 * @param predicate          any
	 *
	 * @return non-null
	 */
	CollectionContext locateCollection(StoreConfiguration storeConfiguration, @Nullable Predicate predicate);

	CodeObjectModelReference objectModelReferenceFromCollectionName(String collectionName);

	@Value(staticConstructor = "of")
	class CollectionContext {

		String collectionName;

		List<GenericObjectAttribute> implicitAttributes;

		@Nullable
		Predicate refinedPredicate;
	}
}
