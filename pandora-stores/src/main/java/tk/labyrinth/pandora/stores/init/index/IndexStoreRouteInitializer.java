package tk.labyrinth.pandora.stores.init.index;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.model.StoreDesignation;

@LazyComponent
@Priority(5)
@RequiredArgsConstructor
public class IndexStoreRouteInitializer extends GenericObjectsContributingInitializer {

	@Override
	protected List<GenericObject> contributeObjects() {
		StoreRoute storeRoute = StoreRoute.builder()
				.distance(100)
				.slug("pandora-index-store-route")
				.storeConfigurationPredicate(Predicates.equalTo(
						StoreConfiguration.DESIGNATION_ATTRIBUTE_NAME,
						StoreDesignation.TEMPORAL))
				.testPredicate(
						ObjectIndex.INDEX_PREDICATE
						// TODO: model.tags.contains(index)
//										Predicates.contains(
//												ObjectModel.TAGS_ATTRIBUTE_NAME,
//												PandoraConstants.TEMPORAL_MODEL_TAG_NAME)
				)
				.build();
		//
		return List.of(convertAndPostProcess(
				storeRoute,
				genericObject -> genericObject.withAddedAttributes(
						PandoraRootUtils.createExplicitSignatureAttributeNameAttribute(StoreRoute.SLUG_ATTRIBUTE_NAME),
						PandoraHardcodedConstants.ATTRIBUTE)));
	}
}
