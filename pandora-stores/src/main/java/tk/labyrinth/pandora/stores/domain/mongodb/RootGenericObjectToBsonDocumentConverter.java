package tk.labyrinth.pandora.stores.domain.mongodb;

import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class RootGenericObjectToBsonDocumentConverter {

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	public Document convert(CodeObjectModelReference objectModelReference, GenericObject source) {
		Document result = new Document();
		{
			ObjectModel objectModel = objectModelReference != null
					? objectModelSearcher.findSingle(objectModelReference)
					: null;
			//
			source.getAttributes().forEach(attribute -> {
				ObjectModelAttribute objectModelAttribute = objectModel != null
						? objectModel.getAttributes()
						.filter(attributeElement -> Objects.equals(attributeElement.getName(), attribute.getName()))
						.getOrNull()
						: null;
				//
				Object convertedValue = convertValue(objectModelAttribute, attribute.getValue());
				//
				result.put(attribute.getName(), convertedValue);
			});
		}
		return result;
	}

	public static Object convertValue(@Nullable ObjectModelAttribute objectModelAttribute, ValueWrapper value) {
		Object result;
		{
			if (objectModelAttribute != null) {
				if (Objects.equals(
						objectModelAttribute.getDatatype().toString(),
						MongoDbDatatypeConstants.DATE_DATATYPE.toString())) {
					throw new NotImplementedException();
				} else if (Objects.equals(
						objectModelAttribute.getDatatype().toString(),
						MongoDbDatatypeConstants.OBJECT_ID_DATATYPE.toString())) {
					result = new ObjectId(value.asSimple().unwrap());
				} else {
					result = value.unwrap();
				}
			} else {
				result = value.unwrap();
			}
		}
		return result;
	}
}
