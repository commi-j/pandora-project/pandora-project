package tk.labyrinth.pandora.context.converter;

import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.stores.PandoraStoresModule;

@SpringBootTest(classes = PandoraStoresModule.class)
class ConverterRegistryTest {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Test
	void testConvertGenericObjectToBsonDocument() {
		Assertions
				.assertThat(converterRegistry.convert(
						GenericObject.of(
								GenericObjectAttribute.ofSimple("foo", "bar"),
								GenericObjectAttribute.ofObject("qwe", GenericObject.ofSingleSimple("aze", "rty"))),
						Document.class))
				.isEqualTo(new Document()
						.append("foo", "bar")
						.append("qwe", new Document("aze", "rty")));
	}
}
