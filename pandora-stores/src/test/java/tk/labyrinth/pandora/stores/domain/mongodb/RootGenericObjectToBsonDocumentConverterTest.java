package tk.labyrinth.pandora.stores.domain.mongodb;

import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;

import java.util.Map;

@SpringBootTest
class RootGenericObjectToBsonDocumentConverterTest {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Test
	void testConvert() {
		Assertions.assertEquals(
				new Document(Map.of(
						"attribute", "value",
						"uid", "blah")),
				converterRegistry.convert(
						GenericObject.of(
								GenericObjectAttribute.ofSimple("attribute", "value"),
								GenericObjectAttribute.ofSimple("uid", "blah")),
						Document.class));
	}
}
