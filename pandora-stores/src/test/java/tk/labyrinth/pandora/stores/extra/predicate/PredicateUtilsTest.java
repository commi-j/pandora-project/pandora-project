package tk.labyrinth.pandora.stores.extra.predicate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;

class PredicateUtilsTest {

	@Test
	void testParseWithPrefixNotation() {
		org.assertj.core.api.Assertions
				.assertThat(PredicateUtils.parseWithPrefixNotation("IN ( a [b, c] )"))
				.isEqualTo(Predicates.in("a", "b", "c"));
		Assertions.assertEquals(
				Predicates.and(
						Predicates.equalTo("a", "b"),
						Predicates.equalTo("c", "d"),
						Predicates.or(
								Predicates.and(
										Predicates.greaterThanOrEqualTo("e", "0"),
										Predicates.lessThan("e", "1")),
								Predicates.in("year", "2020", "2021"))),
				PredicateUtils.parseWithPrefixNotation(
						"AND ( EQUAL_TO ( a b ) , EQUAL_TO ( c d ) , OR ( AND ( GREATER_THAN_OR_EQUAL_TO ( e 0 ) , LESS_THAN ( e 1 ) ) , IN ( year [2020, 2021] ) ) )"));
	}

	@Test
	void testRenderWithInfixJunction() {
		Assertions.assertEquals(
				"a EQUAL_TO b AND c EQUAL_TO d AND ( ( e GREATER_THAN_OR_EQUAL_TO 0 AND e LESS_THAN 1 ) OR year IN [2020, 2021] )",
				PredicateUtils.renderWithInfixJunctions(
						Predicates.and(
								Predicates.equalTo("a", "b"),
								Predicates.equalTo("c", "d"),
								Predicates.or(
										Predicates.and(
												Predicates.greaterThanOrEqualTo("e", 0),
												Predicates.lessThan("e", 1)),
										Predicates.in("year", 2020, 2021))),
						false));
	}

	@Test
	void testRenderWithPrefixNotation() {
		Assertions.assertEquals(
				"AND ( EQUAL_TO ( a b ) , EQUAL_TO ( c d ) , OR ( AND ( GREATER_THAN_OR_EQUAL_TO ( e 0 ) , LESS_THAN ( e 1 ) ) , IN ( year [2020, 2021] ) ) )",
				PredicateUtils.renderWithPrefixNotation(
						Predicates.and(
								Predicates.equalTo("a", "b"),
								Predicates.equalTo("c", "d"),
								Predicates.or(
										Predicates.and(
												Predicates.greaterThanOrEqualTo("e", 0),
												Predicates.lessThan("e", 1)),
										Predicates.in("year", 2020, 2021)))));
	}
}
