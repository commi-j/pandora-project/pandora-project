package tk.labyrinth.pandora.stores.extra.predicate;

import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.operator.StringOperator;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.predicate.PropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;

@SpringBootTest
class PredicateToolTest {

	@Autowired
	private PredicateTool predicateTool;

	@Test
	void testIncludesWithAlphaEqVsConjunctionOfBetaEqAndGammaEq() {
		Assertions.assertFalse(predicateTool.isIncludedBy(
				Predicates.equalTo("alpha", "a"),
				Predicates.and(
						Predicates.equalTo("beta", "b"),
						Predicates.equalTo("gamma", "g"))));
	}

	@Test
	void testIncludesWithConjunctionOfAlphaEqAAndBetaEqVsAlphaEqA() {
		Assertions.assertTrue(predicateTool.isIncludedBy(
				Predicates.and(
						Predicates.equalTo("alpha", "a"),
						Predicates.equalTo("beta", "b")),
				Predicates.equalTo("alpha", "a")));
	}

	@Test
	void testIncludesWithConjunctionOfAlphaEqAAndBetaEqVsAlphaEqB() {
		Assertions.assertFalse(predicateTool.isIncludedBy(
				Predicates.and(
						Predicates.equalTo("alpha", "a"),
						Predicates.equalTo("beta", "b")),
				Predicates.equalTo("alpha", "b")));
	}

	@Test
	void testIncludesWithConjunctionOfAlphaEqAndBetaEqVsGammaEq() {
		Assertions.assertFalse(predicateTool.isIncludedBy(
				Predicates.and(
						Predicates.equalTo("alpha", "a"),
						Predicates.equalTo("beta", "b")),
				Predicates.equalTo("gamma", "g")));
	}

	@Test
	void testIntersectsWithAlphaEqAVsAlphaEqB() {
		Assertions.assertFalse(predicateTool.intersects(
				Predicates.equalTo("alpha", "a"),
				Predicates.equalTo("alpha", "b")));
	}

	@Test
	void testIntersectsWithAlphaEqAVsAlphaInAAndNull() {
		Assertions.assertTrue(predicateTool.intersects(
				Predicates.equalTo("alpha", "a"),
				Predicates.in("alpha", "a", null)));
	}

	@Test
	void testIntersectsWithAlphaEqVsConjunctionOfBetaEqAndGammaEq() {
		Assertions.assertTrue(predicateTool.intersects(
				Predicates.equalTo("alpha", "a"),
				Predicates.and(
						Predicates.equalTo("beta", "b"),
						Predicates.equalTo("gamma", "g"))));
	}

	@Disabled("Do we need this one?")
	@Test
	void testIsIncludedByWithEqVsIn() {
		Assertions.assertTrue(predicateTool.isIncludedBy(
				Predicates.equalTo("a", "b"),
				Predicates.in("a", "b", null)));
		// x = y VS x in (y, null)
	}

	@Test
	void testNormalizePredicateWithDisjunction() {
		predicateTool.normalizePredicate(Predicates.or(
				Predicates.greaterThanOrEqualTo("a", 12),
				Predicates.and(
						Predicates.equalTo("a", null),
						Predicates.greaterThanOrEqualTo("b", 12))));
	}

	@Test
	void testResolveStringRelationWithContains() {
		org.assertj.core.api.Assertions
				.assertThat(PredicateTool.resolveStringRelation(
						new ObjectPropertyPredicate(ObjectOperator.EQUAL_TO, "foo", "bar"),
						new StringPropertyPredicate(true, StringOperator.CONTAINS, "foo", "b")))
				.isEqualTo(SetRelation.SUBSET);
		org.assertj.core.api.Assertions
				.assertThat(PredicateTool.resolveStringRelation(
						new ObjectPropertyPredicate(ObjectOperator.EQUAL_TO, "foo", "bar"),
						new StringPropertyPredicate(true, StringOperator.CONTAINS, "foo", "B")))
				.isEqualTo(SetRelation.DISJOINT);
		org.assertj.core.api.Assertions
				.assertThat(PredicateTool.resolveStringRelation(
						new ObjectPropertyPredicate(ObjectOperator.EQUAL_TO, "foo", "bar"),
						new StringPropertyPredicate(false, StringOperator.CONTAINS, "foo", "B")))
				.isEqualTo(SetRelation.SUBSET);
	}

	@Test
	void testToCanonicalForm() {
		Assertions.assertEquals(
				MintermCanonicalForm.of(List.of(
						Minterm.of(List.of(
								(PropertyPredicate<?>) Predicates.equalTo("a", 12),
								(PropertyPredicate<?>) Predicates.equalTo("b", 14))),
						Minterm.of(List.of(
								(PropertyPredicate<?>) Predicates.equalTo("a", 12),
								(PropertyPredicate<?>) Predicates.greaterThanOrEqualTo("c", 14))))),
				predicateTool.toCanonicalForm(
						Predicates.and(
								Predicates.equalTo("a", 12),
								Predicates.or(
										Predicates.equalTo("b", 14),
										Predicates.greaterThanOrEqualTo("c", 14)))));
	}

	@Test
	void testToCanonicalFormWithConjunctionOfSameAttribute() {
		Assertions.assertEquals(
				MintermCanonicalForm.of(List.of(
						Minterm.of(List.of(
								(PropertyPredicate<?>) Predicates.equalTo("a", 12),
								(PropertyPredicate<?>) Predicates.greaterThanOrEqualTo("b", 10),
								(PropertyPredicate<?>) Predicates.lessThan("b", 20))))),
				predicateTool.toCanonicalForm(
						Predicates.and(
								Predicates.equalTo("a", 12),
								Predicates.and(
										Predicates.greaterThanOrEqualTo("b", 10),
										Predicates.lessThan("b", 20)))));
	}

	@Test
	void testToCanonicalFormWithDisjunctionOfSameAttribute() {
		Assertions.assertEquals(
				MintermCanonicalForm.of(List.of(
						Minterm.of(List.of(
								(PropertyPredicate<?>) Predicates.equalTo("a", 12),
								(PropertyPredicate<?>) Predicates.lessThan("b", 10))),
						Minterm.of(List.of(
								(PropertyPredicate<?>) Predicates.equalTo("a", 12),
								(PropertyPredicate<?>) Predicates.greaterThanOrEqualTo("b", 20))))),
				predicateTool.toCanonicalForm(
						Predicates.and(
								Predicates.equalTo("a", 12),
								Predicates.or(
										Predicates.lessThan("b", 10),
										Predicates.greaterThanOrEqualTo("b", 20)))));
	}
}
