package tk.labyrinth.pandora.stores.datatypes.javabasetype;

import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeParameterUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;

@SpringBootTest
class DatatypeRegistrationContributingJavaBaseTypeChangeListenerTest {

	@SmartAutowired
	private TypedObjectSearcher<DatatypeBase> datatypeBaseSearcher;

	@Test
	void testDatatypeBaseDatatypeHasAllRegistrationsProperlyDone() {
		Assertions
				.assertThat(datatypeBaseSearcher
						.getSingle(JavaBaseTypeUtils.createDatatypeBaseReference(DatatypeBase.class))
						.getAliases())
				.isEqualTo(List.of(
						"objectmodel:datatypebase",
						"javabasetype:tk.labyrinth.pandora.datatypes.domain.datatypebase:DatatypeBase"));
	}

	@Test
	void testPairDatatypeHasAllRegistrationsProperlyDone() {
		Assertions
				.assertThat(datatypeBaseSearcher
						.getSingle(JavaBaseTypeUtils.createDatatypeBaseReference(Pair.class))
						.getAliases())
				.isEqualTo(List.of(
						"javabasetype:org.apache.commons.lang3.tuple:Pair",
						"objectmodel:javabasetype:org.apache.commons.lang3.tuple:Pair"));
		Assertions
				.assertThat(datatypeBaseSearcher
						.getSingle(JavaBaseTypeParameterUtils.createDatatypeBaseReference(Pair.class.getTypeParameters()[0]))
						.getAliases())
				.isEqualTo(List.of(
						"javabasetypeparameter:org.apache.commons.lang3.tuple:Pair%L",
						"objectmodelparameter:javabasetype:org.apache.commons.lang3.tuple:Pair%L"));
		Assertions
				.assertThat(datatypeBaseSearcher
						.getSingle(JavaBaseTypeParameterUtils.createDatatypeBaseReference(Pair.class.getTypeParameters()[1]))
						.getAliases())
				.isEqualTo(List.of(
						"javabasetypeparameter:org.apache.commons.lang3.tuple:Pair%R",
						"objectmodelparameter:javabasetype:org.apache.commons.lang3.tuple:Pair%R"));
	}
}
