package tk.labyrinth.pandora.stores.domain.mongodb;

import de.bwaldvogel.mongo.bson.Document;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

import java.util.UUID;
import java.util.stream.Stream;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DefaultMongoDbCollectionLocatorTest {

	@Autowired
	private GenericObjectManipulator genericObjectManipulator;

	@Autowired
	private GenericObjectSearcher genericObjectSearcher;

	@SmartAutowired
	private TypedObjectManipulator<ObjectModel> objectModelManipulator;

	@Autowired
	private PandoraTestMongoDbServer testMongoDbServer;

	@BeforeAll
	private void beforeAll() {
		objectModelManipulator.createWithAdjustment(
				MongoDbTestObjects.OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME,
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		//
		testMongoDbServer.getDatabase().createCollectionOrThrowIfExists("explicit-collection");
	}

	@Test
	void testCreateWithExplicitCollectionName() {
		UUID uid = UUID.randomUUID();
		//
		genericObjectManipulator.create(GenericObject.of(
				GenericObjectAttribute.ofUuid("uid", uid),
				PandoraRootUtils.createModelReferenceAttribute(MongoDbTestObjects.OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME)));
		//
		List<Document> documents = List.ofAll(testMongoDbServer.getDatabase()
				.resolveCollection("explicit-collection", true)
				.handleQuery(new Document("_id", uid.toString())));
		//
		Assertions
				.assertThat(MongoDbTestObjects.OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME.getCode())
				.isEqualTo("OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME");
		Assertions
				.assertThat(documents)
				.isEqualTo(List.of(new Document("_id", uid.toString())));
	}

	@Test
	void testSearchWithExplicitCollectionName() {
		testMongoDbServer.getDatabase()
				.resolveCollection("explicit-collection", true)
				.addDocuments(Stream.of(
						new Document()
								.append("_id", "foo")
								.append("search", "true"),
						new Document()
								.append("_id", "bar")
								.append("search", "true")));
		//
		Assertions
				.assertThat(genericObjectSearcher.search(
						CodeObjectModelReference.from(MongoDbTestObjects.OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME),
						Predicates.equalTo("search", true)))
				.isEqualTo(List.of(
						GenericObject.of(
								PandoraRootUtils.createModelReferenceAttribute(
										MongoDbTestObjects.OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME),
								GenericObjectAttribute.ofSimple("search", "true"),
								GenericObjectAttribute.ofSimple("uid", "foo")),
						GenericObject.of(
								PandoraRootUtils.createModelReferenceAttribute(
										MongoDbTestObjects.OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME),
								GenericObjectAttribute.ofSimple("search", "true"),
								GenericObjectAttribute.ofSimple("uid", "bar"))));
	}
}
