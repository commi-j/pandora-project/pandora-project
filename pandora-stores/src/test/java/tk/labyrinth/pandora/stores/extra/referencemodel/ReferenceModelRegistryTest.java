package tk.labyrinth.pandora.stores.extra.referencemodel;

import io.vavr.collection.List;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ModelCodeAndAttributeNamesReferenceModelReference;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ReferenceModel;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

@SpringBootTest
class ReferenceModelRegistryTest {

	@Autowired
	private ReferenceModelRegistry referenceModelRegistry;

	@Test
	void testCreateAttributePredicateForReferenceModel() {
		Assertions
				.assertThat(referenceModelRegistry.createAttributePredicateForReferenceModel(
						ModelCodeAndAttributeNamesReferenceModelReference.of("someobject", List.of("foo", "bar"))))
				.isEqualTo(Predicates.and(
						Predicates.notEqualTo("foo", null),
						Predicates.notEqualTo("bar", null)));
	}

	@Test
	void testCreateReferenceModel() {
		Assertions
				.assertThat(referenceModelRegistry.createReferenceModel(
						TypeUtils.parameterize(UidReference.class, ObjectModel.class)))
				.isEqualTo(ReferenceModel.builder()
						.attributeNames(List.of("uid"))
						.classSignature(ClassSignature.of(UidReference.class))
						.modelCode("objectmodel")
						.name("UidReference<ObjectModel>")
						.build());
	}

	@Test
	void testGetReferenceModel() {
		Assertions
				.assertThat(referenceModelRegistry.getReferenceModel(
						TypeUtils.parameterize(Reference.class, DatatypeBase.class)))
				.isEqualTo(ReferenceModel.builder()
						.attributeNames(List.of("signature"))
						.modelCode("datatypebase")
						.name("DatatypeBasePrimaryReference")
						.build());
	}
}
