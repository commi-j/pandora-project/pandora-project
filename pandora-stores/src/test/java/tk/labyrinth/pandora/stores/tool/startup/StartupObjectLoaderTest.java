package tk.labyrinth.pandora.stores.tool.startup;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;

@SpringBootTest
// TODO: Support both file and classpath sources.
//@TestPropertySource(properties = StartupObjectLoader.STARTUP_OBJECT_FOLDERS_PROPERTY + ":" + "src/test/data/startupobjects")
@TestPropertySource(properties = StartupObjectLoader.STARTUP_OBJECT_DIRECTORIES_PROPERTY + ":" + "classpath:startupobjects")
class StartupObjectLoaderTest {

	@Test
	void testRunWithClasspathFolder(@Autowired GenericObjectSearcher genericObjectSearcher) {
		Assertions.assertEquals(
				1,
				genericObjectSearcher.count(ParameterizedQuery.<CodeObjectModelReference>builder()
						.parameter(CodeObjectModelReference.of(ObjectModel.MODEL_CODE))
						.predicate(Predicates.equalTo(StartupConstants.STARTUP_ATTRIBUTE_NAME, true))
						.build()));
		Assertions.assertEquals(
				2,
				genericObjectSearcher.count(ParameterizedQuery.<CodeObjectModelReference>builder()
						.parameter(CodeObjectModelReference.of("startupobject"))
						.build()));
		Assertions.assertEquals(
				1,
				genericObjectSearcher.count(ParameterizedQuery.<CodeObjectModelReference>builder()
						.parameter(CodeObjectModelReference.of(JavaBaseType.MODEL_CODE))
						.predicate(Predicates.equalTo(StartupConstants.STARTUP_ATTRIBUTE_NAME, true))
						.build()));
	}

	@ExtendWithSpring
	@Test
	void testRunWithFileFolder(ApplicationContext applicationContext) {
		// TODO
	}
}
