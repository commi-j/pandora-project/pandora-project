package tk.labyrinth.pandora.stores.extra.javabasetype;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

@Deprecated // We now have DatatypeRegistrations.
@SpringBootTest
class DatatypeBaseContributingJavaBaseTypeChangeListenerTest {

	@Autowired
	private DatatypeBaseRegistry datatypeBaseRegistry;

	@SmartAutowired
	private TypedObjectManipulator<JavaBaseType> javaBaseTypeManipulator;

	@Disabled("JBT is a bad example since it should never be changed (unless we talk about some lib reloading?)")
	@Test
	void testOnObjectChangeWorksWithCreateAndUpdateAndDelete() {
		JavaBaseType javaBaseType = JavaBaseType.builder()
				.implicitHints(List.of(JavaBaseTypeHint.SIMPLE))
				.signature(TypeSignature.of(PandoraHardcodedConstants.class))
				.build();
		SignatureJavaBaseTypeReference javaBaseTypeReference = SignatureJavaBaseTypeReference.from(javaBaseType);
		//
		javaBaseTypeManipulator.createWithAdjustment(javaBaseType, PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		javaBaseTypeManipulator.updateWithAdjustment(
				javaBaseType.withImplicitHints(List.empty()),
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		//
		AliasDatatypeBaseReference datatypeBaseReference = JavaBaseTypeUtils.createDatatypeBaseReference(
				javaBaseTypeReference);
		//
		Assertions
				.assertThat(datatypeBaseRegistry.findDatatypeBase(datatypeBaseReference))
				.isNotNull();
		//
		javaBaseTypeManipulator.delete(javaBaseTypeReference);
		//
		Assertions
				.assertThat(datatypeBaseRegistry.findDatatypeBase(datatypeBaseReference))
				.isNull();
	}
}
