package tk.labyrinth.pandora.stores.datatypes.datatype;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeParameterUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.testdata.ClassWithTypeParameter;

@SpringBootTest
class DatatypeRegistryTest {

	@Autowired
	private DatatypeRegistry datatypeRegistry;

	@Autowired
	private JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	@Test
	void testResolveDatatype() {
		javaBaseTypeRegistry.registerJavaClass(ClassWithTypeParameter.class);
		//
		Assertions
				.assertThat(datatypeRegistry.resolveDatatype(
						Datatype.builder()
								.baseReference(ObjectModelUtils.createDatatypeBaseReference(JavaBaseTypeUtils.createDatatypeAlias(
										TypeSignature.ofValid("tk.labyrinth.pandora.stores.testdata:ClassWithTypeParameter"))))
								.parameters(List.of(Datatype.ofNonParameterized(JavaBaseTypeUtils.createDatatypeBaseReference(
										TypeSignature.ofValid("tk.labyrinth.pandora.datatypes.domain.datatype:Datatype")))))
								.build(),
						Datatype.builder()
								.baseReference(JavaBaseTypeUtils.createDatatypeBaseReference(
										TypeSignature.ofValid("tk.labyrinth.pandora.datatypes.domain.reference:Reference")))
								.parameters(List.of(Datatype.ofNonParameterized(JavaBaseTypeParameterUtils.createDatatypeBaseReference(
										TypeDescription.of("tk.labyrinth.pandora.stores.testdata:ClassWithTypeParameter%T")))))
								.build()))
				.isEqualTo(Datatype.builder()
						.baseReference(JavaBaseTypeUtils.createDatatypeBaseReference(
								TypeSignature.ofValid("tk.labyrinth.pandora.datatypes.domain.reference:Reference")))
						.parameters(List.of(Datatype.ofNonParameterized(JavaBaseTypeUtils.createDatatypeBaseReference(
								TypeSignature.ofValid("tk.labyrinth.pandora.datatypes.domain.datatype:Datatype")))))
						.build());
	}
}
