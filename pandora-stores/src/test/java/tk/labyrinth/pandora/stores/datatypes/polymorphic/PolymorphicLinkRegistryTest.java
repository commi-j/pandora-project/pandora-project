package tk.labyrinth.pandora.stores.datatypes.polymorphic;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicObjectModelFeature;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicLinkIndex;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectModelFeature;

@SpringBootTest
class PolymorphicLinkRegistryTest {

	@Autowired
	private PolymorphicLinkRegistry polymorphicLinkRegistry;

	@Test
	void testGetIndicesForRootDatatype() {
		Set<PolymorphicLinkIndex> expected = HashSet.of(
				PolymorphicLinkIndex.builder()
						.leafReference(ObjectModelUtils.createDatatypeBaseReference(
								JavaBaseTypeUtils.createJavaBaseTypeObjectModelCode(PolymorphicObjectModelFeature.class)))
						.qualifierAttribute(GenericObjectAttribute.ofSimple("name", "polymorphic"))
						.rootReference(JavaBaseTypeUtils.createDatatypeBaseReference(ObjectModelFeature.class))
						.build(),
				PolymorphicLinkIndex.builder()
						.leafReference(ObjectModelUtils.createDatatypeBaseReference(
								JavaBaseTypeUtils.createJavaBaseTypeObjectModelCode(RootObjectModelFeature.class)))
						.qualifierAttribute(GenericObjectAttribute.ofSimple("name", "root"))
						.rootReference(JavaBaseTypeUtils.createDatatypeBaseReference(ObjectModelFeature.class))
						.build());
		//
		Assertions
				.assertThat(polymorphicLinkRegistry
						.getIndicesForRootDatatype(JavaBaseTypeUtils.createDatatypeBaseReference(ObjectModelFeature.class))
						.toSet())
				.isEqualTo(expected);
		Assertions
				.assertThat(polymorphicLinkRegistry
						.getIndicesForRootDatatype(ObjectModelUtils.createDatatypeBaseReference(
								ReflectionObjectModelFactory.resolveModelCode(ObjectModelFeature.class)))
						.toSet())
				.isEqualTo(expected);
	}
}
