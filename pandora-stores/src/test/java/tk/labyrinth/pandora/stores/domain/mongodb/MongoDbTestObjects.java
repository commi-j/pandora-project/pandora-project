package tk.labyrinth.pandora.stores.domain.mongodb;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectModelFeature;

public class MongoDbTestObjects {

	public static ObjectModel OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME = ObjectModel.builder()
			.attributes(List.empty())
			.code("OBJECT_MODEL_WITH_EXPLICIT_COLLECTION_NAME")
			.tags(List.of(PandoraMongoDbUtils.createCollectionNameTag("explicit-collection")))
			.build();

	public static ObjectModel OBJECT_MODEL_WITH_OBJECT_ID = ObjectModel.builder()
			.attributes(List.of(ObjectModelAttribute.builder()
					.datatype(MongoDbDatatypeConstants.OBJECT_ID_DATATYPE)
					.name("_id")
					.build()))
			.code("OBJECT_MODEL_WITH_OBJECT_ID")
			.features(List.of(RootObjectModelFeature.of("_id")))
			.build();
}
