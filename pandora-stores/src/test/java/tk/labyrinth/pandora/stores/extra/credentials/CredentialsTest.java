package tk.labyrinth.pandora.stores.extra.credentials;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vavr.collection.HashMap;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

@SpringBootTest
class CredentialsTest {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testConvertFromGenericObject() {
		GenericObject basicGenericObject = GenericObject.of(
				GenericObjectAttribute.ofSimple("key", "test-key"),
				GenericObjectAttribute.ofSimple("modelReference", "objectmodel(code=credentials)"));
		Credentials basicCredentials = Credentials.builder()
				.key("test-key")
				.build();
		//
		{
			// Null attributes.
			//
			ContribAssertions.assertThrows(
					() -> converterRegistry.convert(
							basicGenericObject.withNullableAttribute("attributes", null),
							Credentials.class),
					fault -> Assertions
							.assertThat(fault.getCause().getCause().getCause().toString())
							.isEqualTo("java.lang.IllegalArgumentException: When attributes node is not present we expect to have >= 2 keys: 'key' and any other, actual = [key]"));
		}
		{
			// Empty attributes.
			//
			Assertions
					.assertThat(converterRegistry.convert(
							basicGenericObject.withNonNullAttribute("attributes", ObjectValueWrapper.empty()),
							Credentials.class))
					.isEqualTo(basicCredentials.withAttributes(HashMap.empty()));
		}
		{
			// Two attributes.
			//
			Assertions
					.assertThat(converterRegistry.convert(
							basicGenericObject.withNonNullAttribute(
									"attributes",
									ObjectValueWrapper.of(
											GenericObjectAttribute.ofSimple("password", "test-password"),
											GenericObjectAttribute.ofSimple("username", "test-username"))),
							Credentials.class))
					.isEqualTo(basicCredentials.withAttributes(HashMap.of(
							"password", "test-password",
							"username", "test-username")));
		}
	}

	@Test
	void testConvertFromGenericObjectWithExtraAttributeOfGenericObject() {
		Assertions
				.assertThat(converterRegistry.convert(
						GenericObject.of(
								GenericObjectAttribute.ofObject(
										"attributes",
										GenericObject.of(
												GenericObjectAttribute.ofSimple("password", "test-password"),
												GenericObjectAttribute.ofSimple("username", "test-username"))),
								GenericObjectAttribute.ofTrue("hardcoded"),
								GenericObjectAttribute.ofSimple("key", "test-key")),
						Credentials.class))
				.isEqualTo(Credentials.builder()
						.attributes(HashMap.of(
								"password", "test-password",
								"username", "test-username"))
						.key("test-key")
						.build());
	}

	@Test
	void testConvertToGenericObject() {
		Credentials basicCredentials = Credentials.builder()
				.key("test-key")
				.build();
		GenericObject basicGenericObject = GenericObject.of(
				GenericObjectAttribute.ofSimple("key", "test-key"),
				GenericObjectAttribute.ofSimple("modelReference", "objectmodel(code=credentials)"));
		//
		{
			// Null attributes.
			//
			Assertions
					.assertThat(converterRegistry.convert(
							basicCredentials.withAttributes(null),
							GenericObject.class))
					.isEqualTo(basicGenericObject.withNullableAttribute("attributes", null));
		}
		{
			// Empty attributes.
			//
			Assertions
					.assertThat(converterRegistry.convert(
							basicCredentials.withAttributes(HashMap.empty()),
							GenericObject.class))
					.isEqualTo(basicGenericObject
							.withNonNullAttribute("attributes", ObjectValueWrapper.empty()));
		}
		{
			// Two attributes.
			//
			Assertions
					.assertThat(converterRegistry.convert(
							basicCredentials.withAttributes(HashMap.of(
									"password", "test-password",
									"username", "test-username")),
							GenericObject.class))
					.isEqualTo(basicGenericObject.withNonNullAttribute(
							"attributes",
							ObjectValueWrapper.of(
									GenericObjectAttribute.ofSimple("password", "test-password"),
									GenericObjectAttribute.ofSimple("username", "test-username"))));
		}
	}

	@Test
	void testFromObjectNode() throws JsonProcessingException {
		Assertions
				.assertThat(Credentials
						.fromObjectNode(objectMapper.readValue(
								"""
										{
											"attributes": {
												"password": "PW",
												"username": "UN"
											},
											"key": "test-key"
										}""",
								ObjectNode.class))
						.toString())
				.isEqualTo("Credentials(attributes=HashMap((username, UN), (password, PW)), key=test-key)");
	}
}
