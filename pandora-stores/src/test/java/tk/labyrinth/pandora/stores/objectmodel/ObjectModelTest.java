package tk.labyrinth.pandora.stores.objectmodel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;

@SpringBootTest
class ObjectModelTest {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ObjectModelRegistryOld objectModelRegistry;

	/**
	 * Tests that @Jacksonized @SuperBuilder works.<br>
	 * Tests that extra attribute 'test-attribute' does not cause failure.<br>
	 */
	@Test
	void testConvertFromGenericObject() {
		Assertions.assertEquals(
				ObjectModel.builder()
						.code("test-code")
						.build(),
				converterRegistry.convert(
						GenericObject.of(
								GenericObjectAttribute.ofSimple("code", "test-code"),
								GenericObjectAttribute.ofSimple("test-attribute", "test-value")),
						ObjectModel.class));
	}

	@Test
	void testToJson() throws JsonProcessingException {
		Assertions.assertEquals(
				"""
						{
						  "attributes" : [ {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute:ObjectModelAttribute)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "attributes"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ {
						      "name" : "signature"
						    } ],
						    "name" : "code"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.objectmodel.feature:ObjectModelFeature)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "features"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "name"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "parameterNames"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "renderRule"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.tag:Tag)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "tags"
						  } ],
						  "code" : "objectmodel",
						  "features" : [ {
						    "name" : "root",
						    "key" : "code"
						  } ],
						  "name" : "ObjectModel",
						  "renderRule" : "attribute:name",
						  "tags" : [ {
						    "name" : "pandora"
						  } ]
						}""",
				objectMapper
						.writerWithDefaultPrettyPrinter()
						.writeValueAsString(objectModelRegistry.findModelForJavaClass(ObjectModel.class)));
	}
}
