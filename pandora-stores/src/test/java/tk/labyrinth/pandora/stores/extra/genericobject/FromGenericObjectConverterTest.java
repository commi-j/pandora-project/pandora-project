package tk.labyrinth.pandora.stores.extra.genericobject;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.convert.TypeDescriptor;
import tk.labyrinth.pandora.core.tool.converter.common.TypeDescriptorUtils;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

@SpringBootTest
class FromGenericObjectConverterTest {

	@Autowired
	private FromGenericObjectConverter fromGenericObjectConverter;

	@Test
	void testConvertToParameterizedType() {
		// FIXME: Use some function to acquire this value.
		GenericObjectAttribute pairModelReferenceAttribute = RootObjectUtils.createModelReferenceAttribute(
				CodeObjectModelReference.of("javabasetype:org.apache.commons.lang3.tuple:Pair"));
		//
		Assertions
				.assertThat(fromGenericObjectConverter.convert(
						GenericObject.of(
								GenericObjectAttribute.ofSimple("left", "foo"),
								GenericObjectAttribute.ofSimple("right", "12"),
								pairModelReferenceAttribute),
						TypeDescriptor.valueOf(GenericObject.class),
						TypeDescriptorUtils.from(TypeUtils.parameterize(
								Pair.class,
								String.class,
								Integer.class))))
				.isEqualTo(Pair.of("foo", 12));
		Assertions
				.assertThat(fromGenericObjectConverter.convert(
						GenericObject.of(pairModelReferenceAttribute),
						TypeDescriptor.valueOf(GenericObject.class),
						TypeDescriptorUtils.from(TypeUtils.parameterize(
								Pair.class,
								String.class,
								Integer.class))))
				.isEqualTo(Pair.of(null, null));
	}
}
