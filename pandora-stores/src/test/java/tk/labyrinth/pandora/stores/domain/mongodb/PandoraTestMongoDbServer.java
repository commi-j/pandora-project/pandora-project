package tk.labyrinth.pandora.stores.domain.mongodb;

import de.bwaldvogel.mongo.MongoDatabase;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.MongoDbStoreSettings;
import tk.labyrinth.pandora.stores.domain.storeconfiguration.StoreConfiguration;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.model.StoreDesignation;
import tk.labyrinth.pandora.stores.model.StoreKind;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

import java.net.InetSocketAddress;

@Bean
public class PandoraTestMongoDbServer {

	private final MemoryBackend memoryBackend = new MemoryBackend();

	@SmartAutowired
	private TypedObjectManipulator<StoreConfiguration> storeConfigurationManipulator;

	@SmartAutowired
	private TypedObjectManipulator<StoreRoute> storeRouteManipulator;

	@PostConstruct
	private void postConstruct() {
		MongoServer server = new MongoServer(memoryBackend);
		//
		InetSocketAddress serverAddress = server.bind();
		//
		String connectionString = "mongodb://%s:%s/%s".formatted(
				serverAddress.getHostName(),
				serverAddress.getPort(),
				"testdb");
		//
		String storeSlug = "test-store";
		//
		storeConfigurationManipulator.createWithAdjustment(
				StoreConfiguration.builder()
						.designation(StoreDesignation.COMMON)
						.kind(StoreKind.MONGO_DB)
						.mongoDbSettings(MongoDbStoreSettings.builder()
								.uri(connectionString)
								.build())
						.predicate(null)
						.slug(storeSlug)
						.build(),
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		//
		storeRouteManipulator.createWithAdjustment(
				StoreRoute.builder()
						.distance(Integer.MAX_VALUE)
						.storeConfigurationPredicate(Predicates.equalTo(
								StoreConfiguration.SLUG_ATTRIBUTE_NAME,
								storeSlug))
						.slug("%s-route".formatted(storeSlug))
						.build(),
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
	}

	public MongoDatabase getDatabase() {
		return memoryBackend.resolveDatabase("testdb");
	}
}
