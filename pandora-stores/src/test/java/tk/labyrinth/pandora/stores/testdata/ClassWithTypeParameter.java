package tk.labyrinth.pandora.stores.testdata;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ClassWithTypeParameter<T> {

	Reference<T> reference;
}
