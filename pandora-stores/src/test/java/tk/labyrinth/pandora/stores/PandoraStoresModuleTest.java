package tk.labyrinth.pandora.stores;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.ApplicationContext;

import java.util.Map;

class PandoraStoresModuleTest {

	@Test
	void testInitWithoutMongoAutoConfiguration() {
		SpringApplication application = new SpringApplication(PandoraStoresModule.class);
		application.setDefaultProperties(Map.of("server.port", "8888"));
		//
		ApplicationContext applicationContext = application.run();
		//
		Assertions.assertNull(applicationContext.getBeanProvider(MongoAutoConfiguration.class).getIfAvailable());
	}
}