package tk.labyrinth.pandora.stores.extra.valuewrapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

import java.nio.file.Path;
import java.time.LocalDate;

@SpringBootTest
class ToValueWrapperConverterTest {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Test
	void testConvertFromGenericObjectToValueWrapper() {
		GenericObject object = GenericObject.of(
				GenericObjectAttribute.ofSimple("attribute", "value"));
		ValueWrapper valueWrapper = converterRegistry.convert(object, ValueWrapper.class);
		//
		Assertions
				.assertThat(valueWrapper)
				.isEqualTo(ObjectValueWrapper.of(object));
		Assertions
				.assertThat(valueWrapper.unwrap())
				.isSameAs(object);
	}

	@Test
	void testConvertFromListToValueWrapper() {
		{
			// Java
			//
			Assertions
					.assertThat(converterRegistry.convert(java.util.List.of("a", "b"), ValueWrapper.class))
					.isEqualTo(ListValueWrapper.ofElements(
							SimpleValueWrapper.of("a"),
							SimpleValueWrapper.of("b")));
		}
		{
			// Vavr
			//
			Assertions
					.assertThat(converterRegistry.convert(
							io.vavr.collection.List.of("a", "b"),
							ValueWrapper.class))
					.isEqualTo(ListValueWrapper.ofElements(
							SimpleValueWrapper.of("a"),
							SimpleValueWrapper.of("b")));
		}
	}

	@Test
	void testConvertFromLocalDateToValueWrapper() {
		Assertions
				.assertThat(converterRegistry.convert(LocalDate.parse("2022-12-18"), ValueWrapper.class))
				.isEqualTo(SimpleValueWrapper.of("2022-12-18"));
	}

	@Test
	void testConvertFromPathToValueWrapper() {
		ValueWrapper valueWrapper = converterRegistry.convert(Path.of("foo"), ValueWrapper.class);
		//
		Assertions
				.assertThat(valueWrapper.isSimple())
				.isTrue();
	}

	@Test
	void testConvertFromSetToValueWrapper() {
		{
			// Java
			//
			Assertions
					.assertThat(converterRegistry.convert(java.util.Set.of("a", "b"), ValueWrapper.class))
					.isEqualTo(ListValueWrapper.ofElements(
							SimpleValueWrapper.of("a"),
							SimpleValueWrapper.of("b")));
		}
		{
			// Vavr
			//
			Assertions
					.assertThat(converterRegistry.convert(
							io.vavr.collection.LinkedHashSet.of("a", "b"),
							ValueWrapper.class))
					.isEqualTo(ListValueWrapper.ofElements(
							SimpleValueWrapper.of("a"),
							SimpleValueWrapper.of("b")));
		}
	}
}
