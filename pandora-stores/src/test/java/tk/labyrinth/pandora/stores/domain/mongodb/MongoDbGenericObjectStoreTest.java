package tk.labyrinth.pandora.stores.domain.mongodb;

import de.bwaldvogel.mongo.bson.Document;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreRegistry;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

@SpringBootTest
class MongoDbGenericObjectStoreTest {

	@Autowired
	private GenericObjectManipulator genericObjectManipulator;

	@Autowired
	private GenericObjectSearcher genericObjectSearcher;

	@Autowired
	private GenericObjectStoreRegistry genericObjectStoreRegistry;

	@SmartAutowired
	private TypedObjectManipulator<ObjectModel> objectModelManipulator;

	@SmartAutowired
	private TypedObjectManipulator<StoreRoute> storeRouteManipulator;

	@Autowired
	private PandoraTestMongoDbServer testMongoDbServer;

	@Test
	void testCreateAndSearchObjectModel() {
		String objectModelCode = "testmodel";
		//
		objectModelManipulator.create(ObjectModel.builder()
				.code(objectModelCode)
				.build());
		//
		List<Document> all = List.ofAll(testMongoDbServer.getDatabase()
				.resolveCollection("objectmodel", true)
				.queryAll());
		//
		Assertions
				.assertThat(all)
				.isEqualTo(List.of(new Document()
						.append("_id", objectModelCode)));
		//
		List<GenericObject> found = genericObjectSearcher.search(
				CodeObjectModelReference.of("objectmodel"),
				Predicates.equalTo("code", objectModelCode));
		//
		Assertions
				.assertThat(found)
				.isEqualTo(List.of(GenericObject.of(
						GenericObjectAttribute.ofSimple("code", objectModelCode),
						GenericObjectAttribute.ofSimple("modelReference", "objectmodel(code=objectmodel)"))));
	}

	// FIXME: We'd better use some model from pseudo-domain here, like Currency or Country.
	@Test
	void testCreateAndSearchStoreRoute() {
		String storeRouteSlug = "test-data-store-route";
		//
		storeRouteManipulator.create(StoreRoute.builder()
				.slug(storeRouteSlug)
				.build());
		//
		List<Document> documents = List.ofAll(testMongoDbServer.getDatabase()
				.resolveCollection("storeroute", true)
				.queryAll());
		//
		// 65fedcd5940c5171f0cc4dab
		Assertions
				.assertThat(documents)
				.isEqualTo(List.of(new Document()
						.append("_id", storeRouteSlug)));
		//
		List<GenericObject> found = genericObjectSearcher.search(
				CodeObjectModelReference.of("storeroute"),
				Predicates.equalTo("slug", storeRouteSlug));
		//
		Assertions
				.assertThat(found)
				.isEqualTo(List.of(GenericObject.of(
						GenericObjectAttribute.ofSimple("slug", storeRouteSlug),
						GenericObjectAttribute.ofSimple("modelReference", "objectmodel(code=storeroute)"))));
	}

	@Test
	void testCreateAndUpdateWithObjectIdPrimaryAttribute() {
		objectModelManipulator.createWithAdjustment(
				MongoDbTestObjects.OBJECT_MODEL_WITH_OBJECT_ID,
				PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
		//
		GenericObjectStore store = genericObjectStoreRegistry.getStore("test-store");
		//
		ObjectId objectId = new ObjectId();
		//
		GenericObject object = GenericObject.of(
				GenericObjectAttribute.ofInteger("i", 0),
				PandoraRootUtils.createModelReferenceAttribute(MongoDbTestObjects.OBJECT_MODEL_WITH_OBJECT_ID),
				GenericObjectAttribute.ofSimple("_id", objectId.toHexString()));
		//
		store.create(object);
		//
		List<Document> documents = List.ofAll(testMongoDbServer.getDatabase()
				.resolveCollection(MongoDbTestObjects.OBJECT_MODEL_WITH_OBJECT_ID.getCode(), true)
				.queryAll());
		//
		Assertions
				.assertThat(documents)
				.isEqualTo(List.of(new Document()
						.append("_id", new de.bwaldvogel.mongo.bson.ObjectId(objectId.toByteArray()))
						.append("i", "0")));
		//
		store.update(object.withAttribute("i", SimpleValueWrapper.of(1)));
		//
		Assertions
				.assertThat(documents)
				.isEqualTo(List.of(new Document()
						.append("_id", new de.bwaldvogel.mongo.bson.ObjectId(objectId.toByteArray()))
						.append("i", "1")));
	}
	//
	// TODO: The issue was with Predicates.in taking Collection, not Iterable, which misfire with Vavr.
//	@Test
//	void testSearchWithUidIn() {
//		String modelCode = "model-for-search-with-uid-in";
//		List<UUID> uids = List.range(0, 3).map(index -> UUID.randomUUID());
//		//
//		{
//			objectModelManipulator.createWithAdjustment(
//					ObjectModel.builder()
//							.code(modelCode)
//							.build(),
//					PandoraHardcodedConstants.ADJUSTMENT_FUNCTION);
//			//
//			uids.forEach(uid -> genericObjectManipulator.create(GenericObject.of(
//					PandoraRootUtils.createModelReferenceAttribute(modelCode)
//					GenericObjectAttribute.ofUuid("uid", uid)
//			)));
//		}
//		genericObjectSearcher.search(
//				CodeObjectModelReference.of(modelCode),
//				Predicates.in());
//	}
}
