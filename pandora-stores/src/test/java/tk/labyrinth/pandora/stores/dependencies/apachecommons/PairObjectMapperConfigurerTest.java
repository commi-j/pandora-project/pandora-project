package tk.labyrinth.pandora.stores.dependencies.apachecommons;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.core.change.ChangeStatus;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;

@SpringBootTest
class PairObjectMapperConfigurerTest {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Test
	void testConvertToGenericObject() {
		Assertions
				.assertThat(converterRegistry.convert(Pair.of(ChangeStatus.MODIFIED, null), GenericObject.class))
				.isEqualTo(GenericObject.of(
						GenericObjectAttribute.ofSimple("left", "MODIFIED"),
						RootObjectUtils.createModelReferenceAttribute(CodeObjectModelReference.of(
								ReflectionObjectModelFactory.resolveModelCode(ImmutablePair.class)))));
	}
}
