package tk.labyrinth.pandora.stores.extra.javabasetype;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeParameterUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;
import java.util.Optional;

@SpringBootTest
class JavaBaseTypeRegistry2Test {

	@Autowired
	private JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	@Test
	void testFindJavaClass() {
		Assertions
				.assertThat(javaBaseTypeRegistry.findJavaClass(ObjectModelUtils.createDatatypeBaseReference(
						CodeObjectModelReference.of(DatatypeBase.MODEL_CODE))))
				.isEqualTo(DatatypeBase.class);
	}

	@Test
	void testGetJavaBaseTypeWithReferenceClassHasValueWithReferenceHint() {
		Assertions
				.assertThat(javaBaseTypeRegistry.getJavaBaseType(Reference.class))
				.matches(javaBaseType -> Objects.equals(
						javaBaseType.getHints(),
						List.of(JavaBaseTypeHint.REFERENCE, JavaBaseTypeHint.SIMPLE)));
	}

	@Test
	void testGetJavaTypeWithVariableType() {
		javaBaseTypeRegistry.registerJavaClass(Optional.class);
		//
		Assertions
				.assertThat(javaBaseTypeRegistry.getJavaType(Datatype.ofNonParameterized(
						JavaBaseTypeParameterUtils.createDatatypeBaseReference(Optional.class.getTypeParameters()[0]))))
				.isEqualTo(Optional.class.getTypeParameters()[0]);
	}

	@Disabled("TODO: Deprecated due to datatypes rework. This if we can put them somewhere else.")
	@Test
	void testGetNormalizedDatatypeBaseReferenceWithJavaBaseTypeReference() {
//		{
//			// Object with explicit modelCode.
//			//
//			Assertions
//					.assertThat(String.valueOf(javaBaseTypeRegistry.getNormalizedDatatypeBaseReference(
//							SignatureJavaBaseTypeReference.from(ObjectModel.class))))
//					.isEqualTo("datatypebase(originReference=objectmodel(code=objectmodel))");
//		}
//		{
//			// Object with implicit modelCode.
//			//
//			Assertions
//					.assertThat(String.valueOf(javaBaseTypeRegistry.getNormalizedDatatypeBaseReference(
//							SignatureJavaBaseTypeReference.from(ObjectModelAttribute.class))))
//					.isEqualTo("datatypebase(originReference=objectmodel(code=javabasetype:tk.labyrinth.pandora.datatypes.domain.genericobjectmodelattribute:ObjectModelAttribute))");
//		}
//		{
//			// Not an object, discovered at startup.
//			//
//			Assertions
//					.assertThat(String.valueOf(javaBaseTypeRegistry.getNormalizedDatatypeBaseReference(
//							SignatureJavaBaseTypeReference.from(CodeObjectModelReference.class))))
//					.isEqualTo("datatypebase(originReference=javabasetype(signature=tk.labyrinth.pandora.datatypes.domain.genericobjectmodel:CodeObjectModelReference))");
//			Assertions
//					.assertThat(String.valueOf(javaBaseTypeRegistry.getNormalizedDatatypeBaseReference(
//							SignatureJavaBaseTypeReference.from(String.class))))
//					.isEqualTo("datatypebase(originReference=javabasetype(signature=java.lang:String))");
//		}
//		{
//			// Not an object, discovered after startup.
//			//
//			Assertions
//					.assertThat(String.valueOf(javaBaseTypeRegistry.getNormalizedDatatypeBaseReference(
//							SignatureJavaBaseTypeReference.from(WebApplicationType.class))))
//					.isEqualTo("datatypebase(originReference=javabasetype(signature=org.springframework.boot:WebApplicationType))");
//		}
	}
}
