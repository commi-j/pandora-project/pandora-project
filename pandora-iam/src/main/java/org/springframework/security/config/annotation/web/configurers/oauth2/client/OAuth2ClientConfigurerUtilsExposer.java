package org.springframework.security.config.annotation.web.configurers.oauth2.client;

import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;

public class OAuth2ClientConfigurerUtilsExposer {

	public static <B extends HttpSecurityBuilder<B>> ClientRegistrationRepository getClientRegistrationRepository(
			B builder) {
		return OAuth2ClientConfigurerUtils.getClientRegistrationRepository(builder);
	}
}
