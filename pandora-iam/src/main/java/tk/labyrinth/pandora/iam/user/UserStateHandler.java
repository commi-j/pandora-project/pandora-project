package tk.labyrinth.pandora.iam.user;

import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.VaadinSessionScopedComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;

@VaadinSessionScopedComponent
public class UserStateHandler {

	private final Observable<UserState> userStateObservable = Observable.withInitialValue(UserState.builder()
			.authenticationInformation(null)
			.build());
}
