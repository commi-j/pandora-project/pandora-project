package tk.labyrinth.pandora.iam.security;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

@RequiredArgsConstructor
public class PandoraOAuth2AuthorizationRequestResolver implements OAuth2AuthorizationRequestResolver {

	private final OAuth2AuthorizationRequestResolver oAuth2AuthorizationRequestResolver;

	private OAuth2AuthorizationRequest customizeAuthorizationRequest(OAuth2AuthorizationRequest request) {
		return OAuth2AuthorizationRequest.from(request)
				.state("xyz")
				.build();
	}

	@Override
	public OAuth2AuthorizationRequest resolve(HttpServletRequest request) {
		OAuth2AuthorizationRequest resolvedRequest = oAuth2AuthorizationRequestResolver.resolve(request);
		//
		OAuth2AuthorizationRequest customizedRequest = customizeAuthorizationRequest(resolvedRequest);
		//
		return customizedRequest;
	}

	@Override
	public OAuth2AuthorizationRequest resolve(HttpServletRequest request, String clientRegistrationId) {
		OAuth2AuthorizationRequest resolvedRequest = oAuth2AuthorizationRequestResolver
				.resolve(request, clientRegistrationId);
		//
		OAuth2AuthorizationRequest customizedRequest = customizeAuthorizationRequest(resolvedRequest);
		//
		return customizedRequest;
	}
}
