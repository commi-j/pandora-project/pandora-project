package tk.labyrinth.pandora.iam.security;

import com.vaadin.flow.server.auth.AccessAnnotationChecker;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.security.Principal;
import java.util.function.Function;

@LazyComponent
public class PandoraAccessAnnotationChecker extends AccessAnnotationChecker {

	@Override
	public boolean hasAccess(Class<?> cls, Principal principal, Function<String, Boolean> roleChecker) {
		return true;
	}
}
