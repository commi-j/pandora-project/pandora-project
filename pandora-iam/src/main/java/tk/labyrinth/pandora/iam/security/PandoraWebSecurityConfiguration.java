package tk.labyrinth.pandora.iam.security;

import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.boot.autoconfigure.security.oauth2.client.ClientsConfiguredCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyBean;

import java.util.Collections;

@Configuration
public class PandoraWebSecurityConfiguration {

	/**
	 * This one is for application to start if no configuration is provided.
	 */
	@Conditional(InversedClientsConfiguredCondition.class)
	@LazyBean
	public static InMemoryClientRegistrationRepository emptyInMemoryClientRegistrationRepository() {
		return new InMemoryClientRegistrationRepository(Collections.emptyMap());
	}

	/**
	 * @see ClientsConfiguredCondition
	 */
	public static class InversedClientsConfiguredCondition extends SpringBootCondition {

		private final ClientsConfiguredCondition clientsConfiguredCondition = new ClientsConfiguredCondition();

		@Override
		public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
			return ConditionOutcome.inverse(clientsConfiguredCondition.getMatchOutcome(context, metadata));
		}
	}
}
