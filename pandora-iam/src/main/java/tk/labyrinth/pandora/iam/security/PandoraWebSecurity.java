package tk.labyrinth.pandora.iam.security;

import com.vaadin.flow.server.auth.ViewAccessChecker;
import com.vaadin.flow.spring.security.RequestUtil;
import com.vaadin.flow.spring.security.VaadinDefaultRequestCache;
import com.vaadin.flow.spring.security.VaadinWebSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2ClientConfigurerUtilsExposer;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizationRequestResolver;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

/**
 * <a href="https://vaadin.com/docs/latest/security/enabling-security">https://vaadin.com/docs/latest/security/enabling-security</a>
 */
@EnableWebSecurity
@LazyComponent
public class PandoraWebSecurity extends VaadinWebSecurity {

	@Autowired
	private RequestUtil requestUtil;

	@Autowired
	private VaadinDefaultRequestCache vaadinDefaultRequestCache;

	@Autowired
	private ViewAccessChecker viewAccessChecker;

	/**
	 * This is to handle authentication with own mechanism.
	 *
	 * @param http non-null
	 *
	 * @throws Exception propagated
	 */
	// FIXME: We actually want Vaadin requests to be processed anonymously, not everything.
	private void configureAnyAnonymous(HttpSecurity http) throws Exception {
		http.authorizeHttpRequests().anyRequest().permitAll();
		//
		// Required for Vaadin to work properly.
		http.csrf().ignoringRequestMatchers(requestUtil::isFrameworkInternalRequest);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		{
			configureAnyAnonymous(http);
		}
//		super.configure(http);
//		{
//			http.requestCache().requestCache(vaadinDefaultRequestCache);
//			viewAccessChecker.enable();
//		}
		//
		//
//		disableContinueAttribute(http);
//		customizeState(http);
		//
		{
//			setOAuth2LoginPage(http, "/login");
			http.oauth2Login().loginPage("/login");
//			.permitAll();
		}
		//
//		{
//			// FIXME: Think of a smarter solution for this.
//			//
//			http.authorizeHttpRequests().requestMatchers("**").permitAll();
//		}
	}

	private static void customizeState(HttpSecurity http) {
		//
		// The code here replicates the one in
		// OAuth2ClientConfigurer.AuthorizationCodeGrantConfigurer#getAuthorizationRequestResolver().
		DefaultOAuth2AuthorizationRequestResolver defaultOAuth2AuthorizationRequestResolver =
				new DefaultOAuth2AuthorizationRequestResolver(
						OAuth2ClientConfigurerUtilsExposer.getClientRegistrationRepository(http),
						OAuth2AuthorizationRequestRedirectFilter.DEFAULT_AUTHORIZATION_REQUEST_BASE_URI);
		//
//		http
//				.oauth2Login()
//				.authorizationEndpoint()
//				//
//				// TODO: Can use this one if we want to manage state.
////				.authorizationRequestResolver(new PandoraOAuth2AuthorizationRequestResolver(
////						defaultOAuth2AuthorizationRequestResolver));
	}

	/**
	 * <a href="https://stackoverflow.com/questions/75222930/spring-boot-3-0-2-adds-continue-query-parameter-to-request-url-after-login">https://stackoverflow.com/questions/75222930/spring-boot-3-0-2-adds-continue-query-parameter-to-request-url-after-login</a>
	 */
	private static void disableContinueAttribute(HttpSecurity http) {
		HttpSessionRequestCache requestCache = new HttpSessionRequestCache();
		//
		requestCache.setMatchingRequestParameterName(null);
		//
		((VaadinDefaultRequestCache) http.getSharedObject(RequestCache.class)).setDelegateRequestCache(requestCache);
	}
}
