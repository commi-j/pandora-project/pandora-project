package tk.labyrinth.pandora.iam;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

@Route("iam-demo-page")
public class PandoraIamDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		add("I AM DEMO");
	}
}
