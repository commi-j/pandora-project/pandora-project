package tk.labyrinth.pandora.iam.access;

import lombok.AccessLevel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;

public interface AttributeAccessLevelDecider {

	AccessLevel decideAccessLevel(GenericAccessContext context, ObjectModel objectModel, String attributeName);
}
