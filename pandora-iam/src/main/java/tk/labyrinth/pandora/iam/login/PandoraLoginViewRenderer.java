package tk.labyrinth.pandora.iam.login;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.Span;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import tk.labyrinth.pandora.functionalcomponents.html.AnchorRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PandoraLoginViewRenderer {

	private final InMemoryClientRegistrationRepository clientRegistrationRepository;

	public Component render() {
		return render(Parameters.Builder::build);
	}

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			layout.add(new Span("Login with OAuth 2.0"));
			//
			clientRegistrationRepository.forEach(clientRegistration -> layout.add(AnchorRenderer
					.render(builder -> builder
							.configurer(anchor -> anchor.getElement().setAttribute("router-ignore", true))
							.href("oauth2/authorization/%s".formatted(clientRegistration.getRegistrationId()))
							.target(AnchorTarget.BLANK)
							.text(clientRegistration.getClientName())
							.build())));
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {
		// TODO: Populate.

		public static class Builder {
			// Lomboked
		}
	}
}
