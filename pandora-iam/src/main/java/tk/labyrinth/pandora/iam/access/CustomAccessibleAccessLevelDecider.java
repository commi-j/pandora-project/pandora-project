package tk.labyrinth.pandora.iam.access;

import lombok.AccessLevel;

public interface CustomAccessibleAccessLevelDecider {

	AccessLevel decideAccessLevel(GenericAccessContext context, String customAccessibleName);
}
