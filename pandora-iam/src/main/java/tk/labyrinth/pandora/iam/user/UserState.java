package tk.labyrinth.pandora.iam.user;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class UserState {

	@Nullable
	UserAuthenticationInformation authenticationInformation;
}
