package tk.labyrinth.pandora.iam.profile;

import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import tk.labyrinth.pandora.iam.login.PandoraLoginViewRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;
import tk.labyrinth.pandora.uiapplication.navbar.NavbarComponentsProvider;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class NavbarUserWidgetProvider implements NavbarComponentsProvider {

	private final PandoraLoginViewRenderer loginViewRenderer;

	@Override
	public List<PandoraAttachableComponentWrapper> provideNavbarComponents() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		//
		Button button = new Button();
		{
			button.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY);
		}
		{
			// TODO: Use AvatarRenderer.
			Avatar avatar = new Avatar();
			{
				avatar.setTooltipEnabled(true);
			}
			{
				//
				if (principal instanceof OidcUser oidcUser) {
					avatar.setImage(oidcUser.getClaimAsString("picture"));
					avatar.setName(oidcUser.getClaimAsString("name"));
				} else if (Objects.equals(principal, "anonymousUser")) {
//				avatar.setImage(); // TODO
					avatar.setName("Anonymous");
				}
			}
			button.setIcon(avatar);
		}
		{
			button.addClickListener(event -> {
				if (isAuthenticated(principal)) {
					CssVerticalLayout layout = new CssVerticalLayout();
					{
						layout.addClassNames(PandoraStyles.LAYOUT);
					}
					{
						layout.add(new Span("TODO: Logout"));
						{
							OidcUser oidcUser = (OidcUser) principal;
							//
							CssVerticalLayout claimsLayout = new CssVerticalLayout();
							{
								claimsLayout.add("Claims:");
								//
								oidcUser.getClaims().forEach((k, v) ->
										claimsLayout.add(new Span("%s: %s".formatted(k, v))));
							}
							layout.add(claimsLayout);
						}
					}
					//
					Dialogs.show(layout);
				} else {
					Dialogs.show(loginViewRenderer.render());
				}
			});
		}
		//
		return List.of(PandoraAttachableComponentWrapper.builder()
				.component(button)
				.placeAtEnd(true)
				.build());
	}

	private static boolean isAuthenticated(Object principal) {
		return principal instanceof OidcUser;
	}
}
