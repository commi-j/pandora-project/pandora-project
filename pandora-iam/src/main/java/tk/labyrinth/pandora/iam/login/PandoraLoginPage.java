package tk.labyrinth.pandora.iam.login;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.FloatLayout;

@RequiredArgsConstructor
@Route("login")
public class PandoraLoginPage extends FloatLayout {

	private final PandoraLoginViewRenderer loginViewRenderer;

	@PostConstruct
	private void postConstruct() {
		{
			setHeightFull();
		}
		{
			setContent(loginViewRenderer.render());
		}
	}
}
