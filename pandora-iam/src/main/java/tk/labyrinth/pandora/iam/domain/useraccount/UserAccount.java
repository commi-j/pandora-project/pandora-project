package tk.labyrinth.pandora.iam.domain.useraccount;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.bson.codecs.pojo.annotations.BsonId;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.datatypes.domain.secret.Secret;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel
@Value
@With
public class UserAccount implements HasUid {

	String displayName;

	Secret password;

	@BsonId
	UUID uid;

	String username;
}
