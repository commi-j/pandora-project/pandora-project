package tk.labyrinth.pandora.iam;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class PandoraIamModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
