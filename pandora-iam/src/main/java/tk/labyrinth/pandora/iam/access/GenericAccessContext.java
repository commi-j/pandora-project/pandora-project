package tk.labyrinth.pandora.iam.access;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.iam.user.UserState;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public
class GenericAccessContext {

	UserState userState;
}
