package tk.labyrinth.pandora.activities.activity;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.time.Instant;
import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class PandoraActivity {

	@NonNull
	PandoraActivityColourIndicator colourIndicator;

	@Nullable
	Instant finishedAt;

	Reference<?> reference;

	Instant startedAt;

	String statusText;

	UUID uid;
}
