package tk.labyrinth.pandora.activities.activity;

import io.vavr.collection.List;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

import java.time.Duration;
import java.util.Comparator;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Bean
public class PandoraActivityRegistry {

	private final ConcurrentMap<UUID, PandoraActivity> activities = new ConcurrentHashMap<>();

	private final Sinks.Many<PandoraActivity> sink = Sinks.many().replay().limit(Duration.ZERO);

	public List<PandoraActivity> getActivities() {
		return activities.values().stream()
				.sorted(Comparator.comparing(PandoraActivity::getStartedAt).reversed())
				.limit(10)
				.collect(List.collector());
	}

	public Flux<PandoraActivity> getFlux() {
		return sink.asFlux();
	}

	public void updateActivity(PandoraActivity activity) {
		activities.put(activity.getUid(), activity);
		sink.tryEmitNext(activity);
	}
}
