package tk.labyrinth.pandora.activities.activity;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import reactor.core.Disposable;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BackgroundColor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BorderRadius;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Margin;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.ReactiveVaadin;
import tk.labyrinth.pandora.views.style.PandoraColours;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicReference;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class PandoraActivityFeedView extends CssVerticalLayout {

	public static final String CLASS_NAME = "pandora-activity-feed-view";

	private final PandoraActivityRegistry activityRegistry;

	private List<PandoraActivity> activities = List.empty();

	private Component createActivityComponent(PandoraActivity activity) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.addClassNames(PandoraStyles.CARD);
			//
			layout.setAlignItems(AlignItems.CENTER);
		}
		{
			{
				Div div = new Div();
				//
				StyleUtils.setCssProperty(
						div,
						BackgroundColor.of(switch (activity.getColourIndicator()) {
							case BLUE -> PandoraColours.BLUE;
							case GREEN -> PandoraColours.GREEN;
							case GREY -> throw new NotImplementedException();
							case ORANGE -> throw new NotImplementedException();
							case RED -> PandoraColours.RED;
							case YELLOW -> throw new NotImplementedException();
						}));
				StyleUtils.setCssProperty(div, BorderRadius.of("0.25em"));
				StyleUtils.setCssProperty(div, Margin.of("0.5em"));
				div.setHeight("0.5em");
				div.setWidth("0.5em");
				//
				layout.add(div);
			}
			layout.add(activity.getStatusText());
		}
		return layout;
	}

	private void mergeActivity(PandoraActivity activity) {
		List<PandoraActivity> nextActivities = activities
				.prepend(activity)
				.distinctBy(PandoraActivity::getUid)
				.sorted(Comparator.comparing(PandoraActivity::getStartedAt).reversed())
				.take(10);
		//
		activities = nextActivities;
		//
		removeAll();
		//
		nextActivities.forEach(nextActivity -> add(createActivityComponent(nextActivity)));
	}

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames(CLASS_NAME, PandoraStyles.LAYOUT);
		}
		{
			AtomicReference<Disposable> disposableReference = new AtomicReference<>();
			addAttachListener(event -> {
				disposableReference.set(ReactiveVaadin.uiFlux(UI.getCurrent(), activityRegistry.getFlux())
						.subscribe(this::mergeActivity));
				//
				activityRegistry.getActivities().forEach(this::mergeActivity);
			});
			addDetachListener(event -> disposableReference.get().dispose());
		}
	}
}
