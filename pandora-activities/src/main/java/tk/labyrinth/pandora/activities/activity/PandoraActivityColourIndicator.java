package tk.labyrinth.pandora.activities.activity;

public enum PandoraActivityColourIndicator {
	/**
	 * Ongoing.
	 */
	BLUE,
	/**
	 * Completed succesfully.
	 */
	GREEN,
	/**
	 * Scheduled.
	 */
	GREY,
	/**
	 * Completed with important warning.
	 */
	ORANGE,
	/**
	 * Error.
	 */
	RED,
	/**
	 * Completed with non-important warnings.
	 */
	YELLOW,
}
