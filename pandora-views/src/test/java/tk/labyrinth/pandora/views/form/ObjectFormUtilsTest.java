package tk.labyrinth.pandora.views.form;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ObjectFormUtilsTest {

	@Test
	void testPrettifyName() {
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("helloWorld"))
				.isEqualTo("Hello World");
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("HelloWorld"))
				.isEqualTo("Hello World");
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("URLHelloWorld"))
				.isEqualTo("URL Hello World");
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("helloHTTPWorld"))
				.isEqualTo("Hello HTTP World");
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("optionA"))
				.isEqualTo("Option A");
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("optionBEE"))
				.isEqualTo("Option BEE");
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("aBug"))
				.isEqualTo("A Bug");
		Assertions
				.assertThat(ObjectFormUtils.prettifyName("anUmbrella"))
				.isEqualTo("An Umbrella");
	}
}
