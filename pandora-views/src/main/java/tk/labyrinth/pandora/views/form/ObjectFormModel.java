package tk.labyrinth.pandora.views.form;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ObjectFormModel {

	@NonNull
	List<ObjectFormAttribute> attributes;

	Integer columnNumber;

	String title;
}
