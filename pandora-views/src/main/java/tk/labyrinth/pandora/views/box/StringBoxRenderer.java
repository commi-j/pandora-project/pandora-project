package tk.labyrinth.pandora.views.box;

import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.views.style.PandoraColours;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class StringBoxRenderer {

	public static View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static View render(Parameters parameters) {
		String initialValue = parameters.initialValue();
		String currentValue = parameters.currentValue();
		//
		TextField textField = TextFieldRenderer.render(builder -> builder
				.autocomplete(parameters.autocomplete())
				.label(parameters.label())
				.onValueChange(nextValue -> {
					if (parameters.onValueChange() != null) {
						parameters.onValueChange().accept(StringUtils.emptyToNull(nextValue));
					}
				})
				.readOnly(!parameters.calcEditable())
				.title(StringUtils.emptyToNull(parameters.calcNonEmptyNonEditableReasons().mkString("\n")))
				.value(StringUtils.nullToEmpty(currentValue))
				.valueChangeMode(parameters.valueChangeMode())
				.width(parameters.width())
				.build());
		//
		{
			if (!Objects.equals(currentValue, initialValue)) {
				String colour;
				//
				if (initialValue == null) {
					colour = PandoraColours.GREEN_50;
				} else if (currentValue == null) {
					colour = PandoraColours.RED_50;
				} else {
					colour = PandoraColours.BLUE_50;
				}
				//
				textField.getStyle().set("border-radius", "4px");
				textField.getStyle().set("box-shadow", "0 0 0 2px %s".formatted(colour));
			}
		}
		//
		return ComponentView.of(textField);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Autocomplete autocomplete;

		@Nullable
		String currentValue;

		@Nullable
		String initialValue;

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<@Nullable String> onValueChange;

		@Nullable
		ValueChangeMode valueChangeMode;

		@Nullable
		String width;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder {
			// Lomboked
		}
	}
}
