package tk.labyrinth.pandora.views.box;

import tk.labyrinth.pandora.core.meta.HasSupportDistance;

public interface BoxRendererContributor<T> extends HasSupportDistance<BoxRendererRegistry.Context> {

	BoxRenderer<T> contributeBoxRenderer(BoxRendererRegistry registry, BoxRendererRegistry.Context context);
}
