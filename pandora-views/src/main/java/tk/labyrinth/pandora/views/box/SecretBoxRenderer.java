package tk.labyrinth.pandora.views.box;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.secret.Secret;
import tk.labyrinth.pandora.datatypes.simple.access.PandoraAccessLevel;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.PasswordFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
public class SecretBoxRenderer {

	public static View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static View render(Parameters parameters) {
		// TODO: When not viewable we want to render TextField with smth like "SECRET" and not PwField,
		//  but this may misfire if accessLevel is changed mid-lifecycle.
		return ComponentView.of(PasswordFieldRenderer.render(builder -> builder
				//
				// This looks like crutch but Firefox does not suggest passwords for this input kind,
				// which is default behaviour we want for this box.
				.autocomplete(parameters.autocomplete() != null ? parameters.autocomplete() : Autocomplete.NEW_PASSWORD)
				.label(parameters.label())
				.onValueChange(nextValue -> parameters.onValueChange().accept(
						!nextValue.isEmpty() ? Secret.of(nextValue) : null))
				.readOnly(!PandoraAccessLevel.canEdit(parameters.accessLevel()))
				.revealButtonVisible(PandoraAccessLevel.canView(parameters.accessLevel()))
				.suffixComponent(ButtonRenderer.render(copyButtonBuilder -> copyButtonBuilder
						.icon(VaadinIcon.CLIPBOARD.create())
						.onClick(event -> {
							UI.getCurrent().getPage().executeJs(
									"navigator.clipboard.writeText($0)",
									parameters.currentValue().getValue());
							//
							// TODO: Show it on the box itself.
							Notification.show("Copied!", 500, Notification.Position.BOTTOM_END);
						})
						.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
						.visible(PandoraAccessLevel.canView(parameters.accessLevel()) && parameters.currentValue() != null)
						.build()))
				.value(PandoraAccessLevel.canView(parameters.accessLevel())
						? parameters.currentValue() != null ? parameters.currentValue().getValue() : ""
						: "********")
				.valueChangeMode(parameters.valueChangeMode())
				.build()));
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		PandoraAccessLevel accessLevel;

		@Nullable
		Autocomplete autocomplete;

		@Nullable
		Secret currentValue;

		@Nullable
		Secret initialValue;

		String label;

		Consumer<@Nullable Secret> onValueChange;

		@Nullable
		ValueChangeMode valueChangeMode;

		public static class Builder {
			// Lomboked
		}
	}
}
