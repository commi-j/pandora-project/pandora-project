package tk.labyrinth.pandora.views.box;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.TextAreaRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.html.AnchorRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.java.net.UrlUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.views.domain.edition.NonEditableReasons;

import java.net.URL;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class UrlBoxRenderer {

	public static View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static View render(Parameters parameters) {
		CssGridLayout layout = FunctionalComponents.createComponent(CssGridLayout::new);
		{
			// FIXME
			layout.removeAll();
		}
		{
//			layout.getStyle().set("align-self", "start"); // FIXME: Works well for horizontal, poor for vertical flexes.
			//
			layout.getStyle().set("display", "inline-grid");
			layout.getStyle().set("width", "var(--vaadin-field-default-width, 12em)");
			//
			layout.setGridTemplateColumns("auto 1fr");
			layout.setGridTemplateRows("auto auto");
			//
			ElementUtils.setTitle(layout, parameters.nonEditableReasons().toPrettyString());
		}
		{
			URL initialValue = parameters.initialValue();
			URL currentValue = parameters.currentValue();
			//
			{
				Span labelSpan = parameters.label() != null ? new Span(parameters.label()) : null;
				//
				if (labelSpan != null) {
					labelSpan.getStyle().set("color", "var(--lumo-secondary-text-color)");
					labelSpan.getStyle().set("font-size", "var(--lumo-font-size-s)");
					labelSpan.getStyle().set("font-weight", "500");
					labelSpan.getStyle().set("line-height", "1");
					labelSpan.getStyle().set("padding-bottom", "0.5em");
					//
					layout.add(labelSpan, "1", "1");
				}
			}
			{
				CssHorizontalLayout contentLayout = new CssHorizontalLayout();
				{
					contentLayout.setAlignItems(AlignItems.CENTER);
					//
					contentLayout.getStyle().set("border", "1px dashed var(--lumo-contrast-30pct)");
					contentLayout.getStyle().set("border-radius", "var(--lumo-border-radius-m)");
					contentLayout.getStyle().set("gap", "0.5em");
					contentLayout.getStyle().set("height", "2.15em");
					contentLayout.getStyle().set("padding", "0px 0.5em");
				}
				{
					if (currentValue != null) {
						Anchor anchor = AnchorRenderer.render(builder -> builder
								.href(currentValue.toString())
								.target(AnchorTarget.BLANK)
								.text(currentValue.toString())
								.build());
						{
							anchor.getStyle().set("flex-grow", "1");
							anchor.getStyle().set("font-weight", "500");
							anchor.getStyle().set("overflow", "hidden");
							anchor.getStyle().set("text-overflow", "ellipsis");
							anchor.getStyle().set("text-wrap", "nowrap");
						}
						contentLayout.add(anchor);
					} else {
						Div div = new Div();
						{
							CssFlexItem.setFlexGrow(div, 1);
						}
						contentLayout.add(div);
					}
				}
				{
					if (parameters.nonEditableReasons().isEditable()) {
						NativeButton button = new NativeButton();
						{
							button.getStyle().set("background-color", "transparent");
							button.getStyle().set("border", "none");
							button.getStyle().set("color", "var(--lumo-secondary-text-color)");
							button.getStyle().set("padding", "0.25em");
						}
						{
							Icon icon = VaadinIcon.EDIT.create();
							{
								icon.getStyle().set("height", "var(--lumo-icon-size-s)");
								icon.getStyle().set("width", "var(--lumo-icon-size-s)");
							}
							button.add(icon);
						}
						{
							button.addClickListener(event -> {
								String initialStringValue = initialValue != null ? initialValue.toString() : null;
								String currentStringValue = currentValue != null ? currentValue.toString() : null;
								//
								ConfirmationViews
										.showComponentFunctionDialog(
												Optional.ofNullable(currentStringValue),
												(next, sink) -> TextAreaRenderer.render(textAreaBuilder -> textAreaBuilder
														.onValueChange(nextValue -> sink.accept(Optional.ofNullable(
																StringUtils.emptyToNull(nextValue))))
														.pattern("(http://|https://).+")
														.value(next.orElse(""))
														.width("24em")
														.build()))
										.subscribeAlwaysAccepted(result -> parameters.onValueChange()
												.accept(result.map(UrlUtils::from).orElse(null)));
							});
						}
						//
						contentLayout.add(button);
					}
				}
				layout.add(contentLayout, "1 / span 2", "2");
			}
		}
		return ComponentView.of(layout);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		URL currentValue;

		@Nullable
		URL initialValue;

		String label;

		@lombok.Builder.Default
		@NonNull
		NonEditableReasons nonEditableReasons = NonEditableReasons.empty();

		Consumer<@Nullable URL> onValueChange;

		@Nullable
		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
