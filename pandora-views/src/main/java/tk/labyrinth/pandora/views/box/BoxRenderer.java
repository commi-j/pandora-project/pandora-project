package tk.labyrinth.pandora.views.box;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.simple.access.PandoraAccessLevel;
import tk.labyrinth.pandora.functionalvaadin.view.View;

import java.lang.reflect.Type;
import java.util.function.Consumer;
import java.util.function.Function;

public interface BoxRenderer<T> {

	Type getValueType();

	default View render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	View render(Parameters<T> parameters);

	static <T> BoxRenderer<T> of(Type valueType, Function<Parameters<T>, View> renderFunction) {
		return new Typed<>(renderFunction, valueType);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	class Parameters<@Nullable T> {

		@Deprecated
		@Nullable
		PandoraAccessLevel accessLevel;

		@Nullable
		T currentValue;

		@Nullable
		T initialValue;

		@Nullable
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<@Nullable T> onValueChange;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder<T> {
			// Lomboked
		}
	}

	@Value
	class Typed<T> implements BoxRenderer<T> {

		@NonNull
		Function<Parameters<T>, View> renderFunction;

		@NonNull
		Type valueType;

		@Override
		public View render(Parameters<T> parameters) {
			return renderFunction.apply(parameters);
		}
	}
}
