package tk.labyrinth.pandora.views.box;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Anchor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;

import java.net.URL;

public class UrlField extends CustomField<URL> {

	{
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.getStyle().set("border", "1px dashed var(--lumo-contrast-30pct)");
			layout.getStyle().set(
					"border-radius",
					"var(--vaadin-input-field-top-start-radius, " +
							"var(--_input-container-radius)) var(--vaadin-input-field-top-end-radius, " +
							"var(--_input-container-radius)) var(--vaadin-input-field-bottom-end-radius, " +
							"var(--_input-container-radius)) var(--vaadin-input-field-bottom-start-radius, " +
							"var(--_input-container-radius))");
			layout.getStyle().set("color", "var(--lumo-secondary-text-color)");
		}
		{
			{
				Anchor anchor = new Anchor("foobar", "foobar");
				//
				CssFlexItem.setFlexGrow(anchor, 1);
				//
				layout.add(anchor);
			}
			{
//				layout.add(ButtonRenderer.render(builder -> builder
//						.icon(VaadinIcon.EDIT.create())
//						.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
//						.build()));
			}
		}
		add(layout);
	}

	@Override
	protected URL generateModelValue() {
		return null;
	}

	@Override
	protected void setPresentationValue(URL url) {
	}
}
