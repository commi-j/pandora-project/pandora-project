package tk.labyrinth.pandora.views.form;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H3;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;
import tk.labyrinth.pandora.views.box.GenericBoxRenderer;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.function.Consumer;
import java.util.function.Function;

@RequiredArgsConstructor
public class ObjectFormRenderer {

	private final BoxRendererRegistry boxRendererRegistry;

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			ObjectFormModel model = parameters.model();
			//
			{
				layout.add(new H3(model.getTitle()));
			}
			{
				CssGridLayout attributesLayout = new CssGridLayout();
				{
					attributesLayout.addClassName(PandoraStyles.LAYOUT);
				}
				{
					model.getAttributes().forEach(attribute -> {
						GenericBoxRenderer genericRenderer = boxRendererRegistry.getGenericRenderer(
								BoxRendererRegistry.Context.builder()
										.datatype(attribute.getDatatype())
										.features(List.empty()) // TODO
										.build());
						//
						View view = genericRenderer.render(builder -> builder
								.currentValue(parameters.currentValue().findAttributeValue(attribute.getModelName()))
								.initialValue(parameters.initialValue().findAttributeValue(attribute.getModelName()))
								.label(attribute.getDisplayName())
								.onValueChange(nextValue -> parameters.onValueChange().accept(parameters.currentValue()
										.withAttribute(attribute.getModelName(), nextValue)))
								.build());
						//
						attributesLayout.add(view.asVaadinComponent());
					});
				}
			}
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		GenericObject currentValue;

		@NonNull
		GenericObject initialValue;

		@NonNull
		ObjectFormModel model;

		@NonNull
		Consumer<GenericObject> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
