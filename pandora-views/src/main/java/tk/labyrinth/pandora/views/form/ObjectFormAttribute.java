package tk.labyrinth.pandora.views.form;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ObjectFormAttribute {

	Datatype datatype;

	String displayName;

	String modelName;
}
