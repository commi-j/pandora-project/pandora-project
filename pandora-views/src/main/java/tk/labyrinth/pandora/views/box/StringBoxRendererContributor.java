package tk.labyrinth.pandora.views.box;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

@Bean
@RequiredArgsConstructor
public class StringBoxRendererContributor implements BoxRendererContributor<String> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	public BoxRenderer<String> contributeBoxRenderer() {
		return BoxRenderer.of(
				String.class,
				parameters -> StringBoxRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.label(parameters.label())
						.nonEditableReasons(parameters.nonEditableReasons())
						.onValueChange(parameters.onValueChange())
						.build()));
	}

	@Override
	public BoxRenderer<String> contributeBoxRenderer(BoxRendererRegistry registry, BoxRendererRegistry.Context context) {
		return contributeBoxRenderer();
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BoxRendererRegistry.Context target) {
		return datatypeBaseRegistry.matches(target.getDatatype(), String.class)
				? 1000
				: null;
	}
}
