package tk.labyrinth.pandora.views.smartbox;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.views.box.BoxRenderer;
import tk.labyrinth.pandora.views.box.BoxRendererContributor;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;

@Bean
@RequiredArgsConstructor
public class SmartBoxRendererContributor implements BoxRendererContributor<Object> {

	@Override
	@SuppressWarnings("unchecked")
	public BoxRenderer<Object> contributeBoxRenderer(BoxRendererRegistry registry, BoxRendererRegistry.Context context) {
		BoxRenderer<Object> renderer = (BoxRenderer<Object>) registry.getRenderer(context);
		//
		return BoxRenderer.of(
				renderer.getValueType(),
				parameters -> SmartBoxRenderer.render(builder -> builder
						.contentRenderer(renderer)
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.label(parameters.label())
						.onValueChange(parameters.onValueChange())
						.build()));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BoxRendererRegistry.Context target) {
		return null;
	}
}
