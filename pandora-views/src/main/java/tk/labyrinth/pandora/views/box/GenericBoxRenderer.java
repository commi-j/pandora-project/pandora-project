package tk.labyrinth.pandora.views.box;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.functionalvaadin.view.View;

import java.lang.reflect.Type;
import java.util.function.Consumer;
import java.util.function.Function;

public class GenericBoxRenderer {

	private final BoxRenderer<?> boxRenderer;

	// FIXME: Are we sure it should be not ConverterRegistry?
	private final ObjectMapper objectMapper;

	private final Type valueType;

	public GenericBoxRenderer(BoxRenderer<?> boxRenderer, ObjectMapper objectMapper) {
		this.boxRenderer = boxRenderer;
		this.objectMapper = objectMapper;
		this.valueType = boxRenderer.getValueType();
	}

	public View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public View render(Parameters parameters) {
		return boxRenderer.render(builder -> builder
				.currentValue(objectMapper.convertValue(parameters.currentValue(), objectMapper.constructType(valueType)))
				.initialValue(objectMapper.convertValue(parameters.initialValue(), objectMapper.constructType(valueType)))
				.label(parameters.label())
				.nonEditableReasons(parameters.nonEditableReasons())
				.onValueChange(nextValue -> parameters.onValueChange()
						.accept(objectMapper.convertValue(nextValue, ValueWrapper.class)))
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		ValueWrapper currentValue;

		@Nullable
		ValueWrapper initialValue;

		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<ValueWrapper> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
