package tk.labyrinth.pandora.views.box;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

@Bean
@RequiredArgsConstructor
public class IntegerBoxRendererContributor implements BoxRendererContributor<Integer> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	public BoxRenderer<Integer> contributeBoxRenderer(BoxRendererRegistry registry, BoxRendererRegistry.Context context) {
		return BoxRenderer.of(
				Integer.class,
				parameters -> IntegerBoxRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.label(parameters.label())
						// TODO: Non-editable reasons.
						.onValueChange(parameters.onValueChange())
						.build()));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BoxRendererRegistry.Context target) {
		return datatypeBaseRegistry.matches(target.getDatatype(), Integer.class)
				? 1000
				: null;
	}
}
