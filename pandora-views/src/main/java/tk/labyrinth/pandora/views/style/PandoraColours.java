package tk.labyrinth.pandora.views.style;

/**
 * <a href="https://clrs.cc/">https://clrs.cc/</a><br>
 * src/main/resources/META-INF/resources/frontend/style/pandora-colours.css<br>
 */
public class PandoraColours {

	public static final String BLUE = "var(--pandora-blue)";

	public static final String BLUE_50 = "var(--pandora-blue-50)";

	public static final String GREEN = "var(--pandora-green)";

	public static final String GREEN_50 = "var(--pandora-green-50)";

	public static final String PURPLE = "var(--pandora-purple)";

	public static final String PURPLE_50 = "var(--pandora-purple-50)";

	public static final String RED = "var(--pandora-red)";

	public static final String RED_50 = "var(--pandora-red-50)";
}
