package tk.labyrinth.pandora.views;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.functionalvaadin.PandoraFunctionalVaadinModule;

@Import({
		PandoraFunctionalVaadinModule.class,
})
@SpringBootApplication
public class PandoraViewsModule {
	// empty
}
