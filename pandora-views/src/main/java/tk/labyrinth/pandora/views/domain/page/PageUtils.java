package tk.labyrinth.pandora.views.domain.page;

import com.nimbusds.jose.util.Pair;
import io.vavr.collection.List;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.jaap.langreflect.util.MethodUtils;
import tk.labyrinth.pandora.views.domain.page.meta.PageFunction;

import java.lang.reflect.Method;

public class PageUtils {

	public static String computePageFunctionSlug(Method method) {
		return MethodUtils.getFullSignature(method).toString();
	}

	public static String computeSinglePageFunctionSlug(Class<?> pageRendererClass) {
		List<Pair<Method, MergedAnnotation<PageFunction>>> methods = List.of(pageRendererClass.getMethods())
				.map(method -> Pair.of(method, MergedAnnotations.from(method).get(PageFunction.class)))
				.filter(pair -> pair.getRight().isPresent());
		//
		if (methods.size() != 1) {
			throw new IllegalArgumentException();
		}
		//
		return computePageFunctionSlug(methods.single().getLeft());
	}
}
