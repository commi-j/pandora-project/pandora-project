package tk.labyrinth.pandora.views.box;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.arc.All;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.checker.nullness.qual.PolyNull;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.core.selection.SelectionResult;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeature;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;

import java.lang.reflect.Type;
import java.util.function.Consumer;
import java.util.function.Function;

@Bean
@RequiredArgsConstructor
public class BoxRendererRegistry {

	private final ConverterRegistry converterRegistry;

	private final ObjectMapper objectMapper;

	private List<BoxRendererContributor<?>> boxRendererContributors;

	@All
	private java.util.List<BoxRendererContributor<?>> quarkusBoxRendererContributors;

	@Autowired
	private java.util.List<BoxRendererContributor<?>> springBoxRendererContributors;

	@PostConstruct
	private void postConstruct() {
		if (quarkusBoxRendererContributors != null) {
			boxRendererContributors = List.ofAll(quarkusBoxRendererContributors);
		}
		if (springBoxRendererContributors != null) {
			boxRendererContributors = List.ofAll(springBoxRendererContributors);
		}
	}

	public GenericBoxRenderer getGenericRenderer(Context context) {
		BoxRenderer<?> renderer = getRenderer(context);
		//
		return new GenericBoxRenderer(renderer, objectMapper);
	}

	@SuppressWarnings("unchecked")
	public <T> BoxRenderer<T> getRenderer(@NonNull Class<T> javaType) {
		return (BoxRenderer<T>) getRenderer(JavaBaseTypeUtils.createDatatypeFromJavaType(javaType));
	}

	@SuppressWarnings("unchecked")
	public BoxRenderer<?> getRenderer(Context context) {
		val selectionResult = HasSupportDistance.selectHandler(boxRendererContributors, context);
		//
		BoxRenderer<?> boxRenderer = selectionResult.getHandler().contributeBoxRenderer(this, context);
		//
		return boxRenderer;
		//
		// TODO: SmartBoxRenderer is not mature enough.
//				BoxRenderer.of(
//				boxRenderer.getValueType(),
//				parameters -> SmartBoxRenderer.render(builder -> builder
//						.contentRenderer((BoxRenderer<Object>) boxRenderer)
//						.currentValue(parameters.currentValue())
//						.initialValue(parameters.initialValue())
//						.label(parameters.label())
//						.onValueChange(nextValue -> parameters.onValueChange().accept(nextValue))
//						.build()));
	}

	public BoxRenderer<?> getRenderer(@NonNull Datatype datatype) {
		return getRenderer(Context.builder()
				.datatype(datatype)
				.features(List.empty())
				.build());
	}

	@SuppressWarnings("unchecked")
	public <T> BoxRenderer<T> getRendererInferred(@NonNull Type javaType) {
		return (BoxRenderer<T>) getRenderer(JavaBaseTypeUtils.createDatatypeFromJavaType(javaType));
	}

	public View render(Context context, Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(context, parametersConfigurer.apply(Parameters.builder()));
	}

	public View render(Context context, Parameters parameters) {
		BoxRenderer<?> renderer = getRenderer(context);
		//
		return renderer.render(builder -> builder
				.currentValue(ObjectUtils.inferNullable(
						converterRegistry.convert(parameters.currentValue(), renderer.getValueType())))
				.initialValue(ObjectUtils.inferNullable(
						converterRegistry.convert(parameters.initialValue(), renderer.getValueType())))
				.label(parameters.label())
				.nonEditableReasons(parameters.nonEditableReasons())
				.onValueChange(nextValue -> parameters.onValueChange()
						.accept(converterRegistry.convert(nextValue, ValueWrapper.class)))
				.build());
	}

	public SelectionResult<Context, BoxRendererContributor<?>> selectRendererContributors(
			Context context) {
		val selectionResult = HasSupportDistance.selectHandler(boxRendererContributors, context);
		//
		return SelectionResult.of(context, selectionResult.getEvaluatedHandlers());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		ValueWrapper currentValue;

		@Nullable
		ValueWrapper initialValue;

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<ValueWrapper> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Context {

		@PolyNull
		Datatype datatype;

		// TODO: This should be not bound to ObjectModelAttribute.
		@NonNull
		List<ObjectModelAttributeFeature> features;
	}
}
