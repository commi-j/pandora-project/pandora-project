package tk.labyrinth.pandora.views.box;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

@Bean
@RequiredArgsConstructor
public class BooleanBoxRendererContributor implements BoxRendererContributor<Boolean> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	public BoxRenderer<Boolean> contributeBoxRenderer(BoxRendererRegistry registry, BoxRendererRegistry.Context context) {
		return BoxRenderer.of(
				Boolean.class,
				parameters -> BooleanBoxRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.label(parameters.label())
						.nonEditableReasons(parameters.nonEditableReasons())
						.onValueChange(parameters.onValueChange())
						.build()));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BoxRendererRegistry.Context target) {
		return datatypeBaseRegistry.matches(target.getDatatype(), Boolean.class)
				? 1000
				: null;
	}
}
