package tk.labyrinth.pandora.views.box;

import com.vaadin.flow.component.select.Select;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.component.SelectRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class BooleanBoxRenderer {

	public static View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static View render(Parameters parameters) {
		Boolean initialValue = parameters.initialValue();
		Boolean currentValue = parameters.currentValue();
		//
		Select<Boolean> select = SelectRenderer.render(builder -> builder
				.items(List.of(true, false).asJava())
				.label(parameters.label())
				.nullValueCaption("")
				.onValueChange(parameters.onValueChange())
				.readOnly(!parameters.calcEditable())
				.title(StringUtils.emptyToNull(parameters.calcNonEmptyNonEditableReasons().mkString("\n")))
				.value(currentValue)
				.width(parameters.width())
				.build());
		//
		{
			if (!Objects.equals(currentValue, initialValue)) {
				String modificationClassName;
				//
				if (initialValue == null) {
					modificationClassName = "added";
				} else if (currentValue == null) {
					modificationClassName = "deleted";
				} else {
					modificationClassName = "modified";
				}
				//
				select.addClassName(modificationClassName);
			}
		}
		//
		return ComponentView.of(select);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Boolean currentValue;

		@Nullable
		Boolean initialValue;

		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		Consumer<@Nullable Boolean> onValueChange;

		@Nullable
		String width;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder {
			// Lomboked
		}
	}
}
