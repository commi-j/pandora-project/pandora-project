package tk.labyrinth.pandora.views.demo;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;

@Route("manual-demo")
public class ManualDemoPage extends CssHorizontalLayout {

	@PostConstruct
	private void postConstruct() {
		CssVerticalLayout left;
		CssVerticalLayout right;
		{
			left = new CssVerticalLayout();
			//
			CssFlexItem.setFlexGrow(left, 1);
			add(left);
		}
		{
			right = new CssVerticalLayout();
			//
			CssFlexItem.setFlexGrow(right, 1);
			add(right);
		}
		Observable<String> textObservable = Observable.withInitialValue("123");
		{
			textObservable.subscribe((next, sink) -> {
				left.removeAll();
				right.removeAll();
				//
				TextField textField = new TextField();
				textField.setValue(next);
				textField.setValueChangeMode(ValueChangeMode.EAGER);
				textField.addValueChangeListener(event -> sink.accept(event.getValue()));
				//
				if (next.length() < 5) {
					left.add(textField);
				} else {
					right.add(textField);
				}
				//
				textField.focus();
			});
		}
	}
}
