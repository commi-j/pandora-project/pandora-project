package tk.labyrinth.pandora.views.demo;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.views.box.StringBoxRenderer;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Route("string-box-demo")
public class StringBoxDemoPage extends CssHorizontalLayout {

	private static final Pair<String, String> initialValue = Pair.of("first", null);

	private Pair<String, String> currentValue = initialValue;

	@PostConstruct
	private void postConstruct() {
		{
			addClassName(PandoraStyles.LAYOUT);
		}
		{
			refresh();
		}
	}

	public void refresh() {
		removeAll();
		//
		add(StringBoxRenderer
				.render(builder -> builder
						.currentValue(currentValue.getLeft())
						.initialValue(initialValue.getLeft())
						.label("First")
						.onValueChange(nextValue -> {
							currentValue = Pair.of(nextValue, currentValue.getRight());
							//
							refresh();
						})
						.build())
				.asVaadinComponent());
		add(StringBoxRenderer
				.render(builder -> builder
						.currentValue(currentValue.getRight())
						.initialValue(initialValue.getRight())
						.label("Second")
						.onValueChange(nextValue -> {
							currentValue = Pair.of(currentValue.getLeft(), nextValue);
							//
							refresh();
						})
						.build())
				.asVaadinComponent());
		add(StringBoxRenderer
				.render(builder -> builder
						.currentValue("constant")
						.initialValue("constant")
						.label("Constant")
						.onValueChange(nextValue -> {
							// no-op
						})
						.build())
				.asVaadinComponent());
	}
}
