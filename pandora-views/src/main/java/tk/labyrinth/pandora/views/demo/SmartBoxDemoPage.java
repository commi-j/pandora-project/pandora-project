package tk.labyrinth.pandora.views.demo;

import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.views.box.StringBoxRendererContributor;
import tk.labyrinth.pandora.views.smartbox.SmartBoxRenderer;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Route("smart-box-demo")
public class SmartBoxDemoPage extends CssHorizontalLayout {

	private static final Pair<String, String> initialValue = Pair.of("first", null);

	private final StringBoxRendererContributor stringBoxRendererContributor;

	private Pair<String, String> currentValue = initialValue;

	@PostConstruct
	private void postConstruct() {
		{
			addClassName(PandoraStyles.LAYOUT);
		}
		{
			refresh();
		}
	}

	private void refresh() {
		removeAll();
		//
		add(SmartBoxRenderer
				.<String>render(builder -> builder
						.contentRenderer(stringBoxRendererContributor.contributeBoxRenderer())
						.currentValue(currentValue.getLeft())
						.initialValue(initialValue.getLeft())
						.label("First")
						.onValueChange(nextValue -> {
							currentValue = Pair.of(nextValue, currentValue.getRight());
							//
							refresh();
						})
						.build())
				.asVaadinComponent());
		add(SmartBoxRenderer
				.<String>render(builder -> builder
						.contentRenderer(stringBoxRendererContributor.contributeBoxRenderer())
						.currentValue(currentValue.getRight())
						.initialValue(initialValue.getRight())
						.label("Second")
						.onValueChange(nextValue -> {
							currentValue = Pair.of(currentValue.getLeft(), nextValue);
							//
							refresh();
						})
						.build())
				.asVaadinComponent());
	}
}
