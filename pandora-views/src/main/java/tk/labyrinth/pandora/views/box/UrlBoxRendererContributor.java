package tk.labyrinth.pandora.views.box;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.views.domain.edition.NonEditableReasons;

import java.net.URL;

@Bean
@RequiredArgsConstructor
public class UrlBoxRendererContributor implements BoxRendererContributor<URL> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	public BoxRenderer<String> contributeBoxRenderer() {
		return BoxRenderer.of(
				String.class,
				parameters -> StringBoxRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.label(parameters.label())
						.nonEditableReasons(parameters.nonEditableReasons())
						.onValueChange(parameters.onValueChange())
						.build()));
	}

	@Override
	public BoxRenderer<URL> contributeBoxRenderer(BoxRendererRegistry registry, BoxRendererRegistry.Context context) {
		return BoxRenderer.of(
				URL.class,
				parameters -> UrlBoxRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.label(parameters.label())
						.nonEditableReasons(NonEditableReasons.of(parameters.nonEditableReasons()))
						.onValueChange(parameters.onValueChange())
						.build()));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BoxRendererRegistry.Context target) {
		return datatypeBaseRegistry.matches(target.getDatatype(), URL.class)
				? 1000
				: null;
	}
}
