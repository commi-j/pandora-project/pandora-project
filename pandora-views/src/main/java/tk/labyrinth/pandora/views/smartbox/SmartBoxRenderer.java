package tk.labyrinth.pandora.views.smartbox;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.sdk.ContainerWrapper;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.views.box.BoxRenderer;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class SmartBoxRenderer {

	public static <T> View render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static <T> View render(Parameters<T> parameters) {
		ContainerWrapper<CssGridLayout> layout = FunctionalComponents.createContainer(CssGridLayout::new);
		{
			layout.getContainer().setGridTemplateAreas(
					"label toolbar",
					"content content");
			layout.getContainer().setGridTemplateColumns("auto 1fr");
		}
		layout.handleChildren(() -> {
			layout.addAndConfigure(
					new Span(parameters.label()),
					label -> CssGridItem.setGridArea(label, "label"));
			//
			layout.addAndConfigure(
					ObjectUtils.consumeAndReturn(new CssHorizontalLayout(), toolbar -> {
						{
							// TODO: Do we modify this layout?
						}
						{
							// TODO: Add access level to actions. (Actually we should make them provided from outside).
							//
							toolbar.addStretch();
							//
							toolbar.add(ButtonRenderer.render(builder -> builder
									.enabled(!Objects.equals(parameters.currentValue(), parameters.initialValue()))
									.icon(VaadinIcon.ARROW_BACKWARD.create())
									.onClick(event -> parameters.onValueChange().accept(parameters.initialValue()))
									.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
									.build()));
							toolbar.add(ButtonRenderer.render(builder -> builder
									.enabled(parameters.currentValue() != null)
									.icon(VaadinIcon.CLOSE_SMALL.create())
									.onClick(event -> parameters.onValueChange().accept(null))
									.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
									.build()));
							//
							// TODO: Add tools.
						}
					}),
					toolbar -> CssGridItem.setGridArea(toolbar, "toolbar"));
			//
			layout.addAndConfigure(
					parameters.contentRenderer()
							.render(builder -> builder
									.currentValue(parameters.currentValue())
									.initialValue(parameters.initialValue())
									.nonEditableReasons(parameters.nonEditableReasons())
									.onValueChange(parameters.onValueChange())
									.build())
							.asVaadinComponent(),
					content -> CssGridItem.setGridArea(content, "content"));
		});
		return ComponentView.of(layout.getContainer());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<T> {

		@NonNull
		BoxRenderer<@Nullable T> contentRenderer;

		@Nullable
		T currentValue;

		@Nullable
		T initialValue;

		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<@Nullable T> onValueChange;

		public static class Builder<T> {
			// Lomboked
		}
	}
}
