package tk.labyrinth.pandora.views.domain.page.meta;

import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.functionalvaadin.view.View;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates a method that represents a renderer for a Page.<br>
 * It may take Parameters object as input, which may be treated as Page parameters.<br>
 * It must return a {@link View}.<br>
 * <br>
 * FIXME: For now these are only discovered within classes annotated with {@link PageRendererClass}.
 */
@Documented
@PandoraExtensionPoint
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
public @interface PageFunction {
	// empty
}
