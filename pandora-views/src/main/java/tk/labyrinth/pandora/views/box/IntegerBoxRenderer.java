package tk.labyrinth.pandora.views.box;

import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.simple.access.PandoraAccessLevel;
import tk.labyrinth.pandora.functionalvaadin.component.IntegerFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.views.style.PandoraColours;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class IntegerBoxRenderer {

	public static View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static View render(Parameters parameters) {
		Integer initialValue = parameters.initialValue();
		Integer currentValue = parameters.currentValue();
		//
		IntegerField integerField = IntegerFieldRenderer.render(builder -> builder
				.label(parameters.label())
				.max(parameters.max())
				.min(parameters.min())
				.onValueChange(parameters.onValueChange())
				.readOnly(!PandoraAccessLevel.canEdit(parameters.accessLevel()))
				.stepButtonsVisible(parameters.stepButtonsVisible())
				.value(parameters.currentValue())
				.valueChangeMode(parameters.valueChangeMode())
				.width(parameters.width())
				.build());
		//
		{
			if (!Objects.equals(currentValue, initialValue)) {
				String colour;
				//
				if (initialValue == null) {
					colour = PandoraColours.GREEN_50;
				} else if (currentValue == null) {
					colour = PandoraColours.RED_50;
				} else {
					colour = PandoraColours.BLUE_50;
				}
				//
				integerField.getStyle().set("border-radius", "4px");
				integerField.getStyle().set("box-shadow", "0 0 0 2px %s".formatted(colour));
			}
		}
		//
		return ComponentView.of(integerField);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		PandoraAccessLevel accessLevel;

		@Nullable
		Integer currentValue;

		@Nullable
		Integer initialValue;

		String label;

		@Nullable
		Integer max;

		@Nullable
		Integer min;

		Consumer<@Nullable Integer> onValueChange;

		@Nullable
		Boolean stepButtonsVisible;

		@Nullable
		ValueChangeMode valueChangeMode;

		@Nullable
		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
