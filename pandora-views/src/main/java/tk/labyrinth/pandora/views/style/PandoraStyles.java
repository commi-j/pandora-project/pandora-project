package tk.labyrinth.pandora.views.style;

/**
 * src/main/resources/META-INF/resources/frontend/style/pandora.css
 */
public class PandoraStyles {

	public static final String CARD = "pandora-card";

	public static final String LAYOUT = "pandora-layout";

	public static final String LAYOUT_SMALL = "pandora-layout-small";

	public static String defaultCellWidth() {
		return multiplyDefaultCellWidth(1);
	}

	public static String multiplyDefaultCellWidth(int multiplier) {
		return "%sem".formatted(24 * multiplier);
	}

	public static String repeatDefaultCellWidth(int number) {
		return "repeat(%s,%s)".formatted(number, defaultCellWidth());
	}
}
