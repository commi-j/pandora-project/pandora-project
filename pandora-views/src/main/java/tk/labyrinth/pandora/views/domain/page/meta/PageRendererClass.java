package tk.labyrinth.pandora.views.domain.page.meta;

import org.springframework.stereotype.Indexed;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Bean
@Documented
@Indexed
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PageRendererClass {
	// empty
}
