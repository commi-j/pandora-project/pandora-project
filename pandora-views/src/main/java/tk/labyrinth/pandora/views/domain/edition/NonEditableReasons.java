package tk.labyrinth.pandora.views.domain.edition;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Contracts:<br>
 * - Empty -> editable, otherwise not;<br>
 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
 * This means empty string can be used to render view non-editable without explaining the reason.
 * Poor UX, but possible.<br>
 */
@Value(staticConstructor = "of")
public class NonEditableReasons {

	@NonNull
	List<String> nonEditableReasons;

	public boolean isEditable() {
		return nonEditableReasons.isEmpty();
	}

	public boolean isNonEditable() {
		return !nonEditableReasons.isEmpty();
	}

	@Nullable
	public String toPrettyString() {
		String mergedString = nonEditableReasons
				.filter(nonEditableReason -> !nonEditableReason.isEmpty())
				.mkString("\n");
		//
		return !mergedString.isEmpty() ? mergedString : null;
	}

	public static NonEditableReasons empty() {
		return of(List.empty());
	}
}
