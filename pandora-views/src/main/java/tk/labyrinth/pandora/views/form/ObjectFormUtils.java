package tk.labyrinth.pandora.views.form;

import org.apache.commons.lang3.StringUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;

public class ObjectFormUtils {

	public static ObjectFormModel from(ObjectModel objectModel) {
		return ObjectFormModel.builder()
				.attributes(objectModel.getAttributes().map(ObjectFormUtils::from))
				.build();
	}

	public static ObjectFormAttribute from(ObjectModelAttribute attribute) {
		return ObjectFormAttribute.builder()
				.datatype(attribute.getDatatype())
				.displayName(prettifyName(attribute.getName()))
				.modelName(attribute.getName())
				.build();
	}

	public static String prettifyName(String name) {
		int from = 0;
		boolean uppercase = Character.isUpperCase(name.charAt(0));
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 1; i < name.length(); i++) {
			char character = name.charAt(i);
			//
			boolean nextUppercase = Character.isUpperCase(character);
			if (nextUppercase != uppercase) {
				int to = nextUppercase ? i : i - 1;
				//
				if (from != to) {
					stringBuilder
							.append(prettifyWord(name.substring(from, to), uppercase))
							.append(" ");
					//
					from = to;
				}
				//
				uppercase = nextUppercase;
			}
		}
		//
		stringBuilder.append(prettifyWord(name.substring(from), false));
		//
		return stringBuilder.toString().trim();
	}

	public static String prettifyWord(String word, boolean capital) {
		return capital ? word.toUpperCase() : StringUtils.capitalize(word);
	}
}
