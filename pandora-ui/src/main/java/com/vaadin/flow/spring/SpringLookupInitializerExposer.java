package com.vaadin.flow.spring;

import com.vaadin.flow.server.VaadinContext;
import org.springframework.web.context.WebApplicationContext;

/**
 * @see SpringLookupInitializer
 */
public class SpringLookupInitializerExposer {

	public static WebApplicationContext getApplicationContext(VaadinContext context) {
		return SpringLookupInitializer.getApplicationContext(context);
	}
}
