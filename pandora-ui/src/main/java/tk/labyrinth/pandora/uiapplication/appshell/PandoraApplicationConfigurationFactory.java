package tk.labyrinth.pandora.uiapplication.appshell;

import com.vaadin.flow.server.Constants;
import com.vaadin.flow.server.VaadinContext;
import com.vaadin.flow.spring.SpringApplicationConfigurationFactory;
import com.vaadin.flow.spring.SpringLookupInitializerExposer;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import org.springframework.core.env.Environment;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

/**
 * We use this one over Vaadin Spring implementation since the latter has a flaw of missing some properties.<br>
 * Properties handled by superclass:<br>
 * - com.vaadin.flow.spring.SpringServlet#PROPERTY_NAMES (protected field);<br>
 * Properties not handled:<br>
 * - {@link Constants#ALLOW_APPSHELL_ANNOTATIONS}
 *
 * @see SpringApplicationConfigurationFactory
 */
@LazyComponent
public class PandoraApplicationConfigurationFactory extends SpringApplicationConfigurationFactory {

	@Override
	protected ApplicationConfigurationImpl doCreate(VaadinContext context, java.util.Map<String, String> properties) {
		Environment environment = SpringLookupInitializerExposer.getApplicationContext(context).getBean(
				Environment.class);
		//
		Map<String, String> extraProperties = HashMap.of(Constants.ALLOW_APPSHELL_ANNOTATIONS, "true");
		// FIXME: Looks like this solution has no use since the value should always be true.
//				List
//						.of(
//								"vaadin.%s".formatted(Constants.ALLOW_APPSHELL_ANNOTATIONS))
//						.toMap(propertyName -> propertyName.substring("vaadin.".length()), environment::getProperty)
//						.filter(tuple -> tuple._2() != null);
		//
		return super.doCreate(context, extraProperties.merge(HashMap.ofAll(properties)).toJavaMap());
	}
}
