package tk.labyrinth.pandora.uiapplication.layout;

import com.vaadin.flow.component.Component;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class PandoraAttachableComponentWrapper {

	@NonNull
	Component component;

	@Nullable
	Double flexGrow;

	@Default
	@NonNull
	Boolean placeAtEnd = false;

	public static PandoraAttachableComponentWrapper of(Component component) {
		return PandoraAttachableComponentWrapper.builder()
				.component(component)
				.build();
	}
}
