package tk.labyrinth.pandora.uiapplication.navbar;

import com.vaadin.flow.component.Svg;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Height;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Width;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexboxAware;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.FlexDirection;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;

import java.nio.charset.StandardCharsets;

@Bean
@RequiredArgsConstructor
public class PandoraNavbarLogoProvider implements NavbarComponentsProvider {

	@Autowired(required = false)
	private PandoraApplicationTitleProvider applicationTitleProvider;

	@Override
	public List<PandoraAttachableComponentWrapper> provideNavbarComponents() {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.setAlignItems(AlignItems.CENTER);
		}
		{
			{
				// FIXME: The Svg here is wrapped in Div. We don't need, should get rid of it.
				Svg svg = new Svg(IoUtils.unchecked(() -> IOUtils.resourceToString(
						"logo_1.svg",
						StandardCharsets.UTF_8,
						getClass().getClassLoader())));
				//
				StyleUtils.setCssProperty(svg, Height.of("2em"));
				StyleUtils.setCssProperty(svg, Width.of("4em"));
				//
				// FIXME: We can not set size of Svg inside Div, so we need to restrain it with Flex. Should get rid of it.
				CssFlexboxAware.setAlignItems(svg, AlignItems.CENTER);
				CssFlexboxAware.setDisplayFlex(svg);
				CssFlexboxAware.setFlexDirection(svg, FlexDirection.COLUMN);
				//
				layout.add(new Div(svg));
			}
			{
				String applicationTitle = applicationTitleProvider != null
						? applicationTitleProvider.provideApplicationTitle()
						: "Pandora";
				//
				layout.add(new H3(applicationTitle));
				//
				UI.getCurrent().getPage().setTitle(applicationTitle);
			}
		}
		return List.of(PandoraAttachableComponentWrapper.of(layout));
	}
}
