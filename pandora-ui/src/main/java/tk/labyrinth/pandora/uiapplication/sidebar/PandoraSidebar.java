package tk.labyrinth.pandora.uiapplication.sidebar;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;

/**
 * @see ProvidesSidebarComponents
 * @see SidebarComponentsProvider
 */
@PrototypeScopedComponent
@RequiredArgsConstructor
public class PandoraSidebar extends CssVerticalLayout {

	@SmartAutowired
	private List<SidebarComponentsProvider> sidebarComponentProviders;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames("pandora-sidebar", PandoraStyles.LAYOUT_SMALL);
			//
			getElement().setAttribute("theme", "dark");
		}
	}

	public void update(List<PandoraAttachableComponentWrapper> sidebarComponentWrappers) {
		{
			removeAll();
		}
		{
			List<PandoraAttachableComponentWrapper> componentWrappers = sidebarComponentWrappers
					.appendAll(sidebarComponentProviders.flatMap(SidebarComponentsProvider::provideSidebarComponents));
			//
			List<PandoraAttachableComponentWrapper> placeAtStart = componentWrappers
					.filter(item -> !item.getPlaceAtEnd());
			List<PandoraAttachableComponentWrapper> placeAtEnd = componentWrappers
					.filter(PandoraAttachableComponentWrapper::getPlaceAtEnd);
			//
			placeAtStart.forEach(componentWrapper -> {
				Component component = componentWrapper.getComponent();
				//
				if (componentWrapper.getFlexGrow() != null) {
					CssFlexItem.setFlexGrow(component, componentWrapper.getFlexGrow());
				}
				//
				add(component);
			});
			//
			if (!placeAtEnd.isEmpty()) {
				if (!componentWrappers.exists(componentWrapper -> componentWrapper.getFlexGrow() != null)) {
					addStretch();
				}
				//
				placeAtEnd.forEach(componentWrapper -> {
					Component component = componentWrapper.getComponent();
					//
					if (componentWrapper.getFlexGrow() != null) {
						CssFlexItem.setFlexGrow(component, componentWrapper.getFlexGrow());
					}
					//
					add(component);
				});
			}
		}
	}
}
