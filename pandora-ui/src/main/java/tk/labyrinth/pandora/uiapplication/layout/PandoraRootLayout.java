package tk.labyrinth.pandora.uiapplication.layout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Overflow;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.uiapplication.navbar.PandoraNavbar;
import tk.labyrinth.pandora.uiapplication.navbar.ProvidesNavbarComponents;
import tk.labyrinth.pandora.uiapplication.sidebar.PandoraSidebar;
import tk.labyrinth.pandora.uiapplication.sidebar.ProvidesSidebarComponents;

@Deprecated
@AnonymousAllowed
@RequiredArgsConstructor
public class PandoraRootLayout extends CssGridLayout implements RouterLayout {

	private static final String contentAreaName = "content";

	private static final String navbarAreaName = "navbar";

	private static final String sidebarAreaName = "sidebar";

	private final PandoraNavbar navbar;

	private final PandoraSidebar sidebar;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames("pandora-root");
			//
			setGridTemplateAreas(
					"%1$s %1$s".formatted(navbarAreaName),
					"%s %s".formatted(sidebarAreaName, contentAreaName));
			setGridTemplateColumns("auto 1fr");
			setGridTemplateRows("auto 1fr");
			//
			setHeight("100vh");
		}
		{
			add(navbar, navbarAreaName);
			add(sidebar, sidebarAreaName);
		}
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		{
			if (content instanceof ProvidesNavbarComponents providesNavbarComponents) {
				navbar.update(providesNavbarComponents.provideNavbarComponents());
			} else {
				navbar.update(List.empty());
			}
		}
		{
			if (content instanceof ProvidesSidebarComponents providesSidebarComponents) {
				sidebar.update(providesSidebarComponents.provideSidebarComponents());
			} else {
				sidebar.update(List.empty());
			}
		}
		{
			StyleUtils.setCssProperty(content, Overflow.AUTO);
			//
			add((Component) content, contentAreaName);
		}
	}
}
