package tk.labyrinth.pandora.uiapplication.navbar;

import io.vavr.collection.List;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;

/**
 * This is to be registered as Spring bean to be able to provide extra components for {@link PandoraNavbar}.
 */
@PandoraExtensionPoint
public interface NavbarComponentsProvider {

	List<PandoraAttachableComponentWrapper> provideNavbarComponents();
}
