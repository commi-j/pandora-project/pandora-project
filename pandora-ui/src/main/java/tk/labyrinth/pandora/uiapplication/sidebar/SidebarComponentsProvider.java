package tk.labyrinth.pandora.uiapplication.sidebar;

import io.vavr.collection.List;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;

/**
 * This is to be registered as Spring bean to be able to provide extra components for {@link PandoraSidebar}.
 */
@PandoraExtensionPoint
public interface SidebarComponentsProvider {

	List<PandoraAttachableComponentWrapper> provideSidebarComponents();
}
