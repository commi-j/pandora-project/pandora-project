package tk.labyrinth.pandora.uiapplication.sidebar.tools;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.RouterLink;
import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.datatypes.render.DatatypeCheckerPage;
import tk.labyrinth.pandora.ui.tool.otel.OpentelemetrySpansPage;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;
import tk.labyrinth.pandora.uiapplication.sidebar.SidebarComponentsProvider;

@LazyComponent
public class PandoraSidebarToolsButtonProvider implements SidebarComponentsProvider {

	@SmartAutowired
	private List<ToolsButtonProvider> toolsButtonProviders;

	@Override
	public List<PandoraAttachableComponentWrapper> provideSidebarComponents() {
		Button toolsButton = ButtonRenderer.render(builder -> builder
				.icon(VaadinIcon.COGS.create())
				.text("Tools")
				.themeVariants(ButtonVariant.LUMO_TERTIARY)
				.build());
		//
		ContextMenu contextMenu = new ContextMenu(toolsButton);
		{
			contextMenu.setOpenOnClick(true);
		}
		{
			toolsButtonProviders
					.map(ToolsButtonProvider::provideToolsButton)
					.sortBy(Pair::getLeft)
					.map(Pair::getRight)
					.forEach(consumer -> consumer.accept(contextMenu));
			//
			contextMenu.addItem(ObjectUtils.consumeAndReturn(
					new RouterLink("Datatype Checker", DatatypeCheckerPage.class),
					routerLink -> routerLink.getElement().setAttribute("target", AnchorTarget.BLANK.getValue())));
			contextMenu.addItem(ObjectUtils.consumeAndReturn(
					new RouterLink("Opentelemetry Spans", OpentelemetrySpansPage.class),
					routerLink -> routerLink.getElement().setAttribute("target", AnchorTarget.BLANK.getValue())));
		}
		//
		return List.of(PandoraAttachableComponentWrapper.builder()
				.component(toolsButton)
				.placeAtEnd(true)
				.build());
	}
}
