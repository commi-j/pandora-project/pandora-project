package tk.labyrinth.pandora.uiapplication.navbar;

import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.core.meta.PandoraJavaContext;

@PandoraExtensionPoint
@PandoraJavaContext
public interface PandoraApplicationTitleProvider {

	String provideApplicationTitle();
}
