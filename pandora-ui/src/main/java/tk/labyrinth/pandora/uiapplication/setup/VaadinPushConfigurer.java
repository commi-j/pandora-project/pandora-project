package tk.labyrinth.pandora.uiapplication.setup;

import com.vaadin.flow.server.UIInitEvent;
import com.vaadin.flow.server.UIInitListener;
import com.vaadin.flow.shared.communication.PushMode;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
public class VaadinPushConfigurer implements UIInitListener {

	@Override
	public void uiInit(UIInitEvent event) {
		event.getUI().getPushConfiguration().setPushMode(PushMode.AUTOMATIC);
	}
}
