package tk.labyrinth.pandora.uiapplication.appshell;

import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.Uses;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.theme.Theme;
import de.f0rce.ace.AceEditor;

@CssImport("./style/pandora.css")
@CssImport("./style/pandora-lumo.css")
@CssImport("./style/pandora-colours.css")
@Theme("pandora-theme")
//
// FIXME: https://stackoverflow.com/questions/76636685/some-vaadin-components-not-rendered-after-upgrading-from-v22-to-v24
@Uses(Accordion.class)
@Uses(AceEditor.class)
@Uses(Avatar.class)
@Uses(Button.class)
@Uses(Checkbox.class)
@Uses(ComboBox.class)
@Uses(Dialog.class)
@Uses(Icon.class)
@Uses(IntegerField.class)
@Uses(MenuBar.class)
@Uses(Notification.class)
@Uses(RadioButtonGroup.class)
@Uses(Select.class)
@Uses(SplitLayout.class)
@Uses(Tabs.class)
@Uses(TextArea.class)
@Uses(TextField.class)
@Uses(TreeGrid.class)
public interface PandoraAppShellConfiguratorBase extends AppShellConfigurator {

	@Override
	default void configurePage(AppShellSettings settings) {
		// no-op
	}
}
