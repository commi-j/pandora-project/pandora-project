package tk.labyrinth.pandora.uiapplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PandoraUiApplicationModule {
	// empty
}
