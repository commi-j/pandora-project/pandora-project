package tk.labyrinth.pandora.uiapplication.navbar;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class PandoraNavbar extends CssHorizontalLayout {

	@SmartAutowired
	private List<NavbarComponentsProvider> navbarComponentsProviders;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames("pandora-navbar", PandoraStyles.LAYOUT_SMALL);
			//
			getElement().setAttribute("theme", "dark");
		}
	}

	public void update(List<PandoraAttachableComponentWrapper> navbarComponentWrappers) {
		{
			removeAll();
		}
		{
			List<PandoraAttachableComponentWrapper> componentWrappers = navbarComponentWrappers
					.appendAll(navbarComponentsProviders.flatMap(NavbarComponentsProvider::provideNavbarComponents));
			//
			List<PandoraAttachableComponentWrapper> placeAtStart = componentWrappers
					.filter(item -> !item.getPlaceAtEnd());
			List<PandoraAttachableComponentWrapper> placeAtEnd = componentWrappers
					.filter(PandoraAttachableComponentWrapper::getPlaceAtEnd);
			//
			placeAtStart.forEach(componentWrapper -> {
				Component component = componentWrapper.getComponent();
				//
				if (componentWrapper.getFlexGrow() != null) {
					CssFlexItem.setFlexGrow(component, componentWrapper.getFlexGrow());
				}
				//
				add(component);
			});
			//
			if (!placeAtEnd.isEmpty()) {
				if (!componentWrappers.exists(componentWrapper -> componentWrapper.getFlexGrow() != null)) {
					addStretch();
				}
				//
				placeAtEnd.forEach(componentWrapper -> {
					Component component = componentWrapper.getComponent();
					//
					if (componentWrapper.getFlexGrow() != null) {
						CssFlexItem.setFlexGrow(component, componentWrapper.getFlexGrow());
					}
					//
					add(component);
				});
			}
		}
	}
}
