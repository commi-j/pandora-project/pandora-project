package tk.labyrinth.pandora.uiapplication.navbar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.RouterLayout;
import io.vavr.collection.List;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;

/**
 * This is to be implemented by {@link Component Components} or {@link RouterLayout RouterLayouts}
 * to be able to provide extra components for {@link PandoraNavbar}.
 */
@PandoraExtensionPoint
public interface ProvidesNavbarComponents {

	List<PandoraAttachableComponentWrapper> provideNavbarComponents();
}
