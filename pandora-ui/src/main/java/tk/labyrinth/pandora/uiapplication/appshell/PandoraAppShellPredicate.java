package tk.labyrinth.pandora.uiapplication.appshell;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.startup.AppShellPredicate;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
public class PandoraAppShellPredicate implements AppShellPredicate {

	@Override
	public boolean isShell(Class<?> clz) {
		return AppShellConfigurator.class.isAssignableFrom(clz) && !ClassUtils.isAbstract(clz);
	}
}
