package tk.labyrinth.pandora.uiapplication.sidebar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.RouterLayout;
import io.vavr.collection.List;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.uiapplication.layout.PandoraAttachableComponentWrapper;

/**
 * This is to be implemented by {@link Component Components} or {@link RouterLayout RouterLayouts}
 * to be able to provide extra components for {@link PandoraSidebar}.
 */
@PandoraExtensionPoint
public interface ProvidesSidebarComponents {

	List<PandoraAttachableComponentWrapper> provideSidebarComponents();
}
