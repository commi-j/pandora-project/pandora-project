package tk.labyrinth.pandora.uiapplication.sidebar.tools;

import com.vaadin.flow.component.contextmenu.ContextMenu;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;

import java.util.function.Consumer;

@PandoraExtensionPoint
// TODO: Make a better name for class and method.
public interface ToolsButtonProvider {

	Pair<Integer, Consumer<ContextMenu>> provideToolsButton();
}
