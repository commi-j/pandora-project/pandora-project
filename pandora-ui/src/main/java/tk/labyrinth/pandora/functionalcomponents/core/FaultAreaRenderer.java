package tk.labyrinth.pandora.functionalcomponents.core;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextArea;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.io.output.StringBuilderWriter;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextAreaRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;

import java.io.PrintWriter;
import java.util.function.Function;

@Deprecated
public class FaultAreaRenderer {

	public static TextArea render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextArea render(Parameters parameters) {
		return TextAreaRenderer.render(builder -> builder
				.label(parameters.label())
				.readOnly(true)
				.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
						.icon(VaadinIcon.QUESTION_CIRCLE.create())
						.onClick(event -> ConfirmationViews.showComponentDialog(FaultAreaRenderer
								.render(faultAreaBuilder -> faultAreaBuilder
										.fault(parameters.fault())
										.build())))
						.themeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
						.build()))
				.value(renderFault(parameters.fault()))
				.build());
	}

	public static String renderFault(Throwable fault) {
		StringBuilder stringBuilder = new StringBuilder();
		//
		fault.printStackTrace(new PrintWriter(new StringBuilderWriter(stringBuilder)));
		//
		return stringBuilder.toString();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Throwable fault;

		@Nullable
		String label;

		public static class Builder {
			// Lomboked
		}
	}
}
