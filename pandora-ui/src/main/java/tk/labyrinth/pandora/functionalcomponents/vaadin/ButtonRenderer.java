package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@Deprecated
public class ButtonRenderer {

	public static Button render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Button render(Parameters parameters) {
		Button button = new Button();
		{
			Optional.ofNullable(parameters.disableOnClick()).ifPresent(button::setDisableOnClick);
			Optional.ofNullable(parameters.enabled()).ifPresent(button::setEnabled);
			Optional.ofNullable(parameters.height()).ifPresent(button::setHeight);
			Optional.ofNullable(parameters.icon()).ifPresent(button::setIcon);
			Optional.ofNullable(parameters.iconAfterText()).ifPresent(button::setIconAfterText);
			Optional.ofNullable(parameters.margin()).ifPresent(margin -> StyleUtils.setMargin(button, margin));
			Optional.ofNullable(parameters.onClick()).ifPresent(button::addClickListener);
			Optional.ofNullable(parameters.text()).ifPresent(button::setText);
			Optional.ofNullable(parameters.themeVariants()).ifPresent(themeVariants ->
					button.addThemeVariants(themeVariants.toArray(ButtonVariant[]::new)));
			Optional.ofNullable(parameters.tooltipText()).ifPresent(button::setTooltipText);
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(button));
		}
		return button;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<Button> configurer;

		@Nullable
		Boolean disableOnClick;

		@Nullable
		Boolean enabled;

		@Nullable
		String height;

		@Nullable
		Component icon;

		@Nullable
		Boolean iconAfterText;

		@Nullable
		String margin;

		@Nullable
		ComponentEventListener<ClickEvent<Button>> onClick;

		@Nullable
		String text;

		@Nullable
		List<ButtonVariant> themeVariants;

		@Nullable
		String tooltipText;

		public static class Builder {

			public Parameters.Builder themeVariants(List<ButtonVariant> themeVariants) {
				this.themeVariants = themeVariants;
				return this;
			}

			public Parameters.Builder themeVariants(ButtonVariant... themeVariants) {
				return themeVariants(List.of(themeVariants));
			}
		}
	}
}
