package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.textfield.NumberField;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;

import javax.annotation.CheckForNull;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

// TODO: Think if we need this one or can use VaadinComponents instead.
public class NumberFieldRenderer {

	public static NumberField render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public static NumberField render(Properties properties) {
		NumberField numberField = new NumberField();
		{
			Optional.ofNullable(properties.getLabel()).ifPresent(numberField::setLabel);
			Optional.ofNullable(properties.getValue()).ifPresent(numberField::setValue);
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			numberField.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					valueChanged.set(true);
					//
					event.getSource().setValue(event.getOldValue());
					//
					if (properties.getOnValueChange() != null) {
						properties.getOnValueChange().accept(event.getValue());
					}
				}
			});
			Optional.ofNullable(properties.getOnBlur()).ifPresent(onBlur -> numberField.addBlurListener(event ->
					onBlur.accept(valueChanged.getAndSet(false))));
		}
		{
			// Tricky solution to preserve focus on rerender.
			numberField.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		return numberField;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@CheckForNull
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@CheckForNull
		Consumer<Boolean> onBlur;

		@CheckForNull
		Consumer<@Nullable Double> onValueChange;

		@CheckForNull
		Double value;

		public static class Builder {
			// Lomboked
		}
	}
}
