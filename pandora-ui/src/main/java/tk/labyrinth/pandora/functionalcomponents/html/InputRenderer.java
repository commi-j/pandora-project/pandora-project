package tk.labyrinth.pandora.functionalcomponents.html;

import com.vaadin.flow.component.html.Input;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;

import java.util.Optional;
import java.util.function.Consumer;

public class InputRenderer {

	public static Input render(Properties properties) {
		Input input = new Input();
		//
		{
			input.setValue(properties.getValue());
		}
		{
			input.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					event.getSource().setValue(event.getOldValue());
					//
					Optional.ofNullable(properties.getOnValueChange())
							.ifPresent(onValueChange -> onValueChange.accept(event.getValue()));
				}
			});
		}
		{
			// Tricky solution to preserve focus on rerender.
			input.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		return input;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		Consumer<String> onValueChange;

		String value;
	}
}
