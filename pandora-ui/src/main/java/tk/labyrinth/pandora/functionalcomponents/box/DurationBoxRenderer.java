package tk.labyrinth.pandora.functionalcomponents.box;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.IntegerField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;

import java.time.Duration;
import java.util.LongSummaryStatistics;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.LongStream;

public class DurationBoxRenderer {

	public static DurationField render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public static DurationField render(Properties properties) {
		DurationField durationField = new DurationField();
		{
			Optional.ofNullable(properties.getLabel()).ifPresent(durationField::setLabel);
			Optional.ofNullable(properties.getReadOnly()).ifPresent(durationField::setReadOnly);
			Optional.ofNullable(properties.getValue()).ifPresent(durationField::setValue);
		}
		{
			durationField.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					event.getSource().setValue(event.getOldValue());
					//
					Optional.ofNullable(properties.getOnValueChange())
							.ifPresent(onValueChange -> onValueChange.accept(event.getValue()));
				}
			});
		}
		{
			// Tricky solution to preserve focus on rerender.
			durationField.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		return durationField;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		String label;

		@Nullable
		Consumer<Duration> onValueChange;

		@Nullable
		Boolean readOnly;

		@Nullable
		Duration value;

		public static class Builder {
			// Lomboked
		}
	}

	public static class DurationField extends CustomField<Duration> {

		private final IntegerField daysField = new IntegerField();

		private final IntegerField hoursField = new IntegerField();

		private final IntegerField minutesField = new IntegerField();

		private final IntegerField secondsField = new IntegerField();

		{
			{
				// FIXME: Crutch for proper rendering inside ObjectGrid.
				setWidth("100%");
			}
			{
				CssHorizontalLayout layout = new CssHorizontalLayout();
				{
					{
						daysField.setPrefixComponent(new Label("D"));
						hoursField.setPrefixComponent(new Label("H"));
						minutesField.setPrefixComponent(new Label("M"));
						secondsField.setPrefixComponent(new Label("S"));
					}
					List.of(daysField, hoursField, minutesField, secondsField).forEach(field -> {
						{
							// Overriding :host { width: 8em; }
							field.setWidth("25%");
						}
						layout.add(field);
					});
				}
				add(layout);
			}
		}

		@Nullable
		@Override
		protected Duration generateModelValue() {
			LongSummaryStatistics statistics = LongStream
					.of(
							daysField.getValue() != null ? TimeUnit.DAYS.toSeconds(daysField.getValue()) : -1,
							hoursField.getValue() != null ? TimeUnit.HOURS.toSeconds(hoursField.getValue()) : -1,
							minutesField.getValue() != null ? TimeUnit.MINUTES.toSeconds(minutesField.getValue()) : -1,
							secondsField.getValue() != null ? secondsField.getValue() : -1)
					.filter(value -> value != -1)
					.summaryStatistics();
			return statistics.getCount() > 0 ? Duration.ofSeconds(statistics.getSum()) : null;
		}

		@Override
		protected void setPresentationValue(@Nullable Duration duration) {
			if (duration != null) {
				long days = duration.toDaysPart();
				long hours = duration.toHoursPart();
				long minutes = duration.toMinutesPart();
				long seconds = duration.toSecondsPart();
				//
				daysField.setValue(days > 0 ? (int) days : null);
				hoursField.setValue(hours > 0 ? (int) hours : null);
				minutesField.setValue(minutes > 0 ? (int) minutes : null);
				secondsField.setValue((int) seconds);
			} else {
				daysField.setValue(null);
				hoursField.setValue(null);
				minutesField.setValue(null);
				secondsField.setValue(null);
			}
		}
	}
}