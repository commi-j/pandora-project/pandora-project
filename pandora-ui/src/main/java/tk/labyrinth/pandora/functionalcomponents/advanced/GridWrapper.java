package tk.labyrinth.pandora.functionalcomponents.advanced;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.function.ValueProvider;
import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.checker.nullness.qual.PolyNull;

import java.util.function.Function;

@RequiredArgsConstructor
public class GridWrapper<T> extends Composite<Grid<GridWrapper.ItemWrapper<T>>> {

	/**
	 * Used to identify existing item in the grid.
	 */
	private final Function<T, Object> identityObjectFunction;

	private void insertOrUpdateItem(
			TreeData<TreeGridWrapper.ItemWrapper<T>> treeData,
			@Nullable T expectedParent,
			int index,
			T nextItem) {
//		Object nextIdentityObject = identityObjectFunction.apply(nextItem);
//		//
//		TreeGridWrapper.ItemWrapper<T> actualParent = treeData.getParent(TreeGridWrapper.ItemWrapper.of(nextIdentityObject, null, null));
//		//
//		if (Objects.equals(
//				Optional.ofNullable(actualParent).map(TreeGridWrapper.ItemWrapper::getIdentityObject).orElse(null),
//				Optional.ofNullable(expectedParent).map(identityObjectFunction).orElse(null))) {
//			List<TreeGridWrapper.ItemWrapper<T>> actualItems = List.ofAll(actualParent != null
//					? treeData.getChildren(actualParent)
//					: treeData.getRootItems());
//			//
//			int actualIndex = actualItems.indexWhere(actualItem -> Objects.equals(
//					actualItem.getIdentityObject(),
//					nextIdentityObject));
//			//
//			if (actualIndex != -1) {
//				if (actualIndex == index) {
//					Object nextEqualsObject = equalsObjectFunction.apply(nextItem);
//					//
//					TreeGridWrapper.ItemWrapper<T> actualItem = actualItems.get(actualIndex);
//					//
//					actualItem.setItem(nextItem);
//					if (!Objects.equals(actualItem.getEqualsObject(), nextEqualsObject)) {
//						actualItem.setEqualsObject(nextEqualsObject);
//						getContent().getDataProvider().refreshItem(actualItem);
//					}
//					//
//					childrenFunction.apply(nextItem).zipWithIndex().forEach(tuple ->
//							insertOrUpdateItem(treeData, nextItem, tuple._2(), tuple._1()));
//				} else {
//					// TODO: Moved.
//					throw new NotImplementedException();
//				}
//			} else {
//				// TODO: New child.
//				throw new NotImplementedException();
//			}
//		} else {
//			// TODO: Parent changed.
//			throw new NotImplementedException();
//		}
	}

	@Override
	protected Grid<ItemWrapper<T>> initContent() {
		Grid<ItemWrapper<T>> content = super.initContent();
		{
			content.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
		}
		return content;
	}

	public Grid.Column<ItemWrapper<T>> addColumn(ValueProvider<T, ?> valueProvider) {
		return getContent().addColumn(item -> valueProvider.apply(item.getItem()));
	}

	public Grid.Column<ItemWrapper<T>> addComponentColumn(ValueProvider<T, ? extends Component> valueProvider) {
		return getContent().addComponentColumn(item -> valueProvider.apply(item.getItem()));
	}

	public void setItems(List<T> items) {
		getContent().setItems(items.map(item -> ItemWrapper.of(identityObjectFunction.apply(item), item)).asJava());
		//
//		getContent().getDataProvider();
//		TreeData<TreeGridWrapper.ItemWrapper<T>> treeData = getContent().getTreeData();
//		//
//		if (treeData.getRootItems().isEmpty()) {
//			getContent().setItems(items
//					.map(item -> GridWrapper.ItemWrapper.of(identityObjectFunction.apply(item), item))
//					.asJava());
//		} else {
//			items.zipWithIndex().forEach(tuple -> insertOrUpdateItem(treeData, tuple._2(), tuple._1()));
//		}
	}

	@AllArgsConstructor(staticName = "of")
	@Data
	@EqualsAndHashCode(onlyExplicitlyIncluded = true)
	@FieldDefaults(level = AccessLevel.PRIVATE)
	public static class ItemWrapper<T> {

		@EqualsAndHashCode.Include
		@NonNull
		final Object identityObject;

		@PolyNull
		T item;
	}
}
