package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import io.vavr.collection.List;
import io.vavr.control.Either;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class ContextMenuRenderer {

	public static ContextMenu render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static ContextMenu render(Parameters parameters) {
		ContextMenu contextMenu = new ContextMenu();
		{
			Optional.ofNullable(parameters.target()).ifPresent(contextMenu::setTarget);
			Optional.ofNullable(parameters.items()).ifPresent(items -> items.forEach(item -> {
				Either<String, Component> either = item.getLeft();
				//
				if (either.isRight()) {
					contextMenu.addItem(either.get(), item.getRight());
				} else {
					contextMenu.addItem(either.getLeft(), item.getRight());
				}
			}));
		}
		{
			Optional.ofNullable(parameters.customizer()).ifPresent(customizer -> customizer.accept(contextMenu));
		}
		return contextMenu;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<ContextMenu> customizer;

		List<Pair<Either<String, Component>, @Nullable ComponentEventListener<ClickEvent<MenuItem>>>> items;

		@Nullable
		Component target;

		public static class Builder {

			public Builder item(Component component) {
				return item(component, null);
			}

			public Builder item(Component component, @Nullable ComponentEventListener<ClickEvent<MenuItem>> clickListener) {
				Pair<Either<String, Component>, @Nullable ComponentEventListener<ClickEvent<MenuItem>>> item =
						Pair.of(Either.right(component), clickListener);
				//
				items = items != null ? items.append(item) : List.of(item);
				//
				return this;
			}

			public Builder item(String text) {
				return item(text, null);
			}

			public Builder item(String text, @Nullable ComponentEventListener<ClickEvent<MenuItem>> clickListener) {
				Pair<Either<String, Component>, @Nullable ComponentEventListener<ClickEvent<MenuItem>>> item =
						Pair.of(Either.left(text), clickListener);
				//
				items = items != null ? items.append(item) : List.of(item);
				//
				return this;
			}
		}
	}
}
