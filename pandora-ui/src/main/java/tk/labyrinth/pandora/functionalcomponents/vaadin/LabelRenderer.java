package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.html.Label;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@Deprecated // Span
public class LabelRenderer {

	public static Label render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public static Label render(Properties properties) {
		Label label = new Label();
		{
			Optional.ofNullable(properties.getText()).ifPresent(label::setText);
		}
		{
			Optional.ofNullable(properties.getCustomizer()).ifPresent(customizer -> customizer.accept(label));
		}
		return label;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		Consumer<Label> customizer;

		@Nullable
		String text;

		public static class Builder {
			// Lomboked
		}
	}
}
