package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.progressbar.ProgressBar;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class ProgressBarRenderer {

	public static ProgressBar render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static ProgressBar render(Parameters parameters) {
		ProgressBar progressBar = new ProgressBar();
		{
			Optional.ofNullable(parameters.height()).ifPresent(progressBar::setHeight);
			Optional.ofNullable(parameters.indeterminate()).ifPresent(progressBar::setIndeterminate);
			Optional.ofNullable(parameters.value()).ifPresent(progressBar::setValue);
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(progressBar));
		}
		return progressBar;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<ProgressBar> configurer;

		@Nullable
		String height;

		@Nullable
		Boolean indeterminate;

		@Nullable
		Double value;

		public static class Builder {
			// Lomboked
		}
	}
}
