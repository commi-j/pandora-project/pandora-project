package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.textfield.IntegerField;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

// TODO: Think if we need this one or can use VaadinComponents instead.
public class IntegerFieldRenderer {

	public static IntegerField render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public static IntegerField render(Properties properties) {
		IntegerField integerField = new IntegerField();
		{
			Optional.ofNullable(properties.getLabel()).ifPresent(integerField::setLabel);
			Optional.ofNullable(properties.getValue()).ifPresent(integerField::setValue);
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			integerField.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					valueChanged.set(true);
					//
					event.getSource().setValue(event.getOldValue());
					//
					if (properties.getOnValueChange() != null) {
						properties.getOnValueChange().accept(event.getValue());
					}
				}
			});
			Optional.ofNullable(properties.getOnBlur()).ifPresent(onBlur -> integerField.addBlurListener(event ->
					onBlur.accept(valueChanged.getAndSet(false))));
		}
		{
			// Tricky solution to preserve focus on rerender.
			integerField.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		return integerField;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable Integer> onValueChange;

		@Nullable
		Integer value;

		public static class Builder {
			// Lomboked
		}
	}
}
