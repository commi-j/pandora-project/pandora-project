package tk.labyrinth.pandora.functionalcomponents;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootConfiguration
public class PandoraFunctionalComponentsConfiguration {
	// empty
}
