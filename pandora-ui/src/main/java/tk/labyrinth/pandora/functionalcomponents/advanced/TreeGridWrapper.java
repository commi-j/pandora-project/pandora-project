package tk.labyrinth.pandora.functionalcomponents.advanced;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSelectionModel;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.function.ValueProvider;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.checker.nullness.qual.PolyNull;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@RequiredArgsConstructor
public class TreeGridWrapper<T> extends Composite<TreeGrid<TreeGridWrapper.ItemWrapper<T>>> {

	private final Function<T, List<T>> childrenFunction;

	/**
	 * Used to test if item was changed (i.e. information about children should be excluded from here).
	 */
	private final Function<T, Object> equalsObjectFunction;

	/**
	 * Used to identify existing item in the grid tree.
	 */
	private final Function<T, Object> identityObjectFunction;

	private List<T> extractChildren(ItemWrapper<T> itemWrapper) {
		return Objects.requireNonNull(
				childrenFunction.apply(itemWrapper.getItem()),
				() -> "Require non-null children: %s".formatted(itemWrapper));
	}

	@Deprecated
	private void insertOrUpdateItem2(
			TreeData<ItemWrapper<T>> treeData,
			@Nullable T nextParent,
			int nextIndex,
			T nextItem) {
		Object nextIdentityObject = identityObjectFunction.apply(nextItem);
		//
		boolean hasCurrentParent;
		ItemWrapper<T> currentParent;
		try {
			currentParent = treeData.getParent(ItemWrapper.ofIdentityObject(nextIdentityObject));
			hasCurrentParent = true;
		} catch (IllegalArgumentException ex) {
			// TODO: Check the text to reduce risk of misfire.
			//
			hasCurrentParent = false;
			currentParent = null;
		}
		//
		if (hasCurrentParent) {
			if (Objects.equals(
					Optional.ofNullable(currentParent).map(ItemWrapper::getIdentityObject).orElse(null),
					Optional.ofNullable(nextParent).map(identityObjectFunction).orElse(null))) {
				// Same parent.
				//
				List<ItemWrapper<T>> actualItems = List.ofAll(currentParent != null
						? treeData.getChildren(currentParent)
						: treeData.getRootItems());
				//
				int currentIndex = actualItems.indexWhere(actualItem -> Objects.equals(
						actualItem.getIdentityObject(),
						nextIdentityObject));
				//
				if (currentIndex != -1) {
					if (currentIndex == nextIndex) {
						Object nextEqualsObject = equalsObjectFunction.apply(nextItem);
						//
						ItemWrapper<T> actualItem = actualItems.get(currentIndex);
						//
						actualItem.setItem(nextItem);
						//
						if (!Objects.equals(actualItem.getEqualsObject(), nextEqualsObject)) {
							actualItem.setEqualsObject(nextEqualsObject);
							getContent().getDataProvider().refreshItem(actualItem);
						}
						//
						extractChildren(actualItem).zipWithIndex().forEach(tuple ->
								insertOrUpdateItem2(treeData, nextItem, tuple._2(), tuple._1()));
					} else {
						// TODO: Moved.
						throw new NotImplementedException();
					}
				} else {
					// TODO: New child. (Actually this should never happen since for the new child there won't be a parent found.
					throw new NotImplementedException();
				}
			} else {
				// TODO: Parent changed.
				throw new NotImplementedException();
			}
		} else {
			// TODO: Check order.
			//
			throw new NotImplementedException();
		}
	}

	private void insertOrUpdateItems(
			TreeData<ItemWrapper<T>> treeData,
			@Nullable ItemWrapper<T> nextParentWrapper,
			List<T> nextItems) {
		List<Pair<T, Object>> nextItemAndIdentityObjectPairs = nextItems
				.map(nextItem -> Pair.of(nextItem, identityObjectFunction.apply(nextItem)));
		//
		List<ItemWrapper<T>> currentItemWrappers = List.ofAll(treeData.getChildren(nextParentWrapper));
		//
		List<Pair<Integer, Integer>> nextAndCurrentIndices = nextItemAndIdentityObjectPairs
				.zipWithIndex()
				.map(tuple -> Pair.of(tuple._2(), currentItemWrappers.indexWhere(currentItemWrapper ->
						Objects.equals(currentItemWrapper.getIdentityObject(), tuple._1().getRight()))));
		//
		{
			// Handle next items.
			//
			if (nextAndCurrentIndices.exists(pair -> !Objects.equals(pair.getLeft(), pair.getRight()))) {
				// Order changed -> reordering children.
				//
				// TODO: This can be optimized by selective removal or moving.
				//
				currentItemWrappers.forEach(treeData::removeItem);
				//
				nextItemAndIdentityObjectPairs.forEach(pair -> {
					ItemWrapper<T> nextItemWrapper = ItemWrapper.of(
							pair.getRight(),
							equalsObjectFunction.apply(pair.getLeft()),
							pair.getLeft());
					//
					treeData.addItem(nextParentWrapper, nextItemWrapper);
					//
					getContent().getDataProvider().refreshItem(nextItemWrapper);
					//
					insertOrUpdateItems(treeData, nextItemWrapper, extractChildren(nextItemWrapper));
				});
			} else {
				// Order not changed -> just updating single items.
				//
				nextItemAndIdentityObjectPairs
						.zipWithIndex()
						.forEach(tuple -> {
							Object nextEqualsObject = equalsObjectFunction.apply(tuple._1().getLeft());
							//
							ItemWrapper<T> currentItemWrapper = currentItemWrappers.get(tuple._2());
							//
							currentItemWrapper.setItem(tuple._1().getLeft());
							//
							if (!Objects.equals(currentItemWrapper.getEqualsObject(), nextEqualsObject)) {
								currentItemWrapper.setEqualsObject(nextEqualsObject);
								//
								getContent().getDataProvider().refreshItem(currentItemWrapper);
							}
							//
							insertOrUpdateItems(treeData, currentItemWrapper, extractChildren(currentItemWrapper));
						});
			}
		}
		{
			// Handle deleted items.
			//
			currentItemWrappers
					.zipWithIndex()
					.reject(currentTuple ->
							nextAndCurrentIndices.exists(pair -> Objects.equals(pair.getLeft(), currentTuple._2())))
					.map(Tuple2::_1)
					.forEach(treeData::removeItem);
		}
	}

	@Override
	protected TreeGrid<ItemWrapper<T>> initContent() {
		TreeGrid<ItemWrapper<T>> result = new TreeGrid<>();
		{
			//
		}
		{
			//
		}
		return result;
	}

	public Grid.Column<ItemWrapper<T>> addColumn(ValueProvider<T, ?> valueProvider) {
		return getContent().addColumn(itemWrapper -> valueProvider.apply(itemWrapper.getItem()));
	}

	public <V extends Component> Grid.Column<ItemWrapper<T>> addComponentColumn(ValueProvider<T, V> valueProvider) {
		return getContent().addComponentColumn(itemWrapper -> valueProvider.apply(itemWrapper.getItem()));
	}

	public <V extends Component> Grid.Column<ItemWrapper<T>> addComponentHierarchyColumn(
			ValueProvider<T, V> valueProvider) {
		return getContent().addComponentHierarchyColumn(itemWrapper -> valueProvider.apply(itemWrapper.getItem()));
	}

	public Grid.Column<ItemWrapper<T>> addHierarchyColumn(ValueProvider<T, ?> valueProvider) {
		return getContent().addHierarchyColumn(itemWrapper -> valueProvider.apply(itemWrapper.getItem()));
	}

	public void addThemeVariants(GridVariant... variants) {
		getContent().addThemeVariants(variants);
	}

	public void expandAllItems() {
		expandRecursively(Integer.MAX_VALUE);
	}

	public void expandRecursively(int depth) {
		getContent().expandRecursively(getContent().getTreeData().getRootItems(), depth);
	}

	public void expandRootItems() {
		expandRecursively(0);
	}

	public List<TreeGridWrapper.ItemWrapper<T>> getChildrenOf(ItemWrapper<T> item) {
		return List.ofAll(getContent().getTreeData().getChildren(item));
	}

	public List<T> getRootItems() {
		return List.ofAll(getContent().getTreeData().getRootItems()).map(ItemWrapper::getItem);
	}

	public void setAllRowsVisible(boolean allRowsVisible) {
		getContent().setAllRowsVisible(allRowsVisible);
	}

	public void setItems(List<T> items) {
		TreeData<ItemWrapper<T>> treeData = getContent().getTreeData();
		//
		if (treeData.getRootItems().isEmpty()) {
			getContent().setItems(
					items
							.map(item -> ItemWrapper.of(
									identityObjectFunction.apply(item),
									equalsObjectFunction.apply(item),
									item))
							.asJava(),
					itemWrapper -> extractChildren(itemWrapper)
							.map(child -> ItemWrapper.of(
									identityObjectFunction.apply(child),
									equalsObjectFunction.apply(child),
									child))
							.asJava());
		} else {
			insertOrUpdateItems(treeData, null, items);
			//
			// FIXME: Trying to make it work. Then we need to only refresh actual changes.
			getContent().getDataProvider().refreshAll();
		}
	}

	public void setSelected(ItemWrapper<T> itemWrapper, boolean selected) {
		if (selected) {
			getContent().select(itemWrapper);
		} else {
			getContent().deselect(itemWrapper);
		}
	}

	public GridSelectionModel<ItemWrapper<T>> setSelectionMode(Grid.SelectionMode selectionMode) {
		return getContent().setSelectionMode(selectionMode);
	}

	public void updateItem(T item) {
		// TODO
		throw new NotImplementedException();
	}

	@AllArgsConstructor(staticName = "of")
	@Data
	@EqualsAndHashCode(onlyExplicitlyIncluded = true)
	@FieldDefaults(level = AccessLevel.PRIVATE)
	public static class ItemWrapper<T> {

		@EqualsAndHashCode.Include
		@NonNull
		final Object identityObject;

		@Nullable
		Object equalsObject;

		@PolyNull
		T item;

		public static <T> ItemWrapper<T> ofIdentityObject(Object identityObject) {
			return of(identityObject, null, null);
		}
	}
}
