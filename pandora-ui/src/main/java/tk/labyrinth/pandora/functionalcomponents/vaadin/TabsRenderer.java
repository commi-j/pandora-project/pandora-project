package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class TabsRenderer {

	public static Tabs render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public static Tabs render(Properties properties) {
		Tabs tabs = new Tabs();
		{
			Optional.ofNullable(properties.getTabs())
					.ifPresent(propertiesTabs -> tabs.add(propertiesTabs.toArray(Tab[]::new)));
		}
		return tabs;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		List<Tab> tabs;

		public static class Builder {
			// Lomboked
		}
	}
}