package tk.labyrinth.pandora.functionalcomponents.core;

import com.vaadin.flow.component.textfield.TextArea;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;

@Deprecated
public class FaultViews {

	public static TextArea createTextArea(Throwable fault) {
		TextArea textArea = FaultAreaRenderer.render(builder -> builder
				.fault(fault)
				.build());
		//
		textArea.setWidth("80vw");
		//
		return textArea;
	}

	public static void showDialog(Throwable fault) {
		Dialogs.show(createTextArea(fault));
	}
}
