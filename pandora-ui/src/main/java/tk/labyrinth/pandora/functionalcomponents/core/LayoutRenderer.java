package tk.labyrinth.pandora.functionalcomponents.core;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import java.util.Objects;
import java.util.function.Consumer;

public class LayoutRenderer {

	public static Div renderFlexStretch() {
		Div result = new Div();
		//
		CssFlexItem.setFlexGrow(result, 1);
		//
		return result;
	}

	public static CssHorizontalLayout renderHorizontal(@Nullable Component... children) {
		return renderHorizontal(
				layout -> {
					// no-op
				},
				children);
	}

	public static CssHorizontalLayout renderHorizontal(
			Consumer<CssHorizontalLayout> configurer,
			@Nullable Component... children) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		//
		configurer.accept(layout);
		//
		layout.add(List.of(children).filter(Objects::nonNull).asJava());
		//
		return layout;
	}

	public static CssVerticalLayout renderVertical(@Nullable Component... children) {
		return renderVertical(
				layout -> {
					// no-op
				},
				children);
	}

	public static CssVerticalLayout renderVertical(
			Consumer<CssVerticalLayout> configurer,
			@Nullable Component... children) {
		CssVerticalLayout layout = new CssVerticalLayout();
		//
		configurer.accept(layout);
		//
		layout.add(List.of(children).filter(Objects::nonNull).asJava());
		//
		return layout;
	}
}
