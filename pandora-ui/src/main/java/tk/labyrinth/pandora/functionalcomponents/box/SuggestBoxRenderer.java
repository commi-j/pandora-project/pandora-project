package tk.labyrinth.pandora.functionalcomponents.box;

import com.vaadin.flow.component.combobox.ComboBox;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.component.ComboBoxRenderer;

import java.util.function.Consumer;
import java.util.function.Function;

public class SuggestBoxRenderer {

	public static <T> ComboBox<T> render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static <T> ComboBox<T> render(Parameters<T> parameters) {
		return ComboBoxRenderer.render(builder -> builder
				.configurer(parameters.configurer())
				.items(parameters.items() != null ? parameters.items().asJava() : null)
				.itemsFunction(parameters.itemsFunction() != null ? parameters.itemsFunction().andThen(List::asJava) : null)
				.label(parameters.label())
				.onValueChange(parameters.onValueChange())
				.value(parameters.value())
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<T> {

		@Nullable
		Consumer<ComboBox<T>> configurer;

		@Nullable
		String errorMessage;

		@Nullable
		List<T> items;

		@Nullable
		Function<String, List<T>> itemsFunction;

		@Nullable
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		/**
		 * true if value was changed before, false otherwise.
		 */
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<T> onValueChange;

		@Nullable
		T value;

		@Nullable
		String width;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder<T> {
			// Lomboked
		}
	}
}
