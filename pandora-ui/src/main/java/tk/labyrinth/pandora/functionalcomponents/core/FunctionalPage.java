package tk.labyrinth.pandora.functionalcomponents.core;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.util.function.Consumer;

public abstract class FunctionalPage<P> extends Composite<FunctionalComponent<P>> {

	private Observable<P> propertiesObservable;

	protected void configureHost(FunctionalComponent<P> host) {
		// no-op
	}

	protected abstract P getInitialProperties();

	@Override
	protected FunctionalComponent<P> initContent() {
		FunctionalComponent<P> result;
		{
			FunctionalComponent<P> functionalComponent = FunctionalComponent.create(
					getInitialProperties(),
					this::render);
			//
			configureHost(functionalComponent);
			//
			propertiesObservable = functionalComponent.getPropertiesObservable();
			//
			result = functionalComponent;
		}
		return result;
	}

	protected abstract Component render(P properties, Consumer<P> sink);

	/**
	 * This method is for external use to enable features like integration with other services.
	 * Do not use it within this class and its inheritors.
	 *
	 * @return non-null
	 */
	public Observable<P> getPropertiesObservable() {
		return propertiesObservable;
	}
}
