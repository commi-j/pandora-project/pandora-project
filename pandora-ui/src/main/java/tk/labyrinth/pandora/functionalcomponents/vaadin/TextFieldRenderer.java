package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

@Deprecated
public class TextFieldRenderer {

	public static TextField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextField render(Parameters parameters) {
		TextField textField = new TextField();
		{
			Optional.ofNullable(parameters.label()).ifPresent(textField::setLabel);
			Optional.ofNullable(parameters.readOnly()).ifPresent(textField::setReadOnly);
			Optional.ofNullable(parameters.suffixComponent()).ifPresent(textField::setSuffixComponent);
			Optional.ofNullable(parameters.value()).ifPresent(textField::setValue);
			Optional.ofNullable(parameters.width()).ifPresent(textField::setWidth);
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			textField.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					valueChanged.set(true);
					//
					event.getSource().setValue(event.getOldValue());
					//
					if (parameters.onValueChange() != null) {
						parameters.onValueChange().accept(!event.getValue().isEmpty() ? event.getValue() : null);
					}
				}
			});
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> textField.addBlurListener(event ->
					onBlur.accept(valueChanged.getAndSet(false))));
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(textField));
		}
		{
			// Tricky solution to preserve focus on rerender.
			textField.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		return textField;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<TextField> configurer;

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable String> onValueChange;

		@Nullable
		Boolean readOnly;

		@Nullable
		Component suffixComponent;

		@Nullable
		String value;

		@Nullable
		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
