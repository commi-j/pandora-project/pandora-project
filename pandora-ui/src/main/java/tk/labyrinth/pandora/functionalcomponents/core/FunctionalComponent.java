package tk.labyrinth.pandora.functionalcomponents.core;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexboxAware;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

@Deprecated
public class FunctionalComponent<P> extends Div {

	@Getter(AccessLevel.PACKAGE)
	private final Observable<P> propertiesObservable;

	private FunctionalComponent(
			Observable<P> propertiesObservable,
			@NonNull BiFunction<P, Consumer<P>, Component> renderFunction) {
		this.propertiesObservable = propertiesObservable;
		//
		propertiesObservable.subscribe((nextProperties, propertiesSink) -> {
			Component nextComponent;
			//
			try {
				Component renderedComponent = renderFunction.apply(nextProperties, propertiesSink);
				//
				Objects.requireNonNull(renderedComponent, "renderedComponent");
				//
				nextComponent = renderedComponent;
			} catch (RuntimeException ex) {
				nextComponent = FaultAreaRenderer.render(builder -> builder
						.fault(ex)
						.build());
			}
			//
			CssFlexItem.setFlexGrow(nextComponent, 1);
			//
			Component currentComponent = getChildren().findFirst().orElse(null);
			//
			if (currentComponent != null) {
				remove(currentComponent);
				//
				add(nextComponent);
				//
				FocusHandler.restoreFocus();
			} else {
				add(nextComponent);
			}
		});
		{
			// FIXME: Find a way to design this without extra Div or make it customizable.
			//  Right now we hardcode this style because otherwise chained layout styles work bad.
			addClassNames(PandoraStyles.LAYOUT);
			//
			CssFlexboxAware.setDisplayFlex(this);
		}
	}

	public P getProperties() {
		return propertiesObservable.get();
	}

	public Flux<P> getPropertiesFlux() {
		return propertiesObservable.getFlux();
	}

	public void setProperties(P properties) {
		propertiesObservable.set(properties);
	}

	public static <P> FunctionalComponent<P> create(
			@NonNull P initialProperties,
			@NonNull BiFunction<P, Consumer<P>, Component> renderFunction) {
		return createWithExternalObservable(Observable.withInitialValue(initialProperties), renderFunction);
	}

	public static <P> FunctionalComponent<P> create(
			@NonNull P initialProperties,
			@NonNull Function<P, Component> renderFunction) {
		return create(initialProperties, (nextProperties, propertiesSink) -> renderFunction.apply(nextProperties));
	}

	public static <P> FunctionalComponent<P> createWithExternalObservable(
			@NonNull Observable<P> propertiesObservable,
			@NonNull BiFunction<P, Consumer<P>, Component> renderFunction) {
		return new FunctionalComponent<>(propertiesObservable, renderFunction);
	}

	public static <P> FunctionalComponent<P> createWithExternalObservable(
			@NonNull Observable<P> propertiesObservable,
			@NonNull Function<P, Component> renderFunction) {
		return createWithExternalObservable(
				propertiesObservable,
				(nextProperties, propertiesSink) -> renderFunction.apply(nextProperties));
	}
}
