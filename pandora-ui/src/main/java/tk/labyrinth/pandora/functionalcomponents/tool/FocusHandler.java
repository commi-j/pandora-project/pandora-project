package tk.labyrinth.pandora.functionalcomponents.tool;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.UI;
import io.vavr.Tuple3;
import io.vavr.collection.List;
import lombok.val;

import javax.annotation.CheckForNull;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Deprecated
public class FocusHandler {

	// FIXME: Clean up closed UIs.
	private static final ConcurrentMap<Integer, List<Tuple3<Long, Integer, Class<? extends Component>>>>
			uiIdToFocusPathMap = new ConcurrentHashMap<>();

	public static List<Tuple3<Long, Integer, Class<? extends Component>>> getComponentPath(
			@CheckForNull Component component) {
		List<Tuple3<Long, Integer, Class<? extends Component>>> result;
		{
			if (component != null) {
				ArrayList<Tuple3<Long, Integer, Class<? extends Component>>> componentPath = new ArrayList<>();
				{
					Component currentComponent = component;
					Component parentComponent;
					while ((parentComponent = currentComponent.getParent().orElse(null)) != null) {
						int currentComponentIndex = List.ofAll(parentComponent.getChildren()).indexOf(currentComponent);
						if (currentComponentIndex != -1) {
							componentPath.add(
									0,
									new Tuple3<>(
											parentComponent.getChildren().count(),
											currentComponentIndex,
											currentComponent.getClass()));
						} else {
							componentPath = new ArrayList<>();
							break;
						}
						//
						currentComponent = parentComponent;
					}
				}
				result = List.ofAll(componentPath);
			} else {
				result = List.empty();
			}
		}
		return result;
	}

	public static void onFocusChange(@CheckForNull Component component) {
		uiIdToFocusPathMap.put(UI.getCurrent().getUIId(), getComponentPath(component));
	}

	public static void restoreFocus() {
		val focusPath = uiIdToFocusPathMap.getOrDefault(UI.getCurrent().getUIId(), List.empty());
		//
		Component currentComponent = UI.getCurrent();
		for (int i = 0; i < focusPath.size(); i++) {
			val focusTuple = focusPath.get(i);
			//
			if (currentComponent.getChildren().count() == focusTuple._1()) {
				Component nextComponent = currentComponent.getChildren()
						.skip(focusTuple._2())
						.findFirst()
						.orElseThrow();
				if (nextComponent.getClass() == focusTuple._3()) {
					currentComponent = nextComponent;
				} else {
					currentComponent = null;
					break;
				}
			} else {
				currentComponent = null;
				break;
			}
		}
		//
		if (currentComponent instanceof Focusable<?> focusable) {
			focusable.focus();
		}
	}
}
