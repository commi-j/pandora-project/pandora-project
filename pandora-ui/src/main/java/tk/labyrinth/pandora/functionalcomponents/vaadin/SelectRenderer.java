package tk.labyrinth.pandora.functionalcomponents.vaadin;

import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.select.Select;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

@Deprecated
// TODO: Think if we need this one or can use VaadinComponents instead.
public class SelectRenderer {

	public static <T> Select<T> render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static <T> Select<T> render(Parameters<T> parameters) {
		Select<T> select = new Select<>();
		{
			// We always set generator to handle nulls as empty strings.
			// We also need to set it before items or value.
			select.setItemLabelGenerator(Optional
					.ofNullable(parameters.itemLabelGenerator())
					.orElse(item -> item != null ? item.toString() : ""));
		}
		{
			Optional.ofNullable(parameters.items()).ifPresent(select::setItems);
			Optional.ofNullable(parameters.label()).ifPresent(select::setLabel);
			Optional.ofNullable(parameters.value()).ifPresent(select::setValue);
			Optional.ofNullable(parameters.width()).ifPresent(select::setWidth);
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(select));
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			select.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					valueChanged.set(true);
					//
					event.getSource().setValue(event.getOldValue());
					//
					if (parameters.onValueChange() != null) {
						parameters.onValueChange().accept(event.getValue());
					}
				}
			});
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> select.addBlurListener(event ->
					onBlur.accept(valueChanged.getAndSet(false))));
		}
		{
			// Tricky solution to preserve focus on rerender.
			select.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		return select;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<T> {

		@Nullable
		Consumer<Select<T>> configurer;

		@Nullable
		ItemLabelGenerator<T> itemLabelGenerator;

		@Nullable
		List<T> items;

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable T> onValueChange;

		@Nullable
		T value;

		@Nullable
		String width;

		public static class Builder<T> {
			// Lomboked
		}
	}
}
