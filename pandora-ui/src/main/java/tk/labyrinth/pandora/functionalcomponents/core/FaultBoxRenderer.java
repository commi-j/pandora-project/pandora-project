package tk.labyrinth.pandora.functionalcomponents.core;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.vaadin.TextFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;

import java.util.function.Function;

public class FaultBoxRenderer {

	public static TextField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextField render(Parameters parameters) {
		return TextFieldRenderer.render(builder -> builder
				.label(parameters.label())
				.readOnly(true)
				.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
						.icon(VaadinIcon.QUESTION_CIRCLE.create())
						.onClick(event -> ConfirmationViews.showComponentDialog(FaultAreaRenderer
								.render(faultAreaBuilder -> faultAreaBuilder
										.fault(parameters.fault())
										.build())))
						.themeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
						.build()))
				.value(parameters.fault().toString())
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Throwable fault;

		@Nullable
		String label;

		public static class Builder {
			// Lomboked
		}
	}
}
