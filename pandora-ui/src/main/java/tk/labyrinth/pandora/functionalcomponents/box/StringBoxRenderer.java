package tk.labyrinth.pandora.functionalcomponents.box;

import com.vaadin.flow.component.textfield.TextField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.ui.style.PandoraColours;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

@Deprecated
public class StringBoxRenderer {

	public static TextField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextField render(Parameters parameters) {
		TextField textField = new TextField();
		{
			textField.setLabel(parameters.label());
			textField.setReadOnly(!parameters.calcEditable());
			textField.setTitle(StringUtils.emptyToNull(parameters.calcNonEmptyNonEditableReasons().mkString("\n")));
			textField.setValue(parameters.value() != null ? parameters.value() : "");
			textField.setWidth(parameters.width());
		}
		{
			Optional.ofNullable(parameters.errorMessage()).ifPresent(errorText -> {
				textField.getStyle().set("border-radius", "4px");
				textField.getStyle().set("box-shadow", "0 0 0 2px %s".formatted(PandoraColours.RED_50));
			});
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			textField.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					valueChanged.set(true);
					//
					event.getSource().setValue(event.getOldValue());
					//
					Optional.ofNullable(parameters.onValueChange()).ifPresent(onValueChange ->
							onValueChange.accept(!event.getValue().isEmpty() ? event.getValue() : null));
				}
			});
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur ->
					textField.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false))));
		}
		{
			// Tricky solution to preserve focus on rerender.
			textField.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(textField));
		}
		return textField;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<TextField> configurer;

		@Nullable
		String errorMessage;

		@Nullable
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		/**
		 * true if value was changed before, false otherwise.
		 */
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<String> onValueChange;

		@Nullable
		String value;

		@Nullable
		String width;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder {
			// Lomboked
		}
	}
}
