package tk.labyrinth.pandora.webcomponents;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.PropertyDescriptor;
import com.vaadin.flow.component.PropertyDescriptors;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import lombok.NonNull;

/**
 * <a href="https://vaadin.com/directory/component/dacelislspinners">https://vaadin.com/directory/component/dacelislspinners</a>
 */
@JsModule("spinner-element/spinner-element.js")
@NpmPackage(value = "spinner-element", version = "0.0.4")
@Tag("spinner-element")
public class SpinnerElement extends Component {

	public enum Tipo {
		BARS,
		BOUNCE,
		BUBBLES,
		CIRCLES,
		CUBE,
		DEFAULT,
		DOTS,
		FOLDING,
		INFINITY,
		RIPPLE
	}

	private static final PropertyDescriptor<String, String> tipoProperty = PropertyDescriptors.propertyWithDefault(
			"tipo",
			"default");

	private static final PropertyDescriptor<Boolean, Boolean> visibleProperty = PropertyDescriptors.propertyWithDefault(
			"visible",
			false);

	public Tipo getTipo() {
		return Tipo.valueOf(tipoProperty.get(this).toUpperCase());
	}

	public boolean isVisible() {
		return visibleProperty.get(this);
	}

	public void setTipo(@NonNull Tipo tipo) {
		tipoProperty.set(this, tipo.name().toLowerCase());
	}

	public void setVisible(boolean visible) {
		visibleProperty.set(this, visible);
	}
}
