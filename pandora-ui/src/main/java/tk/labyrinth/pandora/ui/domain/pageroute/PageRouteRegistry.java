package tk.labyrinth.pandora.ui.domain.pageroute;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.TypedObjectChangeListener;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.page.PandoraPage;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Bean
public class PageRouteRegistry {

	private final ConcurrentMap<String, PageRoute> pageRoutes = new ConcurrentHashMap<>();

	@SmartAutowired
	private TypedObjectSearcher<PandoraPage> pandoraPageSearcher;

	public PageRoute findMatch(String locationString) {
		return List.ofAll(pageRoutes.values())
				.filter(pageRoute -> Objects.equals(pageRoute.getPath(), locationString))
				.getOrNull();
	}

	public PandoraPage getPage(PageRoute pageRoute) {
		return pandoraPageSearcher.getSingle(pageRoute.getPageReference());
	}

	@Bean
	@RequiredArgsConstructor
	public static class RouteChangeListener extends TypedObjectChangeListener<PageRoute, PathPageRouteReference> {

		private final PageRouteRegistry pageRouteRegistry;

		@Override
		protected void onObjectChange(TypedObjectChangeEvent<PageRoute, PathPageRouteReference> event) {
			if (event.hasNextObject()) {
				PageRoute nextObject = event.getNextObjectOrFail();
				//
				pageRouteRegistry.pageRoutes.put(nextObject.getPath(), nextObject);
			} else {
				pageRouteRegistry.pageRoutes.remove(event.getPrimaryReference().getPath());
			}
		}
	}
}
