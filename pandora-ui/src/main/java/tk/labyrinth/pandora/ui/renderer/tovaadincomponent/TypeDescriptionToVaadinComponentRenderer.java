package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
public class TypeDescriptionToVaadinComponentRenderer extends ToVaadinComponentRendererBase<TypeDescription> {

	public static final int DISTANCE = GenericObjectToInlineVaadinComponentRenderer.DISTANCE - 1;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, TypeDescription value) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			StyleUtils.setCssProperty(layout, WhiteSpace.PRE_WRAP);
		}
		{
			layout.add(context.getRendererRegistry().render(
					ToVaadinComponentRendererRegistry.Context.builder()
							.datatype(TypeSignature.class)
							.hints(List.empty())
							.build(),
					TypeSignature.ofValid(value.getFullName())));
			//
			if (value.getLowerBound() != null) {
				layout.add(" super ");
				//
				layout.add(context.getRendererRegistry().render(
						ToVaadinComponentRendererRegistry.Context.builder()
								.datatype(TypeDescription.class)
								.hints(List.empty())
								.build(),
						value.getLowerBound()));
			}
			//
			if (value.getUpperBound() != null) {
				layout.add(" extends ");
				{
					List<Component> components = List.ofAll(value.getUpperBound())
							.map(upperBoundElement -> context.getRendererRegistry().render(
									ToVaadinComponentRendererRegistry.Context.builder()
											.datatype(TypeDescription.class)
											.hints(List.empty())
											.build(),
									upperBoundElement));
					//
					layout.add(components.head());
					components.tail().forEach(component -> {
						layout.add(",");
						layout.add(component);
					});
				}
			}
			//
			if (value.getParameters() != null) {
				layout.add("<");
				{
					List<Component> components = List.ofAll(value.getParameters())
							.map(parameter -> context.getRendererRegistry().render(
									ToVaadinComponentRendererRegistry.Context.builder()
											.datatype(TypeDescription.class)
											.hints(List.empty())
											.build(),
									parameter));
					//
					layout.add(components.head());
					components.tail().forEach(component -> {
						layout.add(",");
						layout.add(component);
					});
				}
				layout.add(">");
			}
			//
			if (value.getArrayDepth() != null) {
				layout.add(StringUtils.repeat("[]", value.getArrayDepth()));
			}
		}
		return layout;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), TypeDescription.class)
				? DISTANCE
				: null;
	}

	public static String getLongName(String fullName) {
		int indexOfColon = fullName.indexOf(':');
		return indexOfColon != -1 ? fullName.substring(indexOfColon + 1) : fullName;
	}

	public static String renderSimple(TypeDescription typeDescription) {
		String result;
		{
			String resultBeforeArray;
			{
				String longName = getLongName(typeDescription.getFullName());
				if (typeDescription.getParameters() != null) {
					resultBeforeArray = "%s<%s>".formatted(
							longName,
							List.ofAll(typeDescription.getParameters())
									.map(TypeDescriptionToVaadinComponentRenderer::renderSimple)
									.mkString(","));
				} else {
					resultBeforeArray = longName;
				}
			}
			result = "%s%s".formatted(
					resultBeforeArray,
					Optional.ofNullable(typeDescription.getArrayDepth())
							.map(arrayDepth -> StringUtils.repeat("[]", arrayDepth))
							.orElse(""));
		}
		return result;
	}
}
