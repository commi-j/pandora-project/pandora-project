package tk.labyrinth.pandora.ui.domain.state;

import lombok.Value;

// TODO: May be scope should be a combination of layer and unique name (e.g. Class).
public interface UiStateScope {

	SimpleStateScope PAGE = new SimpleStateScope("PAGE");

	SimpleStateScope SESSION = new SimpleStateScope("SESSION");

	SimpleStateScope UI = new SimpleStateScope("UI");

	@Value
	class SimpleStateScope {

		String name;
	}
}
