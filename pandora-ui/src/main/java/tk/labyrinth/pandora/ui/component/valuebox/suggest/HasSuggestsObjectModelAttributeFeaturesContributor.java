package tk.labyrinth.pandora.ui.component.valuebox.suggest;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeaturesContributor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;

@LazyComponent
@RequiredArgsConstructor
public class HasSuggestsObjectModelAttributeFeaturesContributor implements ObjectModelAttributeFeaturesContributor {

	@Override
	public List<ObjectModelAttributeFeature> contributeFeatures(Member javaMember) {
		List<ObjectModelAttributeFeature> result;
		{
			MergedAnnotation<HasSuggests> hasSuggestsMergedAnnotation = MergedAnnotations
					.from((AnnotatedElement) javaMember)
					.get(HasSuggests.class);
			//
			if (hasSuggestsMergedAnnotation.isPresent()) {
				HasSuggests hasSuggestsAnnotation = hasSuggestsMergedAnnotation.synthesize();
				//
				if (hasSuggestsAnnotation.provider() == NoOpSuggestProvider.class) {
					throw new IllegalArgumentException("Require proper SuggestProvider");
				}
				//
				result = List.of(HasSuggestsObjectModelAttributeFeature.builder()
						.suggestProviderClass(hasSuggestsAnnotation.provider())
						.build());
			} else {
				result = List.empty();
			}
		}
		return result;
	}
}
