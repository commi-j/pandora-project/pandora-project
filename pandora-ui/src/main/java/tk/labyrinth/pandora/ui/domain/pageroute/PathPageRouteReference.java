package tk.labyrinth.pandora.ui.domain.pageroute;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class PathPageRouteReference implements Reference<PageRoute> {

	String path;

	public GenericReference<PageRoute> toGenericReference() {
		return GenericReference.of(
				PageRoute.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(PageRoute.PATH_ATTRIBUTE_NAME, path)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				PageRoute.MODEL_CODE,
				PageRoute.PATH_ATTRIBUTE_NAME,
				path);
	}

	public static PathPageRouteReference from(GenericReference<?> reference) {
		if (!isPathPageRouteReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(PageRoute.PATH_ATTRIBUTE_NAME));
	}

	public static PathPageRouteReference from(PageRoute value) {
		return of(value.getPath());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static PathPageRouteReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static String getPath(GenericReference<PageRoute> pathPageRouteReference) {
		return pathPageRouteReference.getAttributeValue(PageRoute.PATH_ATTRIBUTE_NAME);
	}

	public static boolean isPathPageRouteReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), PageRoute.MODEL_CODE) &&
				reference.hasAttribute(PageRoute.PATH_ATTRIBUTE_NAME);
	}
}
