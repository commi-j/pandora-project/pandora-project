package tk.labyrinth.pandora.ui.component.genericobject;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;

/**
 * Does not force attributes to:<br>
 * - be non-null;<br>
 * - be unique;<br>
 * - be sorted;<br>
 * - have fit name;<br>
 * - have non-null value;<br>
 */
@Value(staticConstructor = "of")
public class EditableGenericObject {

	@NonNull
	List<EditableGenericObjectAttribute> attributes;

	public GenericObject toGenericObject() {
		List<GenericObjectAttribute> convertedAttributes = attributes.map(attribute -> {
			if (attribute == null) {
				throw new IllegalArgumentException();
			}
			return attribute.toGenericObjectAttribute();
		});
		//
		if (convertedAttributes.distinctBy(GenericObjectAttribute::getName).size() < convertedAttributes.size()) {
			throw new IllegalArgumentException();
		}
		//
		return GenericObject.of(convertedAttributes.sortBy(GenericObjectAttribute::getName));
	}

	public static EditableGenericObject empty() {
		return of(List.empty());
	}

	public static EditableGenericObject from(GenericObject genericObject) {
		return of(genericObject.getAttributes().map(EditableGenericObjectAttribute::from));
	}
}
