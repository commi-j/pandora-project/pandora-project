package tk.labyrinth.pandora.ui.datatypes.render;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.treegrid.TreeGrid;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.routing.routetarget.annotation.RouteTarget;

import java.util.function.Consumer;

@RequiredArgsConstructor
@RouteTarget
@Slf4j
public class DatatypeCheckerTablePage extends FunctionalPage<DatatypeCheckerTablePage.Parameters> {

	private final ToStringRendererRegistry toStringRendererRegistry;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	@SmartAutowired
	private TypedObjectSearcher<DatatypeBase> datatypeBaseSearcher;

	@Override
	protected Parameters getInitialProperties() {
		return Parameters.builder()
				.build();
	}

	@Override
	protected Component render(Parameters properties, Consumer<Parameters> sink) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
		}
		{
			TreeGrid<DatatypeBase> grid = new TreeGrid<>();
			{
				grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			}
			{
				grid
						.addHierarchyColumn(DatatypeBase::getName)
						.setHeader("Name")
						.setResizable(true);
				grid
						.addColumn(DatatypeBase::getSignature)
						.setHeader("Signature")
						.setResizable(true)
						.setSortable(true);
				grid
						.addComponentColumn(item -> GridUtils.renderValueList(item.getAliases()))
						.setHeader("Aliases")
						.setResizable(true);
				grid
						.addComponentColumn(item -> {
							val selectionResult = toStringRendererRegistry.selectRenderers(ToStringRendererRegistry.Context.builder()
									.datatype(Datatype.builder()
											.baseReference(AliasDatatypeBaseReference.of(item.getAliases().get(0)))
											.parameters(null)
											.build())
									.hints(List.empty())
									.build());
							//
							return GridUtils.renderValueList(selectionResult.getEvaluatedHandlers()
									.filter(pair -> pair.getLeft() != null)
									.flatMap(Pair::getRight)
									.map(ToStringRenderer::getClass)
									.map(Class::getSimpleName));
						})
						.setHeader("To String Renderers")
						.setResizable(true);
				grid
						.addComponentColumn(item -> {
//							val selectionResult = toVaadinComponentRendererRegistry.selectRenderers(ToStringRendererRegistry.Context.builder()
//									.datatype(Datatype.builder()
//											.baseReference(AliasDatatypeBaseReference.of(item.getAliases().get(0)))
//											.parameters(null)
//											.build())
//									.hints(List.empty())
//									.build());
//							//
//							return GridUtils.renderList(selectionResult.getEvaluatedHandlers()
//									.filter(pair -> pair.getLeft() != null)
//									.flatMap(Pair::getRight)
//									.map(ToStringRenderer::getClass)
//									.map(Class::getSimpleName));
							return new Div();
						})
						.setHeader("To Vaadin Component Renderers")
						.setResizable(true);
			}
			{
				List<DatatypeBase> datatypeBases = datatypeBaseSearcher.searchAll();
				//
				Map<AliasDatatypeBaseReference, DatatypeBase> referenceToDatatypeBaseMap = datatypeBases
						.flatMap(datatypeBase -> datatypeBase.getAliases().map(alias -> Pair.of(alias, datatypeBase)))
						.toMap(
								pair -> AliasDatatypeBaseReference.of(pair.getLeft()),
								Pair::getRight);
				//
				Map<AliasDatatypeBaseReference, List<DatatypeBase>> parentReferenceToDatatypeBasesMap = datatypeBases
						.groupBy(DatatypeBase::getParentReference);
				//
				List<DatatypeBase> nodesWithMissingParents = parentReferenceToDatatypeBasesMap
						.filter(tuple -> tuple._1() != null)
						.filter(tuple -> !referenceToDatatypeBaseMap.containsKey(tuple._1()))
						.flatMap(Tuple2::_2)
						.toList();
				//
				List<DatatypeBase> rootItems = parentReferenceToDatatypeBasesMap.get(null).getOrElse(List.empty())
						.appendAll(nodesWithMissingParents);
				//
				grid.setItems(
						rootItems.asJava(),
						item -> item.getAliases()
								.flatMap(alias -> parentReferenceToDatatypeBasesMap.getOrElse(
										AliasDatatypeBaseReference.of(alias),
										List.empty()))
								.asJava());
				//
				grid.expandRecursively(rootItems.asJava(), Integer.MAX_VALUE);
			}
			layout.add(grid);
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {
		// empty
	}
}
