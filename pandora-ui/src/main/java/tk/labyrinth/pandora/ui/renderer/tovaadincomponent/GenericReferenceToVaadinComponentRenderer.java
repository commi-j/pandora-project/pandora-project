package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectReferenceItem;
import tk.labyrinth.pandora.ui.renderer.custom.GenericObjectReferenceToItemRenderer;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

import javax.annotation.CheckForNull;
import java.util.Objects;

@RequiredArgsConstructor
@SpringComponent
public class GenericReferenceToVaadinComponentRenderer extends ToVaadinComponentRendererBase<GenericReference<?>> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;
	//
	// FIXME: Decide what link we want to have here.
//	private final TablePredicateHandler predicateHandler;

	private final ObjectProvider<GenericObjectReferenceToItemRenderer> objectReferenceToItemRendererProvider;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@Override
	@SuppressWarnings("unchecked")
	protected Component renderNonNull(Context context, GenericReference<?> value) {
		ObjectModel objectModel = objectModelSearcher.findSingle(value.getModelReference());
		//
		Objects.requireNonNull(objectModel, "objectModel");
		//
		GenericObjectReferenceToItemRenderer objectReferenceToItemRenderer = objectReferenceToItemRendererProvider
				.getObject();
		//
		GenericObjectReferenceItem referenceItem = objectReferenceToItemRenderer.renderNonNull(
				GenericObjectReference.from((GenericReference<GenericObject>) value));
		//
		return new Anchor(
				"TODO",
//				"beholder/%s?predicate=%s".formatted(
//						BeholderModelTablePathReference.from(objectModel).toString(),
//						predicateHandler.referenceToQueryString(value)),
				referenceItem.toDisplayString());
	}

	@CheckForNull
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.isReference(context.getDatatype())
				? ToStringRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}
}
