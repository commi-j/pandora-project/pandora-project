package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

@LazyComponent
@RequiredArgsConstructor
public class MethodFullSignatureToVaadinComponentRenderer extends ToVaadinComponentRendererBase<MethodFullSignature> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, MethodFullSignature value) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.add(context.getRendererRegistry().render(
					ToVaadinComponentRendererRegistry.Context.builder()
							.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(ClassSignature.class))
							.hints(List.empty())
							.build(),
					ClassSignature.from(value.getTypeFullSignature())));
			layout.add("#");
			layout.add(value.getName());
			layout.add("(");
			{
				List<Component> components = List.ofAll(value.getParameters())
						.map(parameter -> context.getRendererRegistry().render(
								ToVaadinComponentRendererRegistry.Context.builder()
										.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(parameter.getClass()))
										.hints(List.empty())
										.build(),
								parameter));
				//
				if (!components.isEmpty()) {
					layout.add(components.head());
					components.tail().forEach(component -> {
						layout.add(",");
						layout.add(component);
					});
				}
			}
			layout.add(")");
		}
		return layout;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), MethodFullSignature.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_TWO
				: null;
	}
}
