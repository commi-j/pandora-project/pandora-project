package tk.labyrinth.pandora.ui.domain.state.model;

import com.vaadin.flow.component.HasElement;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.MergedAnnotations;
import reactor.core.Disposable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.domain.state.StringUiStateAttributeHandler;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @see UiStateAttribute
 */
@LazyComponent
@RequiredArgsConstructor
public class UiStateAttributeBeanPostProcessor implements BeanPostProcessor {

	private final ObjectProvider<ConverterRegistry> converterRegistryProvider;

	private final ObjectProvider<GenericUiStateBinder> uiStateBinderProvider;

	@SuppressWarnings("unchecked")
	private <T> void attachUiStateAttributeHandling(
			HasElement hasElement,
			Field uiStateAttributeField,
			GenericUiStateBinder uiStateBinder) {
		AtomicReference<Disposable> disposableReference = new AtomicReference<>(null);
		//
		hasElement.getElement().addAttachListener(event -> {
			Observable<Optional<T>> uiStateAttributeObservable;
			try {
				uiStateAttributeField.setAccessible(true);
				//
				uiStateAttributeObservable = (Observable<Optional<T>>) uiStateAttributeField.get(hasElement);
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			}
			//
			ConverterRegistry converterRegistry = converterRegistryProvider.getObject();
			//
			UiStateAttribute uiStateAttributeAnnotation = MergedAnnotations.from(uiStateAttributeField)
					.get(UiStateAttribute.class)
					.synthesize();
			//
			disposableReference.set(uiStateBinder.bindAttribute(
					StringUiStateAttributeHandler.of(
							UiStateAttributeUtils.computeAttributeName(
									uiStateAttributeAnnotation,
									uiStateAttributeField.getName()),
							converterRegistry.convert(
									uiStateAttributeObservable.get().orElse(null),
									String.class),
							uiStateAttributeAnnotation.kind(),
							UiStateAttributeUtils.computeQueryAttributeName(
									uiStateAttributeAnnotation,
									uiStateAttributeField.getName())),
					next -> uiStateAttributeObservable.set(next.map(value -> (T) converterRegistry
							.convert(value, resolveFieldType(uiStateAttributeField)))),
					uiStateAttributeObservable.getFlux().map(next -> next.map(value -> converterRegistry
							.convert(value, ValueWrapper.class)))));
		});
		//
		hasElement.getElement().addDetachListener(event -> disposableReference.get().dispose());
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof HasElement hasElement) {
			List<Field> uiStateAttributeFields = List.of(bean.getClass().getDeclaredFields())
					.filter(field -> MergedAnnotations.from(field).isPresent(UiStateAttribute.class))
					.peek(field -> {
						if (!Observable.class.isAssignableFrom(field.getType())) {
							throw new IllegalStateException("Only Observables are supported");
						}
					});
			//
			if (!uiStateAttributeFields.isEmpty()) {
				GenericUiStateBinder uiStateBinder = uiStateBinderProvider.getObject();
				//
				uiStateAttributeFields.forEach(uiStateAttributeField ->
						attachUiStateAttributeHandling(hasElement, uiStateAttributeField, uiStateBinder));
			}
		}
		return bean;
	}

	/**
	 * @param field expected to be of type Observable of Optional of T
	 *
	 * @return non-null
	 */
	public static Type resolveFieldType(Field field) {
		return ParameterUtils.getFirstActualParameter(
				ParameterUtils.getFirstActualParameter(field.getGenericType(), Observable.class),
				Optional.class);
	}
}
