package tk.labyrinth.pandora.ui.tool;

import com.vaadin.flow.component.UI;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.telemetry.tracing.SpanAttributeContributor;

import java.lang.reflect.Method;
import java.util.List;

@LazyComponent
public class UiIdSpanAttributeContributor implements SpanAttributeContributor {

	@Override
	public List<Pair<String, Object>> contributeAttributes(Method method, @Nullable Object target) {
		UI ui = UI.getCurrent();
		//
		return ui != null ? List.of(Pair.of("ui-id", ui.getUIId())) : List.of();
	}
}
