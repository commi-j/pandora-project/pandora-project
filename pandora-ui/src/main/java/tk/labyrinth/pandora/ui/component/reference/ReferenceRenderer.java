package tk.labyrinth.pandora.ui.component.reference;

import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import javax.annotation.Nullable;

public interface ReferenceRenderer<T, R extends Reference<T>> {

	@Nullable
	ReferenceItem<R> renderNonNullObject(T value);

	ReferenceItem<R> renderNonNullReference(R value);

	@Nullable
	default ReferenceItem<R> renderReference(@Nullable R value) {
		return value != null ? renderNonNullReference(value) : null;
	}
}
