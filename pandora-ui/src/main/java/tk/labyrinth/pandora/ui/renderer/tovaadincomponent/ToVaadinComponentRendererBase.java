package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import org.checkerframework.checker.nullness.qual.Nullable;

public abstract class ToVaadinComponentRendererBase<T> implements ToVaadinComponentRenderer<T> {

	protected abstract Component renderNonNull(Context context, T value);

	@Override
	public Component render(Context context, @Nullable T value) {
		return value != null ? renderNonNull(context, value) : new Div();
	}
}
