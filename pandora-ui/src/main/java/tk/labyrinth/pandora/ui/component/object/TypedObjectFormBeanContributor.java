package tk.labyrinth.pandora.ui.component.object;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistryOld;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectForm;

import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class TypedObjectFormBeanContributor extends UnaryParameterizedBeanContributor<ObjectForm<?>> {

	public static final int DISTANCE = Integer.MAX_VALUE;

	private final ObjectProvider<ConvertingObjectForm<?, GenericObject>> convertingFormProvider;

	// TODO: Do we need this wrapped in a provider?
	private final ObjectProvider<ObjectModelRegistryOld> objectModelRegistryProvider;

	@Override
	protected Object doContributeBean(BeanFactory beanFactory, Class<? extends ObjectForm<?>> base, Type parameter) {
		Class<?> parameterClass = TypeUtils.getClass(parameter);
		//
		ObjectModel objectModel = objectModelRegistryProvider.getObject().findOrCreateModelForJavaClass(parameterClass);
		//
		GenericObjectForm genericObjectForm = beanFactory
				.withBean(ObjectModelUtils.createDatatype(objectModel))
				.withBean(objectModel)
				.getBean(GenericObjectForm.class);
		//
		return convertingFormProvider.getObject(
				parameter,
				genericObjectForm,
				GenericObject.class);
	}

	@Override
	protected Integer doGetSupportDistance(Class<? extends ObjectForm<?>> base, Type parameter) {
		return DISTANCE;
	}
}
