package tk.labyrinth.pandora.ui.component.valuewrapper;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

@LazyComponent
@RequiredArgsConstructor
public class ValueWrapperBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericReference valueWrapperGenericReference = SignatureJavaBaseTypeReference
			.from(ValueWrapper.class)
			.toGenericReference();

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		return new FunctionalMutableValueBox<ValueWrapper>(
				parameters -> ValueWrapperBoxRenderer.render(builder -> builder
						.label(parameters.getLabel())
						.nonEditableReasons(parameters.getNonEditableReasons())
						.onValueChange(parameters.getOnValueChange())
						.value(parameters.getCurrentValue())
						.build()),
				Predicate.class);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), ValueWrapper.class)
				? ToStringRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
