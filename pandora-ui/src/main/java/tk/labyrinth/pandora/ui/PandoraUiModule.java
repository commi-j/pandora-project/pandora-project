package tk.labyrinth.pandora.ui;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.functionalcomponents.PandoraFunctionalComponentsConfiguration;
import tk.labyrinth.pandora.misc4j.lib.vaadin.tool.setup.SpringVaadinBeans;
import tk.labyrinth.pandora.misc4j.lib.vaadin.tool.setup.UiScopedBeansInitializer;
import tk.labyrinth.pandora.storage.PandoraStorageConfiguration;
import tk.labyrinth.pandora.uiapplication.PandoraUiApplicationModule;
import tk.labyrinth.pandora.views.PandoraViewsModule;

@Import({
		PandoraFunctionalComponentsConfiguration.class,
		PandoraStorageConfiguration.class,
		PandoraUiApplicationModule.class,
		PandoraViewsModule.class,
		SpringVaadinBeans.class,
		UiScopedBeansInitializer.class,
})
@SpringBootApplication
public class PandoraUiModule {
	// empty
}
