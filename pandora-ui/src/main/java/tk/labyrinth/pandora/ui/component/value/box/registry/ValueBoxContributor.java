package tk.labyrinth.pandora.ui.component.value.box.registry;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;

public interface ValueBoxContributor extends HasSupportDistance<ValueBoxRegistry.Context> {

	MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context);

	@Nullable
	@Override
	Integer getSupportDistance(ValueBoxRegistry.Context context);
}
