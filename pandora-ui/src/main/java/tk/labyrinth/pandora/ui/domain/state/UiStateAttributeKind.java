package tk.labyrinth.pandora.ui.domain.state;

public enum UiStateAttributeKind {
	QUERY,
	SERVER
}
