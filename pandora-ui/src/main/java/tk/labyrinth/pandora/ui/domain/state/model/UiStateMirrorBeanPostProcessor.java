package tk.labyrinth.pandora.ui.domain.state.model;

import com.vaadin.flow.component.HasElement;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.BeanPostProcessor;
import reactor.core.Disposable;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @see UiStateMirror
 */
@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class UiStateMirrorBeanPostProcessor implements BeanPostProcessor {

	private final ObjectProvider<TypedUiStateBinder<?>> uiStateBinderProvider;

	@SuppressWarnings("unchecked")
	private <T> void attachUiStateMirrorHandling(
			HasElement hasElement,
			Class<T> uiStateMirrorClass,
			Field uiStateMirrorField) {
		AtomicReference<Disposable> disposableReference = new AtomicReference<>(null);
		//
		hasElement.getElement().addAttachListener(event -> {
			Observable<T> uiStateMirrorObservable;
			try {
				uiStateMirrorField.setAccessible(true);
				//
				uiStateMirrorObservable = (Observable<T>) uiStateMirrorField.get(hasElement);
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			}
			//
			TypedUiStateBinder<T> uiStateBinder = (TypedUiStateBinder<T>) uiStateBinderProvider.getObject();
			//
			disposableReference.set(uiStateBinder.bind(
					uiStateMirrorClass,
					uiStateMirrorObservable.get(),
					uiStateMirrorObservable::get,
					uiStateMirrorObservable::set,
					uiStateMirrorObservable.getFlux()));
		});
		//
		hasElement.getElement().addDetachListener(event -> Optional.ofNullable(disposableReference.get())
				.ifPresent(Disposable::dispose));
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof HasElement hasElement) {
			List<Field> uiStateMirrorFields = List.of(bean.getClass().getDeclaredFields())
					.filter(field -> field.getAnnotation(UiStateMirror.class) != null)
					.peek(field -> {
						if (!Observable.class.isAssignableFrom(field.getType())) {
							throw new IllegalStateException("Only Observables are supported");
						}
					});
			//
			if (!uiStateMirrorFields.isEmpty()) {
				Field uiStateMirrorField = uiStateMirrorFields.single();
				//
				Type uiStateMirrorType = ParameterUtils.getFirstActualParameter(
						uiStateMirrorField.getGenericType(),
						Observable.class);
				Class<?> uiStateMirrorClass = TypeUtils.getClass(uiStateMirrorType);
				//
				List<Field> uiStateAttributeFields = List.of(uiStateMirrorClass.getDeclaredFields())
						.filter(field -> field.getAnnotation(UiStateAttribute.class) != null);
				//
				if (!uiStateAttributeFields.isEmpty()) {
					attachUiStateMirrorHandling(hasElement, uiStateMirrorClass, uiStateMirrorField);
				}
			}
		}
		return bean;
	}
}
