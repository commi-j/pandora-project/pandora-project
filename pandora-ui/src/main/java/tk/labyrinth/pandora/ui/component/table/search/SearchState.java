package tk.labyrinth.pandora.ui.component.table.search;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;

@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class SearchState {

	@NonNull
	SearchParameters parameters;

	/**
	 * Null means search is in progress.
	 */
	@Nullable
	SearchResult result;

	/**
	 * TraceId/SpanId of ongoing search.
	 */
	@NonNull
	Pair<String, String> spanIdPair;

	@NonNull
	Instant startedAt;
}
