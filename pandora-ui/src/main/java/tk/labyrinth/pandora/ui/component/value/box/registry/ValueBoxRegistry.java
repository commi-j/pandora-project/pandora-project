package tk.labyrinth.pandora.ui.component.value.box.registry;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.checker.nullness.qual.PolyNull;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeature;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;

import java.lang.reflect.Type;
import java.util.function.Supplier;

@Deprecated // See BoxRendererRegistry.
@LazyComponent
@RequiredArgsConstructor
public class ValueBoxRegistry {

	@SmartAutowired
	private List<ValueBoxContributor> valueBoxContributors;

	@SuppressWarnings("unchecked")
	public <T> MutableValueBox<T> getValueBox(@NonNull Class<T> javaClass) {
		return (MutableValueBox<T>) getValueBox((Type) javaClass);
	}

	public MutableValueBox<?> getValueBox(@NonNull Context context) {
		return getValueBoxSupplier(context).get();
	}

	public MutableValueBox<?> getValueBox(@Nullable Datatype datatype) {
		return getValueBoxSupplier(datatype).get();
	}

	public MutableValueBox<?> getValueBox(@NonNull Type javaType) {
		return getValueBoxSupplier(javaType).get();
	}

	@SuppressWarnings("unchecked")
	public <T> MutableValueBox<T> getValueBoxInferred(@NonNull Datatype datatype) {
		return (MutableValueBox<T>) getValueBox(datatype);
	}

	@SuppressWarnings("unchecked")
	public <T> MutableValueBox<T> getValueBoxInferred(@NonNull Type javaType) {
		return (MutableValueBox<T>) getValueBox(javaType);
	}

	public Supplier<MutableValueBox<?>> getValueBoxSupplier(Context context) {
		val selectionResult = HasSupportDistance.selectHandler(
				valueBoxContributors,
				context);
		//
		ValueBoxContributor valueBoxContributor = selectionResult.getHandler();
		//
		return () -> valueBoxContributor.contributeValueBox(this, context);
	}

	public Supplier<MutableValueBox<?>> getValueBoxSupplier(@Nullable Datatype datatype) {
		return getValueBoxSupplier(Context.builder()
				.datatype(datatype)
				.features(List.empty())
				.build());
	}

	@SuppressWarnings("unchecked")
	public <T> Supplier<MutableValueBox<T>> getValueBoxSupplier(@NonNull Type javaType) {
		return (Supplier<MutableValueBox<T>>) (Object) getValueBoxSupplier(JavaBaseTypeUtils.createDatatypeFromJavaType(
				javaType));
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Context {

		@PolyNull
		Datatype datatype;

		// TODO: This should be not bound to ObjectModelAttribute. (Are we sure?)
		@NonNull
		List<ObjectModelAttributeFeature> features;
	}
}
