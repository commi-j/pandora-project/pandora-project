package tk.labyrinth.pandora.ui.component.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextArea;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import java.util.Optional;

@Deprecated
@PrototypeScopedComponent
@RequiredArgsConstructor
public class JsonDisplay implements RenderableView<JsonDisplay.Properties> {

	private final ObjectMapper objectMapper;

	private final TextArea textArea = new TextArea();

	@PostConstruct
	private void postConstruct() {
		textArea.setReadOnly(true);
		textArea.setWidthFull();
	}

	@Override
	public Component asVaadinComponent() {
		return textArea;
	}

	@Override
	public void render(Properties properties) {
		try {
			Optional.ofNullable(properties.readOnly()).ifPresent(textArea::setReadOnly); // FIXME: Override happens here.
			textArea.setValue(objectMapper
					.writerWithDefaultPrettyPrinter()
					.writeValueAsString(properties.value()));
		} catch (JsonProcessingException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		Boolean readOnly;

		@NonNull
		Object value;
	}
}
