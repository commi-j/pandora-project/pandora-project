package tk.labyrinth.pandora.ui.component.value.box.mutable;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;

import java.lang.reflect.Type;
import java.util.function.Function;

@RequiredArgsConstructor
public class FunctionalMutableValueBox<T> implements MutableValueBox<T> {

	private final Function<Properties<T>, Component> componentFunction;

	private final Div div = new Div();

	private final Type parameterType;

	@Override
	public Component asVaadinComponent() {
		return div;
	}

	@Override
	public Type getParameterType() {
		return parameterType;
	}

	@Override
	public void render(Properties<T> properties) {
		div.removeAll();
		//
		Component component = componentFunction.apply(properties);
		StyleUtils.setWidth(component, "100%");
		div.add(component);
	}
}
