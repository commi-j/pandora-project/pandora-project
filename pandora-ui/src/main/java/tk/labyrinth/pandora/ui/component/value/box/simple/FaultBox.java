package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import lombok.Getter;
import tk.labyrinth.pandora.functionalcomponents.core.FaultAreaRenderer;
import tk.labyrinth.pandora.ui.component.TextDisplayer;
import tk.labyrinth.pandora.ui.component.value.box.ValueBox;

// TODO: Decide if we need this class.
public class FaultBox<T> extends Composite<TextField> implements ValueBox<T> {

	@Getter
	private Throwable fault;

	{
		getContent().setReadOnly(true);
		Button showFaultButton = new Button(
				VaadinIcon.EYE.create(),
				event -> TextDisplayer.display(FaultAreaRenderer.renderFault(fault)));
		getContent().setSuffixComponent(showFaultButton);
	}

	@Override
	public Component asVaadinComponent() {
		return getContent();
	}

	@Override
	public Properties<T> getProperties() {
		throw new UnsupportedOperationException();
	}

	public void setFault(Throwable fault) {
		this.fault = fault;
		getContent().setValue(fault.toString());
	}

	@Override
	public void setProperties(Properties<T> properties) {
		getContent().setLabel(properties.getLabel());
	}
}
