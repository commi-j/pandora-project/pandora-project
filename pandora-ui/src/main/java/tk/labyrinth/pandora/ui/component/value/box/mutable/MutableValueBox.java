package tk.labyrinth.pandora.ui.component.value.box.mutable;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.ui.component.value.box.ValueBoxContext;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import java.util.function.Consumer;
import java.util.function.Function;

public interface MutableValueBox<T> extends RenderableView<MutableValueBox.Properties<T>>, TypeAware<T> {

	default void render(Function<Properties.Builder<T>, Properties<T>> propertiesConfigurer) {
		render(propertiesConfigurer.apply(Properties.builder()));
	}

	@Override
	void render(Properties<T> properties);

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	class Properties<T> {

		// TODO: Think if we want to let it be nullable or not.
		@Nullable
		ValueBoxContext context;

		T currentValue;

		T initialValue;

		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<T> onValueChange;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder<T> {
			// Lomboked
		}
	}
}
