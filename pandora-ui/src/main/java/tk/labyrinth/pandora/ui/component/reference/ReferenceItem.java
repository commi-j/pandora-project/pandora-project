package tk.labyrinth.pandora.ui.component.reference;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

/**
 * Item to display in components like {@link com.vaadin.flow.component.combobox.ComboBox ComboBox}
 * or {@link com.vaadin.flow.component.select.Select Select}.
 *
 * @param <R> Reference
 */
@Builder(toBuilder = true)
@Value
@With
public class ReferenceItem<R extends Reference<?>> {

	@NonNull
	R reference;

	@NonNull
	String text;

	public static <R extends Reference<?>> ReferenceItem<R> of(R reference, String text) {
		return new ReferenceItem<>(reference, text);
	}

	public static <R extends Reference<?>> ReferenceItem<R> ofMissing(R reference) {
		return new ReferenceItem<>(reference, "Not Found: " + reference);
	}
}
