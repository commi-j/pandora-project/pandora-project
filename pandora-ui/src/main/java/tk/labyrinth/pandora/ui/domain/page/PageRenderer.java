package tk.labyrinth.pandora.ui.domain.page;

import com.vaadin.flow.component.html.Div;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class PageRenderer extends Div {

	@PostConstruct
	private void postConstruct() {
		add("PageRenderer");
	}
}
