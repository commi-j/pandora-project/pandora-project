package tk.labyrinth.pandora.ui.domain.pageroute;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.ui.domain.page.SlugPandoraPageReference;
import tk.labyrinth.pandora.ui.domain.route.PandoraRouteParameterMapping;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel(code = PageRoute.MODEL_CODE, primaryAttribute = PageRoute.PATH_ATTRIBUTE_NAME)
@Value
@With
public class PageRoute {

	public static final String MODEL_CODE = "pageroute";

	public static final String PATH_ATTRIBUTE_NAME = "path";

	SlugPandoraPageReference pageReference;

	List<PandoraRouteParameterMapping> parameterMappings;

	String path;

	/**
	 * This one is for TreeGrid.
	 */
	UUID uid;

	public List<PandoraRouteParameterMapping> getParameterMappingsOrEmpty() {
		return parameterMappings != null ? parameterMappings : List.empty();
	}
}
