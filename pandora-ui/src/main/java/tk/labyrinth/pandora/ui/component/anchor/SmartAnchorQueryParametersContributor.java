package tk.labyrinth.pandora.ui.component.anchor;

import com.vaadin.flow.router.QueryParameters;

public interface SmartAnchorQueryParametersContributor {

	QueryParameters contributeQueryParameters();
}
