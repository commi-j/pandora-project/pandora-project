package tk.labyrinth.pandora.ui.component.valuebox.suggest;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model // FIXME: We don't need it to be model, should be discoverable because of being polymorphic.
@PolymorphicLeaf(rootClass = ObjectModelAttributeFeature.class, qualifierAttributeValue = "hasSuggests")
@Value
@With
public class HasSuggestsObjectModelAttributeFeature implements ObjectModelAttributeFeature {

	Class<? extends SuggestProvider<?>> suggestProviderClass;
}
