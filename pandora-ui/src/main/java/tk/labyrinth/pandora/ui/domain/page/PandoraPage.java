package tk.labyrinth.pandora.ui.domain.page;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderAttribute(PandoraPage.SLUG_ATTRIBUTE_NAME)
@RootModel(code = PandoraPage.MODEL_CODE, primaryAttribute = PandoraPage.SLUG_ATTRIBUTE_NAME)
@Value
@With
public class PandoraPage {

	public static final String MODEL_CODE = "pandorapage";

	public static final String SLUG_ATTRIBUTE_NAME = "slug";

	/**
	 * Handle is used to determine how to render the page. Examples are:<br>
	 * - vaadinRoute:{className} - page registered using Vaadin's @Route;<br>
	 * - vaadinComponent:{className} - page registered with Pandora and referring to a class extending Vaadin's Component;<br>
	 * - pandoraPageFunction:{methodFullSignature} - page registered using Pandora's @PageFunction;<br>
	 */
	String handle;

	List<ObjectModelAttribute> parametersModel;

	String slug;
}
