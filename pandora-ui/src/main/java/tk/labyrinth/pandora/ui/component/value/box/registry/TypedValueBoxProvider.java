package tk.labyrinth.pandora.ui.component.value.box.registry;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;

import java.lang.reflect.Type;

@Deprecated
@PrototypeScopedComponent
@RequiredArgsConstructor
public class TypedValueBoxProvider<T> {

	private final ValueBoxRegistry valueBoxRegistry;

	@SmartAutowired
	private Type javaType;

	@SuppressWarnings("unchecked")
	public MutableValueBox<T> getValueBox() {
		return (MutableValueBox<T>) valueBoxRegistry.getValueBoxSupplier(this.javaType).get();
	}
}
