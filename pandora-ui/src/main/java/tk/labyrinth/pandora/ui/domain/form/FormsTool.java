package tk.labyrinth.pandora.ui.domain.form;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootConstants;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReference;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectMultiView;

@Bean
@RequiredArgsConstructor
public class FormsTool {

	private final BeanContext beanContext;

	private final ConverterRegistry converterRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	public ConfirmationHandle<GenericObject> showDialog(@NonNull ObjectModel objectModel, GenericObject initialValue) {
		if (initialValue.hasAttribute(PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME)) {
			// TODO: Check that primaryReference did not change if it was present.
			RootObjectPrimaryReference primaryReference = rootObjectPrimaryReferenceHandler
					.findPrimaryReference(initialValue);
		}
		//
		GenericObjectMultiView view = beanContext.getBeanFactory()
				.withBean(objectModel)
				.getBean(GenericObjectMultiView.class);
		//
		view.render(GenericObjectMultiView.Properties.builder()
				.currentValue(initialValue)
				.datatype(ObjectModelUtils.createDatatype(objectModel))
				.initialValue(initialValue)
				.build());
		//
		return ConfirmationViews.showViewDialog(view)
				.map(success -> view.getValue());
	}

	@SuppressWarnings("unchecked")
	public <T> ConfirmationHandle<T> showDialog(@NonNull T initialValue) {
		ObjectModel objectModel = javaBaseTypeRegistry.getObjectModel(initialValue.getClass());
		GenericObject genericInitialValue = converterRegistry.convert(initialValue, GenericObject.class);
		//
		return showDialog(objectModel, genericInitialValue)
				.map(value -> (T) converterRegistry.convert(value, initialValue.getClass()));
	}
}
