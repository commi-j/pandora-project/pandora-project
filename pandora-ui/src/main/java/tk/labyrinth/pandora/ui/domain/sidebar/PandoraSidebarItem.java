package tk.labyrinth.pandora.ui.domain.sidebar;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@RenderPattern("$text${target? ($)}")
@Value
@With
public class PandoraSidebarItem {

	List<PandoraSidebarItem> children;

	/**
	 * If external, links are created with target="_blank".
	 */
	@Nullable
	Boolean external;

	@Nullable
	String icon;

	@Nullable
	String target;

	String text;

	UUID uid;

	public List<PandoraSidebarItem> flatten() {
		return getChildrenOrEmpty()
				.flatMap(PandoraSidebarItem::flatten)
				.prepend(this);
	}

	public List<PandoraSidebarItem> getChildrenOrEmpty() {
		return children != null ? children : List.empty();
	}
}
