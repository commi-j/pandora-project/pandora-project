package tk.labyrinth.pandora.ui.component.bulkedit;

import com.vaadin.flow.component.Component;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.selector.SelectorBoxRenderer;
import tk.labyrinth.pandora.ui.component.valuewrapper.ValueWrapperBoxRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import javax.annotation.CheckForNull;
import java.util.Optional;
import java.util.function.Consumer;

@LazyComponent
@RequiredArgsConstructor
public class BulkEditViewRenderer {

	private final SelectorBoxRenderer selectorBoxRenderer;

	public Component render(Properties properties) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				layout.add(selectorBoxRenderer.render(SelectorBoxRenderer.Parameters.builder()
						.onValueChange(nextValue -> Optional.ofNullable(properties.getOnSelectorChange())
								.ifPresent(onSelectorChange -> onSelectorChange.accept(nextValue)))
						.value(properties.getSelector())
						.build()));
			}
			{
				CssHorizontalLayout attributeLayout = new CssHorizontalLayout();
				{
					attributeLayout.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					attributeLayout.add(StringBoxRenderer.render(builder -> builder
							.label("Attribute Name")
							.onValueChange(nextValue -> Optional.ofNullable(properties.getOnAttributeNameChange())
									.ifPresent(onAttributeNameChange -> onAttributeNameChange.accept(nextValue)))
							.value(properties.getAttributeName())
							.build()));
					attributeLayout.add(ValueWrapperBoxRenderer.render(ValueWrapperBoxRenderer.Parameters.builder()
							.label("Attribute Value")
							.onValueChange(properties.getOnAttributeValueChange() != null
									? nextValue -> properties.getOnAttributeValueChange().accept(nextValue)
									: null)
							.value(properties.getAttributeValue())
							.build()));
				}
				layout.add(attributeLayout);
			}
		}
		return layout;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@CheckForNull
		String attributeName;

		@CheckForNull
		ValueWrapper attributeValue;

		@CheckForNull
		Consumer<String> onAttributeNameChange;

		@CheckForNull
		Consumer<ValueWrapper> onAttributeValueChange;

		@CheckForNull
		Consumer<Predicate> onSelectorChange;

		@CheckForNull
		Predicate selector;
	}
}
