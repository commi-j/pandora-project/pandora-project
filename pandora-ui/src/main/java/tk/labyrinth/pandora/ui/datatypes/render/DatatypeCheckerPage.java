package tk.labyrinth.pandora.ui.datatypes.render;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.SignatureDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.functionalcomponents.core.FaultAreaRenderer;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextAreaRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.TypedValueBoxProvider;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.routing.routetarget.ParametersAware;
import tk.labyrinth.pandora.ui.routing.routetarget.annotation.RouteTarget;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

@RequiredArgsConstructor
@RouteTarget
@Slf4j
public class DatatypeCheckerPage extends FunctionalPage<DatatypeCheckerPage.Parameters> implements
		ParametersAware<DatatypeCheckerPage.Parameters> {

	private final BoxRendererRegistry boxRendererRegistry;

	private final ConverterRegistry converterRegistry;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ToStringRendererRegistry toStringRendererRegistry;

	private final ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	private final ValueBoxRegistry valueBoxRegistry;

	@SmartAutowired
	private TypedValueBoxProvider<AliasDatatypeBaseReference> datatypeBaseReferenceBoxProvider;

	private Component renderValue(Datatype datatype, @Nullable Object value, Consumer<Object> valueSink) {
		CssGridLayout layout = new CssGridLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setGridTemplateColumns("1fr 1fr");
		}
		{
			Reference<DatatypeBase> datatypeBaseReference = datatype.getBaseReference();
//			DatatypeBase datatypeBase = datatypeBaseSearcher.getSingle(datatypeBaseReference);
			{
				layout.add(TextFieldRenderer.render(builder -> builder
						.label("Datatype - Base Reference")
						.readOnly(true)
						.value(datatypeBaseReference.toString())
						.build()));
				//
				layout.add(TextFieldRenderer.render(builder -> builder
						.label("Datatype - Java Type")
						.readOnly(true)
						.value(javaBaseTypeRegistry.getJavaType(datatype).toString())
						.build()));
			}
			{
				layout.add(renderValueBoxLayout(datatype, value, valueSink));
				//
				layout.add(TextFieldRenderer.render(builder -> builder
						.label("Value - Class")
						.readOnly(true)
						.value(String.valueOf(value != null ? value.getClass() : null))
						.build()));
			}
			{
				layout.add(TextAreaRenderer.render(builder -> builder
						.label("Value - To String")
						.readOnly(true)
						.value(String.valueOf(value))
						.build()));
				//
				Try
						.ofSupplier(() -> toStringRendererRegistry.render(
								ToStringRendererRegistry.Context.builder()
										.datatype(datatype)
										.hints(List.empty())
										.build(),
								value))
						.map(string -> TextAreaRenderer.render(builder -> builder
								.label("Value - Rendered To String")
								.readOnly(true)
								.value(string)
								.build()))
						.recover(fault -> FaultAreaRenderer.render(builder -> builder
								.fault(fault)
								.label("Value - Rendered To String")
								.build()))
						.onSuccess(layout::add);
			}
			{
				// TODO: Make it smaller like all other labels.
				layout.add(new Span("Value - Rendered To Vaadin Component:"));
				//
				Try
						.ofSupplier(() -> toVaadinComponentRendererRegistry.render(
								ToVaadinComponentRendererRegistry.Context.builder()
										.datatype(datatype)
										.hints(List.empty())
										.build(),
								value))
						.recover(fault -> FaultAreaRenderer.render(builder -> builder
								.fault(fault)
								.build()))
						.onSuccess(layout::add);
			}
			{
				Try<ValueWrapper> valueWrapperTry = Try.ofSupplier(() ->
						converterRegistry.convert(value, ValueWrapper.class));
				//
				valueWrapperTry
						.map(valueWrapper -> TextAreaRenderer.render(builder -> builder
								.label("To ValueWrapper")
								.readOnly(true)
								.value(String.valueOf(valueWrapper))
								.build()))
						.recover(fault -> FaultAreaRenderer.render(builder -> builder
								.fault(fault)
								.label("To ValueWrapper")
								.build()))
						.onSuccess(component -> {
							CssFlexItem.setFlexGrow(component, 1);
							//
							layout.add(component);
						});
				//
				valueWrapperTry
						.map(valueWrapper -> converterRegistry.convert(
								valueWrapper,
								javaBaseTypeRegistry.getJavaType(datatype)))
						.map(convertedBackValue -> TextAreaRenderer.render(builder -> builder
								.label("From ValueWrapper")
								.readOnly(true)
								.value(String.valueOf(convertedBackValue))
								.build()))
						.recover(fault -> FaultAreaRenderer.render(builder -> builder
								.fault(fault)
								.label("From ValueWrapper")
								.build()))
						.onSuccess(component -> {
							CssFlexItem.setFlexGrow(component, 1);
							//
							layout.add(component);
						});
			}
			{
				Try
						.ofSupplier(() -> String.valueOf(converterRegistry.convert(value, JsonNode.class)))
						.map(string -> TextAreaRenderer.render(builder -> builder
								.label("Converted to JsonNode")
								.readOnly(true)
								.value(string)
								.build()))
						.recover(fault -> FaultAreaRenderer.render(builder -> builder
								.fault(fault)
								.label("Converted to JsonNode")
								.build()))
						.onSuccess(layout::add);
			}
		}
		return layout;
	}

	@SuppressWarnings("unchecked")
	private Component renderValueBoxLayout(Datatype datatype, @Nullable Object value, Consumer<Object> valueSink) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setAlignItems(AlignItems.BASELINE);
		}
		{
			{
				MutableValueBox<Object> valueBox = (MutableValueBox<Object>) valueBoxRegistry.getValueBox(datatype);
				//
				valueBox.render(builder -> builder
						.currentValue(value)
						.initialValue(value)
						.label("Value - Box")
						.onValueChange(valueSink)
						.build());
				//
				CssFlexItem.setFlexGrow(valueBox.asVaadinComponent(), 1);
				//
				layout.add(valueBox.asVaadinComponent());
			}
			{
				layout.add(ButtonRenderer.render(builder -> builder
						.icon(VaadinIcon.QUESTION_CIRCLE.create())
						.onClick(event -> {
							MutableValueBox<?> valueBox = valueBoxRegistry.getValueBox(datatype);
							//
							// TODO: This is for breakpoint, should replace with proper UI element.
							valueBox.asVaadinComponent();
						})
						.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
						.build()));
			}
		}
		//
		return layout;
	}

	@Override
	protected Parameters getInitialProperties() {
		return getInitialParameters();
	}

	@Override
	protected Component render(Parameters parameters, Consumer<Parameters> sink) {
		SignatureDatatypeBaseReference datatypeBaseReference = parameters.datatypeBaseReference();
		List<Datatype> datatypeParameters = parameters.parameters();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout toolbar = new CssHorizontalLayout();
				{
					toolbar.addClassNames(PandoraStyles.LAYOUT);
				}
				{
					{
						toolbar.add(boxRendererRegistry.getRenderer(SignatureDatatypeBaseReference.class)
								.render(builder -> builder
										.currentValue(datatypeBaseReference)
										.initialValue(datatypeBaseReference)
										.label("Base")
										.onValueChange(nextValue -> {
											Datatype nextDatatype;
											//
											if (nextValue != null) {
												DatatypeBase datatypeBase = datatypeBaseRegistry.getSingle(nextValue);
												//
												nextDatatype = Datatype.builder()
														.baseReference(nextValue)
														.parameters(datatypeBase.getParameters() != null
																? datatypeBase.getParameters().map(parameterName ->
																Datatype.builder()
																		.baseReference(AliasDatatypeBaseReference.of(
																				PandoraDatatypeConstants.VALUE_DATATYPE_NAME))
																		.build())
																: List.empty())
														.build();
											} else {
												nextDatatype = Datatype.builder()
														.parameters(List.empty())
														.build();
											}
											//
											sink.accept(parameters.toBuilder()
													.datatypeBaseReference(nextValue)
													.parameters(nextDatatype.getParameters())
													.build());
										})
										.build())
								.asVaadinComponent());
					}
					if (datatypeBaseReference != null) {
						DatatypeBase datatypeBase = datatypeBaseRegistry.getSingle(datatypeBaseReference);
						//
						// FIXME: Should be injected via Spring.
						Supplier<MutableValueBox<Datatype>> datatypeValueBoxSupplier = valueBoxRegistry
								.getValueBoxSupplier(Datatype.class);
						//
						if (datatypeBase.getParameters() != null) {
							datatypeBase.getParameters().zipWithIndex().forEach(tuple -> {
								MutableValueBox<Datatype> valueBox = datatypeValueBoxSupplier.get();
								//
								valueBox.render(MutableValueBox.Properties.<Datatype>builder()
										.currentValue(datatypeParameters.get(tuple._2()))
										.label(tuple._1())
										.onValueChange(next -> sink.accept(parameters.withParameters(
												datatypeParameters.update(tuple._2(), next))))
										.build());
								//
								toolbar.add(valueBox.asVaadinComponent());
							});
						}
					}
				}
				layout.add(toolbar);
			}
			{
				if (datatypeBaseReference != null) {
					Observable<Optional<?>> valueObservable = Observable.withInitialValue(Optional.empty());
					//
					valueObservable.subscribe((nextValue, valueSink) -> {
						layout.getChildren().skip(1).findFirst().ifPresent(layout::remove);
						//
						layout.add(renderValue(
								Datatype.builder()
										.baseReference(datatypeBaseReference)
										.parameters(!parameters.parameters().isEmpty() ? parameters.parameters() : null)
										.build(),
								nextValue.orElse(null),
								nextNextValue -> valueSink.accept(Optional.ofNullable(nextNextValue))));
					});
				}
			}
		}
		return layout;
	}

	@Override
	public void acceptParameters(Parameters parameters) {
		// TODO
	}

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder()
				.datatypeBaseReference(null)
				.parameters(List.empty())
				.build();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		SignatureDatatypeBaseReference datatypeBaseReference;

		@NonNull
		List<Datatype> parameters;
	}
}
