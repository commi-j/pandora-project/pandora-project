package tk.labyrinth.pandora.ui.component.genericobject;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

@LazyComponent
@RequiredArgsConstructor
public class ObjectModelBasedValueBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final ModelAwareGenericObjectBoxRenderer genericObjectBoxRenderer;

	private final ObjectModelRegistry objectModelRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		return new FunctionalMutableValueBox<GenericObject>(
				parameters -> genericObjectBoxRenderer.render(builder -> builder
						.currentValue(parameters.getCurrentValue())
						.datatype(context.getDatatype())
						.initialValue(parameters.getInitialValue())
						.label(parameters.getLabel())
						.nonEditableReasons(parameters.getNonEditableReasons())
						.objectModelReference(ObjectModelUtils.getObjectModelReference(context.getDatatype().getBaseReference()))
						.onValueChange(parameters.getOnValueChange())
						.build()),
				GenericObject.class);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return context.getDatatype() != null &&
				ObjectModelUtils.isObjectModelDatatypeBaseReference(context.getDatatype().getBaseReference())
				? ToStringRenderer.MAX_DISTANCE_MINUS_TWO
				: null;
	}
}
