package tk.labyrinth.pandora.ui.component.polymorphic;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.datatypes.polymorphic.PolymorphicLinkIndex;
import tk.labyrinth.pandora.functionalvaadin.component.SelectRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.datatypes.polymorphic.PolymorphicLinkRegistry;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectForm;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;

@RequiredArgsConstructor
public class PolymorphicGenericObjectForm implements GenericObjectForm {

	private final BeanContext beanContext;

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private final CssVerticalLayout layout = new CssVerticalLayout();

	private final PolymorphicLinkRegistry polymorphicLinkRegistry;

	@SmartAutowired
	private ObjectModel objectModel;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@PostConstruct
	private void postConstruct() {
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
	}

	@Override
	public Component asVaadinComponent() {
		return layout;
	}

	@Override
	public void render(Properties<GenericObject> parameters) {
		layout.removeAll();
		//
		List<PolymorphicLinkIndex> polymorphicLinkIndices = polymorphicLinkRegistry
				.getIndicesForRootDatatype(ObjectModelUtils.createDatatypeBaseReference(objectModel));
		//
		val indexAndObjectModelPairs = polymorphicLinkIndices.map(polymorphicLinkIndex -> Pair.of(
				polymorphicLinkIndex,
				objectModelSearcher.getSingle(getObjectModelReference(polymorphicLinkIndex))));
		//
		GenericObject currentValue = parameters.getCurrentValue();
		GenericObject currentValueToUse;
		{
			if (Objects.equals(
					PandoraRootUtils.getModelReference(currentValue),
					CodeObjectModelReference.from(objectModel))) {
				// FIXME: This is not an obvious solution. If we receive an object with model equal to
				//  polymorphic root interface this means that this object is new, and we replace it with first
				//  leaf option.
				//
				val firstPair = indexAndObjectModelPairs.get(0);
				//
				currentValueToUse = GenericObject.of(
						firstPair.getLeft().getQualifierAttribute(),
						// FIXME: Get rid of modelReference.
						PandoraRootUtils.createModelReferenceAttribute(firstPair.getRight()));
			} else {
				currentValueToUse = currentValue;
			}
		}
		//
		Pair<PolymorphicLinkIndex, ObjectModel> currentPair = indexAndObjectModelPairs
				.find(pair -> currentValueToUse.getAttributes().contains(pair.getLeft().getQualifierAttribute()))
				.get();
		//
		{
			layout.add(SelectRenderer.<Pair<PolymorphicLinkIndex, ObjectModel>>render(builder -> builder
					.items(indexAndObjectModelPairs.asJava())
					.itemLabelGenerator(item -> item.getRight().getName())
					.label("Qualifier")
					.readOnly(!parameters.calcEditable())
					.onValueChange(nextValue -> parameters.getOnValueChange().accept(
							GenericObject.of(
									nextValue.getLeft().getQualifierAttribute(),
									// FIXME: Get rid of modelReference.
									PandoraRootUtils.createModelReferenceAttribute(nextValue.getRight()))))
					.title(StringUtils.emptyToNull(parameters.calcNonEmptyNonEditableReasons().mkString("\n")))
					.value(currentPair)
					.build()));
		}
		//
		{
			GenericObjectForm objectForm = beanContext.getBeanFactory()
					.withBean(currentPair.getRight())
					.getBean(GenericObjectForm.class);
			//
			objectForm.render(Properties.<GenericObject>builder()
					.currentValue(currentValueToUse)
					.initialValue(parameters.getInitialValue())
					.nonEditableReasons(parameters.getNonEditableReasons())
					.onValueChange(parameters.getOnValueChange())
					.build());
			//
			layout.add(objectForm.asVaadinComponent());
		}
	}

	public static CodeObjectModelReference getObjectModelReference(PolymorphicLinkIndex polymorphicLinkIndex) {
		return ObjectModelUtils.getObjectModelReference(polymorphicLinkIndex.getLeafReference());
	}
}
