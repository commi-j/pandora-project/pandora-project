package tk.labyrinth.pandora.ui.component.table;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;

import java.util.function.Consumer;

@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class AdvancedTablePageModel {

	@NonNull
	Consumer<Integer> onSelectedPageIndexChange;

	@NonNull
	Integer selectedPageIndex;
}
