package tk.labyrinth.pandora.ui.renderer.tostring;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRenderer;

@LazyComponent
@RequiredArgsConstructor
public class ShortClassToStringRenderer extends ToStringRendererBase<Class<?>> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected String renderNonNull(Context context, Class<?> value) {
		return value.getSimpleName();
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(
				context.getDatatype(),
				JavaBaseTypeUtils.createDatatypeBaseReference(Class.class))
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}
}
