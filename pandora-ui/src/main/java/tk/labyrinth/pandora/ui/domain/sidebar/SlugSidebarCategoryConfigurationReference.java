package tk.labyrinth.pandora.ui.domain.sidebar;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class SlugSidebarCategoryConfigurationReference implements Reference<SidebarCategoryConfiguration> {

	String slug;

	public GenericReference<SidebarCategoryConfiguration> toGenericReference() {
		return GenericReference.of(
				SidebarCategoryConfiguration.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(SidebarCategoryConfiguration.SLUG_ATTRIBUTE_NAME, slug)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				SidebarCategoryConfiguration.MODEL_CODE,
				SidebarCategoryConfiguration.SLUG_ATTRIBUTE_NAME,
				slug);
	}

	public static SlugSidebarCategoryConfigurationReference from(GenericReference<?> reference) {
		if (!isSlugSidebarCategoryConfigurationReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(SidebarCategoryConfiguration.SLUG_ATTRIBUTE_NAME));
	}

	public static SlugSidebarCategoryConfigurationReference from(SidebarCategoryConfiguration value) {
		return of(value.getSlug());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static SlugSidebarCategoryConfigurationReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static String getSlug(
			GenericReference<SidebarCategoryConfiguration> slugSidebarCategoryConfigurationReference) {
		return slugSidebarCategoryConfigurationReference.getAttributeValue(
				SidebarCategoryConfiguration.SLUG_ATTRIBUTE_NAME);
	}

	public static boolean isSlugSidebarCategoryConfigurationReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), SidebarCategoryConfiguration.MODEL_CODE) &&
				reference.hasAttribute(SidebarCategoryConfiguration.SLUG_ATTRIBUTE_NAME);
	}
}
