package tk.labyrinth.pandora.ui.domain.mongodb;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.domain.mongodb.MongoDbDatatypeConstants;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.extra.referencemodel.ReferenceModelRegistry;
import tk.labyrinth.pandora.ui.component.genericobjectreference.GenericObjectReferenceBoxRenderer;
import tk.labyrinth.pandora.views.box.BoxRenderer;
import tk.labyrinth.pandora.views.box.BoxRendererContributor;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;

@Bean
@RequiredArgsConstructor
public class MongoDbObjectIdBoxRendererContributor implements BoxRendererContributor<String> {

	private final ConverterRegistry converterRegistry;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericObjectReferenceBoxRenderer genericObjectReferenceBoxRenderer;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ReferenceModelRegistry referenceModelRegistry;

	@Override
	public BoxRenderer<String> contributeBoxRenderer(BoxRendererRegistry registry, BoxRendererRegistry.Context context) {
		return BoxRenderer.of(
				String.class,
				parameters -> MongoDbObjectIdBoxRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.label(parameters.label())
						.nonEditableReasons(parameters.nonEditableReasons())
						.onValueChange(nextValue -> parameters.onValueChange().accept(nextValue))
						.build()));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BoxRendererRegistry.Context target) {
		return datatypeBaseRegistry.matches(
				target.getDatatype(),
				AliasDatatypeBaseReference.of(MongoDbDatatypeConstants.OBJECT_ID_ALIAS))
				? 400
				: null;
	}
}
