package tk.labyrinth.pandora.ui.component.object;

import com.vaadin.flow.component.Component;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;

import java.lang.reflect.Type;

/**
 * @param <E> External
 * @param <I> Internal
 */
@PrototypeScopedComponent
@RequiredArgsConstructor
public class ConvertingObjectForm<E, I> implements ObjectForm<E> {

	private final Type externalType;

	private final ObjectForm<I> internalForm;

	private final Type internalType;

	@Autowired
	private ConverterRegistry converterRegistry;

	@SuppressWarnings("unchecked")
	private E toExternal(I value) {
		return (E) converterRegistry.convert(value, externalType);
	}

	@SuppressWarnings("unchecked")
	private I toInternal(E value) {
		return (I) converterRegistry.convert(value, internalType);
	}

	@Override
	public Component asVaadinComponent() {
		return internalForm.asVaadinComponent();
	}

	@Override
	public void render(Properties<E> properties) {
		internalForm.render(Properties.<I>builder()
				.onValueChange(next -> properties.getOnValueChange().accept(toExternal(next)))
				.currentValue(toInternal(properties.getCurrentValue()))
				.initialValue(toInternal(properties.getInitialValue()))
				.nonEditableReasons(properties.getNonEditableReasons())
				.build());
	}
}
