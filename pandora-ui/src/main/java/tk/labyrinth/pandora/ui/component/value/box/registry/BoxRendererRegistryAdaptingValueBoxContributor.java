package tk.labyrinth.pandora.ui.component.value.box.registry;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.views.box.BoxRenderer;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;

@Bean
@RequiredArgsConstructor
public class BoxRendererRegistryAdaptingValueBoxContributor implements ValueBoxContributor {

	private final BoxRendererRegistry boxRendererRegistry;

	private <T> FunctionalMutableValueBox<T> createValueBox(BoxRenderer<T> boxRenderer) {
		return new FunctionalMutableValueBox<>(
				properties -> boxRenderer
						.render(builder -> builder
								.currentValue(properties.getCurrentValue())
								.initialValue(properties.getInitialValue())
								.label(properties.getLabel())
								.nonEditableReasons(properties.getNonEditableReasons())
								.onValueChange(properties.getOnValueChange())
								.build())
						.asVaadinComponent(),
				boxRenderer.getValueType());
	}

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		BoxRenderer<?> boxRenderer = boxRendererRegistry.getRenderer(convertContext(context));
		//
		return createValueBox(boxRenderer);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return boxRendererRegistry
				.selectRendererContributors(convertContext(context)).getEvaluatedResults()
				.getBestDistance();
	}

	public static BoxRendererRegistry.Context convertContext(ValueBoxRegistry.Context context) {
		return BoxRendererRegistry.Context.builder()
				.datatype(context.getDatatype())
				.features(context.getFeatures())
				.build();
	}
}
