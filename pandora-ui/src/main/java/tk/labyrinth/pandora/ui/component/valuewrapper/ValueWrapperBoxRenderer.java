package tk.labyrinth.pandora.ui.component.valuewrapper;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapperKind;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;

import java.util.function.Consumer;
import java.util.function.Function;

public class ValueWrapperBoxRenderer {

	public static ValueWrapperKind determineKind(@Nullable ValueWrapper valueWrapper) {
		ValueWrapperKind result;
		{
			if (valueWrapper != null) {
				if (valueWrapper instanceof ListValueWrapper) {
					result = ValueWrapperKind.LIST;
				} else if (valueWrapper instanceof ObjectValueWrapper) {
					result = ValueWrapperKind.OBJECT;
				} else if (valueWrapper instanceof SimpleValueWrapper) {
					result = ValueWrapperKind.SIMPLE;
				} else {
					throw new UnreachableStateException();
				}
			} else {
				result = ValueWrapperKind.SIMPLE;
			}
		}
		return result;
	}

	public static Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(ValueWrapperBoxRenderer.Parameters.builder()));
	}

	public static Component render(Parameters parameters) {
		ValueWrapperKind valueWrapperKind = determineKind(parameters.value());
		//
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.setAlignItems(AlignItems.BASELINE);
		}
		{
			{
				Component valueWrapperBox = switch (valueWrapperKind) {
					case LIST -> ListValueWrapperBoxRenderer.render(
							ListValueWrapperBoxRenderer.Properties.builder()
									.label(parameters.label())
									.nonEditableReasons(parameters.nonEditableReasons())
									.onValueChange(parameters.onValueChange() != null
											? nextValue -> parameters.onValueChange().accept(nextValue)
											: null)
									.value((ListValueWrapper) parameters.value())
									.build());
					case OBJECT -> ObjectValueWrapperBoxRenderer.render(
							ObjectValueWrapperBoxRenderer.Properties.builder()
									.label(parameters.label())
									.nonEditableReasons(parameters.nonEditableReasons())
									.onValueChange(parameters.onValueChange() != null
											? nextValue -> parameters.onValueChange().accept(nextValue)
											: null)
									.value((ObjectValueWrapper) parameters.value())
									.build());
					case SIMPLE -> SimpleValueWrapperBoxRenderer.render(
							SimpleValueWrapperBoxRenderer.Parameters.builder()
									.label(parameters.label())
									.nonEditableReasons(parameters.nonEditableReasons())
									.onValueChange(parameters.onValueChange() != null
											? nextValue -> parameters.onValueChange().accept(nextValue)
											: null)
									.value((SimpleValueWrapper) parameters.value())
									.build());
				};
				CssFlexItem.setFlexGrow(valueWrapperBox, 1);
				layout.add(valueWrapperBox);
			}
			{
				Button kindButton = new Button(switch (valueWrapperKind) {
					case LIST -> "[ ]";
					case OBJECT -> "{ }";
					case SIMPLE -> "\"a\"";
				});
				{
					kindButton.addThemeVariants(ButtonVariant.LUMO_ICON);
				}
				{
					ContextMenu contextMenu = new ContextMenu();
					{
						contextMenu.setOpenOnClick(true);
					}
					{
						List
								.of(
										ValueWrapperKind.SIMPLE,
										ValueWrapperKind.OBJECT,
										ValueWrapperKind.LIST)
								.forEach(valueWrapperKindToSelect -> contextMenu.addItem(
										switch (valueWrapperKindToSelect) {
											case LIST -> "List [ ]";
											case OBJECT -> "Object { }";
											case SIMPLE -> "Simple \"a\"";
										},
										event -> {
											if (valueWrapperKindToSelect != valueWrapperKind) {
												if (parameters.onValueChange() != null) {
													parameters.onValueChange().accept(
															switch (valueWrapperKindToSelect) {
																case LIST -> ListValueWrapper.empty();
																case OBJECT -> ObjectValueWrapper.of(GenericObject.empty());
																case SIMPLE -> null;
															});
												}
											}
										}));
					}
					contextMenu.setTarget(kindButton);
				}
				layout.add(kindButton);
			}
		}
		return layout;
	}

	// TODO: Ensure kind and value are consistent.
	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<ValueWrapper> onValueChange;

		@Nullable
		ValueWrapper value;

		public static class Builder {
			// Lomboked
		}
	}
}
