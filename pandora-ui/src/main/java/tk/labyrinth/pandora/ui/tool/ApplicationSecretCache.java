package tk.labyrinth.pandora.ui.tool;

import jakarta.annotation.PostConstruct;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import tk.labyrinth.pandora.misc4j.java.util.Base64Utils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;

@ConditionalOnProperty("pandora.persistent-cache-key-password")
@LazyComponent
public class ApplicationSecretCache {

	@Value("${pandora.persistent-cache-key-password}")
	private String keyPassword;

	private SecretKey secretKey;

	@PostConstruct
	private void postConstruct() throws GeneralSecurityException {
		secretKey = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
				.generateSecret(new PBEKeySpec(keyPassword.toCharArray(), new byte[]{0}, 256, 256));
		//
		secretKey = new SecretKeySpec(secretKey.getEncoded(), "AES");
	}

	@Nullable
	public String find(String key) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			//
			byte[] bytes = cipher.doFinal(Base64Utils.decodeStringToBytes(key, StandardCharsets.UTF_8));
			//
			String value = new String(bytes, StandardCharsets.UTF_8);
			//
			return value;
		} catch (GeneralSecurityException ex) {
			throw new RuntimeException(ex);
		}
	}

	public String put(String value) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			//
			byte[] bytes = cipher.doFinal(value.getBytes(StandardCharsets.UTF_8));
			//
			String key = Base64Utils.encodeBytesToString(bytes, StandardCharsets.UTF_8);
			//
			return key;
		} catch (GeneralSecurityException ex) {
			throw new RuntimeException(ex);
		}
	}
}
