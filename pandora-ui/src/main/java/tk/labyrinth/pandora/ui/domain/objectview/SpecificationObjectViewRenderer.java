package tk.labyrinth.pandora.ui.domain.objectview;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Function;

public class SpecificationObjectViewRenderer {

	public static Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Component render(Parameters parameters) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout modelLayout = new CssHorizontalLayout();
				{
					modelLayout.add("Model: ");
					//
					modelLayout.add(parameters.objectModel().getCode());
					modelLayout.add(ButtonRenderer.render(builder -> builder
							.enabled(false)
							.icon(VaadinIcon.EDIT.create())
							.build()));
				}
				layout.add(modelLayout);
			}
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		ObjectModel objectModel;

		public static class Builder {
			// Lomboked
		}
	}
}
