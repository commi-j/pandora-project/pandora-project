package tk.labyrinth.pandora.ui.domain.sidebar;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel(code = SidebarCategoryConfiguration.MODEL_CODE, primaryAttribute = "slug")
@Value
@With
public class SidebarCategoryConfiguration {

	public static final String MODEL_CODE = "sidebarcategoryconfiguration";

	public static final String SLUG_ATTRIBUTE_NAME = "slug";

	List<PandoraSidebarItem> items;

	String slug;

	String text;

	public List<PandoraSidebarItem> getItemsOrEmpty() {
		return items != null ? items : List.empty();
	}
}
