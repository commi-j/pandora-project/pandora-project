package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import javax.annotation.CheckForNull;
import java.util.function.Consumer;

// TODO: Replace Boolean with Tri-state enum.
@Slf4j
public class Checkbox implements RenderableView<Checkbox.Properties> {

	private final com.vaadin.flow.component.checkbox.Checkbox checkbox =
			new com.vaadin.flow.component.checkbox.Checkbox();

	private boolean eventHandlingInProgress = false;

	private Properties properties = Properties.builder().build();

	private boolean renderInProgress = false;

	{
		checkbox.addValueChangeListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						checkbox.setValue(event.getOldValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					Consumer<Boolean> onValueChange = properties.getOnValueChange();
					if (onValueChange != null) {
						onValueChange.accept(event.getValue());
					}
				}
			}
		});
	}

	private void doRender(Properties properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		checkbox.setLabel(properties.getLabel());
		checkbox.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				checkbox,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		checkbox.setValue(properties.getValue());
	}

	@Override
	public Component asVaadinComponent() {
		return checkbox;
	}

	@Override
	public void render(Properties properties) {
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@CheckForNull
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@CheckForNull
		Consumer<Boolean> onValueChange;

		@CheckForNull
		Boolean value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
