package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.function.Consumer;

@Deprecated
@Slf4j
public class StringBox implements SimpleValueBox<String> {

	private final TextField textField = new TextField();

	private boolean eventHandlingInProgress = false;

	private Properties<String> properties;

	private boolean renderInProgress = false;

	{
		textField.addValueChangeListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						textField.setValue(event.getOldValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					Consumer<String> onValueChange = properties.getOnValueChange();
					if (onValueChange != null) {
						onValueChange.accept(!event.getValue().isEmpty() ? event.getValue() : null);
					}
				}
			}
		});
	}

	private void doRender(Properties<String> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		textField.setLabel(properties.getLabel());
		textField.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				textField,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		textField.setValue(properties.getValue() != null ? properties.getValue() : "");
	}

	@Override
	public Component asVaadinComponent() {
		return textField;
	}

	@Override
	public void render(Properties<String> properties) {
		if (renderInProgress) {
			throw new IllegalStateException("Invoked render while already rendering");
		}
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}
}
