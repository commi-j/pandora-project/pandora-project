package tk.labyrinth.pandora.ui.domain.objectview;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;

import java.util.function.Consumer;
import java.util.function.Function;

public interface GenericObjectViewRenderer {

	default Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	Component render(Parameters parameters);

	static GenericObjectViewRenderer wrap(Function<Parameters, Component> renderFunction) {
		return renderFunction::apply;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	class Parameters {

		@NonNull
		GenericObject currentValue;

		@NonNull
		Datatype datatype;

		@NonNull
		GenericObject initialValue;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		ObjectModel objectModel;

		@NonNull
		Consumer<GenericObject> onValueChange;

		/**
		 * Contains attributes that are not expected to be modified by current renderer.
		 */
		// TODO: We want a smarter object that will explain why certain attributes are unmodifiable.
		@NonNull
		GenericObject unmodifiableValue;

		public static class Builder {
			// Lomboked
		}
	}
}
