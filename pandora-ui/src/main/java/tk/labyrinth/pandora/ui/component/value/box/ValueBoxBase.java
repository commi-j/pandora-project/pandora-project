package tk.labyrinth.pandora.ui.component.value.box;

import java.util.Objects;

@Deprecated
public abstract class ValueBoxBase<T> implements ValueBox<T> {

	private Properties<T> properties = getInitialProperties();

	protected Properties<T> getInitialProperties() {
		return Properties.<T>builder().build();
	}

	protected abstract void render(Properties<T> properties);

	@Override
	public Properties<T> getProperties() {
		return properties;
	}

	@Override
	public void setProperties(Properties<T> properties) {
		if (!Objects.equals(properties, this.properties)) {
			this.properties = properties;
			render(properties);
		}
	}
}
