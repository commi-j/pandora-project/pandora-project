package tk.labyrinth.pandora.ui.component.view;

public interface RenderableView<P> extends View {

	void render(P properties);
}
