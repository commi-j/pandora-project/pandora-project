package tk.labyrinth.pandora.ui.renderer.tostring;

import org.checkerframework.checker.nullness.qual.Nullable;

public abstract class ToStringRendererBase<T> implements ToStringRenderer<T> {

	protected abstract String renderNonNull(Context context, T value);

	@Nullable
	@Override
	public String render(Context context, @Nullable T value) {
		return value != null ? renderNonNull(context, value) : null;
	}
}
