package tk.labyrinth.pandora.ui.renderer.tostring;

import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

public interface ToStringRenderer<T> extends HasSupportDistance<ToStringRenderer.Context>, TypeAware<T> {

	Integer MAX_DISTANCE = Integer.MAX_VALUE;

	Integer MAX_DISTANCE_MINUS_FOUR = MAX_DISTANCE - 4;

	Integer MAX_DISTANCE_MINUS_ONE = MAX_DISTANCE - 1;

	Integer MAX_DISTANCE_MINUS_THREE = MAX_DISTANCE - 3;

	Integer MAX_DISTANCE_MINUS_TWO = MAX_DISTANCE - 2;

	// Overrides to rename parameter.
	@Nullable
	@Override
	Integer getSupportDistance(Context context);

	@Nullable
	String render(Context context, @Nullable T value);

	@Builder(toBuilder = true)
	@Value
	class Context {

		@Nullable
		Datatype datatype;

		Object hints;

		ToStringRendererRegistry rendererRegistry;
	}
}
