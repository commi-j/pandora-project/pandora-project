package tk.labyrinth.pandora.ui.domain.route;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;

public interface PandoraRouteContributor {

	@Nullable
	PageRoute contributeRouteForClass(Class<?> cl, Object hints);
}
