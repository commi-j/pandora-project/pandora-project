package tk.labyrinth.pandora.ui.component.value.list;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import javax.annotation.CheckForNull;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Slf4j
public class VavrListBox<E> implements RenderableView<VavrListBox.Properties<E>> {

	private final Button editButton = new Button();

	private final Observable<State<E>> stateObservable = Observable.notInitialized();

	private final TextField textField = new TextField();

	private Runnable editRunnable;

	{
		{
			textField.setReadOnly(true);
		}
		{
			editButton.addClickListener(event -> editRunnable.run());
			editButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
			editButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
			textField.setSuffixComponent(editButton);
		}
		{
			stateObservable.subscribe(nextState -> {
				Properties<E> properties = nextState.getProperties();
				//
				editRunnable = () -> {
					List<E> initialValue = properties.getInitialValue() != null
							? properties.getInitialValue()
							: List.empty();
					List<E> currentValue = properties.getCurrentValue() != null
							? properties.getCurrentValue()
							: initialValue;
					//
					Observable<List<E>> currentValueObservable = Observable.withInitialValue(currentValue);
					//
					StandaloneVavrListView<E> view = new StandaloneVavrListView<>();
					view.render(StandaloneVavrListView.Properties.<E>builder()
							.currentValue(currentValue)
							.elementBoxSupplier(properties.getElementBoxSupplier())
							.initialValue(initialValue)
							.newElementSupplier(() -> null)
							.nonEditableReasons(properties.getNonEditableReasons())
							.onValueChange(currentValueObservable::set)
							.build());
					//
					StyleUtils.setWidth(view.asVaadinComponent(), "49em");
					ConfirmationViews.showViewDialog(view).subscribeAlwaysAccepted(success ->
							properties.getOnValueChange().accept(currentValueObservable.get()));
				};
				//
				textField.setLabel(properties.getLabel());
				editButton.setIcon(properties.calcEditable() ? VaadinIcon.EDIT.create() : VaadinIcon.EYE.create());
				ElementUtils.setTitle(
						textField,
						StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
				{
					textField.setValue(properties.getCurrentValue() != null
							? properties.getCurrentValue()
							.map(element -> properties.getElementRenderer() != null
									? properties.getElementRenderer().apply(element)
									: String.valueOf(element))
							.mkString(", ")
							: "");
				}
			});
		}
		{
			// Tricky solution to preserve focus on rerender.
			textField.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
	}

	private void doRender(Properties<E> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		stateObservable.set(State.<E>builder()
				.properties(properties)
				.build());
	}

	@Override
	public Component asVaadinComponent() {
		return textField;
	}

	@Override
	public void render(Properties<E> properties) {
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		}
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties<E> {

		List<E> currentValue;

		Supplier<MutableValueBox<E>> elementBoxSupplier;

		@CheckForNull
		Function<@Nullable E, String> elementRenderer;

		List<E> initialValue;

		/**
		 * Present in form boxes, absent in list boxes.
		 */
		@CheckForNull
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<List<E>> onValueChange;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State<E> {

		Properties<E> properties;
	}
}
