package tk.labyrinth.pandora.ui.component.reference;

import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcher;

public abstract class ReferenceRendererBase<T, R extends Reference<T>> implements
		ReferenceRenderer<T, R>,
		TypeAware<R> {

	@SmartAutowired
	private ObjectSearcher<PlainQuery, T> objectSearcher;

	@Override
	public ReferenceItem<R> renderNonNullReference(R value) {
		T object = objectSearcher.findSingle(value);
		return object != null ? renderNonNullObject(object) : ReferenceItem.ofMissing(value);
	}
}
