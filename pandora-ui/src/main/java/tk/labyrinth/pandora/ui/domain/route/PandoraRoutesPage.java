package tk.labyrinth.pandora.ui.domain.route;

import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.advanced.GridWrapper;
import tk.labyrinth.pandora.functionalcomponents.html.AnchorRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.UUID;

@RequiredArgsConstructor
@Route(value = "pandora/routes", layout = ConfigurableAppLayout.class)
@Slf4j
public class PandoraRoutesPage extends CssVerticalLayout {

	@SmartAutowired
	private TypedObjectSearcher<PageRoute> pandoraRouteSearcher;

	@PostConstruct
	private void postConstruct() {
		{
			addClassName(PandoraStyles.LAYOUT);
		}
		{
			List<PageRoute> pandoraRoutes = pandoraRouteSearcher.searchAll()
					.map(pandoraRoute -> pandoraRoute.withUid(UUID.randomUUID()));
			//
			GridWrapper<PageRoute> grid = new GridWrapper<>(PageRoute::getUid);
			{
				grid
						.addComponentColumn(item -> AnchorRenderer.render(builder -> builder
								.href(item.getPath())
								.text(item.getPath())
								.build()))
						.setHeader("Path")
						.setResizable(true);
				grid
						.addComponentColumn(item -> GridUtils.renderValueList(
								item.getParameterMappingsOrEmpty(),
								element -> "%s = %s".formatted(element.getTo(), element.getFromSchema())))
						.setHeader("Parameter Mappings")
						.setResizable(true);
				grid
						.addColumn(PageRoute::getPageReference)
						.setHeader("Page")
						.setResizable(true);
			}
			{
				grid.setItems(pandoraRoutes);
			}
			add(grid);
		}
	}
}
