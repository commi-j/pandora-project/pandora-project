package tk.labyrinth.pandora.ui.style;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.dom.ThemeList;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.stereotype.Service;

/**
 * https://vaadin.com/learn/tutorials/toggle-dark-theme
 */
@Service
public class LumoThemeHandler {

	public boolean isDarkTheme() {
		return UI.getCurrent().getElement().getThemeList().contains(Lumo.DARK);
	}

	public void setDarkTheme(boolean darkTheme) {
		ThemeList themeList = UI.getCurrent().getElement().getThemeList();
		if (darkTheme != themeList.contains(Lumo.DARK)) {
			if (darkTheme) {
				themeList.add(Lumo.DARK);
			} else {
				themeList.remove(Lumo.DARK);
			}
		}
	}

	public boolean toggleDarkTheme() {
		boolean result;
		{
			ThemeList themeList = UI.getCurrent().getElement().getThemeList();
			if (!themeList.contains(Lumo.DARK)) {
				themeList.add(Lumo.DARK);
				result = true;
			} else {
				themeList.remove(Lumo.DARK);
				result = false;
			}
		}
		return result;
	}
}
