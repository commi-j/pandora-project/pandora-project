package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

@LazyComponent
@RequiredArgsConstructor
public class VavrMapToVaadinComponentRenderer extends ToVaadinComponentRendererBase<Map<?, ?>> {

	public static final int LARGE_THRESHOLD = 5;

	public static final Object MAP_KEY_HINT = new Object();

	public static final Object MAP_VALUE_HINT = new Object();

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, Map<?, ?> value) {
		CssVerticalLayout result = new CssVerticalLayout();
		{
			boolean large = value.size() > LARGE_THRESHOLD;
			//
			value
					.take(large ? LARGE_THRESHOLD - 1 : LARGE_THRESHOLD)
					.forEach(tuple -> {
						CssHorizontalLayout tupleLayout = new CssHorizontalLayout();
						tupleLayout.add(context.getRendererRegistry().render(
								context
										.withDatatype(context.getDatatype().getParameters().get(0))
										.withHints(context.getHints().append(MAP_KEY_HINT))
										.toRegistryContext(),
								tuple._1()));
						tupleLayout.add(" : ");
						tupleLayout.add(context.getRendererRegistry().render(
								context
										.withDatatype(context.getDatatype().getParameters().get(1))
										.withHints(context.getHints().append(MAP_VALUE_HINT))
										.toRegistryContext(),
								tuple._2()));
						result.add(tupleLayout);
					});
			//
			if (large) {
				result.add(new Span("%s more".formatted(value.size() - (LARGE_THRESHOLD - 1))));
			}
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), Map.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}
}
