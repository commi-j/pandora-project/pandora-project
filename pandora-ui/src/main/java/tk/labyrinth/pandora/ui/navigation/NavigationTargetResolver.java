package tk.labyrinth.pandora.ui.navigation;

import com.vaadin.flow.router.Location;

public interface NavigationTargetResolver<T extends NavigationTargetHandler> {

	Location resolveNavigationTarget(NavigationTargetHandler navigationTargetHandler);
}
