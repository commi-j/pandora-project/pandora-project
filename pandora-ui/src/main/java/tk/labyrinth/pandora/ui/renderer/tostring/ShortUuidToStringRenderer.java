package tk.labyrinth.pandora.ui.renderer.tostring;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.java.util.UuidUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRenderer;

import java.util.UUID;

@LazyComponent
@RequiredArgsConstructor
public class ShortUuidToStringRenderer extends ToStringRendererBase<UUID> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected String renderNonNull(Context context, UUID value) {
		return UuidUtils.uuidToShortString(value);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(
				context.getDatatype(),
				JavaBaseTypeUtils.createDatatypeBaseReference(UUID.class))
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}
}
