package tk.labyrinth.pandora.ui.domain.objectview;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.diversity.domain.bean.EagerBean;

@EagerBean
@RequiredArgsConstructor
public class SpecificationObjectViewRendererContributor {

	private final ObjectViewRendererRegistry objectViewRendererRegistry;

	@PostConstruct
	private void postConstruct() {
		objectViewRendererRegistry.registerDefinition(ObjectViewRendererDefinition.builder()
				.distance(1000)
				.objectViewRenderer(parameters -> SpecificationObjectViewRenderer.render(builder -> builder
						.objectModel(parameters.objectModel())
						.build()))
				.slug("specification")
				.text("Specification")
				.build());
	}
}
