package tk.labyrinth.pandora.ui.domain.applayout;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarConfiguration;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class PandoraAppLayoutConfiguration {

	Integer priority;

	@NonNull
	SidebarConfiguration sidebar;

	@NonNull
	Boolean useHeader;
}
