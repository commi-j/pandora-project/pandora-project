package tk.labyrinth.pandora.ui.paginator;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.JustifyContent;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class Paginator extends CssHorizontalLayout implements RenderableView<Paginator.Properties> {

	{
		addClassName("paginator");
		//
		setJustifyContent(JustifyContent.CENTER);
	}

	private Button createSelectableButton(int pageNumber, Consumer<Integer> onPageSelect) {
		Button result = new Button(Integer.toString(pageNumber), event -> {
			if (onPageSelect != null) {
				onPageSelect.accept(pageNumber - 1);
			}
		});
		result.addThemeVariants(ButtonVariant.LUMO_ICON);
		result.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		return result;
	}

	@Override
	public Component asVaadinComponent() {
		return this;
	}

	@Override
	public void render(Properties properties) {
		removeAll();
		//
		int selectedPageNumber = properties.getSelectedPageIndex() + 1;
		List<Integer> offsets = List.of(1, 2, 5);
		int buttonsAtEachSide = offsets.size() + 1;
		{
			List<Integer> pageNumbersAtLeftSide = PaginatorUtils.getIndicesAtLeftSide(offsets, selectedPageNumber);
			IntStream.range(0, buttonsAtEachSide - pageNumbersAtLeftSide.size()).forEach(index ->
					add(createInvisibleButton()));
			pageNumbersAtLeftSide.forEach(pageNumber -> add(createSelectableButton(
					pageNumber,
					properties.getOnSelectedPageIndexChange())));
		}
		{
			add(createSelectedButton(selectedPageNumber));
		}
		{
			List<Integer> pageNumbersAtRightSide = PaginatorUtils.getIndicesAtRightSide(
					offsets,
					selectedPageNumber,
					properties.computePageCount());
			pageNumbersAtRightSide.forEach(pageNumber -> add(createSelectableButton(
					pageNumber,
					properties.getOnSelectedPageIndexChange())));
			IntStream.range(0, buttonsAtEachSide - pageNumbersAtRightSide.size()).forEach(index ->
					add(createInvisibleButton()));
		}
	}

	private static Button createInvisibleButton() {
		Button result = new Button();
		result.addThemeVariants(ButtonVariant.LUMO_ICON);
		result.getStyle().set("visibility", "hidden");
		return result;
	}

	private static Button createSelectedButton(int pageNumber) {
		Button result = new Button(Integer.toString(pageNumber));
		result.addThemeVariants(ButtonVariant.LUMO_ICON);
		result.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		return result;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		/**
		 * Returns page index.
		 */
		@NonNull
		Consumer<Integer> onSelectedPageIndexChange;

		@Nullable
		Integer pageCount;

		@NonNull
		Integer selectedPageIndex;

		int computePageCount() {
			return pageCount != null ? pageCount : selectedPageIndex;
		}
	}
}
