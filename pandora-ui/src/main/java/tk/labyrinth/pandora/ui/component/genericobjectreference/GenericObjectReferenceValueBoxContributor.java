package tk.labyrinth.pandora.ui.component.genericobjectreference;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.referencemodel.ReferenceModelRegistry;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

import java.lang.reflect.Type;

// TODO: Rework into proper GenericReferenceValueBox, potentially wrapped with JBT.
@LazyComponent
@RequiredArgsConstructor
public class GenericObjectReferenceValueBoxContributor implements ValueBoxContributor {

	private final ConverterRegistry converterRegistry;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericObjectReferenceBoxRenderer genericObjectReferenceBoxRenderer;

	private final ReferenceModelRegistry referenceModelRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		MutableValueBox<?> result;
		{
			Type javaType = JavaBaseTypeUtils.getJavaTypeFromDatatype(context.getDatatype());
			//
			result = new FunctionalMutableValueBox<>(
					parameters -> genericObjectReferenceBoxRenderer
							.render(builder -> builder
									.currentValue(converterRegistry.convert(
											parameters.getCurrentValue(),
											GenericObjectReference.class))
									.initialValue(converterRegistry.convert(
											parameters.getInitialValue(),
											GenericObjectReference.class))
									.label(parameters.getLabel())
									.nonEditableReasons(parameters.getNonEditableReasons())
									.referenceModel(referenceModelRegistry.createReferenceModel(javaType))
									.onValueChange(nextValue -> parameters.getOnValueChange().accept(
											converterRegistry.convert(nextValue, javaType)))
									.build())
							.asVaadinComponent(),
					javaType);
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.isReference(context.getDatatype())
				? ToStringRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
