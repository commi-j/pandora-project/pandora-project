package tk.labyrinth.pandora.ui.component.valuewrapper;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.ui.component.value.SimpleValueBoxRenderer;

import java.util.function.Consumer;
import java.util.function.Function;

public class SimpleValueWrapperBoxRenderer {

	public static Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Component render(Parameters parameters) {
		return SimpleValueBoxRenderer.render(builder -> builder
				.label(parameters.label())
				.nonEditableReasons(parameters.nonEditableReasons())
				.onValueChange(parameters.onValueChange() != null
						? nextValue -> parameters.onValueChange().accept(nextValue != null
						? SimpleValueWrapper.of(nextValue)
						: null)
						: null)
				.value(parameters.value() != null ? parameters.value().unwrap() : null)
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<SimpleValueWrapper> onValueChange;

		@Nullable
		SimpleValueWrapper value;

		public static class Builder {
			// Lomboked
		}
	}
}
