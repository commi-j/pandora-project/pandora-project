package tk.labyrinth.pandora.ui.domain.sidebar;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class SidebarConfiguration {

	public enum SidebarMode {
		SLIDE,
		SNAP,
	}

	@NonNull
	List<SlugSidebarCategoryConfigurationReference> categoryReferences;

	@NonNull
	SidebarMode mode;
}
