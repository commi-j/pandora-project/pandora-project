package tk.labyrinth.pandora.ui.routing.route;

import io.vavr.collection.Map;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.ui.routing.routetarget.RouteTarget;

@Deprecated
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GenericRoute {

	From from;

	RouteTarget to;

	@lombok.Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class From {

		Map<String, String> parameterMappings;

		String path;
	}
}
