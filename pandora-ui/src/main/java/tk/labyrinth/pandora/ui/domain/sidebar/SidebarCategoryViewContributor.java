package tk.labyrinth.pandora.ui.domain.sidebar;

import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.core.meta.PandoraJavaContext;
import tk.labyrinth.pandora.functionalvaadin.view.View;

@PandoraExtensionPoint
@PandoraJavaContext
public interface SidebarCategoryViewContributor {

	View contributeView(SidebarCategoryConfiguration configuration);

	boolean matches(SidebarCategoryConfiguration configuration);
}
