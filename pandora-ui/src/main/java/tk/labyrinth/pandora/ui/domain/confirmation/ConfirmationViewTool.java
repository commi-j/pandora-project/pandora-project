package tk.labyrinth.pandora.ui.domain.confirmation;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;

@Bean
@RequiredArgsConstructor
public class ConfirmationViewTool {

	private final BeanContext beanContext;

	public <T> ConfirmationHandle<T> showTypedViewDialog(T parameters) {
		// TODO: Enforce parameter.class to be plain. For generic classes we need to use method with Type parameter
		//  to support generics.
		//
		ObjectForm<T> objectForm = beanContext.getBeanFactory().getBean(TypeUtils.parameterize(
				ObjectForm.class,
				parameters.getClass()));
		Observable<T> stateObservable = Observable.withInitialValue(parameters);
		//
		stateObservable.subscribe((next, sink) -> objectForm.render(ObjectForm.Properties.<T>builder()
				.currentValue(next)
				.initialValue(parameters)
				.onValueChange(sink)
				.build()));
		//
		return ConfirmationViews.showViewDialog(
				objectForm,
				stateObservable::get);
	}
}
