package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.TextDecoration;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class TypeSignatureToVaadinComponentRenderer extends ToVaadinComponentRendererBase<TypeSignature> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, TypeSignature value) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			{
				Div div = new Div();
				{
					String shortText = value.getLongName();
					String fullText = value.toString();
					if (!Objects.equals(shortText, fullText)) {
						StyleUtils.setCssProperty(div, TextDecoration.UNDERLINE);
						ElementUtils.setTitle(div, fullText);
					}
					div.add(shortText);
				}
				layout.add(div);
			}
			layout.add(StringUtils.repeat("[]", value.getArrayCount()));
		}
		return layout;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), TypeSignature.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_TWO
				: null;
	}
}
