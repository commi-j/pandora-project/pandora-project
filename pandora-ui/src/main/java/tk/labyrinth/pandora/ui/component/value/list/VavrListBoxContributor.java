package tk.labyrinth.pandora.ui.component.value.list;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;

import javax.annotation.Nullable;

// FIXME: We can use just ListBoxContributor.
//  Or make VavrListBoxContributor support list to retain it's boundary to Vavr?
@Deprecated
@Bean
@RequiredArgsConstructor
public class VavrListBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final ToStringRendererRegistry toStringRendererRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		Datatype firstParameter = context.getDatatype().getParameters().get(0);
		//
		return ListBoxContributor.createValueBox(
				JavaBaseTypeUtils.getJavaTypeFromDatatype(context.getDatatype()),
				firstParameter,
				() -> registry.getValueBox(firstParameter),
				toStringRendererRegistry);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), List.class)
				? ToStringRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
