package tk.labyrinth.pandora.ui.domain.state.model;

import tk.labyrinth.pandora.ui.domain.state.UiStateAttributeKind;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface UiStateAttribute {

	String NO_VALUE = "__NO_VALUE__";

	/**
	 * Name of the attribute within UiState. If value is empty or not specified, name of annotated element is used.
	 *
	 * @return non-null
	 */
	String attributeName() default "";

	/**
	 * Default value as seen in the url.
	 *
	 * @return non-null
	 */
	String defaultValue() default NO_VALUE;

	/**
	 * Name of the attribute as seen in the url query. If value is empty or not specified,
	 * {@link #attributeName()} is used.
	 *
	 * @return non-null
	 */
	String queryAttributeName() default "";

	UiStateAttributeKind kind() default UiStateAttributeKind.SERVER;

	/**
	 * Will render 'attribute=' in case of null value.
	 * This is viable when you want to demonstrate certain parameter is available to user.
	 *
	 * @return true or false
	 */
	boolean renderIfNull() default false;
}
