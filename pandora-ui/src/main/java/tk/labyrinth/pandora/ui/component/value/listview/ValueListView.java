package tk.labyrinth.pandora.ui.component.value.listview;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.ui.component.value.view.ValueView;

import javax.annotation.CheckForNull;
import java.util.function.Consumer;

@Deprecated
public interface ValueListView<T> extends TypeAware<T>, ValueView<List<T>> {

	Properties<T> getProperties();

	@Override
	default List<T> getValue() {
		return getProperties().getCurrentValue();
	}

	default void setChangeListener(Consumer<List<T>> changeListener) {
		setProperties(getProperties().withChangeListener(changeListener));
	}

	void setProperties(Properties<T> properties);

	default void setValue(List<T> value) {
		setValue(value, value);
	}

	default void setValue(List<T> initialValue, List<T> currentValue) {
		setProperties(getProperties().toBuilder()
				.currentValue(currentValue)
				.initialValue(initialValue)
				.build());
	}

	default ValueListView<T> withProperties(Properties<T> properties) {
		setProperties(properties);
		return this;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	class Properties<T> {

		@CheckForNull
		Consumer<List<T>> changeListener;

		@NonNull
		List<T> currentValue;

		@NonNull
		List<T> initialValue;
	}
}
