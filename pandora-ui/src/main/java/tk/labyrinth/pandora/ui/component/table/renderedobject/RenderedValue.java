package tk.labyrinth.pandora.ui.component.table.renderedobject;

import lombok.Value;

import java.awt.*;

public interface RenderedValue {

	@Value(staticConstructor = "of")
	class ComponentVariant implements RenderedValue {

		Component component;
	}

	@Value(staticConstructor = "of")
	class StringVariant implements RenderedValue {

		String string;
	}
}
