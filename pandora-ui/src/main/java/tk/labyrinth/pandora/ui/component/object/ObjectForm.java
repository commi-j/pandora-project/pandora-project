package tk.labyrinth.pandora.ui.component.object;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import java.util.function.Consumer;

public interface ObjectForm<T> extends RenderableView<ObjectForm.Properties<T>> {

	@Builder(toBuilder = true)
	@Value
	class Properties<T> {

		@NonNull
		T currentValue;

		@NonNull
		T initialValue;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<T> onValueChange;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
