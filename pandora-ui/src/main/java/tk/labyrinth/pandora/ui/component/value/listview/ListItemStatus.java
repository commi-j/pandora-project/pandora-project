package tk.labyrinth.pandora.ui.component.value.listview;

public enum ListItemStatus {
	ADDED,
	MODIFIED,
	NOT_EXISTING,
	NOT_MODIFIED,
	REMOVED,
}
