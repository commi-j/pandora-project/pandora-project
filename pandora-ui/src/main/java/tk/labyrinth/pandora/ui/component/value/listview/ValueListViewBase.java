package tk.labyrinth.pandora.ui.component.value.listview;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.dnd.GridDropLocation;
import com.vaadin.flow.component.grid.dnd.GridDropMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.function.ValueProvider;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.EnumUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.views.style.PandoraColours;

import javax.annotation.CheckForNull;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Deprecated
public abstract class ValueListViewBase<@Nullable T> implements ValueListView<T> {

	private final Grid<ListItem<T>> grid = new Grid<>();

	@CheckForNull
	private Consumer<List<T>> changeListener = null;

	@Nullable
	private ListItem<T> draggedItem = null;

	private Properties<T> properties = getInitialProperties();

	/**
	 * Can not transform into initializer, because there could be children that use Spring injections.
	 *
	 * @see #initialize()
	 */
	@PostConstruct
	private void postConstruct() {
		{
			// Configure Grid.
			//
//			grid.addClassNames(PandoraStyles.CARD);
			//
			grid.setAllRowsVisible(true);
			grid.setSelectionMode(Grid.SelectionMode.NONE);
			grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
			{
				// Configure item reordering (Grid Dnd).
				//
				grid.addDragEndListener(event -> {
					draggedItem = null;
					grid.setDropMode(null);
				});
				grid.addDragStartListener(event -> {
					ListItem<T> pickedItem = List.ofAll(event.getDraggedItems()).single();
					//
					// Drag is unavailable for items with status NOT_EXISTING.
					if (pickedItem.getStatus() != ListItemStatus.NOT_EXISTING) {
						draggedItem = pickedItem;
						grid.setDropMode(GridDropMode.BETWEEN);
					}
				});
				grid.addDropListener(event -> {
					ListItem<T> dropTarget = event.getDropTargetItem().orElseThrow();
					//
					// Drop is unavailable below items with status NOT_EXISTING.
					if (dropTarget.getStatus() != ListItemStatus.NOT_EXISTING ||
							event.getDropLocation() != GridDropLocation.BELOW) {
						// TODO: Add index to items so that they are rendered as modified.
						//
						List<ListItem<T>> itemsAfterRemoval = List
								.ofAll(grid.getListDataView().getItems())
								.remove(draggedItem);
						int indexOfDropTarget = itemsAfterRemoval.indexOf(dropTarget);
						int indexToInsert = switch (event.getDropLocation()) {
							case ABOVE -> indexOfDropTarget;
							case BELOW -> indexOfDropTarget + 1;
							case EMPTY, ON_TOP -> throw new NotImplementedException();
						};
						setItems(itemsAfterRemoval.insert(indexToInsert, draggedItem));
					}
				});
				grid.setRowsDraggable(true);
			}
		}
		{
			grid
					.addComponentColumn(item -> {
						CssHorizontalLayout layout = new CssHorizontalLayout();
						{
							layout.add(renderActionButton(item));
						}
						{
							Div div = new Div();
							div.setHeight("1em");
							div.setWidth("1em");
							div.getStyle().set(
									"background-color",
									switch (item.getStatus()) {
										case ADDED -> PandoraColours.GREEN;
										case REMOVED -> PandoraColours.RED;
										case MODIFIED -> PandoraColours.BLUE;
										default -> "transparent";
									});
							layout.add(div);
						}
						return layout;
					})
					.setFlexGrow(0);
			//
			configureGrid(grid);
		}
		{
			render(properties);
		}
	}

	private Button renderActionButton(ListItem<T> item) {
		ListItemStatus status = item.getStatus();
		//
		Icon icon = EnumUtils.in(status, ListItemStatus.NOT_EXISTING, ListItemStatus.REMOVED)
				? VaadinIcon.PLUS.create()
				: VaadinIcon.MINUS.create();
		//
		List<ListItem<T>> items = List.ofAll(grid.getListDataView().getItems());
		Supplier<List<ListItem<T>>> callback = switch (status) {
			case ADDED -> () -> items.remove(item);
			case MODIFIED, NOT_MODIFIED -> () -> items.replace(item, item.asRemoved());
			case NOT_EXISTING -> () -> items.init()
					.append(ListItem.ofAdded(createNewElementInstance()))
					.append(ListItem.ofNotExisting());
			case REMOVED -> () -> items.replace(item, item.asNotRemoved());
		};
		//
		return new Button(icon, event -> setItems(callback.get()));
	}

	private void setItems(List<ListItem<T>> items) {
		grid.setItems(items
				.map(item -> Objects.requireNonNull(item, "item, items = %s".formatted(items)))
				.asJava());
		if (changeListener != null) {
			changeListener.accept(items
					.filter(item -> !EnumUtils.in(item.getStatus(), ListItemStatus.NOT_EXISTING, ListItemStatus.REMOVED))
					.map(ListItem::getValue));
		}
	}

	@SuppressWarnings("unchecked")
	protected <V extends Component> ValueProvider<ListItem<T>, V> component(
			ValueProvider<Pair<ListItem<T>, T>, V> componentProvider) {
		// TODO: Find a way to specify empty html element instead of div.
		return item -> item.getStatus() != ListItemStatus.NOT_EXISTING
				? componentProvider.apply(Pair.of(item, item.getValue()))
				: (V) new Div();
	}

	protected abstract void configureGrid(Grid<ListItem<T>> grid);

	protected abstract T createNewElementInstance();

	protected Properties<T> getInitialProperties() {
		return Properties.<T>builder()
				.currentValue(List.empty())
				.initialValue(List.empty())
				.build();
	}

	protected void initialize() {
		postConstruct();
	}

	protected void render(Properties<T> properties) {
		changeListener = null;
		//
		setItems(properties.getCurrentValue()
				.map(ListItem::ofNotModified)
				.append(ListItem.ofNotExisting()));
		//
		changeListener = properties.getChangeListener();
	}

	/**
	 * @param item      non-null
	 * @param nextValue any
	 */
	protected void updateItem(ListItem<T> item, T nextValue) {
		if (Objects.equals(nextValue, item.getOutputValue())) {
			throw new IllegalArgumentException("Require not equal: item = %s, nextValue = %s"
					.formatted(item, nextValue));
		}
		setItems(List
				.ofAll(grid.getListDataView().getItems())
				.replace(item, item.withOutputValue(nextValue)));
	}

	protected ValueProvider<ListItem<T>, ?> value(
			ValueProvider<Pair<ListItem<T>, T>, ?> valueProvider) {
		// TODO: Find a way to specify empty html element instead of div.
		return item -> item.getStatus() != ListItemStatus.NOT_EXISTING
				? valueProvider.apply(Pair.of(item, item.getValue()))
				: null;
	}

	@Override
	public Component asVaadinComponent() {
		return grid;
	}

	@Override
	public Properties<T> getProperties() {
		return properties;
	}

	@Override
	public void setProperties(Properties<T> properties) {
		if (!Objects.equals(properties, this.properties)) {
			this.properties = properties;
			render(properties);
		}
	}
}
