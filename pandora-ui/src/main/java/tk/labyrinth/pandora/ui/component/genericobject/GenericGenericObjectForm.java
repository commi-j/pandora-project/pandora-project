package tk.labyrinth.pandora.ui.component.genericobject;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import io.vavr.control.Option;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Overflow;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.stores.datatypes.datatype.DatatypeRegistry;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.ui.component.value.box.ValueBoxContext;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.lang.reflect.Type;
import java.util.Objects;

@PrototypeScopedComponent
@RequiredArgsConstructor
@Slf4j
public class GenericGenericObjectForm implements GenericObjectForm {

	private final ConverterRegistry converterRegistry;

	private final DatatypeRegistry datatypeRegistry;

	@Getter
	private final CssGridLayout layout = new CssGridLayout();

	private final ValueBoxRegistry valueBoxRegistry;

	private List<Pair<String, MutableValueBox<?>>> attributeNameAndBoxPairs;

	@SmartAutowired
	private Datatype datatype;

	@Getter
	@SmartAutowired
	private ObjectModel objectModel;

	@SuppressWarnings("unchecked")
	private <T> T convertAttributeValue(ValueWrapper attributeValue, Type valueBoxValueType) {
		T result;
		{
			if (attributeValue != null) {
				Object objectToConvert;
				if (attributeValue.isList()) {
					objectToConvert = attributeValue.asList().getValue().map(ValueWrapper::unwrapNullable);
				} else {
					objectToConvert = attributeValue.unwrap();
				}
				result = (T) converterRegistry.convert(objectToConvert, valueBoxValueType);
			} else {
				result = null;
			}
		}
		return result;
	}

	@PostConstruct
	private void postConstruct() {
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setAlignItems(AlignItems.BASELINE);
			layout.setGridTemplateColumns(PandoraStyles.repeatDefaultCellWidth(2));
		}
		{
			attributeNameAndBoxPairs = Option.of(objectModel.getAttributes()).getOrElse(List.empty()).map(attribute -> {
				Datatype rawDatatype = attribute.getDatatype();
				//
				Datatype resolvedDatatype = datatypeRegistry.resolveDatatype(datatype, rawDatatype);
				//
				MutableValueBox<?> valueBox = valueBoxRegistry.getValueBox(ValueBoxRegistry.Context.builder()
						.datatype(resolvedDatatype)
						.features(attribute.getFeaturesOrEmpty())
						.build());
				//
				return Pair.of(attribute.getName(), valueBox);
			});
			attributeNameAndBoxPairs.toStream().forEach(pair -> {
				Component component = pair.getRight().asVaadinComponent();
				//
				StyleUtils.setCssProperty(component, Overflow.HIDDEN);
				//
				// This is for better automatic navigation capabilities, important for browser tests.
				component.getElement().setAttribute("name", "attribute.%s".formatted(pair.getLeft()));
				//
				layout.add(component);
			});
		}
	}

	// TODO: Rendering errors should result in proper error box.
	private <T> void renderAttributeBox(
			String attributeName,
			boolean isSignatureAttribute,
			MutableValueBox<T> valueBox,
			Properties<GenericObject> properties) {
		ValueWrapper currentValueWrapper = properties.getCurrentValue().findAttributeValue(attributeName);
		ValueWrapper initialValueWrapper = properties.getInitialValue().findAttributeValue(attributeName);
		//
		T currentValue, initialValue;
		boolean successfullyConverted;
		//
		try {
			currentValue = convertAttributeValue(currentValueWrapper, valueBox.getParameterType());
			initialValue = convertAttributeValue(initialValueWrapper, valueBox.getParameterType());
			successfullyConverted = true;
		} catch (RuntimeException ex) {
			currentValue = null;
			initialValue = null;
			successfullyConverted = false;
		}
		//
		valueBox.render(MutableValueBox.Properties.<T>builder()
				.context(ValueBoxContext.builder()
						.currentValue(currentValueWrapper)
						.parent(ValueBoxContext.builder()
								.currentValue(ObjectValueWrapper.of(properties.getCurrentValue()))
								.parent(null)
								.path("<root>")
								.build())
						.path(attributeName)
						.build())
				.currentValue(currentValue)
				.initialValue(initialValue)
				.label(successfullyConverted ? attributeName : "%s (conversion error)".formatted(attributeName))
				.nonEditableReasons(properties.getNonEditableReasons()
						.append(checkNonEditableDueToBeingSignature(isSignatureAttribute, initialValueWrapper))
						.filter(Objects::nonNull))
				.onValueChange(next -> properties.getOnValueChange().accept(properties.getCurrentValue()
						.withNullableAttribute(attributeName, converterRegistry.convert(next, ValueWrapper.class))))
				.build());
	}

	@Override
	public Component asVaadinComponent() {
		return layout;
	}

	public MutableValueBox<?> getAttributeBox(String attributeName) {
		return attributeNameAndBoxPairs.find(pair -> Objects.equals(pair.getLeft(), attributeName))
				.map(Pair::getRight)
				.get();
	}

	@Override
	public void render(Properties<GenericObject> properties) {
		String signatureAttributeName = PandoraRootUtils.findSignatureAttributeName(objectModel);
		//
		attributeNameAndBoxPairs.forEach(pair -> renderAttributeBox(
				pair.getLeft(),
				Objects.equals(pair.getLeft(), signatureAttributeName),
				pair.getRight(),
				properties));
	}

	@Nullable
	public static String checkNonEditableDueToBeingSignature(
			boolean isSignatureAttribute,
			ValueWrapper initialValueWrapper) {
		return isSignatureAttribute && initialValueWrapper != null
				? "Signature attributes are non-editable"
				: null;
	}
}
