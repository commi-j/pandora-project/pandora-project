package tk.labyrinth.pandora.ui.datatypes.render;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;

@Value(staticConstructor = "of")
public class GenericObjectReferenceItem {

	@Nullable
	Throwable fault;

	/**
	 * Null means not found.
	 */
	@Nullable
	GenericObjectItem objectItem;

	GenericObjectReference reference;

	public String toDisplayString() {
		String result;
		{
			if (fault != null) {
				result = fault.toString();
			} else {
				if (objectItem != null) {
					result = objectItem.toPrettyString();
				} else {
					result = "Not found: " + reference;
				}
			}
		}
		return result;
	}
}
