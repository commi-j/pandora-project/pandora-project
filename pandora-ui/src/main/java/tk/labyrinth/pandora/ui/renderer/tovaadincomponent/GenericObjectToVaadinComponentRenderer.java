package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootConstants;

import java.util.Objects;

/**
 * Renders object as a list of attributes in form of key = value pairs.
 *
 * @see GenericObjectToInlineVaadinComponentRenderer
 */
@LazyComponent
@RequiredArgsConstructor
public class GenericObjectToVaadinComponentRenderer extends ToVaadinComponentRendererBase<GenericObject> {

	public static final int DISTANCE = ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_ONE;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, GenericObject value) {
		CssVerticalLayout result = new CssVerticalLayout();
		value.getAttributes()
				.filter(attribute -> !Objects.equals(attribute.getName(), PandoraRootConstants.MODEL_REFERENCE_ATTRIBUTE_NAME))
				.forEach(attribute -> {
					Div div = new Div();
					// TODO: Render value as inline.
					div.add("%s: %s".formatted(attribute.getName(), attribute.getValue().unwrap()));
					result.add(div);
				});
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.determineKind(context.getDatatype()) == DatatypeKind.OBJECT
				? DISTANCE
				: null;
	}
}
