package tk.labyrinth.pandora.ui.component.table;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;

import java.util.function.Consumer;

@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class AdvancedTableSelectionModel {

	@NonNull
	Consumer<@Nullable Predicate> onPredicateChange;

	@Nullable
	Predicate predicate;
}
