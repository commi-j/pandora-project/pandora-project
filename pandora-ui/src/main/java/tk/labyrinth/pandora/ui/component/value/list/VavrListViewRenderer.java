package tk.labyrinth.pandora.ui.component.value.list;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.dnd.GridDropMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.checker.nullness.qual.PolyNull;
import tk.labyrinth.pandora.functionalcomponents.advanced.GridWrapper;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.java.lang.EnumUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.ui.component.value.box.ValueBoxContext;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.listview.ListItem;
import tk.labyrinth.pandora.ui.component.value.listview.ListItemStatus;
import tk.labyrinth.pandora.views.style.PandoraColours;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class VavrListViewRenderer {

	private static <E> Button renderActionButton(Parameters<E> parameters, ListItem<E> item) {
		ListItemStatus status = item.getStatus();
		//
		Icon icon = EnumUtils.in(status, ListItemStatus.NOT_EXISTING, ListItemStatus.REMOVED)
				? VaadinIcon.PLUS.create()
				: VaadinIcon.MINUS.create();
		//
		Runnable runnable = () -> {
			if (status != ListItemStatus.NOT_EXISTING) {
				List<ListItem<E>> nextItems = switch (status) {
					case ADDED -> parameters.value().remove(item);
					case MODIFIED, NOT_MODIFIED -> parameters.value().replace(item, item.asRemoved());
					case NOT_EXISTING -> throw new UnreachableStateException();
					case REMOVED -> parameters.value().replace(item, item.asNotRemoved());
				};
				//
				parameters.onValueChange().accept(nextItems);
			} else {
				parameters.onAddElementClick().run();
			}
		};
		//
		return ButtonRenderer.render(builder -> builder
				.icon(icon)
				.onClick(event -> runnable.run())
				.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
				.build());
	}

	public static <@PolyNull E> Component render(Function<Parameters.Builder<E>, Parameters<E>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static <@PolyNull E> Component render(Parameters<E> parameters) {
		GridWrapper<ListItem<E>> grid = new GridWrapper<>(self -> self);
		{
//			grid.addClassNames(PandoraStyles.CARD);
			grid.getContent().setSelectionMode(Grid.SelectionMode.NONE);
			grid.getContent().addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
			//
			grid.getContent().setDragFilter(item -> item.getItem().getStatus() != ListItemStatus.NOT_EXISTING);
			grid.getContent().setDropFilter(item -> item.getItem().getStatus() != ListItemStatus.NOT_EXISTING);
			grid.getContent().setDropMode(GridDropMode.BETWEEN);
			grid.getContent().setRowsDraggable(true);
		}
		{
			grid
					.addComponentColumn(item -> {
						CssHorizontalLayout layout = new CssHorizontalLayout();
						{
							layout.setAlignItems(AlignItems.CENTER);
							//
							layout.getStyle().set("column-gap", "1em");
						}
						{
							{
								layout.add(renderActionButton(parameters, item));
							}
							{
								Div div = new Div();
								//
								div.setHeight("0.75em");
								div.setWidth("0.75em");
								//
								div.getStyle().set(
										"background-color",
										switch (item.getStatus()) {
											case ADDED -> PandoraColours.GREEN;
											case MODIFIED -> PandoraColours.BLUE;
											case REMOVED -> PandoraColours.RED;
											default -> "transparent";
										});
								div.getStyle().set("border-radius", "0.375em");
								//
								div.setTitle(switch (item.getStatus()) {
									case ADDED -> "Added";
									case MODIFIED -> "Modified";
									case REMOVED -> "Removed";
									default -> "";
								});
								//
								layout.add(div);
							}
							{
								// TODO: We want to drag using certain component, not sure we can do it with current Grid.
//							layout.add(ButtonRenderer.render(builder -> builder
//									.icon(VaadinIcon.MENU.create())
//									.onClick(event -> {
//									})
//									.themeVariants(ButtonVariant.LUMO_TERTIARY_INLINE)
//									.build()));
							}
							{
								MutableValueBox<E> elementBox = Objects.requireNonNull(
										parameters.elementBoxSupplier().get(),
										"elementBox");
								//
								elementBox.render(MutableValueBox.Properties.<E>builder()
										.context(ValueBoxContext.builder()
												.currentValue(null) // TODO
												.parent(parameters.context())
												.path("INDEX")
												.build())
										.currentValue(item.isRemoved() ? item.getInputValue() : item.getOutputValue())
										.initialValue(item.getInputValue())
										.nonEditableReasons(item.isRemoved() ? List.of("Removed") : List.empty())
										.onValueChange(nextValue -> parameters.onValueChange().accept(
												parameters.value().replace(item, item.withOutputValue(nextValue))))
										.build());
								//
								StyleUtils.setWidth(elementBox.asVaadinComponent(), "100%");
								//
								layout.add(elementBox.asVaadinComponent());
							}
						}
						return layout;
					});
		}
		{
			AtomicInteger dragIndexWrapper = new AtomicInteger(-1);
			grid.getContent().addDragStartListener(event ->
					dragIndexWrapper.set(parameters.value().indexOf(event.getDraggedItems().get(0).getItem())));
			grid.getContent().addDropListener(event -> {
				List<ListItem<E>> items = parameters.value();
				//
				int dragIndex = dragIndexWrapper.get();
				int dropIndex = items.indexOf(event.getDropTargetItem().get().getItem());
				//
				int nextIndex = switch (event.getDropLocation()) {
					case ABOVE -> dragIndex < dropIndex ? dropIndex - 1 : dropIndex;
					case BELOW -> dragIndex < dropIndex ? dropIndex : dropIndex + 1;
					default -> throw new NotImplementedException();
				};
				//
				if (nextIndex != dragIndex) {
					parameters.onValueChange().accept(items
							.removeAt(dragIndex)
							.insert(nextIndex, items.get(dragIndex)));
				} else {
					System.out.println();
				}
			});
		}
		{
			grid.setItems(parameters.value());
		}
		return grid;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<@PolyNull E> {

		@Nullable
		ValueBoxContext context;

		@NonNull
		Supplier<MutableValueBox<E>> elementBoxSupplier;

		@NonNull
		Runnable onAddElementClick;

		@NonNull
		Consumer<List<ListItem<E>>> onValueChange;

		@NonNull
		List<ListItem<E>> value;

		public static class Builder<E> {
			// Lomboked
		}
	}
}
