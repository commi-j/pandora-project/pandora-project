package tk.labyrinth.pandora.ui.component.genericobject;

import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.ui.component.editor.EditableValue;

import java.util.Objects;

@Value(staticConstructor = "of")
@With
public class EditableGenericObjectAttribute {

	@NonNull
	EditableValue<String> name;

	@NonNull
	EditableValue<ValueWrapper> value;

	public GenericObjectAttribute toGenericObjectAttribute() {
		Objects.requireNonNull(name.getValue(), "name.value");
		Objects.requireNonNull(value.getValue(), "value.value");
		//
		return GenericObjectAttribute.of(name.getValue(), value.getValue());
	}

	public static EditableGenericObjectAttribute empty() {
		return of(EditableValue.empty(), EditableValue.empty());
	}

	public static EditableGenericObjectAttribute from(GenericObjectAttribute genericObjectAttribute) {
		return of(
				EditableValue.ofValue(genericObjectAttribute.getName()),
				EditableValue.ofValue(genericObjectAttribute.getValue()));
	}
}
