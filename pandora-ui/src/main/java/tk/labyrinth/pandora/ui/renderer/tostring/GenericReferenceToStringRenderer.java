package tk.labyrinth.pandora.ui.renderer.tostring;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;

@LazyComponent
@RequiredArgsConstructor
public class GenericReferenceToStringRenderer extends ToStringRendererBase<GenericReference<?>> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericObjectSearcher genericObjectSearcher;

	@Override
	protected String renderNonNull(Context context, GenericReference<?> value) {
		Datatype targetDatatype = value.getModelReference().toDatatype();
		GenericObject object = genericObjectSearcher.findSingle(GenericObjectReference.from(value));
		//
		return object != null
				? context.getRendererRegistry().render(
				ToStringRendererRegistry.Context.builder()
						.datatype(targetDatatype)
						.hints(context.getHints())
						.build(),
				object)
				: value.toString();
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return context.getDatatype() != null
				? datatypeBaseRegistry.isReference(context.getDatatype())
				? ToStringRenderer.MAX_DISTANCE_MINUS_ONE
				: null
				: null;
	}
}
