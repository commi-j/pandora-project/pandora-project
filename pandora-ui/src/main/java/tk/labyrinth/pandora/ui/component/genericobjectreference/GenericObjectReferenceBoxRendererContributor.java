package tk.labyrinth.pandora.ui.component.genericobjectreference;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.extra.referencemodel.ReferenceModelRegistry;
import tk.labyrinth.pandora.views.box.BoxRenderer;
import tk.labyrinth.pandora.views.box.BoxRendererContributor;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;

import java.lang.reflect.Type;

@Bean
@RequiredArgsConstructor
public class GenericObjectReferenceBoxRendererContributor implements BoxRendererContributor<Reference<?>> {

	private final ConverterRegistry converterRegistry;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericObjectReferenceBoxRenderer genericObjectReferenceBoxRenderer;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ReferenceModelRegistry referenceModelRegistry;

	@Override
	public BoxRenderer<Reference<?>> contributeBoxRenderer(
			BoxRendererRegistry registry,
			BoxRendererRegistry.Context context) {
		Type referenceJavaType = javaBaseTypeRegistry.getJavaType(context.getDatatype());
		//
		return BoxRenderer.of(
				referenceJavaType,
				parameters -> genericObjectReferenceBoxRenderer.render(builder -> builder
						.currentValue(converterRegistry.convert(parameters.currentValue(), GenericObjectReference.class))
						.initialValue(converterRegistry.convert(parameters.initialValue(), GenericObjectReference.class))
						.label(parameters.label())
						.nonEditableReasons(parameters.nonEditableReasons())
						.onValueChange(nextValue -> parameters.onValueChange().accept(
								converterRegistry.convertInferred(nextValue, referenceJavaType)))
						.referenceModel(referenceModelRegistry.getReferenceModel(context.getDatatype()))
						.build()));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(BoxRendererRegistry.Context target) {
		return datatypeBaseRegistry.isReference(target.getDatatype())
				? 900
				: null;
	}
}
