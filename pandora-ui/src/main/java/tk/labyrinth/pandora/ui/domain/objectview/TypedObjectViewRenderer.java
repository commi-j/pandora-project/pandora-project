package tk.labyrinth.pandora.ui.domain.objectview;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.core.meta.PandoraJavaContext;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

import java.util.function.Consumer;
import java.util.function.Function;

@PandoraExtensionPoint
@PandoraJavaContext
public interface TypedObjectViewRenderer<T> extends TypeAware<T> {

	default Component render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	Component render(Parameters<T> parameters);

	static <T> TypedObjectViewRenderer<T> wrap(Function<Parameters<T>, Component> renderFunction) {
		return renderFunction::apply;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	class Parameters<T> {

		@NonNull
		T currentValue;

		@NonNull
		T initialValue;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<T> onValueChange;

		public static class Builder<T> {
			// Lomboked
		}
	}
}
