package tk.labyrinth.pandora.ui.context;

import com.vaadin.flow.component.UI;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.PlainClassBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Type;
import java.util.Objects;

@LazyComponent
public class VaadinUiBeanContributor implements PlainClassBeanContributor<UI> {

	@Override
	public @Nullable UI doContributeBean(BeanFactory beanFactory, Type type) {
		return Objects.requireNonNull(UI.getCurrent());
	}

	@Nullable
	@Override
	public Integer doGetSupportDistance(BeanFactory beanFactory, Type type) {
		return UI.getCurrent() != null ? Integer.MAX_VALUE : null;
	}
}
