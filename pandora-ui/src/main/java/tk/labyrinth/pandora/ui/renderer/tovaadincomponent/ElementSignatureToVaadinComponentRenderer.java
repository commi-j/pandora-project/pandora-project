package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.ElementSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

import javax.annotation.Nullable;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class ElementSignatureToVaadinComponentRenderer extends ToVaadinComponentRendererBase<ElementSignature> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, ElementSignature value) {
		Component result;
		{
			if (Objects.equals(value.getType(), "annotation")) {
				CssHorizontalLayout layout = new CssHorizontalLayout();
				{
					layout.add(render(context, value.getParent()));
					layout.add("@");
					layout.add(context.getRendererRegistry().render(
							ToVaadinComponentRendererRegistry.Context.builder()
									.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(ClassSignature.class))
									.hints(List.empty())
									.build(),
							ClassSignature.from(value.getContent())));
				}
				result = layout;
			} else if (Objects.equals(value.getType(), "constructor")) {
				CssHorizontalLayout layout = new CssHorizontalLayout();
				{
					layout.add(render(context, value.getParent()));
					layout.add("()");
				}
				result = layout;
			} else if (Objects.equals(value.getType(), "field")) {
				CssHorizontalLayout layout = new CssHorizontalLayout();
				{
					layout.add(render(context, value.getParent()));
					layout.add("#%s".formatted(value.getContent()));
				}
				result = layout;
			} else if (Objects.equals(value.getType(), "formalParameter")) {
				CssHorizontalLayout layout = new CssHorizontalLayout();
				{
					layout.add(render(context, value.getParent()));
					layout.add("#%s".formatted(value.getContent()));
				}
				result = layout;
			} else if (Objects.equals(value.getType(), "method")) {
				result = context.getRendererRegistry().render(
						ToVaadinComponentRendererRegistry.Context.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(MethodFullSignature.class))
								.hints(List.empty())
								.build(),
						MethodFullSignature.of(value.toString()));
			} else if (Objects.equals(value.getType(), "type")) {
				result = context.getRendererRegistry().render(
						ToVaadinComponentRendererRegistry.Context.builder()
								.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(ClassSignature.class))
								.hints(List.empty())
								.build(),
						ClassSignature.from(value.toString()));
			} else if (Objects.equals(value.getType(), "package")) {
				throw new UnsupportedOperationException();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), ElementSignature.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_TWO
				: null;
	}
}
