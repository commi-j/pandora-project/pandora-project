package tk.labyrinth.pandora.ui.component.value.box;

import javax.annotation.Nullable;
import java.util.function.Consumer;

@Deprecated
public abstract class ChangeListeningValueBoxBase<T> extends ValueBoxBase<T> {

	@Nullable
	private Consumer<T> changeListener = null;

	/**
	 * About action name:
	 * https://developer.mozilla.org/en-US/docs/Web/Events/Creating_and_triggering_events
	 * https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/event
	 *
	 * @param next nullable
	 */
	protected void dispatchChangeEvent(@Nullable T next) {
		if (changeListener != null) {
			changeListener.accept(next);
		}
	}

	/**
	 * <b>changeListener</b> is already handled.
	 *
	 * @param properties non-null
	 */
	protected abstract void doRender(Properties<T> properties);

	@Override
	protected void render(Properties<T> properties) {
		changeListener = null;
		//
		doRender(properties);
		//
		changeListener = properties.getChangeListener();
	}
}
