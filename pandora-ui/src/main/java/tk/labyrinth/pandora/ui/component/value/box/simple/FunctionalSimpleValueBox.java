package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;

import java.lang.reflect.Type;

@RequiredArgsConstructor
public class FunctionalSimpleValueBox<T> implements SimpleValueBox<T> {

	private final FunctionalComponent<Properties<T>> functionalComponent;

	private final Class<T> javaClass;

	@Override
	public Component asVaadinComponent() {
		return functionalComponent;
	}

	@Override
	public Type getParameterType() {
		return javaClass;
	}

	@Override
	public void render(Properties<T> properties) {
		functionalComponent.setProperties(properties);
	}
}
