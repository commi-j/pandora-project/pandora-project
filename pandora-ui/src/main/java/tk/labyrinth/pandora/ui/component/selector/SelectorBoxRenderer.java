package tk.labyrinth.pandora.ui.component.selector;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.TextFieldRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.extra.predicate.PredicateUtils;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class SelectorBoxRenderer {

	private final SelectorViewRenderer selectorViewRenderer;

	public TextField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public TextField render(Parameters parameters) {
		return TextFieldRenderer.render(builder -> builder
				.configurer(parameters.configurer())
				.label(parameters.label())
				.readOnly(true)
				.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
						.icon(VaadinIcon.EDIT.create())
						.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
						.onClick(event -> {
							Observable<Predicate> stateObservable = Observable.withInitialValue(parameters.value());
							//
							ConfirmationViews
									.showFunctionDialog(
											stateObservable,
											nextState -> {
												TreeGrid<?> component = selectorViewRenderer.render(
														SelectorViewRenderer.Parameters.builder()
																.currentValue(nextState)
																.onValueChange(stateObservable::set)
																.build());
												//
												component.setWidth("50em");
												//
												return component;
											})
									.subscribeAlwaysAccepted(success -> {
										if (parameters.onValueChange() != null) {
											parameters.onValueChange().accept(stateObservable.get());
										}
									});
						})
						.build()))
				.value(PredicateUtils.renderWithInfixJunctions(parameters.value(), false))
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<TextField> configurer;

		@Nullable
		String label;

		@Nullable
		Consumer<Predicate> onValueChange;

		@NonNull
		Predicate value;

		public static class Builder {
			// Lomboked
		}
	}
}
