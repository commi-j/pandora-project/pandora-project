package tk.labyrinth.pandora.ui.component.valuebox.suggest;

import io.vavr.collection.List;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;

import java.lang.reflect.Type;

public interface SuggestProvider<T> {

	default Type getParentType() {
		return ParameterUtils.getActualParameter(getClass(), SuggestProvider.class, 1);
	}

	List<T> provideSuggests(SuggestContext context);
}
