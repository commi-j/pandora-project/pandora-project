package tk.labyrinth.pandora.ui.component.view.confirmation;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.function.Supplier;

@Deprecated
@Builder(toBuilder = true)
@Value
@With
public class ConfirmationViewProperties {

	/**
	 * Invoked when <b>apply</b> or <b>confirm</b> is triggered.
	 * If <b>true</b> is returned and triggered action is <b>confirm</b>, it proceeds with invoking {@link #closeCallback}.
	 */
	Supplier<Boolean> applyCallback;

	/**
	 * Invoked when <b>confirm</b> is triggered and {@link #applyCallback} returns <b>true</b>
	 * or when <b>close</b> is triggered.
	 */
	Runnable closeCallback;

	Boolean showApplyButton;
}
