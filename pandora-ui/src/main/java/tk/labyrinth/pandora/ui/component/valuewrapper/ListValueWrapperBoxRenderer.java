package tk.labyrinth.pandora.ui.component.valuewrapper;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.list.VavrListBox;

import java.util.function.Consumer;

public class ListValueWrapperBoxRenderer {

	public static Component render(Properties properties) {
		VavrListBox<ValueWrapper> listBox = new VavrListBox<>();
		//
		listBox.render(VavrListBox.Properties.<ValueWrapper>builder()
				.currentValue(properties.getValue() != null
						? properties.getValue().getValue()
						: null)
				.elementBoxSupplier(() -> new FunctionalMutableValueBox<>(
						elementBoxProperties -> ValueWrapperBoxRenderer.render(
								ValueWrapperBoxRenderer.Parameters.builder()
										.nonEditableReasons(elementBoxProperties.getNonEditableReasons())
										.onValueChange(elementBoxProperties.getOnValueChange())
										.value(elementBoxProperties.getCurrentValue())
										.build()),
						ValueWrapper.class
				))
				.initialValue(properties.getValue() != null
						? properties.getValue().getValue()
						: null)
				.label(properties.getLabel())
				.nonEditableReasons(properties.getNonEditableReasons())
				.onValueChange(properties.getOnValueChange() != null
						? nextValue -> properties.getOnValueChange().accept(nextValue != null
						? ListValueWrapper.of(nextValue)
						: null)
						: nextValue -> {
					// no-op
				})
				.build());
		//
		return listBox.asVaadinComponent();
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<ListValueWrapper> onValueChange;

		@Nullable
		ListValueWrapper value;
	}
}
