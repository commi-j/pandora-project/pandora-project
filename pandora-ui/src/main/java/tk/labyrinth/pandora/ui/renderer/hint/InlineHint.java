package tk.labyrinth.pandora.ui.renderer.hint;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class InlineHint {

	public static final InlineHint INSTANCE = new InlineHint();
}
