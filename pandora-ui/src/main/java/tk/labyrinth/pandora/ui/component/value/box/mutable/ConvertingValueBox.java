package tk.labyrinth.pandora.ui.component.value.box.mutable;

import com.vaadin.flow.component.Component;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import java.lang.reflect.Type;
import java.util.function.Function;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ConvertingValueBox<E, I> implements MutableValueBox<E> {

	private final Function<MutableValueBox.Properties<E>, I> propertiesFunction;

	private final Type type;

	private final RenderableView<I> view;

	@Override
	public Component asVaadinComponent() {
		return view.asVaadinComponent();
	}

	@Override
	public Type getParameterType() {
		return type;
	}

	@Override
	public void render(Properties<E> properties) {
		view.render(propertiesFunction.apply(properties));
	}

	public static <E, I> ConvertingValueBox<E, I> of(
			RenderableView<I> view,
			Type type,
			Function<MutableValueBox.Properties<E>, I> propertiesFunction) {
		return new ConvertingValueBox<>(propertiesFunction, type, view);
	}
}
