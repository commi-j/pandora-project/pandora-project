package tk.labyrinth.pandora.ui.domain.pageroute;

import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.Location;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.tool.setup.RegisterOnUiInit;
import tk.labyrinth.pandora.ui.domain.page.PandoraPage;
import tk.labyrinth.pandora.ui.domain.route.PandoraRouteParameterMapping;
import tk.labyrinth.pandora.ui.routing.route.RouteHandler;
import tk.labyrinth.pandora.ui.routing.route.RouteModel;
import tk.labyrinth.pandora.ui.routing.routetarget.ParametersAware;
import tk.labyrinth.pandora.ui.routing.routetarget.SetParameterHookHandler;

import java.lang.reflect.Type;
import java.util.Objects;

@Deprecated
@LazyComponent
@RegisterOnUiInit
@RequiredArgsConstructor
public class PandoraRoutingBeforeEnterListener implements BeforeEnterListener {

	private final ConverterRegistry converterRegistry;

	private final PageRouteRegistry pageRouteRegistry;

	@SmartAutowired
	private List<RouteHandler> routeHandlers;

	private GenericObjectAttribute resolveRouteParameter(
			String routePath,
			Location currentLocation,
			PandoraRouteParameterMapping parameterMapping,
			ObjectModelAttribute parameterModel) {
		ValueWrapper value;
		{
			String fromSchema = parameterMapping.getFromSchema();
			//
			if (fromSchema.startsWith("CONSTANT:")) {
				value = SimpleValueWrapper.of(fromSchema.substring("CONSTANT:".length()));
			} else if (fromSchema.startsWith("PATH:")) {
				String parameterDeclarationString = "{%s}".formatted(fromSchema.substring("PATH:".length()));
				//
				value = List.of(routePath.split("/"))
						.zipWithIndex()
						.filter(tuple -> Objects.equals(tuple._1(), parameterDeclarationString))
						.map(tuple -> currentLocation.getSegments().get(tuple._2()))
						.map(SimpleValueWrapper::of)
						.getOrNull();
			} else {
				throw new NotImplementedException();
			}
		}
		return GenericObjectAttribute.of(parameterModel.getName(), value);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void beforeEnter(BeforeEnterEvent event) {
		PageRoute pageRoute = pageRouteRegistry.findMatch(event.getLocation().getPath());
		//
		if (pageRoute != null) {
			PandoraPage page = pageRouteRegistry.getPage(pageRoute);
			//
			if (ParametersAware.class.isAssignableFrom(event.getNavigationTarget())) {
				GenericObject genericParameters = GenericObject.of(pageRoute.getParameterMappingsOrEmpty()
						.map(parameterMapping -> resolveRouteParameter(
								pageRoute.getPath(),
								event.getLocation(),
								parameterMapping,
								page.getParametersModel()
										.find(attribute -> Objects.equals(attribute.getName(), parameterMapping.getTo()))
										.get())));
				//
				SetParameterHookHandler.setComponentConsumer(component -> {
					ParametersAware<Object> parametersAware = (ParametersAware<Object>) component;
					//
					Type parametersType = ParameterUtils.getFirstActualParameter(
							event.getNavigationTarget(),
							ParametersAware.class);
					Object targetParameters = createTargetParameters(
							parametersType,
							parametersAware.getInitialParameters(),
							genericParameters);
					//
					parametersAware.acceptParameters(targetParameters);
				});
			}
		} else {
			// FIXME: Old branch, to delete.
			//
			List<RouteModel> routeModels = routeHandlers
					.map(routeHandler -> routeHandler.findModel(event))
					.filter(Objects::nonNull);
			//
			if (!routeModels.isEmpty()) {
				if (routeModels.size() > 1) {
					throw new IllegalArgumentException(routeModels.toString());
				}
				//
				if (ParametersAware.class.isAssignableFrom(event.getNavigationTarget())) {
					SetParameterHookHandler.setComponentConsumer(component -> {
						ParametersAware<Object> parametersAware = (ParametersAware<Object>) component;
						//
						Type parametersType = ParameterUtils.getFirstActualParameter(
								event.getNavigationTarget(),
								ParametersAware.class);
						Object targetParameters = createTargetParameters(
								parametersType,
								parametersAware.getInitialParameters(),
								routeModels.single().getParameters());
						//
						parametersAware.acceptParameters(targetParameters);
					});
				}
			}
		}
	}

	public Object createTargetParameters(Type parametersType, Object initialParameters, GenericObject routeParameters) {
		// TODO: Merge with initial.
//		GenericObject genericInitialParameters = converterRegistry.convert(initialParameters, GenericObject.class);
		//
		// TODO: Adapt routeParameters to match target.
		return converterRegistry.convert(routeParameters, parametersType);
	}
}
