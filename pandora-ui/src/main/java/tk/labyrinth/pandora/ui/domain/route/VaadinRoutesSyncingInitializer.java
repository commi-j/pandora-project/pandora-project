package tk.labyrinth.pandora.ui.domain.route;

import com.vaadin.flow.router.RouteData;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.tool.init.ActiveGenericObjectContributor;
import tk.labyrinth.pandora.ui.domain.page.PageRenderer;
import tk.labyrinth.pandora.ui.domain.page.PandoraPage;
import tk.labyrinth.pandora.ui.domain.page.SlugPandoraPageReference;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;

import java.util.Objects;

@Bean
@RequiredArgsConstructor
public class VaadinRoutesSyncingInitializer extends ActiveGenericObjectContributor implements
		VaadinServiceInitListener {

	@Override
	public void serviceInit(ServiceInitEvent event) {
		val applicationRouteRegistry = ApplicationRouteRegistry.getInstance(event.getSource().getContext());
		//
		List<RouteData> routeDatas = List.ofAll(applicationRouteRegistry.getRegisteredRoutes());
		//
		contributeObjects(routeDatas
				.filter(routeData -> !Objects.equals(routeData.getNavigationTarget(), PageRenderer.class))
				.flatMap(routeData -> {
					String pageSlug = "vaadinRoute:%s".formatted(ClassSignature.of(routeData.getNavigationTarget()));
					//
					return List.of(
							PandoraPage.builder()
									.handle("vaadinRoute:TODO")
									.slug(pageSlug)
									.build(),
							PageRoute.builder()
									.pageReference(SlugPandoraPageReference.of(pageSlug))
									.path(!routeData.getTemplate().isEmpty() ? routeData.getTemplate() : "/")
									.build());
				})
				.map(this::convertAndAddHardcoded));
	}
}
