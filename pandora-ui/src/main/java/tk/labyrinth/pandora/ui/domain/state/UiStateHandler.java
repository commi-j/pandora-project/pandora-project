package tk.labyrinth.pandora.ui.domain.state;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationListener;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.QueryParameters;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.UiScopedComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@UiScopedComponent
public class UiStateHandler implements AfterNavigationListener {

	private final AtomicBoolean afterNavigationInProgress = new AtomicBoolean(false);

	private final CopyOnWriteArrayList<UiStateAttributeHandler> attributeHandlers = new CopyOnWriteArrayList<>();

	private final Observable<Pair<Location, GenericObject>> locationAndStateObservable = Observable.notInitialized();

	@SmartAutowired
	private UI ui;

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialize: ui.id = %s".formatted(ui.getUIId()));
		//
		locationAndStateObservable.subscribe(nextLocationAndState -> {
			if (nextLocationAndState.getLeft() != null) {
				Location locationToSet = stateToLocation(
						List.ofAll(attributeHandlers),
						nextLocationAndState.getRight(),
						nextLocationAndState.getLeft());
				//
				logger.trace("locationAndStateObservable.subscribe: nextState = {}, locationToSet = {}",
						nextLocationAndState.getRight(),
						locationToSet.getPathWithQueryParameters());
				//
				UI.getCurrent().getPage().getHistory().replaceState(null, locationToSet);
			} else {
				logger.trace("locationAndStateObservable.subscribe: nextLocation = {}, nextState = {}",
						nextLocationAndState.getLeft(),
						nextLocationAndState.getRight());
			}
		});
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		GenericObject rawQueryObject = queryParametersToGenericObject(
				List.ofAll(attributeHandlers),
				event.getLocation().getQueryParameters());
		//
		GenericObject refinedQueryObject = rawQueryObject.withAddedAttributes(List.ofAll(attributeHandlers)
				.filter(attributeHandler -> !rawQueryObject.hasAttribute(attributeHandler.getAttributeName()))
				.filter(attributeHandler -> attributeHandler.getDefaultValue() != null)
				.map(attributeHandler -> GenericObjectAttribute.of(
						attributeHandler.getAttributeName(),
						attributeHandler.getDefaultValue())));
		//
		Pair<Location, GenericObject> locationAndStateToSet = Pair.of(event.getLocation(), refinedQueryObject);
		//
		logger.trace("afterNavigation: locationToSet = {}, stateToSet = {}",
				locationAndStateToSet.getLeft().getPathWithQueryParameters(),
				locationAndStateToSet.getRight());
		//
		afterNavigationInProgress.set(true);
		try {
			locationAndStateObservable.set(locationAndStateToSet);
		} finally {
			afterNavigationInProgress.set(false);
		}
	}

	public GenericObject getState() {
		return locationAndStateObservable.get().getRight();
	}

	public Flux<GenericObject> getStateFlux() {
		return locationAndStateObservable.getFlux().map(Pair::getRight);
	}

	public void registerAttributeHandler(UiStateScope scope, UiStateAttributeHandler attributeHandler) {
		logger.debug("registerAttributeHandler: {}", attributeHandler);
		//
		if (List.ofAll(attributeHandlers).exists(attributeHandlerElement -> Objects.equals(
				attributeHandlerElement.getAttributeName(),
				attributeHandler.getAttributeName()))) {
			throw new IllegalArgumentException("Already registered: %s".formatted(attributeHandler.getAttributeName()));
		}
		attributeHandlers.add(attributeHandler);
		//
		setAttribute(attributeHandler.getAttributeName(), attributeHandler.getDefaultValue());
	}

	public void setAttribute(String attributeName, @Nullable ValueWrapper attributeValue) {
		setAttributes(List.of(Pair.of(attributeName, attributeValue)));
	}

	public void setAttributes(List<Pair<String, @Nullable ValueWrapper>> attributes) {
		locationAndStateObservable.update(currentLocationAndState -> {
			Location currentLocation = currentLocationAndState != null ? currentLocationAndState.getLeft() : null;
			GenericObject currentState = currentLocationAndState != null
					? currentLocationAndState.getRight()
					: GenericObject.empty();
			GenericObject stateToSet = currentState.withAttributes(attributes);
			//
			logger.trace("setAttributes: stateToSet = {}, anav = {}", stateToSet, afterNavigationInProgress.get());
			//
			return Pair.of(currentLocation, stateToSet);
		});
	}

	public void unregisterAttributeHandler(UiStateAttributeHandler attributeHandler) {
		logger.debug("unregisterAttributeHandler: {}", attributeHandler);
		//
		attributeHandlers.remove(attributeHandler);
	}

	public static GenericObject queryParametersToGenericObject(
			List<UiStateAttributeHandler> attributeHandlers,
			QueryParameters queryParameters) {
		return GenericObject.of(LinkedHashMap.ofAll(queryParameters.getParameters())
				.filterKeys(key -> !key.isEmpty())
				.map(tuple -> {
					GenericObjectAttribute attribute;
					{
						UiStateAttributeHandler attributeHandler = attributeHandlers
								.find(attributeHandlerElement -> Objects.equals(
										attributeHandlerElement.getQueryAttributeName(),
										tuple._1()))
								.getOrNull();
						//
						if (attributeHandler != null) {
							ValueWrapper attributeValue = attributeHandler.fromQueryValue(List.ofAll(tuple._2()).single());
							//
							attribute = GenericObjectAttribute.of(
									attributeHandler.getAttributeName(),
									attributeValue != null ? attributeValue : attributeHandler.getDefaultValue());
						} else {
							attribute = GenericObjectAttribute.ofSimple(tuple._1(), List.ofAll(tuple._2()).single());
						}
					}
					return attribute;
				})
				.toList());
	}

	public static Location stateToLocation(
			List<UiStateAttributeHandler> attributeHandlers,
			GenericObject state,
			Location currentLocation) {
		return new Location(currentLocation.getSegments(), QueryParameters.simple(
				state.getAttributes()
						.map(attribute -> {
							Pair<String, String> queryAttribute;
							{
								UiStateAttributeHandler attributeHandler = attributeHandlers
										.find(attributeHandlerElement -> Objects.equals(
												attributeHandlerElement.getAttributeName(),
												attribute.getName()))
										.getOrNull();
								//
								if (attributeHandler != null) {
									queryAttribute = attributeHandler.getKind() == UiStateAttributeKind.QUERY
											? Pair.of(
											attributeHandler.getQueryAttributeName(),
											attributeHandler.toQueryValue(attribute.getValue()))
											: null;
								} else {
									queryAttribute = Pair.of(
											attribute.getName(),
											attribute.getValue().unwrap().toString());
								}
							}
							return queryAttribute;
						})
						.filter(Objects::nonNull)
						.toMap(Pair::getKey, Pair::getValue)
						.filter(tuple -> tuple._2() != null)
						.toJavaMap()));
	}
}
