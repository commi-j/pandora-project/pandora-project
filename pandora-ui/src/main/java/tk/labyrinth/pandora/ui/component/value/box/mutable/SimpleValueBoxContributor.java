package tk.labyrinth.pandora.ui.component.value.box.mutable;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.component.value.box.simple.FunctionalSimpleValueBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

@LazyComponent
@RequiredArgsConstructor
public class SimpleValueBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		return new SimpleMutableValueBox<>(new FunctionalSimpleValueBox<>(
				FunctionalComponent.create(
						SimpleValueBox.Properties.<String>builder().build(),
						(parameters, sink) -> StringBoxRenderer.render(builder -> builder
								.label(parameters.getLabel())
								.nonEditableReasons(parameters.getNonEditableReasons())
								.onValueChange(parameters.getOnValueChange())
								.value(parameters.getValue())
								.build())),
				String.class));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.determineKind(context.getDatatype()) == DatatypeKind.SIMPLE
				? ToStringRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}
}
