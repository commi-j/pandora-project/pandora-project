package tk.labyrinth.pandora.ui.tool.otel;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import io.opentelemetry.api.trace.SpanId;
import io.opentelemetry.sdk.internal.JavaVersionSpecific;
import io.opentelemetry.sdk.trace.data.SpanData;
import io.vavr.Lazy;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.functionalcomponents.vaadin.IntegerFieldRenderer;
import tk.labyrinth.pandora.functionalcomponents.vaadin.TextFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.CheckboxRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanCache;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttribute;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateMirror;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.time.Instant;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@AnonymousAllowed
@RequiredArgsConstructor
@Route("tools/opentelemetry-spans")
@Slf4j
public class OpentelemetrySpansPage extends CssVerticalLayout {

	private final TreeGrid<SpanData> grid = new TreeGrid<>();

	private final SpanCache spanCache;

	@UiStateMirror
	private final Observable<State> stateObservable = Observable.withInitialValue(State.builder()
			.durationThreshold(null)
			.expandedSpanIds(List.empty())
			.findPattern(null)
			.hideEnded(false)
			.useRenderers(false)
			.build());

	@SmartAutowired
	private Lazy<JavaBaseTypeRegistry> javaBaseTypeRegistryLazy;

	// FIXME: Replace with properties attribute.
	private Long queriedAtNanos = null;

	@SmartAutowired
	private Lazy<ToVaadinComponentRendererRegistry> toVaadinComponentRendererRegistryLazy;

	@PostConstruct
	private void postConstruct() {
		{
			setHeightFull();
		}
		{
			{
				add(FunctionalComponent.createWithExternalObservable(
						stateObservable,
						(properties, sink) -> {
							CssHorizontalLayout header = new CssHorizontalLayout();
							{
								header.addClassNames(PandoraStyles.LAYOUT);
								//
								header.setAlignItems(AlignItems.BASELINE);
							}
							{
								header.add(TextFieldRenderer.render(builder -> builder
										.label("Find")
										.onValueChange(next -> sink.accept(properties.withFindPattern(next)))
										.value(properties.getFindPattern())
										.build()));
								//
								header.add(IntegerFieldRenderer.render(builder -> builder
										.label("Duration Threshold")
										.onValueChange(next -> sink.accept(properties.withDurationThreshold(next)))
										.value(properties.getDurationThreshold())
										.build()));
								//
								header.add(CheckboxRenderer.render(builder -> builder
										.label("Hide Ended")
										.onValueChange(next -> sink.accept(properties.withHideEnded(next)))
										.value(properties.getHideEnded())
										.build()));
								//
								header.add(ButtonRenderer.render(builder -> builder
										.icon(VaadinIcon.REFRESH.create())
										.onClick(event -> refresh(properties))
										.build()));
							}
							return header;
						}));
			}
			{
				grid.setHeightFull();
				{
					grid
							.addColumn(SpanData::getSpanId)
							.setFlexGrow(1)
							.setHeader("Span Id")
							.setResizable(true);
					grid
							.addComponentHierarchyColumn(item -> getNameComponent(
									item.getName(),
									Optional.ofNullable(stateObservable.get().getUseRenderers()).orElse(false)))
							.setFlexGrow(4)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addComponentColumn(item -> {
								CssVerticalLayout layout = new CssVerticalLayout();
								{
									item.getAttributes().asMap().entrySet()
											.stream()
											.sorted(Comparator.comparing(entry -> entry.getKey().getKey()))
											.forEach(entry -> layout.add(new Span(
													"%s : %s".formatted(entry.getKey().getKey(), entry.getValue()))));
								}
								return layout;
							})
							.setFlexGrow(2)
							.setHeader("Attributes")
							.setResizable(true);
					grid
							.addColumn(item -> "%sms".formatted(
									computeSpanDurationMs(item, queriedAtNanos)))
							.setFlexGrow(1)
							.setHeader("Duration")
							.setResizable(true);
					grid
							.addColumn(item -> Instant.ofEpochMilli(
									TimeUnit.NANOSECONDS.toMillis(item.getStartEpochNanos())))
							.setFlexGrow(2)
							.setHeader("Start")
							.setResizable(true);
					grid
							.addColumn(item -> item.hasEnded()
									? Instant.ofEpochMilli(TimeUnit.NANOSECONDS.toMillis(item.getEndEpochNanos()))
									: "-")
							.setFlexGrow(2)
							.setHeader("End")
							.setResizable(true);
				}
				add(grid);
			}
		}
		{
			stateObservable.subscribe(this::refresh);
		}
	}

	private void refresh(State state) {
		Integer durationThreshold = state.getDurationThreshold();
		String findPattern = state.getFindPattern();
		boolean hideEnded = state.getHideEnded();
		//
		List<SpanData> spanDatas = spanCache.getSpanDatas();
		//
		queriedAtNanos = JavaVersionSpecific.get().currentTimeNanos();
		//
		Map<String, SpanData> spanIdToSpanDataMap = spanDatas.toMap(SpanData::getSpanId, Function.identity());
		Map<String, List<SpanData>> parentSpanIdToSpanDatasMap = spanDatas
				.filter(spanData -> !hideEnded || !spanData.hasEnded())
				.filter(spanData -> durationThreshold == null ||
						computeSpanDurationMs(spanData, queriedAtNanos) >= durationThreshold)
				.groupBy(SpanData::getParentSpanId);
		//
		List<SpanData> highlightedSpanDatas = findPattern != null
				? spanDatas
				.filter(spanData -> spanData.getName().toLowerCase().contains(findPattern.toLowerCase()))
				.filter(spanData -> durationThreshold == null ||
						computeSpanDurationMs(spanData, queriedAtNanos) >= durationThreshold)
				: List.empty();
		//
		List<SpanData> spanDatasToExpand = highlightedSpanDatas
				.flatMap(spanData -> getChain(spanIdToSpanDataMap, spanData))
				.distinct();
		//
		grid.setItems(
				parentSpanIdToSpanDatasMap.get(SpanId.getInvalid()).getOrElse(List.empty())
						.filter(spanData -> findPattern == null || spanDatasToExpand.contains(spanData))
						.sorted(Comparator.comparing(SpanData::getStartEpochNanos).reversed())
						.asJava(),
				item -> parentSpanIdToSpanDatasMap.get(item.getSpanId()).getOrElse(List.empty())
						.filter(spanData -> findPattern == null || spanDatasToExpand.contains(spanData))
						.sorted(Comparator.comparing(SpanData::getStartEpochNanos).reversed())
						.asJava());
		//
		if (hideEnded) {
			grid.expand(parentSpanIdToSpanDatasMap.values().flatMap(Function.identity()).asJava());
		} else {
			grid.expand(spanDatasToExpand.asJava());
		}
	}

	public Component getNameComponent(String spanName, boolean useRenderers) {
		Component result;
		{
			if (useRenderers) {
				int indexOfHash = spanName.indexOf('#');
				if (indexOfHash != -1) {
					CssHorizontalLayout layout = new CssHorizontalLayout();
					{
						layout.add(toVaadinComponentRendererRegistryLazy.get().render(
								ToVaadinComponentRendererRegistry.Context.builder()
										.datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(TypeDescription.class))
										.hints(List.empty())
										.build(),
								TypeDescription.of(TypeSignature
										.guessFromQualifiedName(spanName.substring(0, indexOfHash))
										.toString())));
						//
						layout.add(spanName.substring(indexOfHash));
					}
					result = layout;
				} else {
					result = new Span(spanName);
				}
			} else {
				result = new Span(spanName);
			}
		}
		return result;
	}

	private static List<SpanData> getChain(Map<String, SpanData> spanIdToSpanDataMap, SpanData spanData) {
		return !Objects.equals(spanData.getParentSpanId(), SpanId.getInvalid())
				? getChain(spanIdToSpanDataMap, spanIdToSpanDataMap.get(spanData.getParentSpanId()).get())
				.append(spanData)
				: List.of(spanData);
	}

	public static long computeSpanDurationMs(SpanData spanData, long nowNanos) {
		long start = spanData.getStartEpochNanos();
		long end = spanData.hasEnded()
				? spanData.getEndEpochNanos()
				//
				// This is the method used by AnchoredClock#now() to compute end time.
				// It does not match System#nanoTime().
				: nowNanos;
		//
		return TimeUnit.NANOSECONDS.toMillis(end - start);
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@Nullable
		@UiStateAttribute
		Integer durationThreshold;

		// TODO: Make use.
		List<String> expandedSpanIds;

		@Nullable
		@UiStateAttribute
		String findPattern;

		@NonNull
		@UiStateAttribute
		Boolean hideEnded;

		@Nullable
		Boolean useRenderers;
	}
}
