package tk.labyrinth.pandora.ui.component.table.search;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.ui.component.table.TimeWindow;

@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class SearchObject {

	@Nullable
	SearchParameters searchParameters;

	@Nullable
	SearchState searchState;

	@Nullable
	public TimeWindow computeTimeWindow() {
		return searchState != null
				? TimeWindow.of(
				searchState.result() != null ? searchState.result().finishedAt() : null,
				searchState.startedAt())
				: null;
	}
}
