package tk.labyrinth.pandora.ui.tool.otel;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.opentelemetry.sdk.internal.JavaVersionSpecific;
import io.opentelemetry.sdk.trace.data.SpanData;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.advanced.TreeGridWrapper;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanDataWrapper;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.time.Instant;
import java.util.Comparator;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class OpentelemetrySpanTable extends CssVerticalLayout {

	private static final JavaVersionSpecific javaVersionSpecific = JavaVersionSpecific.get();

	private final TreeGridWrapper<SpanDataWrapper> grid = new TreeGridWrapper<>(
			SpanDataWrapper::getChildren,
			SpanDataWrapper::getData,
			item -> item.getData().getSpanId());

	private final Observable<State> stateObservable = Observable.notInitialized();

	{
		{
			//
		}
		{
			{
				CssHorizontalLayout breadcrumbsLayout = new CssHorizontalLayout();
				{
					breadcrumbsLayout.addClassName(PandoraStyles.LAYOUT_SMALL);
				}
				{
					stateObservable.subscribe((nextState, sink) -> {
						breadcrumbsLayout.removeAll();
						//
						if (nextState.focusedSpanId() != null) {
							List<SpanDataWrapper> chain = nextState.parameters().rootItems()
									.map(item -> item.findChain(nextState.focusedSpanId()))
									.filter(Objects::nonNull)
									.get();
							//
							breadcrumbsLayout.add(ButtonRenderer.render(builder -> builder
									.onClick(event -> sink.accept(nextState.withFocusedSpanId(null)))
									.text("<root>")
									.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
									.build()));
							//
							chain.init().forEach(element -> {
								breadcrumbsLayout.add(">");
								//
								breadcrumbsLayout.add(ButtonRenderer.render(builder -> builder
										.onClick(event -> sink.accept(nextState.withFocusedSpanId(element.getData().getSpanId())))
										.text(element.getData().getName())
										.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
										.build()));
							});
						}
					});
				}
				add(breadcrumbsLayout);
			}
			{
				{
					grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
				}
				{
					grid
							.addColumn(item -> item.getData().getSpanId())
							.setFlexGrow(1)
							.setHeader("Span Id")
							.setResizable(true);
					grid
							.addHierarchyColumn(item -> item.getData().getName())
							.setFlexGrow(4)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addComponentColumn(item -> {
								CssVerticalLayout layout = new CssVerticalLayout();
								{
									item.getData().getAttributes().asMap().entrySet()
											.stream()
											.sorted(Comparator.comparing(entry -> entry.getKey().getKey()))
											.forEach(entry -> layout.add(new Span(
													"%s : %s".formatted(entry.getKey().getKey(), entry.getValue()))));
								}
								return layout;
							})
							.setFlexGrow(2)
							.setHeader("Attributes")
							.setResizable(true);
					grid
							.addColumn(item -> "%sms%s".formatted(
									computeSpanDurationMs(item.getData(), javaVersionSpecific.currentTimeNanos()),
									item.getData().hasEnded() ? "" : " (ongoing)")) // TODO: "now" should be one for render.
							.setFlexGrow(1)
							.setHeader("Duration")
							.setResizable(true);
					grid
							.addColumn(item -> Instant.ofEpochMilli(
									TimeUnit.NANOSECONDS.toMillis(item.getData().getStartEpochNanos())))
							.setFlexGrow(2)
							.setHeader("Start")
							.setResizable(true);
					grid
							.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
									.icon(VaadinIcon.EYE.create())
									.onClick(event -> stateObservable.update(state ->
											state.withFocusedSpanId(item.getData().getSpanId())))
									.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
									.build()))
							.setFlexGrow(0)
							.setHeader("Focus");
				}
				{
					stateObservable.subscribe(nextState -> {
						List<SpanDataWrapper> items = nextState.focusedSpanId() != null
								? nextState.parameters().rootItems()
								.map(item -> item.find(nextState.focusedSpanId()))
								.filter(Objects::nonNull)
								: nextState.parameters().rootItems();
						//
						{
							// FIXME: As of now we can't properly update state with root elemens shifted. Should invest into it later.
							grid.getContent().getTreeData().clear();
						}
						grid.setItems(items);
						grid.expandRootItems();
					});
				}
				add(grid);
			}
		}
	}

	private void setFocusedSpan() {
	}

	public void setParameters(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		setParameters(parametersConfigurer.apply(Parameters.builder()));
	}

	public void setParameters(Parameters parameters) {
		stateObservable.set(State.builder()
				.focusedSpanId(null)
				.parameters(parameters)
				.build());
	}

	/**
	 * @param spanData non-null
	 * @param nowNanos obtained via JavaVersionSpecific.get().currentTimeNanos(), not via System.nanoTime().
	 *
	 * @return non-negative
	 */
	public static long computeSpanDurationMs(SpanData spanData, long nowNanos) {
		long start = spanData.getStartEpochNanos();
		long end = spanData.hasEnded()
				? spanData.getEndEpochNanos()
				//
				// This is the method used by AnchoredClock#now() to compute end time.
				// It does not match System#nanoTime().
				: nowNanos;
		//
		return TimeUnit.NANOSECONDS.toMillis(end - start);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		List<SpanDataWrapper> rootItems;

		public static class Builder {
			// Lomboked
		}
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@Nullable
		String focusedSpanId;

		@NonNull
		Parameters parameters;
	}
}
