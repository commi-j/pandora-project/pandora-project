package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.functionalvaadin.component.SpanRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoBadgeVariables;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;

import javax.annotation.Nullable;

@LazyComponent
@RequiredArgsConstructor
public class TagToVaadinComponentRenderer extends ToVaadinComponentRendererBase<Tag> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final ToStringRendererRegistry toStringRendererRegistry;

	@Override
	protected Component renderNonNull(Context context, Tag value) {
		return SpanRenderer.render(builder -> builder
				.text(toStringRendererRegistry.render(
						ToStringRendererRegistry.Context.builder()
								.datatype(JavaBaseTypeUtils.createParameterlessDatatype(Tag.class))
								.hints(List.empty())
								.build(),
						value))
				.themes(List.of(LumoBadgeVariables.BADGE, LumoBadgeVariables.CONTRAST, LumoBadgeVariables.PILL))
				.build());
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), Tag.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
