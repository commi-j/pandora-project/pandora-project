package tk.labyrinth.pandora.ui.component.table.search;

import com.vaadin.flow.component.UI;
import io.opentelemetry.context.Context;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.domain.activity.PandoraSpanActivityRegistry;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSearchParameters;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSearchResult;

import java.time.Instant;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;

@Bean
@RequiredArgsConstructor
@Slf4j
public class AdvancedTableViewSearcher {

	private final PandoraSpanActivityRegistry pandoraSpanActivityRegistry;

	private AdvancedTableSearchResult performSearch(
			Function<AdvancedTableSearchParameters, AdvancedTableSearchResult> searchFunction,
			AdvancedTableSearchParameters searchObject) {
		AdvancedTableSearchResult result;
		{
			try {
				result = searchFunction.apply(searchObject);
			} catch (RuntimeException ex) {
				logger.error("", ex);
				//
				result = AdvancedTableSearchResult.builder()
						.objectsTry(Try.failure(ex))
						.build();
			}
		}
		return result;
	}

	public Observable<SearchState> initiateSearch(SearchParameters parameters) {
		Observable<SearchState> result = Observable.notInitialized();
		//
		pandoraSpanActivityRegistry.withinSpan(
				"search-invoke",
				invocationSpanContext -> {
					Instant startedAt = Instant.now();
					//
					SearchState invocationSearchState = SearchState.builder()
							.parameters(parameters)
							.spanIdPair(invocationSpanContext.getSpanIdPair())
							.startedAt(startedAt)
							.build();
					//
					UI ui = UI.getCurrent();
					//
					ForkJoinPool.commonPool().execute(Context.current().<Runnable>wrap(() -> invocationSpanContext.withinSpan(
							"search-execute",
							executionSpanContext -> {
								AdvancedTableSearchResult searchResult = executionSpanContext.withinSpan(
										"do-search",
										() -> performSearch(
												parameters.searchFunction(),
												AdvancedTableSearchParameters.builder()
														.pageIndex(parameters.pageIndex())
														.predicate(parameters.predicate())
														.build()));
								//
								executionSpanContext.withinSpan(
										"ui-access",
										() -> ui.access(() -> result.set(invocationSearchState.withResult(SearchResult.builder()
												.finishedAt(Instant.now())
												.objectsTry(searchResult.getObjectsTry())
												.pageCount(searchResult.getPageCount())
												.build()))));
							})));
					//
					result.set(invocationSearchState);
				});
		//
		return result;
	}
}
