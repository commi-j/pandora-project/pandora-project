package tk.labyrinth.pandora.ui.component.valuebox.suggest;

import io.vavr.collection.List;

public final class NoOpSuggestProvider implements SuggestProvider<Object> {

	@Override
	public List<Object> provideSuggests(SuggestContext context) {
		throw new UnsupportedOperationException();
	}
}
