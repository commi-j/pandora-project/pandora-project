package tk.labyrinth.pandora.ui.component.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.f0rce.ace.AceEditor;
import de.f0rce.ace.enums.AceMode;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
public class JsonEditorRenderer {

	/**
	 * We're using own ObjectMapper because we don't need any configuration, just parsing simple JSONs.
	 */
	private static final ObjectMapper objectMapper = new ObjectMapper();

	public static AceEditor render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static AceEditor render(Parameters parameters) {
		AceEditor aceEditor = new AceEditor();
		{
			aceEditor.setMode(AceMode.json);
		}
		{
			aceEditor.setReadOnly(BooleanUtils.isTrue(parameters.readOnly()));
			ElementUtils.setTitle(aceEditor, parameters.title());
			aceEditor.setValue(parameters.value().toPrettyString());
		}
		{
			if (parameters.onValueChange() != null) {
				aceEditor.addValueChangeListener(event -> {
					JsonNode value;
					try {
						value = objectMapper.readValue(event.getValue(), JsonNode.class);
					} catch (JsonProcessingException ex) {
						throw new RuntimeException(ex);
					}
					//
					parameters.onValueChange().accept(value);
				});
			} else {
				aceEditor.setEnabled(false);
			}
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(aceEditor));
		}
		return aceEditor;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<AceEditor> configurer;

		@Nullable
		Consumer<JsonNode> onValueChange;

		@Nullable
		Boolean readOnly;

		@Nullable
		String title;

		@NonNull
		JsonNode value;

		public static class Builder {
			// Lomboked
		}
	}
}
