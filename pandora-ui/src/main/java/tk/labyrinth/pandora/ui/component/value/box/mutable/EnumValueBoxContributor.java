package tk.labyrinth.pandora.ui.component.value.box.mutable;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.component.value.box.simple.SelectBoxRenderer;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

@LazyComponent
@RequiredArgsConstructor
public class EnumValueBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		// TODO: Replace with get
		Class<?> javaClass = javaBaseTypeRegistry.findJavaBaseTypeReference(context.getDatatype()).resolveClass();
		//
		return new FunctionalMutableValueBox<>(
				properties -> SelectBoxRenderer.render(SelectBoxRenderer.Properties.builder()
						.label(properties.getLabel())
						.items(List.of(javaClass.getEnumConstants()))
						.nonEditableReasons(properties.getNonEditableReasons())
						.nullSupported(true)
						.onValueChange(properties.getOnValueChange())
						.value(properties.getCurrentValue())
						.build()),
				javaClass);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		SignatureJavaBaseTypeReference javaBaseTypeReference = javaBaseTypeRegistry
				.findJavaBaseTypeReference(context.getDatatype());
		//
		return javaBaseTypeReference != null && javaBaseTypeReference.resolveClass().isEnum()
				? ToStringRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
