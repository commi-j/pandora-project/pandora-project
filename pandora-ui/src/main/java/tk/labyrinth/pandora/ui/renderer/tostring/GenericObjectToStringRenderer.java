package tk.labyrinth.pandora.ui.renderer.tostring;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectItem;
import tk.labyrinth.pandora.ui.renderer.custom.GenericObjectToItemRenderer;

@Bean
@RequiredArgsConstructor
public class GenericObjectToStringRenderer extends ToStringRendererBase<GenericObject> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericObjectToItemRenderer genericObjectToItemRenderer;

	private final ObjectModelRegistry objectModelRegistry;

	@Override
	protected String renderNonNull(Context context, GenericObject value) {
		ObjectModel objectModel = objectModelRegistry.findObjectModelForDatatype(context.getDatatype());
		//
		GenericObjectItem result = genericObjectToItemRenderer.renderNonNull(objectModel, value);
		//
		return result.toPrettyString();
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return context.getDatatype() != null
				? datatypeBaseRegistry.isAssignableTo(
				context.getDatatype().getBaseReference(),
				PandoraDatatypeConstants.OBJECT_DATATYPE_REFERENCE)
				? ToStringRenderer.MAX_DISTANCE_MINUS_ONE
				: null
				: null;
	}
}
