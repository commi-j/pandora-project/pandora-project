package tk.labyrinth.pandora.ui.component.value.box;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ValueBoxContext {

	@Nullable
	ValueWrapper currentValue;

	@Nullable
	ValueBoxContext parent;

	@NonNull
	String path;

	@Nullable
	public ValueWrapper getAncestorValue(int depth) {
		ValueWrapper result;
		{
			if (depth == 0) {
				result = currentValue;
			} else if (depth > 0) {
				result = parent.getAncestorValue(depth - 1);
			} else {
				throw new IllegalArgumentException(Integer.toString(depth));
			}
		}
		return result;
	}
}
