package tk.labyrinth.pandora.ui.domain.objectview;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.diversity.domain.bean.EagerBean;

@EagerBean
@RequiredArgsConstructor
public class GenericFormObjectViewRendererContributor {

	private final GenericFormObjectViewRenderer genericFormObjectViewRenderer;

	private final ObjectViewRendererRegistry objectViewRendererRegistry;

	@PostConstruct
	private void postConstruct() {
		objectViewRendererRegistry.registerDefinition(ObjectViewRendererDefinition.builder()
				.distance(500)
				.objectViewRenderer(parameters -> genericFormObjectViewRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.datatype(parameters.datatype())
						.initialValue(parameters.initialValue())
						.nonEditableReasons(parameters.nonEditableReasons())
						.objectModel(parameters.objectModel())
						.onValueChange(parameters.onValueChange())
						.build()))
				.slug("generic-form")
				.text("Generic Form")
				.build());
	}
}
