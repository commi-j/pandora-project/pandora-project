package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.checkbox.Checkbox;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import javax.annotation.CheckForNull;
import java.util.function.Consumer;

@Deprecated
public class CheckboxRenderer {

	public static Checkbox render(Properties properties) {
		Checkbox checkbox = new Checkbox();
		{
			checkbox.setLabel(properties.getLabel());
			checkbox.setReadOnly(!properties.calcEditable());
			ElementUtils.setTitle(
					checkbox,
					StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
			checkbox.setValue(properties.getValue());
		}
		{
			checkbox.addValueChangeListener(event -> {
				if (event.isFromClient()) {
					event.getSource().setValue(event.getOldValue());
					//
					if (properties.getOnValueChange() != null) {
						properties.getOnValueChange().accept(event.getValue());
					}
				}
			});
		}
		return checkbox;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@CheckForNull
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@CheckForNull
		Consumer<Boolean> onValueChange;

		@CheckForNull
		Boolean value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
