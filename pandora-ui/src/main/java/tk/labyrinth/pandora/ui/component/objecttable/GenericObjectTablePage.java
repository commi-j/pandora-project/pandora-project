package tk.labyrinth.pandora.ui.component.objecttable;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.telemetry.tracing.StaticTracer;
import tk.labyrinth.pandora.ui.component.objectaction.GenericObjectActionProvider;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableColumnDescriptor;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableColumnVisibility;
import tk.labyrinth.pandora.ui.component.table.AdvancedTablePageModel;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSearchResult;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSelectionModel;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableView;
import tk.labyrinth.pandora.ui.component.table.renderedobject.GenericObjectInlineRenderer;
import tk.labyrinth.pandora.ui.component.table.renderedobject.RenderedObject;
import tk.labyrinth.pandora.ui.routing.routetarget.ParametersAware;
import tk.labyrinth.pandora.ui.routing.routetarget.annotation.RouteTarget;

import java.time.Instant;
import java.util.Objects;

@RequiredArgsConstructor
@RouteTarget
@Slf4j
public class GenericObjectTablePage extends CssVerticalLayout implements
		ParametersAware<GenericObjectTablePage.Parameters> {

	@Deprecated
	public static final String TABLE_PATH_NAME_ATTRIBUTE_NAME = "tablePathName";

	private final GenericObjectInlineRenderer genericObjectInlineRenderer;

	private final GenericObjectSearcher genericObjectSearcher;

	private final ObjectProvider<AdvancedTableView> tableViewProvider;

	@SmartAutowired
	private List<GenericObjectActionProvider> objectActionProviders;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	{
		setHeightFull();
	}

	private CssHorizontalLayout createActionsComponent(
			ObjectModel objectModel,
			@Nullable GenericObject genericObject,
			Runnable refreshCallback) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			Datatype datatype = ObjectModelUtils.createDatatype(objectModel);
			//
			objectActionProviders
					.map(objectActionProvider -> objectActionProvider.provideActionForGenericObject(
							GenericObjectActionProvider.Context.<GenericObject>builder()
									.datatype(datatype)
									.object(genericObject)
									.objectModel(objectModel)
									.parentRefreshCallback(refreshCallback)
									.build()))
					.filter(Objects::nonNull)
					.forEach(layout::add);
		}
		return layout;
	}

	@WithSpan
	private RenderedObject renderObject(ObjectModel objectModel, GenericObject genericObject, Runnable refreshCallback) {
		RenderedObject result;
		{
			// TODO: Failure should be scoped with one element.
			RenderedObject renderedObject = genericObjectInlineRenderer.render(objectModel, genericObject);
			//
			result = renderedObject.withAttribute(
					"ACTIONS",
					createActionsComponent(objectModel, genericObject, refreshCallback));
		}
		return result;
	}

	private Try<List<RenderedObject>> renderObjects(
			ObjectModel objectModel,
			List<GenericObject> genericObjects,
			Runnable refreshCallback) {
		Try<List<RenderedObject>> result = Try.ofSupplier(() -> {
			List<RenderedObject> localResult = genericObjects.map(genericObject ->
					renderObject(objectModel, genericObject, refreshCallback));
			//
			return localResult;
		});
		//
		return result;
	}

	@Override
	public void acceptParameters(Parameters parameters) {
		ObjectModel objectModel = objectModelSearcher.findSingle(CodeObjectModelReference.of(parameters.objectModelCode()));
		//
		if (objectModel != null) {
			Observable<State> stateObservable = Observable.withInitialValue(State.builder()
					.columnVisibilities(objectModel.getAttributes()
							.map(attribute -> AdvancedTableColumnVisibility.of(attribute.getName(), true))
							.append(AdvancedTableColumnVisibility.of("ACTIONS", true)))
					.pageNumber(1)
					.predicate(null)
					.build());
			//
			removeAll();
			//
			AdvancedTableView tableView = tableViewProvider.getObject();
			CssFlexItem.setFlexGrow(tableView, 1);
			add(tableView);
			//
			stateObservable.subscribe((state, sink) -> tableView.setParameters(AdvancedTableView.Parameters.builder()
					.columnDescriptors(objectModel.getAttributes()
							.map(attribute -> AdvancedTableColumnDescriptor.builder()
									.attributeName(attribute.getName())
									.displayName(attribute.getName())
									.visible(state.columnVisibilities()
											.find(columnVisibility -> Objects.equals(
													columnVisibility.getAttributeName(),
													attribute.getName()))
											.get()
											.getVisible())
									.build())
							.append(AdvancedTableColumnDescriptor.builder()
									.attributeName("ACTIONS")
									.displayName("Actions")
									.flexGrow(1) // FIXME: Should be 0, but it fails to render all the actions properly.
									.header(createActionsComponent(
											objectModel,
											null,
											() -> sink.accept(state.withRefreshedAt(Instant.now()))))
									.visible(state.columnVisibilities()
											.find(columnVisibility -> Objects.equals(
													columnVisibility.getAttributeName(),
													"ACTIONS"))
											.get()
											.getVisible())
									.build()))
					//
					// FIXME: Should append hidden columns, otherwise they are lost.
					.onColumnOrderChange(nextColumnOrder -> sink.accept(state.withColumnVisibilities(
							nextColumnOrder.map(attributeName -> state.columnVisibilities()
									.find(columnVisibility -> Objects.equals(
											columnVisibility.getAttributeName(),
											attributeName))
									.get()))))
					.onColumnVibisibilyChange(nextColumnVisibility -> sink.accept(state.withColumnVisibilities(
							state.columnVisibilities().replace(
									state.columnVisibilities()
											.find(columnVisibility -> Objects.equals(
													columnVisibility.getAttributeName(),
													nextColumnVisibility.getAttributeName()))
											.get(),
									nextColumnVisibility))))
					.pageModel(AdvancedTablePageModel.builder()
							.onSelectedPageIndexChange(nextSelectedPageIndex ->
									sink.accept(state.withPageNumber(nextSelectedPageIndex + 1)))
							.selectedPageIndex(state.pageNumber() - 1)
							.build())
					.refreshedAt(state.refreshedAt())
					.searchFunction(searchParameters -> {
						long pageSize = 20; // FIXME: Use value from parameters.
						//
						ParameterizedQuery<CodeObjectModelReference> rawQuery = ParameterizedQuery.builder()
								.parameter(CodeObjectModelReference.from(objectModel))
								.predicate(searchParameters.getPredicate())
								.build();
						//
						List<GenericObject> genericObjects = StaticTracer.withinSpan(
								"generic-object-table-search",
								() -> genericObjectSearcher.search(rawQuery.toBuilder()
										.limit(pageSize)
										.offset(searchParameters.getPageIndex() * pageSize)
										.build()));
						//
						long count = StaticTracer.withinSpan(
								"generic-object-table-count",
								() -> genericObjectSearcher.count(rawQuery));
						//
						Try<List<RenderedObject>> renderedObjects = StaticTracer.withinSpan(
								"generic-object-table-render",
								() -> renderObjects(
										objectModel,
										genericObjects,
										() -> sink.accept(state.withRefreshedAt(Instant.now()))));
						//
						return AdvancedTableSearchResult.builder()
								.objectsTry(renderedObjects)
								.pageCount((int) Math.ceil(count / (double) pageSize))
								.build();
					})
					.selectionModel(AdvancedTableSelectionModel.builder()
							.onPredicateChange(nextPredicate -> sink.accept(state.withPredicate(nextPredicate)))
							.predicate(state.predicate())
							.build())
					.build()));
		} else {
			removeAll();
			add("NOT_FOUND: %s".formatted(parameters.objectModelCode()));
		}
	}

	@Override
	public Parameters getInitialParameters() {
		return Parameters.builder().build();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		String objectModelCode;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
		List<AdvancedTableColumnVisibility> columnVisibilities;

		@NonNull
		Integer pageNumber;

		@Nullable
		Predicate predicate;

		@Nullable
		Instant refreshedAt;
	}
}
