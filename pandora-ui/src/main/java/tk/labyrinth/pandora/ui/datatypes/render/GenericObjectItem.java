package tk.labyrinth.pandora.ui.datatypes.render;

import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

@Value
public class GenericObjectItem {

	public enum State {
		MODEL_NOT_FOUND,
		MODEL_REFERENCE_NOT_SPECIFIED,
		RENDER_RULE_NOT_SPECIFIED,
		VALID,
	}

	GenericObject object;

	@NonNull
	State state;

	@Nullable
	String text;

	@Nullable
	public String toPrettyString() {
		return switch (state) {
			case MODEL_NOT_FOUND -> "Model Not Found: " + object;
			case MODEL_REFERENCE_NOT_SPECIFIED -> "Model Reference Not Specified: " + object;
			case RENDER_RULE_NOT_SPECIFIED -> "Render Rule Not Specified: " + object;
			case VALID -> text;
		};
	}

	public static GenericObjectItem ofModelNotFound(GenericObject object) {
		return new GenericObjectItem(object, State.MODEL_NOT_FOUND, null);
	}

	public static GenericObjectItem ofModelReferenceNotSpecified(GenericObject object) {
		return new GenericObjectItem(object, State.MODEL_REFERENCE_NOT_SPECIFIED, null);
	}

	public static GenericObjectItem ofRenderRuleNotSpecified(GenericObject object) {
		return new GenericObjectItem(object, State.RENDER_RULE_NOT_SPECIFIED, null);
	}

	public static GenericObjectItem ofValid(GenericObject object, @Nullable String text) {
		return new GenericObjectItem(object, State.VALID, text);
	}
}
