package tk.labyrinth.pandora.ui.component.value.box.mutable;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import javax.annotation.CheckForNull;
import java.util.function.Consumer;

public interface AdvancedValueBox<T, P extends AdvancedValueBox.Properties<T>> extends RenderableView<P>, TypeAware<T> {

	@NonFinal
	@SuperBuilder(toBuilder = true)
	@Value
	class Properties<T> {

		T currentValue;

		T initialValue;

		/**
		 * Present in form boxes, absent in list boxes.
		 */
		@CheckForNull
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<T> onValueChange;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
