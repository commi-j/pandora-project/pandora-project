package tk.labyrinth.pandora.ui.component.value;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.init.customdatatype.PandoraDatatypeConstants;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectBoxRenderer;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;

@LazyComponent
@RequiredArgsConstructor
public class CoreDatatypesValueBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		return switch (datatypeBaseRegistry.determineKind(context.getDatatype())) {
			case OBJECT -> new FunctionalMutableValueBox<GenericObject>(
					parameters -> GenericObjectBoxRenderer.render(builder -> builder
							.currentValue(parameters.getCurrentValue())
							.initialValue(parameters.getInitialValue())
							.label(parameters.getLabel())
							.onValueChange(parameters.getOnValueChange())
							.build()),
					GenericObject.class);
			case SIMPLE -> new FunctionalMutableValueBox<String>(
					parameters -> SimpleValueBoxRenderer.render(builder -> builder
							.label(parameters.getLabel())
							.nonEditableReasons(parameters.getNonEditableReasons())
							.onValueChange(parameters.getOnValueChange())
							.value(parameters.getCurrentValue())
							.build()),
					String.class);
			default -> throw new NotImplementedException();
		};
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.matches(
				context.getDatatype(),
				PandoraDatatypeConstants.OBJECT_DATATYPE_REFERENCE) ||
				datatypeBaseRegistry.matches(
						context.getDatatype(),
						AliasDatatypeBaseReference.of(PandoraDatatypeConstants.SIMPLE_DATATYPE_NAME))
				? 1000
				: null;
	}
}
