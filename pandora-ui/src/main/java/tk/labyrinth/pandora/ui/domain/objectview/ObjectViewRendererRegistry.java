package tk.labyrinth.pandora.ui.domain.objectview;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

import java.util.concurrent.CopyOnWriteArraySet;

@Bean
@RequiredArgsConstructor
public class ObjectViewRendererRegistry {

	private final CopyOnWriteArraySet<ObjectViewRendererDefinition> definitions = new CopyOnWriteArraySet<>();

	public List<ObjectViewRendererDefinition> getDefinitions() {
		return List.ofAll(definitions);
	}

	public void registerDefinition(ObjectViewRendererDefinition definition) {
		definitions.add(definition);
	}
}
