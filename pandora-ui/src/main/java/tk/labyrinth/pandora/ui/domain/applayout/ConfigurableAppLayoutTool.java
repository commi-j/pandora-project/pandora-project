package tk.labyrinth.pandora.ui.domain.applayout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.BooleanUtils;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalcomponents.advanced.TreeGridWrapper;
import tk.labyrinth.pandora.functionalcomponents.html.AnchorRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.domain.sidebar.PandoraSidebarItem;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarCategoryConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarCategoryViewContributor;

import java.util.UUID;
import java.util.function.Consumer;

@Bean
@RequiredArgsConstructor
public class ConfigurableAppLayoutTool {

	@SmartAutowired
	private List<SidebarCategoryViewContributor> sidebarCategoryCustomRenderers;

	private Component renderRootItemsGrid(
			List<PandoraSidebarItem> rootItems,
			Consumer<TreeGridWrapper<PandoraSidebarItem>> gridConsumer) {
		val grid = new TreeGridWrapper<>(
				item -> item.getChildren() != null
						? item.getChildren().map(child -> child.withUid(UUID.randomUUID()))
						: List.empty(),
				item -> item.withChildren(null),
				PandoraSidebarItem::getUid);
		{
			grid.setAllRowsVisible(true);
			grid.setSelectionMode(Grid.SelectionMode.NONE);
			grid.addThemeVariants(
					GridVariant.LUMO_COMPACT,
					GridVariant.LUMO_NO_BORDER,
					GridVariant.LUMO_NO_ROW_BORDERS);
		}
		{
			grid.addComponentHierarchyColumn(item -> {
				String text = item.getText() != null ? item.getText() : "<empty>";
				//
				return item.getTarget() != null
						? AnchorRenderer.render(builder -> builder
						.href(item.getTarget())
						.target(BooleanUtils.isTrue(item.getExternal()) ? AnchorTarget.BLANK : null)
						.text(text)
						.build())
						: new Span(text);
			});
		}
		{
			grid.setItems(rootItems);
			grid.expandRootItems();
		}
		{
			gridConsumer.accept(grid);
		}
		return grid;
	}

	public Component renderSidebarCategory(
			SidebarCategoryConfiguration sidebarCategoryConfiguration,
			Consumer<TreeGridWrapper<PandoraSidebarItem>> gridConsumer) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			List<SidebarCategoryViewContributor> viewContributors = sidebarCategoryCustomRenderers
					.filter(renderer -> renderer.matches(sidebarCategoryConfiguration));
			List<PandoraSidebarItem> rootItems = sidebarCategoryConfiguration.getItemsOrEmpty()
					.map(item -> item.withUid(UUID.randomUUID()));
			//
			boolean hasViewContributors = !viewContributors.isEmpty();
			boolean hasRootItems = !rootItems.isEmpty();
			//
			if (hasViewContributors || hasRootItems) {
				if (hasViewContributors) {
					viewContributors.forEach(viewContributor -> layout.add(viewContributor
							.contributeView(sidebarCategoryConfiguration)
							.asVaadinComponent()));
				}
				if (hasRootItems) {
					layout.add(renderRootItemsGrid(rootItems, gridConsumer));
				}
			} else {
				layout.add(new Span("<empty>"));
			}
		}
		return layout;
	}
}
