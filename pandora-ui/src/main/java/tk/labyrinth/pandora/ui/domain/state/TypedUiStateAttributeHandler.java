package tk.labyrinth.pandora.ui.domain.state;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

public abstract class TypedUiStateAttributeHandler<T> implements UiStateAttributeHandler, TypeAware<T> {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Nullable
	protected abstract T fromQueryValueTyped(@Nullable String queryValue);

	@Nullable
	protected abstract T getDefaultValueTyped();

	@Nullable
	protected abstract String toQueryValueTyped(@Nullable T javaValue);

	@Nullable
	@Override
	public ValueWrapper fromQueryValue(@Nullable String queryValue) {
		return converterRegistry.convert(fromQueryValueTyped(queryValue), ValueWrapper.class);
	}

	@Nullable
	@Override
	public ValueWrapper getDefaultValue() {
		return converterRegistry.convert(getDefaultValueTyped(), ValueWrapper.class);
	}

	@Nullable
	@Override
	@SuppressWarnings("unchecked")
	public String toQueryValue(@Nullable ValueWrapper javaValue) {
		return toQueryValueTyped((T) converterRegistry.convert(javaValue, getParameterType()));
	}
}
