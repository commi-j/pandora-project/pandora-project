package tk.labyrinth.pandora.ui.datatypes.render;

import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.ui.routing.route.RouteHandler;
import tk.labyrinth.pandora.ui.routing.route.RouteModel;

import java.util.List;

@LazyComponent
@RequiredArgsConstructor
public class DatatypeCheckerPageRouteManager implements RouteHandler {

	private final ApplicationRouteRegistry applicationRouteRegistry;

	@PostConstruct
	private void postConstruct() {
		applicationRouteRegistry.setRoute(
				"pandora/datatype-checker",
				DatatypeCheckerPage.class,
				List.of(ConfigurableAppLayout.class));
		applicationRouteRegistry.setRoute(
				"pandora/datatype-checker-table",
				DatatypeCheckerTablePage.class,
				List.of(ConfigurableAppLayout.class));
	}

	@Nullable
	@Override
	public RouteModel findModel(BeforeEnterEvent beforeEnterEvent) {
		RouteModel result;
		{
			if (beforeEnterEvent.getNavigationTarget() == DatatypeCheckerPage.class) {
				result = RouteModel.builder().build();
			} else if (beforeEnterEvent.getNavigationTarget() == DatatypeCheckerTablePage.class) {
				result = RouteModel.builder().build();
			} else {
				result = null;
			}
		}
		return result;
	}
}
