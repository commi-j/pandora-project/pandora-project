package tk.labyrinth.pandora.ui.component.view.customizer;

import java.lang.reflect.Type;

public interface ViewCustomizer {

	boolean supports(Type type);
}
