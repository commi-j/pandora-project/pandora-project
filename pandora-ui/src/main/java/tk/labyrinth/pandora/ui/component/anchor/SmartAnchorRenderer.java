package tk.labyrinth.pandora.ui.component.anchor;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.router.internal.HasUrlParameterFormat;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.val;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import javax.annotation.CheckForNull;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class SmartAnchorRenderer {

	private final ObjectProvider<SmartAnchorQueryParametersContributor> queryParametersContributorProvider;

	public Component render(Properties properties) {
		if (!BooleanUtils.xor(new boolean[]{
				properties.getLocation() != null,
				properties.getNavigationTarget() != null})) {
			throw new IllegalArgumentException();
		}
		//
		Anchor result = new Anchor();
		{
			result.setEnabled(!Objects.equals(properties.getDisabled(), Boolean.TRUE));
			{
				Location explicitLocation;
				{
					explicitLocation = properties.getLocation() != null
							? properties.getLocation()
							: new Location(RouteConfiguration
							.forRegistry(UI.getCurrent().getInternals().getRouter().getRegistry())
							.getUrl(
									properties.getNavigationTarget(),
									properties.getNavigationTargetParameter() != null
											? new RouteParameters(
											HasUrlParameterFormat.PARAMETER_NAME,
											properties.getNavigationTargetParameter())
											: RouteParameters.empty()));
				}
				List<QueryParameters> contextQueryParameterses = List.ofAll(queryParametersContributorProvider.stream())
						.map(SmartAnchorQueryParametersContributor::contributeQueryParameters);
				//
				result.setHref(new Location(
						explicitLocation.getSegments(),
						mergeQueryParameters(explicitLocation.getQueryParameters(), contextQueryParameterses))
						.getPathWithQueryParameters());
			}
			result.setTarget(properties.getTarget() != null ? properties.getTarget() : AnchorTarget.DEFAULT);
			result.setText(properties.getText());
		}
		return result;
	}

	public static QueryParameters mergeQueryParameters(
			QueryParameters explicitQueryParameters,
			List<QueryParameters> contextQueryParameterses) {
		QueryParameters result;
		{
			val groupedQuery = contextQueryParameterses.prepend(explicitQueryParameters)
					.flatMap(SmartAnchorRenderer::queryParametersToListOfPairs)
					.groupBy(Pair::getLeft);
			//
			result = QueryParameters.simple(groupedQuery
					.toMap(
							Tuple2::_1,
							tuple -> {
								if (tuple._2().size() > 1) {
									throw new IllegalArgumentException();
								}
								return tuple._2().single().getRight();
							})
					.toJavaMap());
		}
		return result;
	}

	public static List<Pair<String, String>> queryParametersToListOfPairs(QueryParameters queryParameters) {
		return HashMap.ofAll(queryParameters.getParameters())
				.map(tuple -> {
					if (tuple._2().size() != 1) {
						throw new IllegalArgumentException();
					}
					return Pair.of(tuple._1(), tuple._2().get(0));
				})
				.toList();
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties {

		@CheckForNull
		Boolean disabled;

		/**
		 * Either <b>location</b> or <b>navigationTarget</b> must be specified.
		 */
		@CheckForNull
		Location location;

		/**
		 * Either <b>location</b> or <b>navigationTarget</b> must be specified.
		 */
		@CheckForNull
		Class<? extends Component> navigationTarget;

		/**
		 * Viable when <b>navigationTarget</b> is specified.
		 */
		@CheckForNull
		String navigationTargetParameter;

		@CheckForNull
		AnchorTarget target;

		@CheckForNull
		String text;
	}
}
