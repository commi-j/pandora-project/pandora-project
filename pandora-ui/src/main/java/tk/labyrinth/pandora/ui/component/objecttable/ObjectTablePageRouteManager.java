package tk.labyrinth.pandora.ui.component.objecttable;

import com.vaadin.flow.router.BeforeEnterEvent;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.pattern.PandoraPattern;
import tk.labyrinth.pandora.ui.routing.route.RouteHandler;
import tk.labyrinth.pandora.ui.routing.route.RouteModel;

@Deprecated
@LazyComponent
@RequiredArgsConstructor
public class ObjectTablePageRouteManager implements RouteHandler {

	private static final PandoraPattern pathPattern = PandoraPattern.from(
			"pandora/objects/$%s".formatted(ObjectModel.MODEL_CODE));

	@Nullable
	@Override
	public RouteModel findModel(BeforeEnterEvent beforeEnterEvent) {
		RouteModel result;
		{
			if (beforeEnterEvent.getNavigationTarget() == GenericObjectTablePage.class) {
				Map<String, String> pathParameters = pathPattern.parse(beforeEnterEvent.getLocation().getPath());
				//
				result = RouteModel.builder()
						.parameters(GenericObject.of(
								GenericObjectAttribute.ofSimple(
										"objectModelCode",
										pathParameters.get(ObjectModel.MODEL_CODE).get())))
						.build();
			} else {
				result = null;
			}
		}
		return result;
	}
}
