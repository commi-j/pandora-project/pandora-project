package tk.labyrinth.pandora.ui.component.value.list;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.ui.component.value.box.mutable.ConvertingValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.function.Supplier;

@Bean
@RequiredArgsConstructor
public class ListBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ToStringRendererRegistry toStringRendererRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		Datatype firstParameter = context.getDatatype().getParameters().get(0);
		//
		return createValueBox(
				javaBaseTypeRegistry.getJavaType(context.getDatatype()),
				firstParameter,
				() -> registry.getValueBox(firstParameter),
				toStringRendererRegistry);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.determineKind(context.getDatatype()) == DatatypeKind.LIST
				? ToStringRenderer.MAX_DISTANCE_MINUS_TWO
				: null;
	}

	public static <E> ConvertingValueBox<List<E>, VavrListBox.Properties<E>> createValueBox(
			Type valueType,
			Datatype elementDatatype,
			Supplier<MutableValueBox<E>> elementBoxSupplier,
			ToStringRendererRegistry toStringRendererRegistry) {
		VavrListBox<E> valueBox = new VavrListBox<>();
		//
		return ConvertingValueBox.of(
				valueBox,
				valueType,
				externalProperties -> VavrListBox.Properties.<E>builder()
						.currentValue(externalProperties.getCurrentValue())
						.elementBoxSupplier(elementBoxSupplier)
						.elementRenderer(element -> toStringRendererRegistry.render(
								ToStringRendererRegistry.Context.of(elementDatatype),
								element))
						.initialValue(externalProperties.getInitialValue())
						.label(externalProperties.getLabel())
						.nonEditableReasons(externalProperties.getNonEditableReasons())
						.onValueChange(externalProperties.getOnValueChange())
						.build());
	}
}
