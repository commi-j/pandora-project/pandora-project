package tk.labyrinth.pandora.ui.paginator;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PaginatorUtils {

	public static List<Integer> getIndicesAtLeftSide(List<Integer> offsets, int selectedPageNumber) {
		Stream<Integer> result;
		if (selectedPageNumber > offsets.size() + 1) {
			result = StreamUtils.concat(1, StreamUtils.indexed(offsets.stream())
					.sorted(Comparator.comparing(Pair<Integer, Integer>::getLeft).reversed())
					.map(pair -> Math.max(selectedPageNumber - pair.getRight(), offsets.size() - pair.getLeft() + 1)));
		} else {
			result = IntStream.range(1, selectedPageNumber).boxed();
		}
		return result.collect(Collectors.toList());
	}

	public static List<Integer> getIndicesAtRightSide(List<Integer> offsets, int selectedPageNumber, int pageCount) {
		Stream<Integer> result;
		if ((pageCount - selectedPageNumber) > offsets.size() + 1) {
			result = StreamUtils.concat(StreamUtils.indexed(offsets.stream())
					.map(pair -> Math.min(selectedPageNumber + pair.getRight(), pageCount - pair.getLeft() + 1)), pageCount);
		} else {
			result = IntStream.range(selectedPageNumber, pageCount).map(index -> index + 1).boxed();
		}
		return result.collect(Collectors.toList());
	}
}
