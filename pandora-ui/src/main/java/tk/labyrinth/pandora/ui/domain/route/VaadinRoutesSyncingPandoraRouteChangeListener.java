package tk.labyrinth.pandora.ui.domain.route;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.internal.HasUrlParameterFormat;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.ui.domain.page.PageRenderer;
import tk.labyrinth.pandora.ui.domain.page.PandoraPage;
import tk.labyrinth.pandora.ui.domain.page.PandoraPageVaadinUtils;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;

@Bean
@Slf4j
public class VaadinRoutesSyncingPandoraRouteChangeListener extends ObjectChangeListener<PageRoute> implements
		VaadinServiceInitListener {

	@Getter(AccessLevel.PACKAGE)
	private ApplicationRouteRegistry applicationRouteRegistry = null;

	@SmartAutowired
	private TypedObjectSearcher<PandoraPage> pandoraPageSearcher;

	@SmartAutowired
	private TypedObjectSearcher<PageRoute> pandoraRouteSearcher;

	private void registerVaadinRoute(PageRoute pageRoute) {
		PandoraPage pandoraPage = pandoraPageSearcher.getSingle(pageRoute.getPageReference());
		//
		if (!pandoraPage.getHandle().startsWith("vaadinRoute")) {
			applicationRouteRegistry.setRoute(
					pageRoute.getPath(),
					resolvePageHandler(pandoraPage),
					List.<Class<? extends RouterLayout>>of(ConfigurableAppLayout.class).asJava());
		} else {
			// Ignoring those already registered by Vaadin itself.
		}
	}

	private Class<? extends Component> resolvePageHandler(PandoraPage pandoraPage) {
		Class<? extends Component> result;
		{
			String pageHandler = pandoraPage.getHandle();
			//
			if (pageHandler.startsWith("pandoraPageFunction")) {
				result = PageRenderer.class;
			} else if (PandoraPageVaadinUtils.isVaadinComponentClassPageHandler(pageHandler)) {
				result = PandoraPageVaadinUtils.getVaadinComponentClass(pageHandler);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Override
	protected void onObjectChange(ObjectChangeEvent<PageRoute> event) {
		if (applicationRouteRegistry != null) {
			if (event.hasPreviousObject()) {
				PandoraPage pandoraPage = pandoraPageSearcher.getSingle(event.getPreviousObjectOrFail().getPageReference());
				//
				// Vaadin 24.1.3: For some reason removeRoute with class with url parameter does not work
				// as specified in the docs and fails to find the route to delete.
				applicationRouteRegistry.removeRoute(HasUrlParameterFormat.getTemplate(
						event.getPreviousObjectOrFail().getPath(),
						resolvePageHandler(pandoraPage)));
			}
			if (event.hasNextObject()) {
				registerVaadinRoute(event.getNextObjectOrFail());
			}
		} else {
			// TODO
			logger.debug("onObjectChange before applicationRouteRegistry was initialized: primaryReference: {}", "TODO");
		}
	}

	@Override
	public void serviceInit(ServiceInitEvent event) {
		applicationRouteRegistry = ApplicationRouteRegistry.getInstance(event.getSource().getContext());
		//
		pandoraRouteSearcher.searchAll().forEach(this::registerVaadinRoute);
	}
}
