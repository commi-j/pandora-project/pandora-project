package tk.labyrinth.pandora.ui.component.object.custom;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CustomizeFormLayout {

	int MINUS_ONE_DUMMY_INTEGER = -1;

	/**
	 * @return all attributeNames or empty as default
	 */
	String[] boxOrder() default {};

	/**
	 * @return number of columns for this form or -1 as default
	 */
	int gridColumnNumber() default MINUS_ONE_DUMMY_INTEGER;

	/**
	 * @return valid css or empty as default
	 */
	String gridTemplateColumns() default "";
}
