package tk.labyrinth.pandora.ui.view;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectPrimaryReferenceHandler;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectMultiView;

import java.util.Objects;
import java.util.function.Consumer;

// TODO: Replace with FormsTool? This one is about dialogs and coupled to object manipulation.
@LazyComponent
@RequiredArgsConstructor
public class ObjectViewRegistry {

	private final BeanContext beanContext;

	private final ConverterRegistry converterRegistry;

	private final GenericObjectManipulator genericObjectManipulator;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final RootObjectPrimaryReferenceHandler rootObjectPrimaryReferenceHandler;

	private ConfirmationHandle<GenericObject> showViewDialog(
			Datatype datatype,
			ObjectModel objectModel,
			GenericObject object,
			@Nullable String nonEditableReason) {
		GenericObjectMultiView view = beanContext.getBeanFactory().withBean(objectModel).getBean(
				GenericObjectMultiView.class);
		//
		view.render(GenericObjectMultiView.Properties.builder()
				.currentValue(object)
				.datatype(datatype)
				.initialValue(object)
				.nonEditableReasons(nonEditableReason != null ? List.of(nonEditableReason) : List.empty())
				.build());
		//
		return ConfirmationViews
				.showViewDialog(view)
				.map(next -> view.getValue());
	}

	@Deprecated
	private void showViewDialog(
			Datatype datatype,
			ObjectModel objectModel,
			GenericObject object,
			@Nullable String nonEditableReason,
			@Nullable Consumer<GenericObject> resultCallback) {
		GenericObjectMultiView view = beanContext.getBeanFactory().withBean(objectModel).getBean(
				GenericObjectMultiView.class);
		//
		view.render(GenericObjectMultiView.Properties.builder()
				.currentValue(object)
				.datatype(datatype)
				.initialValue(object)
				.nonEditableReasons(nonEditableReason != null ? List.of(nonEditableReason) : List.empty())
				.build());
		//
		ConfirmationViews
				.showViewDialog(view)
				.subscribeAlwaysAccepted(next -> {
					if (resultCallback != null) {
						resultCallback.accept(view.getValue());
					}
				});
	}

	public <T> void showCreateViewDialog(
			@NonNull Class<T> javaClass,
			@NonNull T newObject,
			@NonNull Runnable parentRefreshCallback) {
		ObjectModel objectModel = javaBaseTypeRegistry.getObjectModel(javaClass);
		GenericObject genericObject = converterRegistry.convert(newObject, GenericObject.class);
		//
		showCreateViewDialog(
				JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(javaClass),
				objectModel,
				genericObject,
				parentRefreshCallback);
	}

	public void showCreateViewDialog(
			Datatype datatype,
			ObjectModel objectModel,
			GenericObject object,
			Runnable parentRefreshCallback) {
		if (RootObjectUtils.findUid(object) != null) {
			// FIXME: Obsolete - we now use primaryReferences.
			throw new IllegalArgumentException("Require null: object.uid, object = %s".formatted(object));
		}
		//
		showViewDialog(
				datatype,
				objectModel,
				object,
				null,
				next -> {
					genericObjectManipulator.create(next.withAddedAttribute(
							RootObjectUtils.createNewUidAttribute()));
					//
					parentRefreshCallback.run();
				});
	}

	public <T> void showEditViewDialog(Class<T> javaClass, T object, Runnable parentRefreshCallback) {
		ObjectModel objectModel = javaBaseTypeRegistry.getObjectModel(javaClass);
		GenericObject genericObject = converterRegistry.convert(object, GenericObject.class);
		//
		showEditViewDialog(
				JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(javaClass),
				objectModel,
				genericObject,
				parentRefreshCallback);
	}

	public void showEditViewDialog(
			Datatype datatype,
			ObjectModel objectModel,
			GenericObject object,
			Runnable parentRefreshCallback) {
		// TODO: uid must be preserved in box, not here.
		showViewDialog(
				datatype,
				objectModel,
				object,
				null,
				next -> {
					if (!Objects.equals(
							rootObjectPrimaryReferenceHandler.getPrimaryReference(next),
							rootObjectPrimaryReferenceHandler.getPrimaryReference(object))) {
						throw new IllegalArgumentException("Require equal primaryReference");
					}
					genericObjectManipulator.update(next);
					//
					parentRefreshCallback.run();
				});
	}

	public void showViewViewDialog(Datatype datatype, ObjectModel objectModel, GenericObject object) {
		showViewDialog(
				datatype,
				objectModel,
				object,
				"View is read-only",
				null);
	}
}
