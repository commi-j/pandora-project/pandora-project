package tk.labyrinth.pandora.ui.render;

public interface ToStringRenderer<T> extends Renderer<T, String> {
	// empty
}
