package tk.labyrinth.pandora.ui.domain.state;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.BeanPostProcessor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

/**
 * @see UiStateChangeListener
 */
@LazyComponent
public class UiStateChangeListenerBeanPostProcessor implements BeanPostProcessor {

	ObjectProvider<UiStateHandler> uiStateHandlerProvider;

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) {
		if (bean instanceof UiStateChangeListener uiStateChangeListener) {
			// TODO: Think about deregistering?
			uiStateHandlerProvider.getObject().getStateFlux().subscribe(uiStateChangeListener::onUiStateChange);
		}
		return bean;
	}
}
