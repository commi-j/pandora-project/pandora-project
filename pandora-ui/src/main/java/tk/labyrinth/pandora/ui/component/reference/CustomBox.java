package tk.labyrinth.pandora.ui.component.reference;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.textfield.TextField;

public class CustomBox<T> extends CustomField<T> {

	private final TextField textField = new TextField();

	{
		add(textField);
	}

	@Override
	protected T generateModelValue() {
		return null;
	}

	@Override
	protected void setPresentationValue(T t) {
		// TODO
	}
}
