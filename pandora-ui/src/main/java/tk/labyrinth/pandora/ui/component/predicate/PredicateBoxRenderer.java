package tk.labyrinth.pandora.ui.component.predicate;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.extra.predicate.PredicateUtils;
import tk.labyrinth.pandora.ui.component.selector.SelectorViewRenderer;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PredicateBoxRenderer {

	private final SelectorViewRenderer selectorViewRenderer;

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		TextField result = new TextField();
		//
		result.setLabel(parameters.label());
		result.setReadOnly(true);
		{
			Button editButton = new Button(
					VaadinIcon.EDIT.create(),
					event -> {
						Observable<Predicate> stateObservable = Observable.withInitialValue(
								Optional.ofNullable(parameters.currentValue()).orElseGet(Predicates::and));
						//
						ConfirmationViews
								.showFunctionDialog(
										stateObservable,
										nextState -> {
											TreeGrid<?> component = selectorViewRenderer.render(
													SelectorViewRenderer.Parameters.builder()
															.currentValue(nextState)
															.onValueChange(stateObservable::set)
															.build());
											//
											component.setWidth("50em");
											//
											return component;
										})
								.subscribeAlwaysAccepted(success -> {
									Consumer<Predicate> onValueChange = parameters.onValueChange();
									//
									if (onValueChange != null) {
										onValueChange.accept(stateObservable.get());
									}
								});
					});
			//
			editButton.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY);
			//
			result.setSuffixComponent(editButton);
		}
		result.setValue(PredicateUtils.renderWithInfixJunctions(parameters.currentValue(), false));
		//
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Predicate currentValue;

		@Nullable
		String label;

		@Nullable
		Consumer<Predicate> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
