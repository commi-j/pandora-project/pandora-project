package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

// https://dev.vaadin.com/ticket/2986
@Deprecated // FIXME: Or not? We have SelectRenderer, and boxes are for the specific datatypes.
public class SelectBoxRenderer {

	public static <T> Select<T> render(Properties<T> properties) {
		Select<T> select = new Select<>();
		{
			// We always set generator to handle nulls as empty strings.
			// We also need to set it before items or value.
			select.setItemLabelGenerator(properties.getItemToStringRenderer() != null
					? item -> item != null ? properties.getItemToStringRenderer().apply(item) : ""
					: item -> item != null ? item.toString() : "");
		}
		{
			Optional.ofNullable(properties.getNullSupported()).ifPresent(select::setEmptySelectionAllowed);
		}
		{
			List<T> items = properties.getItems();
			select.setItems(items.asJava());
			if (properties.getDividers() != null) {
				properties.getDividers().forEach(divider -> {
					int indexOfItemAfterDivider = items.indexOf(divider.getLeft());
					if (indexOfItemAfterDivider == 0) {
						select.addComponentAsFirst(divider.getRight());
					} else {
						select.addComponents(items.get(indexOfItemAfterDivider - 1), divider.getRight());
					}
				});
			}
		}
		select.setLabel(properties.getLabel());
		select.setReadOnly(!properties.calcEditable());
		if (properties.getItemToComponentRenderer() != null) {
			select.setRenderer(new ComponentRenderer<>(item -> properties.getItemToComponentRenderer().apply(item)));
		}
		select.setValue(properties.getValue());
		select.addValueChangeListener(event -> {
			if (event.isFromClient()) {
				select.setValue(event.getOldValue());
				if (properties.getOnValueChange() != null) {
					properties.getOnValueChange().accept(event.getValue());
				}
			}
		});
		return select;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties<T> {

		/**
		 * Pair left - element before which to add divider;<br>
		 * Pair right - divider;<br>
		 */
		// TODO: Combine with items (List<Component,List<T>>?)
		List<Pair<T, Component>> dividers;

		/**
		 * Applies to items. For value rendering see <b>itemToStringRenderer</b>.
		 * If null value is supported it should be processed by this renderer.
		 */
		@Nullable
		Function<T, Component> itemToComponentRenderer;

		/**
		 * Applies to value and items, if <b>itemToComponentRenderer</b> is absent.
		 * If null value is supported it should be processed by this renderer.
		 */
		@Nullable
		Function<T, String> itemToStringRenderer;

		@NonNull
		List<T> items;

		@Nullable
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		/**
		 * If true, null is prepended to the items. It is rendered with {@link #itemToStringRenderer}.
		 */
		@Nullable
		Boolean nullSupported;

		@Nullable
		Consumer<T> onValueChange;

		@Nullable
		T value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
