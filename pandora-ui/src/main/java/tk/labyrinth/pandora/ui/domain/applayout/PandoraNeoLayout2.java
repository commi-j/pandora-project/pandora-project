package tk.labyrinth.pandora.ui.domain.applayout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.theme.lumo.Lumo;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Margin;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.PlaceSelf;

import java.util.List;

public class PandoraNeoLayout2 extends SplitLayout implements RouterLayout {

	private boolean sidebarCollapsed = false;

	{
		{
			setHeightFull();
			//
			setSplitterPosition(15);
		}
		{
			CssGridLayout sidebar = new CssGridLayout();
			{
				sidebar.setMinWidth("3em");
				//
				sidebar.setGridTemplateAreas(
						"top",
						"centre",
						".",
						"bottom");
				sidebar.setGridTemplateRows("auto auto 1fr auto");
				//
				sidebar.getElement().getThemeList().add(Lumo.DARK);
				//
			}
			{
				TreeGrid<String> tg = new TreeGrid<>();
				//
				tg.addHierarchyColumn(item -> item);
				//
				tg.setSelectionMode(Grid.SelectionMode.NONE);
				tg.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_NO_ROW_BORDERS);
				tg.addItemClickListener(event -> {
					if (tg.isExpanded(event.getItem())) {
						tg.collapse(event.getItem());
					} else {
						tg.expand(event.getItem());
					}
				});
				//
				tg.setItems(
						List.of("aaaaa", "b", "ccc"),
						item -> item.length() > 1 ? List.of(item.substring(1)) : List.of());
				//
				sidebar.add(tg, "centre");
			}
			{
				Button button = ButtonRenderer.render(builder -> builder
						.icon(VaadinIcon.ANGLE_DOUBLE_LEFT.create())
						.build());
				//
				button.setHeight("2em");
				button.setWidth("2em");
				StyleUtils.setCssProperty(button, Margin.of("0.5em"));
				//
				CssGridItem.setJustifySelf(button, PlaceSelf.START);
				//
				button.addClickListener(event -> {
					sidebarCollapsed = !sidebarCollapsed;
					//
					update(sidebar, button);
				});
				//
				sidebar.add(button, "bottom");
				//
				update(sidebar, button);
			}
			//
			addToPrimary(sidebar);
		}
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		addToSecondary((Component) content);
	}

	public void update(CssGridLayout sidebar, Button button) {
		sidebar.setWidth(sidebarCollapsed ? "3em" : "10em");
		//
		button.setIcon(sidebarCollapsed ? VaadinIcon.ANGLE_DOUBLE_RIGHT.create() : VaadinIcon.ANGLE_DOUBLE_LEFT.create());
	}
}
