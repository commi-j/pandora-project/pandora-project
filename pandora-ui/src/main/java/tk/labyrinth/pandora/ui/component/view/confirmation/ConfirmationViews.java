package tk.labyrinth.pandora.ui.component.view.confirmation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.misc4j.data.Success;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.component.view.View;
import tk.labyrinth.pandora.ui.component.view.simple.SimpleView;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@Deprecated
public class ConfirmationViews {

	public static final String APPLY_BUTTON_NAME = "apply-button";

	public static final String CANCEL_BUTTON_NAME = "cancel-button";

	public static final String CONFIRM_BUTTON_NAME = "confirm-button";

	public static ConfirmationHandle<Success> showComponentDialog(Component component) {
		return showViewDialog(SimpleView.of(component));
	}

	public static <T> ConfirmationHandle<Success> showFunctionDialog(
			Observable<T> stateObservable,
			Function<T, Component> componentFunction) {
		FunctionalComponent<T> component = FunctionalComponent.create(
				stateObservable.get(),
				(properties, sink) -> componentFunction.apply(properties));
		//
		stateObservable.subscribe(component::setProperties);
		//
		return showComponentDialog(component);
	}

	public static <T> ConfirmationHandle<T> showFunctionDialog(
			T initialProperties,
			BiFunction<T, Consumer<T>, Component> componentFunction) {
		FunctionalComponent<T> component = FunctionalComponent.create(initialProperties, componentFunction);
		return showViewDialog(SimpleView.of(component), component::getProperties);
	}

	public static ConfirmationHandle<Success> showTextDialog(String text) {
		return showViewDialog(SimpleView.of(new Span(text)));
	}

	public static ConfirmationHandle<Success> showViewDialog(View view) {
		return showViewDialog(view, () -> Success.VALUE);
	}

	public static <T> ConfirmationHandle<T> showViewDialog(View view, Supplier<T> resultSupplier) {
		ConfirmationHandle<T> confirmationHandle = new ConfirmationHandle<>();
		{
			ConfirmationView confirmationView = new ConfirmationView();
			Runnable closeDialogCallback = Dialogs.show(
					confirmationView,
					() -> {
						// no-op
					});
			confirmationView.initialize(
					ConfirmationViewProperties.builder()
							.applyCallback(() -> {
								boolean result;
								{
									Predicate<T> subscribeFunction = confirmationHandle.getSubscribePredicate();
									if (subscribeFunction != null) {
										result = subscribeFunction.test(resultSupplier.get());
									} else {
										result = true;
									}
								}
								return result;
							})
							.closeCallback(closeDialogCallback)
							.showApplyButton(false)
							.build(),
					view);
		}
		return confirmationHandle;
	}
}
