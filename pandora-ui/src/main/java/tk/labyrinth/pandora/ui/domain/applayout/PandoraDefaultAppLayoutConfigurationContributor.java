package tk.labyrinth.pandora.ui.domain.applayout;

import io.vavr.collection.List;
import jakarta.annotation.Priority;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.PandoraInitializer;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarCategoryConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SlugSidebarCategoryConfigurationReference;

@Bean
@Priority(10000)
@RequiredArgsConstructor
public class PandoraDefaultAppLayoutConfigurationContributor implements PandoraInitializer {

	private final AppLayoutConfigurationRegistry appLayoutConfigurationRegistry;

	@SmartAutowired
	private TypedObjectManipulator<SidebarCategoryConfiguration> sidebarCategoryConfigurationManipulator;

	@Override
	public void run() {
		appLayoutConfigurationRegistry.setAppLayoutConfiguration(PandoraAppLayoutConfiguration.builder()
				.priority(0)
				.sidebar(SidebarConfiguration.builder()
						.categoryReferences(List.of(
								SlugSidebarCategoryConfigurationReference.of("pandora-empty"),
								SlugSidebarCategoryConfigurationReference.of("pandora-default")))
						.mode(SidebarConfiguration.SidebarMode.SLIDE)
						.build())
				.useHeader(false)
				.build());
	}
}
