package tk.labyrinth.pandora.ui.domain.mongodb;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.bson.types.ObjectId;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.views.style.PandoraColours;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * This is a very simple solution that supports only generation of a new value.
 */
public class MongoDbObjectIdBoxRenderer {

	public static View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static View render(Parameters parameters) {
		String initialValue = parameters.initialValue();
		String currentValue = parameters.currentValue();
		//
		TextField textField = TextFieldRenderer.render(builder -> builder
				.label(parameters.label())
				.readOnly(true)
				.suffixComponent(parameters.currentValue() == null
						? ButtonRenderer.render(buttonBuilder -> buttonBuilder
						.enabled(parameters.calcEditable())
						.icon(VaadinIcon.RETWEET.create())
						.onClick(event -> parameters.onValueChange().accept(new ObjectId().toHexString()))
						.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
						.build())
						: null)
				.value(StringUtils.nullToEmpty(currentValue))
				.width(parameters.width())
				.build());
		//
		{
			if (!Objects.equals(currentValue, initialValue)) {
				String colour;
				//
				if (initialValue == null) {
					colour = PandoraColours.GREEN_50;
				} else if (currentValue == null) {
					colour = PandoraColours.RED_50;
				} else {
					colour = PandoraColours.BLUE_50;
				}
				//
				textField.getStyle().set("border-radius", "4px");
				textField.getStyle().set("box-shadow", "0 0 0 2px %s".formatted(colour));
			}
		}
		//
		return ComponentView.of(textField);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		String currentValue;

		@Nullable
		String initialValue;

		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		Consumer<@Nullable String> onValueChange;

		@Nullable
		String width;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder {
			// Lomboked
		}
	}
}
