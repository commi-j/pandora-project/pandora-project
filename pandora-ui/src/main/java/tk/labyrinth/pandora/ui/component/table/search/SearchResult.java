package tk.labyrinth.pandora.ui.component.table.search;

import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.ui.component.table.renderedobject.RenderedObject;

import java.time.Instant;

@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class SearchResult {

	@NonNull
	Instant finishedAt;

	@NonNull
	Try<List<RenderedObject>> objectsTry;

	/**
	 * Null when count is not performed or error happened.
	 */
	@Nullable
	Integer pageCount;
}
