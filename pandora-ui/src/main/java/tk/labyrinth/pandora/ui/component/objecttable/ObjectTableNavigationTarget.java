package tk.labyrinth.pandora.ui.component.objecttable;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ObjectTableNavigationTarget {

	String objectModelCode;
}
