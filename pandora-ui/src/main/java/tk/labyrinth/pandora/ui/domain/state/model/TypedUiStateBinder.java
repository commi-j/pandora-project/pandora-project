package tk.labyrinth.pandora.ui.domain.state.model;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotations;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.domain.state.StringListUiStateAttributeHandler;
import tk.labyrinth.pandora.ui.domain.state.StringUiStateAttributeHandler;
import tk.labyrinth.pandora.ui.domain.state.UiStateAttributeHandler;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class TypedUiStateBinder<T> {

	private final ConverterRegistry converterRegistry;

	private final GenericUiStateBinder genericUiStateBinder;

	/**
	 * @param type               non-null
	 * @param localStateSupplier supplies current local state to compute next one when UI state changes;
	 * @param localStateConsumer consumes next local state when UI state changes;
	 * @param localStateFlux     reports when local state is changed to update UI state;
	 */
	public Disposable bind(
			Class<T> type,
			Supplier<T> localStateSupplier,
			Consumer<T> localStateConsumer,
			Flux<T> localStateFlux) {
		return bind(type, null, localStateSupplier, localStateConsumer, localStateFlux);
	}

	/**
	 * @param type               non-null
	 * @param defaultValue       nullable
	 * @param localStateSupplier supplies current local state to compute next one when UI state changes;
	 * @param localStateConsumer consumes next local state when UI state changes;
	 * @param localStateFlux     reports when local state is changed to update UI state;
	 */
	public Disposable bind(
			Class<T> type,
			@Nullable T defaultValue,
			Supplier<T> localStateSupplier,
			Consumer<T> localStateConsumer,
			Flux<T> localStateFlux) {
		List<Pair<Field, UiStateAttributeHandler>> fieldAndAttributeHandlerPairs = resolveAttributeHandlers(
				type,
				defaultValue);
		//
		return genericUiStateBinder.bind(
				fieldAndAttributeHandlerPairs.map(Pair::getRight),
				nextUiState -> {
					GenericObject currentLocalState = converterRegistry.convert(
							localStateSupplier.get(),
							GenericObject.class);
					//
					T nextLocalState = converterRegistry.convert(nextUiState
							.map(attribute -> {
								Pair<Field, UiStateAttributeHandler> foundPair = fieldAndAttributeHandlerPairs
										.find(pair -> Objects.equals(pair.getRight().getAttributeName(), attribute.getName()))
										.get();
								//
								return attribute.withName(foundPair.getLeft().getName());
							})
							.enrichFrom(currentLocalState), type);
					//
					localStateConsumer.accept(nextLocalState);
				},
				localStateFlux.map(nextLocalState -> converterRegistry.convert(nextLocalState, GenericObject.class)
						.map(attribute -> {
							Pair<Field, UiStateAttributeHandler> foundPair = fieldAndAttributeHandlerPairs
									.find(pair -> Objects.equals(pair.getLeft().getName(), attribute.getName()))
									.getOrNull();
							//
							return foundPair != null
									? attribute.withName(foundPair.getRight().getAttributeName())
									: attribute;
						})));
	}

	public List<Pair<Field, UiStateAttributeHandler>> resolveAttributeHandlers(Class<?> type, @Nullable Object defaultValue) {
		GenericObject genericDefaultValue = converterRegistry.convert(defaultValue, GenericObject.class);
		//
		return List.of(type.getDeclaredFields())
				.filter(field -> MergedAnnotations.from(field).isPresent(UiStateAttribute.class))
				.map(field -> {
					UiStateAttribute attributeAnnotation = MergedAnnotations.from(field)
							.get(UiStateAttribute.class)
							.synthesize();
					//
					String attributeName = UiStateAttributeUtils.computeAttributeName(
							attributeAnnotation,
							field.getName());
					//
					UiStateAttributeHandler attributeHandler;
					{
						if (field.getType() == List.class) {
							//noinspection unchecked
							attributeHandler = StringListUiStateAttributeHandler.of(
									attributeName,
									genericDefaultValue != null
											? (List<String>) converterRegistry.convert(
											genericDefaultValue.findAttributeValue(attributeName),
											List.class,
											String.class)
											: null,
									attributeAnnotation.kind(),
									UiStateAttributeUtils.computeQueryAttributeName(
											attributeAnnotation,
											field.getName()));
						} else {
							attributeHandler = StringUiStateAttributeHandler.of(
									attributeName,
									genericDefaultValue != null
											? genericDefaultValue.findAttributeValueAsString(attributeName)
											: null,
									attributeAnnotation.kind(),
									UiStateAttributeUtils.computeQueryAttributeName(
											attributeAnnotation,
											field.getName()));
						}
					}
					return Pair.of(field, attributeHandler);
				});
	}
}
