package tk.labyrinth.pandora.ui.component.objectaction.basic;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.Order;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.ui.component.objectaction.GenericObjectActionProvider;
import tk.labyrinth.pandora.ui.view.ObjectViewRegistry;

@Bean
@Order(ViewObjectActionProvider.PRIORITY)
@RequiredArgsConstructor
public class ViewObjectActionProvider implements GenericObjectActionProvider {

	public static final int PRIORITY = EditObjectActionProvider.PRIORITY - 1;

	private final GenericObjectManipulator genericObjectManipulator;

	private final ObjectViewRegistry objectViewRegistry;

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return 0;
	}

	@Nullable
	@Override
	public Component provideActionForGenericObject(Context<GenericObject> context) {
		Component result;
		{
			ObjectModel objectModel = context.getObjectModel();
			GenericObject object = context.getObject();
			//
			if (object != null) {
				result = ObjectActionUtils.createButton(
						"view-object-button",
						null,
						VaadinIcon.EYE,
						() -> objectViewRegistry.showViewViewDialog(context.getDatatype(), objectModel, object),
						"View Object",
						null);
			} else {
				result = null;
			}
		}
		return result;
	}
}
