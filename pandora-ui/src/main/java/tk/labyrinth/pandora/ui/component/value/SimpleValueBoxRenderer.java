package tk.labyrinth.pandora.ui.component.value;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.views.box.StringBoxRenderer;

import java.util.function.Consumer;
import java.util.function.Function;

public class SimpleValueBoxRenderer {

	public static Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	// TODO: Rework into supporting current/initial.
	public static Component render(Parameters parameters) {
		return StringBoxRenderer.render(builder -> builder
						.currentValue(parameters.value())
						.initialValue(parameters.value())
						.label(parameters.label())
						.nonEditableReasons(parameters.nonEditableReasons())
						.onValueChange(parameters.onValueChange())
						.build())
				.asVaadinComponent();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<String> onValueChange;

		@Nullable
		String value;

		public static class Builder {
			// Lomboked
		}
	}
}
