package tk.labyrinth.pandora.ui.domain.page;

import com.nimbusds.jose.util.Pair;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.dependencies.spring.JavaElementDiscoverer;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.views.domain.page.PageUtils;
import tk.labyrinth.pandora.views.domain.page.meta.PageFunction;
import tk.labyrinth.pandora.views.domain.page.meta.PageRendererClass;

@Bean
@RequiredArgsConstructor
public class PageFunctionDiscoveringPandoraPageContributor extends GenericObjectsContributingInitializer {

	private final JavaElementDiscoverer javaElementDiscoverer;

	@Override
	protected List<GenericObject> contributeObjects() {
		List<TypeSignature> typeSignatures = javaElementDiscoverer.discoverTypeSignatures(
				"tk.labyrinth",
				List.of(new AnnotationTypeFilter(PageRendererClass.class, true, true)));
		//
		List<PandoraPage> pandoraPages = typeSignatures
				.map(typeSignature -> ClassUtils.get(typeSignature.getBinaryName()))
				.flatMap(javaClass -> List.of(javaClass.getMethods())
						.map(method -> Pair.of(method, MergedAnnotations.from(method).get(PageFunction.class)))
						.filter(pair -> pair.getRight().isPresent())
						.map(pair -> {
							String methodFullSignatureString = PageUtils.computePageFunctionSlug(pair.getLeft());
							//
							return PandoraPage.builder()
									.handle("pandoraPageFunction:%s".formatted(methodFullSignatureString))
									.slug(methodFullSignatureString)
									.parametersModel(List.empty()) // TODO
									.build();
						}));
		//
		return pandoraPages.map(this::convertAndAddHardcoded);
	}
}
