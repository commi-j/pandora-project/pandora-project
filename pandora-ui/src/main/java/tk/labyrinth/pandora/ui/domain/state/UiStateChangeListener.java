package tk.labyrinth.pandora.ui.domain.state;

import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

/**
 * Interface for Spring Beans.
 *
 * @see UiStateChangeListenerBeanPostProcessor
 * @see UiStateHandler
 */
public interface UiStateChangeListener {

	void onUiStateChange(GenericObject nextUiState);
}
