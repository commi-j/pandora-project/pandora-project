package tk.labyrinth.pandora.ui.tool;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.UUID;

@Component
@Slf4j
public class StoreAccessWatcher {

	public static class StoreAccessEntry {

		Instant instant;

		UUID uid;
	}
}
