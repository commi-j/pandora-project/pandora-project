package tk.labyrinth.pandora.ui.tool;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationListener;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import io.opentelemetry.api.trace.Span;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.UiScopedComponent;
import tk.labyrinth.pandora.telemetry.StaticTelemetryProvider;

@RequiredArgsConstructor
@Slf4j
@UiScopedComponent
public class UiNavigationWatcher implements AfterNavigationListener, BeforeEnterListener {

	private final ThreadLocal<Span> spanThreadLocal = new ThreadLocal<>();

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		Span span = spanThreadLocal.get();
		if (span != null) {
			span.end();
		}
		//
		logger.info("afterNavigation: ui.id = {}, path = '{}', query = '{}'",
				UI.getCurrent().getUIId(),
				event.getLocation().getPath(),
				event.getLocation().getQueryParameters().getQueryString());
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Span span = StaticTelemetryProvider.getTracer().spanBuilder("ui-navigation")
				.setAttribute("path", event.getLocation().getPath())
				.setAttribute("query", event.getLocation().getQueryParameters().getQueryString())
				.setAttribute("ui-id", event.getUI().getUIId())
				.startSpan();
		spanThreadLocal.set(span);
		//
		logger.info("beforeEnter: ui.id = {}, path = '{}', query = '{}', target = {}",
				UI.getCurrent().getUIId(),
				event.getLocation().getPath(),
				event.getLocation().getQueryParameters().getQueryString(),
				event.getNavigationTarget().getSimpleName());
	}
}
