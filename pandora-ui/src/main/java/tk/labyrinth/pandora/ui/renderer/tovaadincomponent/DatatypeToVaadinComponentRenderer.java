package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

@LazyComponent
@RequiredArgsConstructor
public class DatatypeToVaadinComponentRenderer extends ToVaadinComponentRendererBase<Datatype> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, Datatype value) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			StyleUtils.setCssProperty(layout, WhiteSpace.PRE_WRAP);
		}
		{
			layout.add(context.getRendererRegistry().render(
					ToVaadinComponentRendererRegistry.Context.builder()
							.datatype(AliasDatatypeBaseReference.class)
							.hints(List.empty())
							.build(),
					value.getBaseReference()));
			//
			if (value.getParameters() != null) {
				layout.add("<");
				{
					List<Component> components = List.ofAll(value.getParameters())
							.map(parameter -> context.getRendererRegistry().render(
									ToVaadinComponentRendererRegistry.Context.builder()
											.datatype(Datatype.class)
											.hints(List.empty())
											.build(),
									parameter));
					//
					layout.add(components.head());
					components.tail().forEach(component -> {
						layout.add(",");
						layout.add(component);
					});
				}
				layout.add(">");
			}
		}
		return layout;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), Datatype.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
