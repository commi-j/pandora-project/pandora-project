package tk.labyrinth.pandora.ui.renderer.tostring;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

@Bean
@RequiredArgsConstructor
public class ToStringRendererRegistry {

	private final ConverterRegistry converterRegistry;

	@SmartAutowired
	private List<ToStringRenderer<?>> renderers;

	@Nullable
	@SuppressWarnings("unchecked")
	public <T> ToStringRenderer<T> findRenderer(Context context) {
		HasSupportDistance.SelectionResult<ToStringRenderer<?>, ToStringRenderer.Context> selectionResult =
				HasSupportDistance.selectHandler(
						renderers,
						ToStringRenderer.Context.builder()
								.datatype(context.getDatatype())
								.hints(context.getHints())
								.rendererRegistry(this)
								.build());
		//
		return (ToStringRenderer<T>) selectionResult.findHandler();
	}

	public <T> ToStringRenderer<T> getRenderer(Context context) {
		ToStringRenderer<T> result = findRenderer(context);
		if (result == null) {
			throw new IllegalArgumentException("Not found: context = %s".formatted(context));
		}
		return result;
	}

	@Nullable
	public String render(Context context, @Nullable Object value) {
		ToStringRenderer<Object> renderer = getRenderer(context);
		//
		return renderer.render(
				ToStringRenderer.Context.builder()
						.datatype(context.getDatatype())
						.hints(context.getHints())
						.rendererRegistry(this)
						.build(),
				converterRegistry.convert(value, renderer.getParameterType()));
	}

	public HasSupportDistance.SelectionResult<ToStringRenderer<?>, ToStringRenderer.Context> selectRenderers(
			Context context) {
		return HasSupportDistance.selectHandler(
				renderers,
				ToStringRenderer.Context.builder()
						.datatype(context.getDatatype())
						.hints(context.getHints())
						.rendererRegistry(this)
						.build());
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Context {

		/**
		 * Null is legitimate here, since we may have no model for certain value.
		 */
		@Nullable
		Datatype datatype;

		@Nullable // FIXME: Make non-null.
		Object hints;

		public static Context empty() {
			return builder()
					.datatype(null)
					.hints(List.empty())
					.build();
		}

		public static Context of(Datatype datatype) {
			return builder()
					.datatype(datatype)
					.hints(List.empty())
					.build();
		}
	}
}
