package tk.labyrinth.pandora.ui.component.predicate;

import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.operator.JunctionOperator;
import tk.labyrinth.expresso.lang.operator.ObjectOperator;
import tk.labyrinth.expresso.lang.operator.StringOperator;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.ObjectPropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.PropertyPredicate;
import tk.labyrinth.expresso.query.lang.predicate.StringPropertyPredicate;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.CheckboxRenderer;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.value.box.simple.SelectBoxRenderer;
import tk.labyrinth.pandora.ui.component.valuewrapper.ValueWrapperBoxRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class PredicateViewRenderer {

	private final ConverterRegistry converterRegistry;

	public CssVerticalLayout render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(PredicateViewRenderer.Parameters.builder()));
	}

	public CssVerticalLayout render(Parameters parameters) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			Predicate predicate = parameters.currentValue();
			//
			{
				layout.add(SelectBoxRenderer.render(SelectBoxRenderer.Properties.builder()
						.dividers(List.of(
								Pair.of(JunctionOperator.values()[0], new Span("Junctions")),
								Pair.of(ObjectOperator.values()[0], new Span("Objects")),
								Pair.of(StringOperator.values()[0], new Span("Strings"))))
						.items(List
								.of(
										List.of(JunctionOperator.values()),
										List.of(ObjectOperator.values()),
										List.of(StringOperator.values()))
								.flatMap(Function.identity())
						)
						.label("Predicate Kind")
						.onValueChange(nextValue -> parameters.onValueChange()
								.accept(computePredicateWithNextKind(predicate, nextValue)))
						.value(resolvePredicateKind(predicate))
						.build()));
			}
			{
				if (predicate != null) {
					CssHorizontalLayout attributesLayout = new CssHorizontalLayout();
					{
						if (predicate instanceof JunctionPredicate) {
							// TODO
						} else if (predicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
							attributesLayout.add(StringBoxRenderer.render(builder -> builder
									.label("Property")
									.onValueChange(nextValue -> parameters.onValueChange()
											.accept(objectPropertyPredicate.withProperty(nextValue)))
									.value(objectPropertyPredicate.property())
									.build()));
							attributesLayout.add(ValueWrapperBoxRenderer.render(
									ValueWrapperBoxRenderer.Parameters.builder()
											.label("Value")
											.onValueChange(nextValue -> parameters.onValueChange()
													.accept(objectPropertyPredicate.withValue(nextValue)))
											.value(converterRegistry.convert(
													objectPropertyPredicate.value(),
													ValueWrapper.class))
											.build()));
						} else if (predicate instanceof StringPropertyPredicate stringPropertyPredicate) {
							attributesLayout.add(StringBoxRenderer.render(builder -> builder
									.label("Property")
									.onValueChange(nextValue -> parameters.onValueChange()
											.accept(stringPropertyPredicate.withProperty(nextValue)))
									.value(stringPropertyPredicate.property())
									.build()));
							attributesLayout.add(StringBoxRenderer.render(builder -> builder
									.label("Value")
									.onValueChange(nextValue -> parameters.onValueChange()
											.accept(stringPropertyPredicate.withValue(nextValue)))
									.value(stringPropertyPredicate.value())
									.build()));
							attributesLayout.add(CheckboxRenderer.render(builder -> builder
									.label("Case Sensitive")
									.onValueChange(nextValue -> parameters.onValueChange()
											.accept(stringPropertyPredicate.withCaseSensitive(nextValue)))
									.value(stringPropertyPredicate.caseSensitive())
									.build()));
						} else {
							throw new NotImplementedException();
						}
					}
					if (attributesLayout.getChildren().findAny().isPresent()) {
						attributesLayout.addClassNames(PandoraStyles.LAYOUT);
						//
						attributesLayout.setAlignItems(AlignItems.BASELINE);
						//
						attributesLayout.getChildren().forEach(child -> CssFlexItem.setFlexGrow(child, 1));
						//
						layout.add(attributesLayout);
					}
				}
			}
		}
		return layout;
	}

	public static Predicate computeNewPredicate(Object kind) {
		Predicate result;
		{
			if (kind instanceof JunctionOperator junctionOperator) {
				result = new JunctionPredicate(junctionOperator, java.util.List.of());
			} else if (kind instanceof ObjectOperator objectOperator) {
				result = new ObjectPropertyPredicate(objectOperator, null, null);
			} else if (kind instanceof StringOperator stringOperator) {
				result = new StringPropertyPredicate(false, stringOperator, null, null);
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public static Predicate computePredicateWithNextKind(@Nullable Predicate currentPredicate, Object nextKind) {
		Predicate result;
		{
			if (currentPredicate != null) {
				if (currentPredicate instanceof JunctionPredicate junctionPredicate) {
					result = nextKind instanceof JunctionOperator junctionOperator
							? junctionPredicate.withOperator(junctionOperator)
							: computeNewPredicate(nextKind);
				} else if (currentPredicate instanceof ObjectPropertyPredicate objectPropertyPredicate) {
					if (nextKind instanceof ObjectOperator objectOperator) {
						result = objectPropertyPredicate.withOperator(objectOperator);
					} else if (nextKind instanceof StringOperator stringOperator) {
						result = new StringPropertyPredicate(
								false,
								stringOperator,
								objectPropertyPredicate.property(),
								objectPropertyPredicate.value() instanceof SimpleValueWrapper simpleValueWrapper
										? simpleValueWrapper.getValue()
										: null);
					} else {
						result = computeNewPredicate(nextKind);
					}
				} else if (currentPredicate instanceof StringPropertyPredicate stringPropertyPredicate) {
					if (nextKind instanceof ObjectOperator objectOperator) {
						result = new ObjectPropertyPredicate(
								objectOperator,
								stringPropertyPredicate.property(),
								stringPropertyPredicate.value());
					} else if (nextKind instanceof StringOperator stringOperator) {
						result = stringPropertyPredicate.withOperator(stringOperator);
					} else {
						result = computeNewPredicate(nextKind);
					}
				} else {
					throw new NotImplementedException();
				}
			} else {
				result = computeNewPredicate(nextKind);
			}
		}
		return result;
	}

	public static Object resolvePredicateKind(@Nullable Predicate predicate) {
		Object result;
		{
			if (predicate != null) {
				if (predicate instanceof JunctionPredicate junctionPredicate) {
					result = junctionPredicate.operator();
				} else if (predicate instanceof PropertyPredicate<?> propertyPredicate) {
					result = propertyPredicate.operator();
				} else {
					throw new NotImplementedException();
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Predicate currentValue;

		Consumer<Predicate> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
