package tk.labyrinth.pandora.ui.domain.state;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

public interface UiStateAttributeHandler {

	@Nullable
	ValueWrapper fromQueryValue(@Nullable String queryValue);

	String getAttributeName();

	@Nullable
	ValueWrapper getDefaultValue();

	UiStateAttributeKind getKind();

	/**
	 * May be overriden in case there's a difference between names, e.g. when you want your query to be shorter.
	 *
	 * @return fit
	 */
	default String getQueryAttributeName() {
		return getAttributeName();
	}

	@Nullable
	String toQueryValue(@Nullable ValueWrapper javaValue);
}
