package tk.labyrinth.pandora.ui.component.table.renderedobject;

import com.vaadin.flow.component.Component;
import io.vavr.collection.Map;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RenderedObject {

	@NonNull
	Map<String, Component> attributes;

	@Nullable
	Throwable fault;

	@NonNull
	GenericObject object;

	// TODO: May be rename to withAddedAttribute and fail if present?
	public RenderedObject withAttribute(String name, Component component) {
		return withAttributes(attributes.put(name, component));
	}
}
