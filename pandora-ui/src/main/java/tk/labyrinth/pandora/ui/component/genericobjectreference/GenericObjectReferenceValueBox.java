package tk.labyrinth.pandora.ui.component.genericobjectreference;

import com.vaadin.flow.component.Component;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;

@Deprecated // This class has insufficient level of abstraction, only direct ref classes supported.
@PrototypeScopedComponent
@RequiredArgsConstructor
public class GenericObjectReferenceValueBox implements SimpleValueBox<GenericObjectReference> {

	private final Class<? extends Reference<?>> referenceClass;

	@Autowired
	private GenericObjectReferenceBox referenceBox;

	@Override
	public Component asVaadinComponent() {
		return referenceBox.asVaadinComponent();
	}

	@Override
	public void render(Properties<GenericObjectReference> properties) {
		referenceBox.render(GenericObjectReferenceBox.Properties.builder()
				.label(properties.getLabel())
				.nonEditableReasons(properties.getNonEditableReasons())
				.onValueChange(properties.getOnValueChange())
				.referenceClass(referenceClass)
				.value(properties.getValue())
				.build());
	}
}
