package tk.labyrinth.pandora.ui.component.object.custom;

import tk.labyrinth.pandora.ui.component.genericobject.GenericGenericObjectForm;

public interface GenericGenericObjectFormCustomizer {

	void customize(GenericGenericObjectForm objectForm);
}
