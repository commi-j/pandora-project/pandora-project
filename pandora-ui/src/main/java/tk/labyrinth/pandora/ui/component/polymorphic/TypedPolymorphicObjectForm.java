package tk.labyrinth.pandora.ui.component.polymorphic;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.polymorphic.registry.PolymorphicClassRegistry;
import tk.labyrinth.pandora.functionalvaadin.component.SelectRenderer;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class TypedPolymorphicObjectForm<T> implements ObjectForm<T> {

	private final BeanContext beanContext;

	private final ConverterRegistry converterRegistry;

	private final VerticalLayout layout = new VerticalLayout();

	private final PolymorphicClassRegistry polymorphicClassRegistry;

	@SmartAutowired
	private Class<?> javaClass;

	@PostConstruct
	private void postConstruct() {
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
	}

	@Override
	public Component asVaadinComponent() {
		return layout;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void render(Properties<T> parameters) {
		layout.removeAll();
		//
		Class<?> currentValueClass = parameters.getCurrentValue().getClass();
		//
		{
			layout.add(SelectRenderer.<Class>render(builder -> builder
					.items(polymorphicClassRegistry.getLinks(javaClass)
							.map(polymorphicLink -> (Class) polymorphicLink.getLeafClass())
							.toList()
							.sortBy(Class::getSimpleName)
							.asJava())
					.itemLabelGenerator(Class::getSimpleName)
					.label("Qualifier")
					.onValueChange(nextValue -> parameters.getOnValueChange().accept(
							converterRegistry.convert(GenericObject.empty(), (Class<? extends T>) nextValue)))
					.readOnly(!parameters.calcEditable())
					.title(StringUtils.emptyToNull(parameters.calcNonEmptyNonEditableReasons().mkString("\n")))
					.value(currentValueClass)
					.build()));
		}
		//
		{
			ObjectForm<T> objectForm = beanContext.getBeanFactory().getBean(
					TypeUtils.parameterize(ObjectForm.class, currentValueClass));
			//
			objectForm.render(Properties.<T>builder()
					.currentValue(parameters.getCurrentValue())
					.initialValue(parameters.getInitialValue())
					.nonEditableReasons(parameters.getNonEditableReasons())
					.onValueChange(parameters.getOnValueChange())
					.build());
			//
			layout.add(objectForm.asVaadinComponent());
		}
	}
}
