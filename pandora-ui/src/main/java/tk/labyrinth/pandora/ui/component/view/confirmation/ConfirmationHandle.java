package tk.labyrinth.pandora.ui.component.view.confirmation;

import lombok.Getter;
import lombok.NonNull;

import javax.annotation.Nullable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

@Deprecated
public class ConfirmationHandle<T> {

	@Getter
	@Nullable
	private Predicate<T> subscribePredicate = null;

	public <R> ConfirmationHandle<R> map(@NonNull Function<T, R> mappingFunction) {
		ConfirmationHandle<R> result = new ConfirmationHandle<>();
		//
		subscribe(value -> result.getSubscribePredicate().test(mappingFunction.apply(value)));
		//
		return result;
	}

	/**
	 * Predicate receives confirmed value and returns whether it was accepted or not.
	 *
	 * @param subscribePredicate non-null
	 */
	public void subscribe(@NonNull Predicate<T> subscribePredicate) {
		if (this.subscribePredicate != null) {
			throw new IllegalArgumentException("Already subscribed");
		}
		//
		this.subscribePredicate = subscribePredicate;
	}

	/**
	 * Consumer receives confirmed value, then it is considered accepted.
	 *
	 * @param subscribeConsumer non-null
	 */
	public void subscribeAlwaysAccepted(@NonNull Consumer<T> subscribeConsumer) {
		subscribe(next -> {
			subscribeConsumer.accept(next);
			//
			return true;
		});
	}

	/**
	 * Consumer receives confirmed value, then it is considered accepted.
	 *
	 * @param subscribeConsumer non-null
	 */
	public void subscribeAlwaysNotAccepted(@NonNull Consumer<T> subscribeConsumer) {
		subscribe(next -> {
			subscribeConsumer.accept(next);
			//
			return false;
		});
	}
}
