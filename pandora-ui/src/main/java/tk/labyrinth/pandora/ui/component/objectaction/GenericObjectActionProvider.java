package tk.labyrinth.pandora.ui.component.objectaction;

import com.vaadin.flow.component.Component;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;

public interface GenericObjectActionProvider {

	/**
	 * If non-null value is returned, this action would be rendered in a separate column.
	 * It's order among other actions is determined by the value, smallest to be leftmost.
	 * Value may not be negative.
	 *
	 * @return non-negative or null
	 */
	@Nullable
	Integer getStandaloneDistance();

	@Nullable
	Component provideActionForGenericObject(Context<GenericObject> context);

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	class Context<T> {

		@NonNull
		Datatype datatype;

		@Nullable
		T object;

		@NonNull
		ObjectModel objectModel;

		Runnable parentRefreshCallback;
	}
}
