package tk.labyrinth.pandora.ui.component.objecttable;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;
import tk.labyrinth.pandora.ui.domain.page.PandoraPage;
import tk.labyrinth.pandora.ui.domain.page.PandoraPageVaadinUtils;

@Bean
public class GenericObjectTableContributingInitializer extends GenericObjectsContributingInitializer {

	@Override
	protected List<GenericObject> contributeObjects() {
		return List.of(convertAndAddHardcoded(PandoraPage.builder()
				.handle(PandoraPageVaadinUtils.createVaadinComponentClassPageHandler(GenericObjectTablePage.class))
				.parametersModel(List.of(ObjectModelAttribute.builder()
						.datatype(Datatype.ofNonParameterized(JavaBaseTypeUtils.createDatatypeBaseReference(String.class)))
						.name("objectModelCode")
						.build()))
				.slug("pandora-generic-object-table")
				.build()));
	}
}
