package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class ToVaadinComponentRendererRegistry {

	private final ConverterRegistry converterRegistry;

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	@SmartAutowired
	private List<ToVaadinComponentRenderer<?>> renderers;

	@Nullable
	@SuppressWarnings("unchecked")
	public <T> ToVaadinComponentRenderer<T> findRenderer(Context context) {
		val selectionResult = selectRenderer(context);
		//
		return (ToVaadinComponentRenderer<T>) selectionResult.findHandler();
	}

	public <T> ToVaadinComponentRenderer<T> getRenderer(Context context) {
		ToVaadinComponentRenderer<T> result = findRenderer(context);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: context = %s".formatted(context));
		}
		return result;
	}

	@WithSpan
	public Component render(Context context, @Nullable Object value) {
		ToVaadinComponentRenderer<Object> renderer = getRenderer(context);
		//
		Try<?> convertedValueTry = Try.ofSupplier(() -> converterRegistry.convert(value, renderer.getParameterType()));
		//
		return convertedValueTry
				.map(convertedValue -> renderer.render(
						ToVaadinComponentRenderer.Context.builder()
								.datatype(context.getDatatype())
								.hints(context.getHints())
								.rendererRegistry(this)
								.build(),
						convertedValue))
				.recover(fault -> {
					// TODO
					return new Span("Conversion Failure (TODO)");
				})
				.get();
	}

	public HasSupportDistance.SelectionResult<
			ToVaadinComponentRenderer<?>,
			ToVaadinComponentRenderer.Context> selectRenderer(Context context) {
		return HasSupportDistance.selectHandler(
				renderers,
				ToVaadinComponentRenderer.Context.builder()
						.datatype(context.getDatatype())
						.hints(context.getHints())
						.rendererRegistry(this)
						.build());
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Context {

		@NonNull
		Datatype datatype;

		@NonNull
		List<Object> hints;

		public static class Builder {

			public Builder datatype(@NonNull Datatype datatype) {
				this.datatype = datatype;
				//
				return this;
			}

			public Builder datatype(@NonNull Type javaType) {
				return datatype(JavaBaseTypeUtils.createDatatypeFromJavaType(javaType));
			}
		}
	}
}
