package tk.labyrinth.pandora.ui.domain.store;

import com.nimbusds.jose.util.Pair;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import tk.labyrinth.pandora.functionalcomponents.advanced.GridWrapper;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRoute;
import tk.labyrinth.pandora.stores.domain.storeroute.StoreRouteRegistry;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStore;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectStoreRegistry;
import tk.labyrinth.pandora.ui.domain.applayout.ConfigurableAppLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Route(value = "pandora/stores", layout = ConfigurableAppLayout.class)
@Slf4j
public class PandoraStoresPage extends CssVerticalLayout {

	private final GenericObjectStoreRegistry genericObjectStoreRegistry;

	private final StoreRouteRegistry storeRouteRegistry;

	@PostConstruct
	private void postConstruct() {
		{
			addClassName(PandoraStyles.LAYOUT);
		}
		{
			List<GenericObjectStore> stores = genericObjectStoreRegistry.listStores();
			List<StoreRoute> storeRoutes = storeRouteRegistry.listStoreRoutes();
			val storeNameToRoutesMap = storeRoutes.flatMap(storeRoute -> genericObjectStoreRegistry
							.selectStore(storeRoute.getStoreConfigurationPredicate())
							.getResults()
							.map(result -> Pair.of(result.getConfiguration().getSlug(), storeRoute)))
					.groupBy(Pair::getLeft)
					.mapValues(list -> list.map(Pair::getRight));
			//
			GridWrapper<GenericObjectStore> grid = new GridWrapper<>(item -> item);
			{
				grid
						.addColumn(item -> item.getConfiguration().getSlug())
						.setHeader("Name")
						.setResizable(true);
				grid
						.addColumn(item -> item.getConfiguration().getDesignation())
						.setHeader("Designation")
						.setResizable(true);
				grid
						.addColumn(item -> item.getConfiguration().getKind())
						.setHeader("Kind")
						.setResizable(true);
				grid
						.addColumn(item -> item.getConfiguration().getPredicate())
						.setHeader("Predicate")
						.setResizable(true);
				grid
						.addComponentColumn(item -> GridUtils.renderValueList(
								storeNameToRoutesMap.get(item.getConfiguration().getSlug()).getOrElse(List.empty()),
								StoreRoute::getSlug))
						.setHeader("Routes")
						.setResizable(true);
			}
			{
				grid.setItems(stores);
			}
			add(grid);
		}
	}
}
