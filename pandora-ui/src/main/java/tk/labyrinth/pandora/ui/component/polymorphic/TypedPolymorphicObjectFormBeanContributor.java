package tk.labyrinth.pandora.ui.component.polymorphic;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.object.TypedObjectFormBeanContributor;

import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class TypedPolymorphicObjectFormBeanContributor extends UnaryParameterizedBeanContributor<ObjectForm<?>> {

	public static final int DISTANCE = TypedObjectFormBeanContributor.DISTANCE - 1;

	@Override
	protected Object doContributeBean(BeanFactory beanFactory, Class<? extends ObjectForm<?>> base, Type parameter) {
		Class<?> parameterClass = TypeUtils.getClass(parameter);
		//
		return beanFactory
				.withBean(parameterClass)
				.getBean(TypedPolymorphicObjectForm.class);
	}

	@Nullable
	@Override
	protected Integer doGetSupportDistance(
			BeanFactory beanFactory,
			Class<? extends ObjectForm<?>> base,
			Type parameter) {
		Class<?> parameterClass = TypeUtils.getClass(parameter);
		//
		return PolymorphicUtils.isPolymorphicRoot(parameterClass) ? DISTANCE : null;
	}
}
