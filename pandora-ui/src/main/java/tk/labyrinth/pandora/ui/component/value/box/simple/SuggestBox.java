package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import javax.annotation.CheckForNull;
import java.util.function.Consumer;
import java.util.function.Function;

@Deprecated // See SuggestBoxRenderer
@Slf4j
public class SuggestBox<T> implements RenderableView<SuggestBox.Properties<T>> {

	private final ComboBox<T> comboBox = new ComboBox<>();

	private boolean eventHandlingInProgress = false;

	private Properties<T> properties = Properties.<T>builder().build();

	private boolean renderInProgress = false;

	/**
	 * When we send value to front, it changes text which results in customValueSet event triggered.
	 * We want to ignore such events, so we use simple flag that is raised during rendering and is lowered
	 * upon customValueSet event received.
	 */
	private boolean valueSetByRender = false;

	{
		comboBox.setAllowCustomValue(true);
		comboBox.addCustomValueSetListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						comboBox.setValue(comboBox.getValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					if (valueSetByRender) {
						valueSetByRender = false;
					} else {
						Consumer<String> onTextChange = properties.getOnTextChange();
						if (onTextChange != null) {
							onTextChange.accept(event.getDetail());
						}
					}
				}
			}
		});
		comboBox.addValueChangeListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						comboBox.setValue(event.getOldValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					Consumer<T> onValueChange = properties.getOnValueChange();
					if (onValueChange != null) {
						onValueChange.accept(event.getValue());
					}
				}
			}
		});
	}

	private void doRender(Properties<T> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		valueSetByRender = true;
		//
		comboBox.setItemLabelGenerator(properties.getToStringRenderFunction() != null
				? item -> properties.getToStringRenderFunction().apply(item)
				: String::valueOf);
		{
			Function<String, List<? extends T>> suggestFunction = properties.getSuggestFunction();
			if (suggestFunction != null) {
				comboBox.setItems(new CallbackDataProvider<>(
						query -> {
							{
								// We need to call #getOffset() and #getLimit() to avoid Vaadin internal check failure.
								int offset = query.getOffset();
								int limit = query.getLimit();
							}
							String filter = query.getFilter().orElseThrow();
							//
							List<? extends T> items = suggestFunction.apply(filter);
							//
							logger.debug("fetch: filter = {}, items = {}", filter, items);
							//
							return ObjectUtils.infer(items.toJavaStream());
						},
						query -> suggestFunction.apply(query.getFilter().orElseThrow()).size()));
			} else {
				comboBox.setItems(java.util.List.of());
			}
		}
		comboBox.setLabel(properties.getLabel());
		comboBox.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				comboBox,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		comboBox.setValue(properties.getValue());
	}

	@Override
	public Component asVaadinComponent() {
		return comboBox;
	}

	@Override
	public void render(Properties<T> properties) {
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties<T> {
//		@lombok.Builder.Default
//		@NonNull
//		List<T> items = List.empty();

		@CheckForNull
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@CheckForNull
		Consumer<String> onTextChange;

		@CheckForNull
		Consumer<T> onValueChange;

		@CheckForNull
		Function<@NonNull String, @NonNull List<? extends T>> suggestFunction;
		//
		// FIXME: Not supported for now.
//		@NonNull
//		String text = "";

		Function<T, String> toStringRenderFunction;

		@CheckForNull
		T value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
