package tk.labyrinth.pandora.ui.component.value.box.mutable;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalcomponents.box.DurationBoxRenderer;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalComponent;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.component.value.box.simple.FunctionalSimpleValueBox;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

import java.time.Duration;

@LazyComponent
@RequiredArgsConstructor
public class DurationValueBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		return new SimpleMutableValueBox<>(new FunctionalSimpleValueBox<>(
				FunctionalComponent.create(
						SimpleValueBox.Properties.<Duration>builder().build(),
						(parameters, sink) -> DurationBoxRenderer.render(builder -> builder
								.label(parameters.getLabel())
								.onValueChange(parameters.getOnValueChange())
								.value(parameters.getValue())
								.build())),
				Duration.class));
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), Duration.class)
				? ToStringRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
