package tk.labyrinth.pandora.ui.component.table.search;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.util.Objects;
import java.util.Optional;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class SearchChannel {

	private final AdvancedTableViewSearcher advancedTableViewSearcher;

	private final Observable<Pair<@Nullable SearchParameters, @Nullable SearchState>> observable =
			Observable.notInitialized();

	@PostConstruct
	private void postConstruct() {
		observable.getFlux()
				.filter(pair -> pair.getLeft() != null)
				.map(Pair::getLeft)
				// TODO: Find out why distinct works and untilChanged not.
				.distinct() // TODO: Write a test to ensure A, B, A sequence is processed correctly (e.g. when we have the same value again)
				.subscribe(nextParameters -> {
					Observable<SearchState> searchStateObservable = advancedTableViewSearcher.initiateSearch(nextParameters);
					//
					searchStateObservable.subscribe(nextSearchState -> observable.update(pair ->
							Objects.equals(pair.getLeft(), nextParameters)
									? Pair.of(nextParameters, nextSearchState)
									: pair));
				});
	}

	public Flux<Optional<SearchState>> getFlux() {
		return observable.getFlux()
				.map(pair -> Optional.ofNullable(pair.getRight()))
				.distinct();
	}

	public void setParameters(@Nullable SearchParameters parameters) {
		observable.set(Pair.of(parameters, null));
	}
}
