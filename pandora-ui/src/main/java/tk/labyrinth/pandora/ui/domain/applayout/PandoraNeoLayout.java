package tk.labyrinth.pandora.ui.domain.applayout;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.theme.lumo.Lumo;
import tk.labyrinth.pandora.functionalcomponents.vaadin.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Margin;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.PlaceSelf;

public class PandoraNeoLayout extends CssGridLayout implements RouterLayout {

	private boolean sidebarCollapsed = false;

	{
		{
			setGridTemplateColumns("auto 1fr");
			//
			setHeightFull();
		}
		{
			CssGridLayout sidebar = new CssGridLayout();
			{
				sidebar.setGridTemplateAreas(
						"top",
						"centre",
						".",
						"bottom");
				sidebar.setGridTemplateRows("auto auto 1fr auto");
				//
				sidebar.getElement().getThemeList().add(Lumo.DARK);
			}
			{
				Button button = ButtonRenderer.render(builder -> builder
						.icon(VaadinIcon.ANGLE_DOUBLE_LEFT.create())
						.build());
				//
				button.setHeight("2em");
				button.setWidth("2em");
				StyleUtils.setCssProperty(button, Margin.of("0.5em"));
				//
				CssGridItem.setJustifySelf(button, PlaceSelf.END);
				//
				button.addClickListener(event -> {
					sidebarCollapsed = !sidebarCollapsed;
					//
					update(sidebar, button);
				});
				//
				sidebar.add(button, "bottom");
				//
				update(sidebar, button);
			}
			//
			add(sidebar);
		}
	}

	public void update(CssGridLayout sidebar, Button button) {
		sidebar.setWidth(sidebarCollapsed ? "3em" : "10em");
		//
		button.setIcon(sidebarCollapsed ? VaadinIcon.ANGLE_DOUBLE_RIGHT.create() : VaadinIcon.ANGLE_DOUBLE_LEFT.create());
	}
}
