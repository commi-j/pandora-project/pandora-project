package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Anchor;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

import java.net.URL;

@LazyComponent
@RequiredArgsConstructor
public class UrlToVaadinComponentRenderer extends ToVaadinComponentRendererBase<URL> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, URL value) {
		return new Anchor(value.toString(), value.toString());
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), URL.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}
}
