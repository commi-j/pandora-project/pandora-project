package tk.labyrinth.pandora.ui.component.objectaction.basic;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;

public class ObjectActionUtils {

	public static Button createButton(
			String name,
			@Nullable String text,
			@Nullable VaadinIcon icon,
			Runnable action,
			@Nullable String title,
			@Nullable String nonEditableReason) {
		Button button = new Button(text, icon != null ? icon.create() : null, event -> action.run());
		//
		if (nonEditableReason != null) {
			button.setEnabled(false);
		}
		button.getElement().setAttribute("name", name);
		button.addThemeVariants(ButtonVariant.LUMO_SMALL);
		button.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		//
		{
			String titleToUse;
			//
			if (title != null) {
				if (nonEditableReason != null) {
					titleToUse = "%s\n%s".formatted(title, nonEditableReason);
				} else {
					titleToUse = title;
				}
			} else {
				titleToUse = nonEditableReason;
			}
			//
			if (titleToUse != null) {
				ElementUtils.setTitle(button, titleToUse);
			}
		}
		//
		StyleUtils.setMargin(button, "0px");
		return button;
	}
}
