package tk.labyrinth.pandora.ui.routing.routetarget;

import com.vaadin.flow.component.Component;

import java.util.function.Consumer;

@Deprecated
public class SetParameterHookHandler {

	private static final ThreadLocal<Consumer<Component>> componentConsumerThreadLocal = new ThreadLocal<>();

	/**
	 * Goes second.
	 *
	 * @param component non-null
	 */
	public static void setComponent(Component component) {
		Consumer<Component> componentConsumer = componentConsumerThreadLocal.get();
		//
		if (componentConsumer == null) {
			throw new IllegalStateException();
		}
		//
		componentConsumerThreadLocal.set(null);
		//
		componentConsumer.accept(component);
	}

	/**
	 * Goes first.
	 *
	 * @param componentConsumer non-null
	 */
	public static void setComponentConsumer(Consumer<Component> componentConsumer) {
		Consumer<Component> currentComponentConsumer = componentConsumerThreadLocal.get();
		//
		if (currentComponentConsumer != null) {
			throw new IllegalStateException();
		}
		//
		componentConsumerThreadLocal.set(componentConsumer);
	}
}
