package tk.labyrinth.pandora.ui.component.polymorphic;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import tk.labyrinth.pandora.bean.contributor.PlainClassBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.context.BeanFactoryThreadLocalContext;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicConstants;
import tk.labyrinth.pandora.datatypes.polymorphic.registry.PolymorphicClassRegistry;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectForm;

import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class PolymorphicGenericObjectFormBeanContributor implements PlainClassBeanContributor<GenericObjectForm> {

	public static final int DISTANCE = PARENT_SUPPORT_DISTANCE - 1;

	private final AutowireCapableBeanFactory autowireCapableBeanFactory;

	private final PolymorphicClassRegistry polymorphicClassRegistry;

	@Override
	public GenericObjectForm doContributeBean(BeanFactory beanFactory, Type type) {
		BeanFactoryThreadLocalContext.append(beanFactory);
		try {
			return autowireCapableBeanFactory.createBean(PolymorphicGenericObjectForm.class);
		} finally {
			BeanFactoryThreadLocalContext.remove();
		}
	}

	@Nullable
	@Override
	public Integer doGetSupportDistance(BeanFactory beanFactory, Type type) {
		Integer result;
		{
			ObjectModel objectModel = beanFactory.findBean(ObjectModel.class);
			//
			result = objectModel != null && objectModel.getTagsOrEmpty().contains(PolymorphicConstants.TAG)
					? DISTANCE
					: null;
		}
		return result;
	}
}
