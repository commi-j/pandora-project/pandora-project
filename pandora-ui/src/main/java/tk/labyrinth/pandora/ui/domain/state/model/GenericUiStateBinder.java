package tk.labyrinth.pandora.ui.domain.state.model;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.reactive.observable.SimpleDisposable;
import tk.labyrinth.pandora.ui.domain.state.UiStateAttributeHandler;
import tk.labyrinth.pandora.ui.domain.state.UiStateHandler;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

@PrototypeScopedComponent
@RequiredArgsConstructor
public class GenericUiStateBinder {

	private final UiStateHandler uiStateHandler;

	/**
	 * @param attributeHandlers  list of attributeHandlers to be registered;
	 * @param localStateConsumer consumes next local state when UI state changes;
	 * @param localStateFlux     reports when local state is changed to update UI state;
	 */
	public Disposable bind(
			List<? extends UiStateAttributeHandler> attributeHandlers,
			Consumer<GenericObject> localStateConsumer,
			Flux<GenericObject> localStateFlux) {
		{
			attributeHandlers.forEach(attributeHandler ->
					uiStateHandler.registerAttributeHandler(null, attributeHandler));
		}
		{
			Disposable disposable = uiStateHandler.getStateFlux()
					.subscribe(nextUiState -> {
						// TODO: Check recursion.
						//
						localStateConsumer.accept(GenericObject.of(attributeHandlers
								.map(attributeHandler -> nextUiState.findAttribute(attributeHandler.getAttributeName()))
								.filter(Objects::nonNull)));
					});
			localStateFlux
					.doOnTerminate(() -> {
						disposable.dispose();
						attributeHandlers.forEach(uiStateHandler::unregisterAttributeHandler);
					})
					.subscribe(nextState -> {
						// TODO: Check recursion.
						//
						uiStateHandler.setAttributes(attributeHandlers.map(attributeHandler -> Pair.of(
								attributeHandler.getAttributeName(),
								nextState.findAttributeValue(attributeHandler.getAttributeName()))));
					});
		}
		return SimpleDisposable.of(() -> attributeHandlers.forEach(uiStateHandler::unregisterAttributeHandler));
	}

	public Disposable bindAttribute(
			UiStateAttributeHandler attributeHandler,
			Consumer<Optional<ValueWrapper>> valueOptionalConsumer,
			Flux<Optional<ValueWrapper>> valueOptionalFlux) {
		{
			uiStateHandler.registerAttributeHandler(null, attributeHandler);
		}
		{
			Disposable disposable = uiStateHandler.getStateFlux()
					.subscribe(nextUiState -> {
						// TODO: Check recursion.
						//
						valueOptionalConsumer.accept(Optional.ofNullable(
								nextUiState.findAttributeValue(attributeHandler.getAttributeName())));
					});
			valueOptionalFlux
					.doOnTerminate(() -> {
						disposable.dispose();
						uiStateHandler.unregisterAttributeHandler(attributeHandler);
					})
					.subscribe(nextValueOptional -> {
						// TODO: Check recursion.
						//
						uiStateHandler.setAttributes(List.of(Pair.of(
								attributeHandler.getAttributeName(),
								nextValueOptional.orElse(null))));
					});
		}
		return SimpleDisposable.of(() -> uiStateHandler.unregisterAttributeHandler(attributeHandler));
	}
}
