package tk.labyrinth.pandora.ui.domain.route;

import com.vaadin.flow.router.internal.HasUrlParameterFormat;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import io.vavr.collection.List;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

@Bean
public class PandoraRouteVaadinTool implements VaadinServiceInitListener {

	private ApplicationRouteRegistry applicationRouteRegistry;

	public String getPath(Class<?> vaadinPageClass) {
		return List.ofAll(applicationRouteRegistry.getRegisteredRoutes())
				.filter(routeData -> routeData.getNavigationTarget() == vaadinPageClass)
				.headOption()
				.map(routeData1 -> composePath(routeData1.getTemplate()))
				.get();
	}

	@Override
	public void serviceInit(ServiceInitEvent event) {
		applicationRouteRegistry = ApplicationRouteRegistry.getInstance(event.getSource().getContext());
	}

	public static String composePath(String pathTemplate) {
		return pathTemplate.replaceAll("/?%s\\?".formatted(HasUrlParameterFormat.PARAMETER), "");
	}
}
