package tk.labyrinth.pandora.ui.tool;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.Route;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.uiapplication.layout.PandoraRootLayout;

import java.util.function.Consumer;

@RequiredArgsConstructor
@Route(value = "tools/store-access", layout = PandoraRootLayout.class)
public class StoreAccessPage extends FunctionalPage<StoreAccessPage.State> {

	@Override
	protected State getInitialProperties() {
		return State.builder().build();
	}

	@Override
	protected Component render(State state, Consumer<State> sink) {
		return new Div();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

	}
}
