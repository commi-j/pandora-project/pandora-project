package tk.labyrinth.pandora.ui.domain.page;

import com.vaadin.flow.component.Component;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;

public class PandoraPageVaadinUtils {

	public static String createVaadinComponentClassPageHandler(Class<? extends Component> vaadinComponentClass) {
		return "vaadinComponent:%s".formatted(ClassUtils.getSignature(vaadinComponentClass));
	}

	@SuppressWarnings("unchecked")
	public static Class<? extends Component> getVaadinComponentClass(String vaadinComponentClassPageHandler) {
		// FIXME: Get expects binary name, but we feed qualified. This may misfire one day.
		return (Class<? extends Component>) ClassUtils.get(ClassSignature
				.from(vaadinComponentClassPageHandler.substring("vaadinComponent:".length()))
				.getBinaryName());
	}

	public static boolean isVaadinComponentClassPageHandler(String pageHandler) {
		return pageHandler.startsWith("vaadinComponent:");
	}
}
