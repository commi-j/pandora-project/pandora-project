package tk.labyrinth.pandora.ui.component.objectaction.basic;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.Order;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.genericobject.GenericObjectUidReference;
import tk.labyrinth.pandora.ui.component.objectaction.GenericObjectActionProvider;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

@Bean
@Order(DeleteObjectActionProvider.PRIORITY)
@RequiredArgsConstructor
public class DeleteObjectActionProvider implements GenericObjectActionProvider {

	public static final int PRIORITY = EditObjectActionProvider.PRIORITY + 1;

	private final GenericObjectManipulator genericObjectManipulator;

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return null;
	}

	@Nullable
	@Override
	public Component provideActionForGenericObject(Context<GenericObject> context) {
		Component result;
		{
			GenericObject object = context.getObject();
			//
			if (object != null) {
				result = ObjectActionUtils.createButton(
						"delete-object-button",
						null,
						VaadinIcon.TRASH,
						() -> ConfirmationViews
								.showTextDialog("Delete?")
								.subscribeAlwaysAccepted(next -> {
									genericObjectManipulator.delete(GenericObjectUidReference.from(object));
									//
									context.getParentRefreshCallback().run();
								}),
						"Delete Object",
						genericObjectManipulator.canManipulate(object)
								? null
								: "No StoreRoute to delete this object specified");
			} else {
				result = null;
			}
		}
		return result;
	}
}
