package tk.labyrinth.pandora.ui.domain.applayout;

import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.reactive.observable.Observable;

// TODO: Replace with picking based on priority.
@Bean
public class AppLayoutConfigurationRegistry {

	private final Observable<PandoraAppLayoutConfiguration> appLayoutConfigurationObservable =
			Observable.notInitialized();

	public Observable<PandoraAppLayoutConfiguration> getAppLayoutConfigurationObservable() {
		return appLayoutConfigurationObservable;
	}

	public void setAppLayoutConfiguration(PandoraAppLayoutConfiguration appLayoutConfiguration) {
		appLayoutConfigurationObservable.set(appLayoutConfiguration);
	}
}
