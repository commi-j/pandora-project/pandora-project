package tk.labyrinth.pandora.ui.component.objecttable;

import com.vaadin.flow.router.Location;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.navigation.NavigationTargetHandler;

@LazyComponent
@RequiredArgsConstructor
public class ObjectTableNavigationTargetHandler implements NavigationTargetHandler<ObjectTableNavigationTarget> {

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	@Override
	public Location toLocation(ObjectTableNavigationTarget navigationTarget) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
