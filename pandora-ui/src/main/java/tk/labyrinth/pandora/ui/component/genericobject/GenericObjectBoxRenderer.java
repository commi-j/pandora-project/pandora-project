package tk.labyrinth.pandora.ui.component.genericobject;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.functionalcomponents.tool.FocusHandler;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @see ModelAwareGenericObjectBoxRenderer
 */
public class GenericObjectBoxRenderer {

	public static TextField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextField render(Parameters parameters) {
		TextField textField = new TextField();
		{
			textField.setLabel(parameters.label());
			textField.setReadOnly(true);
			textField.setSuffixComponent(ButtonRenderer.render(builder -> builder
					.icon(VaadinIcon.EDIT.create())
					.onClick(event -> {
						EditableGenericObject initialValue = parameters.initialValue() != null
								? EditableGenericObject.from(parameters.initialValue())
								: EditableGenericObject.empty();
						//
						Observable<EditableGenericObject> currentValueObservable = Observable.withInitialValue(
								parameters.currentValue() != null
										? EditableGenericObject.from(parameters.currentValue())
										: initialValue);
						//
						ConfirmationViews
								.showFunctionDialog(
										currentValueObservable,
										nextState -> {
											Component view = EditableGenericObjectViewRenderer.render(
													EditableGenericObjectViewRenderer.Properties.builder()
															.currentValue(nextState)
															.initialValue(initialValue)
															.nonEditableReasons(parameters.nonEditableReasons())
															.onValueChange(currentValueObservable::set)
															.build());
											StyleUtils.setWidth(view, "50em");
											return view;
										})
								.subscribe(success -> {
									EditableGenericObject currentValue = currentValueObservable.get();
									//
//									return currentValue.
									return false;
								});
					})
					.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
					.build()));
		}
		{
			String renderedValue = parameters.currentValue() != null
					? parameters.currentValue().getAttributes()
					// FIXME: Do we need it?
//					.map(attribute -> "%s : %s".formatted(attribute.getName(), attribute.getValue()))
					.mkString(", ")
					: "";
			textField.setValue(renderedValue != null ? renderedValue : "");
		}
		{
			// Tricky solution to preserve focus on rerender.
			textField.addFocusListener(event -> FocusHandler.onFocusChange(event.getSource()));
		}
		return textField;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		GenericObject currentValue;

		@Nullable
		GenericObject initialValue;

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<GenericObject> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
