package tk.labyrinth.pandora.ui.component.objecttable;

import io.vavr.collection.List;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.origin.PandoraOriginUtils;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.rootobject.RootObjectModelFeature;
import tk.labyrinth.pandora.stores.listener.ObjectChangeEvent;
import tk.labyrinth.pandora.stores.listener.ObjectChangeListener;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;
import tk.labyrinth.pandora.ui.domain.page.SlugPandoraPageReference;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;
import tk.labyrinth.pandora.ui.domain.route.PandoraRouteParameterMapping;

@Bean
public class PandoraRouteContributingObjectModelChangeListener extends ObjectChangeListener<ObjectModel> {

	@SmartAutowired
	private TypedObjectManipulator<PageRoute> pageRouteManipulator;

	@Override
	protected void onObjectChange(ObjectChangeEvent<ObjectModel> event) {
		if (event.hasPreviousObject()) {
			pageRouteManipulator.delete(PandoraOriginUtils.createPredicate(event.getPrimaryReference()));
		} else if (event.hasNextObject()) {
			ObjectModel objectModel = event.getNextObjectOrFail();
			//
			RootObjectModelFeature rootFeature = objectModel.findFeature(RootObjectModelFeature.class);
			//
			if (rootFeature != null) {
				pageRouteManipulator.createWithAdjustment(
						PageRoute.builder()
								.pageReference(SlugPandoraPageReference.of("pandora-generic-object-table"))
								.parameterMappings(List.of(
										PandoraRouteParameterMapping.builder()
												.fromSchema("CONSTANT:%s".formatted(objectModel.getCode()))
												.to("objectModelCode")
												.build()))
								.path("pandora/objects/%s".formatted(objectModel.getCode()))
								.build(),
						genericObject -> genericObject.withAddedAttributes(
								PandoraOriginUtils.createAttribute(event.getPrimaryReference()),
								PandoraIndexConstants.ATTRIBUTE));
			}
		}
	}
}
