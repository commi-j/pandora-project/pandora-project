package tk.labyrinth.pandora.ui.component.table;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.grid.ColumnReorderEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.shared.Registration;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.functionalvaadin.component.CheckboxRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanCache;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanDataWrapper;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.component.selector.SelectorBoxRenderer;
import tk.labyrinth.pandora.ui.component.table.renderedobject.RenderedObject;
import tk.labyrinth.pandora.ui.component.table.search.SearchChannel;
import tk.labyrinth.pandora.ui.component.table.search.SearchObject;
import tk.labyrinth.pandora.ui.component.table.search.SearchParameters;
import tk.labyrinth.pandora.ui.component.table.search.SearchResult;
import tk.labyrinth.pandora.ui.component.table.search.SearchState;
import tk.labyrinth.pandora.ui.paginator.Paginator;
import tk.labyrinth.pandora.ui.style.PandoraStyles;
import tk.labyrinth.pandora.ui.tool.otel.OpentelemetrySpanTable;

import java.time.Instant;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;

@PrototypeScopedComponent
@RequiredArgsConstructor
@Slf4j
public class AdvancedTableView extends Composite<Component> {

	private final CssVerticalLayout columnsConfigurationLayout = new CssVerticalLayout();

	private final Span fetchTimeWindowSpan = new Span();

	private final CssHorizontalLayout filterWrapper = new CssHorizontalLayout();

	private final Grid<RenderedObject> grid = new Grid<>();

	private final Paginator paginator = new Paginator();

	private final SearchChannel searchChannel;

	private final SelectorBoxRenderer selectorBoxRenderer;

	private final SpanCache spanCache;

	private final Observable<State> stateObservable = Observable.notInitialized();

	@Nullable
	private Registration fetchTimeWindowSpanRegistration = null;

	private Consumer<ColumnReorderEvent<?>> onColumnReorder;

	private Runnable onRefresh;

	private void onStateChange(State state, Consumer<State> sink) {
		Parameters parameters = state.parameters();
		SearchObject searchObject = state.searchObject();
		SearchState searchState = searchObject.searchState();
		SearchResult searchResult = searchState != null ? searchState.result() : null;
		List<RenderedObject> items = searchResult != null ? searchResult.objectsTry().getOrElse(List.empty()) : null;
		//
		{
			{
				{
					filterWrapper.removeAll();
					//
					Component selectorBox = selectorBoxRenderer.render(SelectorBoxRenderer.Parameters.builder()
							.onValueChange(nextValue -> parameters.selectionModel().onPredicateChange().accept(
									!((JunctionPredicate) nextValue).getOperands().isEmpty()
											? nextValue
											: null))
							.value(Optional.ofNullable(parameters.selectionModel().predicate())
									.orElse(Predicates.and()))
							.build());
					CssFlexItem.setFlexGrow(selectorBox, 1);
					filterWrapper.addComponentAsFirst(selectorBox);
				}
				{
					onRefresh = () -> sink.accept(state.withSearchObject(state.searchObject()
							.withSearchParameters(state.searchObject().searchParameters().withRefreshedAt(Instant.now()))));
				}
				{
					columnsConfigurationLayout.removeAll();
					parameters.columnDescriptors().forEach(columnDescriptor -> columnsConfigurationLayout.add(
							CheckboxRenderer.render(builder -> builder
									.label(columnDescriptor.getDisplayName())
									.onValueChange(nextValue -> parameters.onColumnVibisibilyChange().accept(
											AdvancedTableColumnVisibility.of(
													columnDescriptor.getAttributeName(),
													nextValue)))
									.value(columnDescriptor.getVisible())
									.build())));
					//
					onColumnReorder = event -> parameters.onColumnOrderChange().accept(
							List.ofAll(event.getColumns()).map(Grid.Column::getKey));
				}
			}
		}
		{
			grid.removeAllColumns();
			//
			parameters.columnDescriptors()
					.filter(AdvancedTableColumnDescriptor::getVisible)
					.forEach(columnDescriptor -> {
						Grid.Column<RenderedObject> column = grid
								.addComponentColumn(item -> item.getAttributes().get(columnDescriptor.getAttributeName())
										.getOrElse(Div::new))
								.setFlexGrow(columnDescriptor.getFlexGrow() != null ? columnDescriptor.getFlexGrow() : 1)
								.setKey(columnDescriptor.getAttributeName())
								.setResizable(true);
						//
						if (columnDescriptor.getHeader() != null) {
							column.setHeader(columnDescriptor.getHeader());
						} else {
							column.setHeader(columnDescriptor.getDisplayName());
						}
					});
			//
			grid.setItems(items != null ? items.asJava() : java.util.List.of());
		}
		{
			updateFetchTimeWindowSpan(
					fetchTimeWindowSpan,
					searchObject.computeTimeWindow(),
					searchResult != null
							? searchResult.objectsTry().isFailure() ? searchResult.objectsTry().getCause() : null
							: null,
					searchState != null ? searchState.spanIdPair() : null);
			//
			paginator.render(Paginator.Properties.builder()
					.onSelectedPageIndexChange(parameters.pageModel().onSelectedPageIndexChange())
					.pageCount(searchResult != null ? searchResult.pageCount() : null)
					.selectedPageIndex(parameters.pageModel().selectedPageIndex())
					.build());
		}
	}

	private void updateFetchTimeWindowSpan(
			Span vaadinSpan,
			@Nullable TimeWindow fetchTimeWindow,
			@Nullable Throwable fault,
			@Nullable Pair<String, String> otelSpanIdPair) {
		{
			if (fetchTimeWindow != null) {
				if (fetchTimeWindow.getFinishedAt() != null) {
					String text;
					{
						if (fault != null) {
							text = "Fetch failed at %s".formatted(fetchTimeWindow.getFinishedAt());
						} else {
							text = "Fetch finished at %s".formatted(fetchTimeWindow.getFinishedAt());
						}
					}
					vaadinSpan.setText(text);
					//
					ElementUtils.setTitle(vaadinSpan, "It took %s ms".formatted(fetchTimeWindow.getDuration().toMillis()));
				} else {
					vaadinSpan.setText("Fetch started at %s".formatted(fetchTimeWindow.getStartedAt()));
					//
					ElementUtils.setTitle(vaadinSpan, null);
				}
			} else {
				vaadinSpan.setText("No fetch");
				//
				ElementUtils.setTitle(vaadinSpan, null);
			}
		}
		{
			if (fetchTimeWindowSpanRegistration != null) {
				fetchTimeWindowSpanRegistration.remove();
				fetchTimeWindowSpanRegistration = null;
			}
			//
			if (otelSpanIdPair != null) {
				fetchTimeWindowSpanRegistration = vaadinSpan.addClickListener(event -> {
					if (event.isAltKey()) {
						SpanDataWrapper spanDataWrapper = spanCache.getSpanDataWrapper(otelSpanIdPair.getRight());
						//
						OpentelemetrySpanTable spanTable = new OpentelemetrySpanTable();
						{
							spanTable.setHeight("90vh");
							spanTable.setWidth("90vw");
						}
						{
							spanTable.setParameters(builder -> builder
									.rootItems(List.of(spanDataWrapper))
									.build());
						}
						Dialogs.show(spanTable);
					}
				});
			}
		}
	}

	@Override
	protected Component initContent() {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				CssHorizontalLayout header = new CssHorizontalLayout();
				{
					header.setAlignItems(AlignItems.BASELINE);
				}
				{
					{
						// Filter Box.
						//
						CssFlexItem.setFlexGrow(filterWrapper, 1);
						header.add(filterWrapper);
					}
					{
						// Refresh & Columns Configuration Buttons.
						//
						MenuBar headerMenuBar = new MenuBar();
						{
							headerMenuBar.addThemeVariants(
									MenuBarVariant.LUMO_ICON,
									MenuBarVariant.LUMO_TERTIARY);
						}
						{
							{
								headerMenuBar.addItem(VaadinIcon.REFRESH.create(), event -> onRefresh.run());
							}
							{
								MenuItem columnsConfigurationMenuItem = headerMenuBar.addItem(
										VaadinIcon.COG.create());
								{
									CssVerticalLayout menuItemLayout = new CssVerticalLayout();
									{
										menuItemLayout.addClassNames(PandoraStyles.LAYOUT);
									}
									{
										menuItemLayout.add("Columns");
										menuItemLayout.add(columnsConfigurationLayout);
									}
									columnsConfigurationMenuItem.getSubMenu().add(menuItemLayout);
								}
							}
						}
						header.add(headerMenuBar);
					}
				}
				layout.add(header);
			}
			{
				grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
				//
				grid.setColumnReorderingAllowed(true);
				grid.setSelectionMode(Grid.SelectionMode.MULTI);
				//
				grid.addColumnReorderListener(event -> onColumnReorder.accept(event));
				//
				CssFlexItem.setFlexGrow(grid, 1);
				layout.add(grid);
			}
			{
				CssGridLayout footer = new CssGridLayout();
				{
					footer.setAlignItems(tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.AlignItems.BASELINE);
					footer.setGridTemplateColumns("1fr 1fr 1fr");
				}
				{
					{
						footer.add(fetchTimeWindowSpan);
					}
					{
						footer.add(paginator);
					}
				}
				layout.add(footer);
			}
		}
		{
			stateObservable.subscribe(this::onStateChange);
		}
		{
			stateObservable
					.getFlux()
					.map(State::searchObject)
					.map(SearchObject::searchParameters)
					.subscribe(searchChannel::setParameters);
			searchChannel.getFlux().subscribe(nextSearchState -> stateObservable.update(state ->
					state.withSearchObject(state.searchObject().withSearchState(nextSearchState.orElse(null)))));
		}
		return layout;
	}

	public void setParameters(Parameters parameters) {
		stateObservable.set(State.builder()
				.parameters(parameters)
				.searchObject(SearchObject.builder()
						.searchParameters(SearchParameters.builder()
								.pageIndex(parameters.pageModel().selectedPageIndex())
								.pageSize(20)
								.predicate(parameters.selectionModel().predicate())
								.refreshedAt(parameters.refreshedAt())
								.searchFunction(parameters.searchFunction())
								.build())
						.searchState(null)
						.build())
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		List<AdvancedTableColumnDescriptor> columnDescriptors;

		IntFunction<AdvancedTableSearchParameters> countFunction;

		@NonNull
		Consumer<List<String>> onColumnOrderChange;

		@NonNull
		Consumer<AdvancedTableColumnVisibility> onColumnVibisibilyChange;

		@NonNull
		AdvancedTablePageModel pageModel;

		@Nullable
		Instant refreshedAt;

		Function<AdvancedTableSearchParameters, AdvancedTableSearchResult> searchFunction;

		@NonNull
		AdvancedTableSelectionModel selectionModel;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
		Parameters parameters;

		@NonNull
		SearchObject searchObject;
	}
}
