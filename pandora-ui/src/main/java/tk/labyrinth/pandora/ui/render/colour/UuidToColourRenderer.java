package tk.labyrinth.pandora.ui.render.colour;

import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.stream.IntStream;

// TODO: Rework into md5 or other hash?
// https://stackoverflow.com/questions/38394240/how-to-generate-color-code-with-uuid-in-java
@Service
public class UuidToColourRenderer implements ToColourRenderer<UUID> {

	@Nullable
	@Override
	public String renderNonNull(UUID value) {
		return toColour(value);
	}

	private static byte[] toBytes(UUID uuid) {
		long leastSignificantBits = uuid.getLeastSignificantBits();
		long mostSignificantBits = uuid.getMostSignificantBits();
		return ByteBuffer.allocate(16).putLong(leastSignificantBits).putLong(mostSignificantBits).array();
	}

	private static String toColour(UUID id) {
		byte[] bytes = toBytes(id);
		//
		int red = (int) Math.abs(IntStream.range(0, 5)
				.map(index -> bytes[index])
				.map(value -> value != 0 ? value : 1)
				.mapToLong(value -> value)
				.reduce(Math::multiplyExact)
				.orElse(0) % 255);
		int green = (int) Math.abs(IntStream.range(5, 10)
				.map(index -> bytes[index])
				.map(value -> value != 0 ? value : 1)
				.mapToLong(value -> value)
				.reduce(Math::multiplyExact)
				.orElse(0) % 255);
		int blue = (int) Math.abs(IntStream.range(10, 15)
				.map(index -> bytes[index])
				.map(value -> value != 0 ? value : 1)
				.mapToLong(value -> value)
				.reduce(Math::multiplyExact)
				.orElse(0) % 255);
		//
		String redHex = Integer.toHexString(red);
		String greenHex = Integer.toHexString(green);
		String blueHex = Integer.toHexString(blue);
		//
		return "#%s%s%s".formatted(
				redHex.length() == 1 ? "0" + redHex : redHex,
				greenHex.length() == 1 ? "0" + greenHex : greenHex,
				blueHex.length() == 1 ? "0" + blueHex : blueHex);
	}
}
