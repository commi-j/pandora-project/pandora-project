package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.IntegerField;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.function.Consumer;

@Slf4j
public class IntegerBox implements SimpleValueBox<Integer> {

	private final IntegerField integerField = new IntegerField();

	private boolean eventHandlingInProgress = false;

	private Properties<Integer> properties;

	private boolean renderInProgress = false;

	{
		{
			// Overriding :host { width: 8em; }
			integerField.setWidth("initial");
		}
		{
			integerField.addValueChangeListener(event -> {
				if (!renderInProgress) {
					if (!eventHandlingInProgress) {
						eventHandlingInProgress = true;
						try {
							integerField.setValue(event.getOldValue());
						} finally {
							eventHandlingInProgress = false;
						}
						//
						Consumer<Integer> onValueChange = properties.getOnValueChange();
						if (onValueChange != null) {
							onValueChange.accept(event.getValue());
						}
					}
				}
			});
		}
	}

	private void doRender(Properties<Integer> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		integerField.setLabel(properties.getLabel());
		integerField.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				integerField,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		integerField.setValue(properties.getValue());
	}

	@Override
	public Component asVaadinComponent() {
		return integerField;
	}

	@Override
	public void render(Properties<Integer> properties) {
		if (renderInProgress) {
			throw new IllegalStateException("Invoked render while already rendering");
		}
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}
}
