package tk.labyrinth.pandora.ui.domain.objectview;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.json.JsonEditorRenderer;

import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class JsonObjectViewRenderer {

	private final ConverterRegistry converterRegistry;

	// FIXME: We use this over CReg since the latter manipulates modelReference. Should replace with hint to avoid it.
	//  Alternatively we can mark ObjectNode as model-agnostic.
	private final ObjectMapper objectMapper;

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		return JsonEditorRenderer.render(builder -> builder
				.configurer(aceEditor -> aceEditor.setWidth("50em"))
				.onValueChange(nextValue -> {
					GenericObject nextGenericObject = objectMapper.convertValue(nextValue, GenericObject.class);
					//
					parameters.onValueChange().accept(nextGenericObject);
				})
				.readOnly(!parameters.calcEditable())
				.title(StringUtils.emptyToNull(parameters.calcNonEmptyNonEditableReasons().mkString("\n")))
				.value(objectMapper.convertValue(parameters.currentValue(), ObjectNode.class))
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		GenericObject currentValue;

		@NonNull
		GenericObject initialValue;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		ObjectModel objectModel;

		@NonNull
		Consumer<GenericObject> onValueChange;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder {
			// Lomboked
		}
	}
}
