package tk.labyrinth.pandora.ui.component.valuewrapper;

import com.vaadin.flow.component.textfield.TextField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectBoxRenderer;

import java.util.function.Consumer;

public class ObjectValueWrapperBoxRenderer {

	public static TextField render(Properties properties) {
		return GenericObjectBoxRenderer.render(GenericObjectBoxRenderer.Parameters.builder()
				.currentValue(properties.getValue() != null
						? properties.getValue().getValue()
						: null)
				.initialValue(properties.getValue() != null
						? properties.getValue().getValue()
						: null)
				.label(properties.getLabel())
				.nonEditableReasons(properties.getNonEditableReasons())
				.onValueChange(properties.getOnValueChange() != null
						? nextValue -> properties.getOnValueChange().accept(ObjectValueWrapper.of(nextValue))
						: null)
				.build());
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<ObjectValueWrapper> onValueChange;

		@Nullable
		ObjectValueWrapper value;
	}
}
