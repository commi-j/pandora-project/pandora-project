package tk.labyrinth.pandora.ui.routing.routetarget;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;

@Deprecated
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RouteTarget {

	Class<?> handlingClass;

	ObjectModel parametersModel;
}
