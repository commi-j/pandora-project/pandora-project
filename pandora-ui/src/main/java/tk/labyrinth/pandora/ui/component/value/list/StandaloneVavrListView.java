package tk.labyrinth.pandora.ui.component.value.list;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.java.lang.EnumUtils;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.listview.ListItem;
import tk.labyrinth.pandora.ui.component.value.listview.ListItemStatus;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class StandaloneVavrListView<E> implements RenderableView<StandaloneVavrListView.Properties<E>> {

	private final Observable<State<E>> stateObservable = Observable.notInitialized();

	private final VavrListView<E> vavrListView = new VavrListView<>();

	{
		stateObservable.subscribe(nextState -> vavrListView.render(VavrListView.Properties.<E>builder()
				.elementBoxSupplier(nextState.getProperties().getElementBoxSupplier())
				.nonEditableReasons(nextState.getProperties().getNonEditableReasons())
				.onAddElementClick(() -> {
					List<ListItem<E>> nextValue = nextState.getItems().insert(
							nextState.getItems().size() - 1,
							ListItem.ofAdded(nextState.getProperties().getNewElementSupplier().get()));
					//
					stateObservable.update(currentState -> currentState.withItems(nextValue));
					nextState.getProperties().getOnValueChange().accept(itemsToElements(nextValue));
				})
				.onValueChange(nextValue -> {
					stateObservable.update(currentState -> currentState.withItems(nextValue));
					nextState.getProperties().getOnValueChange().accept(itemsToElements(nextValue));
				})
				.value(nextState.getItems())
				.build()));
	}

	@Override
	public Component asVaadinComponent() {
		return vavrListView.asVaadinComponent();
	}

	@Override
	public void render(Properties<E> properties) {
		stateObservable.set(State.<E>builder()
				.items(List
						.range(0, Math.max(properties.getInitialValue().size(), properties.getCurrentValue().size()))
						.map(index -> createListItem(properties.getInitialValue(), properties.getCurrentValue(), index))
						.append(ListItem.ofNotExisting()))
				.properties(properties)
				.build());
	}

	public static <E> ListItem<E> createListItem(List<E> initialList, List<E> currentList, int elementIndex) {
		ListItem<E> result;
		{
			if (initialList.size() > elementIndex) {
				// Present in initialList -> modified, not-modified or removed.
				//
				if (currentList.size() > elementIndex) {
					// Present in currentList -> modified or not-modified.
					//
					E initialElement = initialList.get(elementIndex);
					E currentElement = currentList.get(elementIndex);
					//
					result = Objects.equals(initialElement, currentElement)
							? ListItem.ofNotModified(currentElement)
							: currentElement != null
							? ListItem.ofModified(initialElement, currentElement)
							: ListItem.ofRemoved(initialElement);
				} else {
					// Removed.
					//
					result = ListItem.ofRemoved(initialList.get(elementIndex));
				}
			} else {
				// Absent in initialList -> added.
				//
				if (currentList.size() > elementIndex) {
					// Present in currentList -> added.
					//
					result = ListItem.ofAdded(currentList.get(elementIndex));
				} else {
					throw new UnreachableStateException();
				}
			}
		}
		return result;
	}

	public static <E> List<E> itemsToElements(List<ListItem<E>> items) {
		return items
				.filter(item -> !EnumUtils.in(item.getStatus(), ListItemStatus.NOT_EXISTING, ListItemStatus.REMOVED))
				.map(ListItem::getOutputValue);
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties<E> {

		@NonNull
		List<E> currentValue;

		@NonNull
		Supplier<MutableValueBox<E>> elementBoxSupplier;

		@NonNull
		List<E> initialValue;

		@NonNull
		Supplier<E> newElementSupplier;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<List<E>> onValueChange;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State<E> {

		@NonNull
		List<ListItem<E>> items;

		@NonNull
		Properties<E> properties;
	}
}
