package tk.labyrinth.pandora.ui.component.dialog;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;
import lombok.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class Dialogs {

	/**
	 * @param content non-null
	 *
	 * @return callback to close Dialog externally
	 */
	public static Runnable show(@NonNull Component content) {
		return show(content, null);
	}

	/**
	 * @param content       non-null
	 * @param closeCallback nullable, invoked if Dialog is closed internally
	 *
	 * @return callback to close Dialog externally
	 */
	public static Runnable show(@NonNull Component content, @Nullable Runnable closeCallback) {
		Dialog dialog = new Dialog();
		{
			// Configure self.
			//
			dialog.setDraggable(true);
		}
		{
			// Configure children.
			//
			dialog.add(content);
		}
		{
			// Configure listeners.
			//
			if (closeCallback != null) {
				dialog.addOpenedChangeListener(event -> {
					if (!event.isOpened()) {
						closeCallback.run();
					}
				});
			}
		}
		{
			// Initialize.
			//
			dialog.open();
		}
		return dialog::close;
	}
}
