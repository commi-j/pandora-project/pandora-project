package tk.labyrinth.pandora.ui.component.table;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class AdvancedTableColumnsModel {

	/**
	 * Order also reflected here.
	 */
	List<String> visibleColumns;
}
