package tk.labyrinth.pandora.ui.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.textfield.TextArea;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@Deprecated
@LazyComponent
@RequiredArgsConstructor
public class JsonDisplayer {

	private final ObjectMapper objectMapper;

	public void display(Object object) {
		Dialog dialog = new Dialog();
		{
			dialog.setMinWidth("40%");
		}
		{
			TextArea textArea = new TextArea();
			textArea.setReadOnly(true);
			try {
				textArea.setValue(objectMapper
						.writerWithDefaultPrettyPrinter()
						.writeValueAsString(object));
			} catch (JsonProcessingException ex) {
				throw new RuntimeException(ex);
			}
			textArea.setWidthFull();
			dialog.add(textArea);
		}
		dialog.open();
	}
}
