package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.datatypes.domain.hyperlink.Hyperlink;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import javax.annotation.CheckForNull;

@Slf4j
public class HyperlinkBox implements RenderableView<HyperlinkBox.Properties> {

	private final Anchor anchor = new Anchor();

	private void doRender(Properties properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		Option<String> destinationOption = Option.of(properties.getValue()).map(Hyperlink::getDestination);
		{
			if (destinationOption.isDefined()) {
				anchor.setHref(destinationOption.get());
			} else {
				anchor.removeHref();
			}
		}
		anchor.setTarget(properties.getTarget());
		anchor.setText(Option.of(properties.getValue())
				.map(Hyperlink::getText)
				.orElse(destinationOption)
				.getOrElse(""));
	}

	@Override
	public Component asVaadinComponent() {
		return anchor;
	}

	@Override
	public void render(Properties properties) {
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		}
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		AnchorTarget target;

		@CheckForNull
		Hyperlink value;
	}
}
