package tk.labyrinth.pandora.ui.component.reference;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.PlaceSelf;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoVariables;

public class VeryCustomBox extends CssGridLayout {

	private final ComboBox<String> comboBox = new ComboBox<>();

	private final CssHorizontalLayout controls = new CssHorizontalLayout();

	private final TextField input = new TextField();

	private final Label label = new Label();

	{
		{
			setGridTemplateAreas("centre");
		}
		{
			{
				comboBox.setLabel("Label");
				add(comboBox, "centre");
			}
			{
				{
					CssGridItem.setAlignSelf(controls, PlaceSelf.START);
					CssGridItem.setJustifySelf(controls, PlaceSelf.END);
				}
				{
					Icon icon = VaadinIcon.COG.create();
					icon.setColor(LumoVariables.SECONDARY_TEXT_COLOUR);
					icon.setSize("18px");
					Button configureButton = new Button(icon);
					configureButton.setMinHeight("0px");
					configureButton.setMinWidth("0px");
					configureButton.setHeight("21px");
					configureButton.setWidth("21px");
					StyleUtils.setMargin(configureButton, "0px");
					StyleUtils.setPadding(configureButton, "0px");
					configureButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
					configureButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
					controls.add(configureButton);
				}
				{
					Icon icon = VaadinIcon.CLOSE.create();
					icon.setColor(LumoVariables.SECONDARY_TEXT_COLOUR);
					icon.setSize("18px");
					Button clearButton = new Button(icon);
					clearButton.setMinHeight("0px");
					clearButton.setMinWidth("0px");
					clearButton.setHeight("21px");
					clearButton.setWidth("21px");
					StyleUtils.setMargin(clearButton, "0px");
					StyleUtils.setPadding(clearButton, "0px");
					clearButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
					clearButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
					controls.add(clearButton);
				}
				add(controls, "centre");
			}
		}
	}
}
