package tk.labyrinth.pandora.ui.tool;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.VaadinSessionScopedComponent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@VaadinSessionScopedComponent
public class VaadinSessionCache {

	private final ConcurrentMap<Object, Object> cache = new ConcurrentHashMap<>();

	@Nullable
	public Object find(Object key) {
		return cache.get(key);
	}

	public void put(Object key, Object value) {
		cache.put(key, value);
	}
}
