package tk.labyrinth.pandora.ui.component.value.list;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.function.ValueProvider;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.java.lang.EnumUtils;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.ui.component.value.box.ValueBoxContext;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.listview.ListItem;
import tk.labyrinth.pandora.ui.component.value.listview.ListItemStatus;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.pandora.views.style.PandoraColours;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Deprecated
public class VavrListView<E> implements RenderableView<VavrListView.Properties<E>> {

	private final Grid<ListItem<E>> grid = new Grid<>();

	private Properties<E> properties;

	{
		{
//			grid.addClassNames(PandoraStyles.CARD);
			grid.setSelectionMode(Grid.SelectionMode.NONE);
			grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
		}
		{
			grid
					.addComponentColumn(item -> {
						CssHorizontalLayout layout = new CssHorizontalLayout();
						{
							layout.add(renderActionButton(
									StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")),
									item));
						}
						{
							Div div = new Div();
							//
							div.setHeight("1em");
							div.setWidth("1em");
							div.getStyle().set(
									"background-color",
									switch (item.getStatus()) {
										case ADDED -> PandoraColours.GREEN;
										case REMOVED -> PandoraColours.RED;
										case MODIFIED -> PandoraColours.BLUE;
										default -> "transparent";
									});
							//
							layout.add(div);
						}
						return layout;
					})
					.setFlexGrow(0);
			grid
					.addComponentColumn(component(pair -> {
						MutableValueBox<E> elementBox = properties.getElementBoxSupplier().get();
						//
						elementBox.render(MutableValueBox.Properties.<E>builder()
								.context(ValueBoxContext.builder()
										.currentValue(null) // TODO
										.parent(properties.getContext())
										.path("INDEX")
										.build())
								.currentValue(pair.getLeft().getOutputValue())
								.initialValue(pair.getLeft().getInputValue())
								.label(null)
								.nonEditableReasons(properties.getNonEditableReasons())
								.onValueChange(nextValue -> properties.getOnValueChange().accept(properties.getValue()
										.replace(pair.getLeft(), pair.getLeft().withOutputValue(nextValue))))
								.build());
						StyleUtils.setWidth(elementBox.asVaadinComponent(), "100%");
						return elementBox.asVaadinComponent();
					}));
		}
	}

	private Button renderActionButton(@Nullable String nonEditableTitle, ListItem<E> item) {
		ListItemStatus status = item.getStatus();
		//
		Icon icon = EnumUtils.in(status, ListItemStatus.NOT_EXISTING, ListItemStatus.REMOVED)
				? VaadinIcon.PLUS.create()
				: VaadinIcon.MINUS.create();
		//
		List<ListItem<E>> items = List.ofAll(grid.getListDataView().getItems());
		Runnable runnable = () -> {
			if (status != ListItemStatus.NOT_EXISTING) {
				List<ListItem<E>> nextItems = switch (status) {
					case ADDED -> items.remove(item);
					case MODIFIED, NOT_MODIFIED -> items.replace(item, item.asRemoved());
					case NOT_EXISTING -> throw new UnreachableStateException();
					case REMOVED -> items.replace(item, item.asNotRemoved());
				};
				properties.getOnValueChange().accept(nextItems);
			} else {
				properties.getOnAddElementClick().run();
			}
		};
		//
		Button button = new Button(icon, event -> runnable.run());
		//
		if (nonEditableTitle != null) {
			button.setEnabled(false);
			button.setTooltipText(nonEditableTitle);
		}
		//
		return button;
	}

	@Override
	public Component asVaadinComponent() {
		return grid;
	}

	@Override
	public void render(Properties<E> properties) {
		this.properties = properties;
		//
		grid.setItems(properties.getValue().asJava());
	}

	/**
	 * <b>componentProvider</b> receives pair of ListItem and current element.
	 *
	 * @param componentProvider non-null
	 * @param <E>               Element
	 * @param <C>               Component
	 *
	 * @return non-null
	 */
	@SuppressWarnings("unchecked")
	public static <E, C extends Component> ValueProvider<ListItem<E>, C> component(
			ValueProvider<Pair<ListItem<E>, E>, C> componentProvider) {
		// TODO: Find a way to specify empty html element instead of div.
		return item -> item.getStatus() != ListItemStatus.NOT_EXISTING
				? componentProvider.apply(Pair.of(item, item.getValue()))
				: (C) new Div();
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties<E> {

		@Nullable
		ValueBoxContext context;

		@NonNull
		Supplier<MutableValueBox<E>> elementBoxSupplier;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Runnable onAddElementClick;

		@NonNull
		Consumer<List<ListItem<E>>> onValueChange;

		@NonNull
		List<ListItem<E>> value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
