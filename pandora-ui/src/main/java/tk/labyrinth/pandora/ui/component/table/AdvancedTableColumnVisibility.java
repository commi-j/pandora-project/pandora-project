package tk.labyrinth.pandora.ui.component.table;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.datatype.SimpleType;

@SimpleType
@Value(staticConstructor = "of")
@With
public class AdvancedTableColumnVisibility {

	@NonNull
	String attributeName;

	@NonNull
	Boolean visible;

	@JsonValue
	@Override
	public String toString() {
		return "%s%s".formatted(visible ? "+" : "-", attributeName);
	}

	@JsonCreator
	public static AdvancedTableColumnVisibility from(String value) {
		return new AdvancedTableColumnVisibility(
				value.substring(1),
				switch (value.charAt(0)) {
					case '+' -> true;
					case '-' -> false;
					default -> throw new IllegalArgumentException();
				});
	}
}
