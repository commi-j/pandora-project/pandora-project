package tk.labyrinth.pandora.ui.renderer.custom;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.pattern.PandoraPattern;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectItem;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;

import java.util.Objects;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class GenericObjectToItemRenderer {
	//
//	private final FunctionRegistry functionRegistry;

	private final ObjectModelRegistry objectModelRegistry;

	private final ObjectProvider<ToStringRendererRegistry> toStringRendererRegistryProvider;

	// FIXME: Replace with same calls to ObjectModelRegistry.
	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	// TODO: Merge with renderAttributePath.
	private String renderAttribute(ObjectModel objectModel, GenericObjectAttribute attribute) {
		return toStringRendererRegistryProvider.getObject().render(
				ToStringRendererRegistry.Context.builder()
						.datatype(objectModel.getAttributes()
								.find(modelAttribute -> Objects.equals(
										modelAttribute.getName(),
										attribute.getName()))
								.get()
								.getDatatype())
						.hints(List.empty())
						.build(),
				attribute.getValue());
	}

	private String renderAttributePath(ObjectModel objectModel, GenericObject genericObject, String attributePath) {
		ValueWrapper valueWrapper = genericObject.lookup(attributePath);
		ObjectModelAttribute objectModelAttribute = objectModelRegistry.lookupAttribute(objectModel, attributePath);
		//
		return toStringRendererRegistryProvider.getObject().render(
				ToStringRendererRegistry.Context.builder()
						.datatype(objectModelAttribute != null ? objectModelAttribute.getDatatype() : null)
						.hints(List.empty())
						.build(),
				valueWrapper);
	}

	@Nullable
	private ObjectModel resolveModel(@Nullable ObjectModel objectModel, GenericObject object) {
		ObjectModel result;
		{
			if (objectModel != null) {
				// TODO: Ensure compatible, if not throw ex or return 'not compatible' outcome?
				//
				result = objectModel;
			} else {
				CodeObjectModelReference objectModelReference = PandoraRootUtils.findModelReference(object);
				result = objectModelReference != null ? objectModelSearcher.findSingle(objectModelReference) : null;
			}
		}
		return result;
	}

	String renderPatternRule(ObjectModel objectModel, GenericObject genericObject, String patternString) {
		PandoraPattern pattern = PandoraPattern.from(patternString);
		//
		List<String> placeholderNames = pattern.getPlaceholderNames();
		//
		Map<String, String> variableEntries = placeholderNames.toMap(
				Function.identity(),
				placeholderName -> renderAttributePath(objectModel, genericObject, placeholderName));
		//
		return pattern.render(variableEntries);
	}

	public GenericObjectItem renderNonNull(@Nullable ObjectModel objectModel, GenericObject object) {
		GenericObjectItem result;
		{
			ObjectModel objectModelToUse = resolveModel(objectModel, object);
			//
			if (objectModelToUse != null) {
				String renderRule = objectModelToUse.getRenderRule();
				//
				if (renderRule != null) {
					if (renderRule.startsWith("attribute:")) {
						// Attribute.
						//
						List<String> attributeNames = List.of(renderRule.substring("attribute:".length())
								.split(","));
						//
						GenericObjectAttribute objectAttribute = attributeNames
								.map(object::findAttribute)
								.find(Objects::nonNull)
								.getOrNull();
						//
						if (objectAttribute != null) {
							result = GenericObjectItem.ofValid(object, renderAttribute(objectModelToUse, objectAttribute));
						} else {
							result = GenericObjectItem.ofValid(object, "null");
						}
					} else if (renderRule.startsWith("function:")) {
						// Function.
						//
						String functionSignature = renderRule.substring("function:".length());
						//
						// TODO
						throw new NotImplementedException();
//						result = GenericObjectItem.ofValid(
//								object,
//								(String) functionRegistry.invoke(object, functionSignature, List.empty()));
					} else if (renderRule.startsWith("pattern:")) {
						// Pattern.
						//
						result = GenericObjectItem.ofValid(
								object,
								renderPatternRule(objectModelToUse, object, renderRule.substring("pattern:".length())));
					} else {
						throw new IllegalArgumentException("Illegal renderRule: %s".formatted(renderRule));
					}
				} else {
					result = GenericObjectItem.ofRenderRuleNotSpecified(object);
				}
			} else {
				if (PandoraRootUtils.findModelReference(object) != null) {
					result = GenericObjectItem.ofModelNotFound(object);
				} else {
					result = GenericObjectItem.ofModelReferenceNotSpecified(object);
				}
			}
		}
		return result;
	}
}
