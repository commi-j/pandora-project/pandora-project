package tk.labyrinth.pandora.ui.domain.state.rework;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.BeanPostProcessor;
import reactor.core.Disposable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.domain.state.StringUiStateAttributeHandler;
import tk.labyrinth.pandora.ui.domain.state.UiStateAttributeHandler;
import tk.labyrinth.pandora.ui.domain.state.model.GenericUiStateBinder;
import tk.labyrinth.pandora.ui.domain.state.model.UiStateAttributeUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Optional;

/**
 * @see UiStateAttribute2
 */
@LazyComponent
@RequiredArgsConstructor
public class UiStateAttribute2BeanPostProcessor implements BeanPostProcessor {

	private final ObjectProvider<ConverterRegistry> converterRegistryProvider;

	private final ObjectProvider<GenericUiStateBinder> uiStateBinderProvider;

	private <T> void attachUiStateAttributeHandling(
			Object bean,
			Field uiStateAttributeField,
			GenericUiStateBinder uiStateBinder) throws IllegalAccessException {
		uiStateAttributeField.setAccessible(true);
		//
		T defaultValue = getDefaultValue(uiStateAttributeField, bean);
		//
		ConverterRegistry converterRegistry = converterRegistryProvider.getObject();
		//
		UiStateAttribute2 uiStateAttributeAnnotation = uiStateAttributeField.getAnnotation(UiStateAttribute2.class);
		//
		StringUiStateAttributeHandler uiStateAttributeHandler = StringUiStateAttributeHandler.of(
				UiStateAttributeUtils.computeAttributeName(
						uiStateAttributeAnnotation,
						uiStateAttributeField.getName()),
				converterRegistry.convert(
						defaultValue,
						String.class),
				uiStateAttributeAnnotation.kind(),
				UiStateAttributeUtils.computeQueryAttributeName(
						uiStateAttributeAnnotation,
						uiStateAttributeField.getName()));
		//
		bindObservable(bean, uiStateAttributeField, uiStateAttributeHandler, uiStateBinder, converterRegistry);
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		List<Field> uiStateAttributeFields = List.of(bean.getClass().getDeclaredFields())
				.filter(field -> field.getAnnotation(UiStateAttribute2.class) != null)
				.peek(field -> {
					if (!Observable.class.isAssignableFrom(field.getType())) {
						throw new IllegalStateException("Only Observables are supported");
					}
				});
		//
		if (!uiStateAttributeFields.isEmpty()) {
			GenericUiStateBinder uiStateBinder = uiStateBinderProvider.getObject();
			//
			uiStateAttributeFields.forEach(uiStateAttributeField -> {
				try {
					attachUiStateAttributeHandling(bean, uiStateAttributeField, uiStateBinder);
				} catch (IllegalAccessException ex) {
					throw new RuntimeException(ex);
				}
			});
		}
		//
		return bean;
	}

	@SuppressWarnings("unchecked")
	public static <T> Disposable bindObservable(
			Object bean,
			Field uiStateAttributeField,
			UiStateAttributeHandler uiStateAttributeHandler,
			GenericUiStateBinder uiStateBinder,
			ConverterRegistry converterRegistry) throws IllegalAccessException {
		Disposable result;
		{
			if (isOptionalObservable(uiStateAttributeField)) {
				result = bindOptionalObservable(
						uiStateAttributeHandler,
						uiStateBinder,
						(Observable<Optional<T>>) getObservable(uiStateAttributeField, bean),
						converterRegistry,
						getObservableType(uiStateAttributeField));
			} else {
				result = bindValueObservable(
						uiStateAttributeHandler,
						uiStateBinder,
						(Observable<T>) getObservable(uiStateAttributeField, bean),
						converterRegistry,
						getObservableType(uiStateAttributeField));
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static <T> Disposable bindOptionalObservable(
			UiStateAttributeHandler uiStateAttributeHandler,
			GenericUiStateBinder uiStateBinder,
			Observable<Optional<T>> observable,
			ConverterRegistry converterRegistry,
			Type observableType) {
		return uiStateBinder.bindAttribute(
				uiStateAttributeHandler,
				next -> observable.set(next.map(value -> (T) converterRegistry
						.convert(value, observableType))),
				observable.getFlux().map(next -> next.map(value -> converterRegistry
						.convert(value, ValueWrapper.class))));
	}

	@SuppressWarnings("unchecked")
	public static <T> Disposable bindValueObservable(
			UiStateAttributeHandler uiStateAttributeHandler,
			GenericUiStateBinder uiStateBinder,
			Observable<T> observable,
			ConverterRegistry converterRegistry,
			Type valueType) {
		return uiStateBinder.bindAttribute(
				uiStateAttributeHandler,
				next -> observable.set(next
						.map(value -> (T) converterRegistry.convert(value, valueType))
						.orElseThrow()),
				observable.getFlux().map(next -> Optional.of(converterRegistry
						.convert(next, ValueWrapper.class))));
	}

	@Nullable
	@SuppressWarnings("unchecked")
	public static <T> T getDefaultValue(Field uiStateAttributeField, Object bean) throws IllegalAccessException {
		T result;
		{
			boolean isOptional = isOptionalObservable(uiStateAttributeField);
			//
			Observable<?> observable = getObservable(uiStateAttributeField, bean);
			//
			if (observable == null) {
				throw new IllegalStateException("We don't support null values, as of now");
			}
			//
			if (isOptional) {
				Observable<Optional<T>> optionalObservable = (Observable<Optional<T>>) observable;
				//
				if (!optionalObservable.initialized()) {
					throw new IllegalStateException("Observable must be initialized with default value");
				}
				//
				result = optionalObservable.get().orElse(null);
			} else {
				Observable<T> valueObservable = (Observable<T>) observable;
				//
				if (!valueObservable.initialized()) {
					throw new IllegalStateException("Observable must be initialized with default value");
				}
				//
				result = valueObservable.get();
			}
		}
		return result;
	}

	public static Observable<?> getObservable(Field uiStateAttributeField, Object bean) throws IllegalAccessException {
		return (Observable<?>) uiStateAttributeField.get(bean);
	}

	public static Type getObservableType(Field uiStateAttributeField) {
		return ParameterUtils.getFirstActualParameter(uiStateAttributeField.getGenericType(), Observable.class);
	}

	public static boolean isOptionalObservable(Field uiStateAttributeField) {
		return TypeUtils.getClass(getObservableType(uiStateAttributeField)) == Optional.class;
	}
}
