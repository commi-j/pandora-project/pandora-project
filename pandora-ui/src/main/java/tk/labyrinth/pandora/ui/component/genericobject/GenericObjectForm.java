package tk.labyrinth.pandora.ui.component.genericobject;

import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;

public interface GenericObjectForm extends ObjectForm<GenericObject> {
	// empty
}
