package tk.labyrinth.pandora.ui.component.table.renderedobject;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.Lazy;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.val;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.telemetry.annotation.AdvancedSpanAttribute;
import tk.labyrinth.pandora.ui.renderer.tovaadincomponent.ToVaadinComponentRendererRegistry;

@LazyComponent
@RequiredArgsConstructor
// TODO: Find better name. this one is not inline renderer, it makes all children to be inline.
public class GenericObjectInlineRenderer {

	@SmartAutowired
	private Lazy<ToVaadinComponentRendererRegistry> toVaadinComponentRendererRegistryLazy;

	@WithSpan
	public RenderedObject render(
			@AdvancedSpanAttribute(value = "objectModel.code", path = "code") ObjectModel objectModel,
			GenericObject genericObject) {
		ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry = toVaadinComponentRendererRegistryLazy.get();
		//
		return Try
				.ofSupplier(() -> RenderedObject.builder()
						.attributes(objectModel.getAttributes().toMap(
								ObjectModelAttribute::getName,
								attribute -> {
									val context = ToVaadinComponentRendererRegistry.Context.builder()
											.datatype(attribute.getDatatype())
											.hints(List.empty())
											.build();
									//
									return toVaadinComponentRendererRegistry.render(
											context,
											genericObject.findAttributeValue(attribute.getName()));
								}))
						.object(genericObject)
						.build())
				.recover(fault -> RenderedObject.builder()
						.attributes(HashMap.empty())
						.fault(fault)
						.object(genericObject)
						.build())
				.get();
	}
}
