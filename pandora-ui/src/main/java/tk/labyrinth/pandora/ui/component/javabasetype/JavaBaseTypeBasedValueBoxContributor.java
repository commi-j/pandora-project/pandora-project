package tk.labyrinth.pandora.ui.component.javabasetype;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;
import tk.labyrinth.pandora.ui.component.value.box.mutable.ConvertingValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.SimpleValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class JavaBaseTypeBasedValueBoxContributor implements ValueBoxContributor {

	private final ConverterRegistry converterRegistry;

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ObjectModelRegistry objectModelRegistry;

	private final SimpleValueBoxContributor simpleValueBoxContributor;

	@Override
	@SuppressWarnings("unchecked")
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		MutableValueBox<?> result;
		{
			Datatype datatype = context.getDatatype();
			//
			JavaBaseType javaBaseType = javaBaseTypeRegistry.getJavaBaseType(JavaBaseTypeUtils.extractJavaBaseTypeReference(
					datatype.getBaseReference()));
			Type type = JavaBaseTypeUtils.getJavaTypeFromDatatype(datatype);
			//
			result = switch (javaBaseType.computeDatatypeKind()) {
				case LIST -> throw new NotImplementedException();
				case OBJECT -> {
					MutableValueBox<GenericObject> valueBox = registry.getValueBoxInferred(
							objectModelRegistry.getObjectModelDatatype(datatype));
					//
					yield ConvertingValueBox.of(
							valueBox,
							type,
							externalProperties -> MutableValueBox.Properties.<GenericObject>builder()
									.context(externalProperties.getContext())
									.currentValue(converterRegistry.convert(
											externalProperties.getCurrentValue(),
											GenericObject.class))
									.initialValue(converterRegistry.convert(
											externalProperties.getInitialValue(),
											GenericObject.class))
									.label(externalProperties.getLabel())
									.nonEditableReasons(externalProperties.getNonEditableReasons())
									.onValueChange(nextValue -> externalProperties.getOnValueChange().accept(
											converterRegistry.convert(nextValue, type)))
									.build());
				}
				case SIMPLE -> ConvertingValueBox.of(
						(MutableValueBox<String>) simpleValueBoxContributor.contributeValueBox(
								registry,
								ValueBoxRegistry.Context.builder()
										.datatype(datatype)
										.features(List.empty())
										.build()),
						type,
						externalProperties -> MutableValueBox.Properties.<String>builder()
								.context(externalProperties.getContext())
								.currentValue(converterRegistry.convert(
										externalProperties.getCurrentValue(),
										String.class))
								.initialValue(converterRegistry.convert(
										externalProperties.getInitialValue(),
										String.class))
								.label(externalProperties.getLabel())
								.nonEditableReasons(externalProperties.getNonEditableReasons())
								.onValueChange(nextValue -> externalProperties.getOnValueChange().accept(
										converterRegistry.convert(nextValue, type)))
								.build());
				case VALUE -> throw new NotImplementedException();
			};
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return context.getDatatype() != null &&
				JavaBaseTypeUtils.isJavaBaseTypeAliasDatatypeBaseReference(context.getDatatype().getBaseReference())
				? ToStringRenderer.MAX_DISTANCE_MINUS_TWO
				: null;
	}
}
