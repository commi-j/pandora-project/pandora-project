package tk.labyrinth.pandora.ui.component.valuebox.suggest;

import com.vaadin.flow.component.combobox.ComboBox;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.functionalcomponents.box.SuggestBoxRenderer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;

@LazyComponent
@RequiredArgsConstructor
public class SuggestValueBoxContributor implements ValueBoxContributor {

	private final BeanContext beanContext;

	private final ConverterRegistry converterRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		HasSuggestsObjectModelAttributeFeature feature = context.getFeatures()
				.find(featureElement -> featureElement instanceof HasSuggestsObjectModelAttributeFeature)
				.map(HasSuggestsObjectModelAttributeFeature.class::cast)
				.get();
		//
		Class<? extends SuggestProvider<?>> suggestProviderClass = feature.getSuggestProviderClass();
		//
		SuggestProvider<?> suggestProvider = beanContext.getBeanFactory().getBean(suggestProviderClass);
		//
		// TODO: This one is for the "proper" solution.
//		MutableValueBox<?> valueBox = registry.getValueBox(context.withFeatures(context.getFeatures().remove(feature)));
		//
		return new FunctionalMutableValueBox<>(
				properties -> {
					return createSuggestBox(converterRegistry, suggestProvider, properties);
					//
					// TODO: We want it to wrap existing box, not replace.
//					CssHorizontalLayout layout = new CssHorizontalLayout();
//					{
//						layout.setAlignItems(AlignItems.BASELINE);
//					}
//					{
//						{
//							valueBox.render(infer(properties));
//							//
//							layout.add(valueBox.asVaadinComponent());
//						}
//						{
//							layout.add(ButtonRenderer.render(builder -> builder
//									.icon(VaadinIcon.SEARCH.create())
//									.themeVariants(ButtonVariant.LUMO_SMALL)
//									.build()));
//						}
//					}
//					return layout;
				},
				Object.class);
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return context.getFeatures().exists(feature -> feature instanceof HasSuggestsObjectModelAttributeFeature)
				? Integer.MAX_VALUE - 100
				: null;
	}

	@SuppressWarnings("unchecked")
	private static <T> ComboBox<T> createSuggestBox(
			ConverterRegistry converterRegistry,
			SuggestProvider<T> suggestProvider,
			MutableValueBox.Properties<?> rawProperties) {
		MutableValueBox.Properties<T> properties = (MutableValueBox.Properties<T>) rawProperties;
		//
		return SuggestBoxRenderer.render(builder -> builder
				.itemsFunction(filter -> suggestProvider.provideSuggests(SuggestContext.builder()
						.converterRegistry(converterRegistry)
						.filter(filter)
						.valueBoxContext(properties.getContext())
						.build()))
				.label(properties.getLabel())
				.onValueChange(properties.getOnValueChange())
				.value(properties.getCurrentValue())
				.build());
	}
}
