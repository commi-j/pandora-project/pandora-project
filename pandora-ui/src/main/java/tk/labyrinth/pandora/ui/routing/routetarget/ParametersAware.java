package tk.labyrinth.pandora.ui.routing.routetarget;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.WildcardParameter;

/**
 * @param <P> Parameters
 */
@Deprecated
public interface ParametersAware<P> extends HasUrlParameter<String> {

	void acceptParameters(P parameters);

	P getInitialParameters();

	@Override
	default void setParameter(BeforeEvent event, @WildcardParameter String parameter) {
		SetParameterHookHandler.setComponent((Component) this);
	}
}
