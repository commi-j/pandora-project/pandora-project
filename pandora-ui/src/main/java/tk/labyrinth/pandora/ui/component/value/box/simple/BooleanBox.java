package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.select.Select;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.function.Consumer;

@Deprecated
@Slf4j
public class BooleanBox implements SimpleValueBox<Boolean> {

	private final Select<Boolean> select = new Select<>();

	private boolean eventHandlingInProgress = false;

	private Properties<Boolean> properties;

	private boolean renderInProgress = false;

	{
		{
			// Specifying generator because otherwise Vaadin's Select will use item#toString() that results in NPE.
			select.setItemLabelGenerator(item -> item != null ? item.toString() : "");
			//
			select.setItems(null, true, false);
		}
		{
			select.addValueChangeListener(event -> {
				if (!renderInProgress) {
					if (!eventHandlingInProgress) {
						eventHandlingInProgress = true;
						try {
							select.setValue(event.getOldValue());
						} finally {
							eventHandlingInProgress = false;
						}
						//
						Consumer<Boolean> onValueChange = properties.getOnValueChange();
						if (onValueChange != null) {
							onValueChange.accept(event.getValue());
						}
					}
				}
			});
		}
	}

	private void doRender(Properties<Boolean> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		select.setLabel(properties.getLabel());
		select.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				select,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		select.setValue(properties.getValue());
	}

	@Override
	public Component asVaadinComponent() {
		return select;
	}

	@Override
	public void render(Properties<Boolean> properties) {
		if (renderInProgress) {
			throw new IllegalStateException("Invoked render while already rendering");
		}
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}
}
