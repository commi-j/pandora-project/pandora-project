package tk.labyrinth.pandora.ui.component.value.box.registry;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.UnaryParameterizedBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Type;

@LazyComponent
@RequiredArgsConstructor
public class TypedValueBoxProviderBeanContributor extends UnaryParameterizedBeanContributor<TypedValueBoxProvider<?>> {

	@Override
	protected @Nullable Object doContributeBean(
			BeanFactory beanFactory,
			Class<? extends TypedValueBoxProvider<?>> base,
			Type parameter) {
		return beanFactory
				.withSuppressedBeanContributor(this)
				.withSuppressedBeanContributorsOfClass(Type.class)
				.withBean(parameter)
				.getBean(TypedValueBoxProvider.class);
	}

	@Nullable
	@Override
	protected Integer doGetSupportDistance(
			BeanFactory beanFactory,
			Class<? extends TypedValueBoxProvider<?>> base,
			Type parameter) {
		return Integer.MAX_VALUE;
	}
}
