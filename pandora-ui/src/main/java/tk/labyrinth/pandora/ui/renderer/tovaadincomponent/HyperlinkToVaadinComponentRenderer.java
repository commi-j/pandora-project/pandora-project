package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.hyperlink.Hyperlink;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.component.value.box.simple.HyperlinkBox;

import javax.annotation.CheckForNull;

@LazyComponent
@RequiredArgsConstructor
public class HyperlinkToVaadinComponentRenderer extends ToVaadinComponentRendererBase<Hyperlink> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, Hyperlink value) {
		HyperlinkBox hyperlinkBox = new HyperlinkBox();
		hyperlinkBox.render(HyperlinkBox.Properties.builder()
				.target(AnchorTarget.BLANK)
				.value(value)
				.build());
		return hyperlinkBox.asVaadinComponent();
	}

	@CheckForNull
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), Hyperlink.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}
}
