package tk.labyrinth.pandora.ui.component.value.box;

import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.ui.component.value.view.ValueView;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import java.util.function.Consumer;

@Deprecated
public interface ValueBox<T> extends TypeAware<T>, ValueView<T> {

	default void addNonEditableReason(String nonEditableReason) {
		Properties<T> properties = getProperties();
		setProperties(properties.withNonEditableReasons(properties.getNonEditableReasons()
				.append(nonEditableReason)
				.distinct()));
	}

	Properties<T> getProperties();

	@Nullable
	@Override
	default T getValue() {
		return getProperties().getValue();
	}

	default void removeNonEditableReason(String nonEditableReason) {
		Properties<T> properties = getProperties();
		setProperties(properties.withNonEditableReasons(properties.getNonEditableReasons()
				.removeAll(nonEditableReason)));
	}

	default void setChangeListener(@Nullable Consumer<T> changeListener) {
		setProperties(getProperties().withChangeListener(changeListener));
	}

	default void setLabel(@Nullable String label) {
		setProperties(getProperties().withLabel(label));
	}

	void setProperties(Properties<T> properties);

	default void setValue(Option<T> valueOption) {
		setProperties(getProperties().withValue(valueOption.getOrNull()));
	}

	default void setValue(@Nullable T value) {
		setProperties(getProperties().withValue(value));
	}

	default ValueBox<T> withChangeListener(@Nullable Consumer<T> changeListener) {
		setChangeListener(changeListener);
		return this;
	}

	default ValueBox<T> withLabel(@Nullable String label) {
		setLabel(label);
		return this;
	}

	default ValueBox<T> withNonEditableReason(String nonEditableReason) {
		addNonEditableReason(nonEditableReason);
		return this;
	}

	default ValueBox<T> withProperties(Properties<T> properties) {
		setProperties(properties);
		return this;
	}

	default ValueBox<T> withValue(@Nullable T value) {
		setValue(value);
		return this;
	}

	default ValueBox<T> withoutNonEditableReason(String nonEditableReason) {
		removeNonEditableReason(nonEditableReason);
		return this;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	class Properties<T> {

		@CheckForNull
		Consumer<T> changeListener;

		@CheckForNull
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		List<String> nonEditableReasons = List.empty();

		@CheckForNull
		T value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
