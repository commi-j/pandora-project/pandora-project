package tk.labyrinth.pandora.ui.component.value.view;

import tk.labyrinth.pandora.ui.component.view.View;

import javax.annotation.Nullable;

public interface ValueView<T> extends View {

	@Nullable
	T getValue();
}
