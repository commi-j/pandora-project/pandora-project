package tk.labyrinth.pandora.ui.component.genericobject;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.stores.rootobject.RootObjectUtils;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @see GenericObjectBoxRenderer
 */
@LazyComponent
@RequiredArgsConstructor
public class ModelAwareGenericObjectBoxRenderer {

	private final BeanContext beanContext;

	private final ToStringRendererRegistry toStringRendererRegistry;

	@SmartAutowired
	private TypedObjectSearcher<ObjectModel> objectModelSearcher;

	public TextField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public TextField render(Parameters parameters) {
		TextField textField = new TextField();
		{
			textField.setLabel(parameters.getLabel());
			textField.setReadOnly(true);
			textField.setSuffixComponent(ButtonRenderer.render(builder -> builder
					.icon(parameters.calcEditable() ? VaadinIcon.EDIT.create() : VaadinIcon.EYE.create())
					.onClick(event -> {
						ObjectModel objectModel = objectModelSearcher.getSingle(parameters.getObjectModelReference());
						//
						GenericObjectMultiView genericObjectView = beanContext.getBeanFactory()
								.withBean(objectModel)
								.getBean(GenericObjectMultiView.class);
						//
						GenericObject initialValue = parameters.getInitialValue() != null
								? parameters.getInitialValue()
								: createNewObject(parameters.getObjectModelReference());
						//
						Observable<GenericObject> currentValueObservable = Observable.withInitialValue(
								parameters.getCurrentValue() != null ? parameters.getCurrentValue() : initialValue);
						//
						currentValueObservable.subscribe(nextCurrentValue -> genericObjectView.render(
								GenericObjectMultiView.Properties.builder()
										.currentValue(nextCurrentValue)
										.datatype(parameters.getDatatype())
										.initialValue(initialValue)
										.nonEditableReasons(parameters.getNonEditableReasons())
//										.objectModel(objectModel)
										.onValueChange(currentValueObservable::set)
										.build()));
						//
						ConfirmationViews.showViewDialog(genericObjectView).subscribeAlwaysAccepted(success ->
								parameters.getOnValueChange().accept(currentValueObservable.get()));
					})
					.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
					.build()));
			textField.setTitle(StringUtils.emptyToNull(parameters.calcNonEmptyNonEditableReasons().mkString("\n")));
		}
		{
			String renderedValue = toStringRendererRegistry.render(
					ToStringRendererRegistry.Context.builder()
							.datatype(parameters.getObjectModelReference() != null
									? parameters.getObjectModelReference().toDatatype()
									: null)
							.hints(null)
							.build(),
					parameters.getCurrentValue());
			textField.setValue(renderedValue != null ? renderedValue : "");
		}
		return textField;
	}

	// FIXME: Only root models should have model explicitly set, or maybe even they don't need it.
	private static GenericObject createNewObject(@Nullable CodeObjectModelReference objectModelReference) {
		return objectModelReference != null
				? GenericObject.of(RootObjectUtils.createModelReferenceAttribute(objectModelReference))
				: GenericObject.empty();
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		GenericObject currentValue;

		@NonNull
		Datatype datatype;

		@Nullable
		GenericObject initialValue;

		@Nullable
		String label;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		CodeObjectModelReference objectModelReference;

		@Nullable
		Consumer<GenericObject> onValueChange;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder {
			// Lomboked
		}
	}
}
