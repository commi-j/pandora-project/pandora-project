package tk.labyrinth.pandora.ui.component.selector;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.treegrid.TreeGrid;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.JunctionPredicate;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.extra.predicate.PredicateUtils;
import tk.labyrinth.pandora.ui.component.predicate.PredicateViewRenderer;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

import java.util.Collection;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

@LazyComponent
@RequiredArgsConstructor
public class SelectorViewRenderer {

	private final PredicateViewRenderer predicateViewRenderer;

	public TreeGrid<Item> render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public TreeGrid<Item> render(Parameters parameters) {
		TreeGrid<Item> grid = new TreeGrid<>();
		{
			grid
					.addComponentHierarchyColumn(item -> {
						Component predicateComponent;
						{
							Predicate predicate = item.getPredicate();
							//
							if (predicate != null) {
								if (predicate instanceof JunctionPredicate junctionPredicate &&
										(junctionPredicate.predicates().isEmpty() || grid.isExpanded(item))) {
									predicateComponent = new Label(junctionPredicate.operator().toString());
								} else {
									predicateComponent = new Label(
											PredicateUtils.renderWithInfixJunctions(predicate, false));
								}
							} else {
								// FIXME: Think if we need this one.
								predicateComponent = new Button(
										VaadinIcon.PLUS.create(),
										event -> {
											// TODO
										});
							}
						}
						return predicateComponent;
					});
			grid
					.addComponentColumn(item -> {
						Predicate predicate = item.getPredicate();
						//
						if (predicate != null) {
							CssHorizontalLayout buttonLayout = new CssHorizontalLayout();
							if (predicate instanceof JunctionPredicate junctionPredicate) {
								Button addElementButton = new Button(
										VaadinIcon.PLUS.create(),
										event -> {
											Observable<Predicate> stateObservable = Observable.withInitialValue(
													JunctionPredicate.emptyAnd());
											ConfirmationViews
													.showFunctionDialog(
															stateObservable,
															nextState -> {
																CssVerticalLayout component = predicateViewRenderer
																		.render(builder -> builder
																				.currentValue(nextState)
																				.onValueChange(stateObservable::set)
																				.build());
																//
																component.setWidth("50em");
																//
																return component;
															})
													.subscribeAlwaysAccepted(success -> parameters.onValueChange()
															.accept(computeNextPredicate(
																	item,
																	junctionPredicate.withUpdatedPredicates(
																			predicates -> List.ofAll(predicates)
																					.append(stateObservable.get())
																					.asJava()))));
										});
								addElementButton.addThemeVariants(
										ButtonVariant.LUMO_SMALL,
										ButtonVariant.LUMO_TERTIARY);
								buttonLayout.add(addElementButton);
							} else {
								Button editButton = new Button(
										VaadinIcon.EDIT.create(),
										event -> {
											Observable<Predicate> stateObservable = Observable.withInitialValue(
													predicate);
											ConfirmationViews
													.showFunctionDialog(
															stateObservable,
															nextState -> {
																CssVerticalLayout component = predicateViewRenderer
																		.render(PredicateViewRenderer.Parameters
																				.builder()
																				.currentValue(nextState)
																				.onValueChange(stateObservable::set)
																				.build());
																component.setWidth("50em");
																return component;
															})
													.subscribeAlwaysAccepted(success -> parameters.onValueChange()
															.accept(computeNextPredicate(item, stateObservable.get())));
										});
								editButton.addThemeVariants(
										ButtonVariant.LUMO_SMALL,
										ButtonVariant.LUMO_TERTIARY);
								buttonLayout.add(editButton);
							}
							{
								Button deleteButton = new Button(
										VaadinIcon.CLOSE_SMALL.create(),
										event -> parameters.onValueChange().accept(
												computeNextPredicate(item, null)));
								deleteButton.setEnabled(item.getParent() != null);
								deleteButton.addThemeVariants(
										ButtonVariant.LUMO_SMALL,
										ButtonVariant.LUMO_TERTIARY);
								buttonLayout.add(deleteButton);
							}
							return buttonLayout;
						} else {
							throw new NotImplementedException();
						}
					})
					.setFlexGrow(0);
		}
		{
			Item rootItem = Item.builder()
					.index(null)
					.parent(null)
					.predicate(parameters.currentValue())
					.build();
			//
			grid.setItems(
					Set.of(rootItem),
					item -> {
						Collection<Item> elements;
						if (item.getPredicate() instanceof JunctionPredicate junctionPredicate) {
							elements = List.ofAll(junctionPredicate.predicates())
									.zipWithIndex()
									.map(tuple -> Item.builder()
											.index(tuple._2())
											.parent(item)
											.predicate(tuple._1())
											.build())
									.asJava();
						} else {
							elements = Set.of();
						}
						return elements;
					});
			grid.expandRecursively(Stream.of(rootItem), Integer.MAX_VALUE);
		}
		{
			grid.addCollapseListener(event -> {
				grid.getDataProvider().refreshAll();
			});
			grid.addExpandListener(event -> {
				grid.getDataProvider().refreshAll();
			});
		}
		return grid;
	}

	public static Predicate computeNextPredicate(Item item, @Nullable Predicate nextValue) {
		Predicate result;
		{
			if (item.getParent() != null) {
				result = computeNextPredicate(item.getParent(), withUpdatedElement(
						(JunctionPredicate) item.getParent().getPredicate(),
						item.getIndex(),
						nextValue));
			} else {
				result = nextValue;
			}
		}
		return result;
	}

	public static JunctionPredicate withUpdatedElement(
			JunctionPredicate predicate,
			int index,
			@Nullable Predicate elementPredicate) {
		return predicate.withUpdatedPredicates(predicates -> elementPredicate != null
				? List.ofAll(predicates).update(index, elementPredicate).asJava()
				: List.ofAll(predicates).removeAt(index).asJava());
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Item {

		/**
		 * Relevant for Junctions.
		 */
		@Nullable
		Integer index;

		/**
		 * Null for root.
		 */
		@Nullable
		Item parent;

		@Nullable
		Predicate predicate;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Predicate currentValue;

		@Nullable
		Consumer<Predicate> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
