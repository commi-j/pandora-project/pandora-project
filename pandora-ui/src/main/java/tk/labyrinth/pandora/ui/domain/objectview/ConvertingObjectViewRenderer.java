package tk.labyrinth.pandora.ui.domain.objectview;

import com.vaadin.flow.component.Component;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;

import java.lang.reflect.Type;

@Bean
@RequiredArgsConstructor
public class ConvertingObjectViewRenderer<T> implements GenericObjectViewRenderer {

	private final TypedObjectViewRenderer<T> typedObjectViewRenderer;

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private Pair<Class<T>, Type> javaClassAndType;

	@PostConstruct
	private void postConstruct() {
		Type parameterType = typedObjectViewRenderer.getParameterType();
		//
		javaClassAndType = Pair.of(TypeUtils.getClassInferred(parameterType), parameterType);
	}

	// TODO: Add error handling for direct/callback conversions.
	@Override
	public Component render(Parameters parameters) {
		T currentValue = converterRegistry.convertInferred(parameters.currentValue(), javaClassAndType.getRight());
		T initialValue = converterRegistry.convertInferred(parameters.initialValue(), javaClassAndType.getRight());
		//
		return typedObjectViewRenderer.render(builder -> builder
				.currentValue(currentValue)
				.initialValue(initialValue)
				.nonEditableReasons(parameters.nonEditableReasons())
				.onValueChange(nextValue -> parameters.onValueChange().accept(
						converterRegistry.convert(nextValue, GenericObject.class)))
				.build());
	}
}
