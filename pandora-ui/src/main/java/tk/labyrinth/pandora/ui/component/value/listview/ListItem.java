package tk.labyrinth.pandora.ui.component.value.listview;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.java.lang.EnumUtils;

import java.util.Objects;
import java.util.UUID;

@Value
public class ListItem<@Nullable T> {

	T inputValue;

	T outputValue;

	ListItemStatus status;

	UUID uid = UUID.randomUUID();

	public ListItem<T> asNotRemoved() {
		return ofNotModified(inputValue);
	}

	public ListItem<T> asRemoved() {
		return ofRemoved(inputValue);
	}

	public T getValue() {
		return outputValue != null ? outputValue : inputValue;
	}

	public boolean isRemoved() {
		return status == ListItemStatus.REMOVED;
	}

	public ListItem<T> withOutputValue(T outputValue) {
		return new ListItem<>(
				inputValue,
				outputValue,
				EnumUtils.in(status, ListItemStatus.MODIFIED, ListItemStatus.NOT_MODIFIED)
						? Objects.equals(inputValue, outputValue)
						? ListItemStatus.NOT_MODIFIED
						: ListItemStatus.MODIFIED
						: status);
	}

	public static <T> ListItem<T> ofAdded(T outputValue) {
		return new ListItem<>(null, outputValue, ListItemStatus.ADDED);
	}

	public static <T> ListItem<T> ofModified(T inputValue, T outputValue) {
		return new ListItem<>(inputValue, outputValue, ListItemStatus.MODIFIED);
	}

	public static <T> ListItem<T> ofNotExisting() {
		return new ListItem<>(null, null, ListItemStatus.NOT_EXISTING);
	}

	public static <T> ListItem<T> ofNotModified(T value) {
		return new ListItem<>(value, value, ListItemStatus.NOT_MODIFIED);
	}

	public static <T> ListItem<T> ofRemoved(T inputValue) {
		return new ListItem<>(inputValue, null, ListItemStatus.REMOVED);
	}
}
