package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRendererRegistry;

import javax.annotation.CheckForNull;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class ObjectToVaadinComponentRenderer extends ToVaadinComponentRendererBase<Object> {

	private final ToStringRendererRegistry toStringRendererRegistry;

	@Override
	protected Component renderNonNull(Context context, Object value) {
		Div result = new Div();
		{
			String renderedValue = toStringRendererRegistry.render(
					ToStringRendererRegistry.Context.builder()
							.datatype(context.getDatatype())
							.hints(context.getHints())
							.build(),
					value);
			result.add(Objects.requireNonNull(renderedValue));
		}
		return result;
	}

	@CheckForNull
	@Override
	public Integer getSupportDistance(Context context) {
		return ToVaadinComponentRenderer.MAX_DISTANCE;
	}
}
