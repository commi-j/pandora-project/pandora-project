package tk.labyrinth.pandora.ui.component.view.simple;

import com.vaadin.flow.component.Component;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.ui.component.view.View;

@Deprecated
@RequiredArgsConstructor(staticName = "of")
public class SimpleView implements View {

	private final Component component;

	@Override
	public Component asVaadinComponent() {
		return component;
	}
}
