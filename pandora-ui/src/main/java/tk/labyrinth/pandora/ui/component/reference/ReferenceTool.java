package tk.labyrinth.pandora.ui.component.reference;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import javax.annotation.Nullable;

public interface ReferenceTool<T, R extends Reference<T>> extends ReferenceRenderer<T, R> {

	@Nullable
	Predicate buildSearchPredicate(String text);
}
