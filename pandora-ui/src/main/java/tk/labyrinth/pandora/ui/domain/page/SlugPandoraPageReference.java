package tk.labyrinth.pandora.ui.domain.page;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class SlugPandoraPageReference implements Reference<PandoraPage> {

	String slug;

	public GenericReference<PandoraPage> toGenericReference() {
		return GenericReference.of(
				PandoraPage.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(PandoraPage.SLUG_ATTRIBUTE_NAME, slug)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				PandoraPage.MODEL_CODE,
				PandoraPage.SLUG_ATTRIBUTE_NAME,
				slug);
	}

	public static SlugPandoraPageReference from(GenericReference<?> reference) {
		if (!isSlugPandoraPageReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(PandoraPage.SLUG_ATTRIBUTE_NAME));
	}

	public static SlugPandoraPageReference from(PandoraPage value) {
		return of(value.getSlug());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static SlugPandoraPageReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static String getSlug(GenericReference<PandoraPage> slugPandoraPageReference) {
		return slugPandoraPageReference.getAttributeValue(PandoraPage.SLUG_ATTRIBUTE_NAME);
	}

	public static boolean isSlugPandoraPageReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), PandoraPage.MODEL_CODE) &&
				reference.hasAttribute(PandoraPage.SLUG_ATTRIBUTE_NAME);
	}
}
