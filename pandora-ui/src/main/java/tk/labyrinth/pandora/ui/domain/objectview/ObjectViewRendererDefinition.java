package tk.labyrinth.pandora.ui.domain.objectview;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@RootModel(primaryAttribute = ObjectViewRendererDefinition.SLUG_ATTRIBUTE_NAME)
@Value
@With
public class ObjectViewRendererDefinition {

	public static final String SLUG_ATTRIBUTE_NAME = "slug";

	@NonNull
	Integer distance;

	@NonNull
	GenericObjectViewRenderer objectViewRenderer;

	@Nullable
	Predicate predicate;
	//
	// TODO: Replace objectViewRenderer with this, then make it storable objects.
//	@NonNull
//	String renderFunction;

	@NonNull
	String slug;

	@NonNull
	String text;
}
