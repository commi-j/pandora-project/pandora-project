package tk.labyrinth.pandora.ui.component.genericobject;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.bean.contributor.PlainClassBeanContributor;
import tk.labyrinth.pandora.context.BeanFactory;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.object.custom.GenericGenericObjectFormCustomizer;

import java.lang.reflect.Type;
import java.util.List;

@LazyComponent
@RequiredArgsConstructor
public class GenericGenericObjectFormBeanContributor implements PlainClassBeanContributor<GenericGenericObjectForm> {

	public static final int DISTANCE = PARENT_SUPPORT_DISTANCE + 1;

	private final List<GenericGenericObjectFormCustomizer> objectFormCustomizers;

	@Nullable
	@Override
	public GenericGenericObjectForm doContributeBean(BeanFactory beanFactory, Type type) {
		GenericGenericObjectForm objectForm = beanFactory
				.withSuppressedBeanContributor(this)
				.getBean(GenericGenericObjectForm.class);
		//
		objectFormCustomizers.forEach(objectFormCustomizer -> objectFormCustomizer.customize(objectForm));
		//
		return objectForm;
	}

	@Nullable
	@Override
	public Integer doGetSupportDistance(BeanFactory beanFactory, Type type) {
		return DISTANCE;
	}
}
