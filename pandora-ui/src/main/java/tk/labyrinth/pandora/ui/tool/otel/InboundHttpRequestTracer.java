package tk.labyrinth.pandora.ui.tool.otel;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

// TODO: Decide if we need this one.
//@LazyComponent
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class InboundHttpRequestTracer extends GenericFilterBean {

	private final Tracer tracer;

	@Override
	public void doFilter(
			ServletRequest servletRequest,
			ServletResponse servletResponse,
			FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		//
		Span span = tracer.spanBuilder("do-filter")
				.setAttribute("uri", httpServletRequest.getRequestURI())
				.startSpan();
		try {
			filterChain.doFilter(servletRequest, servletResponse);
		} finally {
			span.end();
		}
	}
}
