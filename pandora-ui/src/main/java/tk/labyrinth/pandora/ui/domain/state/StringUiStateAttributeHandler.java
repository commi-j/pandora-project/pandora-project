package tk.labyrinth.pandora.ui.domain.state;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

@Value(staticConstructor = "of")
public class StringUiStateAttributeHandler implements UiStateAttributeHandler {

	String attributeName;

	@Getter(AccessLevel.NONE)
	@Nullable
	String defaultValue;

	UiStateAttributeKind kind;

	String queryAttributeName;

	@Nullable
	@Override
	public ValueWrapper fromQueryValue(@Nullable String queryValue) {
		return queryValue != null ? SimpleValueWrapper.of(queryValue) : null;
	}

	@Nullable
	@Override
	public ValueWrapper getDefaultValue() {
		return fromQueryValue(defaultValue);
	}

	@Nullable
	@Override
	public String toQueryValue(@Nullable ValueWrapper javaValue) {
		return javaValue != null ? javaValue.asSimple().getValue() : null;
	}
}
