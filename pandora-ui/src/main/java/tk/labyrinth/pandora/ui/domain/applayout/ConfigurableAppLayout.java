package tk.labyrinth.pandora.ui.domain.applayout;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.theme.lumo.Lumo;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.functionalcomponents.advanced.TreeGridWrapper;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.misc4j.java.net.UrlUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.CustomCssProperty;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Height;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Margin;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.OverflowY;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexboxAware;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.FlexDirection;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.PlaceSelf;
import tk.labyrinth.pandora.stores.objectsearcher.TypedObjectSearcher;
import tk.labyrinth.pandora.ui.domain.sidebar.PandoraSidebarItem;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarCategoryConfiguration;
import tk.labyrinth.pandora.ui.domain.sidebar.SidebarConfiguration;
import tk.labyrinth.pandora.uiapplication.navbar.PandoraNavbarLogoProvider;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.ArrayList;
import java.util.Objects;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class ConfigurableAppLayout extends Div implements BeforeEnterObserver, RouterLayout {

	private final AppLayoutConfigurationRegistry appLayoutConfigurationRegistry;

	private final PandoraNavbarLogoProvider pandoraNavbarLogoProvider;

	private final ConfigurableAppLayoutTool tool;

	private List<TreeGridWrapper<PandoraSidebarItem>> grids;

	private Consumer<Integer> selectCategoryCallback;

	private Consumer<Component> showContentConsumer;

	@SmartAutowired
	private TypedObjectSearcher<SidebarCategoryConfiguration> sidebarCategoryConfigurationSearcher;

	private void configure(PandoraAppLayoutConfiguration configuration) {
		Component sidebar = renderSidebar(configuration.getSidebar());
		//
		Component appLayout = switch (configuration.getSidebar().getMode()) {
			case SLIDE -> {
				SplitLayout splitLayout = new SplitLayout();
				{
					// Children
					//
					splitLayout.addToPrimary(sidebar);
				}
				{
					// Extra
					//
					showContentConsumer = components -> {
						splitLayout.addToSecondary(components);
						splitLayout.setSplitterPosition(20);
					};
				}
				yield splitLayout;
			}
			case SNAP -> {
				CssGridLayout gridLayout = new CssGridLayout();
				{
					// Style
					//
					gridLayout.setGridTemplateAreas(
							"sidebar content");
					gridLayout.setGridTemplateColumns("auto 1fr");
				}
				{
					// Children
					//
					gridLayout.add(sidebar, "sidebar");
				}
				{
					// Extra
					//
					showContentConsumer = content -> gridLayout.add(content, "content");
				}
				yield gridLayout;
			}
		};
		//
		{
			removeAll();
			//
			{
				StyleUtils.setCssProperty(sidebar, CustomCssProperty.of("min-width", "3em"));
				//
				StyleUtils.setCssProperty(appLayout, Height.HUNDRED_PERCENT);
				//
				add(appLayout);
			}
		}
	}

	private boolean handleSelected(TreeGridWrapper<PandoraSidebarItem> grid, String normalizedPath) {
		grid.getContent().setPartNameGenerator(item -> isSelected(item.getItem(), normalizedPath) ? "selected" : null);
		//
		return grid.getRootItems().flatMap(PandoraSidebarItem::flatten).exists(item -> isSelected(item, normalizedPath));
	}

	@PostConstruct
	private void postConstruct() {
		setHeightFull();
		//
		configure(appLayoutConfigurationRegistry.getAppLayoutConfigurationObservable().get());
	}

	private Component renderSidebar(SidebarConfiguration sidebarConfiguration) {
		CssGridLayout sidebar = new CssGridLayout();
		{
			sidebar.addClassName("pandora-sidebar");
			//
			sidebar.setGridTemplateAreas(
					"top",
					"centre",
					"bottom");
			sidebar.setGridTemplateRows("auto 1fr auto");
			//
			sidebar.getElement().getThemeList().add(Lumo.DARK);
			//
			StyleUtils.setCssProperty(sidebar, OverflowY.HIDDEN);
		}
		{
			{
				Component component = pandoraNavbarLogoProvider.provideNavbarComponents().get(0).getComponent();
				{
					component.addClassName(PandoraStyles.LAYOUT_SMALL);
				}
				sidebar.add(component, "top");
			}
			{
				List<SidebarCategoryConfiguration> sidebarCategoryConfigurations = sidebarConfiguration.getCategoryReferences()
						.map(sidebarCategoryConfigurationSearcher::getSingle);
				//
				Accordion accordion = new Accordion();
				{
					accordion.addClassName("sidebar");
					//
					CssFlexboxAware.setDisplayFlex(accordion);
					CssFlexboxAware.setFlexDirection(accordion, FlexDirection.COLUMN);
					//
					StyleUtils.setCssProperty(accordion, OverflowY.AUTO);
				}
				{
					ArrayList<TreeGridWrapper<PandoraSidebarItem>> grids = new ArrayList<>();
					//
					sidebarCategoryConfigurations.forEach(sidebarCategoryConfiguration -> accordion.add(new AccordionPanel(
							sidebarCategoryConfiguration.getText(),
							tool.renderSidebarCategory(sidebarCategoryConfiguration, grids::add))));
					//
					this.grids = List.ofAll(grids);
				}
				{
					selectCategoryCallback = accordion::open;
				}
				sidebar.add(accordion, "centre");
			}
			{
				Button button = ButtonRenderer.render(builder -> builder
						.icon(VaadinIcon.ANGLE_DOUBLE_LEFT.create())
						.build());
				//
				button.setHeight("2em");
				button.setWidth("2em");
				StyleUtils.setCssProperty(button, Margin.of("0.5em"));
				//
				CssGridItem.setJustifySelf(button, PlaceSelf.END);
				//
				button.addClickListener(event -> {
//				sidebarCollapsed = !sidebarCollapsed;
//				//
//				update(sidebar, button);
				});
				//
				sidebar.add(button, "bottom");
				//
//			update(sidebar, button);
			}
		}
		//
		return sidebar;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		String normalizedPath = UrlUtils.normalizePath(event.getLocation().getPath());
		//
		Integer selectedCategoryIndex = grids.map(grid -> handleSelected(grid, normalizedPath))
				.zipWithIndex()
				.filter(Tuple2::_1)
				.map(Tuple2::_2)
				.headOption()
				.getOrNull();
		//
		if (selectedCategoryIndex != null) {
			selectCategoryCallback.accept(selectedCategoryIndex);
		}
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		showContentConsumer.accept((Component) content);
	}

	public static boolean isSelected(PandoraSidebarItem item, String normalizedPath) {
		String itemPath = item.getTarget();
		//
		return Objects.equals(itemPath != null ? UrlUtils.normalizePath(itemPath) : null, normalizedPath);
	}
}
