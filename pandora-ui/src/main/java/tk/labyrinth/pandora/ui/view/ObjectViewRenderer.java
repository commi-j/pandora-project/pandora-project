package tk.labyrinth.pandora.ui.view;

import com.vaadin.flow.component.Component;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;

public interface ObjectViewRenderer<T> {

	Component render(Parameters<T> parameters);

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	class Parameters<T> {

		T currentValue;

		T initialValue;
	}
}
