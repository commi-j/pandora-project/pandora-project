package tk.labyrinth.pandora.ui.render;

import javax.annotation.Nullable;

/**
 * @param <T> Type
 * @param <R> Result
 *
 * @author Commitman
 */
public interface Renderer<T, R> {

	@Nullable
	default R render(@Nullable T value) {
		return value != null ? renderNonNull(value) : null;
	}

	@Nullable
	R renderNonNull(T value);
}
