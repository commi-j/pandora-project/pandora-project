package tk.labyrinth.pandora.ui.domain.state.model;

import tk.labyrinth.pandora.ui.domain.state.rework.UiStateAttribute2;

public class UiStateAttributeUtils {

	public static String computeAttributeName(UiStateAttribute annotation, String elementName) {
		String result;
		{
			String attributeName = annotation.attributeName();
			result = !attributeName.isEmpty() ? attributeName : elementName;
		}
		return result;
	}

	public static String computeAttributeName(UiStateAttribute2 annotation, String elementName) {
		String result;
		{
			String attributeName = annotation.attributeName();
			result = !attributeName.isEmpty() ? attributeName : elementName;
		}
		return result;
	}

	public static String computeQueryAttributeName(UiStateAttribute annotation, String elementName) {
		String result;
		{
			String queryAttributeName = annotation.queryAttributeName();
			if (!queryAttributeName.isEmpty()) {
				result = queryAttributeName;
			} else {
				String attributeName = annotation.attributeName();
				result = !attributeName.isEmpty() ? attributeName : elementName;
			}
		}
		return result;
	}

	public static String computeQueryAttributeName(UiStateAttribute2 annotation, String elementName) {
		String result;
		{
			String queryAttributeName = annotation.queryAttributeName();
			if (!queryAttributeName.isEmpty()) {
				result = queryAttributeName;
			} else {
				String attributeName = annotation.attributeName();
				result = !attributeName.isEmpty() ? attributeName : elementName;
			}
		}
		return result;
	}
}
