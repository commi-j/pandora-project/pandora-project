package tk.labyrinth.pandora.ui.component.view.confirmation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.progressbar.ProgressBar;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.view.View;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

@Deprecated
public class ConfirmationView extends CssVerticalLayout implements View {

	@Override
	public Component asVaadinComponent() {
		return this;
	}

	public void initialize(ConfirmationViewProperties properties, View view) {
		{
			addClassNames(PandoraStyles.LAYOUT);
			//
			setHeightFull();
		}
		{
			CssFlexItem.setFlexGrow(view.asVaadinComponent(), 1);
			add(view.asVaadinComponent());
		}
		{
			add(new ProgressBar(0, 1, 1));
		}
		{
			CssHorizontalLayout buttonFooter = new CssHorizontalLayout();
			{
				buttonFooter.addClassNames(PandoraStyles.LAYOUT);
			}
			{
				Button cancelButton = new Button("Cancel", event -> properties.getCloseCallback().run());
				cancelButton.getElement().setAttribute("name", ConfirmationViews.CANCEL_BUTTON_NAME);
				buttonFooter.add(cancelButton);
			}
			{
				buttonFooter.addStretch();
			}
			if (properties.getShowApplyButton()) {
				Button applyButton = new Button("Apply", event -> properties.getApplyCallback().get());
				applyButton.getElement().setAttribute("name", ConfirmationViews.APPLY_BUTTON_NAME);
				buttonFooter.add(applyButton);
			}
			{
				Button confirmButton = new Button(
						"Confirm",
						event -> {
							if (properties.getApplyCallback().get()) {
								properties.getCloseCallback().run();
							}
						});
				confirmButton.getElement().setAttribute("name", ConfirmationViews.CONFIRM_BUTTON_NAME);
				buttonFooter.add(confirmButton);
			}
			add(buttonFooter);
		}
	}
}
