package tk.labyrinth.pandora.ui.component.objecttable;

import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.RouteRegistry;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Deprecated
//@LazyComponent
@RequiredArgsConstructor
public class ObjectTablePageExposer implements ApplicationRunner {

	private final RouteRegistry routeRegistry;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		routeRegistry.setRoute(null, null, null);
		// TODO
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@ConfigurationProperties(prefix = "pandora.objectTableExposer")
	@Value
	@With
	public static class Properties {

		Boolean enabled;

		Class<? extends RouterLayout> layoutClass;

		String path;
	}
}
