package tk.labyrinth.pandora.ui.component.objectaction;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Color;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;

import java.util.Objects;

public abstract class TypedObjectActionProvider<T> implements GenericObjectActionProvider, TypeAware<T> {

	@Autowired
	private ConverterRegistry converterRegistry;

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	private Class<T> javaClass;

	private CodeObjectModelReference objectModelReference;

	@PostConstruct
	private void postConstruct() {
		javaClass = getParameterClass();
		objectModelReference = javaBaseTypeRegistry.getNormalizedObjectModelReference(javaClass);
	}

	@Nullable
	protected abstract Component provideActionForTypedObject(Context<T> context);

	@Override
	public @Nullable Component provideActionForGenericObject(Context<GenericObject> context) {
		Component result;
		{
			ObjectModel objectModel = context.getObjectModel();
			//
			if (Objects.equals(CodeObjectModelReference.from(objectModel), objectModelReference)) {
				result = Try
						.ofSupplier(() -> provideActionForTypedObject(Context.<T>builder()
								.datatype(context.getDatatype())
								.object(converterRegistry.convert(context.getObject(), javaClass))
								.objectModel(objectModel)
								.parentRefreshCallback(context.getParentRefreshCallback())
								.build()))
						.recover(fault -> {
							Span label = new Span(fault.toString());
							//
							label.setWidth("8em");
							StyleUtils.setCssProperty(label, Color.RED);
							//
							return label;
						})
						.get();
			} else {
				result = null;
			}
		}
		return result;
	}
}
