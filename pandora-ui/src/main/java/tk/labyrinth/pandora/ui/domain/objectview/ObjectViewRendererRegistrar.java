package tk.labyrinth.pandora.ui.domain.objectview;

import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.diversity.domain.bean.EagerBean;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.stores.extra.javabasetype.JavaBaseTypeRegistry2;

@EagerBean
@RequiredArgsConstructor
public class ObjectViewRendererRegistrar implements BeanFactoryAware {

	private final JavaBaseTypeRegistry2 javaBaseTypeRegistry;

	private final ObjectViewRendererRegistry objectViewRendererRegistry;

	private ConfigurableListableBeanFactory beanFactory;

	@PostConstruct
	private void postConstruct() {
		String[] beanNames = BeanFactoryUtils
				.beanNamesForAnnotationIncludingAncestors(beanFactory, RegisterObjectViewRenderer.class);
		//
		List.of(beanNames)
				.map(beanName -> ClassUtils.get(beanFactory.getBeanDefinition(beanName).getBeanClassName()))
				.map(beanClass -> {
					ObjectViewRendererDefinition result;
					{
						val annotation = MergedAnnotations.from(beanClass).get(RegisterObjectViewRenderer.class).synthesize();
						//
						String annotationText = annotation.text();
						String textToUse = !annotationText.isEmpty() ? annotationText : beanClass.getSimpleName();
						//
						String annotationSlug = annotation.slug();
						String slugToUse = !annotationSlug.isEmpty() ? annotationSlug : textToUse.toLowerCase();
						//
						val predicateAndRenderer = resolveRenderer(beanClass);
						//
						result = ObjectViewRendererDefinition.builder()
								.distance(annotation.distance())
								.objectViewRenderer(predicateAndRenderer.getRight())
								.predicate(predicateAndRenderer.getLeft())
								.slug(slugToUse)
								.text(textToUse)
								.build();
						//
					}
					return result;
				})
				.forEach(objectViewRendererRegistry::registerDefinition);
	}

	@SuppressWarnings("unchecked")
	private Pair<@Nullable Predicate, GenericObjectViewRenderer> resolveRenderer(Class<?> beanClass) {
		Pair<Predicate, GenericObjectViewRenderer> result;
		{
			if (GenericObjectViewRenderer.class.isAssignableFrom(beanClass)) {
				result = Pair.of(null, beanFactory.getBean((Class<? extends GenericObjectViewRenderer>) beanClass));
			} else if (TypedObjectViewRenderer.class.isAssignableFrom(beanClass)) {
				Class<?> modelClass = ParameterUtils.getFirstActualParameterAsClass(beanClass, TypedObjectViewRenderer.class);
				//
				result = Pair.of(
						Predicates.contains("datatypeBase.aliases", JavaBaseTypeUtils.createDatatypeAlias(modelClass)),
						beanFactory.getBean(ConvertingObjectViewRenderer.class, beanFactory.getBean(beanClass)));
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
	}
}
