package tk.labyrinth.pandora.ui.component.genericobject;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.functionalcomponents.box.StringBoxRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.ui.component.editor.EditableValue;
import tk.labyrinth.pandora.ui.component.value.box.mutable.FunctionalMutableValueBox;
import tk.labyrinth.pandora.ui.component.value.list.StandaloneVavrListView;
import tk.labyrinth.pandora.ui.component.valuewrapper.ValueWrapperBoxRenderer;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Consumer;

public class EditableGenericObjectViewRenderer {

	public static Component render(Properties properties) {
		StandaloneVavrListView<EditableGenericObjectAttribute> listView = new StandaloneVavrListView<>();
		//
		listView.render(StandaloneVavrListView.Properties.<EditableGenericObjectAttribute>builder()
				.currentValue(properties.getCurrentValue().getAttributes())
				.elementBoxSupplier(() -> new FunctionalMutableValueBox<>(
						elementProperties -> {
							EditableGenericObjectAttribute currentValue = elementProperties.getCurrentValue();
							CssHorizontalLayout elementBoxLayout = new CssHorizontalLayout();
							{
								elementBoxLayout.addClassNames(PandoraStyles.LAYOUT);
							}
							{
								{
									EditableValue<String> name = currentValue.getName();
									//
									TextField nameBox = StringBoxRenderer.render(builder -> builder
											.errorMessage(name.getValue() == null && name.getVisited()
													? "Require non-null"
													: null)
											.onBlur(valueChanged -> {
												if (!valueChanged) {
													elementProperties.getOnValueChange().accept(
															currentValue.withName(name.visited()));
												}
											})
											.onValueChange(nextValue -> elementProperties.getOnValueChange().accept(
													currentValue.withName(EditableValue.ofValue(nextValue).visited())))
											.value(name.getValue())
											.build());
									CssFlexItem.setFlexGrow(nameBox, 1);
									elementBoxLayout.add(nameBox);
								}
								{
									Component valueBox = ValueWrapperBoxRenderer.render(
											ValueWrapperBoxRenderer.Parameters.builder()
													.onValueChange(nextValue -> elementProperties.getOnValueChange()
															.accept(currentValue.withValue(
																	EditableValue.ofValue(nextValue).visited())))
													.value(currentValue.getValue().getValue())
													.build());
									CssFlexItem.setFlexGrow(valueBox, 1);
									elementBoxLayout.add(valueBox);
								}
							}
							return elementBoxLayout;
						},
						EditableGenericObjectAttribute.class))
				.initialValue(properties.getInitialValue().getAttributes())
				.newElementSupplier(EditableGenericObjectAttribute::empty)
				.onValueChange(nextValue -> properties.getOnValueChange().accept(EditableGenericObject.of(nextValue)))
				.build());
		//
		return listView.asVaadinComponent();
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		EditableGenericObject currentValue;

		@NonNull
		EditableGenericObject initialValue;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		Consumer<EditableGenericObject> onValueChange;
	}
}
