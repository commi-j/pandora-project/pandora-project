package tk.labyrinth.pandora.ui.domain.route;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.domain.pageroute.PageRoute;

@LazyComponent
@RequiredArgsConstructor
public class PandoraRouteFactory {

	@SmartAutowired
	private List<PandoraRouteContributor> routeContributors;

	public PageRoute createRouteForClass(Class<?> cl, Object hints) {
		// TODO
		return null;
	}
}
