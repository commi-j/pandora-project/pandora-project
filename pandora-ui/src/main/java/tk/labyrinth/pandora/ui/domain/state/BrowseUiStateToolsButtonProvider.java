package tk.labyrinth.pandora.ui.domain.state;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.ObjectProvider;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.dialog.Dialogs;
import tk.labyrinth.pandora.ui.component.json.JsonEditorRenderer;
import tk.labyrinth.pandora.uiapplication.sidebar.tools.ToolsButtonProvider;

import java.util.function.Consumer;

@LazyComponent
@RequiredArgsConstructor
public class BrowseUiStateToolsButtonProvider implements ToolsButtonProvider {

	private final ObjectMapper objectMapper;

	private final ObjectProvider<UiStateHandler> uiStateHandlerProvider;

	@Override
	public Pair<Integer, Consumer<ContextMenu>> provideToolsButton() {
		return Pair.of(
				1000,
				contextMenu -> contextMenu.addItem(
						"Browse UI State",
						event -> Dialogs.show(JsonEditorRenderer.render(builder -> builder
								.configurer(aceEditor -> aceEditor.setWidth("49em"))
								.readOnly(true)
								.value(objectMapper.convertValue(uiStateHandlerProvider.getObject().getState(), ObjectNode.class))
								.build()))));
	}
}
