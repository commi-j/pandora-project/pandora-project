package tk.labyrinth.pandora.ui.component.value.box.mutable;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxContributor;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;
import tk.labyrinth.pandora.ui.component.value.box.simple.InstantBox;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

import java.time.Instant;

@LazyComponent
@RequiredArgsConstructor
public class InstantValueBoxContributor implements ValueBoxContributor {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	public MutableValueBox<?> contributeValueBox(ValueBoxRegistry registry, ValueBoxRegistry.Context context) {
		return new SimpleMutableValueBox<>(new InstantBox());
	}

	@Nullable
	@Override
	public Integer getSupportDistance(ValueBoxRegistry.Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), Instant.class)
				? ToStringRenderer.MAX_DISTANCE_MINUS_THREE
				: null;
	}
}
