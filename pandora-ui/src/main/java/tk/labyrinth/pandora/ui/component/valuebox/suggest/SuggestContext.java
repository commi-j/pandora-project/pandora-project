package tk.labyrinth.pandora.ui.component.valuebox.suggest;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.ui.component.value.box.ValueBoxContext;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class SuggestContext {

	@NonNull
	ConverterRegistry converterRegistry;

	@Nullable
	String filter;

	ValueBoxContext valueBoxContext;

	@Nullable
	public ValueWrapper getAncestorValue(int depth) {
		return valueBoxContext.getAncestorValue(depth);
	}

	@Nullable
	public <T> T getAncestorValue(int depth, Class<T> javaClass) {
		return converterRegistry.convert(getAncestorValue(depth), javaClass);
	}
}
