package tk.labyrinth.pandora.ui.domain.state.model;

import com.vaadin.flow.component.HasElement;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.MergedAnnotations;
import reactor.core.Disposable;
import tk.labyrinth.pandora.functionalcomponents.core.FunctionalPage;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@LazyComponent
@RequiredArgsConstructor
public class UiStateAttributeFunctionalPageBeanPostProcessor implements BeanPostProcessor {

	private final ObjectProvider<TypedUiStateBinder<?>> uiStateBinderProvider;

	@SuppressWarnings("unchecked")
	private <P> void attachUiStateMirrorHandling(
			HasElement hasElement,
			Class<P> propertiesClass,
			Observable<P> propertiesObservable) {
		AtomicReference<Disposable> disposableReference = new AtomicReference<>(null);
		//
		hasElement.getElement().addAttachListener(event -> {
			TypedUiStateBinder<P> uiStateBinder = (TypedUiStateBinder<P>) uiStateBinderProvider.getObject();
			//
			disposableReference.set(uiStateBinder.bind(
					propertiesClass,
					propertiesObservable.get(),
					propertiesObservable::get,
					propertiesObservable::set,
					propertiesObservable.getFlux()));
		});
		//
		hasElement.getElement().addDetachListener(event -> Optional.ofNullable(disposableReference.get())
				.ifPresent(Disposable::dispose));
	}

	private <P> void processFunctionalPage(FunctionalPage<P> functionalPage) {
		//
		// Initializing content.
		functionalPage.getContent();
		//
		Type propertiesType = ParameterUtils.getFirstActualParameter(
				functionalPage.getClass(),
				FunctionalPage.class);
		Class<P> propertiesClass = TypeUtils.getClassInferred(propertiesType);
		//
		Observable<P> propertiesObservable = functionalPage.getPropertiesObservable();
		//
		List<Field> uiStateAttributeFields = List.of(propertiesClass.getDeclaredFields())
				.filter(field -> MergedAnnotations.from(field).isPresent(UiStateAttribute.class));
		//
		if (!uiStateAttributeFields.isEmpty()) {
			attachUiStateMirrorHandling(functionalPage, propertiesClass, propertiesObservable);
		}
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof FunctionalPage<?> functionalPage) {
			processFunctionalPage(functionalPage);
		}
		//
		return bean;
	}
}
