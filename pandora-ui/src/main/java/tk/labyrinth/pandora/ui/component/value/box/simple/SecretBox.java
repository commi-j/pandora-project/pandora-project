package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.PasswordField;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.datatypes.domain.secret.Secret;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.time.format.DateTimeParseException;
import java.util.function.Consumer;

@Slf4j
public class SecretBox implements SimpleValueBox<Secret> {

	private final PasswordField passwordField = new PasswordField();

	private boolean eventHandlingInProgress = false;

	private Properties<Secret> properties;

	private boolean renderInProgress = false;

	{
		passwordField.addValueChangeListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						passwordField.setValue(event.getOldValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					Consumer<Secret> onValueChange = properties.getOnValueChange();
					if (onValueChange != null) {
						try {
							onValueChange.accept(!event.getValue().isEmpty() ? Secret.of(event.getValue()) : null);
						} catch (DateTimeParseException ex) {
							// no-op
						}
					}
				}
			}
		});
	}

	private void doRender(Properties<Secret> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		passwordField.setLabel(properties.getLabel());
		passwordField.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				passwordField,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		passwordField.setValue(properties.getValue() != null ? properties.getValue().getValue() : "");
	}

	@Override
	public Component asVaadinComponent() {
		return passwordField;
	}

	@Override
	public void render(Properties<Secret> properties) {
		if (renderInProgress) {
			throw new IllegalStateException("Invoked render while already rendering");
		}
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}
}
