package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.TextDecoration;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

import javax.annotation.Nullable;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class ClassSignatureToVaadinComponentRenderer extends ToVaadinComponentRendererBase<ClassSignature> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected Component renderNonNull(Context context, ClassSignature value) {
		Div div = new Div();
		{
			String fullText = value.toString();
			String shortText = renderShortText(value);
			if (!Objects.equals(fullText, shortText)) {
				StyleUtils.setCssProperty(div, TextDecoration.UNDERLINE);
				ElementUtils.setTitle(div, fullText);
			}
			div.add(shortText);
		}
		return div;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.matches(context.getDatatype(), ClassSignature.class)
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_TWO
				: null;
	}

	public static String renderShortText(ClassSignature classSignature) {
		return classSignature.getLongName();
	}
}
