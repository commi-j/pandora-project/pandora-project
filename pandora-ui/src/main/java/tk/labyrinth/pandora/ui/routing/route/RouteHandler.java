package tk.labyrinth.pandora.ui.routing.route;

import com.vaadin.flow.router.BeforeEnterEvent;
import org.checkerframework.checker.nullness.qual.Nullable;

@Deprecated
public interface RouteHandler {

	@Nullable
	RouteModel findModel(BeforeEnterEvent beforeEnterEvent);
}
