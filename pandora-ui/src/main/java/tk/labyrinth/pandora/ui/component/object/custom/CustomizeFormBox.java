package tk.labyrinth.pandora.ui.component.object.custom;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD})
public @interface CustomizeFormBox {

	int MINUS_ONE_DUMMY_INTEGER = -1;

	String NON_EMPTY_DUMMY_STRING = "NON_EMPTY_DUMMY_STRING";

	/**
	 * @return positive or -1 as default
	 */
	int gridColumnSpan() default MINUS_ONE_DUMMY_INTEGER;

	/**
	 * @return positive or -1 as default
	 */
	int gridRowSpan() default MINUS_ONE_DUMMY_INTEGER;

	String nonEditableReason() default NON_EMPTY_DUMMY_STRING;

	String[] renderHints() default {};
}
