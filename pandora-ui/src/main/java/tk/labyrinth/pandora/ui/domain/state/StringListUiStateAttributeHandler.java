package tk.labyrinth.pandora.ui.domain.state;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

@Value(staticConstructor = "of")
public class StringListUiStateAttributeHandler implements UiStateAttributeHandler {

	String attributeName;

	@Getter(AccessLevel.NONE)
	@Nullable
	List<String> defaultValue;

	UiStateAttributeKind kind;

	String queryAttributeName;

	@Nullable
	@Override
	public ValueWrapper fromQueryValue(@Nullable String queryValue) {
		return queryValue != null
				? ListValueWrapper.of(List.of(queryValue.split(",")).map(SimpleValueWrapper::of))
				: null;
	}

	@Nullable
	@Override
	public ValueWrapper getDefaultValue() {
		return fromQueryValue(defaultValue != null ? defaultValue.mkString(",") : null);
	}

	@Nullable
	@Override
	public String toQueryValue(@Nullable ValueWrapper javaValue) {
		return javaValue != null
				? javaValue.asList().getValue()
				.map(ValueWrapper::asSimple)
				.map(SimpleValueWrapper::getValue)
				.mkString(",")
				: null;
	}
}
