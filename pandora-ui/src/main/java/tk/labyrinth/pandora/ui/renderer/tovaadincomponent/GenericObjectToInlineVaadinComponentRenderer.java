package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoVariables;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.objectmodel.ObjectModelRegistry;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectItem;
import tk.labyrinth.pandora.ui.renderer.custom.GenericObjectToItemRenderer;
import tk.labyrinth.pandora.ui.renderer.hint.InlineHint;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

/**
 * Renders object as a single line using {@link ObjectModel#getRenderRule()}.
 *
 * @see GenericObjectToVaadinComponentRenderer
 */
@LazyComponent
@RequiredArgsConstructor
public class GenericObjectToInlineVaadinComponentRenderer extends ToVaadinComponentRendererBase<GenericObject> {

	public static final int DISTANCE = GenericObjectToVaadinComponentRenderer.DISTANCE - 1;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericObjectToItemRenderer genericObjectToItemRenderer;

	private final ObjectMapper objectMapper;

	private final ObjectModelRegistry objectModelRegistry;

	@Override
	protected Component renderNonNull(Context context, GenericObject value) {
		Component result;
		{
			ObjectModel objectModel = objectModelRegistry.findObjectModelForDatatype(context.getDatatype());
			//
			GenericObjectItem genericObjectItem = genericObjectToItemRenderer.renderNonNull(objectModel, value);
			//
			result = switch (genericObjectItem.getState()) {
				case MODEL_NOT_FOUND -> new Span("M_N_F");
				case MODEL_REFERENCE_NOT_SPECIFIED -> new Span("M_R_N_S");
				case RENDER_RULE_NOT_SPECIFIED -> renderRenderRuleNot(objectModel, value, "Specified");
				case VALID -> new Span(genericObjectItem.getText());
			};
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		Integer result;
		{
			if (context.getHints().contains(InlineHint.INSTANCE)) {
				result = datatypeBaseRegistry.determineKind(context.getDatatype()) == DatatypeKind.OBJECT
						? DISTANCE
						: null;
			} else {
				result = null;
			}
		}
		return result;
	}

	public Component renderRenderRuleNot(ObjectModel objectModel, GenericObject object, String details) {
		CssGridLayout result = new CssGridLayout();
		{
			result.setAlignItems(AlignItems.BASELINE);
			result.setGridTemplateAreas(". end");
			result.setGridTemplateColumns("1fr auto");
		}
		{
			Span span = new Span("Render Rule Not %s".formatted(details));
			{
				span.getStyle().set("grid-column", "1 / span 2");
				span.getStyle().set("grid-row", "1 / 1");
				//
				span.setWhiteSpace(HasText.WhiteSpace.NOWRAP);
			}
			result.add(span);
		}
		{
			Icon icon = VaadinIcon.EXCLAMATION_CIRCLE.create();
			icon.setColor(LumoVariables.WARNING_COLOUR);
			//
			Button viewFaultButton = new Button(
					icon,
					event -> {
						//
						Dialog dialog = new Dialog();
						{
							dialog.setMinWidth("80%");
						}
						{
							CssVerticalLayout layout = new CssVerticalLayout();
							{
								layout.addClassNames(PandoraStyles.LAYOUT);
							}
							{
								{
									// TODO: Link to ObjectModel
									TextField textField = new TextField();
									{
										textField.setReadOnly(true);
										textField.setValue(objectModel.getName());
									}
									layout.add(textField);
								}
								{
									TextArea textArea = new TextArea();
									{
										textArea.setReadOnly(true);
										textArea.setValue(IoUtils.unchecked(() -> objectMapper
												.writerWithDefaultPrettyPrinter()
												.writeValueAsString(object)));
										textArea.setWidthFull();
									}
									layout.add(textArea);
								}
							}
							dialog.add(layout);
						}
						dialog.open();
					});
			viewFaultButton.addThemeVariants(
					ButtonVariant.LUMO_SMALL,
					ButtonVariant.LUMO_TERTIARY);
			result.add(viewFaultButton, "end");
		}
		return result;
	}
}
