package tk.labyrinth.pandora.ui.component.table;

import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Duration;
import java.time.Instant;

@Value(staticConstructor = "of")
@With
public class TimeWindow {

	@Nullable
	Instant finishedAt;

	@NonNull
	Instant startedAt;

	Duration getDuration() {
		return Duration.between(startedAt, finishedAt);
	}
}
