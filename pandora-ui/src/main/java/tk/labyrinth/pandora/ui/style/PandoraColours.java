package tk.labyrinth.pandora.ui.style;

/**
 * https://clrs.cc/
 * src/main/resources/META-INF/resources/frontend/style/pandora-colours.css
 */
@Deprecated
public class PandoraColours {

	public static final String BLUE = "var(--pandora-blue)";

	public static final String BLUE_50 = "var(--pandora-blue-50)";

	public static final String GREEN = "var(--pandora-green)";

	public static final String GREEN_50 = "var(--pandora-green-50)";

	public static final String RED = "var(--pandora-red)";

	public static final String RED_50 = "var(--pandora-red-50)";
}
