package tk.labyrinth.pandora.ui.component.table;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class AdvancedTableSearchParameters {

	Integer pageIndex;

	Integer pageSize;

	@Nullable
	Predicate predicate;

	@Nullable
	Object sort;
}
