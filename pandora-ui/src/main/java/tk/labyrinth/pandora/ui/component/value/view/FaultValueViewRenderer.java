package tk.labyrinth.pandora.ui.component.value.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasText;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.io.output.StringBuilderWriter;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoVariables;
import tk.labyrinth.pandora.ui.component.TextDisplayer;

import java.io.PrintWriter;

public class FaultValueViewRenderer {

	public static Component render(Properties properties) {
		CssGridLayout result = new CssGridLayout();
		{
			result.setAlignItems(AlignItems.BASELINE);
			result.setGridTemplateAreas(". end");
			result.setGridTemplateColumns("1fr auto");
			//
//			result.getStyle().set("overflow-x", "clip");
		}
		{
			Span label = new Span(properties.getRawValue());
			{
				label.getStyle().set("grid-column", "1 / span 2");
				label.getStyle().set("grid-row", "1 / 1");
				//
				label.setWhiteSpace(HasText.WhiteSpace.NOWRAP);
			}
			result.add(label);
		}
		{
			Icon icon = VaadinIcon.EXCLAMATION_CIRCLE.create();
			icon.setColor(LumoVariables.ERROR_COLOUR);
			//
			Button viewFaultButton = new Button(
					icon,
					event -> {
						StringBuilder stringBuilder = new StringBuilder();
						properties.getFault().printStackTrace(new PrintWriter(new StringBuilderWriter(stringBuilder)));
						TextDisplayer.display(
								"%s\n%s".formatted(properties.getFault().toString(), stringBuilder),
								"80%");
					});
			viewFaultButton.addThemeVariants(
					ButtonVariant.LUMO_SMALL,
					ButtonVariant.LUMO_TERTIARY);
			result.add(viewFaultButton, "end");
		}
		return result;
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties {

		Throwable fault;

		String rawValue;
	}
}
