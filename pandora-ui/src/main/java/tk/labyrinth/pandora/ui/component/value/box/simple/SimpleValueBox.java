package tk.labyrinth.pandora.ui.component.value.box.simple;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.ui.component.view.RenderableView;

import java.util.function.Consumer;

public interface SimpleValueBox<T> extends RenderableView<SimpleValueBox.Properties<T>>, TypeAware<T> {

	@Builder(toBuilder = true)
	@Value
	@With
	class Properties<T> {

		@Nullable
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<T> onValueChange;

		@Nullable
		T value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
