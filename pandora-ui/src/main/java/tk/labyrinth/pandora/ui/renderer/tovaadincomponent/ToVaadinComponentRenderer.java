package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.core.meta.HasSupportDistance;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.core.meta.PandoraJavaContext;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;
import tk.labyrinth.pandora.ui.renderer.tostring.ToStringRenderer;

import java.util.function.Function;

@PandoraExtensionPoint
@PandoraJavaContext
public interface ToVaadinComponentRenderer<T> extends
		HasSupportDistance<ToVaadinComponentRenderer.Context>,
		TypeAware<T> {

	Integer MAX_DISTANCE = ToStringRenderer.MAX_DISTANCE;

	Integer MAX_DISTANCE_MINUS_ONE = ToStringRenderer.MAX_DISTANCE_MINUS_ONE;

	Integer MAX_DISTANCE_MINUS_TWO = ToStringRenderer.MAX_DISTANCE_MINUS_TWO;

	Integer MAX_DISTANCE_MINUS_THREE = ToStringRenderer.MAX_DISTANCE_MINUS_THREE;

	default <V> Renderer<V> asVaadinRenderer(Context context, Function<V, T> mappingFunction) {
		return new ComponentRenderer<>(item -> render(context, mappingFunction.apply(item)));
	}

	// Overrides to rename parameter.
	@Nullable
	@Override
	Integer getSupportDistance(Context context);

	@WithSpan
	Component render(Context context, @Nullable T value);

	@Builder(toBuilder = true)
	@Value
	@With
	class Context {

		@NonNull
		Datatype datatype;

		@NonNull
		List<Object> hints;

		ToVaadinComponentRendererRegistry rendererRegistry;

		public ToVaadinComponentRendererRegistry.Context toRegistryContext() {
			return ToVaadinComponentRendererRegistry.Context.builder()
					.datatype(datatype)
					.hints(hints)
					.build();
		}
	}
}
