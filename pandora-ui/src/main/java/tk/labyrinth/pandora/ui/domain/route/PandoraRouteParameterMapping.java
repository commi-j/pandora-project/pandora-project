package tk.labyrinth.pandora.ui.domain.route;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class PandoraRouteParameterMapping {

	String fromSchema;

	String to;
}
