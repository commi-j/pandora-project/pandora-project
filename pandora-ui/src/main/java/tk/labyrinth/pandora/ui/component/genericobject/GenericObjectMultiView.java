package tk.labyrinth.pandora.ui.component.genericobject;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectFilterer;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.pandora.ui.domain.objectview.ObjectViewRendererDefinition;
import tk.labyrinth.pandora.ui.domain.objectview.ObjectViewRendererRegistry;

import java.util.function.Consumer;

/**
 * Require ObjectModel in context.
 */
@PrototypeScopedComponent
@RequiredArgsConstructor
public class GenericObjectMultiView implements RenderableView<GenericObjectMultiView.Properties> {

	private final ConverterRegistry converterRegistry;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private final GenericObjectFilterer genericObjectFilterer;

	private final CssVerticalLayout layout = new CssVerticalLayout();

	private final ObjectViewRendererRegistry objectViewRendererRegistry;

	private final Observable<State> stateObservable = Observable.notInitialized();

	// TODO: Move to the parameters (along with refactoring GenericObjectForm the same way).
	@SmartAutowired
	private ObjectModel objectModel;

	@PostConstruct
	private void postConstruct() {
		DatatypeBase datatypeBase = datatypeBaseRegistry.findDatatypeBase(
				ObjectModelUtils.createDatatypeBaseReference(objectModel));
		//
		List<String> objectModelAttributes = objectModel.getAttributesOrEmpty().map(ObjectModelAttribute::getName);
		//
		GenericObject context = GenericObject.of(
				GenericObjectAttribute.ofObject(
						"datatypeBase",
						converterRegistry.convert(datatypeBase, GenericObject.class)),
				GenericObjectAttribute.ofObject(
						"objectModel",
						converterRegistry.convert(objectModel, GenericObject.class)));
		//
		List<ObjectViewRendererDefinition> definitions = objectViewRendererRegistry.getDefinitions()
				.filter(definition -> definition.getPredicate() == null ||
						genericObjectFilterer.matches(context, definition.getPredicate()))
				.sortBy(ObjectViewRendererDefinition::getDistance);
		//
		Tabs tabs = new Tabs();
		{
			{
				tabs.setAutoselect(false);
				tabs.addSelectedChangeListener(event -> stateObservable.update(currentState ->
						currentState.withSelectedTabIndex(tabs.getSelectedIndex())));
			}
			{
				definitions.forEach(definition -> tabs.add(new Tab(definition.getText())));
			}
		}
		{
			stateObservable.subscribe(nextState -> {
				layout.removeAll();
				//
				try {
					ObjectViewRendererDefinition definition = definitions.get(nextState.selectedTabIndex());
					//
					GenericObject rawCurrentValue = nextState.properties().currentValue();
					GenericObject rawInitialValue = nextState.properties().initialValue();
					//
					GenericObject currentValue = rawCurrentValue.withRetainedAttributes(objectModelAttributes);
					GenericObject initialValue = rawInitialValue.withRetainedAttributes(objectModelAttributes);
					GenericObject unmodifiableValue = rawInitialValue.withoutAttributes(objectModelAttributes);
					//
					Component component = definition.getObjectViewRenderer().render(builder -> builder
							.currentValue(currentValue)
							.datatype(nextState.properties().datatype())
							.initialValue(initialValue)
							.nonEditableReasons(nextState.properties().nonEditableReasons())
							.objectModel(objectModel)
							.onValueChange(nextValue -> {
								GenericObject mergedValue = nextValue.withMergedAttributes(unmodifiableValue.getAttributes());
								//
								if (nextState.properties().onValueChange() != null) {
									nextState.properties().onValueChange().accept(mergedValue);
								} else {
									stateObservable.set(nextState.withProperties(nextState.properties().withCurrentValue(mergedValue)));
								}
							})
							.unmodifiableValue(unmodifiableValue)
							.build());
					//
					layout.add(tabs);
					layout.add(component);
					//
					// This is actually required only for the first render.
					tabs.setSelectedIndex(nextState.selectedTabIndex());
				} catch (RuntimeException ex) {
					throw new RuntimeException(ex);
				}
			});
		}
	}

	@Override
	public Component asVaadinComponent() {
		return layout;
	}

	public GenericObject getValue() {
		State state = stateObservable.get();
		//
		return state.properties().currentValue();
	}

	@Override
	public void render(Properties properties) {
		stateObservable.update(currentState -> State.builder()
				.properties(properties)
				.selectedTabIndex(currentState != null ? currentState.selectedTabIndex() : 0)
				.build());
	}

	static GenericObject enrichNextObject(
			ObjectModel objectModel,
			GenericObject initialObject,
			GenericObject nextObject) {
		return nextObject;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		GenericObject currentValue;

		@NonNull
		Datatype datatype;

		@NonNull
		GenericObject initialValue;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();
//
//		TODO:
//		@NonNull
//		ObjectModel objectModel;

		// TODO: Make it required and get rid of internal value management.
		@Nullable
		Consumer<GenericObject> onValueChange;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
		Properties properties;

		@NonNull
		Integer selectedTabIndex;
	}
}
