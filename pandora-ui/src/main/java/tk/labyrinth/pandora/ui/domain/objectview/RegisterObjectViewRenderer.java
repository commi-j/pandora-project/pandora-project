package tk.labyrinth.pandora.ui.domain.objectview;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RegisterObjectViewRenderer {

	int distance() default 0;

	String slug() default "";

	@AliasFor("value")
	String text() default "";

	@AliasFor("text")
	String value() default "";
}
