package tk.labyrinth.pandora.ui.renderer.tostring;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;

import javax.annotation.CheckForNull;

@Deprecated // FIXME: Do we need this one at all?
//@LazyComponent
@RequiredArgsConstructor
public class EnumToStringRenderer extends ToStringRendererBase<SimpleValueWrapper> {

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	@Override
	protected String renderNonNull(Context context, SimpleValueWrapper value) {
		return value.unwrap();
	}

	@CheckForNull
	@Override
	public Integer getSupportDistance(Context context) {
//		return DatatypeBaseTag.hasTag(datatypeBaseRegistry.findDatatypeBase(
//						context.getDatatype()),
//				PandoraDatatypeBaseTag.ENUM)
//				? ToStringRenderer.MAX_DISTANCE_MINUS_ONE
//				: null;
		return null;
	}
}
