package tk.labyrinth.pandora.ui.component;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.textfield.TextArea;

public class TextDisplayer {

	public static void display(String text) {
		display(text, "40%");
	}

	public static void display(String text, String minWidth) {
		Dialog dialog = new Dialog();
		{
			dialog.setMinWidth(minWidth);
		}
		{
			TextArea textArea = new TextArea();
			{
//				textArea.getElement().getChild(0).getStyle().set("white-space", "pre");
			}
			{
				textArea.setReadOnly(true);
				textArea.setValue(text);
				textArea.setWidthFull();
			}
			dialog.add(textArea);
		}
		dialog.open();
	}
}
