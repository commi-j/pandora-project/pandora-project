package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.stores.datatypes.datatypebase.DatatypeBaseRegistry;
import tk.labyrinth.pandora.ui.renderer.hint.InlineHint;

@Bean
@RequiredArgsConstructor
public class ListToVaadinComponentRenderer extends ToVaadinComponentRendererBase<List<?>> {

	public static final Object HORIZONTAL_HINT = new HorizontalHint();

	public static final Object LIST_ELEMENT_HINT = new ListElementHint();

	public static final int LARGE_THRESHOLD = 5;

	private final DatatypeBaseRegistry datatypeBaseRegistry;

	private int computeThreshold(Context context) {
		return context.getHints()
				.filter(ExpandThresholdHint.class::isInstance)
				.headOption()
				.map(ExpandThresholdHint.class::cast)
				.map(ExpandThresholdHint::getThreshold)
				.getOrElse(LARGE_THRESHOLD);
	}

	@Override
	protected Component renderNonNull(Context context, List<?> value) {
		Component result;
		{
			int threshold = computeThreshold(context);
			int thresholdMinusOne = threshold - 1;
			//
			boolean large = value.size() > threshold;
			//
			val elementContext = context
					.withDatatype(context.getDatatype().getParameters().get(0))
					.withHints(context.getHints()
							.append(InlineHint.INSTANCE)
							.append(LIST_ELEMENT_HINT))
					.toRegistryContext();
			//
			List<Component> renderedElements = value
					.take(large ? thresholdMinusOne : threshold)
					.map(element -> context.getRendererRegistry().render(elementContext, element));
			//
			if (context.getHints().contains(HORIZONTAL_HINT)) {
				CssHorizontalLayout layout = new CssHorizontalLayout();
				{
					StyleUtils.setCssProperty(layout, WhiteSpace.PRE_WRAP);
					//
					layout.setAlignItems(AlignItems.BASELINE);
				}
				{
					renderedElements
							.intersperse(null)
							.zipWithIndex()
							.forEach(tuple -> {
								if (tuple._2() % 2 == 0) {
									layout.add(tuple._1());
								} else {
									layout.add(", ");
								}
							});
					//
					if (large) {
						layout.add(", ");
						layout.add(new Span("%s more".formatted(value.size() - thresholdMinusOne)));
					}
				}
				result = layout;
			} else {
				CssVerticalLayout layout = new CssVerticalLayout();
				{
					layout.setAlignItems(AlignItems.BASELINE);
				}
				{
					renderedElements.forEach(layout::add);
					//
					if (large) {
						layout.add(new Span("%s more".formatted(value.size() - thresholdMinusOne)));
					}
				}
				result = layout;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getSupportDistance(Context context) {
		return datatypeBaseRegistry.determineKind(context.getDatatype()) == DatatypeKind.LIST
				? ToVaadinComponentRenderer.MAX_DISTANCE_MINUS_ONE
				: null;
	}

	/**
	 * This hint specifies how much elements are visible without hiding remaining ones with "more".
	 * If not specified, default value of 5 is used.
	 */
	@Value(staticConstructor = "of")
	public static class ExpandThresholdHint {

		/**
		 * Positive.
		 */
		int threshold;
	}

	/**
	 * This hint changes rendering to be horizontal rather than vertical.
	 */
	public static class HorizontalHint {
		// empty
	}

	/**
	 * This hint is added to render invocations for elements.
	 */
	public static class ListElementHint {
		// empty
	}
}
