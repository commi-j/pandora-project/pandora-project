package tk.labyrinth.pandora.ui.component.genericobject;

import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.typedobject.TypedObjectEditorDialogHandler;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

/**
 * @see TypedObjectEditorDialogHandler
 */
@LazyComponent
@RequiredArgsConstructor
public class GenericObjectEditorDialogHandler {

	private final BeanContext beanContext;

	public ConfirmationHandle<GenericObject> showDialog(Parameters parameters) {
		GenericObjectForm genericObjectForm = beanContext.getBeanFactory()
				.withBean(parameters.objectModel())
				.getBean(GenericObjectForm.class);
		//
		Observable<GenericObject> currentValueObservable = Observable.withInitialValue(parameters.currentValue());
		//
		currentValueObservable.subscribe(nextCurrentValue -> genericObjectForm.render(
				ObjectForm.Properties.<GenericObject>builder()
						.currentValue(nextCurrentValue)
						.initialValue(parameters.initialValue())
						.onValueChange(currentValueObservable::set)
						.build()));
		//
		return ConfirmationViews.showViewDialog(genericObjectForm).map(success -> currentValueObservable.get());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		GenericObject currentValue;

		@NonNull
		GenericObject initialValue;

		@NonNull
		ObjectModel objectModel;
	}
}
