package tk.labyrinth.pandora.ui.renderer.tostring;

import org.springframework.stereotype.Component;

@Component
public class ObjectToStringRenderer extends ToStringRendererBase<Object> {

	@Override
	protected String renderNonNull(Context context, Object value) {
		return value.toString();
	}

	@Override
	public Integer getSupportDistance(Context context) {
		return ToStringRenderer.MAX_DISTANCE;
	}
}
