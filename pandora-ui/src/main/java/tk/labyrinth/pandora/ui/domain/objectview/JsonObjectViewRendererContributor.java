package tk.labyrinth.pandora.ui.domain.objectview;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.diversity.domain.bean.EagerBean;

@EagerBean
@RequiredArgsConstructor
public class JsonObjectViewRendererContributor {

	private final JsonObjectViewRenderer jsonObjectViewRenderer;

	private final ObjectViewRendererRegistry objectViewRendererRegistry;

	@PostConstruct
	private void postConstruct() {
		objectViewRendererRegistry.registerDefinition(ObjectViewRendererDefinition.builder()
				.distance(900)
				.objectViewRenderer(parameters -> jsonObjectViewRenderer.render(builder -> builder
						.currentValue(parameters.currentValue())
						.initialValue(parameters.initialValue())
						.nonEditableReasons(parameters.nonEditableReasons())
						.objectModel(parameters.objectModel())
						.onValueChange(parameters.onValueChange())
						.build()))
				.slug("json")
				.text("JSON")
				.build());
	}
}
