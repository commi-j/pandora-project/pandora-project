package tk.labyrinth.pandora.ui.navigation;

import com.vaadin.flow.router.Location;

/**
 * @param <N> NavigationTarget
 */
public interface NavigationTargetHandler<N> {

	Location toLocation(N navigationTarget);
}
