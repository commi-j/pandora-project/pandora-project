package tk.labyrinth.pandora.ui.component.object.custom;

import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridItem;
import tk.labyrinth.pandora.ui.component.genericobject.GenericGenericObjectForm;
import tk.labyrinth.pandora.ui.component.value.box.mutable.MutableValueBox;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.Objects;
import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
public class AnnotationBasedGenericGenericObjectFormCustomizer implements GenericGenericObjectFormCustomizer {

	private final JavaBaseTypeRegistry javaBaseTypeRegistry;

	private void customizeBox(MutableValueBox<?> attributeBox, MergedAnnotation<CustomizeFormBox> customizeBox) {
		{
			int gridColumnSpan = customizeBox.getInt("gridColumnSpan");
			if (gridColumnSpan != CustomizeFormBox.MINUS_ONE_DUMMY_INTEGER) {
				CssGridItem.setGridColumnSpan(attributeBox.asVaadinComponent(), gridColumnSpan);
			}
		}
		{
			String nonEditableReason = customizeBox.getString("nonEditableReason");
			if (!Objects.equals(nonEditableReason, CustomizeFormBox.NON_EMPTY_DUMMY_STRING)) {
				// FIXME: Find a way for it to work after refactoring.
//				attributeBox.addNonEditableReason(nonEditableReason);
			}
		}
	}

	private void customizeLayout(
			GenericGenericObjectForm objectForm,
			MergedAnnotation<CustomizeFormLayout> customizeLayout) {
		{
			String[] boxOrder = customizeLayout.getStringArray("boxOrder");
			if (boxOrder.length > 0) {
				objectForm.getLayout().removeAll();
				//
				Stream.of(boxOrder).forEach(boxOrderElement ->
						objectForm.getLayout().add(objectForm.getAttributeBox(boxOrderElement).asVaadinComponent()));
			}
		}
		{
			Integer gridColumnNumber = Optional.of(customizeLayout.getInt("gridColumnNumber"))
					.filter(value -> !Objects.equals(value, CustomizeFormLayout.MINUS_ONE_DUMMY_INTEGER))
					.orElse(null);
			String gridTemplateColumns = StringUtils.emptyToNull(customizeLayout.getString(
					"gridTemplateColumns"));
			//
			if (gridColumnNumber != null && gridTemplateColumns != null) {
				throw new IllegalStateException(
						"Only one of 'gridColumnNumber' and 'gridTemplateColumns' should be specified");
			}
			//
			if (gridColumnNumber != null) {
				if (gridColumnNumber < 1) {
					throw new IllegalStateException("Require positive gridColumnNumber: %s"
							.formatted(gridColumnNumber));
				}
				objectForm.getLayout().setGridTemplateColumns(PandoraStyles.repeatDefaultCellWidth(gridColumnNumber));
			}
			if (gridTemplateColumns != null) {
				objectForm.getLayout().setGridTemplateColumns(gridTemplateColumns);
			}
		}
	}

	@Override
	public void customize(GenericGenericObjectForm objectForm) {
		JavaBaseType javaBaseType = javaBaseTypeRegistry.findJavaBaseType(objectForm.getObjectModel());
		if (javaBaseType != null) {
			Class<?> javaClass = javaBaseType.resolveClass();
			//
			MergedAnnotation<CustomizeFormLayout> customizeLayout = MergedAnnotations.from(javaClass)
					.get(CustomizeFormLayout.class);
			if (customizeLayout.isPresent()) {
				customizeLayout(objectForm, customizeLayout);
			}
			Stream
					.concat(
							Stream.of(javaClass.getDeclaredFields()),
							Stream.of(javaClass.getDeclaredMethods()))
					.filter(member -> ObjectModelUtils.findAttributeName(member) != null)
					.forEach(member -> {
						MergedAnnotation<CustomizeFormBox> customizeBox = MergedAnnotations.from(member)
								.get(CustomizeFormBox.class);
						if (customizeBox.isPresent()) {
							customizeBox(
									objectForm.getAttributeBox(ObjectModelUtils.getAttributeName(member)),
									customizeBox);
						}
					});
		}
	}
}
