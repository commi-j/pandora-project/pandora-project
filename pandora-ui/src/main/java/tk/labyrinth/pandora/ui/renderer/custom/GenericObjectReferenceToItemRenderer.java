package tk.labyrinth.pandora.ui.renderer.custom;

import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.reference.ReferenceTool;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ModelCodeAndAttributeNamesReferenceModelReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectItem;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectReferenceItem;

@LazyComponent
@RequiredArgsConstructor
public class GenericObjectReferenceToItemRenderer {

	private final GenericObjectSearcher genericObjectSearcher;

	private final GenericObjectToItemRenderer objectToItemRenderer;

	private final ReferenceTool referenceTool;

	@Nullable
	public GenericObjectReferenceItem render(@Nullable GenericObjectReference reference) {
		GenericObjectReferenceItem result;
		{
			if (reference != null) {
				result = renderNonNull(reference);
			} else {
				result = null;
			}
		}
		return result;
	}

	public GenericObjectReferenceItem renderNonNull(GenericObjectReference reference) {
		GenericObjectReferenceItem result;
		{
			try {
				GenericObjectItem objectItem;
				{
					GenericObject object = genericObjectSearcher.findSingle(reference);
					//
					if (object != null) {
						objectItem = objectToItemRenderer.renderNonNull(null, object);
					} else {
						objectItem = null;
					}
				}
				result = GenericObjectReferenceItem.of(null, objectItem, reference);
			} catch (RuntimeException ex) {
				result = GenericObjectReferenceItem.of(ex, null, reference);
			}
		}
		return result;
	}

	public GenericObjectReferenceItem renderNonNull(
			ModelCodeAndAttributeNamesReferenceModelReference objectReferenceModelReference,
			GenericObject object) {
		GenericObjectReferenceItem result;
		{
			GenericObjectReference objectReference = referenceTool.createReferenceToGenericObject(
					objectReferenceModelReference,
					object);
			try {
				GenericObjectItem objectItem = objectToItemRenderer.renderNonNull(null, object);
				result = GenericObjectReferenceItem.of(null, objectItem, objectReference);
			} catch (RuntimeException ex) {
				result = GenericObjectReferenceItem.of(ex, null, objectReference);
			}
		}
		return result;
	}
}
