package tk.labyrinth.pandora.ui.component.value.listview;

import com.vaadin.flow.component.grid.Grid;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.ui.component.value.box.ValueBox;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.function.Supplier;

@Deprecated
@PrototypeScopedComponent
@RequiredArgsConstructor
public class ValueBoxBasedListView<T> extends ValueListViewBase<T> {

	// FIXME: Should be set by factory, not manually, but this require dynamic beans.
//	@Setter
	private final Supplier<ValueBox<T>> valueBoxSupplier;

	private Type elementType;

	@Nullable
	@Setter
	private Supplier<T> newElementInstanceCreator = null;

	@PostConstruct
	private void postConstruct() {
		elementType = valueBoxSupplier.get().getParameterType();
	}

	@Override
	protected void configureGrid(Grid<ListItem<T>> grid) {
		grid
				.addComponentColumn(component(pair -> valueBoxSupplier.get()
						.withChangeListener(next -> updateItem(pair.getLeft(), next))
						.withValue(pair.getValue())
						.asVaadinComponent()));
	}

	@Override
	protected T createNewElementInstance() {
		return newElementInstanceCreator != null ? newElementInstanceCreator.get() : null;
	}

	@Override
	public Type getParameterType() {
		return elementType;
	}
}
