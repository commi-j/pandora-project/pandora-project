package tk.labyrinth.pandora.ui.component.table;

import com.vaadin.flow.component.Component;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class AdvancedTableColumnDescriptor {

	String attributeName;

	String displayName;

	Integer flexGrow;

	@Nullable
	Component header;

	Boolean visible;
}
