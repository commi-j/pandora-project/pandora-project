package tk.labyrinth.pandora.ui.domain.sidebar;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.init.GenericObjectsContributingInitializer;

@Bean
public class PandoraSidebarCategoryContributingInializer extends GenericObjectsContributingInitializer {

	@Override
	protected List<GenericObject> contributeObjects() {
		return List
				.of(
						SidebarCategoryConfiguration.builder()
								.items(List.of(
										PandoraSidebarItem.builder()
												.target("/pandora/routes")
												.text("Routes")
												.build(),
										PandoraSidebarItem.builder()
												.target("/pandora/objects/pandorapage")
												.text("Pages")
												.build(),
										PandoraSidebarItem.builder()
												.target("/pandora/stores")
												.text("Stores")
												.build(),
										PandoraSidebarItem.builder()
												.text("Datatypes")
												.build(),
										PandoraSidebarItem.builder()
												.text("Datatype Checker")
												.target("pandora/datatype-checker")
												.build(),
										PandoraSidebarItem.builder()
												.text("Object Models")
												.children(List.of(
														PandoraSidebarItem.builder()
																.text("Java Base Types")
																.build(),
														PandoraSidebarItem.builder()
																.text("Object Models")
																.build()))
												.build(),
										PandoraSidebarItem.builder()
												// FIXME: This should be added in pandora-functions, since that module is optional.
												.text("Functions")
												.children(List.of(
														PandoraSidebarItem.builder()
																.target("/pandora/functions")
																.text("Functions")
																.build(),
														PandoraSidebarItem.builder()
																.target("/pandora/function-runs")
																.text("Function Runs")
																.build(),
														PandoraSidebarItem.builder()
																.target("/pandora/routines")
																.text("Routines")
																.build()))
												.build()))
								.slug("pandora-default")
								.text("Pandora")
								.build(),
						SidebarCategoryConfiguration.builder()
								.slug("pandora-empty")
								.build())
				.map(this::convertAndAddHardcoded);
	}
}
