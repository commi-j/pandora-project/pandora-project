package tk.labyrinth.pandora.ui.domain.objectview;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectForm;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;

import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class GenericFormObjectViewRenderer {

	private final BeanContext beanContext;

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		GenericObjectForm genericObjectForm = beanContext.getBeanFactory()
				.withBean(parameters.datatype())
				.withBean(parameters.objectModel())
				.getBean(GenericObjectForm.class);
		//
		genericObjectForm.render(ObjectForm.Properties.<GenericObject>builder()
				.currentValue(parameters.currentValue())
				.initialValue(parameters.initialValue())
				.nonEditableReasons(parameters.nonEditableReasons())
				.onValueChange(parameters.onValueChange())
				.build());
		//
		return genericObjectForm.asVaadinComponent();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		GenericObject currentValue;

		@NonNull
		Datatype datatype;

		@NonNull
		GenericObject initialValue;

		/**
		 * Contracts (v1.0.1):<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute, each on a separate line;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@NonNull
		ObjectModel objectModel;

		@NonNull
		Consumer<GenericObject> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
