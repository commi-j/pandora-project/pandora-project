package tk.labyrinth.pandora.ui.component.value.box.mutable;

import com.vaadin.flow.component.Component;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.core.change.ChangeStatus;
import tk.labyrinth.pandora.ui.component.value.box.simple.SimpleValueBox;

import java.lang.reflect.Type;

@RequiredArgsConstructor
public class SimpleMutableValueBox<T> implements MutableValueBox<T> {

	private final SimpleValueBox<T> valueBox;

	@Override
	public Component asVaadinComponent() {
		return valueBox.asVaadinComponent();
	}

	@Override
	public Type getParameterType() {
		return valueBox.getParameterType();
	}

	@Override
	public void render(Properties<T> properties) {
		valueBox.render(SimpleValueBox.Properties.<T>builder()
				.label(properties.getLabel())
				.nonEditableReasons(properties.getNonEditableReasons())
				.onValueChange(properties.getOnValueChange())
				.value(properties.getCurrentValue())
				.build());
		//
		ChangeStatus changeStatus = ChangeStatus.compute(properties.getInitialValue(), properties.getCurrentValue());
		Component component = valueBox.asVaadinComponent();
	}
}
