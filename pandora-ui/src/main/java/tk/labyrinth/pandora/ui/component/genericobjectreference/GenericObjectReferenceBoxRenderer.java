package tk.labyrinth.pandora.ui.component.genericobjectreference;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ModelCodeAndAttributeNamesReferenceModelReference;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ReferenceModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.functionalcomponents.box.SuggestBoxRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.extra.referencemodel.ReferenceModelRegistry;
import tk.labyrinth.pandora.stores.selector.Selector;
import tk.labyrinth.pandora.stores.selector.SelectorResolver;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectReferenceItem;
import tk.labyrinth.pandora.ui.renderer.custom.GenericObjectReferenceToItemRenderer;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

@Bean
@RequiredArgsConstructor
public class GenericObjectReferenceBoxRenderer {

	private final GenericObjectSearcher genericObjectSearcher;

	private final GenericObjectReferenceToItemRenderer objectReferenceToItemRenderer;

	private final ReferenceModelRegistry referenceModelRegistry;

	private final SelectorResolver selectorResolver;

	private List<GenericObjectReferenceItem> getSuggests(ReferenceModel referenceModel, String filterText) {
		ModelCodeAndAttributeNamesReferenceModelReference objectReferenceModelReference =
				ModelCodeAndAttributeNamesReferenceModelReference.from(referenceModel);
		//
		Datatype targetDatatype = ObjectModelUtils.createDatatype(objectReferenceModelReference.getTargetModelReference());
		//
		List<GenericObject> objects = genericObjectSearcher.search(ParameterizedQuery.<CodeObjectModelReference>builder()
				.limit(10L)
				.parameter(referenceModel.computeObjectModelReference())
				.predicate(Predicates.flatAnd(List
						.of(
								referenceModelRegistry.createAttributePredicateForReferenceModel(
										objectReferenceModelReference),
								selectorResolver.selectorToPredicate(
										targetDatatype,
										Selector.builder()
												.searchText(filterText)
												.build()))
						.filter(Objects::nonNull)
						.toJavaStream()))
				.build());
		//
		return objects
				.map(object -> objectReferenceToItemRenderer.renderNonNull(objectReferenceModelReference, object))
				.prependAll(filterText.isEmpty() ? List.of((GenericObjectReferenceItem) null) : List.empty());
	}

	public View render(
			Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public View render(Parameters parameters) {
		return ComponentView.of(SuggestBoxRenderer.<GenericObjectReferenceItem>render(builder -> builder
				.configurer(comboBox -> comboBox.setItemLabelGenerator(GenericObjectReferenceItem::toDisplayString))
				.label(parameters.label())
				.nonEditableReasons(parameters.nonEditableReasons())
				.onValueChange(parameters.onValueChange() != null
						? nextValue -> parameters.onValueChange().accept(
						nextValue != null ? nextValue.getReference() : null)
						: null)
				.itemsFunction(filterText -> getSuggests(parameters.referenceModel(), filterText))
				.value(objectReferenceToItemRenderer.render(parameters.currentValue()))
				.build()));
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		GenericObjectReference currentValue;

		@Nullable
		GenericObjectReference initialValue;

		@Nullable
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@lombok.Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<GenericObjectReference> onValueChange;

		@NonNull
		ReferenceModel referenceModel;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}

		public static class Builder {
			// Lomboked
		}
	}
}
