package tk.labyrinth.pandora.ui.component.typedobject;

import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.pandora.context.BeanContext;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.component.genericobject.GenericObjectEditorDialogHandler;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.ui.component.view.confirmation.ConfirmationViews;

import java.lang.reflect.Type;

/**
 * @param <T> Type
 *
 * @see GenericObjectEditorDialogHandler
 */
@LazyComponent
@RequiredArgsConstructor
public class TypedObjectEditorDialogHandler<T> {

	private final BeanContext beanContext;

	public ConfirmationHandle<T> showDialog(Parameters<T> parameters) {
		ObjectForm<T> objectForm = beanContext.getBeanFactory()
				.withBean(parameters.javaType())
				.getBean(TypeUtils.parameterize(ObjectForm.class, parameters.javaType()));
		//
		Observable<T> currentValueObservable = Observable.withInitialValue(parameters.currentValue());
		//
		currentValueObservable.subscribe(nextCurrentValue -> objectForm.render(
				ObjectForm.Properties.<T>builder()
						.currentValue(nextCurrentValue)
						.initialValue(parameters.initialValue())
						.onValueChange(currentValueObservable::set)
						.build()));
		//
		return ConfirmationViews.showViewDialog(objectForm).map(success -> currentValueObservable.get());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<T> {

		@NonNull
		T currentValue;

		@NonNull
		T initialValue;

		@NonNull
		Type javaType;
	}
}
