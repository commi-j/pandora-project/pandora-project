package tk.labyrinth.pandora.ui.component.genericobjectreference;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ModelCodeAndAttributeNamesReferenceModelReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.PrototypeScopedComponent;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectSearcher;
import tk.labyrinth.pandora.stores.extra.referencemodel.ReferenceModelRegistry;
import tk.labyrinth.pandora.stores.selector.Selector;
import tk.labyrinth.pandora.stores.selector.SelectorResolver;
import tk.labyrinth.pandora.ui.component.value.box.simple.SuggestBox;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.pandora.ui.datatypes.render.GenericObjectReferenceItem;
import tk.labyrinth.pandora.ui.renderer.custom.GenericObjectReferenceToItemRenderer;

import java.util.Objects;
import java.util.function.Consumer;

@Deprecated
@PrototypeScopedComponent
@RequiredArgsConstructor
public class GenericObjectReferenceBox implements RenderableView<GenericObjectReferenceBox.Properties> {

	private final GenericObjectSearcher genericObjectSearcher;

	private final GenericObjectReferenceToItemRenderer objectReferenceToItemRenderer;

	private final ReferenceModelRegistry referenceModelRegistry;

	private final SelectorResolver selectorResolver;

	private final SuggestBox<GenericObjectReferenceItem> suggestBox = new SuggestBox<>();

	private List<GenericObjectReferenceItem> getSuggests(
			ModelCodeAndAttributeNamesReferenceModelReference objectReferenceModelReference,
			Datatype targetDatatype,
			String text) {
		List<GenericObject> objects = genericObjectSearcher.search(ParameterizedQuery.<CodeObjectModelReference>builder()
				.limit(10L)
				.parameter(objectReferenceModelReference.getTargetModelReference())
				.predicate(Predicates.flatAnd(List
						.of(
								referenceModelRegistry.createAttributePredicateForReferenceModel(
										objectReferenceModelReference),
								selectorResolver.selectorToPredicate(
										targetDatatype,
										Selector.builder()
												.searchText(text)
												.build()))
						.filter(Objects::nonNull)
						.toJavaStream()))
				.build());
		return objects
				.map(object -> objectReferenceToItemRenderer.renderNonNull(objectReferenceModelReference, object))
				.prependAll(text.isEmpty() ? List.of((GenericObjectReferenceItem) null) : List.empty());
	}

	@Override
	public Component asVaadinComponent() {
		return suggestBox.asVaadinComponent();
	}

	@Override
	public void render(Properties properties) {
		Class<? extends Reference<?>> referenceClass = properties.getReferenceClass();
		//
		ModelCodeAndAttributeNamesReferenceModelReference objectReferenceModelReference =
				referenceModelRegistry.createReferenceModelReference(referenceClass);
		//
		Datatype targetDatatype = ObjectModelUtils.createDatatype(objectReferenceModelReference.getTargetModelReference());
		//
		suggestBox.render(SuggestBox.Properties.<GenericObjectReferenceItem>builder()
				.label(properties.getLabel())
				.nonEditableReasons(properties.getNonEditableReasons())
				.onValueChange(properties.getOnValueChange() != null
						? nextValue -> properties.getOnValueChange().accept(
						nextValue != null ? nextValue.getReference() : null)
						: null)
				.suggestFunction(text -> getSuggests(objectReferenceModelReference, targetDatatype, text))
				.toStringRenderFunction(GenericObjectReferenceItem::toDisplayString)
				.value(objectReferenceToItemRenderer.render(properties.getValue()))
				.build());
	}

	@Builder(toBuilder = true)
	@Value
	public static class Properties {

		@Nullable
		String label;

		/**
		 * Contracts:<br>
		 * - List is empty -> editable, otherwise not;<br>
		 * - All elements that are not empty Strings are displayed as HTML title attribute;<br>
		 */
		@Builder.Default
		@NonNull
		List<String> nonEditableReasons = List.empty();

		@Nullable
		Consumer<GenericObjectReference> onValueChange;

		@Deprecated
		Class<? extends Reference<?>> referenceClass;

		@Nullable
		GenericObjectReference value;

		public boolean calcEditable() {
			return nonEditableReasons.isEmpty();
		}

		public List<String> calcNonEmptyNonEditableReasons() {
			return nonEditableReasons.filter(nonEditableReason -> !nonEditableReasons.isEmpty());
		}
	}
}
