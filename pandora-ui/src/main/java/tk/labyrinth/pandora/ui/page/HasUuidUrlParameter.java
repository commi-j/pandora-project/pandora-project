package tk.labyrinth.pandora.ui.page;

import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;

import java.util.UUID;

public interface HasUuidUrlParameter extends HasUrlParameter<String> {

	default void acceptInvalidParameter(BeforeEvent event, String parameter) {
		throw new IllegalArgumentException("Require UUID: " + parameter);
	}

	void acceptValidParameter(BeforeEvent event, UUID parameter);

	@Override
	default void setParameter(BeforeEvent event, String parameter) {
		UUID uuidParameter;
		try {
			uuidParameter = parse(parameter);
		} catch (IllegalArgumentException ex) {
			uuidParameter = null;
		}
		if (uuidParameter != null) {
			acceptValidParameter(event, uuidParameter);
		} else {
			acceptInvalidParameter(event, parameter);
		}
	}

	// TODO: Decide if we want this method here.
	static UUID parse(String uuidString) {
		UUID result;
		if (uuidString.length() == 36) {
			result = UUID.fromString(uuidString);
		} else if (uuidString.length() == 32) {
			result = UUID.fromString(uuidString.substring(0, 12) +
					"-" + uuidString.substring(12, 16) +
					"-" + uuidString.substring(16, 20) +
					"-" + uuidString.substring(20, 24) +
					"-" + uuidString.substring(24));
		} else {
			throw new IllegalArgumentException("Wrong length: " + uuidString);
		}
		return result;
	}
}
