package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.function.Consumer;

@Slf4j
@SuppressWarnings("rawtypes")
public class RawClassBox implements SimpleValueBox<Class> {

	private final TextField textField = new TextField();

	private boolean eventHandlingInProgress = false;

	private Properties<Class> properties;

	private boolean renderInProgress = false;

	{
		textField.addValueChangeListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						textField.setValue(event.getOldValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					Consumer<Class> onValueChange = properties.getOnValueChange();
					if (onValueChange != null) {
						Try.of(() -> Class.forName(event.getValue())).onSuccess(onValueChange);
					}
				}
			}
		});
	}

	private void doRender(Properties<Class> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		textField.setLabel(properties.getLabel());
		textField.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				textField,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		textField.setValue(properties.getValue() != null ? properties.getValue().toString() : "");
	}

	@Override
	public Component asVaadinComponent() {
		return textField;
	}

	@Override
	public void render(Properties<Class> properties) {
		if (renderInProgress) {
			throw new IllegalStateException("Invoked render while already rendering");
		}
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}
}
