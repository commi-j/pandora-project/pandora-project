package tk.labyrinth.pandora.ui.component.table.search;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSearchParameters;
import tk.labyrinth.pandora.ui.component.table.AdvancedTableSearchResult;

import java.time.Instant;
import java.util.function.Function;

@Accessors(fluent = true)
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class SearchParameters {

	@NonNull
	Integer pageIndex;

	@NonNull
	Integer pageSize;

	@Nullable
	Predicate predicate;

	@Nullable
	Instant refreshedAt;

	@NonNull
	Function<AdvancedTableSearchParameters, AdvancedTableSearchResult> searchFunction;
}
