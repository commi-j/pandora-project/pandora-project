package tk.labyrinth.pandora.ui.domain.state2;

import org.checkerframework.checker.nullness.qual.Nullable;
import reactor.core.publisher.Flux;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.vaadin.meta.UiScopedComponent;
import tk.labyrinth.pandora.reactive.observable.Observable;

@UiScopedComponent
public class UiStateManager {

	private final Observable<GenericObject> stateObservable = Observable.withInitialValue(GenericObject.empty());

	public GenericObject getState() {
		return stateObservable.get();
	}

	public Flux<GenericObject> getStateFlux() {
		return stateObservable.getFlux();
	}

	// TODO: Path should support chaining (foo.bar), but we need to implement registrations first.
	public void setAttribute(String path, @Nullable ValueWrapper value) {
		stateObservable.update(state -> state.withAttribute(path, value));
	}
}
