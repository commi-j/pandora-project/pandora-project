package tk.labyrinth.pandora.ui.paginator;

import lombok.Builder;
import lombok.Value;

import java.util.function.Consumer;

@Builder(toBuilder = true)
@Value
public class PaginatorProperties {

	Integer pageCount;

	Consumer<Integer> selectPageCallback;

	Integer selectedPageIndex;
}
