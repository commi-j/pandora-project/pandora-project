package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.UUID;
import java.util.function.Consumer;

@Slf4j
public class UuidBox implements SimpleValueBox<UUID> {

	private final TextField textField = new TextField();

	private boolean eventHandlingInProgress = false;

	private Properties<UUID> properties;

	private boolean renderInProgress = false;

	{
		textField.addValueChangeListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						textField.setValue(event.getOldValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					Consumer<UUID> onValueChange = properties.getOnValueChange();
					if (onValueChange != null) {
						UUID uuidValue;
						//
						String stringValue = StringUtils.emptyToNull(event.getValue());
						if (stringValue != null) {
							try {
								uuidValue = UUID.fromString(event.getValue());
							} catch (RuntimeException ex) {
								throw ex;
							}
						} else {
							uuidValue = null;
						}
						onValueChange.accept(uuidValue);
					}
				}
			}
		});
	}

	private void doRender(Properties<UUID> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		textField.setLabel(properties.getLabel());
		textField.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				textField,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		textField.setValue(properties.getValue() != null ? properties.getValue().toString() : "");
	}

	@Override
	public Component asVaadinComponent() {
		return textField;
	}

	@Override
	public void render(Properties<UUID> properties) {
		if (renderInProgress) {
			throw new IllegalStateException("Invoked render while already rendering");
		}
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}
}
