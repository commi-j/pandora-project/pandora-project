package tk.labyrinth.pandora.ui.routing.route;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.ui.routing.routetarget.RouteTarget;

@Deprecated
@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class RouteModel {

	GenericObject parameters;

	RouteTarget target;
}
