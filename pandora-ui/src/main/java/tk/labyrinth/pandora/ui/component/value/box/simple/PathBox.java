package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.nio.file.Path;
import java.time.format.DateTimeParseException;
import java.util.function.Consumer;

@Slf4j
public class PathBox implements SimpleValueBox<Path> {

	private final TextField textField = new TextField();

	private boolean eventHandlingInProgress = false;

	private Properties<Path> properties;

	private boolean renderInProgress = false;

	{
		textField.addValueChangeListener(event -> {
			if (!renderInProgress) {
				if (!eventHandlingInProgress) {
					eventHandlingInProgress = true;
					try {
						textField.setValue(event.getOldValue());
					} finally {
						eventHandlingInProgress = false;
					}
					//
					Consumer<Path> onValueChange = properties.getOnValueChange();
					if (onValueChange != null) {
						try {
							onValueChange.accept(!event.getValue().isEmpty() ? Path.of(event.getValue()) : null);
						} catch (DateTimeParseException ex) {
							// no-op
						}
					}
				}
			}
		});
	}

	private void doRender(Properties<Path> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		this.properties = properties;
		//
		textField.setLabel(properties.getLabel());
		textField.setReadOnly(!properties.calcEditable());
		ElementUtils.setTitle(
				textField,
				StringUtils.emptyToNull(properties.calcNonEmptyNonEditableReasons().mkString("\n")));
		textField.setValue(properties.getValue() != null ? properties.getValue().toString() : "");
	}

	@Override
	public Component asVaadinComponent() {
		return textField;
	}

	@Override
	public void render(Properties<Path> properties) {
		if (renderInProgress) {
			throw new IllegalStateException("Invoked render while already rendering");
		}
		renderInProgress = true;
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		} finally {
			renderInProgress = false;
		}
	}
}
