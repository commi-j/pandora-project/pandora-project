package tk.labyrinth.pandora.ui.component.value.box.simple;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextField;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

@Slf4j
public class ObjectBox implements SimpleValueBox<Object> {

	private final TextField textField = new TextField();

	{
		textField.setReadOnly(true);
	}

	private void doRender(Properties<Object> properties) {
		logger.debug("#doRender: properties = {}", properties);
		//
		textField.setLabel(properties.getLabel());
		ElementUtils.setTitle(
				textField,
				properties.calcNonEmptyNonEditableReasons()
						.append("This box does not support edition")
						.mkString());
		textField.setValue(properties.getValue() != null ? properties.getValue().toString() : "");
	}

	@Override
	public Component asVaadinComponent() {
		return textField;
	}

	@Override
	public void render(Properties<Object> properties) {
		try {
			doRender(properties);
		} catch (RuntimeException ex) {
			logger.error("", ex);
		}
	}
}
