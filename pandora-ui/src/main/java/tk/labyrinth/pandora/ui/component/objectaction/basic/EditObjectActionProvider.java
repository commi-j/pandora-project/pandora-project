package tk.labyrinth.pandora.ui.component.objectaction.basic;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.Order;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.index.PandoraIndexConstants;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.stores.domain.rootobject.PandoraRootUtils;
import tk.labyrinth.pandora.stores.extra.genericobject.GenericObjectManipulator;
import tk.labyrinth.pandora.stores.init.hardcoded.PandoraHardcodedConstants;
import tk.labyrinth.pandora.ui.component.objectaction.GenericObjectActionProvider;
import tk.labyrinth.pandora.ui.view.ObjectViewRegistry;

@Bean
@Order(EditObjectActionProvider.PRIORITY)
@RequiredArgsConstructor
public class EditObjectActionProvider implements GenericObjectActionProvider {

	public static final int PRIORITY = 10;

	private final GenericObjectManipulator genericObjectManipulator;

	private final ObjectViewRegistry objectViewRegistry;

	@Nullable
	private String computeNonEditable(GenericObject object) {
		String result;
		{
			if (object.hasAttribute(PandoraHardcodedConstants.ATTRIBUTE_NAME)) {
				result = "Hardcoded objects are non-editable";
			} else if (object.hasAttribute(PandoraIndexConstants.ATTRIBUTE_NAME)) {
				result = "Index objects are non-editable";
			} else if (!genericObjectManipulator.canManipulate(object)) {
				result = "No StoreRoute specified to edit this object";
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	@Override
	public Integer getStandaloneDistance() {
		return 0;
	}

	@Nullable
	@Override
	public Component provideActionForGenericObject(Context<GenericObject> context) {
		Component result;
		{
			ObjectModel objectModel = context.getObjectModel();
			GenericObject object = context.getObject();
			Runnable parentRefreshCallback = context.getParentRefreshCallback();
			//
			if (object != null) {
				result = ObjectActionUtils.createButton(
						"edit-object-button",
						null,
						VaadinIcon.EDIT,
						() -> objectViewRegistry.showEditViewDialog(
								context.getDatatype(),
								objectModel,
								object,
								parentRefreshCallback),
						"Edit Object",
						computeNonEditable(object));
			} else {
				// TODO: Support extra attributes for new object from context.
				GenericObject newObject = GenericObject.of(
						PandoraRootUtils.createModelReferenceAttribute(objectModel));
				//
				result = ObjectActionUtils.createButton(
						"create-object-button",
						null,
						VaadinIcon.PLUS,
						() -> objectViewRegistry.showCreateViewDialog(
								context.getDatatype(),
								objectModel,
								newObject,
								parentRefreshCallback),
						"Create Object",
						genericObjectManipulator.canManipulate(newObject)
								? null
								: "No StoreRoute to create new object specified");
			}
		}
		return result;
	}
}
