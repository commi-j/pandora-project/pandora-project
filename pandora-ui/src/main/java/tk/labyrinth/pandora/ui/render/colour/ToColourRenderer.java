package tk.labyrinth.pandora.ui.render.colour;

import tk.labyrinth.pandora.ui.render.Renderer;

public interface ToColourRenderer<T> extends Renderer<T, String> {
	// empty
}
