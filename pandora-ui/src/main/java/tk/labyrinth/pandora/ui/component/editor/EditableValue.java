package tk.labyrinth.pandora.ui.component.editor;

import lombok.NonNull;
import lombok.Value;
import lombok.With;

import javax.annotation.CheckForNull;

@Value(staticConstructor = "of")
@With
public class EditableValue<T> {

	@CheckForNull
	String errorMessage;

	@CheckForNull
	T value;

	@NonNull
	Boolean visited;

	public EditableValue<T> visited() {
		return of(errorMessage, value, true);
	}

	public static <T> EditableValue<T> empty() {
		return of(null, null, false);
	}

	public static <T> EditableValue<T> ofValue(T value) {
		return of(null, value, false);
	}
}
