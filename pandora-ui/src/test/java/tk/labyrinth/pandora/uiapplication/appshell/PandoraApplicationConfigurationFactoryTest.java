package tk.labyrinth.pandora.uiapplication.appshell;

import com.vaadin.flow.server.Constants;
import com.vaadin.flow.spring.SpringServlet;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;

import java.util.List;

class PandoraApplicationConfigurationFactoryTest {

	@SuppressWarnings("unchecked")
	@Test
	void testIfThisClassIsNeeded() {
		Assertions
				.assertThat((List<String>) ClassUtils.getDeclaredFieldValueInferred(
						SpringServlet.class,
						"PROPERTY_NAMES",
						null))
				.doesNotContain(Constants.ALLOW_APPSHELL_ANNOTATIONS);
	}
}