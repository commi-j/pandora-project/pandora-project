package tk.labyrinth.pandora.functionalcomponents.core;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

class FunctionalPageTest {

	@Test
	void testInitialization() {
		FunctionalComponent<String> content = new SimpleFunctionalPage().getContent();
		Label label = (Label) content.getChildren().toList().get(0);
		Assertions.assertEquals("foo", label.getText());
	}

	private static class SimpleFunctionalPage extends FunctionalPage<String> {

		@Override
		protected String getInitialProperties() {
			return "foo";
		}

		@Override
		protected Component render(String properties, Consumer<String> sink) {
			return new Label(properties);
		}
	}
}