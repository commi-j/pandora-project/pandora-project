package tk.labyrinth.pandora.functionalcomponents.advanced;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

class TreeGridWrapperTest {

	/**
	 * Check that we don't fail in the simplest case where just one item is updated.
	 */
	@Test
	void testSetItemsWithSingleElementUpdated() {
		TreeGridWrapper<String> grid = new TreeGridWrapper<>(
				item -> List.empty(),
				item -> item,
				item -> item);
		//
		grid.setItems(List.of("foo"));
		grid.setItems(List.of("bar"));
	}
}
