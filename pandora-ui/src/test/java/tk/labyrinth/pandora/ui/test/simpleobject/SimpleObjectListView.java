package tk.labyrinth.pandora.ui.test.simpleobject;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.NumberField;
import tk.labyrinth.pandora.ui.component.value.listview.ListItem;
import tk.labyrinth.pandora.ui.component.value.listview.ValueListViewBase;

public class SimpleObjectListView extends ValueListViewBase<SimpleObject> {

	@Override
	protected void configureGrid(Grid<ListItem<SimpleObject>> grid) {
		grid.addColumn(component(pair -> new NumberField()));
		grid.addColumn(value(pair -> pair.getRight().getUuid()));
	}

	@Override
	protected SimpleObject createNewElementInstance() {
		return SimpleObject.builder().build();
	}
}
