package tk.labyrinth.pandora.ui.domain.route;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PandoraRouteVaadinToolTest {

	@Test
	void testComposePath() {
		Assertions
				.assertThat(PandoraRouteVaadinTool.composePath("page/:___url_parameter?"))
				.isEqualTo("page");
		Assertions
				.assertThat(PandoraRouteVaadinTool.composePath("/:___url_parameter?"))
				.isEqualTo(""); // TODO: Preserve slash on empty path;
	}
}
