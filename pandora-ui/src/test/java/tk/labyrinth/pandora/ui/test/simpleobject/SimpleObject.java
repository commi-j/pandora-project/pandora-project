package tk.labyrinth.pandora.ui.test.simpleobject;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Builder(toBuilder = true)
@Value
public class SimpleObject {

	Integer integer;

	String string;

	UUID uuid;
}
