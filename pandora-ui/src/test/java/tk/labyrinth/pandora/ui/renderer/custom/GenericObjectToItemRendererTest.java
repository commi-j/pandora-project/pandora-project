package tk.labyrinth.pandora.ui.renderer.custom;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.ui.PandoraUiModule;

@ContextConfiguration(classes = PandoraUiModule.class)
@SpringBootTest
class GenericObjectToItemRendererTest {

	@Autowired
	private GenericObjectToItemRenderer genericObjectToItemRenderer;

	@Test
	void testRenderPatternRule() {
		Assertions
				.assertThat(genericObjectToItemRenderer.renderPatternRule(
						ObjectModel.builder()
								.build(),
						GenericObject.of(
								GenericObjectAttribute.ofSimple("foo", "FOO"),
								GenericObjectAttribute.ofObject("one",
										GenericObject.ofSingleSimple("two", "2"))),
						"$foo & ${one.two}"))
				.isEqualTo("FOO & 2");
	}
}
