package tk.labyrinth.pandora.ui.renderer.tovaadincomponent;

import io.vavr.collection.List;
import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeature;
import tk.labyrinth.pandora.ui.PandoraUiModule;

@ContextConfiguration(classes = PandoraUiModule.class)
@SpringBootTest
class ToVaadinComponentRendererRegistryTest {

	@Autowired
	private ToVaadinComponentRendererRegistry toVaadinComponentRendererRegistry;

	@Test
	void testGetRendererForObjectModelFeature() {
		val selectionResult = toVaadinComponentRendererRegistry
				.selectRenderer(ToVaadinComponentRendererRegistry.Context.builder()
						.datatype(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(ObjectModelFeature.class))
						.hints(List.empty())
						.build());
		//
		Assertions
				.assertThat(selectionResult.getHandler())
				.isInstanceOf(GenericObjectToVaadinComponentRenderer.class);
	}
}
