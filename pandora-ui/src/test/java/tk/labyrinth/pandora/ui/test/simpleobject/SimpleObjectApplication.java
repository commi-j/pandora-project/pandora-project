package tk.labyrinth.pandora.ui.test.simpleobject;

import com.vaadin.flow.spring.SpringBootAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@ImportAutoConfiguration(exclude = SpringBootAutoConfiguration.class)
@SpringBootApplication
public class SimpleObjectApplication {
	// empty
}
