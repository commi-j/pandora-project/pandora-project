package tk.labyrinth.pandora.ui.component.javabasetype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.ui.component.value.box.registry.ValueBoxRegistry;

@SpringBootTest
class JavaBaseTypeBasedValueBoxContributorTest {

	@Autowired
	private JavaBaseTypeBasedValueBoxContributor javaBaseTypeBasedValueBoxContributor;

	@Autowired
	private ValueBoxRegistry valueBoxRegistry;
//	@Test
//	void testContributeValueBox() {
////		MutableValueBox<?> objectModelBox = valueBoxRegistry.getValueBox(ObjectModel.class);
////		MutableValueBox<?> objectModelAttributeBox0 = valueBoxRegistry.getValueBox(ObjectModelAttribute.class);
//		javaBaseTypeBasedValueBoxContributor.contributeValueBox(
//				valueBoxRegistry,
//				Datatype.builder()
//						.baseReference(OriginReferenceDatatypeBaseReference.of(
//								SignatureJavaBaseTypeReference.from(ObjectModelAttribute.class).toGenericReference()))
//						.build());
////		MutableValueBox<?> objectModelAttributeBox2 = valueBoxRegistry.getValueBox(
////				Datatype.builder()
////						.baseReference(OriginReferenceDatatypeBaseReference.of(
////								SignatureJavaBaseTypeReference.from(ObjectModelAttribute.class).toGenericReference()))
////						.build());
//	}
}