package tk.labyrinth.pandora.ui.component.genericobject;

import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.ui.component.object.ObjectForm;

import java.util.List;

class GenericGenericObjectFormBeanContributorTest {

	@Test
	void testSupportsType() {
		GenericGenericObjectFormBeanContributor genericGenericObjectFormBeanContributor =
				new GenericGenericObjectFormBeanContributor(List.of());
		//
		Assertions.assertTrue(genericGenericObjectFormBeanContributor.supportsType(
				null,
				TypeUtils.parameterize(ObjectForm.class, GenericObject.class)));
		Assertions.assertFalse(genericGenericObjectFormBeanContributor.supportsType(
				null,
				TypeUtils.parameterize(ObjectForm.class, ObjectModel.class)));
	}
}
