package tk.labyrinth.pandora.ui.vaadintests;

import com.vaadin.flow.component.treegrid.TreeGrid;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TreeGridTest {

	@Test
	void testSettingNewItems() {
		TreeGrid<String> grid = new TreeGrid<>();
		//
		grid.setItems(
				List.of("aa", "bb"),
				item -> item.length() > 1 ? List.of(item.substring(1)) : List.of());
		//
		grid.setItems(
				List.of("aa", "bb"),
				item -> item.length() > 1 ? List.of(item.substring(1)) : List.of());
	}
}
