package tk.labyrinth.pandora.ui.tool;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import tk.labyrinth.pandora.ui.PandoraUiModule;

@ContextConfiguration(classes = PandoraUiModule.class)
@SpringBootTest("pandora.persistent-cache-key-password = test")
class ApplicationSecretCacheTest {

	@Autowired
	private ApplicationSecretCache applicationSecretCache;

	@Test
	void testEncodeAndDecode() {
		Assertions
				.assertThat(applicationSecretCache.put("hello"))
				.isEqualTo("6neEJ7AHoJIveteWpPUw5w==");
		Assertions
				.assertThat(applicationSecretCache.find("6neEJ7AHoJIveteWpPUw5w=="))
				.isEqualTo("hello");
		//
		Assertions
				.assertThat(applicationSecretCache.put("hello_world"))
				.isEqualTo("O03bAosFDpbIFhXQaNXNNQ==");
	}
}
