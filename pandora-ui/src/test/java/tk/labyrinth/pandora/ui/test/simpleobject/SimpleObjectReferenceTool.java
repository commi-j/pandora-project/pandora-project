package tk.labyrinth.pandora.ui.test.simpleobject;

import org.springframework.stereotype.Service;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.ui.component.reference.ReferenceItem;
import tk.labyrinth.pandora.ui.component.reference.ReferenceTool;

import javax.annotation.Nullable;

@Service
public class SimpleObjectReferenceTool implements ReferenceTool<SimpleObject, SimpleObjectReference> {

	@Nullable
	@Override
	public Predicate buildSearchPredicate(String text) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Nullable
	@Override
	public ReferenceItem<SimpleObjectReference> renderNonNullObject(SimpleObject value) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public ReferenceItem<SimpleObjectReference> renderNonNullReference(SimpleObjectReference value) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
