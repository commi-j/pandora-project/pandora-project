package tk.labyrinth.pandora.ui.render.colour;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class UuidToColourRendererTest {

	@Test
	void testRender() {
		UuidToColourRenderer uuidToColourRenderer = new UuidToColourRenderer();
		//
		UUID uuid = UUID.randomUUID();
		System.out.println(uuid);
		Assertions.assertEquals("#003c51",
				uuidToColourRenderer.renderNonNull(UUID.fromString("a88529f0-cfe5-43ae-badd-9033caa9ba1f")));
		Assertions.assertEquals("#1eaa5a",
				uuidToColourRenderer.renderNonNull(UUID.fromString("74ba97e7-7cb4-4b12-b2a7-585a83256aab")));
		Assertions.assertEquals("#7b2de4",
				uuidToColourRenderer.renderNonNull(UUID.fromString("b623d410-4363-4ab5-8b1f-a77ebe41d209")));
		Assertions.assertEquals("#220f93",
				uuidToColourRenderer.renderNonNull(UUID.fromString("ff3cb475-b4d6-4ed2-b167-89d5d70bd802")));
	}
}