package tk.labyrinth.pandora.ui.test.simpleobject;

import io.vavr.collection.List;
import org.springframework.stereotype.Service;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.reference.ResolvedReference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.stores.objectsearcher.ObjectSearcherBase;

@Service
public class SimpleObjectSearcher extends ObjectSearcherBase<PlainQuery, SimpleObject> {

	@Override
	public long count(PlainQuery query) {
		// TODO: Implement.
		throw new NotImplementedException();
	}

	@Override
	public <R extends Reference<SimpleObject>> ResolvedReference<SimpleObject, R> resolve(R reference) {
		return null;
	}

	@Override
	public List<SimpleObject> search(PlainQuery query) {
		// TODO: Implement.
		throw new NotImplementedException();
	}
}
