package tk.labyrinth.pandora.ui.renderer.tostring;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.ui.PandoraUiModule;

@ContextConfiguration(classes = PandoraUiModule.class)
@SpringBootTest
class ToStringRendererRegistryTest {

	@Autowired
	private ToStringRendererRegistry toStringRendererRegistry;

	@Test
	void testRenderWithTag() {
		Assertions
				.assertThat(toStringRendererRegistry.render(
						ToStringRendererRegistry.Context.of(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(Tag.class)),
						Tag.of("foo", "BAR")))
				.isEqualTo("foo:BAR");
		Assertions
				.assertThat(toStringRendererRegistry.render(
						ToStringRendererRegistry.Context.of(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(Tag.class)),
						Tag.of("foo")))
				.isEqualTo("foo");
	}

	@Test
	void testRenderWithValueWrapper() {
		Assertions
				.assertThat(toStringRendererRegistry.render(
						ToStringRendererRegistry.Context.of(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(ValueWrapper.class)),
						SimpleValueWrapper.of("foo")))
				.isEqualTo("foo");
	}
}
