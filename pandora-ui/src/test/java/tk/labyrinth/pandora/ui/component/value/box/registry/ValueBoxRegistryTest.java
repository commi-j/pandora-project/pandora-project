package tk.labyrinth.pandora.ui.component.value.box.registry;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.ui.PandoraUiModule;

@ContextConfiguration(classes = PandoraUiModule.class)
@SpringBootTest
class ValueBoxRegistryTest {

	@Autowired
	private ValueBoxRegistry valueBoxRegistry;

	// TODO: Rework this test into checking contributor/valuebox classes.
	@Test
	void testGetValueBoxContributorForCoreDatatypes() {
		Assertions
				// TODO: Right contributor here would be java-base-type-based.
				.assertThat(valueBoxRegistry.getValueBoxSupplier(JavaBaseTypeUtils.createDatatypeFromJavaType(
						ValueWrapper.class)))
				.isNotNull();
	}
}
