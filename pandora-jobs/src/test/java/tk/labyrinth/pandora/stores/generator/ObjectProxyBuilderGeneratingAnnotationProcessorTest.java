package tk.labyrinth.pandora.stores.generator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicObjectModelFeatureContributor;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRun;

import java.util.List;

@Disabled // FIXME: Are we getting rid of proxies?
class ObjectProxyBuilderGeneratingAnnotationProcessorTest {

	@Test
	void testCreateBuilderSpec() {
		ReflectionObjectModelFactory reflectionObjectModelFactory = new ReflectionObjectModelFactory(
				List.of(),
				List.of(new PolymorphicObjectModelFeatureContributor()));
		//
		ObjectModel objectModel = reflectionObjectModelFactory.createModelFromJavaClass(JobRun.class);
		//
		Assertions.assertEquals(
				"""
						@javax.annotation.processing.Generated("tk.labyrinth.pandora.stores.generator.ObjectProxyBuilderGeneratingAnnotationProcessor")
						public class JobRunBuilder {
						  private io.vavr.collection.List<tk.labyrinth.pandora.jobs.model.jobrun.JobRunEvent> events;
						     
						  private io.vavr.collection.List<tk.labyrinth.pandora.datatypes.fault.Fault> faults;
						     
						  private java.lang.String jobRunnerClassName;
						     
						  private tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject parameters;
						     
						  public io.vavr.collection.List<tk.labyrinth.pandora.jobs.model.jobrun.JobRunEvent> events() {
						    return events;
						  }
						     
						  public tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder events(
						      io.vavr.collection.List<tk.labyrinth.pandora.jobs.model.jobrun.JobRunEvent> events) {
						    this.events = events;
						    return this;
						  }
						     
						  public io.vavr.collection.List<tk.labyrinth.pandora.datatypes.fault.Fault> faults() {
						    return faults;
						  }
						     
						  public tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder faults(
						      io.vavr.collection.List<tk.labyrinth.pandora.datatypes.fault.Fault> faults) {
						    this.faults = faults;
						    return this;
						  }
						     
						  public java.lang.String jobRunnerClassName() {
						    return jobRunnerClassName;
						  }
						     
						  public tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder jobRunnerClassName(
						      java.lang.String jobRunnerClassName) {
						    this.jobRunnerClassName = jobRunnerClassName;
						    return this;
						  }
						     
						  public tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject parameters() {
						    return parameters;
						  }
						     
						  public tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder parameters(
						      tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject parameters) {
						    this.parameters = parameters;
						    return this;
						  }
						     
						  public tk.labyrinth.pandora.jobs.model.jobrun.JobRun build(
						      tk.labyrinth.pandora.stores.extra.objectproxy.ObjectProxyHandler objectProxyHandler) {
						    return objectProxyHandler.wrap(
						    		toObject(objectProxyHandler.getConverterRegistry()),
						    		tk.labyrinth.pandora.jobs.model.jobrun.JobRun.class);
						  }
						     
						  public tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject toObject(
						      tk.labyrinth.pandora.context.converter.ConverterRegistry converterRegistry) {
						    return GenericObject.of(
						    		io.vavr.collection.LinkedHashMap.<String,Object>empty()
						    				.put("events", events)
						    				.put("faults", faults)
						    				.put("jobRunnerClassName", jobRunnerClassName)
						    				.put("parameters", parameters)
						    				.put("modelReference", tk.labyrinth.pandora.datatypes.domain.genericobjectmodel.CodeObjectModelReference.of("model"))
						    		.toList()
						    		.map(tuple -> tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute.of(
						    				tuple._1(),
						    				converterRegistry.convert(tuple._2(), tk.labyrinth.pandora.datatypes.value.wrapper.ValueWrapper.class))));
						  }
						     
						  public static tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder create() {
						    return new tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder();
						  }
						     
						  public static tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder from(
						      tk.labyrinth.pandora.jobs.model.jobrun.JobRun model) {
						    tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder builder = new tk.labyrinth.pandora.jobs.model.jobrun.JobRunBuilder();
						    builder.events(model.getEvents());
						    builder.faults(model.getFaults());
						    builder.jobRunnerClassName(model.getJobRunnerClassName());
						    builder.parameters(model.getParameters());
						    return builder;
						  }
						}
						""",
				ObjectProxyBuilderGeneratingAnnotationProcessor.createBuilderSpec(null, objectModel, "model").toString());
	}
}
