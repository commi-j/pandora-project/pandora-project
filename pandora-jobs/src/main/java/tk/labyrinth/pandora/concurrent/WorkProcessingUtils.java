package tk.labyrinth.pandora.concurrent;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import tk.labyrinth.pandora.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.pandora.misc4j.java.util.concurrent.locks.LockUtils;

import java.util.AbstractQueue;
import java.util.Comparator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.IntStream;

public class WorkProcessingUtils {

	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(WorkProcessingUtils.class);

	public static <T, R> Flux<R> processConcurrentGrowingWork(
			List<T> initialWork,
			BiFunction<T, Consumer<List<T>>, R> workProcessor,
			@Nullable Comparator<T> workComparator,
			int threadCount) {
		AtomicInteger addedWorkItems = new AtomicInteger(0);
		AtomicInteger completedWorkItems = new AtomicInteger(0);
		AbstractQueue<T> workQueue = workComparator != null
				? new PriorityBlockingQueue<>(initialWork.size(), workComparator)
				: new ArrayBlockingQueue<>(initialWork.size(), true);
		//
		workQueue.addAll(initialWork.asJava());
		addedWorkItems.addAndGet(initialWork.size());
		//
		ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
		Sinks.Many<R> sink = Sinks.many().unicast().onBackpressureError();
		Lock lock = new ReentrantLock();
		//
		IntStream.range(0, threadCount).forEach(index -> executorService.submit(() -> {
			while (completedWorkItems.get() < addedWorkItems.get()) {
				T workItem = workQueue.poll();
				//
				if (workItem != null) {
					try {
						R workResult = workProcessor.apply(
								workItem,
								newWorkItems -> {
									workQueue.addAll(newWorkItems.asJava());
									//
									addedWorkItems.addAndGet(newWorkItems.size());
								});
						//
						LockUtils.locked(lock, () -> sink.tryEmitNext(workResult));
					} catch (RuntimeException ex) {
						// TODO: Think about one-line warning. It's workProcessor's responsibility to handle exceptions.
						logger.error("", ex);
					} finally {
						completedWorkItems.incrementAndGet();
					}
					//
				} else {
					ThreadUtils.sleep(500);
				}
			}
			executorService.shutdown();
			sink.tryEmitComplete();
		}));
		//
		return sink.asFlux();
	}

	public static <T, R> Flux<R> processConcurrentWork(
			List<T> work,
			Function<T, R> workProcessor,
			int threadCount) {
		return processConcurrentGrowingWork(
				work,
				(workItem, newWorkItemsConsumer) -> workProcessor.apply(workItem),
				null,
				threadCount);
	}
}
