package tk.labyrinth.pandora.routines;

import org.springframework.scheduling.support.CronExpression;

public interface RoutineHandler {

	CronExpression getCronExpression();

	void handle();
}
