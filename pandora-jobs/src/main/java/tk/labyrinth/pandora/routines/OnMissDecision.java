package tk.labyrinth.pandora.routines;

// TODO: Reschedule require new timestamp, so it could be object and not enum.
// TODO: Raising messages is another dimension from action, so object suits better.
public enum OnMissDecision {
	RESCHEDULE,
	RUN_IMMEDIATELY,
	SKIP,
	SKIP_AND_RAISE_ERROR,
	SKIP_AND_RAISE_WARNING,
}
