package tk.labyrinth.pandora.routines;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.springframework.scheduling.support.CronExpression;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class Routine {

	CronExpression cronExpression;

	// TODO: Parameterize when we support parameters by Pandora Datatypes.
	Class routineHandlerClass;
}
