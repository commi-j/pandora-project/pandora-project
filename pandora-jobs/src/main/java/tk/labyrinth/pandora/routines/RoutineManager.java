package tk.labyrinth.pandora.routines;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.time.ZoneOffset;

@Deprecated
@LazyComponent
@RequiredArgsConstructor
public class RoutineManager implements ApplicationRunner {

	private final TaskScheduler taskScheduler;

	@SmartAutowired(required = false)
	private List<FunctionalRoutineHandler> routineHandlers;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		routineHandlers.forEach(routineHandler -> {
			//
			taskScheduler.schedule(
					routineHandler::handle,
					new CronTrigger(routineHandler.getCronExpression().toString(), ZoneOffset.UTC));
		});
	}
}
