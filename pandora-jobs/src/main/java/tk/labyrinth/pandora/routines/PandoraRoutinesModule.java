package tk.labyrinth.pandora.routines;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.jobs.PandoraJobsModule;

@Deprecated // Functions
@Import({
		PandoraJobsModule.class,
})
@Slf4j
@SpringBootApplication
public class PandoraRoutinesModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}
}
