package tk.labyrinth.pandora.jobs.service.impl;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.jobs.old.JobExecutionLog;
import tk.labyrinth.pandora.jobs.old.JobExecutionLogEntry;
import tk.labyrinth.pandora.jobs.old.JobExecutionRequest;
import tk.labyrinth.pandora.jobs.process.JobProcessor;
import tk.labyrinth.pandora.jobs.service.JobDispatcher;
import tk.labyrinth.pandora.misc4j.java.collectoin.CollectorUtils;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

@RequiredArgsConstructor
public class SimpleJobDispatcher implements JobDispatcher {

	private final Executor executor;

	private final Set<JobProcessor<?>> jobProcessors;

	private <P> void doDispatch(JobExecutionRequest<P> jobExecutionRequest) {
		JobProcessor<P> jobProcessor = pickJobProcessor(jobExecutionRequest.getProcessorType());
		P parameters = jobExecutionRequest.getParameters();
		JobExecutionLog<P> jobExecutionLog = JobExecutionLog.<P>builder()
				.logEntries(List.of(
						new JobExecutionLogEntry(
								Instant.now(),
								"Execution Scheduled",
								0)))
				.parameters(parameters)
				.processorType((Class<? extends JobProcessor<P>>) jobProcessor.getClass())
				.build();
		executor.execute(() -> {
			jobProcessor.process(parameters);
		});
	}

	private <P> JobProcessor<P> pickJobProcessor(Class<? extends JobProcessor<P>> processorType) {
		return jobProcessors.stream()
				.filter(processorType::isInstance)
				.map(processorType::cast)
				.collect(CollectorUtils.findOnly());
	}

	@Override
	public void dispatch(JobExecutionRequest<?> jobExecutionRequest) {
		doDispatch(jobExecutionRequest);
	}
}
