package tk.labyrinth.pandora.jobs.model.jobrun;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.fault.Fault;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.models.tool.builder.GenerateBuilder;
import tk.labyrinth.pandora.stores.rootobject.HasUid;

import java.util.UUID;

@Builder(builderClassName = "Builder", toBuilder = true)
@GenerateBuilder
@Model(JobRun.MODEL_CODE)
@RenderAttribute(JobRun.JOB_RUNNER_CLASS_NAME_ATTRIBUTE_NAME)
@Value
@With
public class JobRun implements HasUid {

	public static final String JOB_RUNNER_CLASS_NAME_ATTRIBUTE_NAME = "jobRunnerClassName";

	public static final String MODEL_CODE = "jobrun";

	List<JobRunEvent> events;

	List<Fault> faults;

	String jobRunnerClassName;

	GenericObject parameters;

	UUID uid;
}
