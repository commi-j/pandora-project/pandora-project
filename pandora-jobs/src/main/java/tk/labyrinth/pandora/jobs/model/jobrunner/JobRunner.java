package tk.labyrinth.pandora.jobs.model.jobrunner;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.function.Consumer;

public interface JobRunner<P> {

	// TODO: Is this method only for UI?
	P getInitialParameters();

	void run(Context<P> context);

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	class Context<P> {

		Consumer<String> logSink;

		P parameters;
	}
}
