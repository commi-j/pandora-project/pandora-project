package tk.labyrinth.pandora.jobs;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.activities.PandoraActivitiesModule;
import tk.labyrinth.pandora.stores.PandoraStoresModule;

@Deprecated
@Import({
		PandoraActivitiesModule.class,
		PandoraStoresModule.class,
})
@SpringBootApplication
public class PandoraJobsModule {
	// empty
}
