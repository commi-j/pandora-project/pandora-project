package tk.labyrinth.pandora.jobs.tool;

import io.vavr.collection.List;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttribute;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttributesContributor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
public class JobRunEnrichingExecutionContextAttributesContributor implements ExecutionContextAttributesContributor {

	private final ThreadLocal<List<ExecutionContextAttribute<?>>> attributesThreadLocal = new ThreadLocal<>();

	public void clearAttributes() {
		attributesThreadLocal.set(null);
	}

	@Override
	public List<ExecutionContextAttribute<?>> contributeAttributes() {
		List<ExecutionContextAttribute<?>> attributes = attributesThreadLocal.get();
		//
		return attributes != null ? attributes : List.empty();
	}

	public void setAttributes(List<ExecutionContextAttribute<?>> attributes) {
		attributesThreadLocal.set(attributes);
	}
}
