package tk.labyrinth.pandora.jobs.old;

import lombok.Value;
import tk.labyrinth.pandora.jobs.process.JobProcessor;

@Value
public class JobExecutionRequest<P> {

	P parameters;

	Class<? extends JobProcessor<P>> processorType;
}
