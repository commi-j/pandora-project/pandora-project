package tk.labyrinth.pandora.jobs.model.jobrun;

import lombok.Builder;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;

@Builder(toBuilder = true)
@Value
public class JobRunEvent {

	Instant happenedAt;

	@Nullable
	JobRunStatus nextStatus;
}
