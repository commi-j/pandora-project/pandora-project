package tk.labyrinth.pandora.jobs.model.jobrunner;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.time.Duration;

/**
 * This is a dummy job runner to test how they work.
 */
@LazyComponent
public class DummyJobRunner implements JobRunner<DummyJobRunner.Parameters> {

	public static final Parameters INITIAL_PARAMETERS = Parameters.builder()
			.string("initial_value")
			.build();

	@Override
	public Parameters getInitialParameters() {
		return INITIAL_PARAMETERS;
	}

	@Override
	public void run(Context<Parameters> context) {
		Duration duration = context.getParameters().getDuration();
		//
		if (duration != null) {
			ThreadUtils.sleep(duration.toMillis());
		}
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		Duration duration;

		@Nullable
		Integer integer;

		@Nullable
		String string;
	}
}
