package tk.labyrinth.pandora.jobs.model.jobrunindex;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRun;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRunStatus;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

import java.time.Instant;

@Builder(toBuilder = true)
// FIXME: Customize through classes enabled when UI is present.
// FIXME: Otherwise make field order not UI, but Object feature.
//@CustomizeTable(
//		attributesOrder = {
//				"jobRunClassName",
//				"parameters",
//				"status",
//				"scheduledAt",
//				"updatedAt",
//		},
//		hideNotDeclaredAttributes = true)
@Model(JobRunIndex.MODEL_CODE)
@ModelTag("jobs")
@RenderPattern("$jobRunnerClassName - $status")
@Value
public class JobRunIndex implements ObjectIndex<JobRun> {

	public static final String MODEL_CODE = "jobrunindex";

	String jobRunnerClassName;

	GenericObject parameters;

	Instant scheduledAt;

	JobRunStatus status;

	UidReference<JobRun> targetReference;

	Instant updatedAt;
}
