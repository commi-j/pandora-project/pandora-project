package tk.labyrinth.pandora.jobs.model.job;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;

import java.util.Objects;

@Value(staticConstructor = "of")
public class JobRunnerClassJobReference implements Reference<Job> {

	// TODO: Add generics: <? extends BeholderJobRunner<?>>
	Class jobRunnerClass;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				Job.MODEL_CODE,
				Job.JOB_RUNNER_CLASS_ATTRIBUTE_NAME,
				jobRunnerClass.getName());
	}

	public static JobRunnerClassJobReference from(Job object) {
		return JobRunnerClassJobReference.of(object.getJobRunnerClass());
	}

	@JsonCreator
	public static JobRunnerClassJobReference from(String value) {
		GenericReference reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), Job.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		return of(ClassUtils.getInferred(reference.getAttributeValue(Job.JOB_RUNNER_CLASS_ATTRIBUTE_NAME)));
	}
}
