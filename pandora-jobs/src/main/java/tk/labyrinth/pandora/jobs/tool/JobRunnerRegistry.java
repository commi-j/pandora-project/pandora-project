package tk.labyrinth.pandora.jobs.tool;

import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.task.TaskExecutor;
import tk.labyrinth.pandora.activities.activity.PandoraActivity;
import tk.labyrinth.pandora.activities.activity.PandoraActivityColourIndicator;
import tk.labyrinth.pandora.activities.activity.PandoraActivityRegistry;
import tk.labyrinth.pandora.context.converter.ConverterRegistry;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttribute;
import tk.labyrinth.pandora.context.spring.SmartAutowired;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRun;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRunEvent;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRunStatus;
import tk.labyrinth.pandora.jobs.model.jobrun.UidJobRunReference;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;
import tk.labyrinth.pandora.misc4j.exception.ExceptionUtils;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectmanipulator.TypedObjectManipulator;

import java.time.Instant;
import java.util.UUID;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class JobRunnerRegistry {

	private final ConverterRegistry converterRegistry;

	private final JobRunEnrichingExecutionContextAttributesContributor
			jobRunEnrichingExecutionContextAttributesContributor;

	private final ObjectProvider<JobRunner<?>> jobRunnerProvider;

	private final PandoraActivityRegistry pandoraActivityRegistry;

	private final TaskExecutor taskExecutor;

	@SmartAutowired
	private List<JobContextAttributeContributor> jobContextAttributeContributors;

	@SmartAutowired
	private TypedObjectManipulator<JobRun> jobRunObjectManipulator;

	private List<JobRunner<?>> jobRunners;

	private <P> JobRunner<P> getJobRunner(Class<? extends JobRunner<P>> jobRunnerClass) {
		return ObjectUtils.infer(jobRunners.find(jobRunner -> jobRunner.getClass() == jobRunnerClass).get());
	}

	@PostConstruct
	private void postConstruct() {
		jobRunners = List.ofAll(jobRunnerProvider);
	}

	public <P> P getInitialParameters(Class<? extends JobRunner<P>> jobRunnerClass) {
		return getJobRunner(jobRunnerClass).getInitialParameters();
	}

	public List<JobRunner<?>> getJobRunners() {
		return jobRunners;
	}

	public <P> UidJobRunReference runJob(
			Class<? extends JobRunner<P>> jobRunnerClass,
			P parameters) {
		logger.info("Scheduling Job: jobRunnerClass = {}, parameters = {}", jobRunnerClass.getName(), parameters);
		//
		JobRunner<P> jobRunner = getJobRunner(jobRunnerClass);
		//
		JobRun newJobRun = JobRun.builder()
				.events(List.of(JobRunEvent.builder()
						.happenedAt(Instant.now())
						.nextStatus(JobRunStatus.SCHEDULED)
						.build()))
				.faults(List.empty()) // FIXME: This is a crutch due to null-handling flaw in generated builder.
				.jobRunnerClassName(jobRunnerClass.getName())
				.parameters(converterRegistry.convert(parameters, GenericObject.class))
				.build();
		//
		JobRun createdJobRun = jobRunObjectManipulator.createWithGeneratedUid(newJobRun);
		UUID jobRunUid = createdJobRun.getUid();
		//
		List<ExecutionContextAttribute<?>> contextAttributes = jobContextAttributeContributors
				.flatMap(jobContextAttributeContributor -> jobContextAttributeContributor
						.contributeJobContextAttributes(jobRunnerClass, parameters));
		//
		jobRunEnrichingExecutionContextAttributesContributor.setAttributes(contextAttributes);
		//
		try {
			logger.info("Scheduled Job: uid = {}", jobRunUid);
			//
			taskExecutor.execute(
					() -> {
						logger.info("Starting Job: uid = {}", jobRunUid);
						//
						Instant startedAt = Instant.now();
						//
						pandoraActivityRegistry.updateActivity(PandoraActivity.builder()
								.colourIndicator(PandoraActivityColourIndicator.BLUE)
								.startedAt(startedAt)
								.statusText(jobRunnerClass.getSimpleName())
								.uid(jobRunUid)
								.build());
						//
						boolean succeeded = false;
						Instant finishedAt = null;
						//
						try {
							jobRunObjectManipulator.updateObjectWithOperator(
									UidJobRunReference.of(jobRunUid),
									currentObject -> currentObject.toBuilder()
											.events(currentObject.getEvents().append(JobRunEvent.builder()
													.happenedAt(startedAt)
													.nextStatus(JobRunStatus.STARTED)
													.build()))
											.build());
							//
							Throwable fault = null;
							//
							try {
								jobRunner.run(JobRunner.Context.<P>builder()
										.logSink(null)
										.parameters(parameters)
										.build());
								succeeded = true;
							} catch (Throwable ex) {
								//
								// TODO: Find out if it is fine to do unwrapping before logs.
								fault = ExceptionUtils.unwrap(ex);
								//
								logger.warn("", ex);
							} finally {
								finishedAt = Instant.now();
								Instant finalFinishedAt = finishedAt;
								//
								Throwable finalFault = fault;
								//
								jobRunObjectManipulator.updateObjectWithOperator(
										UidJobRunReference.of(jobRunUid),
										currentObject -> currentObject.toBuilder()
												.events(currentObject.getEvents().append(JobRunEvent.builder()
														.happenedAt(finalFinishedAt)
														.nextStatus(finalFault == null
																? JobRunStatus.COMPLETED
																: JobRunStatus.FAILED)
														.build()))
												.build());
							}
						} finally {
							logger.info("Finishing Job: uid = {}", jobRunUid);
							//
							Instant finalFinishedAt = finishedAt != null ? finishedAt : Instant.now();
							//
							pandoraActivityRegistry.updateActivity(PandoraActivity.builder()
									.colourIndicator(succeeded
											? PandoraActivityColourIndicator.GREEN
											: PandoraActivityColourIndicator.RED)
									.finishedAt(finalFinishedAt)
									.startedAt(startedAt)
									.statusText(jobRunnerClass.getSimpleName())
									.uid(jobRunUid)
									.build());
						}
					});
		} finally {
			jobRunEnrichingExecutionContextAttributesContributor.clearAttributes();
		}
		//
		return UidJobRunReference.from(createdJobRun);
	}
}
