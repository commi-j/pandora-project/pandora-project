package tk.labyrinth.pandora.jobs.model.jobrun;

import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.UUID;

@Value(staticConstructor = "of")
public class UidJobRunReference implements Reference<JobRun> {

	@NonNull
	UUID uid;

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				JobRun.MODEL_CODE,
				JobRun.UID_ATTRIBUTE_NAME,
				uid);
	}

	public static UidJobRunReference from(JobRun object) {
		return UidJobRunReference.of(object.getUid());
	}
}
