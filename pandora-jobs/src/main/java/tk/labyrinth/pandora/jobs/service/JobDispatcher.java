package tk.labyrinth.pandora.jobs.service;

import tk.labyrinth.pandora.jobs.old.JobExecutionRequest;

public interface JobDispatcher {

	void dispatch(JobExecutionRequest<?> jobExecutionRequest);
}
