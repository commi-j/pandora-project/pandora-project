package tk.labyrinth.pandora.jobs.model.job;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTag;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(Job.MODEL_CODE)
@ModelTag("jobs")
@Value
@With
public class Job {

	public static final String JOB_RUNNER_CLASS_ATTRIBUTE_NAME = "jobRunnerClass";

	public static final String MODEL_CODE = "job";

	// FIXME: <? extends BeholderJobRunner<?>> Support generics by builder generator.
	Class jobRunnerClass;
}
