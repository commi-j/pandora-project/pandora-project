package tk.labyrinth.pandora.jobs.model.jobrun;

public enum JobRunStatus {
	CANCELLED,
	COMPLETED,
	FAILED,
	SCHEDULED,
	STARTED,
}
