package tk.labyrinth.pandora.jobs.old;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import tk.labyrinth.pandora.jobs.process.JobProcessor;

import java.util.List;

// NOTE: Copy these annotations to inheriting classes.
@Data // @EqualsAndHashCode, @Getter, @ToString
@FieldDefaults(level = AccessLevel.PRIVATE) // @Value equivalent
@NoArgsConstructor(access = AccessLevel.PROTECTED) // Default constructor for utility libraries, not for developers.
@Setter(AccessLevel.NONE) // Effectively immutable
@SuperBuilder(toBuilder = true) // Constructed by developers via builders
public class JobExecutionLog<P> {

	String initiator;

	List<JobExecutionLogEntry> logEntries;

	P parameters;

	Class<? extends JobProcessor<P>> processorType;
}
