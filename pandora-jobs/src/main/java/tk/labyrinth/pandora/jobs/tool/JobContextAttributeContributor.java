package tk.labyrinth.pandora.jobs.tool;

import io.vavr.collection.List;
import tk.labyrinth.pandora.context.executioncontext.ExecutionContextAttribute;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.jobs.model.jobrunner.JobRunner;

@PandoraExtensionPoint
public interface JobContextAttributeContributor {

	<P> List<ExecutionContextAttribute<?>> contributeJobContextAttributes(
			Class<? extends JobRunner<P>> jobRunnerClass,
			P parameters);
}
