package tk.labyrinth.pandora.jobs.model.jobrunindex;

import io.vavr.collection.List;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.jobs.model.jobrun.JobRun;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.stores.objectindex.Indexer;
import tk.labyrinth.pandora.stores.rootobject.UidReference;

@LazyComponent
public class JobRunIndexer implements Indexer<JobRun, JobRunIndex> {

	@Override
	public List<JobRunIndex> createIndices(JobRun target) {
		return List.of(JobRunIndex.builder()
				.jobRunnerClassName(target.getJobRunnerClassName())
				.targetReference(createTargetReference(target))
				.parameters(target.getParameters())
				.scheduledAt(target.getEvents().get(0).getHappenedAt())
				.status(target.getEvents().last().getNextStatus())
				.updatedAt(target.getEvents().last().getHappenedAt())
				.build());
	}

	@Override
	public UidReference<JobRun> createTargetReference(JobRun target) {
		return UidReference.of(JobRun.MODEL_CODE, target.getUid());
	}
}
