
- [Datatypes] Rework 'modelReference' so that it's not a mandatory attribute and rename it to have 'pandora' prefix to avoid collisions;
- [Datatypes/UI] Make it possible to specify context to get list of items from (e.g. selecting from attributes of this or parent object);
