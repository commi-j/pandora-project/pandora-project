package tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic;

import tk.labyrinth.pandora.datatypes.domain.tag.Tag;

public class PolymorphicConstants {

	public static final String TAG_NAME = "polymorphic";

	public static final Tag TAG = Tag.of(TAG_NAME);
}
