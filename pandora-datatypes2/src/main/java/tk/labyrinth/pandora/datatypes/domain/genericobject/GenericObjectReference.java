package tk.labyrinth.pandora.datatypes.domain.genericobject;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

@Value(staticConstructor = "of")
public class GenericObjectReference implements Reference<GenericObject> {

	@NonNull
	GenericReference<GenericObject> reference;

	@Nullable
	public GenericReferenceAttribute findAttribute(String attributeName) {
		return reference.findAttribute(attributeName);
	}

	@Nullable
	public String findAttributeValue(String attributeName) {
		return reference.getAttributeValue(attributeName);
	}

	public GenericReferenceAttribute getAttribute(String attributeName) {
		return reference.getAttribute(attributeName);
	}

	public String getAttributeValue(String attributeName) {
		return reference.getAttributeValue(attributeName);
	}

	public List<GenericReferenceAttribute> getAttributes() {
		return reference.getAttributes();
	}

	/**
	 * Returns code of the model of object to which this reference refers to.<br>
	 * E.g. if this reference is <b>foo(bar=12)</b>, result will be <b>foo</b>.<br>
	 *
	 * @return non-null
	 */
	public String getModelCode() {
		return reference.getModelCode();
	}

	/**
	 * Returns reference to the model of object to which this reference refers to.<br>
	 * E.g. if this reference is <b>foo(bar=12)</b>, result will be <b>model(code=foo)</b>.<br>
	 *
	 * @return non-null
	 */
	public CodeObjectModelReference getModelReference() {
		return reference.getModelReference();
	}

	public GenericReference<?> toGenericReference() {
		return reference;
	}

	@SuppressWarnings("unchecked")
	public <T> GenericReference<T> toGenericReferenceInferred() {
		return (GenericReference<T>) reference;
	}

	@Override
	public String toString() {
		return reference.toString();
	}

	@SuppressWarnings("unchecked")
	public static GenericObjectReference from(GenericReference<?> reference) {
		return of((GenericReference<GenericObject>) reference);
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static GenericObjectReference from(String value) {
		return of(GenericReference.from(value));
	}
}
