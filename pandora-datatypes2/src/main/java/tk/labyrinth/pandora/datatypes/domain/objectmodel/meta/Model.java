package tk.labyrinth.pandora.datatypes.domain.objectmodel.meta;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Indexed;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Default annotation will use:<br>
 * - {@link Object#getClass() Class}.{@link Class#getSimpleName() getSimpleName()}.{@link String#toLowerCase() toLowerCase()} for code;<br>
 * - {@link Object#getClass() Class}.{@link Class#getSimpleName() getSimpleName()} for name;<br>
 */
@Documented
@Indexed
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Model {

	/**
	 * Default will use {@link Object#getClass() Class}.{@link Class#getSimpleName() getSimpleName()}.{@link String#toLowerCase() toLowerCase()}.
	 *
	 * @return code
	 */
	@AliasFor("value")
	String code() default "";

	/**
	 * See {@link JavaBaseType#getHasCustomObjectModel()}
	 *
	 * @return hasCustomModel
	 */
	boolean hasCustomModel() default false;

	/**
	 * Default will use {@link Object#getClass() Class}.{@link Class#getSimpleName() getSimpleName()}.
	 *
	 * @return name
	 */
	String name() default "";

	@AliasFor("code")
	String value() default "";
}
