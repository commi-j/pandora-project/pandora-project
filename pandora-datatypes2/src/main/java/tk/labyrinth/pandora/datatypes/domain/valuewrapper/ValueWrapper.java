package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import org.checkerframework.checker.nullness.qual.Nullable;

public interface ValueWrapper {

	ListValueWrapper asList();

	ObjectValueWrapper asObject();

	SimpleValueWrapper asSimple();

	boolean isList();

	boolean isObject();

	boolean isSimple();

	@Override
	String toString();

	Object unwrap();

	@Nullable
	static Object unwrapNullable(@Nullable ValueWrapper value) {
		return value != null ? value.unwrap() : null;
	}
}
