package tk.labyrinth.pandora.datatypes.domain.fault;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.io.IOException;

@LazyComponent
public class FaultObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module, objectMapper);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module, ObjectMapper objectMapper) {
		JsonDeserializer deserializer = new Deserializer(objectMapper);
		//
		module.addDeserializer(deserializer.handledType(), deserializer);
		//
		return module;
	}

	public static class Deserializer extends StdNodeBasedDeserializer<Fault> {

		private final ObjectMapper objectMapper;

		public Deserializer(ObjectMapper objectMapper) {
			super(Fault.class);
			this.objectMapper = objectMapper;
		}

		@Override
		public Fault convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			Fault result;
			{
				if (root.has("classSignature")) {
					result = objectMapper.convertValue(root, ThrowableFault.class);
				} else {
					// TODO: Probably create a special mapper that will read all the attributes.
					result = objectMapper.convertValue(root, StructuredFault.class);
				}
			}
			return result;
		}
	}
}
