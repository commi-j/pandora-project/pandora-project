package tk.labyrinth.pandora.datatypes.domain.datatypebase;

import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicRoot;

@PolymorphicRoot(qualifierAttributeName = "name")
public interface DatatypeBaseFeature {
	// empty
}
