package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import lombok.Value;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;

@PolymorphicLeaf(rootClass = DatatypeBaseFeature.class, qualifierAttributeValue = "backing-java-variable-type")
@Value(staticConstructor = "of")
public class BackingJavaVariableTypeDatatypeBaseFeature implements DatatypeBaseFeature {

	TypeDescription variableTypeDescription;
}
