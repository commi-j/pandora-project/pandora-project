package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;

import java.util.Objects;

@Value(staticConstructor = "of")
public class SignatureJavaBaseTypeReference implements Reference<JavaBaseType> {

	TypeSignature signature;

	public Class<?> resolveClass() {
		return ClassUtils.get(signature.getBinaryName());
	}

	public GenericReference<JavaBaseType> toGenericReference() {
		return GenericReference.from(toString());
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				JavaBaseType.MODEL_CODE,
				JavaBaseType.SIGNATURE_ATTRIBUTE_NAME,
				signature);
	}

	public static SignatureJavaBaseTypeReference from(Class<?> javaClass) {
		return of(TypeSignature.of(javaClass));
	}

	public static SignatureJavaBaseTypeReference from(GenericReference<JavaBaseType> reference) {
		return of(getSignature(reference));
	}

	public static SignatureJavaBaseTypeReference from(JavaBaseType javaClass) {
		return of(javaClass.getSignature());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static SignatureJavaBaseTypeReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static SignatureJavaBaseTypeReference from(TypeDescription typeDescription) {
		return of(TypeSignature.ofValid(typeDescription.getFullName()));
	}

	public static SignatureJavaBaseTypeReference fromTypeSignatureString(String typeSignatureString) {
		return of(TypeSignature.ofValid(typeSignatureString));
	}

	public static TypeSignature getSignature(GenericReference<JavaBaseType> reference) {
		return TypeSignature.ofValid(requireJavaBaseTypeReference(reference).getAttributeValue(
				JavaBaseType.SIGNATURE_ATTRIBUTE_NAME));
	}

	public static boolean isJavaBaseTypeReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), JavaBaseType.MODEL_CODE);
	}

	public static String render(String signature) {
		return "%s(%s=%s)".formatted(
				JavaBaseType.MODEL_CODE,
				JavaBaseType.SIGNATURE_ATTRIBUTE_NAME,
				signature);
	}

	@SuppressWarnings("unchecked")
	public static GenericReference<JavaBaseType> requireJavaBaseTypeReference(GenericReference<?> reference) {
		if (!isJavaBaseTypeReference(reference)) {
			throw new IllegalArgumentException("Require JavaBaseTypeReference: " + reference);
		}
		return (GenericReference<JavaBaseType>) reference;
	}
}
