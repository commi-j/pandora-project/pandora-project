package tk.labyrinth.pandora.datatypes.domain.genericreference;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Reference
 * code		reference(modelReference.code,attributes)
 * full		reference()
 * smpl		model=model
 *
 * ref		reference(model.code,attributes)
 * ex		reference(model=model.reference,attributes=(parent.code,attributes))
 * 			reference(model=datatype,attributes=(code))
 *
 * ref		reference(code)
 * ex		reference(code=model.reference(parent.code,attributes))
 * 			reference(code=datatype(code))
 *
 * ModelCodeReference
 * code		reference(model(code))
 * full		reference()
 * smpl		reference
 *
 * ModelReferenceReference
 * 		model.reference(parent.code,attributes)
 *
 * DatatypeCodeReference
 * ref		datatype(code)
 * ex		reference(code=model.reference(parent.code,attributes))
 *
 */
@Value
@With
public class GenericReference<T> implements Reference<T> {

	public static final Pattern PATTERN = Pattern.compile("(?<modelCode>.+?)\\((?<attributes>.+)\\)");

	public static final String MODEL_CODE_ATTRIBUTE_NAME = "modelCode";

	/**
	 * Corresponds to model.attribute.name<br>
	 * <br>
	 * List: 1..n, sorted;<br>
	 * Elements: ascii;<br>
	 */
	@NonNull
	List<GenericReferenceAttribute> attributes;

	/**
	 * Corresponds to model.code.
	 */
	@NonNull
	String modelCode;

	private GenericReference(@NonNull List<GenericReferenceAttribute> attributes, String modelCode) {
		if (attributes.map(GenericReferenceAttribute::getName).distinct().size() != attributes.size()) {
			throw new IllegalArgumentException("Require no duplicate attributes");
		}
		//
		// Sorting to ensure canonical representation.
		this.attributes = attributes.sortBy(GenericReferenceAttribute::getName);
		this.modelCode = modelCode;
	}

	@Nullable
	public GenericReferenceAttribute findAttribute(String attributeName) {
		return attributes.find(attribute -> Objects.equals(attribute.getName(), attributeName)).getOrNull();
	}

	@Nullable
	public String findAttributeValue(String attributeName) {
		GenericReferenceAttribute attribute = findAttribute(attributeName);
		//
		return attribute != null ? attribute.getValue() : null;
	}

	public GenericReferenceAttribute getAttribute(String attributeName) {
		GenericReferenceAttribute result = findAttribute(attributeName);
		//
		if (result == null) {
			throw new IllegalArgumentException("Required: attributeName = %s, this = %s".formatted(attributeName, this));
		}
		//
		return result;
	}

	public String getAttributeValue(String attributeName) {
		String result = findAttributeValue(attributeName);
		//
		if (result == null) {
			throw new IllegalArgumentException("Required: attributeName = %s, this = %s".formatted(attributeName, this));
		}
		//
		return result;
	}

	public CodeObjectModelReference getModelReference() {
		return CodeObjectModelReference.of(modelCode);
	}

	public boolean hasAttribute(String attributeName) {
		return attributes.exists(attribute -> Objects.equals(attribute.getName(), attributeName));
	}

	// TODO: support chained paths
	public String lookup(String path) {
		return attributes
				.find(attribute -> Objects.equals(attribute.getName(), path))
				.map(GenericReferenceAttribute::getValue)
				.getOrNull();
	}

	@Override
	public String toString() {
		return modelCode + "(" + attributes.mkString(",") + ")";
	}

	/**
	 * Throws an exception if specified attribute name already present.
	 *
	 * @param attributeToAdd non-null
	 *
	 * @return non-null
	 */
	public GenericReference<T> withAddedAttribute(@NonNull GenericReferenceAttribute attributeToAdd) {
		return withAddedAttributes(List.of(attributeToAdd));
	}

	/**
	 * Throws an exception if any of specified attribute names already present.
	 *
	 * @param attributesToAdd non-empty
	 *
	 * @return non-null
	 */
	public GenericReference<T> withAddedAttributes(@NonNull List<GenericReferenceAttribute> attributesToAdd) {
		if (attributes.exists(attribute -> attributesToAdd.exists(attributeToAdd ->
				Objects.equals(attributeToAdd.getName(), attribute.getName())))) {
			throw new IllegalArgumentException("Already exists: attributesToAdd = %s, this = %s"
					.formatted(attributesToAdd, this));
		}
		return of(modelCode, attributes.appendAll(attributesToAdd));
	}

	public static <T> GenericReference<T> from(Reference<?> reference) {
		return from(reference.toString());
	}

	// FIXME: @JsonCreator must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static <T> GenericReference<T> from(String value) {
		GenericReference<T> result;
		{
			Matcher matcher = PATTERN.matcher(value);
			if (matcher.find()) {
				List<GenericReferenceAttribute> attributes;
				{
					String attributesString = matcher.group("attributes");
					// TODO: We removed () from delimiters, not sure if we need them here.
					StringTokenizer tokenizer = new StringTokenizer(attributesString, ",()", true);
					List<String> rawAttributes = List.of("");
					int depth = 0;
					while (tokenizer.hasMoreTokens()) {
						String token = tokenizer.nextToken();
						switch (token) {
							case "," -> {
								if (depth == 0) {
									rawAttributes = rawAttributes.append("");
								} else {
									rawAttributes = rawAttributes.update(
											rawAttributes.size() - 1,
											rawAttribute -> rawAttribute + token);
								}
							}
							case "(" -> {
								depth++;
								rawAttributes = rawAttributes.update(
										rawAttributes.size() - 1,
										rawAttribute -> rawAttribute + token);
							}
							case ")" -> {
								depth--;
								rawAttributes = rawAttributes.update(
										rawAttributes.size() - 1,
										rawAttribute -> rawAttribute + token);
							}
							default -> rawAttributes = rawAttributes.update(
									rawAttributes.size() - 1,
									rawAttribute -> rawAttribute + token);
						}
					}
					attributes = rawAttributes.map(GenericReferenceAttribute::from);
				}
				//
				result = of(matcher.group("modelCode"), attributes);
			} else {
				throw new IllegalArgumentException(value);
			}
		}
		return result;
	}

	public static boolean isReference(String value) {
		return PATTERN.matcher(value).matches();
	}

	public static <T> GenericReference<T> of(String modelCode, GenericReferenceAttribute... attributes) {
		return of(modelCode, List.of(attributes));
	}

	public static <T> GenericReference<T> of(String modelCode, @NonNull List<GenericReferenceAttribute> attributes) {
		return new GenericReference<>(attributes, modelCode);
	}
}
