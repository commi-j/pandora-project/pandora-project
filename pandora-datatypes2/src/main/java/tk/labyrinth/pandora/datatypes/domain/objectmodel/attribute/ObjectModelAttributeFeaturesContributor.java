package tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute;

import io.vavr.collection.List;
import tk.labyrinth.pandora.core.meta.PandoraJavaContext;

import java.lang.reflect.Member;

@PandoraJavaContext
public interface ObjectModelAttributeFeaturesContributor {

	List<ObjectModelAttributeFeature> contributeFeatures(Member javaMember);
}
