package tk.labyrinth.pandora.datatypes.domain.tag;

import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;

@RenderPattern("$name${value?\\:$:}")
@Value(staticConstructor = "of")
@With
public class Tag {

	public static final String NAME_ATTRIBUTE_NAME = "name";

	public static final String VALUE_ATTRIBUTE_NAME = "value";

	String name;

	@Nullable
	String value;

	@Override
	public String toString() {
		return value != null
				? "%s:%s".formatted(name, value)
				: name;
	}

	public static Tag of(String name) {
		return of(name, null);
	}
}
