package tk.labyrinth.pandora.datatypes.domain.datatype;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(code = Datatype.MODEL_CODE)
@ModelTagPandora
@RenderPattern("$baseReference${parameters?<$>:}")
//@RenderFunction("render") // FIXME: The actual function accesses stores (or registries?), so this rule should be located there.
@Value
@With
public class Datatype {

	public static final String MODEL_CODE = "datatype";

	/**
	 * Non-null.
	 */
	Reference<DatatypeBase> baseReference;

	/**
	 * Non-empty or null. Null if base is not parameterized.
	 */
	List<Datatype> parameters;

	public static Datatype ofNonParameterized(Reference<DatatypeBase> baseReference) {
		return new Datatype(baseReference, null);
	}
}
