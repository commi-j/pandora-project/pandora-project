package tk.labyrinth.pandora.datatypes.domain.genericreference;

import io.vavr.collection.Map;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.pattern.PandoraPattern;

@Builder(toBuilder = true)
@Value
public class GenericReferenceAttribute {

	public static final PandoraPattern PATTERN = PandoraPattern.from("$name=$value");

	@NonNull
	String name;

	@NonNull
	String value;

	@Override
	public String toString() {
		return name + "=" + value;
	}

	public static GenericReferenceAttribute from(String value) {
		Map<String, String> parsedValue = PATTERN.parse(value);
		if (parsedValue.size() != 2) {
			throw new IllegalArgumentException("Require pattern '$name=$value': %s".formatted(value));
		}
		//
		return of(parsedValue.get("name").get(), parsedValue.get("value").get());
	}

	public static GenericReferenceAttribute of(String name, String value) {
		return new GenericReferenceAttribute(name, value);
	}
}
