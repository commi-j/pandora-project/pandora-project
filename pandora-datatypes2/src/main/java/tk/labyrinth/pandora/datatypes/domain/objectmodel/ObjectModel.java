package tk.labyrinth.pandora.datatypes.domain.objectmodel;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.SearchDataAttributes;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;

import java.util.Objects;

@Builder(builderClassName = "Builder", toBuilder = true)
@ModelTagPandora
@RenderAttribute("name")
@RootModel(code = ObjectModel.MODEL_CODE, primaryAttribute = ObjectModel.CODE_ATTRIBUTE_NAME)
@SearchDataAttributes({
		ObjectModel.CODE_ATTRIBUTE_NAME,
		ObjectModel.NAME_ATTRIBUTE_NAME})
@Value
@With
public class ObjectModel {

	public static final String CODE_ATTRIBUTE_NAME = "code";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	public static final String MODEL_CODE = "objectmodel";

	public static final String TAGS_ATTRIBUTE_NAME = "tags";

	List<ObjectModelAttribute> attributes;

	String code;

	List<ObjectModelFeature> features;

	String name;

	/**
	 * 1..n or null.
	 */
	@Nullable
	List<String> parameterNames;

	String renderRule;

	List<Tag> tags;

	@Nullable
	public ObjectModelAttribute findAttribute(String attributeName) {
		return getAttributesOrEmpty()
				.find(attribute -> Objects.equals(attribute.getName(), attributeName))
				.getOrNull();
	}

	public <F extends ObjectModelFeature> F findFeature(Class<F> featureClass) {
		// TODO: Fail if multiple.
		return getFeaturesOrEmpty()
				.filter(featureClass::isInstance)
				.map(featureClass::cast)
				.getOrNull();
	}

	public List<ObjectModelAttribute> getAttributesOrEmpty() {
		return attributes != null ? attributes : List.empty();
	}

	public List<ObjectModelFeature> getFeaturesOrEmpty() {
		return features != null ? features : List.empty();
	}

	public List<Tag> getTagsOrEmpty() {
		return tags != null ? tags : List.empty();
	}

	public ObjectModel withAddedTag(Tag tag) {
		if (tags != null && tags.exists(tagElement -> Objects.equals(tagElement.getName(), tag.getName()))) {
			throw new IllegalArgumentException("Require no tag present: %s".formatted(tag));
		}
		//
		return toBuilder()
				.tags(tags != null ? tags.append(tag).sortBy(Tag::getName) : List.of(tag))
				.build();
	}
}
