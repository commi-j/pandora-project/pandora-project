package tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeature;

@Builder(builderClassName = "Builder", toBuilder = true)
@PolymorphicLeaf(rootClass = ObjectModelFeature.class, qualifierAttributeValue = "polymorphic")
@Value
@With
public class PolymorphicObjectModelFeature implements ObjectModelFeature {

	GenericObjectAttribute qualifierAttribute;

	AliasDatatypeBaseReference rootReference;
}
