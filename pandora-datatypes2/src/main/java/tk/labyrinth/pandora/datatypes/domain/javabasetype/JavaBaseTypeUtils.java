package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import io.vavr.collection.List;
import lombok.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils;

import java.lang.reflect.Type;

public class JavaBaseTypeUtils {

	public static final String DATATYPE_ALIAS_PREFIX = "javabasetype:";

	public static String createDatatypeAlias(Class<?> javaClass) {
		return createDatatypeAlias(TypeSignature.of(javaClass));
	}

	public static String createDatatypeAlias(TypeSignature typeSignature) {
		return "%s%s".formatted(DATATYPE_ALIAS_PREFIX, typeSignature);
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(Class<?> javaClass) {
		return createDatatypeBaseReference(TypeSignature.of(javaClass));
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(
			SignatureJavaBaseTypeReference javaBaseTypeReference) {
		return createDatatypeBaseReference(javaBaseTypeReference.getSignature());
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(TypeSignature typeSignature) {
		return AliasDatatypeBaseReference.of(createDatatypeAlias(typeSignature));
	}

	public static Datatype createDatatypeFromJavaType(Type javaType) {
		return Datatype.builder()
				.baseReference(JavaBaseTypeUtils.createDatatypeBaseReference(TypeUtils.getClass(javaType)))
				.parameters(TypeUtils.isParameterizedType(javaType)
						? List.of(TypeUtils.asParameterizedType(javaType).getActualTypeArguments())
						.map(JavaBaseTypeUtils::createDatatypeFromJavaType)
						: null)
				.build();
	}

	public static Datatype createDatatypeFromPlainJavaClass(Class<?> plainJavaClass) {
		// TODO: check for other non-declared.
		ClassUtils.requirePlain(plainJavaClass);
		//
		return createParameterlessDatatype(plainJavaClass);
	}

	public static Datatype createDatatypeFromTypeDescription(TypeDescription typeDescription) {
		return Datatype.builder()
				.baseReference(typeDescription.isVariable()
						? JavaBaseTypeParameterUtils.createDatatypeBaseReference(typeDescription)
						: JavaBaseTypeUtils.createDatatypeBaseReference(typeDescription.getSignature()))
				.parameters(typeDescription.isParameterized()
						? List.ofAll(typeDescription.getParametersOrFail())
						.map(JavaBaseTypeUtils::createDatatypeFromTypeDescription)
						: null)
				.build();
	}

	public static String createJavaBaseTypeObjectModelCode(SignatureJavaBaseTypeReference javaBaseTypeReference) {
		return "%s:%s".formatted(JavaBaseType.MODEL_CODE, javaBaseTypeReference.getSignature());
	}

	public static String createJavaBaseTypeObjectModelCode(Class<?> javaClass) {
		return "%s:%s".formatted(JavaBaseType.MODEL_CODE, TypeSignature.of(javaClass));
	}

	public static Datatype createParameterlessDatatype(Class<?> javaClass) {
		return Datatype.builder()
				.baseReference(createDatatypeBaseReference(javaClass))
				.parameters(null)
				.build();
	}

	// TODO: Rename to get this and other "extract" methods.
	public static SignatureJavaBaseTypeReference extractJavaBaseTypeReference(
			Reference<DatatypeBase> datatypeBaseReference) {
		if (!isJavaBaseTypeAliasDatatypeBaseReference(datatypeBaseReference)) {
			throw new IllegalArgumentException("Require JavaBaseTypeAlias reference: %s".formatted(datatypeBaseReference));
		}
		//
		return SignatureJavaBaseTypeReference.of(TypeSignature.ofValid(
				AliasDatatypeBaseReference.from(datatypeBaseReference).getAlias().substring(DATATYPE_ALIAS_PREFIX.length())));
	}

	@Nullable
	public static SignatureJavaBaseTypeReference findJavaBaseTypeReference(@NonNull DatatypeBase datatypeBase) {
		return findJavaBaseTypeReference(datatypeBase.getAliases());
	}

	@Deprecated
	@Nullable
	public static SignatureJavaBaseTypeReference findJavaBaseTypeReference(
			CodeObjectModelReference objectModelReference) {
		return JavaBaseTypeUtils.isJavaBaseTypeModelCode(objectModelReference.getCode())
				? SignatureJavaBaseTypeReference.fromTypeSignatureString(objectModelReference.getCode()
				.substring("%s:".formatted(JavaBaseType.MODEL_CODE).length()))
				: null;
	}

	@Nullable
	public static SignatureJavaBaseTypeReference findJavaBaseTypeReference(List<String> datatypeAliases) {
		SignatureJavaBaseTypeReference result;
		{
			List<String> signatures = datatypeAliases
					.filter(datatypeAlias -> datatypeAlias.startsWith(DATATYPE_ALIAS_PREFIX))
					.map(datatypeAlias -> datatypeAlias.substring(DATATYPE_ALIAS_PREFIX.length()));
			//
			if (signatures.size() > 1) {
				throw new IllegalArgumentException("Require <=1: %s".formatted(signatures));
			}
			//
			result = !signatures.isEmpty()
					? SignatureJavaBaseTypeReference.of(TypeSignature.ofValid(signatures.get()))
					: null;
		}
		return result;
	}

	public static CodeObjectModelReference getJavaBaseTypeObjectModelReference(Class<?> javaClass) {
		return CodeObjectModelReference.of(createJavaBaseTypeObjectModelCode(javaClass));
	}

	@Deprecated
	public static CodeObjectModelReference getJavaBaseTypeObjectModelReference(
			SignatureJavaBaseTypeReference javaBaseTypeReference) {
		return CodeObjectModelReference.of(createJavaBaseTypeObjectModelCode(javaBaseTypeReference));
	}

	public static Class<?> getJavaClassFromObjectModelReference(CodeObjectModelReference objectModelReference) {
		Class<?> result;
		{
			SignatureJavaBaseTypeReference javaBaseTypeReference = findJavaBaseTypeReference(objectModelReference);
			//
			if (javaBaseTypeReference != null) {
				result = javaBaseTypeReference.resolveClass();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	@Deprecated // TODO: Use JavaBaseTypeRegistry#getJavaType
	public static Type getJavaTypeFromDatatype(Datatype datatype) {
		Type result;
		{
			Class<?> baseClass = extractJavaBaseTypeReference(datatype.getBaseReference()).resolveClass();
			//
			if (datatype.getParameters() != null) {
				result = org.apache.commons.lang3.reflect.TypeUtils.parameterize(
						baseClass,
						datatype.getParameters()
								.map(JavaBaseTypeUtils::getJavaTypeFromDatatype)
								.toJavaArray(Type[]::new));
			} else {
				result = baseClass;
			}
		}
		return result;
	}

	public static boolean isJavaBaseTypeAliasDatatypeBaseReference(Reference<DatatypeBase> datatypeBaseReference) {
		boolean result;
		{
			if (AliasDatatypeBaseReference.isAliasDatatypeBaseReference(datatypeBaseReference)) {
				result = isJavaBaseTypeDatatypeBaseReference(AliasDatatypeBaseReference.from(datatypeBaseReference));
			} else {
				result = false;
			}
		}
		return result;
	}

	@Deprecated
	public static boolean isJavaBaseTypeDatatypeBaseReference(AliasDatatypeBaseReference datatypeBaseReference) {
		return datatypeBaseReference.getAlias().startsWith(DATATYPE_ALIAS_PREFIX);
	}

	public static boolean isJavaBaseTypeModelCode(String objectModelCode) {
		return objectModelCode.startsWith("%s:".formatted(JavaBaseType.MODEL_CODE));
	}

	public static boolean isJavaBaseTypeObjectModelReference(CodeObjectModelReference objectModelReference) {
		return isJavaBaseTypeModelCode(objectModelReference.getCode());
	}
}
