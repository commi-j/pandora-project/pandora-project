package tk.labyrinth.pandora.datatypes.domain.datatypebase;

import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

@PolymorphicLeaf(rootClass = DatatypeBaseFeature.class, qualifierAttributeValue = "generic")
@Value(staticConstructor = "of")
public class GenericDatatypeBaseFeature implements DatatypeBaseFeature {

	// TODO: Or call it smth with "declaring"?
	Reference<DatatypeBase> scope;
}
