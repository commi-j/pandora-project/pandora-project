package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;

import java.lang.reflect.TypeVariable;

public class JavaBaseTypeParameterUtils {

	public static final String DATATYPE_ALIAS_PREFIX = "javabasetypeparameter:";

	public static String composeSignature(String parent, String parameterName) {
		return "%s%s%s".formatted(parent, SignatureSeparators.TYPE_PARAMETER, parameterName);
	}

	public static String createDatatypeAlias(TypeDescription variableTypeDescription) {
		variableTypeDescription.requireVariable();
		//
		return "%s%s".formatted(DATATYPE_ALIAS_PREFIX, variableTypeDescription);
	}

	public static String createDatatypeAlias(TypeSignature baseTypeSignature, String parameterName) {
		return createDatatypeAlias(TypeDescription.of(composeSignature(baseTypeSignature.toString(), parameterName)));
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(TypeDescription variableTypeDescription) {
		return AliasDatatypeBaseReference.of(createDatatypeAlias(variableTypeDescription));
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(TypeVariable<?> javaTypeVariable) {
		return createDatatypeBaseReference(TypeDescription.of(javaTypeVariable));
	}
}
