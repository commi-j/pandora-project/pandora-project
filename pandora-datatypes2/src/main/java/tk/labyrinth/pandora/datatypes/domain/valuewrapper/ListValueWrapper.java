package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class ListValueWrapper implements ValueWrapper {

	@NonNull
	List<ValueWrapper> value;

	@Override
	public ListValueWrapper asList() {
		return this;
	}

	@Override
	public ObjectValueWrapper asObject() {
		throw new UnsupportedOperationException();
	}

	@Override
	public SimpleValueWrapper asSimple() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isList() {
		return true;
	}

	@Override
	public boolean isObject() {
		return false;
	}

	@Override
	public boolean isSimple() {
		return false;
	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public List<?> unwrap() {
		return value.map(element -> element != null ? element.unwrap() : null);
	}

	@SuppressWarnings("unchecked")
	public List<String> unwrapAsListOfSimple() {
		List<?> unwrapped = unwrap();
		//
		if (unwrapped.exists(element -> element != null && !(element instanceof String))) {
			throw new IllegalStateException("Require only simple or null elements: %s".formatted(unwrapped));
		}
		//
		return (List<String>) unwrapped;
	}

	public static ListValueWrapper empty() {
		return of(List.empty());
	}

	public static ListValueWrapper ofElements(ValueWrapper... elements) {
		return of(List.of(elements));
	}
}
