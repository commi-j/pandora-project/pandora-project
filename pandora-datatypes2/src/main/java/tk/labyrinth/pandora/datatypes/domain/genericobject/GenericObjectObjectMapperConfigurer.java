package tk.labyrinth.pandora.datatypes.domain.genericobject;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;

import java.io.IOException;

// TODO: Make all registrations single module
@Slf4j
public class GenericObjectObjectMapperConfigurer implements ObjectMapperConfigurer {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module, objectMapper);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module, ObjectMapper objectMapper) {
		Deserializer deserializer = new Deserializer(objectMapper);
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer(objectMapper));
		return module;
	}

	public static class Deserializer extends StdNodeBasedDeserializer<GenericObject> {

		private final ObjectMapper objectMapper;

		public Deserializer(ObjectMapper objectMapper) {
			super(GenericObject.class);
			this.objectMapper = objectMapper;
		}

		@Override
		public GenericObject convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			GenericObject result;
			{
				if (root instanceof ObjectNode objectNode) {
					result = GenericObject.of(StreamUtils.from(objectNode.fields())
							.map(field -> GenericObjectAttribute.of(field.getKey(), objectMapper.convertValue(
									field.getValue(),
									ValueWrapper.class)))
							.collect(List.collector()));
				} else {
					throw new IllegalArgumentException("Require ObjectNode: %s".formatted(root));
				}
			}
			return result;
		}
	}

	public static class Serializer extends StdSerializer<GenericObject> {

		private final ObjectMapper objectMapper;

		public Serializer(ObjectMapper objectMapper) {
			super(GenericObject.class);
			this.objectMapper = objectMapper;
		}

		@Override
		public void serialize(GenericObject value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			value.getAttributes().forEach(attribute -> {
				try {
					gen.writeFieldName(attribute.getName());
					objectMapper.writeValue(gen, attribute.getValue());
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				}
			});
			gen.writeEndObject();
		}
	}
}
