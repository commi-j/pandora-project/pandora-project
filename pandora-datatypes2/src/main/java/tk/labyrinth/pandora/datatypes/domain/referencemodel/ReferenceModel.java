package tk.labyrinth.pandora.datatypes.domain.referencemodel;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

// TODO: Each Reference must have model and predicate transformer.
// TODO: This should be conceptually reworked into smth that can support entirely custom configurations,
//  like: latest by field, first-of-day by field etc.
//  There is a chance we can not bind references to certain attributes.
@Builder(toBuilder = true)
@RootModel
@Value
@With
public class ReferenceModel {

	public static final String ATTRIBUTE_NAMES_ATTRIBUTE_NAME = "attributeNames";

	public static final String MODEL_CODE = ReferenceModel.class.getSimpleName().toLowerCase();

	public static final String MODEL_CODE_ATTRIBUTE_NAME = "modelCode";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	List<String> attributeNames;

	@Deprecated // FIXME: Do we need it?
	@Nullable
	ClassSignature classSignature;

	String modelCode;

	String name;

	public CodeObjectModelReference computeObjectModelReference() {
		return CodeObjectModelReference.of(modelCode);
	}
}
