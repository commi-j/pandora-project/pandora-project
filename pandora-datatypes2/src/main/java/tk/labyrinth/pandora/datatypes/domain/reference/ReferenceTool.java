package tk.labyrinth.pandora.datatypes.domain.reference;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.expresso.query.lang.search.ParameterizedQuery;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.CustomGenericReferenceToPredicateConverter;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.referencemodel.ModelCodeAndAttributeNamesReferenceModelReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class ReferenceTool {

	@Autowired(required = false)
	private final java.util.List<CustomGenericReferenceToPredicateConverter> customReferenceToPredicateConverters;

	public GenericObjectReference createReferenceToGenericObject(
			ModelCodeAndAttributeNamesReferenceModelReference objectReferenceModelReference,
			GenericObject object) {
		return GenericObjectReference.of(GenericReference.of(
				objectReferenceModelReference.getModelCode(),
				objectReferenceModelReference.getAttributeNames()
						.map(attributeName -> GenericReferenceAttribute.builder()
								.name(attributeName)
								// FIXME: Support not simple values.
								.value(object.getAttributeValue(attributeName).asSimple().getValue())
								.build())));
	}

	public ParameterizedQuery<CodeObjectModelReference> referenceToParameterizedQuery(
			Reference<GenericObject> reference) {
		ParameterizedQuery<CodeObjectModelReference> result;
		{
			GenericReference<?> genericReference;
			{
				if (reference instanceof GenericReference<?> castedReference) {
					genericReference = castedReference;
				} else {
					genericReference = GenericReference.from(reference);
				}
			}
			//
			Predicate predicate;
			{
				List<Predicate> predicates = customReferenceToPredicateConverters.stream()
						.map(converter -> converter.convertReferenceToPredicate(genericReference))
						.filter(Objects::nonNull)
						.collect(List.collector());
				//
				if (predicates.size() > 1) {
					throw new IllegalArgumentException();
				}
				//
				predicate = !predicates.isEmpty()
						? predicates.get(0)
						: Predicates.and(genericReference.getAttributes()
						.map(attribute -> Predicates.equalTo(attribute.getName(), attribute.getValue()))
						.toJavaStream());
			}
			//
			result = ParameterizedQuery.<CodeObjectModelReference>builder()
					.limit(2L)
					.parameter(genericReference.getModelReference())
					.predicate(predicate)
					.build();
		}
		return result;
	}

	public static Predicate referenceToPredicate(GenericReference<?> reference) {
		return Predicates.flatAnd(reference.getAttributes()
				.map(attribute -> Predicates.equalTo(attribute.getName(), attribute.getValue()))
				.toJavaStream());
	}

	public static Predicate referenceToPredicate(Reference<?> reference) {
		return referenceToPredicate(GenericReference.from(reference));
	}
}
