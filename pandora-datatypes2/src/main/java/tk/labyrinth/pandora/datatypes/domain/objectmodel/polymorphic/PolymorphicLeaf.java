package tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that this type is one of the concrete implementations for polymorphic type.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PolymorphicLeaf {

	String qualifierAttributeName() default "";

	String qualifierAttributeValue();

	// TODO: We must compute this one for simple cases on our own.
	Class<?> rootClass();
}
