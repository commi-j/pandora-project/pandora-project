package tk.labyrinth.pandora.datatypes.domain.datatyperegistration;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class AliasDatatypeRegistrationReference implements Reference<DatatypeRegistration> {

	String alias;

	public GenericReference<?> toGenericReference() {
		return GenericReference.of(
				DatatypeRegistration.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(DatatypeRegistration.ALIAS_ATTRIBUTE_NAME, alias)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				DatatypeRegistration.MODEL_CODE,
				DatatypeRegistration.ALIAS_ATTRIBUTE_NAME,
				alias);
	}

	public static AliasDatatypeRegistrationReference from(GenericReference<?> reference) {
		if (!isAliasDatatypeRegistrationReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(DatatypeRegistration.ALIAS_ATTRIBUTE_NAME));
	}

	public static AliasDatatypeRegistrationReference from(DatatypeRegistration value) {
		return of(value.getAlias());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static AliasDatatypeRegistrationReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static String getAlias(GenericReference<AliasDatatypeBaseReference> aliasDatatypeRegistrationReference) {
		return aliasDatatypeRegistrationReference.getAttributeValue(DatatypeRegistration.ALIAS_ATTRIBUTE_NAME);
	}

	public static boolean isAliasDatatypeRegistrationReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), DatatypeRegistration.MODEL_CODE) &&
				reference.hasAttribute(DatatypeRegistration.ALIAS_ATTRIBUTE_NAME);
	}
}
