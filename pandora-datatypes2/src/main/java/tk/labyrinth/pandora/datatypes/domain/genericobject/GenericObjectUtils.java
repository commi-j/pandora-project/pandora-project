package tk.labyrinth.pandora.datatypes.domain.genericobject;

import java.util.Objects;

public class GenericObjectUtils {

	public static boolean hasEqualAttribute(String attributeName, GenericObject first, GenericObject second) {
		return Objects.equals(first.findAttributeValue(attributeName), second.findAttributeValue(attributeName));
	}
}
