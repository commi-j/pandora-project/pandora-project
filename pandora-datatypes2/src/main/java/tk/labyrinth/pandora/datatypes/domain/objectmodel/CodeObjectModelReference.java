package tk.labyrinth.pandora.datatypes.domain.objectmodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class CodeObjectModelReference implements Reference<ObjectModel> {

	String code;

	public Datatype toDatatype() {
		return toDatatype(null);
	}

	public Datatype toDatatype(@Nullable List<Datatype> parameters) {
		return Datatype.builder()
				.baseReference(ObjectModelUtils.createDatatypeBaseReference(code))
				.parameters(parameters)
				.build();
	}

	public GenericReference<ObjectModel> toGenericReference() {
		return GenericReference.of(
				ObjectModel.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(ObjectModel.CODE_ATTRIBUTE_NAME, code)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				ObjectModel.MODEL_CODE,
				ObjectModel.CODE_ATTRIBUTE_NAME,
				code);
	}

	public static CodeObjectModelReference from(GenericReference reference) {
		if (!isCodeObjectModelReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(ObjectModel.CODE_ATTRIBUTE_NAME));
	}

	public static CodeObjectModelReference from(ObjectModel value) {
		return of(value.getCode());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static CodeObjectModelReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static boolean isCodeObjectModelReference(GenericReference reference) {
		return Objects.equals(reference.getModelCode(), ObjectModel.MODEL_CODE) &&
				reference.hasAttribute(ObjectModel.CODE_ATTRIBUTE_NAME);
	}
}
