package tk.labyrinth.pandora.datatypes.domain.objectmodel.feature;

import io.vavr.collection.List;
import tk.labyrinth.pandora.core.meta.PandoraJavaContext;

@PandoraJavaContext
public interface ObjectModelFeaturesContributor {

	List<ObjectModelFeature> contributeFeatures(Class<?> javaClass);
}
