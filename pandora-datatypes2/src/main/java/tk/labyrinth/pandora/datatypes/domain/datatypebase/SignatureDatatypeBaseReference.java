package tk.labyrinth.pandora.datatypes.domain.datatypebase;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class SignatureDatatypeBaseReference implements Reference<DatatypeBase> {

	@NonNull
	String signature;

	public GenericReference<DatatypeBase> toGenericReference() {
		return GenericReference.of(
				DatatypeBase.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(DatatypeBase.SIGNATURE_ATTRIBUTE_NAME, signature)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				DatatypeBase.MODEL_CODE,
				DatatypeBase.SIGNATURE_ATTRIBUTE_NAME,
				signature);
	}

	public static SignatureDatatypeBaseReference from(GenericReference<?> reference) {
		if (!isSignatureDatatypeBaseReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(DatatypeBase.SIGNATURE_ATTRIBUTE_NAME));
	}

	public static SignatureDatatypeBaseReference from(DatatypeBase value) {
		return of(value.getName());
	}

	public static SignatureDatatypeBaseReference from(Reference<?> reference) {
		return reference instanceof SignatureDatatypeBaseReference
				? (SignatureDatatypeBaseReference) reference
				: from(GenericReference.from(reference));
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static SignatureDatatypeBaseReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static String getSignature(GenericReference<DatatypeBase> signatureDatatypeBaseReference) {
		return signatureDatatypeBaseReference.getAttributeValue(DatatypeBase.SIGNATURE_ATTRIBUTE_NAME);
	}

	public static String getSignature(Reference<DatatypeBase> signatureDatatypeBaseReference) {
		return from(signatureDatatypeBaseReference).signature;
	}

	public static boolean isSignatureDatatypeBaseReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), DatatypeBase.MODEL_CODE) &&
				reference.hasAttribute(DatatypeBase.SIGNATURE_ATTRIBUTE_NAME);
	}

	public static boolean isSignatureDatatypeBaseReference(Reference<?> reference) {
		return reference instanceof SignatureDatatypeBaseReference ||
				isSignatureDatatypeBaseReference(GenericReference.from(reference));
	}
}
