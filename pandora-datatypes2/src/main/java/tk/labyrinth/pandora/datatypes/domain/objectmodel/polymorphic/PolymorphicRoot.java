package tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that this type is abstract and is represented by one of inheritors.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
// FIXME: There is an issue that objects are added via AppRunner and their poly features are discovered at this time,
//  so other AppRunners may not be able to properly handle poly types if they run before.
//  Need to move model discovery to early stage (look into Lifecycle interface).
public @interface PolymorphicRoot {

	String qualifierAttributeName() default "";
}
