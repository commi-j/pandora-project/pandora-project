package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.java.lang.EnumUtils;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;

@Builder(builderClassName = "Builder", toBuilder = true)
@ModelTagPandora
@RenderAttribute(JavaBaseType.SIGNATURE_ATTRIBUTE_NAME)
@RootModel(code = JavaBaseType.MODEL_CODE, primaryAttribute = JavaBaseType.SIGNATURE_ATTRIBUTE_NAME)
@Value
@With
public class JavaBaseType {

	public static final String MODEL_CODE = "javabasetype";

	public static final String SIGNATURE_ATTRIBUTE_NAME = "signature";

	Boolean dynamic;

	/**
	 * If this is true no ObjectModel is created on JavaBaseType change.
	 */
	Boolean hasCustomObjectModel;

	List<JavaBaseTypeHint> implicitHints;

	List<JavaBaseTypeHint> inheritedHints;

	String name;

	// TODO: Right now it's only populated for @Models. Should be reworked, probably removed.
	@Nullable
	CodeObjectModelReference objectModelReference;

	List<String> parameterNames;

	TypeSignature signature;

	public DatatypeKind computeDatatypeKind() {
		DatatypeKind result;
		{
			List<JavaBaseTypeHint> datatypeKindHints = getHints()
					.filter(hint -> EnumUtils.in(
							hint,
							JavaBaseTypeHint.LIST, JavaBaseTypeHint.OBJECT, JavaBaseTypeHint.SIMPLE, JavaBaseTypeHint.VALUE));
			//
			// If we don't have anything that can hint us type we consider it to be OBJECT.
			JavaBaseTypeHint datatypeKindHint = !datatypeKindHints.isEmpty()
					? datatypeKindHints.single()
					: JavaBaseTypeHint.OBJECT;
			//
			result = switch (datatypeKindHint) {
				case LIST -> DatatypeKind.LIST;
				case OBJECT -> DatatypeKind.OBJECT;
				case SIMPLE -> DatatypeKind.SIMPLE;
				case VALUE -> DatatypeKind.VALUE;
				default -> throw new UnreachableStateException();
			};
		}
		return result;
	}

	@Nullable
	public String computeObjectModelCode() {
		return null;
	}

	// For Javadoc.
	public Boolean getHasCustomObjectModel() {
		return hasCustomObjectModel;
	}

	public List<JavaBaseTypeHint> getHints() {
		return getImplicitHintsOrEmpty().appendAll(getInheritedHintsOrEmpty());
	}

	public List<JavaBaseTypeHint> getImplicitHintsOrEmpty() {
		return implicitHints != null ? implicitHints : List.empty();
	}

	public List<JavaBaseTypeHint> getInheritedHintsOrEmpty() {
		return inheritedHints != null ? inheritedHints : List.empty();
	}

	public boolean hasHint(JavaBaseTypeHint hint) {
		return getHints().contains(hint);
	}

	public Class<?> resolveClass() {
		return ClassUtils.get(signature.getBinaryName());
	}

	public static class Builder {
		// Lomboked
	}
}
