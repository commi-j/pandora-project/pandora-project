package tk.labyrinth.pandora.datatypes.domain.datatype;

public enum DatatypeKind {
	LIST,
	OBJECT,
	SIMPLE,
	VALUE,
}
