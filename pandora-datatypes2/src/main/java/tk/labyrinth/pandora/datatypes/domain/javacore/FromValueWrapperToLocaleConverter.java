package tk.labyrinth.pandora.datatypes.domain.javacore;

import org.springframework.core.convert.converter.Converter;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Locale;

@LazyComponent
public class FromValueWrapperToLocaleConverter implements Converter<ValueWrapper, Locale> {

	@Override
	public Locale convert(ValueWrapper source) {
		String value = source.asSimple().getValue();
		int indexOfUnderscore = value.indexOf('_');
		return indexOfUnderscore != -1
				? new Locale(value.substring(0, indexOfUnderscore), value.substring(indexOfUnderscore + 1))
				: new Locale(value);
	}
}
