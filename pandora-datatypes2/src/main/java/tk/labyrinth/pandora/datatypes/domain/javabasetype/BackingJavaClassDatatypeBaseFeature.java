package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import lombok.Value;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;

@PolymorphicLeaf(rootClass = DatatypeBaseFeature.class, qualifierAttributeValue = "backing-java-class")
@Value(staticConstructor = "of")
public class BackingJavaClassDatatypeBaseFeature implements DatatypeBaseFeature {

	ClassSignature classSignature;
}
