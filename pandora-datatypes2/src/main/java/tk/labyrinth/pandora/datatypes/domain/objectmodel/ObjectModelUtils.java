package tk.labyrinth.pandora.datatypes.domain.objectmodel;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.exception.ExceptionUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;

import javax.annotation.CheckForNull;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public class ObjectModelUtils {

	public static final String DATATYPE_ALIAS_PREFIX = "objectmodel:";

	public static Datatype createDatatype(CodeObjectModelReference objectModelReference) {
		return createDatatype(objectModelReference.getCode());
	}

	public static Datatype createDatatype(ObjectModel objectModel) {
		return createDatatype(objectModel.getCode());
	}

	public static Datatype createDatatype(String objectModelCode) {
		return Datatype.builder()
				.baseReference(createDatatypeBaseReference(objectModelCode))
				.parameters(null)
				.build();
	}

	public static String createDatatypeAlias(CodeObjectModelReference objectModelReference) {
		return createDatatypeAlias(objectModelReference.getCode());
	}

	public static String createDatatypeAlias(String objectModelCode) {
		return "%s%s".formatted(DATATYPE_ALIAS_PREFIX, objectModelCode);
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(CodeObjectModelReference objectModelReference) {
		return createDatatypeBaseReference(objectModelReference.getCode());
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(ObjectModel objectModel) {
		return createDatatypeBaseReference(objectModel.getCode());
	}

	public static AliasDatatypeBaseReference createDatatypeBaseReference(String objectModelCode) {
		return AliasDatatypeBaseReference.of(createDatatypeAlias(objectModelCode));
	}

	@CheckForNull
	public static String findAttributeName(ElementHandle memberElementHandle) {
		String result;
		{
			if (memberElementHandle.isFieldElement()) {
				result = getAttributeName(memberElementHandle.asFieldElement());
			} else if (memberElementHandle.isMethodElement()) {
				result = findAttributeName(memberElementHandle.asMethodElement());
			} else {
				throw new NotImplementedException(ExceptionUtils.render(memberElementHandle));
			}
		}
		return result;
	}

	@CheckForNull
	public static String findAttributeName(Member member) {
		String result;
		{
			if (member instanceof Field field) {
				result = getAttributeName(field);
			} else if (member instanceof Method method) {
				result = findAttributeName(method);
			} else {
				throw new NotImplementedException(ExceptionUtils.render(member));
			}
		}
		return result;
	}

	@CheckForNull
	public static String findAttributeName(Method method) {
		String result;
		{
			String methodName = method.getName();
			if (methodName.startsWith("get") &&
					methodName.length() > 3 &&
					Character.isUpperCase(methodName.charAt(3))) {
				result = Character.toLowerCase(methodName.charAt(3)) + methodName.substring(4);
			} else {
				result = null;
			}
		}
		return result;
	}

	@CheckForNull
	public static String findAttributeName(MethodElementHandle methodElementHandle) {
		String result;
		{
			String methodName = methodElementHandle.getName();
			if (methodName.startsWith("get") &&
					methodName.length() > 3 &&
					Character.isUpperCase(methodName.charAt(3))) {
				result = Character.toLowerCase(methodName.charAt(3)) + methodName.substring(4);
			} else {
				result = null;
			}
		}
		return result;
	}

	@Nullable
	public static String findObjectModelCode(DatatypeBase datatypeBase) {
		return findObjectModelCode(datatypeBase.getAliases());
	}

	@Nullable
	public static String findObjectModelCode(List<String> datatypeAliases) {
		String result;
		{
			List<String> objectModelCodes = datatypeAliases
					.filter(ObjectModelUtils::isObjectModelDatatypeAlias)
					.map(ObjectModelUtils::getObjectModelCode);
			//
			if (objectModelCodes.size() > 1) {
				throw new IllegalArgumentException("Require <=1: %s".formatted(objectModelCodes));
			}
			//
			result = !objectModelCodes.isEmpty() ? objectModelCodes.get() : null;
		}
		return result;
	}

	@Nullable
	public static CodeObjectModelReference findObjectModelReference(DatatypeBase datatypeBase) {
		return findObjectModelReference(datatypeBase.getAliases());
	}

	@Nullable
	public static CodeObjectModelReference findObjectModelReference(List<String> datatypeAliases) {
		CodeObjectModelReference result;
		{
			String objectModelCode = findObjectModelCode(datatypeAliases);
			//
			result = objectModelCode != null ? CodeObjectModelReference.of(objectModelCode) : null;
		}
		return result;
	}

	public static String getAttributeName(ElementHandle memberElementHandle) {
		String result = findAttributeName(memberElementHandle);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: memberElementHandle = %s".formatted(memberElementHandle));
		}
		//
		return result;
	}

	public static String getAttributeName(Field field) {
		return field.getName();
	}

	public static String getAttributeName(FieldElementHandle fieldElementHandle) {
		return fieldElementHandle.getName();
	}

	public static String getAttributeName(Member member) {
		String result = findAttributeName(member);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: member = %s".formatted(member));
		}
		//
		return result;
	}

	public static String getAttributeName(Method method) {
		String result = findAttributeName(method);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: method = %s".formatted(method));
		}
		//
		return result;
	}

	public static String getGetterName(String attributeName) {
		return "get" + attributeName.substring(0, 1).toUpperCase() + attributeName.substring(1);
	}

	public static String getObjectModelCode(Reference<DatatypeBase> datatypeBaseReference) {
		return getObjectModelCode(AliasDatatypeBaseReference.getAlias(datatypeBaseReference));
	}

	public static String getObjectModelCode(String datatypeAlias) {
		if (!isObjectModelDatatypeAlias(datatypeAlias)) {
			throw new IllegalArgumentException("Require ObjectModelAlias: %s".formatted(datatypeAlias));
		}
		//
		return datatypeAlias.substring(DATATYPE_ALIAS_PREFIX.length());
	}

	public static CodeObjectModelReference getObjectModelReference(DatatypeBase datatypeBase) {
		CodeObjectModelReference result = findObjectModelReference(datatypeBase);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: datatypeBase = %s".formatted(datatypeBase));
		}
		//
		return result;
	}

	public static CodeObjectModelReference getObjectModelReference(Reference<DatatypeBase> datatypeBaseReference) {
		return CodeObjectModelReference.of(getObjectModelCode(datatypeBaseReference));
	}

	public static boolean isObjectModelDatatypeAlias(String datatypeAlias) {
		return datatypeAlias.startsWith(DATATYPE_ALIAS_PREFIX);
	}

	public static boolean isObjectModelDatatypeBaseReference(Reference<DatatypeBase> datatypeBaseReference) {
		return AliasDatatypeBaseReference.isAliasDatatypeBaseReference(datatypeBaseReference) &&
				isObjectModelDatatypeAlias(AliasDatatypeBaseReference.getAlias(datatypeBaseReference));
	}
}
