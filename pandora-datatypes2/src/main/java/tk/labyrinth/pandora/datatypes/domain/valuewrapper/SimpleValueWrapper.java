package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value(staticConstructor = "of")
public class SimpleValueWrapper implements ValueWrapper {

	@NonNull
	String value;

	@Override
	public ListValueWrapper asList() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectValueWrapper asObject() {
		throw new UnsupportedOperationException();
	}

	@Override
	public SimpleValueWrapper asSimple() {
		return this;
	}

	@Override
	public boolean isList() {
		return false;
	}

	@Override
	public boolean isObject() {
		return false;
	}

	@Override
	public boolean isSimple() {
		return true;
	}

	@Override
	public String toString() {
		return value;
	}

	@Override
	public String unwrap() {
		return value;
	}

	public static SimpleValueWrapper of(Integer integer) {
		return of(integer.toString());
	}

	public static SimpleValueWrapper of(UUID uuid) {
		return of(uuid.toString());
	}

	public static SimpleValueWrapper ofFalse() {
		return of(Boolean.FALSE.toString());
	}

	public static SimpleValueWrapper ofTrue() {
		return of(Boolean.TRUE.toString());
	}
}
