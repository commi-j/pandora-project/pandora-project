package tk.labyrinth.pandora.datatypes.domain.objectmodel.render;

import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// TODO
@Documented
@PandoraExtensionPoint
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RenderFunction {

	String value();
}
