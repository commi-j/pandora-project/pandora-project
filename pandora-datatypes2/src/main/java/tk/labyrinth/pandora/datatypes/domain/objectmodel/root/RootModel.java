package tk.labyrinth.pandora.datatypes.domain.objectmodel.root;

import org.springframework.core.annotation.AliasFor;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Model
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RootModel {

	/**
	 * Default will use {@link Object#getClass() Class}.{@link Class#getSimpleName() getSimpleName()}.{@link String#toLowerCase() toLowerCase()}.
	 *
	 * @return code
	 */
	@AliasFor(annotation = Model.class)
	String code() default "";

	/**
	 * See {@link JavaBaseType#getHasCustomObjectModel()}
	 *
	 * @return hasCustomModel
	 */
	@AliasFor(annotation = Model.class)
	boolean hasCustomModel() default false;

	/**
	 * Attribute to use as an object key.<br>
	 * If not specified, <b>uid</b> will be used.<br>
	 *
	 * @return emptiable
	 */
	String primaryAttribute() default "";

	@AliasFor(annotation = Model.class)
	String value() default "";
}
