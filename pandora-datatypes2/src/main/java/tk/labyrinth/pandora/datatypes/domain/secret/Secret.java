package tk.labyrinth.pandora.datatypes.domain.secret;

import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

@Value(staticConstructor = "of")
public class Secret {

	String value;

	@Override
	public String toString() {
		return "SECRET";
	}

	@Nullable
	public static String unwrap(@Nullable Secret secret) {
		return secret != null ? secret.value : null;
	}
}
