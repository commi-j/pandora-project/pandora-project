package tk.labyrinth.pandora.datatypes.domain.objectmodel.meta;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@ModelTag(ModelTagPandora.TAG_NAME)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ModelTagPandora {

	String TAG_NAME = "pandora";
}
