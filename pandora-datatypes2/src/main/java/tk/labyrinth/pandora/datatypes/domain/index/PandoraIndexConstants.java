package tk.labyrinth.pandora.datatypes.domain.index;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;

public class PandoraIndexConstants {

	public static final String ATTRIBUTE_NAME = "pandoraIndex";

	public static final GenericObjectAttribute ATTRIBUTE = GenericObjectAttribute.ofTrue(
			ATTRIBUTE_NAME);

	public static final Predicate PREDICATE = Predicates.equalTo(ATTRIBUTE_NAME, true);
}
