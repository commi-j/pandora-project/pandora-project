package tk.labyrinth.pandora.datatypes.domain.index;

import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTag;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@ModelTag(PandoraIndexConstants.ATTRIBUTE_NAME)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ModelTagIndex {
	// empty
}
