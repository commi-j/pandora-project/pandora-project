package tk.labyrinth.pandora.datatypes.domain.datatypebase;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.genericreference.CustomGenericReferenceToPredicateConverter;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

// TODO: We should learn to resolve references against lists automatically.
@LazyComponent
public class CustomAliasDatatypeBaseGenericReferenceToPredicateConverter implements
		CustomGenericReferenceToPredicateConverter {

	@Nullable
	@Override
	public Predicate convertReferenceToPredicate(GenericReference<?> reference) {
		return AliasDatatypeBaseReference.isAliasDatatypeBaseReference(reference)
				? Predicates.contains(
				DatatypeBase.ALIASES_ATTRIBUTE_NAME,
				reference.getAttributeValue(AliasDatatypeBaseReference.ALIAS_ATTRIBUTE_NAME))
				: null;
	}
}
