package tk.labyrinth.pandora.datatypes.domain.referencemodel;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;
import tk.labyrinth.pandora.core.meta.PandoraJavaContext;

import java.lang.reflect.Type;

@PandoraExtensionPoint
@PandoraJavaContext
public interface CustomReferenceModelFactory {

	@Nullable
	ReferenceModel createCustomReferenceModel(Type javaType);
}
