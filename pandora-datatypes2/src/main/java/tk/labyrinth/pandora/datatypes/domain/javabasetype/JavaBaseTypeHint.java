package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;

import java.util.Objects;
import java.util.function.Function;

public enum JavaBaseTypeHint {
	ENUM,
	INDEX,
	/**
	 * Extends LIST datatype.
	 */
	LIST,
	MODEL,
	/**
	 * Extends OBJECT datatype.
	 */
	OBJECT,
	PROXY,
	/**
	 * Extends SIMPLE datatype.
	 */
	REFERENCE,
	/**
	 * Extends SIMPLE datatype.
	 */
	SIMPLE,
	/**
	 * Extends VALUE datatype.
	 */
	VALUE;

	public static List<JavaBaseTypeHint> computeImplicitHints(Class<?> javaClass) {
		boolean hasModelAnnotation = MergedAnnotations.from(javaClass).get(Model.class).isPresent();
//		boolean hasSimpleTypeAnnotation = MergedAnnotations.from(javaClass).get(SimpleType.class).isPresent();
		boolean isEnum = javaClass.isEnum();
		boolean isInterface = javaClass.isInterface();
//		boolean implementsObjectIndex = ObjectIndex.class.isAssignableFrom(javaClass);
		//
		return List
				.of(
						isEnum ? List.of(ENUM, SIMPLE) : null,
//						implementsObjectIndex ? List.of(INDEX) : null,
						hasModelAnnotation ? List.of(MODEL, OBJECT) : null,
						isInterface && hasModelAnnotation ? List.of(PROXY) : null
//						, hasSimpleTypeAnnotation ? List.of(SIMPLE) : null
				)
				.filter(Objects::nonNull)
				.flatMap(Function.identity());
	}

	public static boolean hasHint(@Nullable JavaBaseType javaBaseType, JavaBaseTypeHint hint) {
		return javaBaseType != null && javaBaseType.hasHint(hint);
	}
}
