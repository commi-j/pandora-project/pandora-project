package tk.labyrinth.pandora.datatypes.domain.genericreference;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.pandora.core.meta.PandoraExtensionPoint;

@PandoraExtensionPoint
public interface CustomGenericReferenceToPredicateConverter {

	@Nullable
	Predicate convertReferenceToPredicate(GenericReference<?> reference);
}
