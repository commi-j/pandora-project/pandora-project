package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.io.IOException;

// TODO: Make all registrations single module
@LazyComponent
public class ValueWrapperObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module, objectMapper);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module, ObjectMapper objectMapper) {
		Deserializer deserializer = new Deserializer(objectMapper);
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer(objectMapper));
		return module;
	}

	public static class Deserializer extends StdNodeBasedDeserializer<ValueWrapper> {

		private final ObjectMapper objectMapper;

		public Deserializer(ObjectMapper objectMapper) {
			super(ValueWrapper.class);
			this.objectMapper = objectMapper;
		}

		@Override
		public ValueWrapper convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			ValueWrapper result;
			{
				if (root.isArray()) {
					result = ListValueWrapper.of(objectMapper.readValue(
							root.traverse(),
							objectMapper.getTypeFactory().constructParametricType(List.class, ValueWrapper.class)));
				} else if (root.isObject()) {
					result = ObjectValueWrapper.of(objectMapper.readValue(root.traverse(), GenericObject.class));
				} else if (root.isValueNode()) {
					result = SimpleValueWrapper.of(root.asText());
				} else {
					throw new NotImplementedException(root.toString());
				}
			}
			return result;
		}
	}

	public static class Serializer extends StdSerializer<ValueWrapper> {

		private final ObjectMapper objectMapper;

		public Serializer(ObjectMapper objectMapper) {
			super(ValueWrapper.class);
			this.objectMapper = objectMapper;
		}

		@Override
		public void serialize(ValueWrapper value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			objectMapper.writeValue(gen, value.unwrap());
		}
	}
}
