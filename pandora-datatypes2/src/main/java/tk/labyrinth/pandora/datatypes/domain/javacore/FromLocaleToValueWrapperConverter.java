package tk.labyrinth.pandora.datatypes.domain.javacore;

import org.springframework.core.convert.converter.Converter;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Locale;

@LazyComponent
public class FromLocaleToValueWrapperConverter implements Converter<Locale, ValueWrapper> {

	@Override
	public ValueWrapper convert(Locale source) {
		return SimpleValueWrapper.of(source.toString());
	}
}
