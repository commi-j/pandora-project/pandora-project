package tk.labyrinth.pandora.datatypes.domain.tag;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;

import java.io.IOException;
import java.util.Optional;

// TODO: Make all registrations single module
// FIXME: Representing Tag as string breaks various GenericObject related features.
//  We are not sure if we treat tag as simple or object type.
//@LazyComponent
@Slf4j
public class TagObjectMapperConfigurer implements ObjectMapperConfigurer {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module) {
		Deserializer deserializer = new Deserializer();
		module.addDeserializer(deserializer.handledType(), (JsonDeserializer) deserializer);
		module.addSerializer(new Serializer());
		return module;
	}

	public static class Deserializer extends StdNodeBasedDeserializer<Tag> {

		public Deserializer() {
			super(Tag.class);
		}

		@Override
		public Tag convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			Tag result;
			{
				if (root.isObject()) {
					result = Tag.of(
							Optional.ofNullable(root.get(Tag.NAME_ATTRIBUTE_NAME))
									.map(JsonNode::asText)
									.orElse(null),
							Optional.ofNullable(root.get(Tag.VALUE_ATTRIBUTE_NAME))
									.map(JsonNode::asText)
									.orElse(null));
				} else if (root.isTextual()) {
					String[] split = root.textValue().split(":", 2);
					result = Tag.of(split[0], split.length > 1 ? split[1] : null);
				} else {
					throw new NotImplementedException();
				}
			}
			return result;
		}
	}

	public static class Serializer extends StdSerializer<Tag> {

		public Serializer() {
			super(Tag.class);
		}

		@Override
		public void serialize(Tag value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.toString());
		}
	}
}
