package tk.labyrinth.pandora.datatypes.domain.objectmodel.meta;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// FIXME: Find a better package for this class. Probably better name as well.
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SearchDataAttributes {

	String[] value();
}
