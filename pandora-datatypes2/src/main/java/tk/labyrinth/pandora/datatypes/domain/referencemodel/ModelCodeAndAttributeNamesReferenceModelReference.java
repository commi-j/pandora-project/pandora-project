package tk.labyrinth.pandora.datatypes.domain.referencemodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class ModelCodeAndAttributeNamesReferenceModelReference implements Reference<ReferenceModel> {

	List<String> attributeNames;

	String modelCode;

	public CodeObjectModelReference getTargetModelReference() {
		return CodeObjectModelReference.of(modelCode);
	}

	@Override
	public String toString() {
		return "%s(%s=%s,%s=%s)".formatted(
				ReferenceModel.MODEL_CODE,
				ReferenceModel.MODEL_CODE_ATTRIBUTE_NAME,
				modelCode,
				ReferenceModel.ATTRIBUTE_NAMES_ATTRIBUTE_NAME,
				attributeNames.mkCharSeq("(", ",", ")"));
	}

	public static ModelCodeAndAttributeNamesReferenceModelReference from(ReferenceModel object) {
		return ModelCodeAndAttributeNamesReferenceModelReference.of(
				object.getModelCode(),
				object.getAttributeNames());
	}

	@JsonCreator
	public static ModelCodeAndAttributeNamesReferenceModelReference from(String value) {
		GenericReference<?> reference = GenericReference.from(value);
		if (!Objects.equals(reference.getModelCode(), ReferenceModel.MODEL_CODE)) {
			throw new IllegalArgumentException(value);
		}
		//
		String attributeNamesString = reference.getAttributeValue(ReferenceModel.ATTRIBUTE_NAMES_ATTRIBUTE_NAME);
		//
		return of(
				reference.getAttributeValue(ReferenceModel.MODEL_CODE_ATTRIBUTE_NAME),
				List.of(attributeNamesString.substring(1, attributeNamesString.length() - 1).split(",")));
	}

	public static ModelCodeAndAttributeNamesReferenceModelReference of(String modelCode, List<String> attributeNames) {
		return new ModelCodeAndAttributeNamesReferenceModelReference(attributeNames, modelCode);
	}
}
