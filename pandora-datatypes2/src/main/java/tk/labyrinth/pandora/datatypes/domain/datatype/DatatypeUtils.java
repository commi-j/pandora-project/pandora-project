package tk.labyrinth.pandora.datatypes.domain.datatype;

import io.vavr.collection.List;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

public class DatatypeUtils {

	public static List<Reference<DatatypeBase>> getAllDatatypeBaseReferences(Datatype datatype) {
		return (datatype.getBaseReference() != null
				? List.of(datatype.getBaseReference())
				: List.<Reference<DatatypeBase>>empty())
				.appendAll(datatype.getParameters() != null
						? datatype.getParameters().flatMap(DatatypeUtils::getAllDatatypeBaseReferences)
						: List.empty())
				.filter(Objects::nonNull)
				.distinct();
	}
}
