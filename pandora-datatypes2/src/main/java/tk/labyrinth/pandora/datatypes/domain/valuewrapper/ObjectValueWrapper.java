package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;

@Value(staticConstructor = "of")
public class ObjectValueWrapper implements ValueWrapper {

	@NonNull
	GenericObject value;

	@Override
	public ListValueWrapper asList() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectValueWrapper asObject() {
		return this;
	}

	@Override
	public SimpleValueWrapper asSimple() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isList() {
		return false;
	}

	@Override
	public boolean isObject() {
		return true;
	}

	@Override
	public boolean isSimple() {
		return false;
	}

	@Override
	public String toString() {
		return value.toString();
	}

	@Override
	public GenericObject unwrap() {
		return value;
	}

	public static ObjectValueWrapper empty() {
		return of(GenericObject.empty());
	}

	public static ObjectValueWrapper of(GenericObjectAttribute... attributes) {
		return of(GenericObject.of(attributes));
	}

	public static ObjectValueWrapper ofSingleSimple(String name, String value) {
		return of(GenericObject.ofSingleSimple(name, value));
	}
}
