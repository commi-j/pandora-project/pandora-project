package tk.labyrinth.pandora.datatypes.domain.fault;

import lombok.Value;

@Value(staticConstructor = "of")
public class StructuredFault implements Fault {

	String message;
	//
	// TODO: Map of attributes.
}
