package tk.labyrinth.pandora.datatypes.domain.genericobject;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Traversable;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;

import java.util.Objects;
import java.util.function.UnaryOperator;

@Value(staticConstructor = "of")
public class GenericObject {

	@NonNull
	List<GenericObjectAttribute> attributes;

	private GenericObject(@NonNull List<GenericObjectAttribute> attributes) {
		// Sorting to ensure canonical representation.
		this.attributes = attributes.sortBy(GenericObjectAttribute::getName);
	}

	/**
	 * Returns attributes of <b>this</b> plus attributes of <b>other</b> that are not present in <b>this</b>.
	 *
	 * @param other non-null
	 *
	 * @return non-null
	 */
	public GenericObject enrichFrom(GenericObject other) {
		return of(attributes.appendAll(other.attributes.filter(otherAttribute -> !attributes.exists(attribute ->
				Objects.equals(attribute.getName(), otherAttribute.getName())))));
	}

	@Nullable
	public GenericObjectAttribute findAttribute(String attributeName) {
		return attributes.find(attribute -> Objects.equals(attribute.getName(), attributeName)).getOrNull();
	}

	@Nullable
	public ValueWrapper findAttributeValue(String attributeName) {
		GenericObjectAttribute attribute = findAttribute(attributeName);
		//
		return attribute != null ? attribute.getValue() : null;
	}

	@Nullable
	public Boolean findAttributeValueAsBoolean(String attributeName) {
		ValueWrapper attributeValue = findAttributeValue(attributeName);
		//
		return attributeValue != null ? Boolean.parseBoolean(attributeValue.asSimple().getValue()) : null;
	}

	@Nullable
	public GenericObject findAttributeValueAsObject(String attributeName) {
		ValueWrapper attributeValue = findAttributeValue(attributeName);
		//
		return attributeValue != null ? attributeValue.asObject().getValue() : null;
	}

	@Nullable
	public Object findAttributeValueAsRaw(String attributeName) {
		ValueWrapper attributeValue = findAttributeValue(attributeName);
		//
		return attributeValue != null ? attributeValue.unwrap() : null;
	}

	@Nullable
	public String findAttributeValueAsString(String attributeName) {
		ValueWrapper attributeValue = findAttributeValue(attributeName);
		//
		return attributeValue != null ? attributeValue.asSimple().getValue() : null;
	}

	public GenericObjectAttribute getAttribute(String attributeName) {
		return Objects.requireNonNull(
				findAttribute(attributeName),
				"Require present: attributeName = %s, this = %s".formatted(attributeName, this));
	}

	public ValueWrapper getAttributeValue(String attributeName) {
		return getAttribute(attributeName).getValue();
	}

	public List<ValueWrapper> getAttributeValueAsList(String attributeName) {
		return getAttributeValue(attributeName).asList().getValue();
	}

	public GenericObject getAttributeValueAsObject(String attributeName) {
		return getAttributeValue(attributeName).asObject().unwrap();
	}

	public Object getAttributeValueAsRaw(String attributeName) {
		return getAttributeValue(attributeName).unwrap();
	}

	public String getAttributeValueAsString(String attributeName) {
		return getAttributeValue(attributeName).asSimple().unwrap();
	}

	public boolean hasAttribute(String attributeName) {
		return findAttribute(attributeName) != null;
	}

	/**
	 * @param path valid path consisting of names separated with dots (.).
	 *
	 * @return nullable
	 */
	// TODO: Make it return navigation object with path explained.
	@Nullable
	public ValueWrapper lookup(String path) {
		ValueWrapper result;
		{
			Pair<String, @Nullable String> nameAndTail = StringUtils.splitByFirstOccurrence(path, "\\.");
			//
			ValueWrapper attributeValue = findAttributeValue(nameAndTail.getLeft());
			//
			if (attributeValue != null) {
				String tail = nameAndTail.getRight();
				//
				if (tail != null) {
					if (attributeValue.isObject()) {
						result = attributeValue.asObject().unwrap().lookup(tail);
					} else if (attributeValue.isSimple()) {
						// References are structured simple values and can be looked up.
						//
						String value = attributeValue.asSimple().unwrap();
						//
						if (GenericReference.isReference(value)) {
							String foundValue = GenericReference.from(value).lookup(tail);
							//
							result = foundValue != null ? SimpleValueWrapper.of(foundValue) : null;
						} else {
							throw new IllegalArgumentException();
						}
					} else {
						throw new IllegalArgumentException();
					}
				} else {
					result = attributeValue;
				}
			} else {
				result = null;
			}
		}
		return result;
	}

	public GenericObject map(UnaryOperator<GenericObjectAttribute> mapOperator) {
		return of(attributes.map(mapOperator));
	}

	/**
	 * @param attributeNames non-empty
	 *
	 * @return this
	 */
	public GenericObject requireNoAttributes(List<String> attributeNames) {
		if (attributes.exists(attribute -> attributeNames.contains(attribute.getName()))) {
			throw new IllegalArgumentException("Require no attributes: attributeNames = %s, this = %s"
					.formatted(attributeNames, this));
		}
		return this;
	}

	@Override
	public String toString() {
		return "GenericObject(%s)".formatted(attributes.mkString(","));
	}

	public Map<String, Object> unwrap() {
		return attributes.toLinkedMap(GenericObjectAttribute::getName, attribute -> attribute.getValue().unwrap());
	}

	/**
	 * Throws an exception if specified attribute name already present.
	 *
	 * @param attributeToAdd non-null
	 *
	 * @return non-null
	 */
	public GenericObject withAddedAttribute(@NonNull GenericObjectAttribute attributeToAdd) {
		return withAddedAttributes(List.of(attributeToAdd));
	}

	/**
	 * Throws an exception if any of specified attribute names already present.
	 *
	 * @param attributesToAdd non-empty
	 *
	 * @return non-null
	 */
	public GenericObject withAddedAttributes(@NonNull GenericObjectAttribute... attributesToAdd) {
		return withAddedAttributes(List.of(attributesToAdd));
	}

	/**
	 * Throws an exception if any of specified attribute names already present.
	 *
	 * @param attributesToAdd non-empty
	 *
	 * @return non-null
	 */
	public GenericObject withAddedAttributes(@NonNull List<GenericObjectAttribute> attributesToAdd) {
		if (attributes.exists(attribute -> attributesToAdd.exists(attributeToAdd ->
				Objects.equals(attributeToAdd.getName(), attribute.getName())))) {
			throw new IllegalArgumentException("Already exists: attributesToAdd = %s, this = %s"
					.formatted(attributesToAdd, this));
		}
		return of(attributes.appendAll(attributesToAdd));
	}

	/**
	 * Throws an exception if specified attribute name already present.
	 *
	 * @param name  non-null
	 * @param value non-null
	 *
	 * @return non-null
	 */
	public GenericObject withAddedObjectAttribute(@NonNull String name, @NonNull GenericObject value) {
		return withAddedAttribute(GenericObjectAttribute.ofObject(name, value));
	}

	public GenericObject withAttribute(GenericObjectAttribute attribute) {
		return of(attributes.map(attributeElement -> Objects.equals(attributeElement.getName(), attribute.getName())
				? attribute
				: attributeElement));
	}

	public GenericObject withAttribute(String name, @Nullable ValueWrapper value) {
		GenericObject result;
		{
			int index = attributes.indexWhere(attribute -> Objects.equals(attribute.getName(), name));
			//
			if (value != null) {
				GenericObjectAttribute attribute = GenericObjectAttribute.of(name, value);
				//
				result = GenericObject.of(index != -1 ? attributes.update(index, attribute) : attributes.append(attribute));
			} else {
				result = index != -1 ? GenericObject.of(attributes.removeAt(index)) : this;
			}
		}
		return result;
	}

	/**
	 * If null is passed as value of pair, attribute with this name will be absent in result object.
	 *
	 * @param attributes non-null list with non-null pairs with non-null keys and nullable values.
	 *
	 * @return non-null
	 */
	public GenericObject withAttributes(List<Pair<String, @Nullable ValueWrapper>> attributes) {
		return withAttributes(attributes.toMap(Pair::getKey, Pair::getValue));
	}

	/**
	 * If null is passed as value of entry, attribute with this name will be absent in result object.
	 *
	 * @param attributes non-null map with non-null keys and nullable values.
	 *
	 * @return non-null
	 */
	public GenericObject withAttributes(Map<String, @Nullable ValueWrapper> attributes) {
		return of(attributes
				.merge(this.attributes.toMap(GenericObjectAttribute::getName, GenericObjectAttribute::getValue))
				.filter(tuple -> tuple._2() != null)
				.map(tuple -> GenericObjectAttribute.of(tuple._1(), tuple._2()))
				.toList());
	}

	/**
	 * If null is passed as value of pair, attribute with this name will be absent in result object.
	 *
	 * @param attributes non-null vararg with non-null pairs with non-null keys and nullable values.
	 *
	 * @return non-null
	 */
	public GenericObject withAttributes(Pair<String, @Nullable ValueWrapper>... attributes) {
		return withAttributes(List.of(attributes));
	}

	/**
	 * Throws an exception if any of specified attribute names already present and value is different.
	 *
	 * @param attributesToMerge non-null
	 *
	 * @return non-null
	 */
	public GenericObject withMergedAttributes(@NonNull List<GenericObjectAttribute> attributesToMerge) {
		if (attributes.exists(attribute -> attributesToMerge.exists(attributeToMerge ->
				Objects.equals(attributeToMerge.getName(), attribute.getName()) &&
						!Objects.equals(attributeToMerge.getValue(), attribute.getValue())))) {
			throw new IllegalArgumentException("Already exists with different values: attributesToMerge = %s, this = %s"
					.formatted(attributesToMerge, this));
		}
		return of(attributes
				.filter(attribute -> !attributesToMerge.exists(attributeToMerge ->
						Objects.equals(attributeToMerge.getName(), attribute.getName())))
				.appendAll(attributesToMerge));
	}

	public GenericObject withNonNullAttribute(String name, ValueWrapper value) {
		Objects.requireNonNull(name, "name");
		Objects.requireNonNull(value, "value");
		//
		return withNullableAttribute(name, value);
	}

	public GenericObject withNonNullSimpleAttribute(String attributeName, String attributeValue) {
		Objects.requireNonNull(attributeName, "attributeName");
		Objects.requireNonNull(attributeValue, "attributeValue");
		//
		return withNonNullAttribute(attributeName, SimpleValueWrapper.of(attributeValue));
	}

	public GenericObject withNullableAttribute(String attributeName, @Nullable ValueWrapper attributeValue) {
		Objects.requireNonNull(attributeName, "attributeName");
		//
		List<GenericObjectAttribute> nextAttributes;
		if (attributeValue != null) {
			GenericObjectAttribute nextAttribute = GenericObjectAttribute.of(attributeName, attributeValue);
			GenericObjectAttribute found = attributes
					.find(attribute -> Objects.equals(attribute.getName(), attributeName)).getOrNull();
			nextAttributes = (found != null
					? attributes.replace(found, nextAttribute)
					: attributes.append(nextAttribute));
		} else {
			nextAttributes = attributes.removeFirst(attribute -> Objects.equals(attribute.getName(), attributeName));
		}
		return of(nextAttributes);
	}

	public GenericObject withRetainedAttributes(Traversable<String> attributeNames) {
		return of(attributes.filter(attribute -> attributeNames.contains(attribute.getName())));
	}

	public GenericObject withTrueAttribute(String attributeName) {
		Objects.requireNonNull(attributeName, "attributeName");
		//
		return withNonNullSimpleAttribute(attributeName, Boolean.TRUE.toString());
	}

	public GenericObject withoutAttribute(String attributeName) {
		return withNullableAttribute(attributeName, null);
	}

	public GenericObject withoutAttributes(List<String> attributeNames) {
		return of(attributes.filter(attribute -> !attributeNames.contains(attribute.getName())));
	}

	public GenericObject withoutAttributes(String... attributeNames) {
		return withoutAttributes(List.of(attributeNames));
	}

	public static GenericObject empty() {
		return new GenericObject(List.of());
	}

	public static GenericObject of(GenericObjectAttribute... attributes) {
		return of(List.of(attributes));
	}

	public static GenericObject ofSingleSimple(String name, String value) {
		return of(GenericObjectAttribute.ofSimple(name, value));
	}
}
