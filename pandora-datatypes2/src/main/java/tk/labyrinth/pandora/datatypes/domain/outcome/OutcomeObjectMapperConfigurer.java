package tk.labyrinth.pandora.datatypes.domain.outcome;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdNodeBasedDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.NonNull;
import tk.labyrinth.pandora.datatypes.domain.fault.Fault;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.io.IOException;

@LazyComponent
public class OutcomeObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module, objectMapper);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module, ObjectMapper objectMapper) {
		JsonDeserializer deserializer = new Deserializer(objectMapper);
		//
		module.addDeserializer(deserializer.handledType(), deserializer);
		module.addSerializer(new Serializer(objectMapper));
		//
		return module;
	}

	@SuppressWarnings("rawtypes")
	public static class Deserializer extends StdNodeBasedDeserializer<Outcome> implements ContextualDeserializer {

		private final ObjectMapper objectMapper;

		@NonNull
		private final JavaType targetType;

		public Deserializer(Deserializer deserializer, JavaType targetType) {
			super(deserializer);
			this.objectMapper = deserializer.objectMapper;
			this.targetType = targetType;
		}

		public Deserializer(ObjectMapper objectMapper) {
			super(Outcome.class);
			this.objectMapper = objectMapper;
			this.targetType = objectMapper.constructType(Void.class);
		}

		@Override
		public Outcome convert(JsonNode root, DeserializationContext ctxt) throws IOException {
			Outcome result;
			{
				if (root.has("result")) {
					result = Outcome.ofResult(objectMapper.convertValue(root.get("result"), targetType.containedType(0)));
				} else if (root.has("success")) {
					result = Outcome.ofResult(null);
				} else {
					result = Outcome.ofFault(objectMapper.convertValue(root.get("fault"), Fault.class));
				}
			}
			return result;
		}

		@Override
		public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) {
			return new Deserializer(this, ctxt.getContextualType());
		}
	}

	@SuppressWarnings("rawtypes")
	public static class Serializer extends StdSerializer<Outcome> {

		private final ObjectMapper objectMapper;

		public Serializer(ObjectMapper objectMapper) {
			super(Outcome.class);
			this.objectMapper = objectMapper;
		}

		@Override
		public void serialize(Outcome value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeStartObject();
			//
			if (value.isSuccess()) {
				Object result = value.getResult();
				//
				if (result != null) {
					gen.writeFieldName("result");
					objectMapper.writeValue(gen, result);
				} else {
					gen.writeBooleanField("success", true);
				}
			} else {
				gen.writeFieldName("fault");
				objectMapper.writeValue(gen, value.getFault());
			}
			//
			gen.writeEndObject();
		}
	}
}
