package tk.labyrinth.pandora.datatypes.domain.origin;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

public class PandoraOriginUtils {

	public static GenericObjectAttribute createAttribute(Reference<?> originReference) {
		return GenericObjectAttribute.ofSimple(PandoraOriginConstants.ATTRIBUTE_NAME, originReference.toString());
	}

	public static Predicate createPredicate(Reference<?> originReference) {
		return Predicates.equalTo(PandoraOriginConstants.ATTRIBUTE_NAME, originReference);
	}
}
