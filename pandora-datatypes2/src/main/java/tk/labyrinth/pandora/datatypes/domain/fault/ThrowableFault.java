package tk.labyrinth.pandora.datatypes.domain.fault;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.ClassSignature;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ThrowableFault implements Fault {

	@Nullable
	ThrowableFault cause;

	ClassSignature classSignature;

	@Nullable
	String message;

	List<String> stackTrace;

	public static ThrowableFault from(Throwable throwable) {
		return ThrowableFault.builder()
				.cause(throwable.getCause() != null ? ThrowableFault.from(throwable.getCause()) : null)
				.classSignature(ClassSignature.of(throwable.getClass()))
				.message(throwable.getMessage())
				.stackTrace(List.of(throwable.getStackTrace()).map(StackTraceElement::toString))
				.build();
	}
}
