package tk.labyrinth.pandora.datatypes.domain.reference;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.Set;

@LazyComponent
@RequiredArgsConstructor
@Slf4j
public class FromStringToReferenceConverter implements ConditionalGenericConverter {

	private final ObjectMapper objectMapper;

	@PostConstruct
	private void postConstruct() {
		logger.debug("Initialized");
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		return objectMapper.convertValue(source, targetType.getType());
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return null;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		return sourceType.getType() == String.class && Reference.class.isAssignableFrom(targetType.getType());
	}
}
