package tk.labyrinth.pandora.datatypes.domain.genericobject;

import io.vavr.collection.List;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ObjectValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;

import java.util.UUID;

@RenderPattern("$name = $value")
@Value(staticConstructor = "of")
@With
public class GenericObjectAttribute {

	String name;

	ValueWrapper value;

	private GenericObjectAttribute(String name, ValueWrapper value) {
		if (name == null || value == null) {
			throw new IllegalArgumentException("Require non-null: name = %s, value = %s".formatted(name, value));
		}
		//
		this.name = name;
		this.value = value;
	}

	@Override
	public String toString() {
		return "%s=%s".formatted(name, value);
	}

	public static GenericObjectAttribute ofInteger(String name, Integer value) {
		return ofSimple(name, value.toString());
	}

	public static GenericObjectAttribute ofObject(String name, GenericObject value) {
		return of(name, ObjectValueWrapper.of(value));
	}

	public static GenericObjectAttribute ofObjectList(
			String name,
			List<GenericObject> value) {
		return of(name, ListValueWrapper.of(value.map(ObjectValueWrapper::of)));
	}

	public static GenericObjectAttribute ofSimple(String name, String value) {
		return of(name, SimpleValueWrapper.of(value));
	}

	public static GenericObjectAttribute ofSimpleList(String name, List<String> value) {
		return of(name, ListValueWrapper.of(value.map(SimpleValueWrapper::of)));
	}

	public static GenericObjectAttribute ofTrue(String name) {
		return ofSimple(name, "true");
	}

	public static GenericObjectAttribute ofUuid(String name, UUID value) {
		return ofSimple(name, value.toString());
	}
}
