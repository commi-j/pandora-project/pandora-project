package tk.labyrinth.pandora.datatypes.domain.objectmodel.meta;

import org.springframework.core.annotation.AliasFor;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @see Tag
 */
@Documented
@Repeatable(ModelTag.Repeater.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ModelTag {

	@AliasFor("value")
	String tagName() default "";

	String tagValue() default "";

	@AliasFor("tagName")
	String value() default "";

	@Documented
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	@interface Repeater {

		ModelTag[] value();
	}
}
