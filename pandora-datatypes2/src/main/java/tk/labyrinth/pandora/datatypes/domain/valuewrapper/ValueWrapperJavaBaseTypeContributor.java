package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.registry.DefaultJavaBaseTypeContributor;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.registry.JavaBaseTypeContributor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

@Bean
public class ValueWrapperJavaBaseTypeContributor implements JavaBaseTypeContributor {

	@Override
	public List<Pair<JavaBaseType, @Nullable ObjectModel>> contributeJavaBaseTypes() {
		return List.of(
				Pair.of(DefaultJavaBaseTypeContributor.createList(ListValueWrapper.class), null),
				createObjectValueWrapperPair(),
				Pair.of(DefaultJavaBaseTypeContributor.createSimple(SimpleValueWrapper.class), null),
				Pair.of(DefaultJavaBaseTypeContributor.createValue(ValueWrapper.class), null));
	}

	public static Pair<JavaBaseType, ObjectModel> createObjectValueWrapperPair() {
		JavaBaseType javaBaseType = DefaultJavaBaseTypeContributor.createObject(ObjectValueWrapper.class)
				.withHasCustomObjectModel(true);
		//
		return Pair.of(
				javaBaseType,
				ObjectModel.builder()
						.attributes(List.empty())
						.code(JavaBaseTypeUtils.createJavaBaseTypeObjectModelCode(ObjectValueWrapper.class))
						.name(ObjectValueWrapper.class.getSimpleName())
						.build());
	}
}
