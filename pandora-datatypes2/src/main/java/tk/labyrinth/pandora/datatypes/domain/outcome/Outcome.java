package tk.labyrinth.pandora.datatypes.domain.outcome;

import io.vavr.control.Either;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.PolyNull;
import tk.labyrinth.pandora.datatypes.domain.fault.Fault;
import tk.labyrinth.pandora.datatypes.domain.fault.StructuredFault;
import tk.labyrinth.pandora.datatypes.domain.fault.ThrowableFault;

@Value(staticConstructor = "of")
public class Outcome<@PolyNull R> {

	@Getter(AccessLevel.NONE)
	@NonNull
	Either<Fault, R> value;

	public Fault getFault() {
		return value.getLeft();
	}

	public R getResult() {
		return value.get();
	}

	public boolean isFailure() {
		return value.isLeft();
	}

	public boolean isSuccess() {
		return value.isRight();
	}

	public static <@PolyNull R> Outcome<R> ofFault(@NonNull Fault fault) {
		return of(Either.left(fault));
	}

	public static <@PolyNull R> Outcome<R> ofFaultMessage(@NonNull String faultMessage) {
		return ofFault(StructuredFault.of(faultMessage));
	}

	public static <@PolyNull R> Outcome<R> ofResult(R result) {
		return of(Either.right(result));
	}

	public static <@PolyNull R> Outcome<R> ofThrowable(@NonNull Throwable fault) {
		return ofFault(ThrowableFault.from(fault));
	}
}
