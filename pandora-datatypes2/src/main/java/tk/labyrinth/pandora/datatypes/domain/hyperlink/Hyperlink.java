package tk.labyrinth.pandora.datatypes.domain.hyperlink;

import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Comparator;

// FIXME: This is not a simple type, need to relocate it.
@Value(staticConstructor = "of")
@With
public class Hyperlink {

	public static final Comparator<Hyperlink> COMPARATOR = Comparator.comparing(
			Hyperlink::getText,
			Comparator.nullsLast(Comparator.naturalOrder()));

	@Nullable
	String destination;

	@Nullable
	String text;
}
