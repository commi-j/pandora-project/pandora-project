package tk.labyrinth.pandora.datatypes.domain.datatyperegistration;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBaseFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@ModelTagPandora
@RenderPattern("$name ($alias)")
@RootModel(code = DatatypeRegistration.MODEL_CODE, primaryAttribute = DatatypeRegistration.ALIAS_ATTRIBUTE_NAME)
@Value
@With
public class DatatypeRegistration {

	public static final String ALIAS_ATTRIBUTE_NAME = "alias";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	public static final String MODEL_CODE = "datatyperegistration";

	public static final String SIGNATURE_ATTRIBUTE_NAME = "signature";

	// TODO: Mark as primary key.
	String alias;

	List<Object> exampleValues;

	List<DatatypeBaseFeature> features;

	String name;

	// TODO: Make objects with names and boundaries.
	List<String> parameters;

	@Nullable
	AliasDatatypeBaseReference parentReference;

	String signature;
}
