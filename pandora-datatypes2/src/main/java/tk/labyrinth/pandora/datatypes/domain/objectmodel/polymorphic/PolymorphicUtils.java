package tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic;

import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;

public class PolymorphicUtils {

	public static boolean isPolymorphicRoot(Class<?> javaClass) {
		return javaClass.isAnnotationPresent(PolymorphicRoot.class);
	}

	public static GenericObjectAttribute resolveQualifierAttribute(Class<?> javaClass) {
		return resolveQualifierAttribute(MergedAnnotations
				.from(javaClass)
				.get(PolymorphicLeaf.class)
				.synthesize());
	}

	public static GenericObjectAttribute resolveQualifierAttribute(PolymorphicLeaf polymorphicLeafAnnotation) {
		if (!polymorphicLeafAnnotation.rootClass().isAnnotationPresent(PolymorphicRoot.class)) {
			throw new IllegalStateException(
					"Require @PolymorphicLeaf#baseClass to be annotated with @PolymorphicRoot: %s"
							.formatted(polymorphicLeafAnnotation));
		}
		//
		return GenericObjectAttribute.ofSimple(
				resolveQualifierAttributeName(polymorphicLeafAnnotation),
				polymorphicLeafAnnotation.qualifierAttributeValue());
	}

	public static String resolveQualifierAttributeName(PolymorphicLeaf polymorphicLeafAnnotation) {
		String result;
		{
			if (!polymorphicLeafAnnotation.qualifierAttributeName().isEmpty()) {
				result = polymorphicLeafAnnotation.qualifierAttributeName();
			} else {
				String LeafAttributeName = polymorphicLeafAnnotation.rootClass()
						.getAnnotation(PolymorphicRoot.class)
						.qualifierAttributeName();
				//
				if (LeafAttributeName.isEmpty()) {
					throw new IllegalStateException(
							"Require LeafAttributeName to be declared on @PolymorphicLeaf or @PolymorphicRoot level");
				}
				//
				result = LeafAttributeName;
			}
		}
		return result;
	}
}
