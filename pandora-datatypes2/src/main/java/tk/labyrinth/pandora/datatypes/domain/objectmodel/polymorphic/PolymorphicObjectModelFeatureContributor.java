package tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic;

import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeature;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeaturesContributor;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class PolymorphicObjectModelFeatureContributor implements ObjectModelFeaturesContributor {

	@Override
	public List<ObjectModelFeature> contributeFeatures(Class<?> javaClass) {
		List<ObjectModelFeature> result;
		{
			MergedAnnotation<PolymorphicLeaf> polymorphicLeafMergedAnnotation = MergedAnnotations
					.from(javaClass)
					.get(PolymorphicLeaf.class);
			//
			if (polymorphicLeafMergedAnnotation.isPresent()) {
				PolymorphicLeaf polymorphicLeafAnnotation = polymorphicLeafMergedAnnotation.synthesize();
				//
				result = List.of(PolymorphicObjectModelFeature.builder()
						.qualifierAttribute(PolymorphicUtils.resolveQualifierAttribute(polymorphicLeafAnnotation))
						.rootReference(JavaBaseTypeUtils.createDatatypeBaseReference(polymorphicLeafAnnotation.rootClass()))
						.build());
			} else {
				result = List.empty();
			}
		}
		return result;
	}
}
