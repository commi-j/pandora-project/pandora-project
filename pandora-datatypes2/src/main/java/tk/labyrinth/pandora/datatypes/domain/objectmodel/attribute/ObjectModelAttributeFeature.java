package tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute;

import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicRoot;

@PolymorphicRoot(qualifierAttributeName = "name")
public interface ObjectModelAttributeFeature {
	// empty
}
