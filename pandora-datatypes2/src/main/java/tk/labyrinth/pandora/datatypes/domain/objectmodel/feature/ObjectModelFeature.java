package tk.labyrinth.pandora.datatypes.domain.objectmodel.feature;

import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicRoot;

@PolymorphicRoot(qualifierAttributeName = "name")
public interface ObjectModelFeature {
	// empty
}
