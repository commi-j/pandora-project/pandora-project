package tk.labyrinth.pandora.datatypes.domain.datatypebase;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class AliasDatatypeBaseReference implements Reference<DatatypeBase> {

	public static final String ALIAS_ATTRIBUTE_NAME = "alias";

	String alias;

	public GenericReference<DatatypeBase> toGenericReference() {
		return GenericReference.of(
				DatatypeBase.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(ALIAS_ATTRIBUTE_NAME, alias)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				DatatypeBase.MODEL_CODE,
				ALIAS_ATTRIBUTE_NAME,
				alias);
	}

	public static AliasDatatypeBaseReference from(GenericReference<?> reference) {
		if (!isAliasDatatypeBaseReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(ALIAS_ATTRIBUTE_NAME));
	}

	public static AliasDatatypeBaseReference from(Reference<?> reference) {
		return reference instanceof AliasDatatypeBaseReference
				? (AliasDatatypeBaseReference) reference
				: from(GenericReference.from(reference));
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static AliasDatatypeBaseReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static String getAlias(Reference<DatatypeBase> aliasDatatypeBaseReference) {
		return from(aliasDatatypeBaseReference).alias;
	}

	public static String getAlias(GenericReference<DatatypeBase> aliasDatatypeBaseReference) {
		return aliasDatatypeBaseReference.getAttributeValue(ALIAS_ATTRIBUTE_NAME);
	}

	public static boolean isAliasDatatypeBaseReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), DatatypeBase.MODEL_CODE) &&
				reference.hasAttribute(ALIAS_ATTRIBUTE_NAME);
	}

	public static boolean isAliasDatatypeBaseReference(Reference<?> reference) {
		return reference instanceof AliasDatatypeBaseReference ||
				isAliasDatatypeBaseReference(GenericReference.from(reference));
	}
}
