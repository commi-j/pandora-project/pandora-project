package tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;

@Builder(toBuilder = true)
@RenderPattern("$name : $datatype")
@Value
@With
public class ObjectModelAttribute {

	Datatype datatype;

	List<ObjectModelAttributeFeature> features;

	String name;

	public <F extends ObjectModelAttributeFeature> F findFeature(Class<F> featureClass) {
		// TODO: Fail if multiple.
		return getFeaturesOrEmpty()
				.filter(featureClass::isInstance)
				.map(featureClass::cast)
				.getOrNull();
	}

	public List<ObjectModelAttributeFeature> getFeaturesOrEmpty() {
		return features != null ? features : List.empty();
	}

	public <F extends ObjectModelAttributeFeature> boolean hasFeature(Class<F> featureClass) {
		return findFeature(featureClass) != null;
	}

	public static ObjectModelAttribute ofDatatypeAndName(Datatype datatype, String name) {
		return new ObjectModelAttribute(datatype, null, name);
	}

	public static ObjectModelAttribute ofNameAndDatatype(String name, Datatype datatype) {
		return new ObjectModelAttribute(datatype, null, name);
	}

	public static ObjectModelAttribute ofNameAndDatatypeAlias(String name, String datatypeAlias) {
		return ofNameAndDatatype(
				name,
				Datatype.builder()
						.baseReference(AliasDatatypeBaseReference.of(datatypeAlias))
						.parameters(null)
						.build());
	}
}
