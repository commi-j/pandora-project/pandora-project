package tk.labyrinth.pandora.datatypes.domain.javabasetype.registry;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.jaap.model.signature.MethodFullSignature;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.hyperlink.Hyperlink;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.domain.secret.Secret;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;
import tk.labyrinth.pandora.pattern.PandoraPattern;

import java.lang.reflect.TypeVariable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.regex.Pattern;

@Bean
public class DefaultJavaBaseTypeContributor implements JavaBaseTypeContributor {

	@Override
	public List<Pair<JavaBaseType, @Nullable ObjectModel>> contributeJavaBaseTypes() {
		return createObjects().map(javaBaseType -> Pair.of(javaBaseType, null));
	}

	private static JavaBaseType.Builder createBase(Class<?> javaClass) {
		return JavaBaseType.builder()
				.inheritedHints(List.empty())
				.name(javaClass.getSimpleName())
				.parameterNames(javaClass.getTypeParameters().length > 0
						? List.of(javaClass.getTypeParameters()).map(TypeVariable::getName)
						: null)
				.signature(TypeSignature.of(javaClass));
	}

	private static List<JavaBaseType> createObjects() {
		return List.<JavaBaseType>empty()
				// Primitives & Wrappers
				//
				.appendAll(Stream
						.of(
								boolean.class,
								Boolean.class,
								byte.class,
								Byte.class,
								char.class,
								Character.class,
								double.class,
								Double.class,
								float.class,
								Float.class,
								int.class,
								Integer.class,
								long.class,
								Long.class,
								short.class,
								Short.class)
						.map(DefaultJavaBaseTypeContributor::createSimple))
				//
				// Java Core
				//
				.appendAll(Stream
						.of(
								BigDecimal.class,
								BigInteger.class,
								Class.class,
								Path.class,
								Pattern.class,
								String.class,
								URL.class,
								UUID.class)
						.map(DefaultJavaBaseTypeContributor::createSimple))
				//
				// Java Time
				//
				.appendAll(Stream
						.of(
								Duration.class,
								Instant.class,
								LocalDate.class,
								LocalDateTime.class,
								LocalTime.class,
								OffsetDateTime.class,
								Period.class,
								ZonedDateTime.class)
						.map(DefaultJavaBaseTypeContributor::createSimple))
				//
				// Pandora & Dependencies
				//
				.appendAll(Stream
						.of(
								ClassSignature.class,
								MethodFullSignature.class,
								PandoraPattern.class,
								Secret.class,
								TypeSignature.class)
						.map(DefaultJavaBaseTypeContributor::createSimple))
				//
				// Objects
				//
				.appendAll(Stream
						.of(
								GenericObject.class,
								Hyperlink.class)
						.map(DefaultJavaBaseTypeContributor::createObject))
				//
				// References
				//
				.appendAll(Stream
						.of(
								Reference.class)
						.map(DefaultJavaBaseTypeContributor::createReference))
				//
				// Lists
				//
				.appendAll(Stream
						.of(
								java.util.Set.class, // For sets to be properly converted into ValueWrappers.
								java.util.List.class,
								List.class,
								io.vavr.collection.Set.class) // For sets to be properly converted into ValueWrappers.
						.map(DefaultJavaBaseTypeContributor::createList));
	}

	public static JavaBaseType createList(Class<?> listJavaClass) {
		return createBase(listJavaClass)
				.implicitHints(List.of(JavaBaseTypeHint.LIST))
				.build();
	}

	public static JavaBaseType createObject(Class<?> objectJavaClass) {
		return createBase(objectJavaClass)
				.implicitHints(List.of(JavaBaseTypeHint.OBJECT))
				.build();
	}

	public static JavaBaseType createReference(Class<?> referenceJavaClass) {
		return createBase(referenceJavaClass)
				.implicitHints(List.of(
						JavaBaseTypeHint.REFERENCE,
						JavaBaseTypeHint.SIMPLE)) // FIXME: REFERENCE is SIMPLE, no need to duplicate it here (but need to check it works well).
				.build();
	}

	public static JavaBaseType createSimple(Class<?> simpleJavaClass) {
		return createBase(simpleJavaClass)
				.implicitHints(List.of(JavaBaseTypeHint.SIMPLE))
				.build();
	}

	public static JavaBaseType createValue(Class<?> valueJavaClass) {
		return createBase(valueJavaClass)
				.implicitHints(List.of(JavaBaseTypeHint.VALUE))
				.build();
	}
}
