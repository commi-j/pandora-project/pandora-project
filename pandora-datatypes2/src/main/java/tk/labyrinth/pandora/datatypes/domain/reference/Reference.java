package tk.labyrinth.pandora.datatypes.domain.reference;

import tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeAware;

/**
 * @param <T> Target
 */
public interface Reference<T> extends TypeAware<T> {
	// empty
}
