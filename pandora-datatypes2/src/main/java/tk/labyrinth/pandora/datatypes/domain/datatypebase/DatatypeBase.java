package tk.labyrinth.pandora.datatypes.domain.datatypebase;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.index.ModelTagIndex;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@ModelTagIndex
@ModelTagPandora
@RenderAttribute("name")
@RootModel(code = DatatypeBase.MODEL_CODE, primaryAttribute = DatatypeBase.SIGNATURE_ATTRIBUTE_NAME, hasCustomModel = true)
@Value
@With
// TODO: Add scope attribute for type parameters (e.g. Pair, Map etc.)
public class DatatypeBase {

	public static final String ALIASES_ATTRIBUTE_NAME = "aliases";

	public static final String MODEL_CODE = "datatypebase";

	public static final String SIGNATURE_ATTRIBUTE_NAME = "signature";

	List<String> aliases;

	List<DatatypeBaseFeature> features;

	String name;

	List<String> parameters;

	AliasDatatypeBaseReference parentReference;

	String signature;

	public <F extends DatatypeBaseFeature> F findFeature(Class<F> featureClass) {
		// TODO: Fail if multiple.
		return getFeaturesOrEmpty()
				.filter(featureClass::isInstance)
				.map(featureClass::cast)
				.getOrNull();
	}

	public List<DatatypeBaseFeature> getFeaturesOrEmpty() {
		return features != null ? features : List.empty();
	}

	public int getParameterIndex(String parameterName) {
		return parameters.indexOf(parameterName);
	}

	public boolean hasFeature(Class<? extends DatatypeBaseFeature> featureJavaClass) {
		return getFeaturesOrEmpty().exists(featureJavaClass::isInstance);
	}
}
