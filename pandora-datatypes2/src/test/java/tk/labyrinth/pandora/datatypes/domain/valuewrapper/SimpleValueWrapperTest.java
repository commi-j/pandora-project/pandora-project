package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SimpleValueWrapperTest {

	@Test
	void testOfFalse() {
		Assertions
				.assertThat(SimpleValueWrapper.ofFalse())
				.isEqualTo(SimpleValueWrapper.of("false"));
		Assertions
				.assertThat(SimpleValueWrapper.ofTrue())
				.isEqualTo(SimpleValueWrapper.of("true"));
	}
}
