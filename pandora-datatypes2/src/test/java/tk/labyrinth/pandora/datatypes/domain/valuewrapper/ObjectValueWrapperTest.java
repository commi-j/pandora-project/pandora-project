package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

class ObjectValueWrapperTest {

	@Test
	void testOfSingleSimple() {
		Assertions
				.assertThat(ObjectValueWrapper.ofSingleSimple("single", "SIMPLE"))
				.isEqualTo(ObjectValueWrapper.of(GenericObject.ofSingleSimple("single", "SIMPLE")));
	}
}
