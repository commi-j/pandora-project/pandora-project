package tk.labyrinth.pandora.datatypes.domain.javabasetype;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;

class JavaBaseTypeUtilsTest {

	@Test
	void testIsJavaBaseTypeAliasDatatypeBaseReference() {
		Assertions
				.assertThat(JavaBaseTypeUtils.isJavaBaseTypeAliasDatatypeBaseReference(
						GenericReference.from("datatypebase(alias=javabasetype:io.vavr.collection:List)")))
				.isEqualTo(true);
	}
}
