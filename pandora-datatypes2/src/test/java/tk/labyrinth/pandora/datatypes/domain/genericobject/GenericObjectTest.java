package tk.labyrinth.pandora.datatypes.domain.genericobject;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ListValueWrapper;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.SimpleValueWrapper;

class GenericObjectTest {

	@Test
	void testLookup() {
		Assertions
				.assertThat(GenericObject.ofSingleSimple("foo", "bar(one=two)").lookup("foo.one"))
				.isEqualTo(SimpleValueWrapper.of("two"));
	}

	@Test
	void testWithAttribute() {
		Assertions
				.assertThat(GenericObject.empty().withAttribute("foo", SimpleValueWrapper.of("bar")))
				.isEqualTo(GenericObject.ofSingleSimple("foo", "bar"));
	}

	@Test
	void testWithAttributeWithGenericObjectAttribute() {
		GenericObject genericObject = GenericObject.ofSingleSimple("foo", "BAR");
		//
		Assertions
				.assertThat(genericObject.withAttribute(GenericObjectAttribute.ofSimple("foo", "NOBAR")))
				.isEqualTo(GenericObject.ofSingleSimple("foo", "NOBAR"));
		Assertions
				.assertThat(genericObject.withAttribute(GenericObjectAttribute.of("foo", ListValueWrapper.empty())))
				.isEqualTo(GenericObject.of(GenericObjectAttribute.of("foo", ListValueWrapper.empty())));
	}

	@Test
	void testWithAttributeWithStringAndValueWrapper() {
		GenericObject genericObject = GenericObject.ofSingleSimple("foo", "BAR");
		//
		Assertions
				.assertThat(genericObject.withAttribute("foo", SimpleValueWrapper.of("NOBAR")))
				.isEqualTo(GenericObject.ofSingleSimple("foo", "NOBAR"));
		Assertions
				.assertThat(genericObject.withAttribute("foo", ListValueWrapper.empty()))
				.isEqualTo(GenericObject.of(GenericObjectAttribute.of("foo", ListValueWrapper.empty())));
		Assertions
				.assertThat(genericObject.withAttribute("foo", null))
				.isEqualTo(GenericObject.empty());
	}
}
