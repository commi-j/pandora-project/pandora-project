package tk.labyrinth.pandora.datatypes.domain.genericreference;

import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.testing.misc4j.lib.junit5.ContribAssertions;

class GenericReferenceTest {

	@Test
	void testFromWithString() {
		Assertions.assertEquals(
				GenericReference.of(
						"test",
						List.of(
								GenericReferenceAttribute.of("foo", "bar"),
								GenericReferenceAttribute.of("aze", "rty"))),
				GenericReference.from("test(foo=bar,aze=rty)"));
		Assertions.assertEquals(
				GenericReference.of(
						"datatypebase",
						List.of(
								GenericReferenceAttribute.of(
										"originReference",
										"javaclass(signature=java.lang:String)"))),
				GenericReference.from("datatypebase(originReference=javaclass(signature=java.lang:String))"));
	}

	@Test
	void testFromWithString2() {
		ContribAssertions.assertThrows(
				() -> GenericReference.from("a()"),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: a()",
						fault.toString()));
		ContribAssertions.assertThrows(
				() -> GenericReference.from("a(b)"),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: Does not match: 'b'",
						fault.toString()));
		//
		Assertions.assertEquals(
				GenericReference.of(
						"a",
						List.of(GenericReferenceAttribute.of("b", "c"))),
				GenericReference.from("a(b=c)"));
		Assertions.assertEquals(
				GenericReference.of(
						"a",
						List.of(
								GenericReferenceAttribute.of("b", "c"),
								GenericReferenceAttribute.of("d", "e"))),
				GenericReference.from("a(b=c,d=e)"));
		Assertions.assertEquals(
				GenericReference.of(
						"a",
						List.of(
								GenericReferenceAttribute.of("b", "c(f=g)"),
								GenericReferenceAttribute.of("d", "e"))),
				GenericReference.from("a(b=c(f=g),d=e)"));
	}
}
