package tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.reference.ReferenceObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperFactory;
import tk.labyrinth.pandora.misc4j.lib.jackson.VavrObjectMapperConfigurer;

class ObjectModelAttributeTest {

	private final ObjectMapper objectMapper = ObjectMapperFactory.defaultConfigured(
			new ReferenceObjectMapperConfigurer(),
			new VavrObjectMapperConfigurer());

	@Test
	void testDeserialize() throws JsonProcessingException {
		ObjectModelAttribute expectedAttribute = ObjectModelAttribute.builder()
				.datatype(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(String.class))
				.name("testAttribute")
				.build();
		ObjectModelAttribute actualAttribute = objectMapper.readValue(
				"""
						{
						  "datatype" : {
						    "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						  },
						  "name" : "testAttribute"
						}""",
				ObjectModelAttribute.class);
		//
		// FIXME: Wrong reference datatypes in object. Should not compare Strings but make it deserialize properly.
		Assertions
				.assertThat(actualAttribute.toString())
				.isEqualTo(expectedAttribute.toString());
	}

	@Test
	void testSerialize() throws JsonProcessingException {
		Assertions
				.assertThat(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(ObjectModelAttribute.builder()
						.datatype(JavaBaseTypeUtils.createDatatypeFromPlainJavaClass(String.class))
						.name("testAttribute")
						.build()))
				.isEqualTo("""
						{
						  "datatype" : {
						    "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						  },
						  "name" : "testAttribute"
						}""");
	}
}
