package tk.labyrinth.pandora.datatypes.domain.valuewrapper;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ListValueWrapperTest {

	@Test
	void testUnwrapWithNullElement() {
		Assertions
				.assertThat(ListValueWrapper
						.ofElements(SimpleValueWrapper.of("foo"), null, SimpleValueWrapper.of("bar"))
						.unwrap())
				.isEqualTo(List.of("foo", null, "bar"));
	}
}
