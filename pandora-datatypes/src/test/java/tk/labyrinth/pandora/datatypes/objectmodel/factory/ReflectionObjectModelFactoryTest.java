package tk.labyrinth.pandora.datatypes.objectmodel.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicObjectModelFeatureContributor;
import tk.labyrinth.pandora.datatypes.domain.reference.ReferenceObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.domain.tag.TagObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.test.InterfaceTestModel;
import tk.labyrinth.pandora.dependencies.jaap.ClassSignatureObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperFactory;
import tk.labyrinth.pandora.misc4j.lib.jackson.VavrObjectMapperConfigurer;

import java.util.List;

class ReflectionObjectModelFactoryTest {

	private final ObjectMapper objectMapper = ObjectMapperFactory.defaultConfigured(
			new ClassSignatureObjectMapperConfigurer(),
			new ReferenceObjectMapperConfigurer(),
			new TagObjectMapperConfigurer(),
			new VavrObjectMapperConfigurer());

	private final ReflectionObjectModelFactory reflectionObjectModelFactory = new ReflectionObjectModelFactory(
			List.of(),
			List.of(new PolymorphicObjectModelFeatureContributor()));

	@Test
	void testCreateModelFromJavaClassWithDatatypeBase() throws JsonProcessingException {
		Assertions.assertEquals(
				"""
						{
						  "attributes" : [ {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "aliases"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.datatypebase:DatatypeBaseFeature)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "features"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "name"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "parameters"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.datatypebase:AliasDatatypeBaseReference)"
						    },
						    "features" : [ ],
						    "name" : "parentReference"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "signature"
						  } ],
						  "code" : "datatypebase",
						  "features" : [ ],
						  "name" : "DatatypeBase",
						  "renderRule" : "attribute:name",
						  "tags" : [ "pandoraIndex", "pandora" ]
						}""",
				objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(
						reflectionObjectModelFactory.createModelFromJavaClass(DatatypeBase.class)));
	}

	@Test
	void testCreateModelFromJavaClassWithInterfaceTestModel() throws JsonProcessingException {
		Assertions.assertEquals(
				"""
						{
						  "attributes" : [ {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:Integer)"
						    },
						    "features" : [ ],
						    "name" : "integer"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "string"
						  } ],
						  "code" : "javabasetype:tk.labyrinth.pandora.datatypes.test:InterfaceTestModel",
						  "features" : [ ],
						  "name" : "InterfaceTestModel",
						  "tags" : [ ]
						}""",
				objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(
						reflectionObjectModelFactory.createModelFromJavaClass(InterfaceTestModel.class)));
	}

	@Test
	void testCreateModelFromJavaClassWithObjectModel() throws JsonProcessingException {
		Assertions.assertEquals(
				"""
						{
						  "attributes" : [ {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute:ObjectModelAttribute)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "attributes"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "code"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.objectmodel.feature:ObjectModelFeature)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "features"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "name"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "parameterNames"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "renderRule"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.tag:Tag)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "tags"
						  } ],
						  "code" : "objectmodel",
						  "features" : [ ],
						  "name" : "ObjectModel",
						  "renderRule" : "attribute:name",
						  "tags" : [ "pandora" ]
						}""",
				objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(
						reflectionObjectModelFactory.createModelFromJavaClass(ObjectModel.class)));
	}

	@Test
	void testCreateModelFromJavaClassWithObjectModelAttribute() throws JsonProcessingException {
		Assertions.assertEquals(
				"""
						{
						  "attributes" : [ {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.datatype:Datatype)"
						    },
						    "features" : [ ],
						    "name" : "datatype"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:io.vavr.collection:List)",
						      "parameters" : [ {
						        "baseReference" : "datatypebase(alias=javabasetype:tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute:ObjectModelAttributeFeature)"
						      } ]
						    },
						    "features" : [ ],
						    "name" : "features"
						  }, {
						    "datatype" : {
						      "baseReference" : "datatypebase(alias=javabasetype:java.lang:String)"
						    },
						    "features" : [ ],
						    "name" : "name"
						  } ],
						  "code" : "javabasetype:tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute:ObjectModelAttribute",
						  "features" : [ ],
						  "name" : "ObjectModelAttribute",
						  "renderRule" : "pattern:$name : $datatype",
						  "tags" : [ ]
						}""",
				objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(
						reflectionObjectModelFactory.createModelFromJavaClass(ObjectModelAttribute.class)));
	}
}
