package tk.labyrinth.pandora.datatypes.javabasetype;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;

class SignatureJavaBaseTypeReferenceTest {

	@Test
	void testFromTypeSignatureString() {
		Assertions.assertEquals(
				Object.class,
				SignatureJavaBaseTypeReference.fromTypeSignatureString("java.lang:Object").resolveClass());
	}

	@Test
	void testResolveClass() {
		SignatureJavaBaseTypeReference reference = SignatureJavaBaseTypeReference.from(Object.class);
		//
		Assertions.assertEquals("javabasetype(signature=java.lang:Object)", reference.toString());
		Assertions.assertEquals(Object.class, reference.resolveClass());
	}
}
