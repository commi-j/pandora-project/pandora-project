package tk.labyrinth.pandora.datatypes.objectmodel.factory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.core.AnnotationProcessingRound;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicObjectModelFeatureContributor;
import tk.labyrinth.pandora.datatypes.test.InterfaceTestModel;

import java.util.List;

@ExtendWithJaap
class JaapObjectModelFactoryTest {

	@CompilationTarget(compiledTypes = DatatypeBase.class)
	@Test
	void testCreateModelOfDatatypeBase(AnnotationProcessingRound round) {
		JaapObjectModelFactory jaapObjectModelFactory = new JaapObjectModelFactory();
		ReflectionObjectModelFactory reflectionObjectModelFactory = new ReflectionObjectModelFactory(
				List.of(),
				List.of(new PolymorphicObjectModelFeatureContributor()));
		//
		ObjectModel jaapModel = jaapObjectModelFactory.createModelFromTypeElementHandle(
				ProcessingContext.of(round).getTypeElementHandle(DatatypeBase.class));
		ObjectModel reflectionModel = reflectionObjectModelFactory.createModelFromJavaClass(DatatypeBase.class);
		//
		Assertions.assertEquals(reflectionModel, jaapModel);
	}

	@CompilationTarget(compiledTypes = InterfaceTestModel.class)
	@Test
	void testCreateModelOfInterfaceTestModel(AnnotationProcessingRound round) {
		JaapObjectModelFactory jaapObjectModelFactory = new JaapObjectModelFactory();
		ReflectionObjectModelFactory reflectionObjectModelFactory = new ReflectionObjectModelFactory(
				List.of(),
				List.of(new PolymorphicObjectModelFeatureContributor()));
		//
		ObjectModel jaapModel = jaapObjectModelFactory.createModelFromTypeElementHandle(
				ProcessingContext.of(round).getTypeElementHandle(InterfaceTestModel.class));
		ObjectModel reflectionModel = reflectionObjectModelFactory.createModelFromJavaClass(InterfaceTestModel.class);
		//
		Assertions.assertEquals(reflectionModel, jaapModel);
	}

	@CompilationTarget(compiledTypes = ObjectModel.class)
	@Test
	void testCreateModelOfObjectModel(AnnotationProcessingRound round) {
		JaapObjectModelFactory jaapObjectModelFactory = new JaapObjectModelFactory();
		ReflectionObjectModelFactory reflectionObjectModelFactory = new ReflectionObjectModelFactory(
				List.of(),
				List.of(new PolymorphicObjectModelFeatureContributor()));
		//
		ObjectModel jaapModel = jaapObjectModelFactory.createModelFromTypeElementHandle(
				ProcessingContext.of(round).getTypeElementHandle(ObjectModel.class));
		ObjectModel reflectionModel = reflectionObjectModelFactory.createModelFromJavaClass(ObjectModel.class);
		//
		Assertions.assertEquals(reflectionModel, jaapModel);
	}
}
