package tk.labyrinth.pandora.datatypes.object;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapperObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperFactory;

class GenericObjectObjectMapperConfigurerTest {

	private final GenericObject canonicalObject = GenericObject.of(
			GenericObjectAttribute.ofSimple("modelReference", CodeObjectModelReference.of("test").toString()),
			GenericObjectAttribute.ofSimple("number", "12"),
			GenericObjectAttribute.ofSimple("string", "Value"),
			GenericObjectAttribute.ofSimple("uid", "a00fc228-7d28-4828-a218-eed7424e59fa"));

	private final ObjectMapper objectMapper = ObjectMapperFactory.defaultConfigured(
			new GenericObjectObjectMapperConfigurer(),
			new ValueWrapperObjectMapperConfigurer());

	@Test
	void testDeserialize() throws JsonProcessingException {
		Assertions.assertEquals(
				canonicalObject,
				objectMapper.readValue(
						"""
								{
								  "modelReference" : "objectmodel(code=test)",
								  "number" : 12,
								  "string" : "Value",
								  "uid" : "a00fc228-7d28-4828-a218-eed7424e59fa"
								}""",
						GenericObject.class));
	}

	@Test
	void testSerialize() throws JsonProcessingException {
		Assertions.assertEquals(
				"""
						{
						  "modelReference" : "objectmodel(code=test)",
						  "number" : "12",
						  "string" : "Value",
						  "uid" : "a00fc228-7d28-4828-a218-eed7424e59fa"
						}""",
				objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(canonicalObject));
	}

	@Test
	void testToString() {
		Assertions.assertEquals(
				"GenericObject(modelReference=objectmodel(code=test),number=12,string=Value,uid=a00fc228-7d28-4828-a218-eed7424e59fa)",
				canonicalObject.toString());
	}
}
