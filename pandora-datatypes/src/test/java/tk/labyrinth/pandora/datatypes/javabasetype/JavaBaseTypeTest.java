package tk.labyrinth.pandora.datatypes.javabasetype;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeKind;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;

import java.nio.file.Path;

@SpringBootTest
class JavaBaseTypeTest {

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	@Test
	void testComputeDatatypeKind() {
		Assertions.assertEquals(
				DatatypeKind.SIMPLE,
				javaBaseTypeRegistry.getJavaBaseType(CodeObjectModelReference.class).computeDatatypeKind());
		Assertions.assertEquals(
				DatatypeKind.SIMPLE,
				javaBaseTypeRegistry.getJavaBaseType(DatatypeKind.class).computeDatatypeKind());
		Assertions.assertEquals(
				DatatypeKind.SIMPLE,
				javaBaseTypeRegistry.getJavaBaseType(Path.of("foo").getClass()).computeDatatypeKind());
		{
			Assertions.assertEquals(
					DatatypeKind.LIST,
					javaBaseTypeRegistry.getJavaBaseType(java.util.Set.class).computeDatatypeKind());
			Assertions.assertEquals(
					DatatypeKind.LIST,
					javaBaseTypeRegistry.getJavaBaseType(io.vavr.collection.Set.class).computeDatatypeKind());
		}
	}
}
