package tk.labyrinth.pandora.datatypes.javabasetype.registry;

import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;

import java.util.HashSet;

@SpringBootTest
class JavaBaseTypeRegistryTest {

	@Autowired
	private JavaBaseTypeRegistry javaBaseTypeRegistry;

	@Test
	void testGetJavaBaseTypeWithJavaHashSetClass() {
		JavaBaseType javaBaseType = javaBaseTypeRegistry.getJavaBaseType(HashSet.class);
		//
		// Expected to be LIST since inherits Java List which is declared as LIST by DefaultJavaBaseTypeContributor.
		Assertions
				.assertThat(javaBaseType.getHints())
				.isEqualTo(List.of(JavaBaseTypeHint.LIST));
	}

	@Test
	void testGetJavaBaseTypeWithVavrListClass() {
		JavaBaseType javaBaseType = javaBaseTypeRegistry.getJavaBaseType(List.class);
		//
		// Expected to be LIST since declared as LIST by DefaultJavaBaseTypeContributor.
		Assertions
				.assertThat(javaBaseType.getHints())
				.isEqualTo(List.of(JavaBaseTypeHint.LIST));
	}
}
