package tk.labyrinth.pandora.datatypes.reference;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

@SpringBootTest
class ReferenceObjectMapperConfigurerTest {

	@Autowired
	ObjectMapper objectMapper;

	@Test
	void testDeserialize() {
		{
			// Control check, these ones do not go through this converter.
			{
				// GenericReference
				//
				Assertions
						.assertThat((Object) objectMapper
								.convertValue(CodeObjectModelReference.of(ObjectModel.MODEL_CODE), GenericReference.class)
								.toString())
						.isEqualTo("objectmodel(code=objectmodel)");
			}
			{
				// CodeObjectModelReference
				//
				Assertions
						.assertThat(objectMapper
								.convertValue(
										CodeObjectModelReference.of(ObjectModel.MODEL_CODE),
										CodeObjectModelReference.class)
								.toString())
						.isEqualTo("objectmodel(code=objectmodel)");
			}
		}
		{
			{
				// Reference<?>
				//
				Assertions
						.assertThat((Object) objectMapper
								.convertValue(
										CodeObjectModelReference.of(ObjectModel.MODEL_CODE),
										objectMapper.constructType(TypeUtils.parameterize(tk.labyrinth.pandora.datatypes.domain.reference.Reference.class, TypeUtils.WILDCARD_ALL)))
								.toString())
						.isEqualTo("objectmodel(code=objectmodel)");
			}
			{
				// Reference<ObjectModel>
				//
				Assertions
						.assertThat((Object) objectMapper
								.convertValue(
										CodeObjectModelReference.of(ObjectModel.MODEL_CODE),
										objectMapper.constructType(TypeUtils.parameterize(tk.labyrinth.pandora.datatypes.domain.reference.Reference.class, ObjectModel.class)))
								.toString())
						.isEqualTo("objectmodel(code=objectmodel)");
			}
			{
				// FIXME: This one should actually fail - we need to check parameter too.
				// Reference<JavaBaseType>
				//
				Assertions
						.assertThat((Object) objectMapper
								.convertValue(
										CodeObjectModelReference.of(ObjectModel.MODEL_CODE),
										objectMapper.constructType(TypeUtils.parameterize(Reference.class, JavaBaseType.class)))
								.toString())
						.isEqualTo("objectmodel(code=objectmodel)");
			}
		}
	}
}
