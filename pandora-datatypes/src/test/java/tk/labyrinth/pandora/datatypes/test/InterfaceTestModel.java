package tk.labyrinth.pandora.datatypes.test;

public interface InterfaceTestModel {

	Integer getInteger();

	String getString();
}
