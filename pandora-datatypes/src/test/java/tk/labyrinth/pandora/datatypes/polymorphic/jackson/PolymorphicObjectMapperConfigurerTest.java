package tk.labyrinth.pandora.datatypes.polymorphic.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Value;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicRoot;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapperObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.polymorphic.registry.PolymorphicClassRegistry;
import tk.labyrinth.pandora.dependencies.jackson.ObjectMapperConfiguration;
import tk.labyrinth.pandora.dependencies.spring.JavaElementDiscoverer;
import tk.labyrinth.pandora.testing.misc4j.lib.spring.ExtendWithSpring;

@ExtendWithSpring
@Import({
		GenericObjectObjectMapperConfigurer.class,
		JavaElementDiscoverer.class,
		ObjectMapperConfiguration.class,
		PolymorphicBeanDeserializerModifier.class,
		PolymorphicBeanSerializerModifier.class,
		PolymorphicClassRegistry.class,
		PolymorphicObjectMapperConfigurer.class,
		ValueWrapperObjectMapperConfigurer.class,
})
class PolymorphicObjectMapperConfigurerTest {

	@Test
	void testConvertFromGenericObjectWithQualifierAttribute(@Autowired ObjectMapper objectMapper) {
		Assertions.assertEquals(
				new DependentChild(),
				objectMapper.convertValue(
						GenericObject.ofSingleSimple("parentish", "dependent"),
						(Class<?>) Parent.class));
		Assertions.assertEquals(
				new IndependentChild(),
				objectMapper.convertValue(
						GenericObject.ofSingleSimple("childish", "independent"),
						(Class<?>) Parent.class));
	}

	@Test
	void testConvertToGenericObjectWithQualifierAttribute(@Autowired ObjectMapper objectMapper) {
		Assertions.assertEquals(
				GenericObject.ofSingleSimple("parentish", "dependent"),
				objectMapper.convertValue(new DependentChild(), GenericObject.class));
		Assertions.assertEquals(
				GenericObject.ofSingleSimple("childish", "independent"),
				objectMapper.convertValue(new IndependentChild(), GenericObject.class));
	}

	@Test
	void testDeserializeWithQualifierAttribute(@Autowired ObjectMapper objectMapper) throws JsonProcessingException {
		Assertions.assertEquals(
				new DependentChild(),
				objectMapper.readValue(
						"""
								{"parentish":"dependent"}""",
						(Class<?>) Parent.class));
		Assertions.assertEquals(
				new IndependentChild(),
				objectMapper.readValue(
						"""
								{"childish":"independent"}""",
						(Class<?>) Parent.class));
	}

	@Test
	void testSerializeWithQualifierAttribute(@Autowired ObjectMapper objectMapper) throws JsonProcessingException {
		Assertions.assertEquals(
				"""
						{"parentish":"dependent"}""",
				objectMapper.writeValueAsString(new DependentChild()));
		Assertions.assertEquals(
				"""
						{"childish":"independent"}""",
				objectMapper.writeValueAsString(new IndependentChild()));
	}

	@PolymorphicRoot(qualifierAttributeName = "parentish")
	public interface Parent {
		// empty
	}

	@PolymorphicLeaf(rootClass = Parent.class, qualifierAttributeValue = "dependent")
	@Value(staticConstructor = "of")
	public static class DependentChild {
		// empty
	}

	@PolymorphicLeaf(
			rootClass = Parent.class,
			qualifierAttributeName = "childish",
			qualifierAttributeValue = "independent")
	@Value(staticConstructor = "of")
	public static class IndependentChild {
		// empty
	}
}
