package tk.labyrinth.pandora.datatypes.polymorphic.jackson;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.jsontype.impl.AsPropertyTypeSerializer;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;
import com.fasterxml.jackson.databind.ser.impl.TypeWrappedSerializer;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class PolymorphicBeanSerializerModifier extends BeanSerializerModifier {

	@Override
	public JsonSerializer<?> modifySerializer(
			SerializationConfig config,
			BeanDescription beanDesc,
			JsonSerializer<?> serializer) {
		JsonSerializer<?> result;
		{
			Class<?> beanClass = beanDesc.getBeanClass();
			//
			// FIXME: Check ObjectModel and not Class.
			if (beanClass.isAnnotationPresent(PolymorphicLeaf.class)) {
				GenericObjectAttribute qualifierAttribute = PolymorphicUtils.resolveQualifierAttribute(beanClass);
				//
				result = new TypeWrappedSerializer(
						new AsPropertyTypeSerializer(
								new PolymorphicTypeIdResolver(qualifierAttribute.getValue().toString()),
								null,
								qualifierAttribute.getName()),
						serializer);
			} else {
				result = serializer;
			}
		}
		return result;
	}

	@RequiredArgsConstructor
	public static class PolymorphicTypeIdResolver extends TypeIdResolverBase {

		private final String value;

		@Override
		public JsonTypeInfo.Id getMechanism() {
			return JsonTypeInfo.Id.CUSTOM;
		}

		@Override
		public String idFromValue(Object value) {
			return this.value;
		}

		@Override
		public String idFromValueAndType(Object value, Class<?> suggestedType) {
			// TODO: Implement.
			throw new NotImplementedException();
		}
	}
}
