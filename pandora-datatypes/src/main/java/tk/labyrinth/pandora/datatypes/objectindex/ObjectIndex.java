package tk.labyrinth.pandora.datatypes.objectindex;

import tk.labyrinth.expresso.lang.predicate.Predicate;
import tk.labyrinth.expresso.query.lang.predicate.Predicates;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;

/**
 * @param <T> Target
 */
public interface ObjectIndex<T> {

	String INDEX_ATTRIBUTE_NAME = "pandoraIndex";

	GenericObjectAttribute INDEX_ATTRIBUTE = GenericObjectAttribute.ofTrue(INDEX_ATTRIBUTE_NAME);

	Predicate INDEX_PREDICATE = Predicates.equalTo(INDEX_ATTRIBUTE_NAME, true);

	String INDEX_MODEL_TAG_NAME = INDEX_ATTRIBUTE_NAME;

	Tag INDEX_MODEL_TAG = Tag.of(INDEX_MODEL_TAG_NAME);

	String TARGET_REFERENCE_ATTRIBUTE_NAME = "targetReference";

	Reference<T> getTargetReference();
}
