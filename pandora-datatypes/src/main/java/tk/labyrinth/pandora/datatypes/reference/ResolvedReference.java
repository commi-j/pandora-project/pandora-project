package tk.labyrinth.pandora.datatypes.reference;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class ResolvedReference<T, R extends Reference<T>> {

	/**
	 * 0..n
	 */
	List<T> objects;

	R reference;

	@Nullable
	public T findSingle() {
		if (objects.size() > 1) {
			throw new IllegalStateException("Require single or empty: reference = %s, objects = %s"
					.formatted(reference, objects));
		}
		return objects.getOrNull();
	}

	public T getSingle() {
		if (objects.size() != 1) {
			throw new IllegalStateException("Require single: reference = %s, objects = %s"
					.formatted(reference, objects));
		}
		return objects.single();
	}

	public boolean hasObjects() {
		return !objects.isEmpty();
	}

	public boolean hasSingle() {
		return objects.size() == 1;
	}

	public static <T, R extends Reference<T>> ResolvedReference<T, R> of(R reference, List<T> objects) {
		return new ResolvedReference<>(objects, reference);
	}

	public static <T, R extends Reference<T>> ResolvedReference<T, R> ofNonNull(R reference, T object) {
		Objects.requireNonNull(object, "object, reference = %s".formatted(reference));
		return of(reference, List.of(object));
	}

	public static <T, R extends Reference<T>> ResolvedReference<T, R> ofNullable(R reference, @Nullable T object) {
		return of(reference, object != null ? List.of(object) : List.empty());
	}
}
