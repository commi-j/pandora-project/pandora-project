package tk.labyrinth.pandora.datatypes.tool;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.expresso.query.domain.java.JavaPropertyAccessor;
import tk.labyrinth.expresso.query.domain.java.SpringPropertyAccessor;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;
import tk.labyrinth.pandora.datatypes.domain.valuewrapper.ValueWrapper;
import tk.labyrinth.pandora.datatypes.objectproxy.ObjectProxy;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

// FIXME: REVIEW_LOCATION
@Bean
@RequiredArgsConstructor
public class PandoraPropertyAccessor implements JavaPropertyAccessor {

	private final ObjectMapper objectMapper;

	private final SpringPropertyAccessor springPropertyAccessor = new SpringPropertyAccessor();

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(Object target, String property) {
		Object result;
		{
			// GenericObject has all values wrapped into ValueWrapper. We need to unwrap them
			// to properly compare simple types like String, as StringPropertyPredicate does not support
			// any other type.
			//
			if (target instanceof GenericObject genericObject) {
				result = ValueWrapper.unwrapNullable(genericObject.lookup(property));
			} else if (target instanceof ObjectProxy objectProxy) {
				result = ValueWrapper.unwrapNullable(objectProxy.getObject().lookup(property));
			} else {
				// TODO: Improve Expresso to convert predicate values to expected type of objects.
				// Normalizing values. The case to fail without normalization was "ROOT" as predicate value
				// and StoreDesignation.ROOT as attribute value.
				//
				result = objectMapper
						.convertValue(springPropertyAccessor.get(target, property), ValueWrapper.class)
						.unwrap();
			}
		}
		return (T) result;
	}
}
