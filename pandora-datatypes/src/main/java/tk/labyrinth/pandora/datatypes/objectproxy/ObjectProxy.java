package tk.labyrinth.pandora.datatypes.objectproxy;

import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObject;

/**
 * Interface that is implemented by created proxies backed by {@link GenericObject}.
 */
public interface ObjectProxy {

	GenericObject getObject();
}
