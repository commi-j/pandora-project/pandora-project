package tk.labyrinth.pandora.datatypes.objectmodelparameter;

import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.jaap.model.signature.SignatureSeparators;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.DatatypeBase;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;

public class ObjectModelParameterUtils {

	public static final String DATATYPE_ALIAS_PREFIX = "objectmodelparameter:";

	public static String createDatatypeAlias(ModelCodeAndNameObjectModelParameterReference reference) {
		return createDatatypeAlias(reference.getModelCode(), reference.getName());
	}

	public static String createDatatypeAlias(String signature) {
		return "%s%s".formatted(DATATYPE_ALIAS_PREFIX, signature);
	}

	public static String createDatatypeAlias(String modelCode, String parameterName) {
		return createDatatypeAlias(createDatatypeSignature(modelCode, parameterName));
	}

	public static String createDatatypeSignature(String modelCode, String parameterName) {
		return "%s%S%s".formatted(modelCode, SignatureSeparators.TYPE_PARAMETER, parameterName);
	}

	public static ModelCodeAndNameObjectModelParameterReference getObjectModelParameterReference(
			Reference<DatatypeBase> datatypeBaseReference) {
		if (!isObjectModelParameterDatatypeBaseReference(datatypeBaseReference)) {
			throw new IllegalArgumentException();
		}
		//
		String modelCodeAndIndexString = AliasDatatypeBaseReference.getAlias(datatypeBaseReference)
				.substring(DATATYPE_ALIAS_PREFIX.length());
		//
		Pair<String, String> modelCodeAndIndex = StringUtils.splitByLastOccurrence(modelCodeAndIndexString, ":");
		//
		return ModelCodeAndNameObjectModelParameterReference.of(modelCodeAndIndex.getLeft(), modelCodeAndIndex.getRight());
	}

	public static boolean isObjectModelParameterDatatypeAlias(String alias) {
		return alias.startsWith(DATATYPE_ALIAS_PREFIX);
	}

	public static boolean isObjectModelParameterDatatypeBaseReference(Reference<DatatypeBase> datatypeBaseReference) {
		return AliasDatatypeBaseReference.isAliasDatatypeBaseReference(datatypeBaseReference) &&
				isObjectModelParameterDatatypeAlias(AliasDatatypeBaseReference.getAlias(datatypeBaseReference));
	}
}
