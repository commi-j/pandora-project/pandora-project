package tk.labyrinth.pandora.datatypes.objectmodel;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

@Bean
@RequiredArgsConstructor
public class ObjectModelFactory {

	private final ReflectionObjectModelFactory reflectionObjectModelFactory;

	public ObjectModel createModelFromJavaClass(Class<?> javaClass) {
		return reflectionObjectModelFactory.createModelFromJavaClass(javaClass);
	}

	public static CodeObjectModelReference computeObjectModelReference(Class<?> javaClass) {
		return CodeObjectModelReference.of(ReflectionObjectModelFactory.resolveModelCode(javaClass));
	}
}
