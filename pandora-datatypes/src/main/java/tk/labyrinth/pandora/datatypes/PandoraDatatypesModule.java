package tk.labyrinth.pandora.datatypes;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.domain.reference.FromStringToReferenceConverter;
import tk.labyrinth.pandora.datatypes.reference.FromGenericReferenceToReferenceConverter;
import tk.labyrinth.pandora.datatypes.reference.FromReferenceToGenericReferenceConverter;
import tk.labyrinth.pandora.dependencies.PandoraDependenciesModule;

@ComponentScan
@Configuration
@Import({
		FromGenericReferenceToReferenceConverter.class,
		FromReferenceToGenericReferenceConverter.class,
		FromStringToReferenceConverter.class,
		GenericObjectObjectMapperConfigurer.class,
		PandoraDependenciesModule.class,
})
@SpringBootApplication
public class PandoraDatatypesModule {
	// empty
}
