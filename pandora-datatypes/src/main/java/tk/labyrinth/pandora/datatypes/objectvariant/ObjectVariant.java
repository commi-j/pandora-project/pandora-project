package tk.labyrinth.pandora.datatypes.objectvariant;

import lombok.Builder;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;

@Builder(toBuilder = true)
@Model
@Value
public class ObjectVariant {

	public static String NAME_ATTRIBUTE_NAME = "name";

	String name;
}
