package tk.labyrinth.pandora.datatypes.polymorphic;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;

@Builder(builderClassName = "Builder", toBuilder = true)
//
// FIXME: Does not need to be model, but fails on startup without it
//  (comment above was added before customModel logic).
@Model(hasCustomModel = true)
@Value
@With
public class PolymorphicLinkIndex implements ObjectIndex<ObjectModel> {

	public static final String LEAF_REFERENCE_ATTRIBUTE_NAME = "leafReference";

	public static final String ROOT_REFERENCE_ATTRIBUTE_NAME = "rootReference";

	AliasDatatypeBaseReference leafReference;

	GenericObjectAttribute qualifierAttribute;

	AliasDatatypeBaseReference rootReference;

	CodeObjectModelReference targetReference;
}
