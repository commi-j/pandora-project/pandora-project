package tk.labyrinth.pandora.datatypes.javabasetype.registry;

import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;

public interface ObjectJavaBaseTypeContributor {

	List<Pair<JavaBaseType, @Nullable ObjectModel>> contributeJavaBaseTypes();
}
