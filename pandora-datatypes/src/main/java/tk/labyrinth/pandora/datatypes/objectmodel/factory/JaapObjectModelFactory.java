package tk.labyrinth.pandora.datatypes.objectmodel.factory;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.jibx.schema.codegen.extend.DefaultNameConverter;
import org.jibx.schema.codegen.extend.NameConverter;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotation;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.handle.type.TypeHandle;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.ElementHandle;
import tk.labyrinth.jaap.model.element.FieldElementHandle;
import tk.labyrinth.jaap.model.element.MethodElementHandle;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.jaap.model.element.TypeParameterElementHandle;
import tk.labyrinth.jaap.model.signature.ClassSignature;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderFunction;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

@LazyComponent
@RequiredArgsConstructor
public class JaapObjectModelFactory {

	public static final MergedAnnotationSpecification mergedAnnotationSpecification = MergedAnnotationSpecification
			.metaAnnotation();

	private static final NameConverter nameConverter = new DefaultNameConverter();

	public ObjectModel createModelFromTypeElementHandle(TypeElementHandle typeElementHandle) {
		String code = resolveCode(typeElementHandle);
		//
		return ObjectModel.builder()
				.attributes(resolveAttributes(typeElementHandle))
				.code(code)
				.features(List.empty()) // FIXME: There should be proper features discovery.
				.name(resolveName(typeElementHandle))
				.parameterNames(typeElementHandle.hasTypeParameters()
						? List.ofAll(typeElementHandle.getTypeParameters()).map(TypeParameterElementHandle::getName)
						: null)
				.renderRule(resolveRenderRule(typeElementHandle))
				.tags(resolveTags(typeElementHandle))
				.build();
	}

	public String getSignature(TypeElementHandle typeElementHandle) {
		return typeElementHandle.getSignature().toString();
	}

	public String getSignature(TypeHandle typeHandle) {
		String result;
		{
			if (typeHandle.isDeclaredType()) {
				result = getSignature(typeHandle.asDeclaredType().toElement());
			} else if (typeHandle.isWildcardType()) {
				result = typeHandle.asWildcardType().getDescription().toString();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public TypeDescription getTypeDescriptionOf(ElementHandle attributeMember) {
		TypeDescription result;
		{
			if (attributeMember.isFieldElement()) {
				result = attributeMember.asFieldElement().getType().getDescription();
			} else if (attributeMember.isMethodElement()) {
				result = attributeMember.asMethodElement().getReturnType().getDescription();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public List<ObjectModelAttribute> resolveAttributes(TypeElementHandle typeElementHandle) {
		List<ElementHandle> attributeMembers = selectAttributeMembers(typeElementHandle);
		return attributeMembers
				.map(attributeMember -> ObjectModelAttribute.builder()
						.datatype(JavaBaseTypeUtils.createDatatypeFromTypeDescription(getTypeDescriptionOf(attributeMember)))
						.features(List.empty()) // FIXME: There should be proper features discovery.
						.name(ObjectModelUtils.getAttributeName(attributeMember))
						.build())
				.sortBy(ObjectModelAttribute::getName);
	}

	public String resolveCode(TypeElementHandle typeElementHandle) {
		String result;
		{
			MergedAnnotation modelAnnotation = typeElementHandle
					.getMergedAnnotation(Model.class, mergedAnnotationSpecification);
			if (modelAnnotation.isPresent()) {
				String codeValue = modelAnnotation.getAttributeValueAsString("value");
				result = !codeValue.isEmpty() ? codeValue : typeElementHandle.getSimpleName().toLowerCase();
			} else {
				result = "%s:%s".formatted(
						JavaBaseType.MODEL_CODE,
						ClassSignature.from(typeElementHandle.getSignature().toString()));
			}
		}
		return result;
	}

	public String resolveName(TypeElementHandle typeElementHandle) {
		return Option.of(typeElementHandle.getMergedAnnotation(Model.class, mergedAnnotationSpecification))
				.filter(MergedAnnotation::isPresent)
				.map(modelAnnotation -> modelAnnotation.getAttributeValueAsString("name"))
				.filter(modelName -> !modelName.isEmpty())
				.getOrElse(typeElementHandle::getSimpleName);
	}

	public String resolveRenderRule(TypeElementHandle typeElementHandle) {
		String result;
		{
			if (typeElementHandle.hasMergedAnnotation(RenderAttribute.class, mergedAnnotationSpecification)) {
				result = "attribute:%s".formatted(String.join(
						",",
						typeElementHandle.getMergedAnnotation(RenderAttribute.class, mergedAnnotationSpecification)
								.getAttributeValueAsStringList("value")));
			} else if (typeElementHandle.hasMergedAnnotation(RenderFunction.class, mergedAnnotationSpecification)) {
				result = "function:%s".formatted(typeElementHandle
						.getMergedAnnotation(RenderFunction.class, mergedAnnotationSpecification)
						.getAttributeValueAsString("value"));
			} else if (typeElementHandle.hasMergedAnnotation(RenderPattern.class, mergedAnnotationSpecification)) {
				result = "pattern:%s".formatted(typeElementHandle
						.getMergedAnnotation(RenderPattern.class, mergedAnnotationSpecification)
						.getAttributeValueAsString("value"));
			} else {
				result = null;
			}
		}
		return result;
	}

	public List<ElementHandle> selectAttributeMembers(TypeElementHandle typeElementHandle) {
		List<ElementHandle> result;
		{
			if (typeElementHandle.isInterface()) {
				// Selecting methods where 'getX' -> 'x'.
				//
				result = Stream.ofAll(typeElementHandle.getDeclaredMethods())
						.filter(MethodElementHandle::isEffectivelyNonStatic)
						.filter(MethodElementHandle::isEffectivelyNonDefault)
						.filter(declaredMethod -> ObjectModelUtils.findAttributeName(declaredMethod) != null)
						.map(MethodElementHandle::asElement)
						.toList();
			} else {
				// Selecting fields.
				//
				result = Stream.ofAll(typeElementHandle.getDeclaredFields())
						.filter(FieldElementHandle::isEffectivelyNonStatic)
						.map(FieldElementHandle::asElement)
						.toList();
			}
		}
		return result;
	}

	public static List<Tag> resolveTags(TypeElementHandle typeElementHandle) {
		// TODO: Use proper MergedAnnotation mechanics instead of direct access to AnnotationHandle.
		return List
				.ofAll(typeElementHandle.getMergedAnnotation(ModelTag.class, mergedAnnotationSpecification)
						.getRelevantAnnotations())
				.map(modelTagAnnotationHandle -> Tag.of(
						ObjectUtils.orElse(
								StringUtils.emptyToNull(modelTagAnnotationHandle.findValueAsString("tagName")),
								StringUtils.emptyToNull(modelTagAnnotationHandle.findValueAsString("value"))),
						StringUtils.emptyToNull(modelTagAnnotationHandle.findValueAsString("tagValue"))));
	}
}
