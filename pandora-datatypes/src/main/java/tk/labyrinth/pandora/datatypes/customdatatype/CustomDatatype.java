package tk.labyrinth.pandora.datatypes.customdatatype;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.root.RootModel;

@Builder(builderClassName = "Builder", toBuilder = true)
@ModelTagPandora
@RootModel(code = CustomDatatype.MODEL_CODE, primaryAttribute = CustomDatatype.NAME_ATTRIBUTE_NAME)
@Value
@With
public class CustomDatatype {

	public static final String MODEL_CODE = "customdatatype";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	SignatureJavaBaseTypeReference backingTypeReference;

	String name;

	// TODO: Support parameter types too.
	List<String> parameterNames;

	@Nullable
	AliasDatatypeBaseReference parentReference;
}
