package tk.labyrinth.pandora.datatypes.javabasetype.registry;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.collection.Traversable;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import tk.labyrinth.jaap.model.signature.TypeSignature;
import tk.labyrinth.pandora.datatypes.domain.datatype.DatatypeUtils;
import tk.labyrinth.pandora.datatypes.domain.datatypebase.AliasDatatypeBaseReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.SignatureJavaBaseTypeReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.registry.JavaBaseTypeContributor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelFactory;
import tk.labyrinth.pandora.dependencies.spring.JavaElementDiscoverer;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

@Deprecated
@LazyComponent
@RequiredArgsConstructor
public class JavaBaseTypeRegistry {

	private final java.util.List<JavaBaseTypeContributor> entryContributors;

	private final JavaElementDiscoverer javaElementDiscoverer;

	private final ObjectModelFactory objectModelFactory;

	private io.vavr.collection.Map<Class<?>, Pair<JavaBaseType, @Nullable ObjectModel>> entries;

	// FIXME: Not sure if it's good that this method accesses entries field.
	private JavaBaseType createJavaBaseType(Class<?> javaClass, boolean computeInheritedHints) {
		JavaBaseType result;
		{
			JavaBaseType rawJavaBaseType = JavaBaseType.builder()
					.implicitHints(JavaBaseTypeHint.computeImplicitHints(javaClass))
					.hasCustomObjectModel(resolveHasCustomModel(javaClass))
					.name(javaClass.getSimpleName())
					.parameterNames(javaClass.getTypeParameters().length > 0
							? List.of(javaClass.getTypeParameters()).map(TypeVariable::getName)
							: null)
					.signature(TypeSignature.of(javaClass))
					.build();
			//
			JavaBaseType enrichedJavaBaseType = rawJavaBaseType.withInheritedHints(computeInheritedHints
					? computeInheritedHints(entries.values().map(Pair::getLeft), rawJavaBaseType)
					: List.empty());
			//
			if (computeInheritedHints) {
				// Ensuring it has consistent DatatypeKind.
				enrichedJavaBaseType.computeDatatypeKind();
			}
			//
			result = enrichedJavaBaseType.withObjectModelReference(enrichedJavaBaseType.hasHint(JavaBaseTypeHint.MODEL)
					? objectModelFactory.computeObjectModelReference(javaClass)
					: null);
		}
		return result;
	}

	private List<JavaBaseType> discoverJavaBaseTypes(
			Set<SignatureJavaBaseTypeReference> registeredJavaBaseTypeReferences) {
		java.util.List<TypeSignature> typeSignaturesToProcess = new ArrayList<>();
		{
			List<TypeSignature> typeSignatures = javaElementDiscoverer.discoverTypeSignatures(
					"tk.labyrinth",
					List.of(
							new AnnotationTypeFilter(Model.class, true, true),
							new AssignableTypeFilter(Reference.class)));
			//
			typeSignatures
					.filter(typeSignature -> !registeredJavaBaseTypeReferences.contains(SignatureJavaBaseTypeReference.of(
							typeSignature)))
					.forEach(typeSignaturesToProcess::add);
		}
		//
		Map<SignatureJavaBaseTypeReference, JavaBaseType> mutableEntries = new HashMap<>();
		//
		while (!typeSignaturesToProcess.isEmpty()) {
			TypeSignature typeSignature = typeSignaturesToProcess.remove(0);
			//
			Class<?> javaClass = ClassUtils.get(typeSignature.getBinaryName());
			//
			mutableEntries.put(SignatureJavaBaseTypeReference.of(typeSignature), createJavaBaseType(javaClass, false));
			//
			ObjectModel objectModel = objectModelFactory.createModelFromJavaClass(javaClass);
			//
			objectModel.getAttributes().forEach(attribute -> DatatypeUtils
					.getAllDatatypeBaseReferences(attribute.getDatatype())
					.forEach(datatypeBaseReference -> {
						if (datatypeBaseReference instanceof AliasDatatypeBaseReference aliasDatatypeBaseReference) {
							if (JavaBaseTypeUtils.isJavaBaseTypeDatatypeBaseReference(aliasDatatypeBaseReference)) {
								SignatureJavaBaseTypeReference javaBaseTypeReference = JavaBaseTypeUtils.extractJavaBaseTypeReference(
										aliasDatatypeBaseReference);
								//
								if (!registeredJavaBaseTypeReferences.contains(javaBaseTypeReference) &&
										!mutableEntries.containsKey(javaBaseTypeReference) &&
										!typeSignaturesToProcess.contains(javaBaseTypeReference.getSignature())) {
									typeSignaturesToProcess.add(javaBaseTypeReference.getSignature());
								}
							}
						}
					}));
		}
		return List.ofAll(mutableEntries.values()).map(entry -> entry.withDynamic(true));
	}

	@PostConstruct
	private void postConstruct() {
		List<Pair<JavaBaseType, @Nullable ObjectModel>> contributedEntries = List.ofAll(entryContributors)
				.flatMap(JavaBaseTypeContributor::contributeJavaBaseTypes)
				.map(pair -> pair.getRight() != null
						? Pair.of(pair.getLeft().withHasCustomObjectModel(true), pair.getRight())
						: pair)
				.toList();
		//
		// Ensuring it has consistent DatatypeKind.
		contributedEntries.forEach(pair -> pair.getLeft().computeDatatypeKind());
		//
		List<JavaBaseType> discoveredJavaBaseTypes = discoverJavaBaseTypes(contributedEntries
				.map(pair -> SignatureJavaBaseTypeReference.from(pair.getLeft()))
				.toSet());
		//
		List<Pair<JavaBaseType, @Nullable ObjectModel>> allEntries = contributedEntries
				.appendAll(discoveredJavaBaseTypes.map(javaBaseType -> Pair.of(javaBaseType, null)));
		List<JavaBaseType> allJavaBaseTypes = allEntries.map(Pair::getLeft);
		//
		{
			val duplicatesMap = allJavaBaseTypes
					.groupBy(JavaBaseType::resolveClass)
					.filter(tuple -> tuple._2().size() > 1);
			//
			if (!duplicatesMap.isEmpty()) {
				throw new IllegalStateException("Duplicates: %s".formatted(duplicatesMap));
			}
		}
		//
		entries = allEntries
				.map(pair -> Pair.of(
						pair.getLeft().withInheritedHints(computeInheritedHints(allJavaBaseTypes, pair.getLeft())),
						pair.getRight()))
				.toMap(
						pair -> ClassUtils.get(pair.getLeft().getSignature().getBinaryName()),
						Function.identity());
	}

	public JavaBaseType createJavaBaseType(Class<?> javaClass) {
		return createJavaBaseType(javaClass, true);
	}

	@Nullable
	public JavaBaseType findJavaBaseType(CodeObjectModelReference objectModelReference) {
		JavaBaseType result;
		{
			JavaBaseType javaBaseType = entries.values()
					.filter(pair -> Objects.equals(
							pair.getLeft().getObjectModelReference(),
							objectModelReference))
					.map(Pair::getLeft)
					.getOrNull();
			//
			if (javaBaseType != null) {
				result = javaBaseType;
			} else {
				SignatureJavaBaseTypeReference javaBaseTypeReference = JavaBaseTypeUtils.findJavaBaseTypeReference(
						objectModelReference);
				//
				if (javaBaseTypeReference != null) {
					result = createJavaBaseType(javaBaseTypeReference.resolveClass());
				} else {
					result = null;
				}
			}
		}
		return result;
	}

	@Nullable
	public JavaBaseType findJavaBaseType(ObjectModel objectModel) {
		return findJavaBaseType(CodeObjectModelReference.from(objectModel));
	}

	@Deprecated // See reworked class.
	public JavaBaseType getJavaBaseType(Class<?> javaClass) {
		return entries.get(javaClass).map(Pair::getLeft).getOrElse(() -> createJavaBaseType(javaClass));
	}

	public JavaBaseType getJavaBaseType(CodeObjectModelReference objectModelReference) {
		JavaBaseType result = findJavaBaseType(objectModelReference);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: %s".formatted(objectModelReference));
		}
		//
		return result;
	}

	public List<JavaBaseType> getJavaBaseTypeEntries() {
		return entries.values().map(Pair::getLeft).toList();
	}

	@Deprecated
	public CodeObjectModelReference getNormalizedObjectModelReference(Class<?> javaClass) {
		return getNormalizedObjectModelReference(SignatureJavaBaseTypeReference.from(javaClass));
	}

	@Deprecated
	public CodeObjectModelReference getNormalizedObjectModelReference(
			SignatureJavaBaseTypeReference javaBaseTypeReference) {
		CodeObjectModelReference result;
		{
			// TODO: Think about dynamic JavaBaseType entry addition.
			JavaBaseType javaBaseType = entries
					.find(entry -> Objects.equals(
							entry._2().getLeft().getSignature(),
							javaBaseTypeReference.getSignature()))
					.map(Tuple2::_2)
					.map(Pair::getLeft)
					.getOrNull();
			//
			if (javaBaseType != null) {
				if (javaBaseType.getObjectModelReference() != null) {
					result = javaBaseType.getObjectModelReference();
				} else {
					// Entry found but has no modelCode specified.
					//
					result = JavaBaseTypeUtils.getJavaBaseTypeObjectModelReference(javaBaseTypeReference);
				}
			} else {
				// No entry found.
				//
				result = JavaBaseTypeUtils.getJavaBaseTypeObjectModelReference(javaBaseTypeReference);
			}
		}
		return result;
	}

	public List<ObjectModel> getObjectModelEntries() {
		return entries.values().map(Pair::getRight).filter(Objects::nonNull).toList();
	}

	public static List<JavaBaseTypeHint> computeInheritedHints(
			Traversable<JavaBaseType> registeredTypes,
			JavaBaseType javaBaseType) {
		Class<?> javaClass = javaBaseType.resolveClass();
		Traversable<JavaBaseType> superTypes = registeredTypes
				.filter(registeredType -> {
					Class<?> registeredClass = registeredType.resolveClass();
					return registeredClass.isAssignableFrom(javaClass) && registeredClass != javaClass;
				});
		//
		return List
				.of(
						superTypes.exists(superType -> superType.hasHint(JavaBaseTypeHint.LIST))
								? List.of(JavaBaseTypeHint.LIST)
								: null,
						//
						superTypes.exists(superType -> superType.hasHint(JavaBaseTypeHint.REFERENCE))
								? List.of(JavaBaseTypeHint.REFERENCE)
								: null,
						superTypes.exists(superType -> superType.hasHint(JavaBaseTypeHint.SIMPLE))
								? List.of(JavaBaseTypeHint.SIMPLE)
								: null)
				.filter(Objects::nonNull)
				.flatMap(Function.identity())
				.filter(hint -> !javaBaseType.getImplicitHints().contains(hint));
	}

	public static Boolean resolveHasCustomModel(Class<?> javaClass) {
		MergedAnnotation<Model> modelMergedAnnotation = MergedAnnotations.from(javaClass).get(Model.class);
		//
		return modelMergedAnnotation.isPresent() && modelMergedAnnotation.synthesize().hasCustomModel();
	}
}
