package tk.labyrinth.pandora.datatypes.javabasetype.contrib;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.registry.DefaultJavaBaseTypeContributor;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.registry.JavaBaseTypeContributor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

@Bean
public class VavrMapJavaBaseTypeContributor implements JavaBaseTypeContributor {

	@Override
	public List<Pair<JavaBaseType, @Nullable ObjectModel>> contributeJavaBaseTypes() {
		String modelCode = ReflectionObjectModelFactory.resolveModelCode(Map.class);
		//
		return List.of(Pair.of(
				DefaultJavaBaseTypeContributor.createObject(Map.class),
				ObjectModel.builder()
						.attributes(List.empty())
						.code(modelCode)
						.name(Map.class.getSimpleName())
						.parameterNames(List.of("K", "V"))
						.renderRule(null) // TODO
						.tags(List.empty())
						.build()));
	}
}
