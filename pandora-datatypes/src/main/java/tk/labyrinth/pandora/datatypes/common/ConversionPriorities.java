package tk.labyrinth.pandora.datatypes.common;

public class ConversionPriorities {

	public static final int GENERIC_OBJECT_PRIORITY = Integer.MAX_VALUE - 100;

	/**
	 * We need From*Wrapper converter to fire before ToGenericObject.
	 */
	public static final int VALUE_WRAPPER_PRIORITY = GENERIC_OBJECT_PRIORITY - 1;
}
