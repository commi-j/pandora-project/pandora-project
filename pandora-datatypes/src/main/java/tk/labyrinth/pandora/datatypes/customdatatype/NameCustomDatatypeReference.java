package tk.labyrinth.pandora.datatypes.customdatatype;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class NameCustomDatatypeReference implements Reference<CustomDatatype> {

	String name;

	public GenericReference<CustomDatatype> toGenericReference() {
		return GenericReference.of(
				CustomDatatype.MODEL_CODE,
				List.of(GenericReferenceAttribute.of(CustomDatatype.NAME_ATTRIBUTE_NAME, name)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s)".formatted(
				CustomDatatype.MODEL_CODE,
				CustomDatatype.NAME_ATTRIBUTE_NAME,
				name);
	}

	public static NameCustomDatatypeReference from(GenericReference<?> reference) {
		if (!isNameCustomDatatypeReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(reference.getAttributeValue(CustomDatatype.NAME_ATTRIBUTE_NAME));
	}

	public static NameCustomDatatypeReference from(CustomDatatype value) {
		return of(value.getName());
	}

	// FIXME: Must be generic in ReferenceOMConfigurer
	@JsonCreator
	public static NameCustomDatatypeReference from(String value) {
		return from(GenericReference.from(value));
	}

	public static String getName(GenericReference<CustomDatatype> nameCustomDatatypeReference) {
		return nameCustomDatatypeReference.getAttributeValue(CustomDatatype.NAME_ATTRIBUTE_NAME);
	}

	public static boolean isNameCustomDatatypeReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), CustomDatatype.MODEL_CODE) &&
				reference.hasAttribute(CustomDatatype.NAME_ATTRIBUTE_NAME);
	}
}
