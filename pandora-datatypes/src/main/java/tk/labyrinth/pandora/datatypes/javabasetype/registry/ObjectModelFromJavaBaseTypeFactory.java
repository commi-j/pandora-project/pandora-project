package tk.labyrinth.pandora.datatypes.javabasetype.registry;

import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;

public interface ObjectModelFromJavaBaseTypeFactory {

	ObjectModel createObjectModelFromJavaBaseType(JavaBaseType javaBaseType);
}
