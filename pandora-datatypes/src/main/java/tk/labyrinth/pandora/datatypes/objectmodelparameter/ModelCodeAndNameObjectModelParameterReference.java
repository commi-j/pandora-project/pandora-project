package tk.labyrinth.pandora.datatypes.objectmodelparameter;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReferenceAttribute;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;

import java.util.Objects;

@Value(staticConstructor = "of")
public class ModelCodeAndNameObjectModelParameterReference implements Reference<ObjectModelParameter> {

	@NonNull
	String modelCode;

	@NonNull
	String name;

	public String toDatatypeAlias() {
		return ObjectModelParameterUtils.createDatatypeAlias(this);
	}

	public GenericReference<ObjectModelParameter> toGenericReference() {
		return GenericReference.of(
				ObjectModelParameter.MODEL_CODE,
				List.of(
						GenericReferenceAttribute.of(ObjectModelParameter.MODEL_CODE_ATTRIBUTE_NAME, modelCode),
						GenericReferenceAttribute.of(ObjectModelParameter.NAME_ATTRIBUTE_NAME, name)));
	}

	@Override
	public String toString() {
		return "%s(%s=%s,%s=%s)".formatted(
				ObjectModelParameter.MODEL_CODE,
				ObjectModelParameter.MODEL_CODE_ATTRIBUTE_NAME,
				modelCode,
				ObjectModelParameter.NAME_ATTRIBUTE_NAME,
				name);
	}

	public static ModelCodeAndNameObjectModelParameterReference from(ObjectModelParameter object) {
		return ModelCodeAndNameObjectModelParameterReference.of(object.getModelCode(), object.getName());
	}

	public static ModelCodeAndNameObjectModelParameterReference from(GenericReference<?> reference) {
		if (!isModelCodeAndIndexObjectModelParameterReference(reference)) {
			throw new IllegalArgumentException(reference.toString());
		}
		return of(
				reference.getAttributeValue(ObjectModelParameter.MODEL_CODE_ATTRIBUTE_NAME),
				reference.getAttributeValue(ObjectModelParameter.NAME_ATTRIBUTE_NAME));
	}

	@JsonCreator
	public static ModelCodeAndNameObjectModelParameterReference from(String value) {
		GenericReference<?> genericReference = GenericReference.from(value);
		//
		return from(genericReference);
	}

	public static boolean isModelCodeAndIndexObjectModelParameterReference(GenericReference<?> reference) {
		return Objects.equals(reference.getModelCode(), ObjectModelParameter.MODEL_CODE) &&
				reference.hasAttribute(ObjectModelParameter.MODEL_CODE_ATTRIBUTE_NAME) &&
				reference.hasAttribute(ObjectModelParameter.NAME_ATTRIBUTE_NAME);
	}
}
