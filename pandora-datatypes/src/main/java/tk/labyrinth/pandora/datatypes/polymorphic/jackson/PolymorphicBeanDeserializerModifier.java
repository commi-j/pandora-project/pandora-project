package tk.labyrinth.pandora.datatypes.polymorphic.jackson;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.deser.impl.TypeWrappedDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import io.vavr.collection.Set;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicRoot;
import tk.labyrinth.pandora.datatypes.polymorphic.registry.PolymorphicClassRegistry;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@LazyComponent
@RequiredArgsConstructor
public class PolymorphicBeanDeserializerModifier extends BeanDeserializerModifier {

	private final PolymorphicClassRegistry polymorphicClassRegistry;

	@Override
	public JsonDeserializer<?> modifyDeserializer(
			DeserializationConfig config,
			BeanDescription beanDesc,
			JsonDeserializer<?> deserializer) {
		JsonDeserializer<?> result;
		{
			Class<?> beanClass = beanDesc.getBeanClass();
			//
			// FIXME: Check ObjectModel and not Class.
			if (beanClass.isAnnotationPresent(PolymorphicRoot.class)) {
//				GenericObjectAttribute qualifierAttribute = PolymorphicUtils.resolveQualifierAttribute(beanClass);
				//
				result = new TypeWrappedDeserializer(
						new PolymorphicTypeDeserializer(polymorphicClassRegistry.getLinks(beanClass)),
						deserializer);
			} else {
				result = deserializer;
			}
		}
		return result;
	}

	// TODO: Find a better name for it.
	@RequiredArgsConstructor
	public static class PolymorphicTypeDeserializer extends TypeDeserializer {

		@NonNull
		private final Set<PolymorphicClassRegistry.PolymorphicLink> links;

		@Override
		public Object deserializeTypedFromAny(JsonParser p, DeserializationContext ctxt) throws IOException {
			// TODO: Implement.
			throw new NotImplementedException();
		}

		@Override
		public Object deserializeTypedFromArray(JsonParser p, DeserializationContext ctxt) throws IOException {
			// TODO: Implement.
			throw new NotImplementedException();
		}

		@Override
		public Object deserializeTypedFromObject(JsonParser p, DeserializationContext ctxt) throws IOException {
			Object result;
			{
				ObjectNode objectNode = p.readValueAs(ObjectNode.class);
				//
				Set<PolymorphicClassRegistry.PolymorphicLink> matchedLinks = links.filter(link -> Objects.equals(
						Optional.ofNullable(objectNode.get(link.getQualifierAttribute().getName()))
								.map(JsonNode::textValue)
								.orElse(null),
						link.getQualifierAttribute().getValue().toString()));
				//
				if (matchedLinks.size() != 1) {
					// TODO: if 0 then we may return a generic proxy of required interface.
					// TODO: Actually for >1 too, not sure about that.
					//
					throw new IllegalArgumentException();
				}
				//
				JsonDeserializer<Object> jsonDeserializer = ctxt.findNonContextualValueDeserializer(ctxt.constructType(
						matchedLinks.single().getLeafClass()));
				//
				TreeTraversingParser treeTraversingParser = new TreeTraversingParser(objectNode, p.getCodec());
				treeTraversingParser.nextToken();
				//
				result = jsonDeserializer.deserialize(treeTraversingParser, ctxt);
			}
			return result;
		}

		@Override
		public Object deserializeTypedFromScalar(JsonParser p, DeserializationContext ctxt) throws IOException {
			// TODO: Implement.
			throw new NotImplementedException();
		}

		@Override
		public TypeDeserializer forProperty(BeanProperty prop) {
			// TODO: Implement.
			throw new NotImplementedException();
		}

		@Override
		public Class<?> getDefaultImpl() {
			// TODO: Implement.
			throw new NotImplementedException();
		}

		@Override
		public String getPropertyName() {
			// TODO: Implement.
			throw new NotImplementedException();
		}

		@Override
		public TypeIdResolver getTypeIdResolver() {
			// TODO: Implement.
			throw new NotImplementedException();
		}

		@Override
		public JsonTypeInfo.As getTypeInclusion() {
			// TODO: Implement.
			throw new NotImplementedException();
		}
	}

	@RequiredArgsConstructor
	public static class PolymorphicTypeIdResolver extends TypeIdResolverBase {

		private final String value;

		@Override
		public JsonTypeInfo.Id getMechanism() {
			return JsonTypeInfo.Id.CUSTOM;
		}

		@Override
		public String idFromValue(Object value) {
			return this.value;
		}

		@Override
		public String idFromValueAndType(Object value, Class<?> suggestedType) {
			// TODO: Implement.
			throw new NotImplementedException();
		}
	}
}
