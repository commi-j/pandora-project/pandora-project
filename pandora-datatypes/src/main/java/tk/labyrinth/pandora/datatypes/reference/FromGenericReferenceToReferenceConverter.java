package tk.labyrinth.pandora.datatypes.reference;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;
import tk.labyrinth.pandora.datatypes.domain.genericreference.GenericReference;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeHint;
import tk.labyrinth.pandora.datatypes.javabasetype.registry.JavaBaseTypeRegistry;

import java.util.Set;

@Component
@RequiredArgsConstructor
@Slf4j
public class FromGenericReferenceToReferenceConverter implements ConditionalGenericConverter {

	private final ObjectProvider<JavaBaseTypeRegistry> javaBaseTypeRegistryProvider;

	private final ObjectProvider<ObjectMapper> objectMapperProvider;

	@PostConstruct
	private void postConstruct() {
		logger.debug("Initialized");
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		return objectMapperProvider.getObject().convertValue(source, targetType.getType());
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		return null;
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		boolean result;
		{
			if (sourceType.getType() == GenericReference.class) {
				result = JavaBaseTypeHint.hasHint(
						javaBaseTypeRegistryProvider.getObject().getJavaBaseType(targetType.getType()),
						JavaBaseTypeHint.REFERENCE);
			} else {
				result = false;
			}
		}
		return result;
	}
}
