package tk.labyrinth.pandora.datatypes.polymorphic.registry;

import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Set;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import tk.labyrinth.pandora.datatypes.domain.genericobject.GenericObjectAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicLeaf;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicUtils;
import tk.labyrinth.pandora.dependencies.spring.JavaElementDiscoverer;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ClassUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@LazyComponent
@RequiredArgsConstructor
public class PolymorphicClassRegistry {

	private final JavaElementDiscoverer javaElementDiscoverer;

	private final ConcurrentMap<Class<?>, Set<PolymorphicLink>> links = new ConcurrentHashMap<>();

	@PostConstruct
	private void postConstruct() {
		javaElementDiscoverer
				.discoverTypeSignatures(
						"tk.labyrinth",
						List.of(new AnnotationTypeFilter(PolymorphicLeaf.class, true, true)))
				.forEach(typeSignature -> {
					Class<?> leafClass = ClassUtils.get(typeSignature.getBinaryName());
					//
					PolymorphicLeaf polymorphicLeafAnnotation = leafClass.getAnnotation(PolymorphicLeaf.class);
					//
					Class<?> rootClass = polymorphicLeafAnnotation.rootClass();
					//
					PolymorphicLink link = PolymorphicLink.builder()
							.leafClass(leafClass)
							.qualifierAttribute(PolymorphicUtils.resolveQualifierAttribute(polymorphicLeafAnnotation))
							.rootClass(rootClass)
							.build();
					//
					links.compute(rootClass, (key, value) -> value != null ? value.add(link) : HashSet.of(link));
				});
		//
		links.forEach((rootClass, leafClasses) -> {
			// TODO: What do we want here?
		});
	}

	public Map<Class<?>, Set<PolymorphicLink>> getAllLinks() {
		return HashMap.ofAll(links);
	}

	public Set<PolymorphicLink> getLinks(Class<?> rootClass) {
		return links.getOrDefault(rootClass, HashSet.empty());
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class PolymorphicLink {

		Class<?> leafClass;

		GenericObjectAttribute qualifierAttribute;

		Class<?> rootClass;
	}
}
