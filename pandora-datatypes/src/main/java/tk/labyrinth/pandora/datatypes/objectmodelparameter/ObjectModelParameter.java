package tk.labyrinth.pandora.datatypes.objectmodelparameter;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.pandora.core.PandoraConstants;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.CodeObjectModelReference;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTagPandora;
import tk.labyrinth.pandora.datatypes.domain.reference.Reference;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;

@Builder(builderClassName = "Builder", toBuilder = true)
@Model(code = ObjectModelParameter.MODEL_CODE, hasCustomModel = true)
@ModelTagPandora
@ModelTag(PandoraConstants.TEMPORAL_MODEL_TAG_NAME)
@Value
@With
public class ObjectModelParameter implements ObjectIndex<ObjectModel> {

	public static final String MODEL_CODE = "objectmodelparameter";

	public static final String INDEX_ATTRIBUTE_NAME = "index";

	public static final String MODEL_CODE_ATTRIBUTE_NAME = "modelCode";

	public static final String NAME_ATTRIBUTE_NAME = "name";

	/**
	 * Non-negative.
	 */
	Integer index;

	/**
	 * Non-null.
	 */
	String modelCode;

	/**
	 * Non-null.
	 */
	String name;

	@Override
	public Reference<ObjectModel> getTargetReference() {
		return CodeObjectModelReference.of(modelCode);
	}
}
