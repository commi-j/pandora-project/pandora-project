package tk.labyrinth.pandora.datatypes.objectmodel.factory;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import org.jibx.schema.codegen.extend.DefaultNameConverter;
import org.jibx.schema.codegen.extend.NameConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttributeFeaturesContributor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.feature.ObjectModelFeaturesContributor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.Model;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.meta.ModelTag;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicConstants;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicRoot;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderAttribute;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderFunction;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.render.RenderPattern;
import tk.labyrinth.pandora.datatypes.domain.tag.Tag;
import tk.labyrinth.pandora.datatypes.objectindex.ObjectIndex;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.lang.StringUtils;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Objects;

@LazyComponent
@RequiredArgsConstructor
public class ReflectionObjectModelFactory {

	private static final NameConverter nameConverter = new DefaultNameConverter();

	@Autowired(required = false)
	private final java.util.List<ObjectModelAttributeFeaturesContributor> attributeFeaturesContributors;

	@Autowired(required = false)
	private final java.util.List<ObjectModelFeaturesContributor> modelFeaturesContributors;

	public ObjectModel createModelFromJavaClass(Class<?> javaClass) {
		String modelCode = resolveModelCode(javaClass);
		//
		return ObjectModel.builder()
				.attributes(resolveAttributes(javaClass))
				.code(modelCode)
				.features(List.ofAll(modelFeaturesContributors)
						.flatMap(featuresContributor -> featuresContributor.contributeFeatures(javaClass)))
				.name(resolveName(javaClass))
				.parameterNames(javaClass.getTypeParameters().length > 0
						? List.of(javaClass.getTypeParameters()).map(TypeVariable::getName)
						: null)
				.renderRule(resolveRenderRule(javaClass))
				.tags(resolveTags(javaClass))
				.build();
	}

	public String getAttributeName(Method method) {
		String result = ObjectModelUtils.findAttributeName(method);
		if (result == null) {
			throw new IllegalArgumentException("Not found: method = %s".formatted(method));
		}
		return result;
	}

	public TypeDescription getTypeDescriptionOf(Member attributeMember) {
		return TypeDescription.of(getTypeOf(attributeMember));
	}

	public Type getTypeOf(Member member) {
		Type result;
		{
			if (member instanceof Field field) {
				result = field.getGenericType();
			} else if (member instanceof Method method) {
				result = method.getGenericReturnType();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public List<ObjectModelAttribute> resolveAttributes(Class<?> javaClass) {
		List<Member> attributeMembers = selectAttributeMembers(javaClass);
		//
		return attributeMembers
				.map(attributeMember -> ObjectModelAttribute.builder()
						.datatype(JavaBaseTypeUtils.createDatatypeFromTypeDescription(getTypeDescriptionOf(attributeMember)))
						.features(List.ofAll(attributeFeaturesContributors)
								.flatMap(featuresContributor -> featuresContributor.contributeFeatures(attributeMember)))
						.name(ObjectModelUtils.getAttributeName(attributeMember))
						.build())
				.sortBy(ObjectModelAttribute::getName);
	}

	public String resolveName(Class<?> javaClass) {
		return Option.of(MergedAnnotations.from(javaClass).get(Model.class))
				.filter(MergedAnnotation::isPresent)
				.map(modelAnnotation -> modelAnnotation.getString("name"))
				.filter(modelName -> !modelName.isEmpty())
				.getOrElse(javaClass::getSimpleName);
	}

	public String resolveRenderRule(Class<?> javaClass) {
		String result;
		{
			MergedAnnotations mergedAnnotations = MergedAnnotations.from(javaClass);
			//
			if (mergedAnnotations.get(RenderAttribute.class).isPresent()) {
				result = "attribute:" + Stream
						.of(mergedAnnotations.get(RenderAttribute.class).getStringArray("value"))
						.mkString(",");
			} else if (mergedAnnotations.get(RenderFunction.class).isPresent()) {
				result = "function:" + mergedAnnotations.get(RenderFunction.class).getString("value");
			} else if (mergedAnnotations.get(RenderPattern.class).isPresent()) {
				result = "pattern:" + mergedAnnotations.get(RenderPattern.class).getString("value");
			} else {
				result = null;
			}
		}
		return result;
	}

	public List<Member> selectAttributeMembers(Class<?> javaClass) {
		List<Member> result;
		{
			if (javaClass.isInterface()) {
				// Selecting methods where 'getX' -> 'x'.
				//
				result = Stream.of(javaClass.getDeclaredMethods())
						.filter(declaredMethod -> !Modifier.isStatic(declaredMethod.getModifiers()))
						.filter(declaredMethod -> !declaredMethod.isDefault())
						.filter(declaredMethod -> ObjectModelUtils.findAttributeName(declaredMethod) != null)
						.map(Member.class::cast)
						.toList();
			} else {
				// Selecting fields.
				//
				result = Stream.of(javaClass.getDeclaredFields())
						.filter(declaredField -> !Modifier.isStatic(declaredField.getModifiers()))
						.map(Member.class::cast)
						.toList();
			}
		}
		return result;
	}

	public static String resolveModelCode(Class<?> javaClass) {
		String result;
		{
			MergedAnnotation<Model> modelMergedAnnotation = MergedAnnotations.from(javaClass).get(Model.class);
			//
			if (modelMergedAnnotation.isPresent()) {
				String codeValue = modelMergedAnnotation.getString("value");
				result = !codeValue.isEmpty() ? codeValue : javaClass.getSimpleName().toLowerCase();
			} else {
				result = JavaBaseTypeUtils.createJavaBaseTypeObjectModelCode(javaClass);
			}
		}
		return result;
	}

	// TODO: Should be tag contributors.
	public static List<Tag> resolveSpecialTags(Class<?> javaClass, MergedAnnotations mergedAnnotations) {
		return List
				.of(
						ObjectIndex.class.isAssignableFrom(javaClass)
								? ObjectIndex.INDEX_MODEL_TAG
								: null,
						mergedAnnotations.get(PolymorphicRoot.class).isPresent() // FIXME: Are we sure we need this?
								? Tag.of(PolymorphicConstants.TAG_NAME)
								: null)
				.filter(Objects::nonNull);
	}

	public static List<Tag> resolveTags(Class<?> javaClass) {
		List<Tag> result;
		{
			MergedAnnotations mergedAnnotations = MergedAnnotations.from(javaClass);
			//
			result = List
					.ofAll(mergedAnnotations.stream(ModelTag.class)).map(modelTagMergedAnnotation -> Tag.of(
							StringUtils.emptyToNull(modelTagMergedAnnotation.getString("tagName")),
							StringUtils.emptyToNull(modelTagMergedAnnotation.getString("tagValue"))))
					.appendAll(resolveSpecialTags(javaClass, mergedAnnotations));
		}
		return result;
	}
}
