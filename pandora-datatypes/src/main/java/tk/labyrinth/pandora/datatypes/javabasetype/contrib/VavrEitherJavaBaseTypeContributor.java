package tk.labyrinth.pandora.datatypes.javabasetype.contrib;

import io.vavr.collection.List;
import io.vavr.control.Either;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseType;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.registry.DefaultJavaBaseTypeContributor;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.registry.JavaBaseTypeContributor;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.datatypes.objectmodelparameter.ModelCodeAndNameObjectModelParameterReference;
import tk.labyrinth.pandora.diversity.domain.bean.Bean;

// FIXME: After we introduce parameters of JavaBaseTypes we can create proper ObjectModel based on JBT,
//  so we don't need to do it manually.
@Bean
public class VavrEitherJavaBaseTypeContributor implements JavaBaseTypeContributor {

	@Override
	public List<Pair<JavaBaseType, @Nullable ObjectModel>> contributeJavaBaseTypes() {
		String modelCode = ReflectionObjectModelFactory.resolveModelCode(Either.class);
		//
		return List.of(Pair.of(
				DefaultJavaBaseTypeContributor.createObject(Either.class),
				ObjectModel.builder()
						.attributes(List.of(
								ObjectModelAttribute.ofNameAndDatatypeAlias(
										"left",
										ModelCodeAndNameObjectModelParameterReference.of(modelCode, "L").toDatatypeAlias()),
								ObjectModelAttribute.ofNameAndDatatypeAlias(
										"right",
										ModelCodeAndNameObjectModelParameterReference.of(modelCode, "R").toDatatypeAlias())))
						.code(modelCode)
						.name(Either.class.getSimpleName())
						.parameterNames(List.of("L", "R"))
						.renderRule("${left?l: $}${right?r: $}") // TODO: Test it.
						.tags(List.empty())
						.build()));
	}
}
