package tk.labyrinth.pandora.datatypes.filterer;

import io.vavr.collection.List;
import io.vavr.collection.Traversable;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import tk.labyrinth.expresso.query.domain.java.JavaContext;
import tk.labyrinth.expresso.query.domain.java.JavaQueryExecutor;
import tk.labyrinth.expresso.query.lang.search.PlainQuery;

@RequiredArgsConstructor
public class SpecificFilterer<T> {

	private final Class<T> javaClass;

	private final JavaQueryExecutor queryExecutor;

	public long count(Traversable<T> objects, PlainQuery query) {
		return queryExecutor.count(new TraversableJavaContext(objects), query.withParameter(javaClass));
	}

	public List<T> filter(Traversable<T> objects, PlainQuery query) {
		return queryExecutor
				.search(new TraversableJavaContext(objects), query.withParameter(javaClass))
				.stream()
				.collect(List.collector());
	}

	@Value
	public static class TraversableJavaContext implements JavaContext {

		Traversable<?> traversable;

		@Override
		@SuppressWarnings("unchecked")
		public <T> java.util.stream.Stream<T> getStream(Class<T> type) {
			return (java.util.stream.Stream<T>) traversable.toJavaStream();
		}
	}
}
