package tk.labyrinth.pandora.datatypes.polymorphic.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

// https://stackoverflow.com/questions/6834677/jackson-mapper-post-construct
@LazyComponent
@RequiredArgsConstructor
public class PolymorphicObjectMapperConfigurer implements ObjectMapperConfigurer {

	private final PolymorphicBeanDeserializerModifier polymorphicBeanDeserializerModifier;

	private final PolymorphicBeanSerializerModifier polymorphicBeanSerializerModifier;

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				module.setDeserializerModifier(polymorphicBeanDeserializerModifier);
				module.setSerializerModifier(polymorphicBeanSerializerModifier);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}
}
