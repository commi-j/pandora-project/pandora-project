package tk.labyrinth.pandora.datatypes.simple.access;

import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;

public enum PandoraAccessLevel {
	EDITABLE,
	HIDDEN,
	LISTABLE,
	REFERABLE,
	VIEWABLE;

	public boolean canEdit() {
		return this == EDITABLE;
	}

	public boolean canList() {
		return !ObjectUtils.in(this, HIDDEN, REFERABLE);
	}

	public boolean canRefer() {
		return this != HIDDEN;
	}

	public boolean canView() {
		return ObjectUtils.in(this, EDITABLE, VIEWABLE);
	}

	public static boolean canEdit(@Nullable PandoraAccessLevel accessLevel) {
		return accessLevel == null || accessLevel.canEdit();
	}

	public static boolean canView(@Nullable PandoraAccessLevel accessLevel) {
		return accessLevel == null || accessLevel.canView();
	}
}
