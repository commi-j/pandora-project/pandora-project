package tk.labyrinth.pandora.telemetry.aspect;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

class CountInvocationsAspectTest {

	/**
	 * Ensures aspect works only around execution, not invocation of a method, so that we don't have duplicity.
	 */
	@Test
	void testAspectedMethodInvokingOtherAspectedMethod() throws ReflectiveOperationException {
		Dummy dummy = Dummy.class.getConstructor().newInstance();
		dummy.inner();
		dummy.outer();
		dummy.test();
		//
		Assertions.assertEquals(
				Map.of(
						Dummy.class.getMethod("inner"), 2L,
						Dummy.class.getMethod("outer"), 1L,
						Dummy.class.getMethod("test"), 1L),
				CountInvocationsAspect.COUNT_MAP);
	}

	public static class Dummy {

		@CountInvocations
		public void inner() {
			// no-op
		}

		@CountInvocations
		public void outer() {
			inner();
		}

		@CountInvocations
		public void test() {
			// no-op
		}
	}
}