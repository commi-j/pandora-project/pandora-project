package tk.labyrinth.pandora.telemetry.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Aspect
public class CountInvocationsAspect {

	public static final ConcurrentMap<Method, Long> COUNT_MAP = new ConcurrentHashMap<>();

	@Around("@annotation(tk.labyrinth.pandora.telemetry.aspect.CountInvocations) && execution(* *(..))")
	public Object aroundCountInvocations(ProceedingJoinPoint joinPoint) throws Throwable {
		COUNT_MAP.compute(
				((MethodSignature) joinPoint.getSignature()).getMethod(),
				(key, value) -> value != null ? value + 1 : 1);
		//
		return joinPoint.proceed();
	}
}
