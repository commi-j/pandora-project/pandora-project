package tk.labyrinth.pandora.telemetry.milestone.attribute;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.instrumentation.annotations.SpanAttribute;
import io.vavr.collection.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperFactory;

import java.util.function.Function;

class SpanAttributeMilestoneAttributeContributorTest {

	private final ObjectMapper objectMapper = ObjectMapperFactory.defaultConfigured();

	@Disabled // TODO
	@Test
	void testCreateExtractArgumentsFunctionWithJackson() throws NoSuchMethodException {
		Function<List<Object>, Attributes> extractArgumentsFunction =
				SpanAttributeMilestoneAttributeContributor.createExtractArgumentsFunction(
						objectMapper,
						SpanAttributeMilestoneAttributeContributorTest.class.getDeclaredMethod(
								"foo",
								String.class,
								java.util.List.class));
		//
		Assertions.assertEquals(
				Attributes.of(
						AttributeKey.stringKey("arg0"), "\"string\"",
						AttributeKey.stringKey("2nd"), "[1,2]"),
				extractArgumentsFunction.apply(List.of("string", java.util.List.of(1, 2))));
	}

	@Test
	void testCreateExtractArgumentsFunctionWithToString() throws NoSuchMethodException {
		Function<List<Object>, Attributes> extractArgumentsFunction =
				SpanAttributeMilestoneAttributeContributor.createExtractArgumentsFunction(
						objectMapper,
						SpanAttributeMilestoneAttributeContributorTest.class.getDeclaredMethod(
								"foo",
								String.class,
								java.util.List.class));
		//
		Assertions.assertEquals(
				Attributes.of(
						AttributeKey.stringKey("arg0"), "string",
						AttributeKey.stringKey("2nd"), "[1, 2]"),
				extractArgumentsFunction.apply(List.of("string", java.util.List.of(1, 2))));
	}

	public static String foo(@SpanAttribute String first, @SpanAttribute("2nd") java.util.List<Integer> second) {
		throw new UnsupportedOperationException();
	}
}