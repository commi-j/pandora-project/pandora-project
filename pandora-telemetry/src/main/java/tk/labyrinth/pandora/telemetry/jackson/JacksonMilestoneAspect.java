package tk.labyrinth.pandora.telemetry.jackson;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;
import tk.labyrinth.pandora.telemetry.milestone.MilestoneHandler;
import tk.labyrinth.pandora.telemetry.milestone.SimpleMilestoneHandler;
import tk.labyrinth.pandora.telemetry.spring.SpringMilestoneAspect;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Aspect
public class JacksonMilestoneAspect {

	private static final Thread initThread = Thread.currentThread();

	private static MilestoneHandler milestoneHandler = new SimpleMilestoneHandler();

	@Around("execution(* com.fasterxml.jackson.databind.ObjectMapper.convertValue(..))")
	public Object aroundAbstractApplicationContextRefresh(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	public static void setMilestoneHandler(MilestoneHandler milestoneHandler, boolean checkThreadSafety) {
		if (checkThreadSafety) {
			if (Thread.currentThread() != initThread) {
				throw new IllegalArgumentException(
						"Attempt to set MilestoneHandler from different Thread: currentThread = %s, initThread = %s"
								.formatted(Thread.currentThread().getName(), initThread.getName()));
			}
		}
		//
		JacksonMilestoneAspect.milestoneHandler = milestoneHandler;
		LoggerFactory.getLogger(SpringMilestoneAspect.class).info("MilestoneHandler set to {}", milestoneHandler);
	}
}
