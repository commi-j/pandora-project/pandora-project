package tk.labyrinth.pandora.telemetry.opentelemetry;

import io.opentelemetry.context.Context;
import io.opentelemetry.sdk.trace.ReadWriteSpan;
import io.opentelemetry.sdk.trace.ReadableSpan;
import io.opentelemetry.sdk.trace.SpanProcessor;

public class ContextStepInSpanProcessor implements SpanProcessor {

	public static ContextStepInSpanProcessor INSTANCE = new ContextStepInSpanProcessor();

	@Override
	public boolean isEndRequired() {
		return false;
	}

	@Override
	public boolean isStartRequired() {
		return true;
	}

	@Override
	public void onEnd(ReadableSpan span) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onStart(Context parentContext, ReadWriteSpan span) {
		span.makeCurrent();
	}
}
