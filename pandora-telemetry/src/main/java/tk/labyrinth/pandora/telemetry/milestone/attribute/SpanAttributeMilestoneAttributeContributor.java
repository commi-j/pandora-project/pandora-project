package tk.labyrinth.pandora.telemetry.milestone.attribute;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.common.AttributesBuilder;
import io.opentelemetry.instrumentation.annotations.SpanAttribute;
import io.vavr.Tuple3;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class SpanAttributeMilestoneAttributeContributor implements MilestoneAttributeContributor {

	private final ConcurrentMap<Method, Function<List<Object>, Attributes>> extractArgumentsFunctions =
			new ConcurrentHashMap<>();

	private final ObjectMapper objectMapper;

	@Override
	public Attributes contributeAttributes(Method method, @Nullable Object target, List<Object> arguments) {
		return extractArgumentsFunctions
				.computeIfAbsent(method, key -> createExtractArgumentsFunction(objectMapper, key))
				.apply(arguments);
	}

	public static Function<List<Object>, Attributes> createExtractArgumentsFunction(
			ObjectMapper objectMapper,
			Method method) {
		Function<List<Object>, Attributes> result;
		{
			List<Tuple3<Parameter, Integer, AttributeKey<String>>> tuples = List.of(method.getParameters())
					.zipWithIndex()
					.map(tuple -> tuple.append(MergedAnnotations.from(tuple._1()).get(SpanAttribute.class)))
					.filter(tuple -> tuple._3().isPresent())
					.map(tuple -> tuple.map3(annotation -> AttributeKey.stringKey(Optional.of(annotation.getString("value"))
							.map(value -> !value.isEmpty() ? value : tuple._1().getName())
							.orElseThrow())));
			//
			if (!tuples.isEmpty()) {
				result = arguments -> {
					AttributesBuilder attributesBuilder = Attributes.builder();
					//
					tuples.forEach(tuple -> {
						// TODO: Think whether we want #toString() or Jackson to write values.
//						String value;
//						try {
//							value = objectMapper.writeValueAsString(arguments.get(tuple._2()));
//						} catch (JsonProcessingException ex) {
//							throw new RuntimeException(ex);
//						}
						String value = String.valueOf(arguments.get(tuple._2()));
						//
						attributesBuilder.put(tuple._3(), value);
					});
					//
					return attributesBuilder.build();
				};
			} else {
				result = arguments -> Attributes.empty();
			}
		}
		return result;
	}
}
