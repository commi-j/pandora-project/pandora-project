package tk.labyrinth.pandora.telemetry.tracing.span;

import io.opentelemetry.sdk.trace.data.SpanData;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;

@Value(staticConstructor = "of")
public class SpanDataWrapper {

	@NonNull
	List<SpanDataWrapper> children;

	@NonNull
	SpanData data;

	@Nullable
	public SpanDataWrapper find(String spanId) {
		return Objects.equals(data.getSpanId(), spanId)
				? this
				: children.map(child -> child.find(spanId)).filter(Objects::nonNull).getOrNull();
	}

	@Nullable
	public List<SpanDataWrapper> findChain(String spanId) {
		return Objects.equals(data.getSpanId(), spanId)
				? List.of(this)
				: children
				.map(child -> child.findChain(spanId))
				.filter(Objects::nonNull)
				.map(foundChain -> foundChain.prepend(this))
				.getOrNull();
	}
}
