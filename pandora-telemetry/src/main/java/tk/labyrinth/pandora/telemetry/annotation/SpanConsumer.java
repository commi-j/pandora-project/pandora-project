package tk.labyrinth.pandora.telemetry.annotation;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.instrumentation.annotations.WithSpan;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Consumer;

/**
 * This annotation helps you to obtain instance of {@link Span} created for method execution. To do so:<br>
 * - Have method annotated with @{@link WithSpan};<br>
 * - Have parameter with type {@link Consumer}&lt;{@link Span}&gt; annotated with @{@link SpanConsumer};<br>
 * - Supply non-null argument when invoking this method;<br>
 * If null is supplied, nothing happens. If {@link RuntimeException} is thrown from {@link Consumer#accept(Object)},
 * it is logged with error level.<br>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface SpanConsumer {
	// no-op
}
