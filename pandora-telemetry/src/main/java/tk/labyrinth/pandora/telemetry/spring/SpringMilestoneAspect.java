package tk.labyrinth.pandora.telemetry.spring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;
import tk.labyrinth.pandora.telemetry.milestone.MilestoneHandler;
import tk.labyrinth.pandora.telemetry.milestone.SimpleMilestoneHandler;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Aspect
public class SpringMilestoneAspect {

	private static final Thread initThread = Thread.currentThread();

	private static MilestoneHandler milestoneHandler = new SimpleMilestoneHandler();

	@Around("execution(* org.springframework.context.support.AbstractApplicationContext.refresh())")
	public Object aroundAbstractApplicationContextRefresh(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("execution(* org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.initializeBean(String,Object,org.springframework.beans.factory.support.RootBeanDefinition))")
	public Object aroundAbstractAutowireCapableBeanFactoryInitializeBean(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("target(org.springframework.boot.ApplicationRunner) && execution(public void run(org.springframework.boot.ApplicationArguments))")
	public Object aroundApplicationRunnerRun(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("target(org.springframework.core.convert.converter.GenericConverter) && execution(public Object convert(Object,org.springframework.core.convert.TypeDescriptor,org.springframework.core.convert.TypeDescriptor))")
	public Object aroundGenericConverterConvert(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("@annotation(javax.annotation.PostConstruct) && execution(* *(..))")
	public Object aroundPostConstruct(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("execution(* org.springframework.boot.SpringApplication.callRunners(..))")
	public Object aroundSpringApplicationCallRunners(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("execution(* org.springframework.boot.SpringApplication.run(String...))")
	public Object aroundSpringApplicationRun(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	public static void setMilestoneHandler(MilestoneHandler milestoneHandler, boolean checkThreadSafety) {
		if (checkThreadSafety) {
			if (Thread.currentThread() != initThread) {
				throw new IllegalArgumentException(
						"Attempt to set MilestoneHandler from different Thread: currentThread = %s, initThread = %s"
								.formatted(Thread.currentThread().getName(), initThread.getName()));
			}
		}
		//
		SpringMilestoneAspect.milestoneHandler = milestoneHandler;
		LoggerFactory.getLogger(SpringMilestoneAspect.class).info("MilestoneHandler set to {}", milestoneHandler);
	}
}
