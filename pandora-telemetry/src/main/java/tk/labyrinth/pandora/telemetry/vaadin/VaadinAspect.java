package tk.labyrinth.pandora.telemetry.vaadin;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;
import tk.labyrinth.pandora.telemetry.milestone.MilestoneHandler;
import tk.labyrinth.pandora.telemetry.milestone.SimpleMilestoneHandler;

/**
 * @author Commitman
 * @version 1.0.0
 */
@Aspect
public class VaadinAspect {

	private static final Thread initThread = Thread.currentThread();

	private static MilestoneHandler milestoneHandler = new SimpleMilestoneHandler();

	@Around("execution(* com.vaadin.flow.data.provider.DataCommunicator.generateJson(..))")
	public Object aroundDataCommunicatorGenerateJson(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("execution(* com.vaadin.flow.data.provider.DataCommunicator.getJsonItems(..))")
	public Object aroundDataCommunicatorGetJsonItems(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	@Around("execution(* com.vaadin.flow.server.VaadinServlet.service(..))")
	public Object aroundVaadinServletService(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	public static void setMilestoneHandler(MilestoneHandler milestoneHandler, boolean checkThreadSafety) {
		if (checkThreadSafety) {
			if (Thread.currentThread() != initThread) {
				throw new IllegalArgumentException(
						"Attempt to set MilestoneHandler from different Thread: currentThread = %s, initThread = %s"
								.formatted(Thread.currentThread().getName(), initThread.getName()));
			}
		}
		//
		VaadinAspect.milestoneHandler = milestoneHandler;
		LoggerFactory.getLogger(VaadinAspect.class).info("MilestoneHandler set to {}", milestoneHandler);
	}
}
