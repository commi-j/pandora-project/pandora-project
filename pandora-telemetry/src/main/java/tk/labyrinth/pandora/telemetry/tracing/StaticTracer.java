package tk.labyrinth.pandora.telemetry.tracing;

import io.opentelemetry.api.trace.Span;
import tk.labyrinth.pandora.telemetry.StaticTelemetryProvider;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class StaticTracer {

	public static Void withinSpan(String spanName, Consumer<Span> function) {
		return withinSpan(
				spanName,
				span -> {
					function.accept(span);
					//
					return null;
				});
	}

	public static <T> T withinSpan(String spanName, Function<Span, T> function) {
		Span span = StaticTelemetryProvider.getTracer().spanBuilder(spanName).startSpan();
		//
		try {
			return function.apply(span);
		} finally {
			span.end();
		}
	}

	public static Void withinSpan(String spanName, Runnable function) {
		return withinSpan(
				spanName,
				spanContext -> {
					function.run();
					//
					return null;
				});
	}

	public static <T> T withinSpan(String spanName, Supplier<T> function) {
		return withinSpan(
				spanName,
				spanContext -> {
					return function.get();
				});
	}
}
