package tk.labyrinth.pandora.telemetry.tracing;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;
import tk.labyrinth.pandora.telemetry.milestone.MilestoneHandler;
import tk.labyrinth.pandora.telemetry.milestone.SimpleMilestoneHandler;

/**
 * @author Commitman
 * @version 1.0.0
 * @see WithSpan
 */
@Aspect
public class WithSpanAspect {

	private static final Thread initThread = Thread.currentThread();

	private static MilestoneHandler milestoneHandler = new SimpleMilestoneHandler();

	@Around("@annotation(io.opentelemetry.instrumentation.annotations.WithSpan) && execution(* *(..))")
	public Object aroundWithSpan(ProceedingJoinPoint joinPoint) throws Throwable {
		return milestoneHandler.handleMilestone(joinPoint);
	}

	public static void setMilestoneHandler(MilestoneHandler milestoneHandler, boolean checkThreadSafety) {
		if (checkThreadSafety) {
			if (Thread.currentThread() != initThread) {
				throw new IllegalArgumentException(
						"Attempt to set MilestoneHandler from different Thread: currentThread = %s, initThread = %s"
								.formatted(Thread.currentThread().getName(), initThread.getName()));
			}
		}
		//
		WithSpanAspect.milestoneHandler = milestoneHandler;
		LoggerFactory.getLogger(WithSpanAspect.class).info("MilestoneHandler set to {}", milestoneHandler);
	}
}
