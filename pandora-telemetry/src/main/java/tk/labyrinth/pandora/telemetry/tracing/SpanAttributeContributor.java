package tk.labyrinth.pandora.telemetry.tracing;

import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.lang.reflect.Method;
import java.util.List;

public interface SpanAttributeContributor {

	List<Pair<String, Object>> contributeAttributes(Method method, @Nullable Object target);
}
