package tk.labyrinth.pandora.telemetry.opentelemetry;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.SdkTracerProviderBuilder;
import io.opentelemetry.sdk.trace.SpanProcessor;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanCache;

import java.util.List;

public class OpentelemetryFactory {

	public static OpenTelemetry createBasic() {
		return OpenTelemetrySdk.builder()
				.setTracerProvider(createSdkTracerProvider(List.of(
						ContextStepInSpanProcessor.INSTANCE,
						new LocallyCachingSpanProcessor(SpanCache.GLOBAL),
						ContextStepOutSpanProcessor.INSTANCE)))
				.build();
	}

	public static SdkTracerProvider createSdkTracerProvider(List<SpanProcessor> spanProcessors) {
		SdkTracerProviderBuilder builder = SdkTracerProvider.builder();
		//
		spanProcessors.forEach(builder::addSpanProcessor);
		//
		return builder.build();
	}
}
