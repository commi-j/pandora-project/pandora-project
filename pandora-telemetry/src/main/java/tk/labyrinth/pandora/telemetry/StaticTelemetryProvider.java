package tk.labyrinth.pandora.telemetry;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.metrics.Meter;
import io.opentelemetry.api.trace.Tracer;
import io.vavr.Lazy;
import lombok.Setter;
import tk.labyrinth.pandora.telemetry.opentelemetry.OpentelemetryFactory;

public class StaticTelemetryProvider {

	@Setter
	private static Lazy<Meter> meterLazy;

	@Setter
	private static Lazy<Tracer> tracerLazy;

	static {
		OpenTelemetry openTelemetry = OpentelemetryFactory.createBasic();
		//
		meterLazy = Lazy.of(() -> openTelemetry.getMeter("pandora"));
		tracerLazy = Lazy.of(() -> openTelemetry.getTracer("pandora"));
	}

	public static Meter getMeter() {
		return meterLazy.get();
	}

	public static Tracer getTracer() {
		return tracerLazy.get();
	}
}
