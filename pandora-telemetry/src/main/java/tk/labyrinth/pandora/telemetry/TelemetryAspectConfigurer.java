package tk.labyrinth.pandora.telemetry;

import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import tk.labyrinth.pandora.telemetry.jackson.JacksonMilestoneAspect;
import tk.labyrinth.pandora.telemetry.milestone.SimpleMilestoneHandler;
import tk.labyrinth.pandora.telemetry.milestone.attribute.MilestoneAttributeContributor;
import tk.labyrinth.pandora.telemetry.spring.SpringMilestoneAspect;
import tk.labyrinth.pandora.telemetry.tracing.WithSpanAspect;
import tk.labyrinth.pandora.telemetry.vaadin.VaadinAspect;


@RequiredArgsConstructor
public class TelemetryAspectConfigurer {

	@Value("${pandora.telemetry.disableThreadSafetyCheck:false}")
	private boolean disableThreadSafetyCheck;

	@Autowired(required = false)
	private java.util.List<MilestoneAttributeContributor> milestoneAttributeContributors;

	@PostConstruct
	private void postConstruct() {
		SimpleMilestoneHandler simpleMilestoneHandler = new SimpleMilestoneHandler(
				milestoneAttributeContributors != null ? List.ofAll(milestoneAttributeContributors) : List.empty());
		//
		JacksonMilestoneAspect.setMilestoneHandler(simpleMilestoneHandler, !disableThreadSafetyCheck);
		SpringMilestoneAspect.setMilestoneHandler(simpleMilestoneHandler, !disableThreadSafetyCheck);
		WithSpanAspect.setMilestoneHandler(simpleMilestoneHandler, !disableThreadSafetyCheck);
		VaadinAspect.setMilestoneHandler(simpleMilestoneHandler, !disableThreadSafetyCheck);
	}
}
