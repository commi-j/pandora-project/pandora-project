package tk.labyrinth.pandora.telemetry.milestone.attribute;

import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Method;

@ConditionalOnProperty("pandora.telemetry.enableArgumentCountingMilestoneAttributeContributor")
@LazyComponent
public class ArgumentCountingMilestoneAttributeContributor implements MilestoneAttributeContributor {

	public static final AttributeKey<Long> ARGUMENT_COUNT_ATTRIBUTE_KEY = AttributeKey.longKey("argumentCount");

	@Override
	public Attributes contributeAttributes(Method method, @Nullable Object target, List<Object> arguments) {
		return Attributes.of(ARGUMENT_COUNT_ATTRIBUTE_KEY, (long) arguments.size());
	}
}
