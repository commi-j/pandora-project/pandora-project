package tk.labyrinth.pandora.telemetry.milestone;

import org.aspectj.lang.ProceedingJoinPoint;

public interface MilestoneHandler {

	Object handleMilestone(ProceedingJoinPoint joinPoint) throws Throwable;
}
