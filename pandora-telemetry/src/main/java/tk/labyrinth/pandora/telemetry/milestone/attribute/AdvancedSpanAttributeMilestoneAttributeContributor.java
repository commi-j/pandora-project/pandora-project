package tk.labyrinth.pandora.telemetry.milestone.attribute;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.common.AttributesBuilder;
import io.vavr.Tuple4;
import io.vavr.collection.List;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.core.annotation.MergedAnnotations;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.telemetry.annotation.AdvancedSpanAttribute;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
public class AdvancedSpanAttributeMilestoneAttributeContributor implements MilestoneAttributeContributor {

	private final ConcurrentMap<Method, Function<List<Object>, Attributes>> extractArgumentsFunctions =
			new ConcurrentHashMap<>();

	private final ObjectMapper objectMapper;

	@Override
	public Attributes contributeAttributes(Method method, @Nullable Object target, List<Object> arguments) {
		return extractArgumentsFunctions
				.computeIfAbsent(method, key -> createExtractArgumentsFunction(objectMapper, key))
				.apply(arguments);
	}

	public static Function<List<Object>, Attributes> createExtractArgumentsFunction(
			ObjectMapper objectMapper,
			Method method) {
		Function<List<Object>, Attributes> result;
		{
			List<Tuple4<Parameter, Integer, AttributeKey<String>, String>> tuples = List.of(method.getParameters())
					.zipWithIndex()
					.map(tuple -> tuple.append(MergedAnnotations.from(tuple._1()).get(AdvancedSpanAttribute.class)))
					.filter(tuple -> tuple._3().isPresent())
					.map(tuple -> tuple
							.map3(annotation -> AttributeKey.stringKey(Optional.of(annotation.getString("value"))
									.map(value -> !value.isEmpty() ? value : tuple._1().getName())
									.orElseThrow()))
							.append(Optional.of(tuple._3().getString("path"))
									.filter(path -> !path.isEmpty())
									.orElse(null)));
			//
			if (!tuples.isEmpty()) {
				result = arguments -> {
					AttributesBuilder attributesBuilder = Attributes.builder();
					//
					tuples.forEach(tuple -> {
						// TODO: Think whether we want #toString() or Jackson to write values.
//						String value;
//						try {
//							value = objectMapper.writeValueAsString(arguments.get(tuple._2()));
//						} catch (JsonProcessingException ex) {
//							throw new RuntimeException(ex);
//						}
						//
						Object argument = arguments.get(tuple._2());
						//
						String value = String.valueOf(tuple._4() != null && argument != null
								? objectMapper.convertValue(argument, ObjectNode.class).get(tuple._4()) // TODO: support chainged accessors.
								: argument);
						//
						attributesBuilder.put(tuple._3(), value);
					});
					//
					return attributesBuilder.build();
				};
			} else {
				result = arguments -> Attributes.empty();
			}
		}
		return result;
	}
}
