package tk.labyrinth.pandora.telemetry.opentelemetry;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.context.Context;
import io.opentelemetry.sdk.trace.ReadWriteSpan;
import io.opentelemetry.sdk.trace.ReadableSpan;
import io.opentelemetry.sdk.trace.SpanProcessor;

public class ContextStepOutSpanProcessor implements SpanProcessor {

	public static ContextStepOutSpanProcessor INSTANCE = new ContextStepOutSpanProcessor();

	@Override
	public boolean isEndRequired() {
		return true;
	}

	@Override
	public boolean isStartRequired() {
		return false;
	}

	@Override
	public void onEnd(ReadableSpan span) {
		Span.wrap(span.getParentSpanContext()).makeCurrent();
	}

	@Override
	public void onStart(Context parentContext, ReadWriteSpan span) {
		throw new UnsupportedOperationException();
	}
}
