package tk.labyrinth.pandora.telemetry.milestone;

import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.common.AttributesBuilder;
import io.opentelemetry.api.metrics.LongHistogram;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanBuilder;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.labyrinth.pandora.telemetry.StaticTelemetryProvider;
import tk.labyrinth.pandora.telemetry.milestone.attribute.MilestoneAttributeContributor;

import java.lang.reflect.Method;

// TODO: Fail fast on attribute collision and tests for that.
@RequiredArgsConstructor
public class SimpleMilestoneHandler implements MilestoneHandler {

	@NonNull
	private final List<MilestoneAttributeContributor> attributeContributors;

	public SimpleMilestoneHandler() {
		this(List.empty());
	}

	private Attributes contributeAttributes(Method method, @Nullable Object target, Object[] args) {
		Attributes result;
		{
			List<Object> arguments = List.of(args);
			//
			List<Attributes> attributeses = attributeContributors.map(attributeContributor ->
					attributeContributor.contributeAttributes(method, target, arguments));
			//
			if (!attributeses.isEmpty()) {
				if (attributeses.size() > 1) {
					AttributesBuilder attributesBuilder = Attributes.builder();
					//
					attributeses.forEach(attributesBuilder::putAll);
					//
					result = attributesBuilder.build();
				} else {
					result = attributeses.single();
				}
			} else {
				result = Attributes.empty();
			}
		}
		return result;
	}

	@Override
	public Object handleMilestone(ProceedingJoinPoint joinPoint) throws Throwable {
		Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
		Class<?> targetClass = joinPoint.getTarget().getClass();
		//
		Logger logger = LoggerFactory.getLogger(targetClass);
		String milestoneName = "%s#%s".formatted(targetClass.getName(), method.getName());
		//
		LongHistogram histogram = StaticTelemetryProvider.getMeter().histogramBuilder("milestone")
				.ofLongs()
				.setUnit("ms")
				.build();
		SpanBuilder spanBuilder = StaticTelemetryProvider.getTracer().spanBuilder(milestoneName)
				.setAttribute("thread", Thread.currentThread().getName());
		//
		Attributes tags = contributeAttributes(method, joinPoint.getTarget(), joinPoint.getArgs());
		//
		Timer.Sample sample = Timer.start();
		long startedAt = System.currentTimeMillis();
		Span span = spanBuilder
				.setAllAttributes(tags)
				.startSpan();
		//
		logger.debug("Starting execution: methodName = {}", method.getName());
		try {
			return joinPoint.proceed();
		} finally {
			logger.debug("Finished execution: methodName = {}", method.getName());
			//
			long finishedAt = System.currentTimeMillis();
			span.end();
			histogram.record(
					finishedAt - startedAt,
					Attributes.builder()
							.put("milestoneName", milestoneName)
							.putAll(tags)
							.build());
			sample.stop(Metrics.timer(
					"milestone",
					HashMap.ofAll(tags.asMap())
							.map(tuple -> Tag.of(tuple._1().getKey(), tuple._2().toString()))
							.prepend(Tag.of("milestoneName", milestoneName))));
		}
	}
}
