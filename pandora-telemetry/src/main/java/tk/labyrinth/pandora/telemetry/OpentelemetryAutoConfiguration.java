package tk.labyrinth.pandora.telemetry;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.SdkTracerProviderBuilder;
import io.opentelemetry.sdk.trace.SpanProcessor;
import io.opentelemetry.sdk.trace.export.SimpleSpanProcessor;
import io.opentelemetry.sdk.trace.export.SpanExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.List;

/**
 * <a href="https://opentelemetry.io/docs/instrumentation/java/manual/">https://opentelemetry.io/docs/instrumentation/java/manual/</a>
 */
@ConditionalOnClass({OpenTelemetry.class, WithSpan.class})
//@Configuration(proxyBeanMethods = false)
public class OpentelemetryAutoConfiguration {
//
//	{
//		Resource resource = Resource.getDefault()
//				.merge(Resource.create(Attributes.of(ResourceAttributes.SERVICE_NAME, "logical-service-name")));
//		SdkTracerProvider sdkTracerProvider = SdkTracerProvider.builder()
//				.setResource(resource)
//				.build();
//		SdkMeterProvider sdkMeterProvider = SdkMeterProvider.builder()
//				.setResource(resource)
//				.build();
//		OpenTelemetry openTelemetry = OpenTelemetrySdk.builder()
//				.setTracerProvider(sdkTracerProvider)
//				.setMeterProvider(sdkMeterProvider)
//				.setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
//				.buildAndRegisterGlobal();
//	}

	@Bean
	@ConditionalOnMissingBean(OpenTelemetry.class)
	public OpenTelemetry openTelemetry(@Autowired(required = false) SdkTracerProvider sdkTracerProvider) {
		return OpenTelemetrySdk.builder()
				.setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
				.setTracerProvider(sdkTracerProvider)
				.build(); // TODO: May be use buildAndRegisterGlobal if running app and not tests.
	}

	@Bean
	@ConditionalOnMissingBean(SdkTracerProvider.class)
	public SdkTracerProvider sdkTracerProvider(@Autowired(required = false) List<SpanProcessor> spanProcessors) {
		SdkTracerProviderBuilder builder = SdkTracerProvider.builder();
		//
		spanProcessors.forEach(builder::addSpanProcessor);
		//
		return builder.build();
	}

	@Bean
	@ConditionalOnBean(SpanExporter.class)
	public SpanProcessor spanProcessor(SpanExporter spanExporter) {
		return SimpleSpanProcessor.create(spanExporter);
	}

	@Bean
	@ConditionalOnBean(OpenTelemetry.class)
	public Tracer tracer(OpenTelemetry openTelemetry) {
		return openTelemetry.getTracer(getClass().getName());
	}
}
