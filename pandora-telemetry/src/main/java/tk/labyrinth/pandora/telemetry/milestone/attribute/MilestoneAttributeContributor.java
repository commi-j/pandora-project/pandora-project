package tk.labyrinth.pandora.telemetry.milestone.attribute;

import io.opentelemetry.api.common.Attributes;
import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.lang.reflect.Method;

public interface MilestoneAttributeContributor {

	Attributes contributeAttributes(Method method, @Nullable Object target, List<Object> arguments);
}
