package tk.labyrinth.pandora.telemetry.opentelemetry;

import io.opentelemetry.context.Context;
import io.opentelemetry.sdk.trace.ReadWriteSpan;
import io.opentelemetry.sdk.trace.ReadableSpan;
import io.opentelemetry.sdk.trace.SpanProcessor;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanCache;

@RequiredArgsConstructor
public class LocallyCachingSpanProcessor implements SpanProcessor {

	private final SpanCache spanCache;

	@Override
	public boolean isEndRequired() {
		return true;
	}

	@Override
	public boolean isStartRequired() {
		return true;
	}

	@Override
	public void onEnd(ReadableSpan span) {
		spanCache.addSpanData(span.toSpanData());
	}

	@Override
	public void onStart(Context parentContext, ReadWriteSpan span) {
		spanCache.addSpanData(span.toSpanData());
	}
}
