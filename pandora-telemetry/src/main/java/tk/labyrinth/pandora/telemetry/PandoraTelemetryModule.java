package tk.labyrinth.pandora.telemetry;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyBean;
import tk.labyrinth.pandora.telemetry.tracing.span.SpanCache;

@Import(TelemetryAspectConfigurer.class)
@Slf4j
@SpringBootApplication
public class PandoraTelemetryModule {

	@PostConstruct
	private void postConstruct() {
		logger.info("Initialized");
	}

	@LazyBean
	public SpanCache spanCache() {
		return SpanCache.GLOBAL;
	}
}
