package tk.labyrinth.pandora.telemetry.tracing.span;

import io.opentelemetry.sdk.trace.data.SpanData;
import io.vavr.collection.List;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Slf4j
public class SpanCache {

	public static final SpanCache GLOBAL = new SpanCache();

	private static final Integer cleanupThreshold = 100000;

	private final ConcurrentMap<String, List<String>> spanIdToChildrenSpanIds = new ConcurrentHashMap<>();

	private final ConcurrentMap<String, SpanData> spanIdToSpanDataMap = new ConcurrentHashMap<>();

	public void addSpanData(SpanData spanData) {
		spanIdToSpanDataMap.compute(
				spanData.getSpanId(),
				(key, value) -> {
					if (value != null) {
						if (!spanData.hasEnded()) {
							logger.warn("Reported not ended Span with existing one: next = {}, current = {}", spanData, value);
						} else {
							if (value.hasEnded()) {
								logger.warn("Reported ended Span with current one already ended: next = {}, current = {}",
										spanData, value);
							}
						}
					} else {
						if (spanData.hasEnded()) {
							logger.warn("Reported ended Span without started: next = {}", spanData);
						}
						//
						spanIdToChildrenSpanIds.put(spanData.getSpanId(), List.empty());
						if (!Objects.equals(spanData.getParentSpanId(), "0000000000000000")) {
							spanIdToChildrenSpanIds.compute(
									spanData.getParentSpanId(),
									(innerKey, innerValue) -> innerValue != null ? innerValue.append(spanData.getSpanId()) : null);
						}
					}
					return spanData;
				});
		//
		// FIXME: This is dirty trick to avoid memory overhead.
		if (spanIdToSpanDataMap.size() > cleanupThreshold * 2) {
			spanIdToSpanDataMap.entrySet().stream()
					.sorted(Comparator.comparing(entry -> entry.getValue().getStartEpochNanos()))
					.limit(cleanupThreshold)
					.map(Map.Entry::getKey)
					.forEach(spanId -> {
						spanIdToSpanDataMap.remove(spanId);
						spanIdToChildrenSpanIds.remove(spanId);
					});
		}
	}

	@Nullable
	public SpanDataWrapper findSpanDataWrapper(String spanId) {
		SpanData spanData = spanIdToSpanDataMap.get(spanId);
		//
		return spanData != null
				? SpanDataWrapper.of(
				spanIdToChildrenSpanIds.get(spanId).map(this::getSpanDataWrapper),
				spanData)
				: null;
	}

	public SpanDataWrapper getSpanDataWrapper(String spanId) {
		return Objects.requireNonNull(findSpanDataWrapper(spanId));
	}

	public List<SpanData> getSpanDatas() {
		return List.ofAll(spanIdToSpanDataMap.values());
	}
}
