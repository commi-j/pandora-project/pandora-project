package tk.labyrinth.pandora.testing.misc4j.lib.junit5;

import org.opentest4j.AssertionFailedError;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class CollectionAssertions {

	/**
	 * <em>Assert</em> that {@code actual} contains all elements of {@code expected},
	 * transforming actual with {@link Collectors#toSet()} if it is not null.
	 *
	 * @param expected nullable
	 * @param actual   nullable
	 * @param <T>      Type
	 *
	 * @since 1.0.0
	 */
	public static <T> void assertContainsAll(Set<T> expected, @Nullable Stream<T> actual) {
		Objects.requireNonNull(expected, "expected");
		//
		boolean matches = actual != null && actual.collect(Collectors.toSet()).containsAll(expected);
		//
		if (!matches) {
			throw new AssertionFailedError(
					"Failed containsAll",
					expected,
					actual);
		}
	}
}
