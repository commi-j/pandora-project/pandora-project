package tk.labyrinth.pandora.testing.misc4j.lib.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Commitman
 * @version 1.0.7
 */
public class ContribAssertions {

	/**
	 * <em>Assert</em> that <b>expected</b> {@link String#endsWith(String) endsWith} <b>actual</b>.<br>
	 * If <b>actual</b> is {@code null}, assertion fails.<br>
	 * If <b>expected</b> is {@code null}, exception is thrown.<br>
	 *
	 * @param expected non-null
	 * @param actual   nullable
	 *
	 * @since 1.0.3
	 */
	public static void assertEndsWith(String expected, @Nullable String actual) {
		Objects.requireNonNull(expected, "expected");
		//
		if (actual == null || !actual.endsWith(expected)) {
			Assertions.fail("endsWith:" +
					"\nExpected: " + expected +
					"\nActual: " + actual);
		}
	}

	/**
	 * <em>Assert</em> that {@code expected} and {@code actual} are equal,
	 * transforming actual with {@link Collectors#toList()} if it is not null.
	 * <p>If both are {@code null}, they are considered equal.
	 *
	 * @param expected nullable
	 * @param actual   nullable
	 * @param <T>      Type
	 *
	 * @see Assertions#assertEquals(Object, Object)
	 * @since 1.0.2
	 */
	public static <T> void assertEquals(@Nullable List<T> expected, @Nullable Stream<T> actual) {
		Assertions.assertEquals(expected, actual != null
				? actual.collect(Collectors.toList())
				: null);
	}

	/**
	 * <em>Assert</em> that {@code expected} and {@code actual} are equal,
	 * transforming actual with {@link Collectors#toSet()} if it is not null.
	 * <p>If both are {@code null}, they are considered equal.
	 *
	 * @param expected nullable
	 * @param actual   nullable
	 * @param <T>      Type
	 *
	 * @see Assertions#assertEquals(Object, Object)
	 * @since 1.0.5
	 */
	public static <T> void assertEquals(@Nullable Set<T> expected, @Nullable Stream<T> actual) {
		Assertions.assertEquals(expected, actual != null
				? actual.collect(Collectors.toSet())
				: null);
	}

	/**
	 * @param condition any
	 *
	 * @since 1.0.7
	 */
	public static void assertFalse(@Nullable Boolean condition) {
		Assertions.assertEquals(false, condition);
	}

	/**
	 * @param type  non-null
	 * @param value non-null
	 *
	 * @since 1.0.0
	 */
	public static void assertInstanceOf(Class<?> type, Object value) {
		if (!type.isInstance(value)) {
			Assertions.fail("instanceof expected:" +
					"\nType: " + type.getCanonicalName() +
					"\nValue: " + value + (value != null ? " of type: " + value.getClass() : ""));
		}
	}

	/**
	 * <em>Assert</em> that <b>expected</b> {@link String#startsWith(String)}  startsWith} <b>actual</b>.<br>
	 * If <b>actual</b> is {@code null}, assertion fails.<br>
	 * If <b>expected</b> is {@code null}, exception is thrown.<br>
	 *
	 * @param expected non-null
	 * @param actual   nullable
	 *
	 * @since 1.0.4
	 */
	public static void assertStartsWith(String expected, @Nullable String actual) {
		Objects.requireNonNull(expected, "expected");
		//
		if (actual == null || !actual.startsWith(expected)) {
			Assertions.fail("startsWith:" +
					"\nExpected: " + expected +
					"\nActual: " + actual);
		}
	}

	/**
	 * @param executable    non-null
	 * @param faultConsumer non-null
	 *
	 * @since 1.0.0
	 */
	public static void assertThrows(Executable executable, Consumer<Throwable> faultConsumer) {
		Throwable fault = Assertions.assertThrows(Throwable.class, executable);
		faultConsumer.accept(fault);
	}

	/**
	 * @param condition any
	 *
	 * @since 1.0.7
	 */
	public static void assertTrue(@Nullable Boolean condition) {
		Assertions.assertEquals(true, condition);
	}

	/**
	 * @param fromInclusive any
	 * @param toInclusive   GTE fromInclusive
	 * @param actual        any
	 *
	 * @since 1.0.7
	 */
	public static void assertWithinRange(long fromInclusive, long toInclusive, long actual) {
		if (fromInclusive > toInclusive) {
			throw new IllegalArgumentException("Inconsistent range: [" + fromInclusive + ", " + toInclusive + "]");
		}
		if (actual < fromInclusive || actual > toInclusive) {
			Assertions.fail("withinRange:" +
					"\nExpected: [" + fromInclusive + ", " + toInclusive + "]" +
					"\nActual: " + actual);
		}
	}
}
