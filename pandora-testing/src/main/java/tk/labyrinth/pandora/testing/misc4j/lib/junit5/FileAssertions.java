package tk.labyrinth.pandora.testing.misc4j.lib.junit5;

import lombok.Getter;
import org.junit.jupiter.api.Assertions;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class FileAssertions {

	@Nullable
	private static Integer compareFileContents(File first, File second) {
		Integer result = null;
		try (InputStream firstInputStream = new FileInputStream(first);
			 InputStream secondInputStream = new FileInputStream(second)) {
			int chunkSize = 1000;
			byte[] firstChunk = new byte[chunkSize];
			byte[] secondChunk = new byte[chunkSize];
			//
			int offset = 0;
			do {
				int firstChunkReadCount = firstInputStream.read(firstChunk);
				int secondChunkReadCount = secondInputStream.read(secondChunk);
				int minimumReadCount = Math.min(firstChunkReadCount, secondChunkReadCount);
				//
				for (int i = 0; i < minimumReadCount; i++) {
					if (firstChunk[i] != secondChunk[i]) {
						//
						// Byte difference found.
						result = offset + i;
						break;
					}
				}
				if (firstChunkReadCount != secondChunkReadCount) {
					//
					// One of files is depleted but other is not.
					// Although we detect this before comparing bytes we do it later
					// to find the exact offset where files start differ.
					result = offset + minimumReadCount;
					break;
				}
				offset += firstChunkReadCount;
			} while (firstInputStream.available() > 0 || secondInputStream.available() > 0);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
		return result;
	}

	/**
	 * @param expected non-null
	 * @param actual   non-null
	 *
	 * @since 1.0.0
	 */
	public static void assertContentEquals(Path expected, Path actual) {
		// FIXME: Only works for directories now.
		//
		Function<Path, List<Path>> collectPathsFunction = path -> {
			CollectingPathsFileVisitor fileVisitor = new CollectingPathsFileVisitor();
			try {
				Files.walkFileTree(path, fileVisitor);
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
			return fileVisitor.getPaths();
		};
		//
		List<Path> actualPaths = collectPathsFunction.apply(actual);
		List<Path> expectedPaths = collectPathsFunction.apply(expected);
		//
		String diffText;
		if (actualPaths.size() == expectedPaths.size()) {
			//
			// Paths are expected to be ordered alphabetically.
			//
			diffText = null;
			for (int i = 0; i < actualPaths.size(); i++) {
				Path actualPathElement = actualPaths.get(i);
				Path expectedPathElement = expectedPaths.get(i);
				//
				if (!Objects.equals(
						actual.relativize(actualPathElement).toString(),
						expected.relativize(expectedPathElement).toString())) {
					//
					// Path names not equal;
					diffText = "Different names:" +
							" expected = " + expectedPathElement +
							" actual = " + actualPathElement;
					break;
				} else {
					File actualFile = actualPathElement.toFile();
					File expectedFile = expectedPathElement.toFile();
					if (actualFile.isDirectory() != expectedFile.isDirectory()) {
						//
						// One file is directory and other is not.
						diffText = "Different directory flag: " +
								"expected = " + expectedPathElement + ", " +
								"actual = " + actualPathElement + ", " +
								"flags = " + expectedFile.isDirectory() + "/" + actualFile.isDirectory();
						break;
					} else {
						if (!actualFile.isDirectory()) {
							//
							// Paths are not directories -> files, need to compare contents.
							Integer diffOffset = compareFileContents(actualFile, expectedFile);
							if (diffOffset != null) {
								//
								// Content difference found.
								diffText = "Different content offset: " +
										"expected = " + expectedPathElement + ", " +
										"actual = " + actualPathElement + ", " +
										"offset = " + diffOffset;
								break;
							} else {
								//
								// Content difference not found -> going to next one.
							}
						} else {
							//
							// Paths are directories -> going to next one.
						}
					}
				}
			}
		} else {
			//
			// Different number of nested elements.
			diffText = "Different nested structures";
		}
		if (diffText != null) {
			Assertions.fail(diffText);
		}
	}

	public static class CollectingPathsFileVisitor extends SimpleFileVisitor<Path> {

		@Getter
		private final List<Path> paths = new ArrayList<>();

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			FileVisitResult result = super.preVisitDirectory(dir, attrs);
			paths.add(dir);
			return result;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			FileVisitResult result = super.visitFile(file, attrs);
			paths.add(file);
			return result;
		}
	}
}
