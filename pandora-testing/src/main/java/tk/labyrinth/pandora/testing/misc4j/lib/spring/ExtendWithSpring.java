package tk.labyrinth.pandora.testing.misc4j.lib.spring;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Shorthand for {@link ExtendWith @ExtendWith}({@link SpringExtension SpringExtension.class}).
 *
 * @author Commitman
 */
@Documented
@ExtendWith(SpringExtension.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ExtendWithSpring {
	// empty
}
