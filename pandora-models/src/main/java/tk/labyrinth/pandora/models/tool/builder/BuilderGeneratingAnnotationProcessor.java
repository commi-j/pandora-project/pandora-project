package tk.labyrinth.pandora.models.tool.builder;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.WildcardTypeName;
import io.vavr.collection.List;
import org.apache.commons.lang3.reflect.TypeUtils;
import tk.labyrinth.jaap.annotation.merged.MergedAnnotationSpecification;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.jaap.model.element.TypeElementHandle;
import tk.labyrinth.pandora.datatypes.domain.datatype.Datatype;
import tk.labyrinth.pandora.datatypes.domain.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.JaapObjectModelFactory;
import tk.labyrinth.pandora.misc4j.exception.ExceptionUtils;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.java.io.IoUtils;

import javax.lang.model.element.Modifier;

// TODO: Do we need them?
//@AutoService(Processor.class)
public class BuilderGeneratingAnnotationProcessor extends CallbackAnnotationProcessor {

	{
		onEachRound(round -> {
			JaapObjectModelFactory modelFactory = new JaapObjectModelFactory();
			//
			RoundContext roundContext = RoundContext.of(round);
			//
			roundContext.getAllTypeElements()
					// FIXME: Probably look for constructor or check non-abstract?
					.filter(typeElementHandle -> !typeElementHandle.isInterface())
					.filter(typeElementHandle -> typeElementHandle.hasMergedAnnotation(
							GenerateBuilder.class,
							MergedAnnotationSpecification.javaCore()))
					.forEach(typeElementHandle -> {
						ObjectModel model = modelFactory.createModelFromTypeElementHandle(typeElementHandle);
						//
						TypeSpec typeSpec = createBuilderSpec(typeElementHandle, model);
						//
						JavaFile javaFile = JavaFile
								.builder(typeElementHandle.getPackageQualifiedName(), typeSpec)
								.build();
						//
						IoUtils.unchecked(() -> javaFile.writeTo(round.getProcessingEnvironment().getFiler()));
					});
		});
	}

	public static TypeSpec createBuilderSpec(TypeElementHandle typeElementHandle, ObjectModel objectModel) {
		ClassName builderClassName = pandoraSignatureToClassName(
				"%sBuilder".formatted(typeElementHandle.getQualifiedName()));
		TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(builderClassName);
		{
			objectModel.getAttributes().forEach(attribute -> {
				String attributeName = attribute.getName();
				TypeName attributeTypeName = typeDescriptionToTypeName(datatypeToTypeDescription(
						attribute.getDatatype()));
				//
				// private A a;
				typeBuilder.addField(FieldSpec.builder(
						attributeTypeName,
						attributeName,
						Modifier.PRIVATE).build());
				//
				// public A a();
				typeBuilder.addMethod(MethodSpec.methodBuilder(attributeName)
						.addModifiers(Modifier.PUBLIC)
						.returns(attributeTypeName)
						.addCode("return %s;".formatted(attributeName))
						.build());
				//
				// public TBuilder a(A a);
				typeBuilder.addMethod(MethodSpec.methodBuilder(attributeName)
						.addModifiers(Modifier.PUBLIC)
						.returns(builderClassName)
						.addParameter(attributeTypeName, attributeName)
						.addCode("""
								this.%1$s = %1$s;
								return this;""".formatted(attributeName))
						.build());
			});
		}
		{
			// TODO:
			// public T build();
			typeBuilder.addMethod(MethodSpec.methodBuilder("build").build());
		}
		return typeBuilder.build();
	}

	public static TypeDescription datatypeToTypeDescription(Datatype datatype) {
		return TypeDescription.builder()
				.fullName(JavaBaseTypeUtils.extractJavaBaseTypeReference(datatype.getBaseReference()).getSignature().toString())
				.parameters(datatype.getParameters() != null
						? datatype.getParameters()
						.map(BuilderGeneratingAnnotationProcessor::datatypeToTypeDescription)
						.asJava()
						: null)
				.build();
	}

	public static ClassName pandoraSignatureToClassName(String pandoraSignature) {
		return ClassName.bestGuess(pandoraSignature.replace(':', '.'));
	}

	public static TypeName typeDescriptionToTypeName(TypeDescription typeDescription) {
		TypeName result;
		{
			if (typeDescription.isPrimitive()) {
				throw new NotImplementedException(ExceptionUtils.render(typeDescription));
			} else if (typeDescription.isReference()) {
				if (typeDescription.isArray()) {
					throw new NotImplementedException(ExceptionUtils.render(typeDescription));
				} else if (typeDescription.isDeclared()) {
					if (typeDescription.isParameterized()) {
						result = ParameterizedTypeName.get(
								pandoraSignatureToClassName(typeDescription.getFullName()),
								List.ofAll(typeDescription.getParameters())
										.map(BuilderGeneratingAnnotationProcessor::typeDescriptionToTypeName)
										.toJavaArray(TypeName[]::new));
					} else {
						result = pandoraSignatureToClassName(typeDescription.getFullName());
					}
				} else if (typeDescription.isVariable()) {
					throw new NotImplementedException(ExceptionUtils.render(typeDescription));
				} else if (typeDescription.isWildcard()) {
					if (typeDescription.getLowerBound() != null) {
						result = WildcardTypeName.supertypeOf(typeDescriptionToTypeName(
								typeDescription.getLowerBound()));
					} else if (typeDescription.getUpperBound() != null) {
						if (typeDescription.getUpperBound().size() > 1) {
							throw new NotImplementedException(ExceptionUtils.render(typeDescription));
						} else {
							result = WildcardTypeName.subtypeOf(typeDescriptionToTypeName(
									typeDescription.getUpperBound().get(0)));
						}
					} else {
						result = WildcardTypeName.get(TypeUtils.wildcardType().build());
					}
				} else {
					throw new NotImplementedException(ExceptionUtils.render(typeDescription));
				}
			} else if (typeDescription.isVoid()) {
				result = TypeName.VOID;
			} else {
				throw new NotImplementedException(ExceptionUtils.render(typeDescription));
			}
		}
		return result;
	}
}
