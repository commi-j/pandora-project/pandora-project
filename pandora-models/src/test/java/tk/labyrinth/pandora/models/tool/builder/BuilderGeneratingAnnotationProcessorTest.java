package tk.labyrinth.pandora.models.tool.builder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jaap.core.CompilationTarget;
import tk.labyrinth.jaap.testing.JaapTestCompiler;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.domain.objectmodel.polymorphic.PolymorphicObjectModelFeatureContributor;
import tk.labyrinth.pandora.datatypes.objectmodel.factory.ReflectionObjectModelFactory;
import tk.labyrinth.pandora.models.test.InterfaceTestModel;

import java.util.List;

@Disabled
class BuilderGeneratingAnnotationProcessorTest {

	@Test
	void testCreateBuilderSpec() {
		ReflectionObjectModelFactory reflectionObjectModelFactory = new ReflectionObjectModelFactory(
				List.of(),
				List.of(new PolymorphicObjectModelFeatureContributor()));
		//
		ObjectModel objectModel = reflectionObjectModelFactory.createModelFromJavaClass(InterfaceTestModel.class);
		//
		Assertions.assertEquals(
				"""
						class InterfaceTestModelBuilder {
						  private java.lang.Integer integer;
						      
						  private java.lang.String string;
						      
						  public java.lang.Integer integer() {
						    return integer;
						  }
						      
						  public tk.labyrinth.pandora.models.test.InterfaceTestModelBuilder integer(
						      java.lang.Integer integer) {
						    this.integer = integer;
						    return this;
						  }
						      
						  public java.lang.String string() {
						    return string;
						  }
						      
						  public tk.labyrinth.pandora.models.test.InterfaceTestModelBuilder string(
						      java.lang.String string) {
						    this.string = string;
						    return this;
						  }
						      
						  void build() {
						  }
						}
						""",
				BuilderGeneratingAnnotationProcessor.createBuilderSpec(null, objectModel).toString()); // FIXME: null
	}

	@Test
	void testProcess() {
		JaapTestCompiler testCompiler = new JaapTestCompiler();
		List<String> output = testCompiler.run(
				CompilationTarget.ofSourceTypes(InterfaceTestModel.class),
				new BuilderGeneratingAnnotationProcessor());
		System.out.println();
		// TODO: Check generated files.
	}
}
