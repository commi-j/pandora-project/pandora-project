package tk.labyrinth.pandora.models.test;

public interface InterfaceTestModel {

	Integer getInteger();

	String getString();
}
