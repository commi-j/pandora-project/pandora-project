[![MAVEN_CENTRAL](https://img.shields.io/maven-central/v/tk.labyrinth.pandora/pandora-project-parent.svg?label=Maven+Central&metadataUrl=https%3A%2F%2Foss.sonatype.org%2Fcontent%2Frepositories%2Fsnapshots%2Ftk%2Flabyrinth%2Fpandora%2Fpandora-project-parent%2Fmaven-metadata.xml&style=flat-square)](https://oss.sonatype.org/content/repositories/snapshots/tk/labyrinth/pandora/pandora-project-parent/)
[![LICENSE](https://img.shields.io/badge/license-MIT-green.svg?label=License&style=flat-square)](LICENSE)
[![TEST_COVERAGE](https://gitlab.com/commi-j/pandora-project/pandora-project/badges/dev/coverage.svg?style=flat-square)](https://gitlab.com/commi-j/pandora-project/pandora-project/badges/feature%2fcoverage/coverage.svg)
[![PIPELINE](https://gitlab.com/commi-j/pandora-project/pandora-project/badges/dev/pipeline.svg?style=flat-square)](https://gitlab.com/commi-j/pandora-project/pandora-project/-/pipelines)

# Pandora Project

## Installment Guide

### 1. Add Maven BOM Dependency

This BOM includes compatible versions of Spring Boot, Spring Cloud and Vaadin BOMs (and few other), so you should prefer
to avoid declaring versions yourself.

```xml
<dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>tk.labyrinth.pandora</groupId>
      <artifactId>pandora-project-parent</artifactId>
      <version>0.4.0-SNAPSHOT</version>
      <scope>import</scope>
      <type>pom</type>
    </dependency>
  </dependencies>
</dependencyManagement>
```

### 2. Add Required Module Dependencies

```xml
<dependencies>
  <dependency>
    <groupId>tk.labyrinth.pandora</groupId>
    <artifactId>pandora-MODULE</artifactId>
  </dependency>
</dependencies>
```

### 3. Enable Modules in Java

```java
@Import({
    PandoraXModule.class,
    PandoraYModule.class,
})
```

### 4. Configure Build Plugins (Optional)

In case you're building your own application you need to add Vaadin frontend tasks to your build script to get proper
executable.

```xml
<build>
  <plugins>
    <plugin>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-maven-plugin</artifactId>
      <version>SHOULD_MATCH_VAADIN_BOM_VERSION_FROM_PANDORA</version>
      <!---->
      <executions>
        <execution>
          <goals>
            <goal>prepare-frontend</goal>
          </goals>
        </execution>
      </executions>
    </plugin>
  </plugins>
</build>
```

```xml
<profiles>
  <profile>
    <id>build-fat-jar</id>
    <build>
      <plugins>
        <plugin>
          <groupId>com.vaadin</groupId>
          <artifactId>vaadin-maven-plugin</artifactId>
          <version>SHOULD_MATCH_VAADIN_BOM_VERSION_FROM_PANDORA</version>
          <!---->
          <executions>
            <execution>
              <goals>
                <goal>build-frontend</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
      </plugins>
    </build>
  </profile>
</profiles>
```

## Modules

![docs/modules.png](docs/modules.png)

- Pandora Misc4j - compilation of utility code;
- Pandora Telemetry - telemetry utilities, i.e. logging, metrics, tracing;
    - q
- Pandora Core - stuff that other modules depend on;
- Pandora Dependencies - enhancements for libraries used by Pandora;
- Pandora Datatypes - classes related to data models - value, simple, object, list;
- Pandora Context - enhancements for application context, Spring-related mostly;
- Pandora Stores - utilities for models to work with data persistence.
- Pandora Reactive - TODO;
- Pandora Jobs - TODO;
- Pandora UI - TODO;
- Pandora Bundle - collection of all modules above (TODO);
- Pandora Testing;
- Pandora Application - runnable module to test and features;

Deprecated:

- Pandora Models - replaced with Datatypes;
- Pandora Storage - replaced with Stores;

## Typical module usage

## Some Docs

- [Notion](https://vintage-citron-a38.notion.site/Pandora-Project-a951e72013af4c30885fe01fb14b88db)

TODO
