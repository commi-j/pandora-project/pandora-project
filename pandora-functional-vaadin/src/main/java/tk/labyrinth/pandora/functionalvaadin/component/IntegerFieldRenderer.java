package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

// TODO: Think if we need this one or can use VaadinComponents instead.
public class IntegerFieldRenderer {

	public static IntegerField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static IntegerField render(Parameters parameters) {
		IntegerField integerField = FunctionalComponents.createComponent(IntegerField::new);
		{
			integerField.setLabel(parameters.label());
			{
				if (parameters.max() != null) {
					integerField.setMax(parameters.max());
				} else {
					integerField.getElement().removeProperty("max");
				}
			}
			{
				if (parameters.min() != null) {
					integerField.setMin(parameters.min());
				} else {
					integerField.getElement().removeProperty("min");
				}
			}
			{
				if (parameters.stepButtonsVisible() != null) {
					integerField.setStepButtonsVisible(parameters.stepButtonsVisible());
				} else {
					integerField.getElement().removeProperty("stepButtonsVisible");
				}
			}
			integerField.setValue(parameters.value());
			integerField.setValueChangeMode(parameters.valueChangeMode() != null
					? parameters.valueChangeMode()
					: ValueChangeMode.ON_CHANGE);
			integerField.setWidth(parameters.width());
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			FunctionalComponents.recordRegistration(
					integerField,
					integerField.addValueChangeListener(event -> {
						if (event.isFromClient()) {
							valueChanged.set(true);
							//
							event.getSource().setValue(event.getOldValue());
							//
							if (parameters.onValueChange() != null) {
								parameters.onValueChange().accept(event.getValue());
							}
						}
					}));
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> FunctionalComponents.recordRegistration(
					integerField,
					integerField.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false)))));
		}
		{
			// TODO: Customizer
		}
		return integerField;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		String label;

		@Nullable
		Integer max;

		@Nullable
		Integer min;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable Integer> onValueChange;

		@Nullable
		Boolean readOnly;

		@Nullable
		Boolean stepButtonsVisible;

		@Nullable
		Integer value;

		@Nullable
		ValueChangeMode valueChangeMode;

		@Nullable
		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
