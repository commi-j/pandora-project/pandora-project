package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public class TextFieldRenderer {

	public static TextField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextField render(Parameters parameters) {
		TextField textField = FunctionalComponents.createComponent(TextField::new);
		{
			textField.setAutocomplete(parameters.autocomplete());
			textField.setEnabled(BooleanUtils.isNotFalse(parameters.enabled()));
			textField.setHelperText(parameters.helperText());
			textField.setLabel(parameters.label());
			textField.setPrefixComponent(parameters.prefixComponent());
			textField.setReadOnly(BooleanUtils.isTrue(parameters.readOnly()));
			textField.setSuffixComponent(parameters.suffixComponent());
			ElementUtils.setTitle(textField, parameters.title());
			textField.setValue(parameters.value());
			textField.setValueChangeMode(parameters.valueChangeMode() != null
					? parameters.valueChangeMode()
					: ValueChangeMode.ON_CHANGE);
			textField.setWidth(parameters.width());
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			FunctionalComponents.recordRegistration(
					textField,
					textField.addValueChangeListener(event -> {
						if (event.isFromClient()) {
							valueChanged.set(true);
							//
							event.getSource().setValue(event.getOldValue());
							//
							if (parameters.onValueChange() != null) {
								parameters.onValueChange().accept(!event.getValue().isEmpty() ? event.getValue() : null);
							}
						}
					}));
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> FunctionalComponents.recordRegistration(
					textField,
					textField.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false)))));
		}
		{
			// TODO: Support on clear.
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(textField));
		}
		return textField;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Autocomplete autocomplete;

		@Nullable
		Consumer<TextField> configurer;

		@Nullable
		Boolean enabled;

		@Nullable
		String helperText;

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable String> onValueChange;

		@Nullable
		Component prefixComponent;

		@Nullable
		Boolean readOnly;

		@Nullable
		Component suffixComponent;

		@Nullable
		String title;

		@lombok.Builder.Default
		@Nullable
		String value = "";

		@Nullable
		ValueChangeMode valueChangeMode;

		@Nullable
		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
