package tk.labyrinth.pandora.functionalvaadin.demo;

import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponent2;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;

@Route(value = "functional-component-demo")
public class FunctionalComponentDemoPage extends CssVerticalLayout {

	@PostConstruct
	private void postConstruct() {
		{
			addClassName("pandora-layout");
		}
		{
			Observable<String> stringObservable = Observable.withInitialValue("foo");
			//
			add(FunctionalComponent2.of(
					stringObservable,
					(state, sink) -> TextFieldRenderer.render(builder -> builder
							.label("String")
							.onValueChange(sink)
							.value(state)
							.valueChangeMode(ValueChangeMode.EAGER)
							.build())));
			add(FunctionalComponents.createRootContainer(
					stringObservable,
					CssVerticalLayout::new,
					(state, sink, layout) -> layout.add(TextFieldRenderer.render(builder -> builder
							.label("String 2")
							.onValueChange(sink)
							.value(state)
							.valueChangeMode(ValueChangeMode.EAGER)
							.build()))));
		}
	}
}
