package tk.labyrinth.pandora.functionalvaadin.view.fault;

import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextArea;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.domain.fault.Fault;
import tk.labyrinth.pandora.datatypes.domain.fault.StructuredFault;
import tk.labyrinth.pandora.datatypes.domain.fault.ThrowableFault;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextAreaRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;

import java.util.function.Function;

public class FaultViewRenderer {

	public static TextArea render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextArea render(Parameters parameters) {
		return TextAreaRenderer.render(builder -> builder
				.label(parameters.label())
				.readOnly(true)
				.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
						.icon(VaadinIcon.QUESTION_CIRCLE.create())
						.onClick(event -> ConfirmationViews.showComponentDialog(FaultViewRenderer
								.render(faultAreaBuilder -> faultAreaBuilder
										.fault(parameters.fault())
										.build())))
						.themeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
						.build()))
				.value(renderFault(parameters.fault()))
				.build());
	}

	public static String renderFault(Fault fault) {
		String result;
		//
		if (fault instanceof StructuredFault structuredFault) {
			result = structuredFault.getMessage();
		} else if (fault instanceof ThrowableFault throwableFault) {
			StringBuilder stringBuilder = new StringBuilder();
			//
			stringBuilder
					.append(throwableFault.getClassSignature())
					.append(": ")
					.append(throwableFault.getMessage())
					.append('\n');
			//
			throwableFault.getStackTrace().forEach(stackTraceElement -> stringBuilder
					.append('\t')
					.append(stackTraceElement)
					.append('\n'));
			//
			// TODO: cause
			//
			result = stringBuilder.toString();
		} else {
			result = fault.toString();
		}
		//
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Fault fault;

		@Nullable
		String label;

		public static class Builder {
			// Lomboked
		}
	}
}
