package tk.labyrinth.pandora.functionalvaadin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import tk.labyrinth.pandora.functionalvaadin.offstage.OffstageState;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.util.function.BiFunction;
import java.util.function.Consumer;

public class FunctionalComponent2<S> extends Composite<Component> {

	private final Component component;

	private final BiFunction<S, Consumer<S>, Component> renderFunction;

	private final Observable<S> stateObservable;

	private FunctionalComponent2(BiFunction<S, Consumer<S>, Component> renderFunction, Observable<S> stateObservable) {
		this.renderFunction = renderFunction;
		this.stateObservable = stateObservable;
		//
		OffstageState offstageState = new OffstageState();
		//
		this.component = offstageState.compute(() -> renderFunction.apply(stateObservable.get(), stateObservable::set));
		//
		stateObservable.subscribe((state, sink) -> {
			//
			Component nextComponent = offstageState.compute(() -> renderFunction.apply(state, sink));
			//
			if (nextComponent != component) {
				throw new IllegalStateException();
			}
		});
	}

	@Override
	protected Component initContent() {
		return component;
	}

	public static <S> Component of(Observable<S> stateObservable, BiFunction<S, Consumer<S>, Component> renderFunction) {
		return new FunctionalComponent2<>(renderFunction, stateObservable);
	}
}
