package tk.labyrinth.pandora.functionalvaadin.view.confirmation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.progressbar.ProgressBar;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import java.util.function.Supplier;

@Deprecated
public class ConfirmationView extends CssVerticalLayout implements View {

	@Override
	public Component asVaadinComponent() {
		return this;
	}

	public void initialize(Parameters parameters, View view) {
		{
			// TODO: Think about that.
//			addClassNames(PandoraStyles.LAYOUT);
			//
			setHeightFull();
		}
		{
			CssFlexItem.setFlexGrow(view.asVaadinComponent(), 1);
			add(view.asVaadinComponent());
		}
		{
			add(new ProgressBar(0, 1, 1));
		}
		{
			CssHorizontalLayout buttonFooter = new CssHorizontalLayout();
			{
				// TODO: Think about that.
//				buttonFooter.addClassNames(PandoraStyles.LAYOUT);
			}
			{
				Button cancelButton = new Button("Cancel", event -> parameters.closeCallback().run());
				cancelButton.getElement().setAttribute("name", ConfirmationViews.CANCEL_BUTTON_NAME);
				buttonFooter.add(cancelButton);
			}
			{
				buttonFooter.addStretch();
			}
			if (parameters.showApplyButton()) {
				Button applyButton = new Button("Apply", event -> parameters.applyCallback().get());
				applyButton.getElement().setAttribute("name", ConfirmationViews.APPLY_BUTTON_NAME);
				buttonFooter.add(applyButton);
			}
			{
				Button confirmButton = new Button(
						"Confirm",
						event -> {
							if (parameters.applyCallback().get()) {
								parameters.closeCallback().run();
							}
						});
				confirmButton.getElement().setAttribute("name", ConfirmationViews.CONFIRM_BUTTON_NAME);
				buttonFooter.add(confirmButton);
			}
			add(buttonFooter);
		}
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		/**
		 * Invoked when <b>apply</b> or <b>confirm</b> is triggered.
		 * If <b>true</b> is returned and triggered action is <b>confirm</b>, it proceeds with invoking {@link #closeCallback}.
		 */
		Supplier<Boolean> applyCallback;

		/**
		 * Invoked when <b>confirm</b> is triggered and {@link #applyCallback} returns <b>true</b>
		 * or when <b>close</b> is triggered.
		 */
		Runnable closeCallback;

		Boolean showApplyButton;
	}
}
