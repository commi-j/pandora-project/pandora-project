package tk.labyrinth.pandora.functionalvaadin;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PandoraFunctionalVaadinModule {
	// empty
}
