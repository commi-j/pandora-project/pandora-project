package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.details.Details;
import io.vavr.control.Either;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @see Details
 */
public class DetailsRenderer {

	public static Details render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Details render(Parameters parameters) {
		Details details = new Details();
		{
			Optional.ofNullable(parameters.content()).ifPresent(details::setContent);
			Optional.ofNullable(parameters.opened()).ifPresent(details::setOpened);
			Optional.ofNullable(parameters.summary()).ifPresent(summary -> summary.bimap(
				left -> {
					details.setSummaryText(left);
					//
					return Void.class;
				},
				right -> {
					details.setSummary(right);
					//
					return Void.class;
				}));
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(details));
		}
		return details;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<Details> configurer;

		@Nullable
		Component content;

		@Nullable
		Boolean opened;

		@Nullable
		Either<String, Component> summary;

		public static class Builder {

			public Builder summary(@Nullable Component summary) {
				return summary(summary != null ? Either.right(summary) : null);
			}

			public Builder summary(@Nullable Either<String, Component> summary) {
				this.summary = summary;
				//
				return this;
			}

			public Builder summary(@Nullable String summary) {
				return summary(summary != null ? Either.left(summary) : null);
			}
		}
	}
}
