package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class TabsRenderer {

	public static Tabs render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Tabs render(Parameters parameters) {
		Tabs tabs = FunctionalComponents.createComponent(Tabs::new);
		{
			tabs.setAutoselect(BooleanUtils.isNotFalse(parameters.autoselect()));
		}
		{
			// FIXME: Not sure if it works right for functional rendering.
			Optional.ofNullable(parameters.tabs())
					.ifPresent(parametersTabs -> tabs.add(parametersTabs.toArray(Tab[]::new)));
		}
		return tabs;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Boolean autoselect;

		@Nullable
		List<Tab> tabs;

		public static class Builder {
			// Lomboked
		}
	}
}
