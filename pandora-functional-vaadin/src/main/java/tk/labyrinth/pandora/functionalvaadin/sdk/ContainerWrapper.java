package tk.labyrinth.pandora.functionalvaadin.sdk;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.dom.Element;
import lombok.NonNull;
import lombok.Value;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.offstage.OffstageComponentChildrenHandler;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Value(staticConstructor = "of")
public class ContainerWrapper<C extends Component> implements HasStyle {

	@NonNull
	C container;

	private void doAdd(Component component) {
		OffstageComponentChildrenHandler.recordAdded(component);
		//
		if (component.getParent().orElse(null) != container) {
			((HasComponents) container).add(component);
		}
	}

	public void add(Component component) {
		doAdd(component);
	}

	public void add(Supplier<Component> newComponentSupplier) {
		doAdd(FunctionalComponents.createComponent(newComponentSupplier));
	}

	public void add(View view) {
		doAdd(view.asVaadinComponent());
	}

	public void addAndConfigure(Component component, Consumer<Component> configurer) {
		doAdd(component);
		//
		configurer.accept(component);
	}

	public void addAndConfigure(Supplier<Component> newComponentSupplier, Consumer<Component> configurer) {
		addAndConfigure(FunctionalComponents.createComponent(newComponentSupplier), configurer);
	}

	public View asView() {
		return ComponentView.of(container);
	}

	public void complete() {
		//
	}

	@Override
	public Element getElement() {
		return container.getElement();
	}

	public void handleChildren(Runnable runnable) {
		OffstageComponentChildrenHandler.handle(container, runnable);
	}
}
