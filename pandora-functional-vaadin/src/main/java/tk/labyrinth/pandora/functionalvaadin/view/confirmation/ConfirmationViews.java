package tk.labyrinth.pandora.functionalvaadin.view.confirmation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponent2;
import tk.labyrinth.pandora.functionalvaadin.dialog.Dialogs;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.data.Success;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ConfirmationViews {

	public static final String APPLY_BUTTON_NAME = "apply-button";

	public static final String CANCEL_BUTTON_NAME = "cancel-button";

	public static final String CONFIRM_BUTTON_NAME = "confirm-button";

	public static ConfirmationHandle<Success> showComponentDialog(Component component) {
		return showViewDialog(ComponentView.of(component));
	}

	public static <T> ConfirmationHandle<T> showComponentDialog(Component component, Supplier<T> resultSupplier) {
		return showViewDialog(ComponentView.of(component), resultSupplier);
	}

	public static <S> ConfirmationHandle<S> showComponentFunctionDialog(
			Observable<S> stateObservable,
			BiFunction<S, Consumer<S>, Component> renderFunction) {
		return showComponentDialog(FunctionalComponent2.of(stateObservable, renderFunction), stateObservable::get);
	}

	public static <S> ConfirmationHandle<S> showComponentFunctionDialog(
			S initialState,
			BiFunction<S, Consumer<S>, Component> renderFunction) {
		return showComponentFunctionDialog(Observable.withInitialValue(initialState), renderFunction);
	}

	public static ConfirmationHandle<Success> showTextDialog(String text) {
		return showViewDialog(ComponentView.of(new Span(text)));
	}

	public static ConfirmationHandle<Success> showViewDialog(View view) {
		return showViewDialog(view, () -> Success.VALUE);
	}

	public static <T> ConfirmationHandle<T> showViewDialog(View view, Supplier<T> resultSupplier) {
		ConfirmationHandle<T> confirmationHandle = new ConfirmationHandle<>();
		{
			ConfirmationView confirmationView = new ConfirmationView();
			//
			Runnable closeDialogCallback = Dialogs.show(
					confirmationView,
					() -> {
						// no-op
					});
			confirmationView.initialize(
					ConfirmationView.Parameters.builder()
							.applyCallback(() -> {
								boolean result;
								{
									Predicate<T> subscribeFunction = confirmationHandle.getSubscribePredicate();
									//
									if (subscribeFunction != null) {
										result = subscribeFunction.test(resultSupplier.get());
									} else {
										result = true;
									}
								}
								return result;
							})
							.closeCallback(closeDialogCallback)
							.showApplyButton(false)
							.build(),
					view);
		}
		return confirmationHandle;
	}

	public static <S> ConfirmationHandle<S> showViewFunctionDialog(
			Observable<S> stateObservable,
			BiFunction<S, Consumer<S>, View> renderFunction) {
		return showViewDialog(
				// FIXME: This is some odd crutch. We want to get rid of these View wrapping/unwrapping.
				ComponentView.of(FunctionalComponent2.of(stateObservable, renderFunction.andThen(View::asVaadinComponent))),
				stateObservable::get);
	}

	public static <S> ConfirmationHandle<S> showViewFunctionDialog(
			S initialState,
			BiFunction<S, Consumer<S>, View> renderFunction) {
		return showViewFunctionDialog(Observable.withInitialValue(initialState), renderFunction);
	}
}
