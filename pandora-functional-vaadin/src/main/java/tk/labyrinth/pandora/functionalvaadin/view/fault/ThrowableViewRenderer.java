package tk.labyrinth.pandora.functionalvaadin.view.fault;

import com.vaadin.flow.component.textfield.TextArea;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.io.output.StringBuilderWriter;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.component.TextAreaRenderer;

import java.io.PrintWriter;
import java.util.function.Function;

public class ThrowableViewRenderer {

	public static TextArea render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextArea render(Parameters parameters) {
		return TextAreaRenderer.render(builder -> builder
				.label(parameters.label())
				.readOnly(true)
//				.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
//						.icon(VaadinIcon.QUESTION_CIRCLE.create())
//						.onClick(event -> ConfirmationViews.showComponentDialog(ThrowableViewRenderer
//								.render(textAreaBuilder -> textAreaBuilder
//										.throwable(parameters.throwable())
//										.build())))
//						.themeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
//						.build()))
				.value(renderThrowable(parameters.throwable()))
				.build());
	}

	public static String renderThrowable(Throwable throwable) {
		StringBuilder stringBuilder = new StringBuilder();
		//
		throwable.printStackTrace(new PrintWriter(new StringBuilderWriter(stringBuilder)));
		//
		return stringBuilder.toString();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		String label;

		@NonNull
		Throwable throwable;

		public static class Builder {
			// Lomboked
		}
	}
}
