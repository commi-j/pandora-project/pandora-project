package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class ButtonRenderer {

	public static Button render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Button render(Parameters parameters) {
		Button button = FunctionalComponents.createComponent(Button::new);
		{
			button.setDisableOnClick(BooleanUtils.isTrue(parameters.disableOnClick()));
			button.setEnabled(BooleanUtils.isNotFalse(parameters.enabled()));
			button.setHeight(parameters.height());
			button.setIcon(parameters.icon());
			button.setIconAfterText(BooleanUtils.isTrue(parameters.iconAfterText()));
			StyleUtils.setMargin(button, parameters.margin());
			button.setText(parameters.text());
			{
				button.getThemeNames().clear();
				//
				if (parameters.themeVariants() != null) {
					button.addThemeVariants(parameters.themeVariants().toArray(ButtonVariant[]::new));
				}
			}
			button.setTooltipText(parameters.tooltipText());
			button.setVisible(BooleanUtils.isNotFalse(parameters.visible()));
		}
		{
			Optional.ofNullable(parameters.onClick()).ifPresent(onClick -> FunctionalComponents.recordRegistration(
					button,
					button.addClickListener(onClick)));
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(button));
		}
		return button;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<Button> configurer;

		@Nullable
		Boolean disableOnClick;

		@Nullable
		Boolean enabled;

		@Nullable
		String height;

		@Nullable
		Component icon;

		@Nullable
		Boolean iconAfterText;

		@Nullable
		String margin;

		@Nullable
		ComponentEventListener<ClickEvent<Button>> onClick;

		@Nullable
		String text;

		@Nullable
		List<ButtonVariant> themeVariants;

		@Nullable
		String tooltipText;

		@Nullable
		Boolean visible;

		public static class Builder {

			public Builder themeVariants(List<ButtonVariant> themeVariants) {
				this.themeVariants = themeVariants;
				return this;
			}

			public Builder themeVariants(ButtonVariant... themeVariants) {
				return themeVariants(List.of(themeVariants));
			}
		}
	}
}
