package tk.labyrinth.pandora.functionalvaadin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.shared.Registration;
import io.vavr.collection.List;
import org.apache.logging.log4j.util.TriConsumer;
import tk.labyrinth.pandora.functionalvaadin.offstage.OffstageComponentState;
import tk.labyrinth.pandora.functionalvaadin.offstage.OffstageState;
import tk.labyrinth.pandora.functionalvaadin.sdk.ContainerWrapper;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.reactive.observable.Observable;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class FunctionalComponents {

	private static final ConcurrentMap<Component, OffstageComponentState> componentStates = new ConcurrentHashMap<>();

	private static void resetComponent(Component component) {
		OffstageComponentState componentState = componentStates.get(component);
		//
		clearRegistrations(componentState.getRegistrations());
		//
		List.ofAll(component.getStyle().getNames())
				.filter(name -> !componentState.getUnmodifiableStyles().contains(name))
				.forEach(name -> component.getStyle().remove(name));
		//
		// TODO: Remove not mentioned in the end.
//		if (component instanceof HasComponents hasComponents) {
//			hasComponents.removeAll();
//		}
	}

	public static void clearRegistrations(ArrayList<Registration> registrations) {
		registrations.forEach(Registration::remove);
		//
		registrations.clear();
	}

	@SuppressWarnings("unchecked")
	public static <C extends Component> C createComponent(Supplier<C> newComponentSupplier) {
		C result;
		{
			C newComponent = newComponentSupplier.get();
			//
			OffstageState state = OffstageState.get();
			//
			if (state != null) {
				Component nextComponent = state.findNext();
				//
				if (nextComponent != null) {
					if (nextComponent.getClass() == newComponent.getClass()) {
						state.recordNext(nextComponent);
						//
						resetComponent(nextComponent);
						//
						result = (C) nextComponent;
					} else {
						throw new IllegalStateException();
					}
				} else {
					componentStates.put(
							newComponent,
							OffstageComponentState.builder()
									.registrations(new ArrayList<>())
									.unmodifiableStyles(List.ofAll(newComponent.getStyle().getNames()))
									.build());
					//
					state.recordNext(newComponent);
					//
					result = newComponent;
				}
			} else {
				result = newComponent;
			}
		}
		return result;
	}

	public static <C extends Component> ContainerWrapper<C> createContainer(Supplier<C> newComponentSupplier) {
		return ContainerWrapper.of(createComponent(newComponentSupplier));
	}

	public static <S, C extends Component> Component createRootContainer(
			Observable<S> stateObservable,
			Supplier<C> newComponentSupplier,
			TriConsumer<S, Consumer<S>, ContainerWrapper<C>> renderFunction) {
		return FunctionalComponent2.of(
				stateObservable,
				(state, sink) -> {
					C component = FunctionalComponents.createComponent(newComponentSupplier);
					//
					renderFunction.accept(state, sink, ContainerWrapper.of(component));
					//
					return component;
				});
	}

	public static void recordRegistration(Component component, Registration registration) {
		OffstageComponentState componentState = componentStates.get(component);
		//
		if (componentState != null) {
			componentState.getRegistrations().add(registration);
		}
	}

	public static <C extends Component> Component render(
			Supplier<C> newComponentSupplier,
			Consumer<ContainerWrapper<C>> renderFunction) {
		// TODO
		throw new NotImplementedException();
	}
}
