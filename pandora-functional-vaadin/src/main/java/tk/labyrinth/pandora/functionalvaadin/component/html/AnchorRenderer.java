package tk.labyrinth.pandora.functionalvaadin.component.html;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class AnchorRenderer {

	public static Anchor render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Anchor render(Parameters parameters) {
		Anchor anchor = new Anchor();
		{
			Optional.ofNullable(parameters.href()).ifPresent(anchor::setHref);
			Optional.ofNullable(parameters.target()).ifPresent(anchor::setTarget);
			Optional.ofNullable(parameters.text()).ifPresent(anchor::setText);
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(anchor));
		}
		return anchor;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<Anchor> configurer;

		@Nullable
		String href;

		@Nullable
		AnchorTarget target;

		@Nullable
		String text;

		public static class Builder {
			// Lomboked
		}
	}
}
