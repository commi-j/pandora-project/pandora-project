package tk.labyrinth.pandora.functionalvaadin.offstage;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.ArrayList;
import java.util.function.Supplier;

public class OffstageState {

	private static final ThreadLocal<OffstageState> stateThreadLocal = new ThreadLocal<>();

	private int currentIndex = 0;

	private ArrayList<Pair<Object, Component>> currentlyRecordedComponents = null;

	private List<Pair<Object, Component>> lastRecordedComponents = List.empty();

	public Component compute(Supplier<Component> componentSupplier) {
		OffstageState currentState = stateThreadLocal.get();
		//
		if (currentState != null) {
			throw new IllegalStateException();
		}
		//
		currentlyRecordedComponents = new ArrayList<>();
		currentIndex = 0;
		stateThreadLocal.set(this);
		//
		try {
			//
			return componentSupplier.get();
		} finally {
			stateThreadLocal.set(null);
			//
			lastRecordedComponents = List.ofAll(currentlyRecordedComponents);
			currentlyRecordedComponents = null;
		}
	}

	@Nullable
	public Component findNext() {
		return lastRecordedComponents.size() > currentIndex
				? lastRecordedComponents.get(currentIndex++).getRight()
				: null;
	}

	public void recordNext(Component component) {
		currentlyRecordedComponents.add(Pair.of(null, component));
	}

	public static OffstageState get() {
		return stateThreadLocal.get();
	}
}
