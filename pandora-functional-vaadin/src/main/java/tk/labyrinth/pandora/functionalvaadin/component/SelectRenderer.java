package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.select.Select;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public class SelectRenderer {

	public static <T> Select<T> render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static <T> Select<T> render(Parameters<T> parameters) {
		Select<T> select = FunctionalComponents.createComponent(Select::new);
		{
			{
				List<T> items = parameters.items();
				//
				if (items != null && items.contains(null)) {
					throw new IllegalArgumentException("Require no null value: items = %s".formatted(items));
				}
				//
				select.setItems(items);
			}
			select.setLabel(parameters.label());
			Optional.ofNullable(parameters.nullValueCaption()).ifPresentOrElse(
					nullValueCaption -> {
						select.setEmptySelectionAllowed(true);
						select.setEmptySelectionCaption(nullValueCaption);
					},
					() -> {
						select.setEmptySelectionAllowed(false);
						select.setEmptySelectionCaption("");
					});
			select.setReadOnly(BooleanUtils.isTrue(parameters.readOnly()));
			ElementUtils.setTitle(select, parameters.title());
			select.setValue(parameters.value());
			select.setWidth(parameters.width());
		}
		{
			// FIXME: Rerendering may work improperly here.
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(select));
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			FunctionalComponents.recordRegistration(
					select,
					select.addValueChangeListener(event -> {
						if (event.isFromClient()) {
							valueChanged.set(true);
							//
							event.getSource().setValue(event.getOldValue());
							//
							if (parameters.onValueChange() != null) {
								parameters.onValueChange().accept(event.getValue());
							}
						}
					}));
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> FunctionalComponents.recordRegistration(
					select,
					select.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false)))
			));
		}
		return select;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<T> {

		@Nullable
		Consumer<Select<T>> configurer;

		@Nullable
		ItemLabelGenerator<T> itemLabelGenerator;

		/**
		 * For null support see {@link #nullValueCaption()}.
		 */
		@Nullable
		List<@NonNull T> items;

		@Nullable
		String label;

		/**
		 * If the value is specified, null is added as a possible option.
		 */
		@Nullable
		String nullValueCaption;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable T> onValueChange;

		@Nullable
		Boolean readOnly;

		@Nullable
		String title;

		@Nullable
		T value;

		@Nullable
		String width;

		public static class Builder<T> {
			// Lomboked
		}
	}
}
