package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.tabs.Tab;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Function;

public class TabRenderer {

	public static Tab render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public static Tab render(Properties properties) {
		Tab tab = new Tab();
		{
			Optional.ofNullable(properties.getId()).ifPresent(tab::setId);
			Optional.ofNullable(properties.getLabel()).ifPresent(tab::setLabel);
			Optional.ofNullable(properties.getSelected()).ifPresent(tab::setSelected);
		}
		return tab;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		String id;

		@Nullable
		String label;

		@Nullable
		Boolean selected;

		public static class Builder {
			// Lomboked
		}
	}
}
