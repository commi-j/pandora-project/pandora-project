package tk.labyrinth.pandora.functionalvaadin.offstage;

import com.vaadin.flow.shared.Registration;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.ArrayList;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class OffstageComponentState {

	ArrayList<Registration> registrations;

	List<String> unmodifiableStyles;
}
