package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.html.Span;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

// Design version: 2024-01-02.
public class SpanRenderer {

	public static Span render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Span render(Parameters parameters) {
		Span span = FunctionalComponents.createComponent(Span::new);
		{
			span.setText(parameters.text());
			{
				span.getElement().getThemeList().clear();
				Optional.ofNullable(parameters.themes()).ifPresent(themes ->
						span.getElement().getThemeList().addAll(themes.asJava()));
			}
			span.setTitle(parameters.title() != null ? parameters.title() : "");
			span.setWidth(parameters.width());
		}
		{
			Optional.ofNullable(parameters.customizer()).ifPresent(customizer -> customizer.accept(span));
		}
		return span;
	}

	public static Span render(String text) {
		return render(builder -> builder
				.text(text)
				.build());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Nullable
	@Value
	@With
	public static class Parameters {

		Consumer<Span> customizer;

		String text;

		List<String> themes;

		String title;

		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
