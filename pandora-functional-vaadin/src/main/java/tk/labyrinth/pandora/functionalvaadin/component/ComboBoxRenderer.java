package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.combobox.ComboBox;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

// TODO: Think if we need this one or can use VaadinComponents instead.
public class ComboBoxRenderer {

	public static <T> ComboBox<T> render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static <T> ComboBox<T> render(Parameters<T> parameters) {
		ComboBox<T> comboBox = FunctionalComponents.createComponent(ComboBox::new);
		{
			// We always set generator to handle nulls as empty strings.
			// We also need to set it before items or value.
			comboBox.setItemLabelGenerator(Optional
					.ofNullable(parameters.itemLabelGenerator())
					.orElse(item -> item != null ? item.toString() : ""));
		}
		{
			comboBox.setAllowCustomValue(BooleanUtils.isTrue(parameters.allowCustomValue()));
			{
				if (parameters.items() != null) {
					comboBox.setItems(parameters.items());
				} else if (parameters.itemsFunction() != null) {
					comboBox.setItems(query -> {
						// FIXME: This is a dirty hack to skip Vaadin's validations.
						query.getLimit();
						query.getOffset();
						//
						return parameters.itemsFunction().apply(query.getFilter().orElse(null)).stream();
					});
				} else {
					comboBox.setItems(Collections.emptyList());
				}
			}
			comboBox.setLabel(parameters.label());
			comboBox.setReadOnly(BooleanUtils.isTrue(parameters.readOnly()));
			comboBox.setValue(parameters.value());
			comboBox.setWidth(parameters.width());
		}
		{
			// TODO: Not fully supports rerendering.
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(comboBox));
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			FunctionalComponents.recordRegistration(
					comboBox,
					comboBox.addValueChangeListener(event -> {
						if (event.isFromClient()) {
							valueChanged.set(true);
							//
							event.getSource().setValue(event.getOldValue());
							//
							if (parameters.onValueChange() != null) {
								parameters.onValueChange().accept(event.getValue());
							}
						}
					}));
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> FunctionalComponents.recordRegistration(
					comboBox,
					comboBox.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false)))));
		}
		return comboBox;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<T> {

		@Nullable
		Boolean allowCustomValue;

		@Nullable
		Consumer<ComboBox<T>> configurer;

		@Nullable
		ItemLabelGenerator<T> itemLabelGenerator;

		@Nullable
		List<T> items;

		@Nullable
		Function<String, List<T>> itemsFunction;

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable T> onValueChange;

		@Nullable
		Boolean readOnly;

		@Nullable
		T value;

		@Nullable
		String width;

		public static class Builder<T> {
			// Lomboked
		}
	}
}
