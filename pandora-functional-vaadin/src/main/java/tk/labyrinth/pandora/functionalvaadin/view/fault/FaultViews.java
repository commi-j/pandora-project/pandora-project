package tk.labyrinth.pandora.functionalvaadin.view.fault;

import com.vaadin.flow.component.textfield.TextArea;
import tk.labyrinth.pandora.datatypes.domain.fault.Fault;
import tk.labyrinth.pandora.datatypes.domain.fault.ThrowableFault;
import tk.labyrinth.pandora.functionalvaadin.dialog.Dialogs;

public class FaultViews {

	public static TextArea createView(Fault fault) {
		TextArea textArea = FaultViewRenderer.render(builder -> builder
				.fault(fault)
				.build());
		//
		textArea.setWidth("80vw");
		//
		return textArea;
	}

	public static TextArea createView(Throwable throwable) {
		return createView(ThrowableFault.from(throwable));
	}

	public static void showDialog(Fault fault) {
		Dialogs.show(createView(fault));
	}

	public static void showDialog(Throwable throwable) {
		Dialogs.show(createView(throwable));
	}
}
