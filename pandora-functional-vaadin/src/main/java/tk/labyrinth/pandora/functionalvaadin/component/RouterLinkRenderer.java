package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.router.RouterLink;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @see RouterLink
 */
public class RouterLinkRenderer {

	public static RouterLink render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static RouterLink render(Parameters parameters) {
		RouterLink routerLink = new RouterLink();
		{
			Optional.ofNullable(parameters.enabled()).ifPresent(routerLink::setEnabled);
			Optional.ofNullable(parameters.queryParameters()).ifPresent(routerLink::setQueryParameters);
			Optional.ofNullable(parameters.target()).ifPresent(target ->
					routerLink.getElement().setAttribute("target", target.getValue()));
			Optional.ofNullable(parameters.text()).ifPresent(routerLink::setText);
		}
		{
			if (parameters.route() != null) {
				if (parameters.routeParameters() != null) {
					routerLink.setRoute(parameters.route(), parameters.routeParameters());
				} else if (parameters.parameter() != null) {
					routerLink.setRoute((Class) parameters.route(), parameters.parameter());
				} else {
					routerLink.setRoute(parameters.route());
				}
			}
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(routerLink));
		}
		return routerLink;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Consumer<RouterLink> configurer;

		@Nullable
		Boolean enabled;

		@Nullable
		Object parameter;

		@Nullable
		QueryParameters queryParameters;

		@Nullable
		Class<? extends Component> route;

		@Nullable
		RouteParameters routeParameters;

		@Nullable
		AnchorTarget target;

		@Nullable
		String text;

		public static class Builder {
			// Lomboked
		}
	}
}
