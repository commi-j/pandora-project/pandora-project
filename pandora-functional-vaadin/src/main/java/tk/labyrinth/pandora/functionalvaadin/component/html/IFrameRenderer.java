package tk.labyrinth.pandora.functionalvaadin.component.html;

import com.vaadin.flow.component.html.IFrame;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class IFrameRenderer {

	public static IFrame render(Function<Properties.Builder, Properties> propertiesConfigurer) {
		return render(propertiesConfigurer.apply(Properties.builder()));
	}

	public static IFrame render(Properties properties) {
		IFrame iFrame = new IFrame();
		{
			Optional.ofNullable(properties.src()).ifPresent(iFrame::setSrc);
		}
		{
			Optional.ofNullable(properties.configurer()).ifPresent(configurer -> configurer.accept(iFrame));
		}
		return iFrame;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Properties {

		@Nullable
		Consumer<IFrame> configurer;

		@Nullable
		String src;

		public static class Builder {
			// Lomboked
		}
	}
}
