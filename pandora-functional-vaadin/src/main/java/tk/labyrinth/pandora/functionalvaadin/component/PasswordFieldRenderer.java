package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public class PasswordFieldRenderer {

	public static PasswordField render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static PasswordField render(Parameters parameters) {
		PasswordField passwordField = FunctionalComponents.createComponent(PasswordField::new);
		{
			passwordField.setAutocomplete(parameters.autocomplete());
			passwordField.setEnabled(BooleanUtils.isNotFalse(parameters.enabled()));
			passwordField.setHelperText(parameters.helperText());
			passwordField.setLabel(parameters.label());
			passwordField.setReadOnly(BooleanUtils.isTrue(parameters.readOnly()));
			passwordField.setRevealButtonVisible(BooleanUtils.isNotFalse(parameters.revealButtonVisible()));
			passwordField.setSuffixComponent(parameters.suffixComponent());
			passwordField.setValue(parameters.value());
			passwordField.setValueChangeMode(parameters.valueChangeMode() != null
					? parameters.valueChangeMode()
					: ValueChangeMode.ON_CHANGE);
			passwordField.setWidth(parameters.width());
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			FunctionalComponents.recordRegistration(
					passwordField,
					passwordField.addValueChangeListener(event -> {
						if (event.isFromClient()) {
							valueChanged.set(true);
							//
							event.getSource().setValue(event.getOldValue());
							//
							if (parameters.onValueChange() != null) {
								parameters.onValueChange().accept(event.getValue());
							}
						}
					}));
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> FunctionalComponents.recordRegistration(
					passwordField,
					passwordField.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false)))));
		}
		{
			// TODO: Support on clear.
			Optional.ofNullable(parameters.customizer()).ifPresent(customizer -> customizer.accept(passwordField));
		}
		return passwordField;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Autocomplete autocomplete;

		@Nullable
		Consumer<PasswordField> customizer;

		@Nullable
		Boolean enabled;

		@Nullable
		String helperText;

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@NonNull String> onValueChange;

		@Nullable
		Boolean readOnly;

		@Nullable
		Boolean revealButtonVisible;

		@Nullable
		Component suffixComponent;

		@NonNull
		String value;

		@Nullable
		ValueChangeMode valueChangeMode;

		@Nullable
		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
