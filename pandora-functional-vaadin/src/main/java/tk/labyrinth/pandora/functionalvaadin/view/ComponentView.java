package tk.labyrinth.pandora.functionalvaadin.view;

import com.vaadin.flow.component.Component;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class ComponentView implements View {

	private final Component component;

	@Override
	public Component asVaadinComponent() {
		return component;
	}
}
