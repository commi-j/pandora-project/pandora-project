package tk.labyrinth.pandora.functionalvaadin.view;

import com.vaadin.flow.component.Component;

public interface View {

	Component asVaadinComponent();
}
