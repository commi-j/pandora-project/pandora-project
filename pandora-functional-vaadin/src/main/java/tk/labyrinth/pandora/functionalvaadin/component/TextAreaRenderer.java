package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.value.ValueChangeMode;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.ElementUtils;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public class TextAreaRenderer {

	public static TextArea render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static TextArea render(Parameters parameters) {
		TextArea textArea = FunctionalComponents.createComponent(TextArea::new);
		{
			textArea.setAutocomplete(parameters.autocomplete());
			textArea.setEnabled(BooleanUtils.isNotFalse(parameters.enabled()));
			textArea.setHelperText(parameters.helperText());
			textArea.setLabel(parameters.label());
			textArea.setPattern(parameters.pattern());
			textArea.setPrefixComponent(parameters.prefixComponent());
			textArea.setReadOnly(BooleanUtils.isTrue(parameters.readOnly()));
			textArea.setSuffixComponent(parameters.suffixComponent());
			ElementUtils.setTitle(textArea, parameters.title());
			textArea.setValue(parameters.value());
			textArea.setValueChangeMode(parameters.valueChangeMode() != null
					? parameters.valueChangeMode()
					: ValueChangeMode.ON_CHANGE);
			textArea.setWidth(parameters.width());
			Optional.ofNullable(parameters.label()).ifPresent(textArea::setLabel);
			Optional.ofNullable(parameters.readOnly()).ifPresent(textArea::setReadOnly);
			Optional.ofNullable(parameters.suffixComponent()).ifPresent(textArea::setSuffixComponent);
			Optional.ofNullable(parameters.value()).ifPresent(textArea::setValue);
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			FunctionalComponents.recordRegistration(
					textArea,
					textArea.addValueChangeListener(event -> {
						if (event.isFromClient()) {
							valueChanged.set(true);
							//
							event.getSource().setValue(event.getOldValue());
							//
							if (parameters.onValueChange() != null) {
								parameters.onValueChange().accept(!event.getValue().isEmpty() ? event.getValue() : null);
							}
						}
					}));
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> FunctionalComponents.recordRegistration(
					textArea,
					textArea.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false)))));
		}
		{
			// FIXME: Not fully supported with rerendering.
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(textArea));
		}
		return textArea;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Autocomplete autocomplete;

		@Nullable
		Consumer<TextArea> configurer;

		@Nullable
		Boolean enabled;

		@Nullable
		String helperText;

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<@Nullable String> onValueChange;

		@Nullable
		String pattern;

		@Nullable
		Component prefixComponent;

		@Nullable
		Boolean readOnly;

		@Nullable
		Component suffixComponent;

		@Nullable
		String title;

		@Nullable
		String value;

		@Nullable
		ValueChangeMode valueChangeMode;

		@Nullable
		String width;

		public static class Builder {
			// Lomboked
		}
	}
}
