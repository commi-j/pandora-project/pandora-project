package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;

import javax.annotation.CheckForNull;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class RadioButtonGroupRenderer {

	public static <T> RadioButtonGroup<T> render(Function<Parameters.Builder<T>, Parameters<T>> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static <T> RadioButtonGroup<T> render(Parameters<T> parameters) {
		RadioButtonGroup<T> radioButtonGroup = new RadioButtonGroup<>();
		{
			Optional.ofNullable(parameters.itemLabelGenerator()).ifPresent(radioButtonGroup::setItemLabelGenerator);
			Optional.ofNullable(parameters.items()).ifPresent(radioButtonGroup::setItems);
			Optional.ofNullable(parameters.onValueChange()).ifPresent(radioButtonGroup::addValueChangeListener);
			Optional.ofNullable(parameters.value()).ifPresent(radioButtonGroup::setValue);
		}
		{
			Optional.ofNullable(parameters.configurer()).ifPresent(configurer -> configurer.accept(radioButtonGroup));
		}
		return radioButtonGroup;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters<T> {

		@Nullable
		Consumer<RadioButtonGroup<T>> configurer;
//
//		@Nullable
//		Boolean disableOnClick;
//
//		@Nullable
//		Boolean enabled;
//
//		@Nullable
//		String height;
//
//		@Nullable
//		Component icon;
//
//		@Nullable
//		Boolean iconAfterText;

		@Nullable
		ItemLabelGenerator<T> itemLabelGenerator;

		@Nullable
		List<T> items;

		@CheckForNull // For some reason @Nullable gives error here.
		HasValue.ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<RadioButtonGroup<T>, T>> onValueChange;
//
//		@Nullable
//		String margin;
//
//		@Nullable
//		ComponentEventListener<ClickEvent<Button>> onClick;
//
//		@Nullable
//		String tooltipText;

		@Nullable
		T value;

		public static class Builder<T> {
//
//			public Builder<T> themeVariants(List<ButtonVariant> themeVariants) {
//				this.themeVariants = themeVariants;
//				//
//				return this;
//			}
//
//			public Builder<T> themeVariants(ButtonVariant... themeVariants) {
//				return themeVariants(List.of(themeVariants));
//			}
		}
	}
}
