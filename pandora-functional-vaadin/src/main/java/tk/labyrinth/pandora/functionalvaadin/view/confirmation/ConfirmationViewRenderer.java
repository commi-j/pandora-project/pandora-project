package tk.labyrinth.pandora.functionalvaadin.view.confirmation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.progressbar.ProgressBar;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class ConfirmationViewRenderer {

	public static Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Component render(Parameters parameters) {
		CssVerticalLayout layout = FunctionalComponents.createComponent(CssVerticalLayout::new);
		{
			// FIXME: PandoraStyles.LAYOUT_SMALL.
			layout.addClassName("pandora-layout-small");
		}
		{
			CssFlexItem.setFlexGrow(parameters.content().asVaadinComponent(), 1);
			//
			layout.add(parameters.content().asVaadinComponent());
		}
		{
			layout.add(FunctionalComponents.createComponent(() -> new ProgressBar(0, 1, 1)));
		}
		{
			CssHorizontalLayout buttonFooter = FunctionalComponents.createComponent(CssHorizontalLayout::new);
			{
				// FIXME: PandoraStyles.LAYOUT_SMALL.
				layout.addClassName("pandora-layout-small");
			}
			{
				if (BooleanUtils.isNotFalse(parameters.showCancelButton())) {
					buttonFooter.add(ButtonRenderer.render(builder -> builder
							.configurer(button -> button.getElement().setAttribute("name", ConfirmationViews.CANCEL_BUTTON_NAME))
							.onClick(event -> {
								Consumer<Boolean> onClose = parameters.onClose();
								//
								if (onClose != null) {
									onClose.accept(false);
								}
							})
							.text("Cancel")
							.themeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
							.build()));
				}
				//
				buttonFooter.addStretch();
				//
				if (BooleanUtils.isTrue(parameters.showApplyButton())) {
					buttonFooter.add(ButtonRenderer.render(builder -> builder
							.configurer(button -> button.getElement().setAttribute("name", ConfirmationViews.APPLY_BUTTON_NAME))
							.onClick(event -> {
								Supplier<Boolean> onApply = parameters.onApply();
								//
								if (onApply != null) {
									onApply.get();
								}
							})
							.text("Apply")
							.themeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
							.build()));
				}
				//
				if (BooleanUtils.isNotFalse(parameters.showConfirmButton())) {
					buttonFooter.add(ButtonRenderer.render(builder -> builder
							.configurer(button -> button.getElement().setAttribute("name", ConfirmationViews.APPLY_BUTTON_NAME))
							.onClick(event -> {
								Supplier<Boolean> onApply = parameters.onApply();
								//
								boolean succeeded = onApply != null ? onApply.get() : true;
								//
								if (succeeded) {
									Consumer<Boolean> onClose = parameters.onClose();
									//
									if (onClose != null) {
										onClose.accept(true);
									}
								}
							})
							.text("Confirm")
							.themeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
							.build()));
				}
			}
			layout.add(buttonFooter);
		}
		return layout;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		View content;

		/**
		 * Invoked when <b>apply</b> or <b>confirm</b> is triggered.
		 * If <b>true</b> is returned and triggered action is <b>confirm</b>, it proceeds with invoking {@link #onClose}.<br>
		 * If not provided, <b>always apply</b> strategy is used.<br>
		 */
		@Nullable
		Supplier<Boolean> onApply;

		/**
		 * Invoked when <b>confirm</b> is triggered and {@link #onApply} returns <b>true</b>
		 * or when <b>close</b> is triggered.<br>
		 * Receives the value indicating whether this close was triggered by apply (true) or cancel (false).
		 */
		@Nullable
		Consumer<Boolean> onClose;

		/**
		 * Default: false.
		 */
		@Nullable
		Boolean showApplyButton;

		/**
		 * Default: true.
		 */
		@Nullable
		Boolean showCancelButton;

		/**
		 * Default: true.
		 */
		@Nullable
		Boolean showConfirmButton;

		public static class Builder {
			// Lomboked
		}
	}
}
