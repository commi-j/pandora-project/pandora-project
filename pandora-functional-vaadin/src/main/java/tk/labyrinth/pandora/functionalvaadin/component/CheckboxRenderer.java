package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.BooleanUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public class CheckboxRenderer {

	public static Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Component render(Parameters parameters) {
		Checkbox checkbox = FunctionalComponents.createComponent(Checkbox::new);
		{
			checkbox.setEnabled(BooleanUtils.isNotFalse(parameters.enabled()));
			checkbox.setLabel(parameters.label());
			checkbox.setValue(parameters.value());
		}
		{
			AtomicBoolean valueChanged = new AtomicBoolean(false);
			//
			FunctionalComponents.recordRegistration(
					checkbox,
					checkbox.addValueChangeListener(event -> {
						if (event.isFromClient()) {
							valueChanged.set(true);
							//
							event.getSource().setValue(event.getOldValue());
							//
							if (parameters.onValueChange() != null) {
								parameters.onValueChange().accept(event.getValue());
							}
						}
					}));
			Optional.ofNullable(parameters.onBlur()).ifPresent(onBlur -> FunctionalComponents.recordRegistration(
					checkbox,
					checkbox.addBlurListener(event -> onBlur.accept(valueChanged.getAndSet(false)))));
		}
		return checkbox;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		Boolean enabled;

		@Nullable
		String label;

		/**
		 * true if value was changed before, false otherwise.
		 */
		@Nullable
		Consumer<Boolean> onBlur;

		@Nullable
		Consumer<Boolean> onValueChange;

		@Nullable
		Boolean value;

		public static class Builder {
			// Lomboked
		}
	}
}
