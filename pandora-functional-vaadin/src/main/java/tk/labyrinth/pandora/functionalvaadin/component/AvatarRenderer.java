package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.avatar.Avatar;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

// Design version: 2023-12-10.
public class AvatarRenderer {

	public static Avatar render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static Avatar render(Parameters parameters) {
		Avatar avatar = new Avatar();
		{
			Optional.ofNullable(parameters.imageUrl()).ifPresent(avatar::setImage);
			Optional.ofNullable(parameters.name()).ifPresent(avatar::setName);
			Optional.ofNullable(parameters.tooltipEnabled()).ifPresent(avatar::setTooltipEnabled);
		}
		{
			Optional.ofNullable(parameters.customizer()).ifPresent(customizer -> customizer.accept(avatar));
		}
		return avatar;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Nullable
	@Value
	@With
	public static class Parameters {

		Consumer<Avatar> customizer;

		String imageUrl;

		String name;

		String title; // TODO

		Boolean tooltipEnabled;

		public static class Builder {
			// Lomboked
		}
	}
}
