package tk.labyrinth.pandora.functionalvaadin.offstage;

import com.vaadin.flow.component.Component;
import io.vavr.collection.List;

public class OffstageComponentChildrenHandler {

	private static final ThreadLocal<List<Component>> tl = new ThreadLocal<>();

	public static void handle(Component component, Runnable runnable) {
		tl.set(List.empty());
		//
		try {
			runnable.run();
			//
			List<Component> recordedAddedChildren = tl.get();
			//
			component.getChildren()
					.filter(child -> !recordedAddedChildren.contains(child))
					.forEach(Component::removeFromParent);
		} finally {
			tl.remove();
		}
	}

	public static void recordAdded(Component component) {
		List<Component> currentState = tl.get();
		//
		if (currentState != null) {
			tl.set(currentState.append(component));
		}
	}
}
