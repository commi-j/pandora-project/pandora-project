package tk.labyrinth.pandora.functionalvaadin.component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;

import java.util.function.Consumer;

public class VaadinComponents {

	public static Button button(Consumer<Button> buttonConfigurer) {
		Button contextMenu = new Button();
		//
		buttonConfigurer.accept(contextMenu);
		//
		return contextMenu;
	}

	public static ContextMenu contextMenu(Consumer<ContextMenu> contextMenuConfigurer) {
		ContextMenu contextMenu = new ContextMenu();
		//
		contextMenuConfigurer.accept(contextMenu);
		//
		return contextMenu;
	}

	public static IntegerField integerField(Consumer<IntegerField> integerFieldConfigurer) {
		IntegerField integerField = new IntegerField();
		//
		integerFieldConfigurer.accept(integerField);
		//
		return integerField;
	}

	public static NumberField numberField(Consumer<NumberField> numberFieldConfigurer) {
		NumberField numberField = new NumberField();
		//
		numberFieldConfigurer.accept(numberField);
		//
		return numberField;
	}
}
